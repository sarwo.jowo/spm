<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gtc_m extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	
	
	function get_gtc_ajax($offset, $limit, $q='', $sort, $order) {
		$sql = "SELECT * FROM history_gtc";
			
		$result['count'] = $this->db->query($sql)->num_rows();
		$sql .=" ORDER BY {$sort} {$order} ";
		$sql .=" LIMIT {$offset},{$limit} ";
		$result['data'] = $this->db->query($sql)->result();
		return $result;
	}
	
	function get_lastgtc($jenis=0){
		$this->db->where('jn', $jenis);
		$this->db->order_by('id', 'desc');
		return $this->db->get('gtc',1)->row();
	}
	
	public function create() {
		if (str_replace(',', '', $this->input->post('harga')) < 0) {
			return FALSE;
		}
		if (str_replace(',', '', $this->input->post('unit')) < 0) {
			return FALSE;
		}
		
		// $last_gtc = $this->get_lastgtc(1); 
		
		
		// TRANSACTIONAL DB COMMIT
		$this->db->trans_start();	
		$data = array(			
			'username'		=> 'dxplor',
			'price'		    => str_replace(',', '', $this->input->post('harga')),	
			'unit'		    => str_replace(',', '', $this->input->post('unit')),	
			'jn'		    => 1,	
			'ket'		    => 'Jual'	
				
		);		
		$this->db->insert('history_gtc', $data);
		
		$data_gtc = array(			
			'username'		=> 'dxplor',
			'price_jual'    => str_replace(',', '', $this->input->post('harga')),	
			'unit'		    => str_replace(',', '', $this->input->post('unit')),	
			'jn'		    => 1			
		);		
		$this->db->insert('gtc', $data_gtc);
		
		$data_transaksi = array(			
			'username'		=> 'dxplor',
			'price' 	    => str_replace(',', '', $this->input->post('harga')),	
			'unit'		    => str_replace(',', '', $this->input->post('unit')),	
			'jn'		    => 1,
			'type_tr'	    => 'J'
		);		
		$this->db->insert('gtc_transaksi', $data_transaksi);
		
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			// error insert
			return FALSE;
		} else {
			$this->db->trans_complete();
			return TRUE;
		}		
	}	
	
	public function get_gtc($jns=null)
	{
		// $tgl_dari = isset($_REQUEST['froms']) ? $_REQUEST['froms'] : '';
		// $tgl_sampai = isset($_REQUEST['to']) ? $_REQUEST['to'] : '';
		
		// if(!empty($tgl_dari) && !empty($tgl_sampai)){
			// $tgl_dari = explode(' ',$tgl_dari);
			// $tgl_daris = explode('-',$tgl_dari[0]);
			// $tgl_sampai = explode(' ',$tgl_sampai);
			// $tgl_sampais = explode('-',$tgl_sampai[0]);
			// $froms = $tgl_daris[2].'-'.$tgl_daris[1].'-'.$tgl_daris[0].' '.$tgl_dari[1];
			// $to = $tgl_sampais[2].'-'.$tgl_sampais[1].'-'.$tgl_sampais[0].' '.$tgl_sampai[1];
			// $this->db->where("GRAB_ON >=", $froms);
			// $this->db->where("GRAB_ON <=", $to);
		// }
		$res = array();
		$this->db->where('jenis', $jns);				
		$t = $this->db->get('master_point')->result_array();		
		if(!empty($t)){
			foreach($t as $gtc){
				$res[$gtc['username']] = $gtc['nilai'];
			}
		}
		return $res;
	}
	
	function get_gtc_jual(){
		$res = null;
		$this->db->where('jn',1);
		$this->db->where('type_tr','J');
		$t = $this->db->get('gtc_transaksi')->result_array();
		if(!empty($t)){
			$res = $t;
		}else{
			$this->db->where('jn !=',1);
			$this->db->where('type_tr','J');
			$res = $this->db->get('gtc_transaksi')->result_array();			
		}
			return $res;
	}
	
	function buy_gtc(){
		$id = isset($_POST['id_gtc']) ? $_POST['id_gtc'] : 0;
		$available_unit = isset($_POST['available_unit']) ? $_POST['available_unit'] : 0;
		$price_beli = isset($_POST['price_beli']) ? $_POST['price_beli'] : 0;
		$unit = isset($_POST['unit']) ? $_POST['unit'] : 0;
		//error_log(serialize($_POST));
		$sisa = $available_unit - $unit;
		$this->db->trans_start();
		$data_insert = array(
			'unit' 			=> $unit,
			'price_beli' 	=> $price_beli,
			'jn'			=> 2,
			'username' 		=> $this->session->userdata('u_name')
		);
		$this->db->insert('gtc', $data_insert);
		
		$this->db->where('id', $id);
		$this->db->update('gtc_transaksi', array('unit'=>$sisa));
		
		$data_history = array(
			'username' 	=> $this->session->userdata('u_name'),
			'price'	   	=> $price_beli,
			'unit' 		=> $unit,
			'jn'		=> 2,
			'ket'		=> 'Beli'
		);
		$this->db->insert('history_gtc', $data_history);	
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			// error insert
			return FALSE;
		} else {
			$this->db->trans_complete();
			return TRUE;
		}
	}
}

/* End of file pendapatan.php */
/* Location: ./application/models/pendapatan.php */