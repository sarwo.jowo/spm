<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Withdraw_m extends CI_Model {
	public function __construct() {
		parent::__construct();
	}			
	
	function get_member_ajax($offset, $limit, $sort, $order) {
		$sql = "SELECT * FROM master_point ";			
		$sql .= "where jenis='cash' ";			
		$result['count'] = $this->db->query($sql)->num_rows();
		$sql .=" ORDER BY {$sort} {$order} ";
		$sql .=" LIMIT {$offset},{$limit} ";
		$result['data'] = $this->db->query($sql)->result();		
		return $result;
	}
	
	function get_member_pending(){
		$tgl_dari = isset($_REQUEST['froms']) ? $_REQUEST['froms'] : '';
		$tgl_sampai = isset($_REQUEST['to']) ? $_REQUEST['to'] : '';
		if(!empty($tgl_dari) && !empty($tgl_sampai)){
			$froms = date('Y-m-d', strtotime($tgl_dari));
			$to = date('Y-m-d', strtotime($tgl_sampai));
			$this->db->where('tgl_withdraw >=', $froms);
			$this->db->where('tgl_withdraw <=', $to);
		}
		$this->db->select("withdraw.*, member.bank, member.norek, member.nama");
		$this->db->where("withdraw.status", "Withdraw");
		$this->db->join("member", "member.username = withdraw.username", "left");
		$t = $this->db->get('withdraw')->result_array();		
		return $t;
	}
	
	function get_member_bayar(){
		$tgl_dari = isset($_REQUEST['froms']) ? $_REQUEST['froms'] : '';
		$tgl_sampai = isset($_REQUEST['to']) ? $_REQUEST['to'] : '';
		if(!empty($tgl_dari) && !empty($tgl_sampai)){
			$froms = date('Y-m-d', strtotime($tgl_dari));
			$to = date('Y-m-d', strtotime($tgl_sampai));
			$this->db->where('tgl_bayar >=', $froms);
			$this->db->where('tgl_bayar <=', $to);
		}
		$this->db->where("status", "Paid");
		
		$t = $this->db->get('withdraw')->result_array();		
		return $t;
	}
	
	function save(){
		$username = $_POST['nama_anggota'];
		$available = $_POST['available'];
		$withdraw = $_POST['withdraw_ammount'];
		$this->db->trans_start();	
		$data_insert = array(
			'username' 		=> $username,
			'jumlah'	 	=> $withdraw,
			'tgl_withdraw' 	=> date('Y-m-d'),
			'petugas' 		=> $this->session->userdata('u_name'),
			'status' 		=> 'Withdraw'
		);
		$this->db->insert('withdraw', $data_insert);
		
		$sisa = $available - $withdraw;
		$data_point = array(
			'username' 		=> $username,
			'jenis' 		=> 'cash',
			'nilai' 		=> '-'.$withdraw,
			'Description' 	=> 'Withdraw',
			'Balance' 		=> $sisa
		);
		$this->db->insert('point', $data_point);
		
		$this->db->where('username', $username);
		$this->db->where('jenis', 'cash');
		$this->db->update('master_point', array('nilai' => $sisa));
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			// error insert
			return FALSE;
		} else {
			$this->db->trans_complete();
			return TRUE;
		}
	}
	
	function proses(){
		$member = $_POST['member'];
		$this->db->where_in('id', $member);
		$this->db->where('status', 'Withdraw');
		return $this->db->update('withdraw',
				array('status' => 'Paid', 
					  'petugas'=>$this->session->userdata('u_name'),
					  'tgl_bayar'=> date('Y-m-d')));
	}
	
}