<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Packages_m extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	function get_packages_ajax($offset, $limit, $q='', $sort, $order) {
		$sql = "SELECT * FROM package ";			
		$result['count'] = $this->db->query($sql)->num_rows();
		$sql .=" ORDER BY {$sort} {$order} ";
		$sql .=" LIMIT {$offset},{$limit} ";
		$result['data'] = $this->db->query($sql)->result();
		return $result;
	}
	
	function get_package(){
		return $this->db->get('package')->result_array();
	}
	
	public function create() {
		if (str_replace(',', '', $this->input->post('price')) < 0) {
			return FALSE;
		}
		if (str_replace(',', '', $this->input->post('pv')) < 0) {
			return FALSE;
		}
		
		if (str_replace(',', '', $this->input->post('shop_points')) < 0) {
			return FALSE;
		}
		
		if (str_replace(',', '', $this->input->post('ppsi_points')) < 0) {
			return FALSE;
		}
		if (str_replace(',', '', $this->input->post('coins')) < 0) {
			return FALSE;
		}
		if (str_replace(',', '', $this->input->post('adv_credits')) < 0) {
			return FALSE;
		}
		if (str_replace(',', '', $this->input->post('kompasangan')) < 0) {
			return FALSE;
		}
		if (str_replace(',', '', $this->input->post('komsponsor')) < 0) {
			return FALSE;
		}
		if (str_replace(',', '', $this->input->post('matchingupline')) < 0) {
			return FALSE;
		}
		if (str_replace(',', '', $this->input->post('komgenerasi')) < 0) {
			return FALSE;
		}
		$id_sewa = 0;
		if($this->input->post('id_package') > 0) $id_package = $this->input->post('id_package');
		
		// TRANSACTIONAL DB COMMIT
		$price = str_replace(',', '', $this->input->post('price'));
		$percent_pasangan = str_replace(',', '', $this->input->post('kompasangan'));
		$percent_sponsor = str_replace(',', '', $this->input->post('komsponsor'));
		$percent_matchingupline = str_replace(',', '', $this->input->post('matchingupline'));
		$percent_generasi = str_replace(',', '', $this->input->post('komgenerasi'));
		
		$this->db->trans_start();	
		$data = array(			
			'package'					=> ucwords($this->input->post('nama_package')),
			'price'		    			=> $price,	
			'pv'						=> str_replace(',', '', $this->input->post('pv')),			
			'shopping_points'			=> str_replace(',', '', $this->input->post('shop_points')),	
			'ppsi_points'				=> str_replace(',', '', $this->input->post('ppsi_points')),					
			'gtc'						=> str_replace(',', '', $this->input->post('coins')),	
			'adv_credits'				=> str_replace(',', '', $this->input->post('adv_credits')),	
			'kompasangan'				=> ($percent_pasangan/100) * $price,	
			'komsponsor'				=> ($percent_sponsor/100) * $price,	
			'matchingupline'			=> $percent_matchingupline, //($percent_matchingupline/100) * $price,
			'komgenerasi'				=> $percent_generasi, //($percent_generasi/100) * $price,			
			'percent_pasangan'			=> $percent_pasangan,	
			'percent_sponsor'			=> $percent_sponsor,	
			'percent_matchingupline'	=> $percent_matchingupline,	
			'percent_generasi'			=> $percent_generasi
		);
		if(!empty($id_package)){
			$this->db->where('id_package', $id_package);
			$this->db->update('package', $data);
		}else{
			$this->db->insert('package', $data);
		}		
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			// error insert
			return FALSE;
		} else {
			$this->db->trans_complete();
			return TRUE;
		}		
	}	
	
}

