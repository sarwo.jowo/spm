<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_m extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	//hitung jumlah anggota total
	function get_anggota_all() {
		$this->db->select('*');
		$this->db->from('member');
		$query = $this->db->get();
		return $query->num_rows();
	}

	

	//hitung jumlah anggota tdk aktif
	function get_rate($date=null){
		$this->db->where('date', $date);
		$this->db->select('rate');
		return $this->db->get('tbl_ppsi_rate')->row();
	}

	
	//menghitung jumlah points
	function get_sum($jns) {
		$cnts = '00';
		$this->db->select_sum('nilai');
		$this->db->where('jenis', $jns);		
		$query = $this->db->get('master_point')->row();	
		$cnt = $query->nilai;
		if($cnt > 0){
			$cnts = $cnt;
		}
		return $cnts;
	}
	
	function get_gtc() {
		$cnts = '00';
		$this->db->select_sum('unit');			
		$query = $this->db->get('gtc')->row();	
		$cnt = $query->unit;
		if($cnt > 0){
			$cnts = $cnt;
		}
		return $cnts;
	}

	
}