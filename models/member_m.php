<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Member_m extends CI_Model {

	public function validasi() {
		$form_rules = array(
			array(
				'field' => 'u_name',
				'label' => 'username',
				'rules' => 'required'
				),
			array(
				'field' => 'pass_word',
				'label' => 'password',
				'rules' => 'required'
				),
			);
		$this->form_validation->set_rules($form_rules);

		if ($this->form_validation->run()) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

    // cek status user, login atau tidak?
	public function cek_user() {
		$u_name = $this->input->post('u_name');
		$pass_word = md5($this->input->post('pass_word'));

		$query = $this->db->where('username', $u_name)
		->where('pass', $pass_word)		
		->limit(1)
		->get('member');

		if ($query->num_rows() == 1) {
			$row = $query->row();
			//$level = $row->level;
			$data = array(
				'login'		=> TRUE,
				'u_name' 	=> $u_name, 
				'id'		=> $row->id,
				'level' 	=> 'member'
			);
			// simpan data session jika login benar
			$this->session->set_userdata($data);
			return TRUE;
		} else {
			return FALSE;
		}
	}

	

}
