<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Record_m extends CI_Model {
	public function __construct() {
		parent::__construct();
	}		
	
	function get_record($jns=null, $member=null) {	
		$tgl_dari = isset($_REQUEST['from_date']) ? $_REQUEST['from_date'] : '';
		$tgl_sampai = isset($_REQUEST['to_date']) ? $_REQUEST['to_date'] : '';
		$jam = date('H:i:s');
		if(!empty($tgl_dari) && !empty($tgl_sampai)){
			$tgl_dari = explode(' ',$tgl_dari);
			$tgl_daris = explode('-',$tgl_dari[0]);
			$tgl_sampai = explode(' ',$tgl_sampai);
			$tgl_sampais = explode('-',$tgl_sampai[0]);
			$froms = $tgl_daris[2].'-'.$tgl_daris[1].'-'.$tgl_daris[0].' '.$jam;
			$to = $tgl_sampais[2].'-'.$tgl_sampais[1].'-'.$tgl_sampais[0].' '.$jam;
			
			$this->db->where("tanggal >=", $froms);
			$this->db->where("tanggal <=", $to);
		}
		$this->db->where('jenis', $jns);		
		$this->db->where('username', str_replace('%20',' ',$member));		
		$query = $this->db->get('point')->result_array();		
		
		return $query;
	}
	
	function get_withdraw($username=null, $status=null){
		$tgl_dari = isset($_REQUEST['froms']) ? $_REQUEST['froms'] : '';
		$tgl_sampai = isset($_REQUEST['to']) ? $_REQUEST['to'] : '';
		if(!empty($tgl_dari) && !empty($tgl_sampai)){
			$froms = date('Y-m-d', strtotime($tgl_dari));
			$to = date('Y-m-d', strtotime($tgl_sampai));
			$this->db->where('tgl_withdraw >=', $froms);
			$this->db->where('tgl_withdraw <=', $to);
		}
		if(!empty($status)){
			$this->db->where('status', $status);
		}
		$this->db->where('username', $username);
		return $this->db->get('withdraw')->result_array();
	}
	
	function get_point($jns=null, $member=null) {			
		$this->db->where('jenis', $jns);		
		$this->db->where('username', $member);		
		$query = $this->db->get('master_point')->row();		
		//error_log($this->db->last_query());
		return $query;
	}
	
	function get_member_ajax($offset, $limit, $q=null, $sort, $order) {
		$sql = "SELECT * FROM master_point ";
		$sql .= "WHERE jenis ='".$q."'";		
		$result['count'] = $this->db->query($sql)->num_rows();
		$sql .=" ORDER BY {$sort} {$order} ";
		$sql .=" LIMIT {$offset},{$limit} ";
		$result['data'] = $this->db->query($sql)->result();		
		return $result;
	}
	
	function get_member_withdraw($offset, $limit, $sort, $order) {
		$sql = "SELECT * FROM member ";		
		$result['count'] = $this->db->query($sql)->num_rows();
		$sql .=" ORDER BY {$sort} {$order} ";
		$sql .=" LIMIT {$offset},{$limit} ";
		$result['data'] = $this->db->query($sql)->result();		
		return $result;
	}
	
	function get_last_point(){
		$dup = array();
		$dt = $this->db->get('master_point')->result_array();
		if(!empty($dt)){
			foreach($dt as $t){
				$dup[$t['username']][$t['jenis']]['nilai'] = $t['nilai'];							
				$dup[$t['username']][$t['jenis']]['tgl'] = $t['last_update'];							
			}
		}	
		// error_log($this->db->last_query());
		return $dup;
	}
	
	function save(){
		$username = $_POST['nama_anggota'];
		$nilai_awal = $_POST['available'];        //19
		$nilai_conv = $_POST['trans_ammount'];    //3
		$from = $_POST['from'];
		$to = $_POST['to'];		
		
		$res = $this->get_point($to, $username)->nilai;
		$res = isset($res) ? $res : '0';
		// if(!empty($res)){
			// $res = $res;
		// }else{
			// $res = 0;
		// }
		
		$this->db->trans_start();	
		
		$hasil_pengurangan = $nilai_awal - $nilai_conv;
		$hasil_penambahan = $res + $nilai_conv;
		
		$data_kurang = array(
			'username'		=> $username,
			'nilai'			=> '-'.$nilai_conv,
			'Description'	=> 'Convert on date :',
			'jenis'			=> $from,
			'Balance'		=> $hasil_pengurangan
		);
		$this->db->insert('point', $data_kurang);
		
		$data_tambah = array(
			'username'		=> $username,
			'nilai'			=> $nilai_conv,
			'Description'	=> 'Convert on date :',
			'jenis'			=> $to,
			'Balance'		=> $hasil_penambahan
		);
		$this->db->insert('point', $data_tambah);
		
		$data_global = array(
			'kurang' => $hasil_pengurangan,
			'tambah' => $hasil_penambahan
		);
		$this->db->where('jenis', $from);
		$this->db->where('username', $username);
		$this->db->update('master_point', array('nilai'=>$hasil_pengurangan));
		
		$this->db->where('jenis', $to);
		$this->db->where('username', $username);
		$this->db->update('master_point', array('nilai'=>$hasil_penambahan));
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			// error insert
			return FALSE;
		} else {
			$this->db->trans_complete();
			return TRUE;
		}
		
	}
	
}