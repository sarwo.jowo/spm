<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transf_points_m extends CI_Model {
	public function __construct() {
		parent::__construct();
	}		
	
	function get_record($jns=null, $member=null) {	
		$tgl_dari = isset($_REQUEST['from_date']) ? $_REQUEST['from_date'] : '';
		$tgl_sampai = isset($_REQUEST['to_date']) ? $_REQUEST['to_date'] : '';
		$jam = date('H:i:s');
		if(!empty($tgl_dari) && !empty($tgl_sampai)){
			$tgl_dari = explode(' ',$tgl_dari);
			$tgl_daris = explode('-',$tgl_dari[0]);
			$tgl_sampai = explode(' ',$tgl_sampai);
			$tgl_sampais = explode('-',$tgl_sampai[0]);
			$froms = $tgl_daris[2].'-'.$tgl_daris[1].'-'.$tgl_daris[0].' '.$jam;
			$to = $tgl_sampais[2].'-'.$tgl_sampais[1].'-'.$tgl_sampais[0].' '.$jam;
			
			$this->db->where("tanggal >=", $froms);
			$this->db->where("tanggal <=", $to);
		}
		$this->db->where('jenis', $jns);		
		$this->db->where('username', $member);		
		$query = $this->db->get('point')->result_array();		
		return $query;
	}
	
	
	function get_point($jns=null, $member=null) {			
		$this->db->where('jenis', $jns);		
		$this->db->where('username', $member);		
		$query = $this->db->get('master_point')->row();		
		//error_log($this->db->last_query());
		return $query;
	}
	
	function get_member_ajax($offset, $limit, $q=null, $sort, $order) {
		$sql = "SELECT * FROM master_point ";
		$sql .= "WHERE jenis ='".$q."'";		
		$result['count'] = $this->db->query($sql)->num_rows();
		$sql .=" ORDER BY {$sort} {$order} ";
		$sql .=" LIMIT {$offset},{$limit} ";
		$result['data'] = $this->db->query($sql)->result();		
		return $result;
	}
	
	function get_last_point(){
		$dup = array();
		$dt = $this->db->get('master_point')->result_array();
		if(!empty($dt)){
			foreach($dt as $t){
				$dup[$t['username']][$t['jenis']]['nilai'] = $t['nilai'];							
				$dup[$t['username']][$t['jenis']]['tgl'] = $t['last_update'];							
			}
		}	
		//error_log($this->db->last_query());
		return $dup;
	}
	
	function save(){
		$username = $_POST['nama_anggota'];
		$jenis_transfer = $_POST['convert'];
		$nilai_awal = $_POST['available'];        
		$nilai_transfer = $_POST['trans_ammount'];
		
		if($jenis_transfer == 'cash'){
			$jenis_transfer = 'rgtr';
		}else{
			$jenis_transfer = $jenis_transfer;
		}
		
		$from = $username;
		$to = $_POST['to'];		
		
		$res = $this->get_point($jenis_transfer, $to);
		$res = isset($res) ? $res : '0';
		if(!empty($res)){
			$res = $res->nilai;
			$hasil_penambahan = $res + $nilai_transfer;
		}else{
			 $res = 0;
			 $hasil_penambahan = $nilai_transfer;
		}
		
		$this->db->trans_start();	
		
		$hasil_pengurangan = $nilai_awal - $nilai_transfer;
		
		
		$data_kurang = array(
			'username'		=> $username,
			'nilai'			=> '-'.$nilai_transfer,
			'Description'	=> 'Transfer to:'.$to. ' on date :',
			'jenis'			=> $jenis_transfer,
			'Balance'		=> $hasil_pengurangan
		);
		$this->db->insert('point', $data_kurang);
		
		$data_tambah = array(
			'username'		=> $to,
			'nilai'			=> $nilai_transfer,
			'Description'	=>'Transfer from:'.$username. ' on date :',
			'jenis'			=> $jenis_transfer,
			'Balance'		=> $hasil_penambahan
		);
		$this->db->insert('point', $data_tambah);		
		
		$this->db->where('jenis', $jenis_transfer);
		$this->db->where('username', $username);
		$this->db->update('master_point', array('nilai'=>$hasil_pengurangan));
		
		if($res > 0){
			$this->db->where('jenis', $jenis_transfer);
			$this->db->where('username', $to);
			$this->db->update('master_point', array('nilai'=>$hasil_penambahan));		
		}else{			
			$data_to = array(
				'jenis'		=> $jenis_transfer,
				'username'	=> $to,
				'nilai'		=> $hasil_penambahan
			);
			$this->db->insert('master_point', $data_to);
		}		
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			// error insert
			return FALSE;
		} else {
			$this->db->trans_complete();
			return TRUE;
		}
		
	}
	
	function get_paket($username=null){
		$this->db->select('paket');
		$this->db->where('username', $username);
		$paket = $this->db->get('member')->row();
		
		$this->db->select('matchingupline');
		$this->db->where('id_package', $paket->paket);
		$leader = $this->db->get('package')->row();		
		return $leader->matchingupline;
	}
	
	function dataupline($field=null, $username=null) {
		$dup = array();
		$this->db->select($field);
		$this->db->where('username', $username);
		$dt = $this->db->get('upline')->result_array();
		if(!empty($dt)){
			foreach($dt as $t){
				$dup[] = $t[$field];
			}
		}	
		return $dup;
	}
	
	function get_rate($date=null){
		$this->db->where('date', $date);
		$this->db->select('payout_rate');
		return $this->db->get('tbl_ppsi_rate')->row();
	}
	
	function get_points(){
		$res = array();
		$get = $this->db->get('master_point')->result_array();
		if(!empty($get)){
			foreach($get as $g){
				$res[$g['username']][$g['jenis']] = $g['nilai'];
			}
		}
		return $res;
	}
	
	function transfer_to_ppsi(){
		
		$jenis_from = $_POST['convert'];
		$username = $_POST['nama_anggota'];
		$available = $_POST['available'];
		$nilai_transfer = $_POST['trans_ammount'];
		
		$paket = $this->get_paket($username);	
		
		$hasil_pengurangan = $available - $nilai_transfer;
		$this->db->trans_start();	
		$data_point = array(
			'username'		=> $username,
			'nilai'			=> '-'.$nilai_transfer,
			'Description'	=> 'Transfer to: PPSI on date :',
			'jenis'			=> $jenis_from,
			'Balance'		=> $hasil_pengurangan
		);
		$this->db->insert('point', $data_point);
		
		$this->db->where('jenis', $jenis_from);
		$this->db->where('username', $username);
		$this->db->update('master_point', array('nilai'=>$hasil_pengurangan));
		
		$tgl = date('Y-m-d');
		$rate = $this->get_rate($tgl);
		$payout_rate = isset($rate->payout_rate) ? $rate->payout_rate : 0;
		
		$mpv = 2 * $nilai_transfer;
		$amount_received = ($payout_rate/100) * $nilai_transfer;
		$balance_receive = $mpv - $amount_received;
		$data_ppsi = array(
			'MDPRR'   			=> 'up to 10%',
			'username' 			=> $username, 		
			'DETAILS'			=> 'Deposit:'.$nilai_transfer,
			'MPV'				=> $mpv,
			'Amount_Received'	=> $amount_received,
			'Balance_receive'	=> $balance_receive,
			'VALUE' 			=> $nilai_transfer					
		);	
		$this->db->insert('ppsi', $data_ppsi);
		
		if($paket > 0){
			$last_point = $this->get_points();
			for($i=0; $i<$paket; $i++){
				$uplines = null;
				$upline = null;
				$uplines = 'upline'.$i;
				$upline = $this->dataupline($uplines, $m);
				$upline = isset($upline[0]) ? $upline[0] : 0;
				$bonus_ppsi = $nilai_transfer * (2/100);	
				
				$cash_incentive = $bonus_ppsi * (50/100);
				$sp_incentive = $bonus_ppsi * (20/100);
				$ppsi_incentive = $bonus_ppsi * (30/100);
	
				if(!empty($upline)){
					$last['rgtr']= isset($last_point[$upline]['rgtr']) ? $last_point[$upline]['rgtr'] : 0;
					$last['cash'] = isset($last_point[$upline]['cash']) ? $last_point[$upline]['cash'] : 0;
					
					$bonus_sponsor = array(
						'shop' => $sp_incentive,
						'cash' => $cash_incentive
					);
					
					foreach($bonus_sponsor as $keys=>$bs){
						$data_bonus = array();
						$data_bonus = array(
							'nilai'   		=> $bs,
							'username' 		=> $upline, 
							'Description' 	=> 'Bonus Top up PPSI Member '.$username, 
							'jenis' 		=> $keys, 
							'Balance' 		=> $last[$keys] + $bs
						);
						$this->db->insert('point', $data_points);
						
						$dt_upd = array();
						$dt_upd = array(
							'nilai'		=> $last[$keys] + $bs,
						);
						$this->db->where('jenis', $keys);
						$this->db->where('username', $username);
						$this->db->update('master_point', $dt_upd);
					}
					
					$data_ppsi = array();
					$mpv_sponsor = 2 * $ppsi_incentive;
					$amount_received_sponsor = ($payout_rate/100) * $mpv_sponsor;
					$balance_receive_sponsor = $mpv_sponsor - $amount_received_sponsor;
					$data_ppsi = array(
						'MDPRR'   			=> 'up to 10%',
						'username' 			=> $upline, 		
						'DETAILS'			=> 'Bonus Top up PPSI :'.$username,
						'MPV'				=> $mpv_sponsor,
						'Amount_Received'	=> $amount_received_sponsor,
						'Balance_receive'	=> $balance_receive_sponsor,
						'VALUE' 			=> $ppsi_incentive					
					);	
					$this->db->insert('master_point', $data_ppsi);
				}				
			}
		}
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			// error insert
			return FALSE;
		} else {
			$this->db->trans_complete();
			return TRUE;
		}
		
	}
	
}