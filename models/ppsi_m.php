<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ppsi_m extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function get($username=null)
	{
		$tgl_dari = isset($_REQUEST['froms']) ? $_REQUEST['froms'] : '';
		$tgl_sampai = isset($_REQUEST['to']) ? $_REQUEST['to'] : '';
		
		if(!empty($tgl_dari) && !empty($tgl_sampai)){
			$tgl_dari = explode(' ',$tgl_dari);
			$tgl_daris = explode('-',$tgl_dari[0]);
			$tgl_sampai = explode(' ',$tgl_sampai);
			$tgl_sampais = explode('-',$tgl_sampai[0]);
			$froms = $tgl_daris[2].'-'.$tgl_daris[1].'-'.$tgl_daris[0].' '.$tgl_dari[1];
			$to = $tgl_sampais[2].'-'.$tgl_sampais[1].'-'.$tgl_sampais[0].' '.$tgl_sampai[1];
			$this->db->where("GRAB_ON >=", $froms);
			$this->db->where("GRAB_ON <=", $to);
		}
		if(!empty($username)){
			$this->db->where('username', $username);
		}		
		$t = $this->db->get('ppsi')->result_array();		
		return $t;
	}
	
	function get_member_ajax($offset, $limit, $q='', $sort, $order) {
		$sql = "SELECT * FROM member ";
			
		$result['count'] = $this->db->query($sql)->num_rows();
		$sql .=" ORDER BY {$sort} {$order} ";
		$sql .=" LIMIT {$offset},{$limit} ";
		$result['data'] = $this->db->query($sql)->result();
		return $result;
	}
	
	function get_member_kompolsponsor($offset, $limit, $q='', $sort, $order) {
		$dt = $this->get_komppoolsponsor();
		$result = array();
		$names = array();
		if(!empty($dt)){
			$i=0;
			$sql = "SELECT * FROM member ";	
			foreach($dt as $t=>$k){				
				$names[] = $k;				
			}
			$tt = implode(',', $names);
			
					
			$sql .= "where username in(";
			foreach($dt as $t=>$k){				
				$sql .= "'".$k."',";				
			}
			$sql .="'vcx'";
			$sql .= ") ";			
			$result['count'] = $this->db->query($sql)->num_rows();
			$sql .=" ORDER BY {$sort} {$order} ";
			$sql .=" LIMIT {$offset},{$limit} ";
			$result['data'] = $this->db->query($sql)->result();
		}	
		
		return $result;
	}
	
	
	function rate_list($offset, $limit, $sort, $order) {
	
	    $sql = "SELECT * FROM tbl_ppsi_rate";			
		$result['count'] = $this->db->query($sql)->num_rows();
		$sql .=" ORDER BY {$sort} {$order} ";
		$sql .=" LIMIT {$offset},{$limit} ";
		$result['data'] = $this->db->query($sql)->result();
		
		return $result;
	}
	
	function rate(){
		return $this->db->get('tbl_ppsi_rate')->result_array();
	}
	
	function get_rate($date=null){
		$this->db->where('date', $date);
		$this->db->select('rate');
		return $this->db->get('tbl_ppsi_rate',1)->row();
	}
	
	function get_ppsi_rate(){
		$this->db->order_by('id', 'DESC');
		$this->db->select('payout_rate');
		return $this->db->get('tbl_ppsi_rate',1)->row(); //198
	}

	public function create() {
		if (str_replace(',', '', $this->input->post('rate')) < 0) {
			return FALSE;
		}
		if (str_replace(',', '', $this->input->post('date')) < 0) {
			return FALSE;
		}
				
		$id_sewa = 0;
		if($this->input->post('id') > 0) $id = $this->input->post('id');
		$rate = $this->get_ppsi_rate();
		$ppsi_rate = isset($rate->payout_rate) ? $rate->payout_rate : 0;	
		
		$ppsi_rates = $ppsi_rate + $this->input->post('rate');	
		if($ppsi_rate > 0 && $ppsi_rates < 200){
			$ppsi_rate = $ppsi_rates;	
		}else{
			$ppsi_rate = $this->input->post('rate');
		}		
		
		// TRANSACTIONAL DB COMMIT
		$this->db->trans_start();	
		$data = array(			
			'payout_rate'	=> $ppsi_rate,
			'rate'		    => str_replace(',', '', $this->input->post('rate')),	
			'date'			=> date('Y-m-d')		
		);		
		
		if(!empty($id)){
			$data = array(			
				'payout_rate'	=> $ppsi_rate,
				'rate'		    => str_replace(',', '', $this->input->post('rate'))				
			);	
			$this->db->where('id', $id);
			$this->db->update('tbl_ppsi_rate', $data);
		}else{
			$this->db->insert('tbl_ppsi_rate', $data);
		}		
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			// error insert
			return FALSE;
		} else {
			$this->db->trans_complete();
			return TRUE;
		}		
	}	
	
	function get_ppsi($username=null){		
		$r = 0;
		$this->db->select_sum('VALUE');
		$this->db->where('username', $username);
		$this->db->group_by('username');
		$r = $this->db->get('ppsi',1)->row();	
		if(!empty($r)){
			$r = $r->VALUE;
		}else{
			$r = 0;
		}
		
		return $r;
	}
	
	function get_sum_mpv($username=null){		
		$r = 0;
		$this->db->select_sum('MPV');
		$this->db->where('username', $username);
		$this->db->group_by('username');
		$r = $this->db->get('ppsi',1)->row();	
		if(!empty($r)){
			$r = $r->MPV;
		}else{
			$r = 0;
		}
		
		return $r;
	}
	
	function get_sum_receive($username=null){		
		$r = 0;
		$this->db->select_sum('Amount_Received');
		$this->db->where('username', $username);
		$this->db->group_by('username');
		$r = $this->db->get('ppsi',1)->row();	
		if(!empty($r)){
			$r = $r->Amount_Received;
		}else{
			$r = 0;
		}
		
		return $r;
	}
	
	function get_sum_balance($username=null){		
		$r = 0;
		$this->db->select_sum('Balance_receive');
		$this->db->where('username', $username);
		$this->db->group_by('username');
		$r = $this->db->get('ppsi',1)->row();	
		if(!empty($r)){
			$r = $r->Balance_receive;
		}else{
			$r = 0;
		}
		
		return $r;
	}
	
	function check_payout($tgl=null, $username=null){
		$cnt = 0;
		$this->db->where('date', $tgl);
		$this->db->where('jenis', 'kompayout');
		$this->db->where('username', $username);
		$this->db->from('komisi');
		$cnt = $this->db->count_all_results();	
		if($cnt > 0){
			$cnt = $cnt;
		}	
	
		return $cnt;
	}
	
	function dataupline($field=null, $username=null) {
		$dup = array();
		$this->db->select($field);
		$this->db->where('username', $username);
		$dt = $this->db->get('upline')->result_array();
		if(!empty($dt)){
			foreach($dt as $t){
				$dup[] = $t[$field];
			}
		}	
		return $dup;
	}
	
	function get_points(){
		$res = array();
		$get = $this->db->get('master_point')->result_array();
		if(!empty($get)){
			foreach($get as $g){
				$res[$g['username']][$g['jenis']] = $g['nilai'];
			}
		}
		return $res;
	}	
	
	function proses($data=array()){
		return $this->db->insert('komisi', $data);
	}
	
	function proses_point($table=null, $data=array()){
		return $this->db->insert($table, $data);
	}
	
	function myupdate($table=null, $m=null, $key=null, $data=array()){
		$this->db->where('username', $m);
		$this->db->where('jenis', $key);
		return $this->db->update($table, $data);
	}
	
	function insert($table=null, $data=array()){
		return $this->db->insert($table, $data);
	}
	
	function get_val_ppsi($username=null, $month=null){
		$res = 0;
		$sql = "SELECT sum(VALUE) as jml FROM ppsi ";
		$sql .= "WHERE MONTH(GRAB_ON) = '$month' and username = '$username' ";
		$sql .= "GROUP BY username";
		$dt = $this->db->query($sql)->row();
		$setting = $this->get_key_val();
		if(!empty($dt->jml) && $dt->jml >=$setting['jml_ppsi']){
			$res = $dt->jml;
		}
		return $res;
	}
	
	function get_key_val() {
		$out = array();
		$this->db->select('id,opsi_key,opsi_val');
		$this->db->from('tbl_setting');
		$query = $this->db->get();
		if($query->num_rows()>0){
				$result = $query->result();
				foreach($result as $value){
					$out[$value->opsi_key] = $value->opsi_val;
				}
				return $out;
		} else {
			return $out;
		}
	}
	
	
	function get_komppoolsponsor(){	
		$res = array();
		$result = array();
		$month = date('m');
		$setting = $this->get_key_val();
		$sql = "SELECT username, COUNT(*) as total FROM komisi ";
		$sql .= "WHERE MONTH(date) = '$month' AND jenis ='komppoolsponsor' ";
		$sql .= "GROUP BY `username`";
		$cnt = $this->db->query($sql)->result_array();		
		
		foreach($cnt as $c){
			if($c['total'] >= $setting['jml_ppsi']){
				
				// $res[$c['username']] = $c['total'];
				$res[] = $c['username'];
			}
		}
		
		if(!empty($res)){
			foreach ($res as $key){	
				
				$ok_ppsi = $this->get_val_ppsi($key, $month);				
				if($ok_ppsi >= $setting['jml_ppsi_points']){
					$result[] = $key;
				}
			// error_log($this->db->last_query());
			}
		}
		//error_log(serialize($result));
		return $result;
	}
	
	function get_paket($username=null){
		$this->db->select('paket');
		$this->db->where('username', $username);
		$paket = $this->db->get('member')->row();
		
		$this->db->select('matchingupline');
		$this->db->where('id_package', $paket->paket);
		$leader = $this->db->get('package')->row();		
		return $leader->matchingupline;
	}
	
	function get_lastpoint($username=null, $jns=null){
		$this->db->select('nilai');
		$this->db->where('username', $username);
		$this->db->where('jenis', $jns);
		$nilai = $this->db->get('master_point')->row();		
		if(!empty($nilai)){
			return $nilai->nilai;
		}else{
			return null;
		}
		
	}	
	
	function check_auto_convert($username=null){
		$dta = 0;
		$this->db->select('auto_convert');
		$this->db->where('username', $username);
		$dt = $this->db->get('member')->row();
		if($dt->auto_convert > 0){
			$dta = $dt->auto_convert;
		}
		return $dta;
	}
	
	
	function topup_ppsi(){
		$data = array();
		$data2 = array();
		$username = isset($_POST['nama_anggota']) ? $_POST['nama_anggota'] : '';
		$ammount = isset($_POST['ammount']) ? $_POST['ammount'] : '';
		$jns = isset($_POST['jenis']) ? $_POST['jenis'] : '';
		$last_actv = $this->get_lastpoint($username, 'actv');
		
		if($last_actv > 0){
			$nilai = $last_actv + $ammount;
		}else{
			$nilai = $ammount;
		}
		$this->db->trans_start();	
		if(!empty($last_actv)){
			$data = array(			
				'nilai'		=> $nilai		
			);
			$this->db->where('username', $username);
			$this->db->where('jenis', 'actv');
			$this->db->update('master_point', $data);
		}else{
			$data = array(
				'nilai'		=> $nilai,
				'username'	=> $username,
				'jenis'		=> $jns
			);
			$this->db->insert('master_point', $data);
		}
				
		$data2 = array(
			'username'		=> $username,
			'nilai'			=> $ammount,
			'Balance'		=> $nilai,
			'Description'	=> 'Top up PPSI sejumlah:'.$ammount,
			'jenis'			=> $jns
		);
		$this->db->insert('point', $data2);
		$chk_convert = $this->check_auto_convert($username);
		$tgl = date('Y-m-d');
		$rate = $this->get_rate($tgl);
		$rate = isset($rate->payout_rate) ? $rate->payout_rate : 0;
		if($chk_convert > 0){
			$mpv = 2 * $ammount;
			$amount_received = ($rate/100) * $ammount;
			$balance_receive = $mpv - $amount_received;
			$data_ppsi = array(
				'MDPRR'   			=> 'up to 10%',
				'username' 			=> $username, 		
				'DETAILS'			=> 'Deposit:'.$ammount,
				'MPV'				=> $mpv,
				'Amount_Received'	=> $amount_received,
				'Balance_receive'	=> $balance_receive,
				'VALUE' 			=> $ammount					
			);				
			$this->db->insert('ppsi', $data_ppsi);
			
			$bnus_upline = (2 / 100) * $ammount;
			$cash = (60 / 100) * $bnus_upline;
			$rgtr = (20 / 100) * $bnus_upline;
			$ppsi = (10 / 100) * $bnus_upline;
			$upline = $this->get_paket($username);
			$last_point = $this->get_points();
			for($i=0;$i<$upline; $i++){
				$uplines = 'upline'.$i;
				$_upline = $this->dataupline($uplines, $username);
				$_upline = isset($_upline[0]) ? $_upline[0] : 0;
				$data_payout = array(
					'username' 	=> $_upline,
					'total'	   	=> $cash,
					'jenis'	   	=> 'komleader',
					'date'		=> $tgl,
					'dari'		=> $username
				);
				$this->db->insert('komisi', $data_payout);
				
				$last['cash']= $last_point[$_upline]['cash'] > 0 ? $last_point[$_upline]['cash'] : '';
				$last['rgtr']= $last_point[$_upline]['rgtr'] > 0 ? $last_point[$_upline]['rgtr'] : '';
				$match_point = array(
					'cash'	=> $cash,
					'rgtr'	=> $rgtr
				);
				foreach($match_point as $key=>$dp){
					$data_points = array();
					$data_points = array(
						'nilai'   		=> $dp,
						'username' 		=> $_upline, 
						'Description' 	=> 'Bonus dari member', 
						'jenis' 		=> $key, 
						'Balance' 		=> $last[$key] + $dp
					);
					$this->db->insert('point', $data_points);
					$_points = array();
					$_points = array(
						'nilai'		=> $last[$key] + $dp									
					);			
					$this->db->where('jenis', $key);
					$this->db->where('username', $_upline);
					$this->db->update('master_point', $_points);	
					
					$mpv = '';
					$amount_received = '';
					$balance_receive ='';
					$mpv = 2 * $ppsi;
					$amount_received = ($rate/100) * $ppsi;
					$balance_receive = $mpv - $amount_received;
					$data_ppsi = array(
						'MDPRR'   			=> 'up to 10%',
						'username' 			=> $_upline, 		
						'DETAILS'			=> 'Deposit:'.$ppsi,
						'MPV'				=> $mpv,
						'Amount_Received'	=> $amount_received,
						'Balance_receive'	=> $balance_receive,
						'VALUE' 			=> $ppsi				
					);				
					$this->db->insert('ppsi', $data_ppsi);
					
				}
			}
		}
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			// error insert
			return FALSE;
		} else {
			$this->db->trans_complete();
			return TRUE;
		}
	}
	
	function get_bonus($username=null, $jns=null){
		
		$this->db->where('username', $username);
		if(!empty($jns)){
			$this->db->where('jenis', $jns);
		}
		return $this->db->get('komisi')->result_array();
	}	
	
	function get_daily($username=null){
		$froms = isset($_REQUEST['froms']) ? $_REQUEST['froms'] : '';
		$to = isset($_REQUEST['to']) ? $_REQUEST['to'] : '';
		
		$date = date('Y-m-d');
		$this->db->where('username', $username);
		if(!empty($froms) && !empty($to)){
			$froms = date('Y-m-d', strtotime($froms));
			$to = date('Y-m-d', strtotime($to));
			$this->db->where('date >=', $froms.' '.'00:00:00');
			$this->db->where('date <=', $to.' '.'23:59:59');
		}else{
			$this->db->where('date >=', $date.' '.'00:00:00');
			$this->db->where('date <=', $date.' '.'23:59:59');
		}
		
		$t = $this->db->get('komisi')->result_array();		
		
		return $t;
	}	
	
	function save_auto(){
		$username = $_POST['usernamee'];
		$auto = isset($_POST['autoconverts']) ? $_POST['autoconverts'] : 0;
		$this->db->trans_start();	
		$this->db->where('username', $username);
		$this->db->update('member', array('auto_convert' => $auto));
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			// error insert
			return FALSE;
		} else {
			$this->db->trans_complete();
			return TRUE;
		}
	}
	
}

/* End of file pendapatan.php */
/* Location: ./application/models/pendapatan.php */