<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Genealogy_m extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	function get_member_ajax($offset, $limit, $q='', $sort, $order) {
		$sql = "SELECT * FROM member ";
			
		$result['count'] = $this->db->query($sql)->num_rows();
		$sql .=" ORDER BY {$sort} {$order} ";
		$sql .=" LIMIT {$offset},{$limit} ";
		$result['data'] = $this->db->query($sql)->result();
		return $result;
	}
	
	function get_dataku($field=null, $username=null){
		$query = "SELECT $field from member where username = '$username'";
		$data = $this->db->query($query)->row();
		return $data;
	}
	
	function dataupline($field=null, $username=null) {
		$query = "SELECT $field from upline where username = '$username'";
		$data = $this->db->query($query)->row();
		return $data;
	}	
	
	function getData_member(){
		$dup = array();
		$dt = $this->db->get('member')->result_array();
		if(!empty($dt)){
			foreach($dt as $t){
				$dup[$t['username']]['foto'] = $t['foto'];				
			}
		}			
		return $dup;
	}
	
	function get_ki_ka(){
		$dup = array();
		$dt = $this->db->get('upline')->result_array();
		if(!empty($dt)){
			foreach($dt as $t){
				$dup[$t['username']]['kiri'] = $t['kiri'];
				$dup[$t['username']]['kanan'] = $t['kanan'];
			}
		}	
		//error_log($this->db->last_query());
		return $dup;
	}
	
	function get_last_point(){
		$dup = array();
		$dt = $this->db->get('master_point')->result_array();
		if(!empty($dt)){
			foreach($dt as $t){
				$dup[$t['username']][$t['jenis']] = $t['nilai'];				
								
			}
		}	
		//error_log($this->db->last_query());
		return $dup;
	}
}

