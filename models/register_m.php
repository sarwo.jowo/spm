<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register_m extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	function dataupline($field=null, $username=null) {
		$dup = array();
		$this->db->select($field);
		$this->db->where('username', $username);
		$dt = $this->db->get('upline')->result_array();
		if(!empty($dt)){
			foreach($dt as $t){
				$dup[] = $t[$field];
			}
		}	
		return $dup;
	}	
	
	function get_rate($date=null){
		$this->db->where('date', $date);
		$this->db->select('payout_rate');
		return $this->db->get('tbl_ppsi_rate')->row();
	}
	
	function get_dataposisi($posisi=null, $username=null){
		$this->db->select($posisi);
		$this->db->where('username', $username);		
		return $this->db->get('jaringan')->row();
	}
	
	function match($username) {
			$mt="";	
			$sql="select kiri, kanan from jaringan where username='$username'";			
			$data=$this->db->query($sql)->row();			
			if($data->kiri > $data->kanan) {
				$match = $data->kanan;

			} else {
				$match = $data->kiri;

			}

			$mt= $match;

			return $mt;
		}
		
	function matches($user=null){
		$sql="select username from komisi where jenis='kompasangan' and username='$user'";
		$data=$this->db->query($sql)->num_rows();
		return $data;
	}
	
	function chek_kompasangan($user=null){
		$sql="select * from komisi where jenis='kompasangan' and dari='$user'";
		$data=$this->db->query($sql)->num_rows();
		return $data;
	}	
	function insert($table=null){
		
		$save = 0;	
		$_upline = isset($_POST['place_under_account']) ? $_POST['place_under_account'] : 0;
		$ki = $this->dataupline("kiri", $_upline);
		$ka = $this->dataupline("kanan", $_upline);
		$levele = $this->dataupline("level", $_upline);
		$upline1 = $this->dataupline("upline0", $_upline);
		$upline2 = $this->dataupline("upline1", $_upline);
		$upline3 = $this->dataupline("upline2", $_upline);
		$upline4 = $this->dataupline("upline3", $_upline);
		$upline5 = $this->dataupline("upline4", $_upline);
		$upline6 = $this->dataupline("upline5", $_upline);
		$upline7 = $this->dataupline("upline6", $_upline);
		$upline8 = $this->dataupline("upline7", $_upline);
		$upline9 = $this->dataupline("upline8", $_upline);
		$upline10 = $this->dataupline("upline9", $_upline);
		$upline11 = $this->dataupline("upline10", $_upline);
		$upline12 = $this->dataupline("upline11", $_upline);
		$upline13 = $this->dataupline("upline12", $_upline);
		$upline14 = $this->dataupline("upline13", $_upline);
		$upline15 = $this->dataupline("upline14", $_upline);
		$upline16 = $this->dataupline("upline15", $_upline);
		$upline17 = $this->dataupline("upline16", $_upline);
		$upline18 = $this->dataupline("upline17", $_upline);
		$upline19 = $this->dataupline("upline18", $_upline);
		$upline20 = $this->dataupline("upline19", $_upline);	
		$username = isset($_POST['username']) ? $_POST['username'] : 0;		
		$password = isset($_POST['passwords']) ? $_POST['passwords'] : 0;	
		$pass = 0;
		if($password > 0){
			$pass = md5($password);	
		}			
		$email = isset($_POST['email']) ? $_POST['email'] : 0;
		$sponsore = isset($_POST['referal']) ? $_POST['referal'] : 0;		
		$posisi = isset($_POST['posisi']) ? $_POST['posisi'] : 0;
		
		
		$nama = 0;		
		$tglahir = 0;
		$sex = 0;		
		$levele = isset($levele[0]) ? $levele[0]+1 : 0;
		$dl = 0;
		$sp = 0;
		$kp = 0;
		$ms = 0;		
		$alamat = 0;
		$kota = 0;
		$propinsi = 0;		
		$phone = 0;
		$hp	= 0; 
		$bankku =0;		
		$sprp = 0;
		$rpadmin = 0; 		                                                                                                      ;
		$harga = 0;
		$oid	= 0;
		$paket = isset($_POST['paket_selected']) ? $_POST['paket_selected'] : 0;

		$tgl = date('Y-m-d');
		$rate = $this->get_rate($tgl);
		$rate = isset($rate->payout_rate) ? $rate->payout_rate : 0;
		
		$package = isset($_POST['package']) ? $_POST['package'] : 0;		
		$package = explode('_', $package);
		
		$price = $package[8];
		$komsponsor = $package[7];
		$kompasangan = $package[11];
		$percent_komsponsor = $package[9];
		$leader_matchupline = $package[10];
		$adv = $package[3];
		$gtc = $package[4];
		$ppsi = $package[5];
		$shop = $package[6];
		$rgtr = isset($_POST['user_activation_points']) ? $_POST['user_activation_points'] : 0;	
		$actv = isset($_POST['user_registrasi_points']) ? $_POST['user_registrasi_points'] : 0;	
		
		$komppoolsponsor = ((12 - $percent_komsponsor) / 100 ) * $price;
		
		$komsponsores = $komsponsor;
		
		$cash_sponsor = (($komsponsor / 100) * $price) * 0.6;  //60%
		$rgtr_sponsor = (($komsponsor / 100) * $price) * 0.2;  //20%
		$ppsi_sponsor = (($komsponsor / 100) * $price) * 0.2;  //20%
		
		
		$last_point = $this->get_points();		
		
		$last['rgtr']= isset($last_point[$sponsore]['rgtr']) ? $last_point[$sponsore]['rgtr'] : 0;
		$last['cash'] = isset($last_point[$sponsore]['cash']) ? $last_point[$sponsore]['cash'] : 0;
		
		$u_name = $this->session->userdata('u_name');
		$last_rgtr_uname = isset($last_point[$u_name]['rgtr']) ? $last_point[$sponsore]['rgtr'] : 0;
		$last_actv_uname = isset($last_point[$u_name]['actv']) ? $last_point[$sponsore]['actv'] : 0;		
		
		$bonus_sponsor = array(
			'rgtr' => $rgtr_sponsor,
			'cash' => $cash_sponsor
		);
		
		$dataa = array(
			'rsel' => $adv > 0 ? $adv : 0,
			'gtc'  => $gtc > 0 ? $gtc : 0,
			'rgtr' => $rgtr > 0 ? $rgtr : 0,
			'actv' => $actv > 0 ? $actv : 0,			
			'shop' => $shop > 0 ? $shop : 0,	
			'cash' => 0			
		);		
		$this->db->trans_start();		
			
		//update actv dan rgtr point dan master point yang mendaftarkan
		$sisa_actv_uname = $last_actv_uname - $actv;	
		$this->db->where('jenis', 'actv');
		$this->db->where('username', $u_name);
		$this->db->update('master_point', array('nilai' => $sisa_actv_uname));
		$data_history = array(
				'nilai'   		=> '-'.$actv,
				'username' 		=> $u_name, 
				'Description' 	=> 'Use to Register Member:'.$username, 
				'jenis' 		=> 'actv', 
				'Balance' 		=> $sisa_actv_uname
			);
		$this->db->insert('point', $data_points);
		$data_history = array();
		if((int)$rgtr > 0){
			$sisa_rgtr_uname = $last_rgtr_uname - $rgtr;			
			$this->db->where('jenis', 'rgtr');
			$this->db->where('username', $u_name);
			$this->db->update('master_point', array('nilai' => $sisa_rgtr_uname));
			
			$data_history = array(
				'nilai'   		=> '-'.$rgtr,
				'username' 		=> $u_name, 
				'Description' 	=> 'Use to Register Member:'.$username, 
				'jenis' 		=> 'rgtr', 
				'Balance' 		=> $sisa_rgtr_uname
			);
			$this->db->insert('point', $data_points);
			
		}
		/* end update */
			
			// if($table == 'point'){
				// $data_points = array();
				// foreach($dataa as $key=>$val){
					// $data_points = array(
						// 'nilai'   		=> $val,
						// 'username' 		=> $username, 
						// 'Description' 	=> 'Initial', 
						// 'jenis' 		=> $key, 
						// 'Balance' 		=> 0
					// );	
					// $this->db->insert($table, $data_points);							
				// }
				
				// $data_points = array();
				// foreach($dataa as $key=>$val){
					// $data_points = array(
						// 'nilai'		=> $val,
						// 'username' 	=> $username, 					
						// 'jenis' 	=> $key					
					// );	
					// $this->db->insert('master_point', $data_points);				
				// }
				
				// foreach($bonus_sponsor as $keys=>$bs){
					// $data_bonus = array();
					// $data_bonus = array(
						// 'nilai'   		=> $bs,
						// 'username' 		=> $sponsore, 
						// 'Description' 	=> 'Bonus Register Member', 
						// 'jenis' 		=> $keys, 
						// 'Balance' 		=> $last[$keys] + $bs
					// );
					// $this->db->insert($table, $data_points);
				// }
				
				// foreach($bonus_sponsor as $keys=>$bs){
					// $data_bonus = array();
					// $data_bonus = array(
						// 'nilai' => $last[$keys] + $bs				
					// );
					// $this->db->where('username', $sponsore);
					// $this->db->where('jenis', $keys);
					// $this->db->update('master_point', $data_bonus);
				// }		
			// }
		
		
		
		
		$upline = array(
			'upline0'  => $_upline,
			'upline1'  => isset($upline1[0]) ? $upline1[0] : 0,
			'upline2'  => isset($upline2[0]) ? $upline2[0] : 0,
			'upline3'  => isset($upline3[0]) ? $upline3[0] : 0,
			'upline4'  => isset($upline4[0]) ? $upline4[0] : 0,
			'upline5'  => isset($upline5[0]) ? $upline5[0] : 0,
			'upline6'  => isset($upline6[0]) ? $upline6[0] : 0,
			'upline7'  => isset($upline7[0]) ? $upline7[0] : 0, 
			'upline8'  => isset($upline8[0]) ? $upline8[0] : 0,
			'upline9'  => isset($upline9[0]) ? $upline9[0] : 0,
			'upline10' => isset($upline10[0]) ? $upline10[0] : 0,
			'upline11' => isset($upline11[0]) ? $upline11[0] : 0,
			'upline12' => isset($upline12[0]) ? $upline12[0] : 0,
			'upline13' => isset($upline13[0]) ? $upline13[0] : 0,
			'upline14' => isset($upline14[0]) ? $upline14[0] : 0,
			'upline15' => isset($upline15[0]) ? $upline15[0] : 0,
			'upline16' => isset($upline16[0]) ? $upline16[0] : 0,
			'upline17' => isset($upline17[0]) ? $upline17[0] : 0,
			'upline18' => isset($upline18[0]) ? $upline18[0] : 0,
			'upline19' => isset($upline19[0]) ? $upline19[0] : 0,
			'upline20' => isset($upline20[0]) ? $upline20[0] : 0,		
			'upline21' => 0,
			'upline22' => 0,
			'upline23' => 0,
			'upline24' => 0,
			'upline25' => 0,
			'upline26' => 0,
			'upline27' => 0,
			'upline28' => 0,
			'upline29' => 0,
			'upline30' => 0,
			'upline31' => 0,
			'upline32' => 0,
			'upline33' => 0,
			'upline34' => 0,
			'upline35' => 0,
			'upline36' => 0,
			'upline37' => 0,
			'upline38' => 0,
			'upline39' => 0,
			'upline40' => 0,			
			'username' => $username,
			'sponsor'  => $sponsore,
			'posisi'   => $posisi,		   
			'level'	   => $levele,
			'dl'	   => $dl,
			'sp'	   => $sp,
			'kp'	   => $kp,
			'ms'	   => $ms,
			'paket'	   => $paket
		);
				
		$member = array(
			'username' 	=> $username,
			'nama' 		=> $nama,
			'pass' 		=> $pass,	
			'sponsor'	=> $sponsore,
			'upline'	=> $_upline,			
			'email' 	=> $email,
			'tglahir' 	=> $tglahir,
			'kelamin' 	=> $sex,			
			'kawin' 	=> 0,			
			'alamat' 	=> $alamat,
			'kota' 		=> $kota,
			'propinsi' 	=> $propinsi,			
			'phone' 	=> $phone,
			'hp' 		=> $hp,	
			'bank' 		=> $bankku,
			'norek' 	=> 0,
			'foto' 		=> 0,
			'adminrp' 	=> $rpadmin,
			'tgl' 		=> date("d-M Y"),			
			'paket' 	=> $paket,
			'harga' 	=> $harga,
			'blokir' 	=> 0,
			'status' 	=> 0,
			'jenis' 	=> 0,
			'stockis' 	=> 0			
		);	

		$jaringan = array(
			'username'	=> $username,
			'kiri'		=> 0,
			'kanan'		=> 0,
			'level'		=> 0,
		);
		
		// $dataa = array(
			// 'kompsponsor' => $komsponsores,			
			// 'komppoolsponsor' => $komppoolsponsor			
		// );
			
		
		
		// if($table == 'komisi'){			
			// $data_komsponsor = array();
			// foreach($dataa as $key=>$val){
				// $data_komsponsor = array(
					// 'total' => $val,
					// 'jenis'	=> $key,
					// 'dari'	=> $username,
					// 'username' => $sponsore,
					// 'status' => 0,
					// 'bayar'	=> 0,
					// 'tglbayar' => 0					
				// );	
				// $this->db->insert($table, $data_komsponsor);             
			// }
			
		// }			
		
		if($table == 'upline'){
			$this->db->insert($table, $upline);
			$this->db->insert('jaringan', $jaringan);
		}
		
		if($table == 'member'){
			$this->db->insert($table, $member);
			$upline='';
			for($i=0;$i<$levele;$i++){
				$upline = '';
				$uplines = 'upline'.$i;
				$upline = $this->dataupline($uplines, $username);
				$upline =  isset($upline[0]) ? $upline[0] : 0;
				$posisi_pv = 'pv'.$posisi;
				$_posisi = $this->get_dataposisi($posisi,$upline);
				$_pv = $this->get_dataposisi($posisi_pv,$upline);
				
				$nilai_posisi = $_posisi > 0 ? $_posisi->$posisi : '';
				$nilai_pv = $_pv > 0 ? $_pv->$posisi_pv : '';
				
				$nilai_posisi = $nilai_posisi + 1;
				$nilai_pv = $nilai_posisi + $price;
				
				$data_posisi = array(
					$posisi 	=> $nilai_posisi,
					$posisi_pv	=> $nilai_pv
				);		
				
				$this->db->where('username', $upline);
				$this->db->update('jaringan', $data_posisi);
				
				$jaringan_day = array(
					'username'	=> $upline,					
					$posisi_pv	=> $nilai_pv,
					'tgl'		=> date('Y-m-d')
				);
				
				$this->db->insert('jaringan_day', $jaringan_day);
				
				$matchnow=$this->match($upline);
				$match=$this->matches($upline);
				
				$dataKomp = array(
					'username'	=> $upline,
					'jenis' => 'kompasangan',
					'dari'	=> $username,
					'total' => $kompasangan
				);
				if($matchnow == $match + 1){
					if($this->chek_kompasangan($username) < 1){
						$this->db->insert("komisi", $dataKomp);
					}					
				}
			}			
		}
		
		$_posisis = array($posisi => $username);
		$this->db->where('username', $_upline);
		$save = $this->db->update('upline', $_posisis);
		
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			// error insert
			return FALSE;
		} else {
			$this->db->trans_complete();
			return TRUE;
		}
		
		//return $save;
	}	
	
	//$this->register_m->select_field('tbl_setting', 'opsi_val','opsi_key','password');
	function select_field($table=null, $field=null, $key=null, $where=null){
		$dup = array();
		
		$this->db->select($field);	
		if(!empty($key)){
			$this->db->where($key, $where);
		}
		$dt = $this->db->get($table)->result_array();
		if(!empty($dt)){
			foreach($dt as $t){
				$dup[] = $t[$field];
			}
		}			
		return $dup;
	}	
	
	function check_ada($table=null, $field=null, $key=null, $where=null)
	{
    $this->db->where($field,$where);
    $query = $this->db->get($table);
    if ($query->num_rows() > 0){
        return true;
    }
    else{
        return false;
    }
	}
	
	function select_wheree($table=null, $field=null,$type=null,$where=null){
		$dup = array();	
		$this->db->select($field);	
		$this->db->where($type, $where);
		$dt = $this->db->get($table)->result_array();
		if(!empty($dt)){
			foreach($dt as $t){
				$dup[] = $t[$field];
			}
		}	
		return $dup[0];
	}
	
	function get_points(){
		$res = array();
		$get = $this->db->get('master_point')->result_array();
		if(!empty($get)){
			foreach($get as $g){
				$res[$g['username']][$g['jenis']] = $g['nilai'];
			}
		}
		return $res;
	}
	
	function get_sponsor(){
		$res = array();
		$get = $this->db->get('member')->result_array();
		if(!empty($get)){
			foreach($get as $g){
				$res[$g['username']] = $g['sponsor'];
			}
		}
		return $res;
	}
	
	function upgrade(){
		//error_log(serialize($_POST));	
		$username = $_POST['username_upg'];
		$package = isset($_POST['package_upg']) ? $_POST['package_upg'] : 0;		
		$package = explode('_', $package);
		$komsponsore = $package[7];
		$adv = $package[3];
		$gtc = $package[4];
		$ppsi = $package[5];
		$shop = $package[6];
		$rgtr = 0;
		$actv = 0;
		
		$last_point = $this->get_points();
		$sponsor = $this->get_sponsor();
		$sponsore = isset($sponsor[$username]) ? $sponsor[$username] : 0;
		$adv_last = isset($last_point[$username]['rsel']) ? $last_point[$username]['rsel'] : 0;
		$gtc_last = isset($last_point[$username]['gtc']) ? $last_point[$username]['gtc'] : 0;
		$rgtr_last = isset($last_point[$username]['rgtr']) ? $last_point[$username]['rgtr'] : 0;
		$actv_last = isset($last_point[$username]['actv']) ? $last_point[$username]['actv'] : 0;
		$shop_last = isset($last_point[$username]['shop']) ? $last_point[$username]['shop'] : 0;
		
		$paket = isset($_POST['paket_selected_upg']) ? $_POST['paket_selected_upg'] : 0;
		//error_log($paket);
		$input_point['rsel'] = $adv;
		$input_point['gtc'] = $gtc;
		$input_point['rgtr'] = $rgtr;
		$input_point['actv'] = $actv;
		$input_point['shop'] = $shop;
		
		$dataa = array(
			'rsel' => $adv + $adv_last,
			'gtc'  => $gtc + $gtc_last,
			'rgtr' => $rgtr + $rgtr_last,
			'actv' => $actv + $actv_last,			
			'shop' => $actv + $shop_last
		);
		$this->db->trans_start();	
		$data_points = array();
		foreach($dataa as $key=>$val){
			$this->db->where('username', $username);
			$this->db->where('jenis', $key);
			$this->db->delete('master_point');
			$data_points = array(
				'nilai'		=> $val,
				'username' 	=> $username, 					
				'jenis' 	=> $key					
			);			
			$this->db->insert('master_point', $data_points);				
		}
		
		$data_points = array();
		foreach($dataa as $key=>$val){
			$data_points = array(
				'nilai'   		=> $input_point[$key],
				'username' 		=> $username, 
				'Description' 	=> 'Initial', 
				'jenis' 		=> $key, 
				'Balance' 		=> $val
			);	
			$this->db->insert('point', $data_points);				
		}
		
		$data_komsponsor = array(
			'total'		 => $komsponsore,
			'jenis'		 => 'kompsponsor',
			'dari'		 => $username,
			'username'	 => $sponsore,
			'status'	 => 0,
			'bayar'		 => 0,
			'tglbayar'	 => 0,
		);
		
		$this->db->insert('komisi', $data_komsponsor);
		
		$this->db->where('username', $username);
		$this->db->update('member', array('paket'=>$paket));
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			// error insert
			return FALSE;
		} else {
			$this->db->trans_complete();
			return TRUE;
		}
	}
	
	function get_flashout($username=null){
		$this->db->select('paket');
		$this->db->where('username', $username);
		$paket = $this->db->get('member')->row();
		
		$this->db->select('flushout');
		$this->db->where('id_package', $paket->paket);
		$leader = $this->db->get('package')->row();		
		return $leader->flushout;
	}
	
	function get_sum_komisi($username = null){
		$date = date("Y-m-d H:i:s");
		$this->db->select_sum('komisi');
		$this->db->where('date', $date);
		$this->db->where('username', $date);
		$this->db->get('komisi')->row();
	}
}

