-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 05, 2019 at 11:14 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spm`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `tentang` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `tentang`) VALUES
(1, '<p>Direktorat Jenderal Bina Pembangunan Daerah Kementerian Dalam Negeri mengundang pemerintah daerah guna membahas Standar Pelayanan Minimal (SPM) urusan Ketentraman, Ketertiban Umum, dan Perlindungan Masyarakat (Trantibumlinmas) ke dalam dokumen perencanaan daerah\n</p>');

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `id` int(11) NOT NULL,
  `nama` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`id`, `nama`) VALUES
(1, 'Perizinan'),
(2, 'Hibah dan Bantuan Sosial'),
(3, 'Kepegawaian'),
(4, 'Pendidikan'),
(5, 'Dana Desa'),
(6, 'Pelayanan Publik'),
(7, 'Pengadaan Barang dan Jasa'),
(8, 'Kegiatan Lainnya');

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `id` int(11) NOT NULL,
  `judul` varchar(250) NOT NULL,
  `berita` longtext NOT NULL,
  `tgl` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `file` varchar(200) NOT NULL,
  `lamp` varchar(100) NOT NULL,
  `utama` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id`, `judul`, `berita`, `tgl`, `file`, `lamp`, `utama`) VALUES
(3, 'asdasd', '<p>\n asdasda</p>\n', '2019-09-15 12:13:23', 'def34-thesis---albert-ricia---km---fip.docx', '728cb-thesis---albert-ricia---km---fip.docx', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('45c7adacd0953621cb1b8ee9118c3fd2', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', 1572947921, 'a:11:{s:9:\"user_data\";s:0:\"\";s:5:\"login\";b:1;s:6:\"u_name\";s:5:\"admin\";s:6:\"deptid\";N;s:9:\"kabupaten\";N;s:8:\"propinsi\";N;s:5:\"level\";s:5:\"admin\";s:7:\"id_prov\";s:1:\"0\";s:10:\"Keterangan\";s:5:\"admin\";s:9:\"id_kabkot\";s:1:\"0\";s:8:\"dinas_id\";s:1:\"6\";}');

-- --------------------------------------------------------

--
-- Table structure for table `departement`
--

CREATE TABLE `departement` (
  `nama` varchar(150) NOT NULL,
  `deptid` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departement`
--

INSERT INTO `departement` (`nama`, `deptid`) VALUES
('Menteri Sekretaris N', 'kd01'),
('Menteri Perencanaan ', 'kd02'),
('Menteri Sekretaris N', 'kd03'),
('Menteri Perencanaan ', 'kd04'),
('Menteri Perhubungan', 'kd05');

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `title` varchar(100) NOT NULL,
  `idsb` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`id`, `filename`, `title`, `idsb`) VALUES
(2, '27ada130890b48236aaed0b97e2ea036.jpg', '', 13),
(3, '154c6d0c0fd3eade538c561b6cdc30ea.jpg', '', 13),
(4, '406313f7aa78e6ea657ea06be7953b44.jpg', '', 15),
(5, '4bbf0a0352489f22f45180e9b68a732d.jpg', '', 15),
(6, 'a494e39a5e272865d9fabbc6837acbd4.jpg', '', 16),
(8, 'a54dc1124b337b0c928ce4244194ab7b.pdf', '', 18),
(9, 'a49e5fb4d005268529ca44411dd7b1ff.jpg', '', 19),
(10, '43d140579c7282cb125e2d38cbf3fdc5.jpg', '', 20),
(11, 'a0b709c77a6e4108c9f4180bade29f2c.jpg', '', 22),
(12, '3cb8dc03fbfa9538fada14c74900b2bd.docx', '', 23),
(13, '875155c026c6e3daa94f0bd01c97135a.png', '', 24),
(14, 'b077b75397045a9abe178297f6b21679.png', '', 25),
(15, '284b40bf9ffa457bc9365beb7edffff1.png', '', 26);

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jns_akun`
--

CREATE TABLE `jns_akun` (
  `id` bigint(20) NOT NULL,
  `kd_aktiva` varchar(5) DEFAULT NULL,
  `jns_trans` varchar(50) NOT NULL,
  `akun` enum('Aktiva','Pasiva') DEFAULT NULL,
  `laba_rugi` enum('','PENDAPATAN','BIAYA') NOT NULL DEFAULT '',
  `pemasukan` enum('Y','N') DEFAULT NULL,
  `pengeluaran` enum('Y','N') DEFAULT NULL,
  `aktif` enum('Y','N') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jns_akun`
--

INSERT INTO `jns_akun` (`id`, `kd_aktiva`, `jns_trans`, `akun`, `laba_rugi`, `pemasukan`, `pengeluaran`, `aktif`) VALUES
(5, 'A4', 'Piutang Usaha', 'Aktiva', '', 'Y', 'Y', 'Y'),
(6, 'A5', 'Piutang Karyawan', 'Aktiva', '', 'N', 'Y', 'N'),
(7, 'A6', 'Pinjaman Anggota', 'Aktiva', '', NULL, NULL, 'Y'),
(8, 'A7', 'Piutang Anggota', 'Aktiva', '', 'Y', 'Y', 'N'),
(9, 'A8', 'Persediaan Barang', 'Aktiva', '', 'N', 'Y', 'Y'),
(10, 'A9', 'Biaya Dibayar Dimuka', 'Aktiva', '', 'N', 'Y', 'Y'),
(11, 'A10', 'Perlengkapan Usaha', 'Aktiva', '', 'N', 'Y', 'Y'),
(17, 'C', 'Aktiva Tetap Berwujud', 'Aktiva', '', NULL, NULL, 'Y'),
(18, 'C1', 'Peralatan Kantor', 'Aktiva', '', 'N', 'Y', 'Y'),
(19, 'C2', 'Inventaris Kendaraan', 'Aktiva', '', 'N', 'Y', 'Y'),
(20, 'C3', 'Mesin', 'Aktiva', '', 'N', 'Y', 'Y'),
(21, 'C4', 'Aktiva Tetap Lainnya', 'Aktiva', '', 'Y', 'N', 'Y'),
(26, 'E', 'Modal Pribadi', 'Aktiva', '', NULL, NULL, 'N'),
(27, 'E1', 'Prive', 'Aktiva', '', 'Y', 'Y', 'N'),
(28, 'F', 'Utang', 'Pasiva', '', NULL, NULL, 'Y'),
(29, 'F1', 'Utang Usaha', 'Pasiva', '', 'Y', 'Y', 'Y'),
(31, 'K3', 'Pengeluaran Lainnya', 'Aktiva', '', 'N', 'Y', 'N'),
(32, 'F4', 'Simpanan Sukarela', 'Pasiva', '', NULL, NULL, 'Y'),
(33, 'F5', 'Utang Pajak', 'Pasiva', '', 'Y', 'Y', 'Y'),
(36, 'H', 'Utang Jangka Panjang', 'Pasiva', '', NULL, NULL, 'Y'),
(37, 'H1', 'Utang Bank', 'Pasiva', '', 'Y', 'Y', 'Y'),
(38, 'H2', 'Obligasi', 'Pasiva', '', 'Y', 'Y', 'N'),
(39, 'I', 'Modal', 'Pasiva', '', NULL, NULL, 'Y'),
(40, 'I1', 'Simpanan Pokok', 'Pasiva', '', NULL, NULL, 'Y'),
(41, 'I2', 'Simpanan Wajib', 'Pasiva', '', NULL, NULL, 'Y'),
(42, 'I3', 'Modal Awal', 'Pasiva', '', 'Y', 'Y', 'Y'),
(43, 'I4', 'Modal Penyertaan', 'Pasiva', '', 'Y', 'Y', 'N'),
(44, 'I5', 'Modal Sumbangan', 'Pasiva', '', 'Y', 'Y', 'Y'),
(45, 'I6', 'Modal Cadangan', 'Pasiva', '', 'Y', 'Y', 'Y'),
(47, 'J', 'Pendapatan', 'Pasiva', 'PENDAPATAN', NULL, NULL, 'Y'),
(48, 'J1', 'Pembayaran Angsuran', 'Pasiva', '', NULL, NULL, 'Y'),
(49, 'J2', 'Pendapatan Lainnya', 'Pasiva', 'PENDAPATAN', 'Y', 'N', 'Y'),
(50, 'K', 'Beban', 'Aktiva', '', NULL, NULL, 'Y'),
(52, 'K2', 'Beban Gaji Karyawan', 'Aktiva', 'BIAYA', 'N', 'Y', 'Y'),
(53, 'K3', 'Biaya Listrik dan Air', 'Aktiva', 'BIAYA', 'N', 'Y', 'Y'),
(54, 'K4', 'Biaya Transportasi', 'Aktiva', 'BIAYA', 'N', 'Y', 'Y'),
(60, 'K10', 'Biaya Lainnya', 'Aktiva', 'BIAYA', 'N', 'Y', 'Y'),
(110, 'TRF', 'Transfer Antar Kas', NULL, '', NULL, NULL, 'N'),
(111, 'A11', 'Permisalan', 'Aktiva', '', 'Y', 'Y', 'Y'),
(112, 'IU034', 'Iuran Kematian', 'Aktiva', 'PENDAPATAN', 'Y', 'N', 'Y'),
(113, 'SR', 'Sewa Ruangan', 'Aktiva', 'PENDAPATAN', 'Y', 'N', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `kab`
--

CREATE TABLE `kab` (
  `id_kabkot` int(11) NOT NULL,
  `nama_kabkot` varchar(250) NOT NULL,
  `id_prov` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kab`
--

INSERT INTO `kab` (`id_kabkot`, `nama_kabkot`, `id_prov`) VALUES
(1101, 'ACEH SELATAN', 11),
(1102, 'ACEH TENGGARA', 11),
(1103, 'ACEH TIMUR', 11),
(1104, 'ACEH TENGAH', 11),
(1105, 'ACEH BARAT 2', 11),
(1106, 'ACEH BESAR', 11),
(1107, 'PIDIE', 11),
(1108, 'ACEH UTARA', 11),
(1109, 'SIMEULUE', 11),
(1110, 'ACEH SINGKIL', 11),
(1111, 'BIREUEN', 11),
(1112, 'ACEH BARAT DAYA', 11),
(1113, 'GAYO LUES', 11),
(1114, 'ACEH JAYA', 11),
(1115, 'NAGAN RAYA', 11),
(1116, 'ACEH TAMIANG', 11),
(1117, 'BENER MERIAH', 11),
(1118, 'PIDIE JAYA', 11),
(1171, 'KOTA BANDA ACEH', 11),
(1172, 'KOTA SABANG', 11),
(1173, 'KOTA LHOKSEUMAWE', 11),
(1174, 'KOTA LANGSA', 11),
(1175, 'SUBULUSSALAM', 11),
(1201, 'TAPANULI TENGAH', 12),
(1202, 'TAPANULI UTARA', 12),
(1203, 'TAPANULI SELATAN', 12),
(1204, 'NIAS', 12),
(1205, 'LANGKAT', 12),
(1206, 'KARO', 12),
(1207, 'DELI SERDANG', 12),
(1208, 'SIMALUNGUN', 12),
(1209, 'ASAHAN', 12),
(1210, 'LABUAN BATU', 12),
(1211, 'DAIRI', 12),
(1212, 'TOBA SAMOSIR', 12),
(1213, 'MANDAILING NATAL', 12),
(1214, 'NIAS SELATAN', 12),
(1215, 'PAKPAK BHARAT', 12),
(1216, 'HUMBANG HASUNDUTAN', 12),
(1217, 'SAMOSIR', 12),
(1218, 'SERDANG BEDAGAI', 12),
(1219, 'BATU BARA', 12),
(1220, 'PADANG LAWAS UTARA', 12),
(1221, 'PADANG LAWAS', 12),
(1222, 'LABUAN BATU SELATAN', 12),
(1223, 'LABUAN BATU UTARA', 12),
(1224, 'NIAS UTARA', 12),
(1225, 'NIAS BARAT', 12),
(1271, 'KOTA MEDAN', 12),
(1272, 'KOTA PEMATANG SIANTAR', 12),
(1273, 'KOTA SIBOLGA', 12),
(1274, 'KOTA TANJUNG BALAI', 12),
(1275, 'KOTA BINJAI', 12),
(1276, 'KOTA TEBING TINGGI', 12),
(1277, 'KOTA PADANG SIDEMPUAN', 12),
(1278, 'KOTA GUNUNGSITOLI', 12),
(1301, 'PESISIR SELATAN', 13),
(1302, 'SOLOK', 13),
(1303, 'SAWAHLUNTOH-SIJUNJUNG', 13),
(1304, 'TANAH DATAR', 13),
(1305, 'PADANG PARIAMAN', 13),
(1306, 'AGAM', 13),
(1307, 'LIMA PULUH KOTO', 13),
(1308, 'PASAMAN', 13),
(1309, 'KEPULAUAN MENTAWAI', 13),
(1310, 'DHARMASRAYA', 13),
(1311, 'SOLOK SELATAN', 13),
(1312, 'PASAMAN BARAT', 13),
(1371, 'KOTA PADANG', 13),
(1372, 'KOTA SOLOK', 13),
(1373, 'KOTA SAWAH LUNTO', 13),
(1374, 'KOTA PADANG PANJANG', 13),
(1375, 'KOTA BUKIT TINGGI', 13),
(1376, 'KOTA PAYAKUMBUH', 13),
(1377, 'KOTA PARIAMAN', 13),
(1401, 'KAMPAR', 14),
(1402, 'INDRAGIRI HULU', 14),
(1403, 'BENGKALIS', 14),
(1404, 'INDRAGIRI HILIR', 14),
(1405, 'PELALAWAN', 14),
(1406, 'ROKAN HULU', 14),
(1407, 'ROKAN HILIR', 14),
(1408, 'SIAK', 14),
(1409, 'KUANTAN SINGINGI', 14),
(1410, 'PULAU MERANTI', 14),
(1471, 'KOTA PEKANBARU', 14),
(1472, 'KOTA DUMAI', 14),
(1501, 'KERINCI', 15),
(1502, 'MERANGIN', 15),
(1503, 'SAROLANGUN', 15),
(1504, 'BATANG HARI', 15),
(1505, 'MUARO JAMBI', 15),
(1506, 'TANJUNG JABUNG BARAT', 15),
(1507, 'TANJUNG JABUNG TIMUR', 15),
(1508, 'BUNGO', 15),
(1509, 'TEBO', 15),
(1571, 'KOTA JAMBI', 15),
(1572, 'KOTA SUNGAI PENUH', 15),
(1601, 'OGAN KOMERING ULU', 16),
(1602, 'OGAN KOMERING ILIR', 16),
(1603, 'MUARA ENIM', 16),
(1604, 'LAHAT', 16),
(1605, 'MUSI RAWAS', 16),
(1606, 'MUSI BANYUASIN', 16),
(1607, 'BANYUASIN', 16),
(1608, 'OGAN KOMERING ULU TIMUR', 16),
(1609, 'OGAN KOMERING ULU SELATAN', 16),
(1610, 'OGAN ILIR', 16),
(1611, 'EMPAT LAWANG', 16),
(1612, 'PENUKAL ABAB LEMATANG ILIR', 16),
(1613, 'MUSI RAWAS UTARA', 16),
(1671, 'KOTA PALEMBANG', 16),
(1672, 'KOTA PAGAR ALAM', 16),
(1673, 'KOTA LUBUK LINGGAU', 16),
(1674, 'KOTA PRABUMULIH', 16),
(1701, 'BENGKULU SELATAN', 17),
(1702, 'REJANG LEBONG', 17),
(1703, 'BENGKULU UTARA', 17),
(1704, 'KAUR', 17),
(1705, 'SELUMA', 17),
(1706, 'MUKO-MUKO', 17),
(1707, 'LEBONG', 17),
(1708, 'KEPAHIANG', 17),
(1709, 'BENGKULU TENGAH', 17),
(1771, 'BENGKULU', 17),
(1801, 'LAMPUNG SELATAN', 18),
(1802, 'LAMPUNG TENGAH', 18),
(1803, 'LAMPUNG UTARA', 18),
(1804, 'LAMPUNG BARAT', 18),
(1805, 'TULANG BAWANG', 18),
(1806, 'TANGGAMUS', 18),
(1807, 'LAMPUNG TIMUR', 18),
(1808, 'WAY KANAN', 18),
(1809, 'PESAWARAN', 18),
(1810, 'PRINGSEWU', 18),
(1811, 'MESUJI', 18),
(1812, 'TULANG BAWANG BARAT', 18),
(1813, 'PESISIR BARAT', 18),
(1871, 'BANDAR LAMPUNG', 18),
(1872, 'METRO', 18),
(1901, 'BANGKA', 19),
(1902, 'BELITUNG', 19),
(1903, 'BANGKA SELATAN', 19),
(1904, 'BANGKA TENGAH', 19),
(1905, 'BANGKA BARAT', 19),
(1906, 'BELITUNG TIMUR', 19),
(1971, 'KOTA PANGKAL PINANG', 19),
(2101, 'BINTAN', 21),
(2102, 'KARIMUN', 21),
(2103, 'NATUNA', 21),
(2104, 'LINGGA', 21),
(2105, 'KEPULAUAN ANAMBAS', 21),
(2171, 'KOTA BATAM', 21),
(2172, 'KOTA TANJUNG PINANG', 21),
(3101, 'KEPULAUAN SERIBU', 31),
(3171, 'JAKARTA PUSAT', 31),
(3172, 'JAKARTA UTARA', 31),
(3173, 'JAKARTA BARAT', 31),
(3174, 'JAKARTA SELATAN', 31),
(3175, 'JAKARTA TIMUR', 31),
(3201, 'BOGOR', 32),
(3202, 'SUKABUMI', 32),
(3203, 'CIANJUR', 32),
(3204, 'BANDUNG', 32),
(3205, 'GARUT', 32),
(3206, 'TASIKMALAYA', 32),
(3207, 'CIAMIS', 32),
(3208, 'KUNINGAN', 32),
(3209, 'CIREBON', 32),
(3210, 'MAJALENGKA', 32),
(3211, 'SUMEDANG', 32),
(3212, 'INDRAMAYU', 32),
(3213, 'SUBANG', 32),
(3214, 'PURWAKARTA', 32),
(3215, 'KARAWANG', 32),
(3216, 'BEKASI', 32),
(3217, 'BANDUNG BARAT', 32),
(3218, 'PANGANDARAN', 32),
(3271, 'KOTA BOGOR', 32),
(3272, 'KOTA SUKABUMI', 32),
(3273, 'KOTA BANDUNG', 32),
(3274, 'KOTA CIREBON', 32),
(3275, 'KOTA BEKASI', 32),
(3276, 'KOTA DEPOK', 32),
(3277, 'KOTA CIMAHI', 32),
(3278, 'KOTA TASIKMALAYA', 32),
(3279, 'KOTA BANJAR', 32),
(3301, 'CILACAP', 33),
(3302, 'BANYUMAS', 33),
(3303, 'PURBALINGGA', 33),
(3304, 'BANJARNEGARA', 33),
(3305, 'KEBUMEN', 33),
(3306, 'PURWOREJO', 33),
(3307, 'WONOSOBO', 33),
(3308, 'MAGELANG', 33),
(3309, 'BOYOLALI', 33),
(3310, 'KLATEN', 33),
(3311, 'SUKOHARJO', 33),
(3312, 'WONOGIRI', 33),
(3313, 'KARANGANYAR', 33),
(3314, 'SRAGEN', 33),
(3315, 'GROBOGAN', 33),
(3316, 'BLORA', 33),
(3317, 'REMBANG', 33),
(3318, 'PATI', 33),
(3319, 'KUDUS', 33),
(3320, 'JEPARA', 33),
(3321, 'DEMAK', 33),
(3322, 'SEMARANG', 33),
(3323, 'TEMANGGUNG', 33),
(3324, 'KENDAL', 33),
(3325, 'BATANG', 33),
(3326, 'PEKALONGAN', 33),
(3327, 'PEMALANG', 33),
(3328, 'TEGAL', 33),
(3329, 'BREBES', 33),
(3371, 'KOTA MAGELANG', 33),
(3372, 'KOTA SURAKARTA', 33),
(3373, 'KOTA SALATIGA', 33),
(3374, 'KOTA SEMARANG', 33),
(3375, 'KOTA PEKALONGAN', 33),
(3376, 'KOTA TEGAL', 33),
(3401, 'KULONPROGO', 34),
(3402, 'BANTUL', 34),
(3403, 'GUNUNG KIDUL', 34),
(3404, 'SLEMAN', 34),
(3471, 'KOTA YOGYAKARTA', 34),
(3501, 'PACITAN', 35),
(3502, 'PONOROGO', 35),
(3503, 'TRENGGALEK', 35),
(3504, 'TULUNGAGUNG', 35),
(3505, 'BLITAR', 35),
(3506, 'KEDIRI', 35),
(3507, 'MALANG', 35),
(3508, 'LUMAJANG', 35),
(3509, 'JEMBER', 35),
(3510, 'BANYUWANGI', 35),
(3511, 'BONDOWOSO', 35),
(3512, 'SITUBONDO', 35),
(3513, 'PROBOLINGGO', 35),
(3514, 'PASURUAN', 35),
(3515, 'SIDOARJO', 35),
(3516, 'MOJOKERTO', 35),
(3517, 'JOMBANG', 35),
(3518, 'NGANJUK', 35),
(3519, 'MADIUN', 35),
(3520, 'MAGETAN', 35),
(3521, 'NGAWI', 35),
(3522, 'BOJONEGORO', 35),
(3523, 'TUBAN', 35),
(3524, 'LAMONGAN', 35),
(3525, 'GRESIK', 35),
(3526, 'BANGKALAN', 35),
(3527, 'SAMPANG', 35),
(3528, 'PAMEKASAN', 35),
(3529, 'SUMENEP', 35),
(3571, 'KOTA KEDIRI', 35),
(3572, 'KOTA BLITAR', 35),
(3573, 'KOTA MALANG', 35),
(3574, 'KOTA PROBOLINGGO', 35),
(3575, 'KOTA PASURUAN', 35),
(3576, 'KOTA MOJOKERTO', 35),
(3577, 'KOTA MADIUN', 35),
(3578, 'KOTA SURABAYA', 35),
(3579, 'KOTA BATU', 35),
(3601, 'PANDEGLANG', 36),
(3602, 'LEBAK', 36),
(3603, 'TANGERANG', 36),
(3604, 'SERANG', 36),
(3671, 'KOTA TANGERANG', 36),
(3672, 'KOTA CILEGON', 36),
(3673, 'KOTA SERANG', 36),
(3674, 'KOTA TANGERANG SELATAN', 36),
(5101, 'JEMBRANA', 51),
(5102, 'TABANAN', 51),
(5103, 'BADUNG', 51),
(5104, 'GIANYAR', 51),
(5105, 'KLUNGKUNG', 51),
(5106, 'BANGLI', 51),
(5107, 'KARANG ASEM', 51),
(5108, 'BULELENG', 51),
(5171, 'KOTA DENPASAR', 51),
(5201, 'LOMBOK BARAT', 52),
(5202, 'LOMBOK TENGAH ', 52),
(5203, 'LOMBOK TIMUR', 52),
(5204, 'SUMBAWA', 52),
(5205, 'DOMPU', 52),
(5206, 'BIMA', 52),
(5207, 'SUMBAWA BARAT', 52),
(5208, 'LOMBOK UTARA', 52),
(5271, 'KOTA MATARAM', 52),
(5272, 'KOTA BIMA', 52),
(5301, 'KUPANG', 53),
(5302, 'TIMOR TENGAH SELATAN', 53),
(5303, 'TIMOR TENGAH UTARA', 53),
(5304, 'BELU', 53),
(5305, 'ALOR', 53),
(5306, 'FLORES TIMUR', 53),
(5307, 'SIKA', 53),
(5308, 'ENDE', 53),
(5309, 'NGADA', 53),
(5310, 'MANGGARAI', 53),
(5311, 'SUMBA TIMUR', 53),
(5312, 'SUMBA BARAT', 53),
(5313, 'LEMBATA', 53),
(5314, 'ROTENDAO', 53),
(5315, 'MANGGARAI BARAT', 53),
(5316, 'NAGEKO', 53),
(5317, 'SUMBA TENGAH', 53),
(5318, 'SUMBA BARAT DAYA', 53),
(5319, 'MANGGARAI TIMUR', 53),
(5320, 'SABU RAIJUA', 53),
(5321, 'MALAKA', 53),
(5371, 'KOTA KUPANG', 53),
(6101, 'SAMBAS', 61),
(6102, 'PONTIANAK', 61),
(6103, 'SANGGAU', 61),
(6104, 'KETAPANG', 61),
(6105, 'SINTANG', 61),
(6106, 'KAPUAS HULU', 61),
(6107, 'BENGKAYANG', 61),
(6108, 'LANDAK', 61),
(6109, 'SEKADAU', 61),
(6110, 'MELAWI', 61),
(6111, 'KAYONG UTARA', 61),
(6112, 'KUBU RAYA', 61),
(6171, 'KOTA PONTIANAK', 61),
(6172, 'KOTA SINGKAWANG', 61),
(6201, 'KOTAWARINGIN BARAT', 62),
(6202, 'KOTAWARINGIN TIMUR', 62),
(6203, 'KAPUAS', 62),
(6204, 'BARITO SELATAN', 62),
(6205, 'BARITO UTARA', 62),
(6206, 'KATINGAN', 62),
(6207, 'SERUYAN', 62),
(6208, 'SUKAMARA', 62),
(6209, 'LAMANDAU', 62),
(6210, 'GUNUNG MAS', 62),
(6211, 'PULANG PISAU', 62),
(6212, 'MURUNG RAYA', 62),
(6213, 'BARITO TIMUR', 62),
(6271, 'KOTA PALANGKA RAYA', 62),
(6301, 'TANAH LAUT', 63),
(6302, 'BARU', 63),
(6303, 'BANJAR', 63),
(6304, 'BARITO KUALA', 63),
(6305, 'TAPIN', 63),
(6306, 'HULU SUNGAI SELATAN', 63),
(6307, 'HULU SUNGAI TENGAH', 63),
(6308, 'HULU SUNGAI UTARA', 63),
(6309, 'TABALONG', 63),
(6310, 'TANAH BUMBU', 63),
(6311, 'BALANGAN', 63),
(6371, 'KOTA BANJARMASIN', 63),
(6372, 'KOTA BANJAR BARU', 63),
(6401, 'PASER', 64),
(6402, 'KUTAI KERTANEGARA', 64),
(6403, 'BERAU', 64),
(6404, 'BULUNGAN', 64),
(6405, 'NUNUKAN', 64),
(6406, 'MALINAU', 64),
(6407, 'KUTAI BARAT', 64),
(6408, 'KUTAI TIMUR', 64),
(6409, 'PENAJAM PASER UTAMA', 64),
(6410, 'TANAH TIDUNG', 64),
(6471, 'KOTA BALIKPAPAN', 64),
(6472, 'KOTA SAMARINDA', 64),
(6473, 'KOTA TARAKAN', 64),
(6474, 'KOTA BONTANG', 64),
(6501, 'BULUNGAN', 65),
(6504, 'TANA TIDUNG', 65),
(7101, 'BOLAANG MONGONDO', 71),
(7102, 'MINAHASA', 71),
(7103, 'KEPULAUAN SANGIHE', 71),
(7104, 'KEPULAUAN TALAUD', 71),
(7105, 'MINAHASA SELATAN', 71),
(7106, 'MINAHSA UTARA', 71),
(7107, 'MINAHASA TENGGARA', 71),
(7108, 'BOLMONG UTARA', 71),
(7109, 'SITARO', 71),
(7110, 'BOLAANG MONGONDO TIMUR', 71),
(7111, 'BOLAANG MONGONDO SELATAN', 71),
(7171, 'KOTA MANADO', 71),
(7172, 'KOTA BITUNG', 71),
(7173, 'KOTA TOMOHON', 71),
(7174, 'KOTA MOBAGO', 71),
(7201, 'BANGGAI', 72),
(7202, 'POSO', 72),
(7203, 'DONGGALA', 72),
(7204, 'TOLI-TOLI', 72),
(7205, 'BUOL', 72),
(7206, 'MOROWALI', 72),
(7207, 'BANGGAI KEPULAUAN', 72),
(7208, 'PARIGI MOUTONG', 72),
(7209, 'TOJO UNA-UNA', 72),
(7210, 'SIGI', 72),
(7212, 'MOROWALI UTARA', 72),
(7271, 'KOTA PALU', 72),
(7301, 'KEPULAUAN SELAYAR', 73),
(7302, 'BULUKUMBA', 73),
(7303, 'BANTAENG', 73),
(7304, 'JENEPONTO', 73),
(7305, 'TAKALAR', 73),
(7306, 'GOWA', 73),
(7307, 'SINJAI', 73),
(7308, 'BONE', 73),
(7309, 'MAROS', 73),
(7310, 'PANGKAJENE KEPULAUAN', 73),
(7311, 'BARRU', 73),
(7312, 'SOPPENG', 73),
(7313, 'WAJO', 73),
(7314, 'SIDENRENG RAPPANG', 73),
(7315, 'PINRANG', 73),
(7316, 'ENREKANG', 73),
(7317, 'LUWU', 73),
(7318, 'TANA TORAJA', 73),
(7322, 'LUWU UTARA', 73),
(7324, 'LUWU TIMUR', 73),
(7326, 'TORAJA UTARA', 73),
(7371, 'KOTA MAKASAR', 73),
(7372, 'KOTA PARE-PARE', 73),
(7373, 'KOTA PALOPO', 73),
(7401, 'KOLAKA', 74),
(7402, 'KENDARI', 74),
(7403, 'MUNA', 74),
(7404, 'BUTON', 74),
(7405, 'KONAWE SELATAN', 74),
(7406, 'BOMBANA', 74),
(7407, 'WAKATOBI', 74),
(7408, 'KOLAKA UTARA', 74),
(7409, 'KONAWE UTARA', 74),
(7410, 'BUTON UTARA', 74),
(7411, 'KOLAKA TIMUR', 74),
(7412, 'KONAWE KEPULAUAN', 74),
(7414, 'BUTON TENGAH', 74),
(7415, 'BUTON SELATAN', 74),
(7471, 'KOTA KENDARI', 74),
(7472, 'KOTA BAU-BAU', 74),
(7501, 'GORONTALO', 75),
(7502, 'BOALEMO', 75),
(7503, 'BONE BOLANGO', 75),
(7504, 'POHUWATO', 75),
(7505, 'GORONTALO UTARA', 75),
(7571, 'KOTA GORONTALO', 75),
(7601, 'MAMUJU UTARA', 76),
(7602, 'MAMUJU', 76),
(7603, 'MAMASA', 76),
(7604, 'POLEWALI MANDAR', 76),
(7605, 'MAJENE', 76),
(7606, 'MAMUJU TENGAH', 76),
(8101, 'MALUKU TENGAH', 81),
(8102, 'MALUKU TENGGARA', 81),
(8103, 'MALUKU TENGGARA BARAT', 81),
(8104, 'BURU', 81),
(8105, 'KEPULAUAN ARU', 81),
(8106, 'SERAM BAGIAN BARAT', 81),
(8107, 'SERAM BAGIAN TIMUR', 81),
(8108, 'MALUKU BARAT DAYA', 81),
(8109, 'BURU SELATAN', 81),
(8171, 'KOTA AMBON', 81),
(8172, 'KAB. TUAL', 81),
(8201, 'HALMAHERA BARAT', 82),
(8202, 'HALMAHERA TENGAH', 82),
(8203, 'HALMAHERA UTARA', 82),
(8204, 'HALMAHERA SELATAN', 82),
(8205, 'KEPULAUAN SULA', 82),
(8206, 'HALMAHERA TIMUR', 82),
(8207, 'PULAU MOROTAI', 82),
(8208, 'KAB. P. TALIABU', 82),
(8271, 'KOTA TERNATE', 82),
(8272, 'KOTA TIDORE KEPULAUAN', 82),
(9101, 'MERAUKE', 91),
(9102, 'JAYAWIJAYA', 91),
(9103, 'JAYAPURA', 91),
(9104, 'NABIRE', 91),
(9105, 'KEPULAUAN YAPEN', 91),
(9106, 'BIAK NUMFOR', 91),
(9107, 'PUNCAK JAYA', 91),
(9108, 'PANIAI', 91),
(9109, 'MIMIKA', 91),
(9110, 'SARMI', 91),
(9111, 'KEEROM', 91),
(9112, 'PEG. BINTANG', 91),
(9113, 'YAHUKIMO', 91),
(9114, 'TOLIKARA', 91),
(9115, 'WAROPEN', 91),
(9116, 'BOVEN DIGUL', 91),
(9117, 'MAPPI', 91),
(9118, 'ASMAT', 91),
(9119, 'SUPIORI', 91),
(9120, 'MAMBERAMO RAYA', 91),
(9121, 'MAMBERAMO TENGAH', 91),
(9122, 'YALIMO', 91),
(9123, 'LANNY JAYA', 91),
(9124, 'NDUGA', 91),
(9125, 'KABUPATEN PUNCAK', 91),
(9126, 'DOGIYAI', 91),
(9127, 'INTAN JAYA', 91),
(9128, 'DEIYAI', 91),
(9171, 'KOTA JAYAPURA', 91),
(9201, 'SORONG', 92),
(9202, 'MANOKWARI', 92),
(9203, 'FAKFAK', 92),
(9204, 'SORONG SELATAN', 92),
(9205, 'RAJA AMPAT', 92),
(9206, 'TELUK BINTUNI', 92),
(9207, 'TELUK WONDAMA', 92),
(9208, 'KAIMANA', 92),
(9209, 'TAMBRAUW', 92),
(9210, 'MAYBRAT', 92),
(9211, 'MANOKWARI SELATAN', 92),
(9271, 'KOTA SORONG', 92);

-- --------------------------------------------------------

--
-- Table structure for table `project_requests`
--

CREATE TABLE `project_requests` (
  `id` int(11) NOT NULL,
  `month` varchar(100) DEFAULT NULL,
  `wordpress` int(11) DEFAULT NULL,
  `codeigniter` int(11) DEFAULT NULL,
  `highcharts` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_requests`
--

INSERT INTO `project_requests` (`id`, `month`, `wordpress`, `codeigniter`, `highcharts`) VALUES
(1, '1', 4, 5, 7),
(2, '2', 5, 2, 8),
(3, '3', 6, 3, 9),
(4, '4', 2, 6, 6),
(5, '5', 5, 7, 7),
(6, '6', 7, 1, 10),
(7, '7', 2, 2, 9),
(8, '8', 1, 6, 7),
(9, '9', 6, 6, 6),
(10, '10', 7, 4, 9),
(11, '11', 3, 6, 8),
(12, '12', 4, 3, 4),
(13, '13', 4, 5, 7),
(14, '14', 5, 2, 8),
(15, '15', 6, 3, 9),
(16, '16', 2, 6, 6),
(17, '17', 5, 7, 7),
(18, '18', 7, 1, 10),
(19, '7', 2, 2, 9),
(20, '8', 1, 6, 7),
(21, '9', 6, 6, 6),
(22, '10', 7, 4, 9),
(23, '11', 3, 6, 8),
(24, '12', 4, 3, 4),
(25, '1', 4, 5, 7),
(26, '2', 5, 2, 8),
(27, '3', 6, 3, 9),
(28, '4', 2, 6, 6),
(29, '5', 5, 7, 7),
(30, '6', 7, 1, 10),
(31, '7', 2, 2, 9),
(32, '8', 1, 6, 7),
(33, '9', 6, 6, 6),
(34, '10', 7, 4, 9),
(35, '11', 3, 6, 8),
(36, '12', 4, 3, 4),
(37, '1', 4, 5, 7),
(38, '2', 5, 2, 8),
(39, '3', 6, 3, 9),
(40, '4', 2, 6, 6),
(41, '5', 5, 7, 7),
(42, '6', 7, 1, 10),
(43, '7', 2, 2, 9);

-- --------------------------------------------------------

--
-- Table structure for table `prop`
--

CREATE TABLE `prop` (
  `id_prov` int(11) NOT NULL,
  `nama_prov` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prop`
--

INSERT INTO `prop` (`id_prov`, `nama_prov`) VALUES
(11, 'ACEH'),
(12, 'SUMATERA UTARA'),
(13, 'SUMATERA BARAT'),
(14, 'RIAU'),
(15, 'JAMBI'),
(16, 'SUMATERA SELATAN'),
(17, 'BENGKULU'),
(18, 'LAMPUNG'),
(19, 'KEPULAUAN BANGKA BELITUNG'),
(21, 'KEPULAUAN RIAU'),
(31, 'DKI JAKARTA'),
(32, 'JAWA BARAT'),
(33, 'JAWA TENGAH'),
(34, 'DAERAH ISTIMEWA YOGYAKARTA'),
(35, 'JAWA TIMUR'),
(36, 'BANTEN'),
(51, 'BALI'),
(52, 'NUSA TENGGARA BARAT'),
(53, 'NUSA TENGGARA TIMUR'),
(61, 'KALIMANTAN BARAT'),
(62, 'KALIMANTAN TENGAH'),
(63, 'KALIMANTAN SELATAN'),
(64, 'KALIMANTAN TIMUR'),
(65, 'KALIMANTAN UTARA'),
(71, 'SULAWESI UTARA'),
(72, 'SULAWESI TENGAH'),
(73, 'SULAWESI SELATAN'),
(74, 'SULAWESI TENGGARA'),
(75, 'GORONTALO'),
(76, 'SULAWESI BARAT'),
(81, 'MALUKU'),
(82, 'MALUKU UTARA'),
(91, 'PAPUA'),
(92, 'PAPUA BARAT');

-- --------------------------------------------------------

--
-- Table structure for table `subdit`
--

CREATE TABLE `subdit` (
  `id` int(11) NOT NULL,
  `nama` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subdit`
--

INSERT INTO `subdit` (`id`, `nama`) VALUES
(1, 'Perizinan'),
(2, 'Hibah dan Bantuan Sosial'),
(3, 'Kepegawaian'),
(4, 'Pendidikan'),
(5, 'Dana Desa'),
(6, 'Pelayanan Publik'),
(7, 'Pengadaan Barang dan Jasa'),
(8, 'Kegiatan Lainnya');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_anggaran`
--

CREATE TABLE `tbl_anggaran` (
  `id_tr_dinas` int(11) NOT NULL,
  `dinas` varchar(100) NOT NULL,
  `id_prov` int(11) NOT NULL,
  `id_kabkot` int(11) NOT NULL,
  `program` varchar(250) NOT NULL,
  `kegiatan` varchar(250) NOT NULL,
  `sub_kegiatan` varchar(250) NOT NULL,
  `tahun` enum('2018','2019','2020','2020','2021') NOT NULL,
  `anggaran` float NOT NULL,
  `wilayah` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_anggaran`
--

INSERT INTO `tbl_anggaran` (`id_tr_dinas`, `dinas`, `id_prov`, `id_kabkot`, `program`, `kegiatan`, `sub_kegiatan`, `tahun`, `anggaran`, `wilayah`) VALUES
(3, '3', 0, 1114, '', '', '', '2019', 0, 'Kabupaten/Kota'),
(4, '2', 51, 0, 'Kesehatan Bayi', 'Pemberian Susu gratis', 'Pemberian Susu gratis', '2018', 2000000, 'Provinsi'),
(5, '5', 11, 0, 'Pembelian ATK', 'Pembelian ATK', 'Pembelian Penghapus', '2019', 30000000, 'Provinsi'),
(6, '5', 0, 1101, '', '', '', '2020', 200000, 'Kabupaten/Kota');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dinas`
--

CREATE TABLE `tbl_dinas` (
  `id_tr_dinas` int(11) NOT NULL,
  `dinas` varchar(100) NOT NULL,
  `id_prov` int(11) NOT NULL,
  `id_kabkot` int(11) NOT NULL,
  `tahun` enum('2018','2019','2020','2020','2021') NOT NULL,
  `flag_generated` tinyint(1) NOT NULL,
  `wilayah` varchar(100) NOT NULL,
  `status_dokumen` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_dinas`
--

INSERT INTO `tbl_dinas` (`id_tr_dinas`, `dinas`, `id_prov`, `id_kabkot`, `tahun`, `flag_generated`, `wilayah`, `status_dokumen`) VALUES
(16, '1', 11, 1101, '2019', 1, 'Kabupaten/Kota', ''),
(21, '6', 11, 1101, '2019', 1, 'Kabupaten/Kota', ''),
(22, '4', 11, 1101, '2019', 1, 'Kabupaten/Kota', ''),
(26, '2', 36, 3602, '2019', 1, 'Kabupaten/Kota', ''),
(27, '2', 33, 3302, '2019', 1, 'Kabupaten/Kota', ''),
(29, '5', 51, 0, '2019', 1, 'Provinsi', ''),
(30, '5', 75, 7501, '2019', 1, 'Kabupaten/Kota', ''),
(31, '3', 11, 1102, '2019', 1, 'Kabupaten/Kota', ''),
(33, '5', 36, 0, '2019', 1, 'Provinsi', ''),
(35, '2', 36, 0, '2019', 1, 'Provinsi', ''),
(36, '2', 11, 1101, '2019', 1, 'Kabupaten/Kota', ''),
(40, '3', 11, 0, '2019', 1, 'Provinsi', ''),
(41, '5', 11, 1104, '2018', 1, 'Kabupaten/Kota', ''),
(45, '5', 11, 1101, '2019', 1, 'Kabupaten/Kota', ''),
(47, '4', 11, 0, '2019', 1, 'Provinsi', ''),
(48, '5', 11, 0, '2019', 1, 'Provinsi', ''),
(51, '6', 11, 0, '2019', 0, 'Provinsi', ''),
(52, '2', 11, 0, '2019', 0, 'Provinsi', ''),
(53, '1', 11, 0, '2019', 1, 'Provinsi', ''),
(54, '2', 13, 0, '2019', 0, 'Provinsi', ''),
(55, '5', 13, 0, '2019', 0, 'Provinsi', ''),
(56, '3', 13, 0, '2019', 0, 'Provinsi', ''),
(57, '1', 13, 0, '2019', 1, 'Provinsi', ''),
(58, '4', 13, 0, '2019', 1, 'Provinsi', ''),
(59, '2', 13, 1301, '2018', 1, 'Kabupaten/Kota', ''),
(60, '5', 13, 1301, '2019', 0, 'Kabupaten/Kota', ''),
(61, '3', 13, 1301, '2019', 0, 'Kabupaten/Kota', ''),
(62, '6', 13, 1301, '2019', 1, 'Kabupaten/Kota', ''),
(63, '1', 13, 1301, '2019', 0, 'Kabupaten/Kota', ''),
(64, '4', 13, 1301, '2019', 1, 'Kabupaten/Kota', ''),
(65, '1', 13, 1301, '2020', 0, 'Kabupaten/Kota', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dinas_dokumen`
--

CREATE TABLE `tbl_dinas_dokumen` (
  `ID` int(11) NOT NULL,
  `id_tr_dinas` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `keterangan` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_dinas_dokumen`
--

INSERT INTO `tbl_dinas_dokumen` (`ID`, `id_tr_dinas`, `filename`, `keterangan`) VALUES
(3, 11, '1f34dfb73ac03ae1b9d0095142f7c13a.pdf', 'laporan contoh j'),
(2, 9, '95a9fbce96d121e72f650e72895f9f1d.pdf', 'demo');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dinas_indikator`
--

CREATE TABLE `tbl_dinas_indikator` (
  `indikator_id` int(11) NOT NULL,
  `id_tr_dinas` int(11) NOT NULL,
  `wilayah` enum('Provinsi','Kabupaten') NOT NULL,
  `indikator_kinerja` varchar(300) NOT NULL,
  `indikator_pencapaian` varchar(300) NOT NULL,
  `subtotal` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_dinas_indikator`
--

INSERT INTO `tbl_dinas_indikator` (`indikator_id`, `id_tr_dinas`, `wilayah`, `indikator_kinerja`, `indikator_pencapaian`, `subtotal`) VALUES
(1, 6, 'Provinsi', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di dalam Panti Sosial', 'Jumlah   Warga   Negara penyandang disabilitas yang memperoleh rehabilitasi sosial diluar panti', 0),
(2, 6, 'Provinsi', 'Rehabilitasi Sosial Dasar Anak Terlantar di dalam Panti Sosial', 'Jumlah anak telantar yang memperoleh   rehabilitasi sosial diluar panti', 0),
(3, 6, 'Provinsi', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di daam Panti Sosial', 'Jumlah   Warga   Negara lanjut usia terlantar yang memperoleh rehabilitasi sosial diluar panti', 0),
(4, 6, 'Provinsi', 'Rehabilitasi Sosial Dasar Tuna Sosial Khususnya Gelandangan dan Pengemis di dalam Panti Sosial', 'Jumlah Warga Negara/ gelandangan   dan pengemis  yang memperoleh rehabilitasi sosialdasartuna sosial diluar panti', 0),
(5, 6, 'Provinsi', 'Perlindungan dan Jaminan Sosial pada Saat dan Setelah Tanggap Darurat Bencana bagi Korban Bencana daerah provinsi', 'Jumlah    Warga    Negara korban bencana kab/kota yang memperoleh perlindungan dan jaminan sosial', 0),
(6, 9, 'Provinsi', 'pelayanan kesehatan bagi penduduk terdampak krisis kesehatan akibat bencana dan/atau berpotensi bencana provinsi', 'Jumlah Warga Negara yang terdampak krisis kesehatan akibat bencana dan/atau berpotensi bencana provinsi yang mendapatkan layanan kesehatan', 0),
(7, 9, 'Provinsi', 'pelayanan kesehatan bagi penduduk pada kondisi kejadian luar biasa provinsi', 'Jumlah Warga Negara pada kondisi kejadian luar biasa provinsi yang mendapatkan layanan kesehatan', 0),
(8, 23, '', 'Pelayanan kesehatan ibu hamil', 'Jumlah Ibu Hamil yang mendapatkan layanan kesehatan', 0),
(9, 23, '', 'Pelayanan kesehatan ibu bersalin', 'Jumlah Ibu Bersalin yang mendapatkan layanan kesehatan', 0),
(10, 23, '', 'Pelayanan kesehatan bayi baru lahir', 'Jumlah Bayi Baru Lahir yang mendapatkan layanan kesehatan', 0),
(11, 23, '', 'Pelayanan kesehatan balita', 'Jumlah Balita yang mendapatkan layanan kesehatan', 0),
(12, 23, '', 'Pelayanan kesehatan pada usia pendidikan dasar', 'Jumlah Warga Negara usia pendidikan dasar yang mendapatkan layanan kesehatan', 0),
(13, 23, '', 'Pelayanan kesehatan pada usia produktif', 'Jumlah Warga Negara usia produktif yang mendapatkan layanan kesehatan', 0),
(14, 23, '', 'Pelayanan kesehatan pada usia lanjut', 'warga negara usia lanjut yang mendapatkan layanan kesehatan', 0),
(15, 23, '', 'Pelayanan kesehatan penderita hipertensi', 'Jumlah Warga Negara penderita hipertensi yang mendapatkan layanan kesehatan', 0),
(16, 23, '', 'Pelayanan kesehatan penderita diabetes melitus', 'Jumlah Warga Negara penderita diabetes mellitus yang mendapatkan layanan kesehatan', 0),
(17, 23, '', 'Pelayanan kesehatan orang dengan gangguan jiwa berat', 'Jumlah Warga Negara dengan gangguan jiwa berat yang terlayani kesehatan', 0),
(18, 23, '', 'Pelayanan kesehatan orang terduga tuberkulosis', 'Jumlah Warga Negara terduga tuberculosis yang mendapatkan layanan kesehatan', 0),
(19, 23, '', 'Pelayanan kesehatan orang dengan risiko terinfeksi virus yang melemahkan daya tahan tubuh manusia (Human Immunodeficiency Virus)', 'Jumlah Warga Negara dengan risiko terinfeksi virus yang melemahkan daya tahan tubuh manusia (Human Immunodeficiency Virus) yang mendapatkan layanan kesehatan', 0),
(20, 18, '', 'Penyediaan dan rehabilitasi rumah yang layak huni bagi korban bencana kabupaten/kota', 'Jumlah Warga Negara korban bencana yang memperoleh rumah layak huni', 0),
(21, 18, '', 'Fasilitasi penyediaan rumah yang layak huni bagi masyarakat yang terkena relokasi program Pemerintah Daerah kabupaten/kota', 'Jumlah Warga Negara yang terkena relokasi akibat program Pemerintah Daerah kabupaten/kota yang memperoleh fasilitasi penyediaan rumah yang layak huni', 0),
(22, 24, 'Provinsi', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan menengah merupakan Peserta Didik yang berusia 16 (enam belas) tahun sampai dengan 18 (delapan belas) tahun', 'menghitung jumlah anak usia 16 (enam belas) sampai dengan 18 (delapan belas) tahun pada provinsi yang bersangkutan', 0),
(23, 24, 'Provinsi', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan menengah merupakan Peserta Didik yang berusia 16 (enam belas) tahun sampai dengan 18 (delapan belas) tahun', 'menghitung jumlah anak usia 16 (enam belas) sampai dengan 18 (delapan belas) tahun yang sudah tamat atau sedang belajar di sekolah menengah atas', 0),
(24, 24, 'Provinsi', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan khusus merupakan Peserta Didik penyandang disabilitas yang berusia 4 (empat) tahun sampai dengan 18 (delapan  belas) tahun', 'menghitung jumlah anak usia 4 (empat) sampai dengan 18 (delapan belas) tahun pada provinsi yang bersangkutan', 0),
(25, 24, 'Provinsi', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan khusus merupakan Peserta Didik penyandang disabilitas yang berusia 4 (empat) tahun sampai dengan 18 (delapan  belas) tahun', 'menghitung jumlah anak usia 4 (empat) sampai dengan 18 (delapan belas) tahun yang sudah tamat atau sedang belajar di pendidikan khusus', 0),
(26, 25, 'Provinsi', 'Pemenuhan kebutuhan air minum curah lintas kabupaten/kota', 'Jumlah Warga Negara yang memperoleh kebutuhan air minum curah lintas kabupaten/kota', 0),
(27, 25, 'Provinsi', 'Penyediaan pelayanan pengolahan air limbah domestik regional lintas kabupaten/ kota', 'Jumlah Warga Negara yang memperoleh layanan pengolahan air limbah domestik regional lintas kabupaten/kota', 0),
(28, 12, 'Provinsi', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di dalam Panti Sosial', 'Jumlah   Warga   Negara penyandang disabilitas yang memperoleh rehabilitasi sosial diluar panti', 0),
(29, 12, 'Provinsi', 'Rehabilitasi Sosial Dasar Anak Terlantar di dalam Panti Sosial', 'Jumlah anak telantar yang memperoleh   rehabilitasi sosial diluar panti', 0),
(30, 12, 'Provinsi', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di daam Panti Sosial', 'Jumlah   Warga   Negara lanjut usia terlantar yang memperoleh rehabilitasi sosial diluar panti', 0),
(31, 12, 'Provinsi', 'Rehabilitasi Sosial Dasar Tuna Sosial Khususnya Gelandangan dan Pengemis di dalam Panti Sosial', 'Jumlah Warga Negara/ gelandangan   dan pengemis  yang memperoleh rehabilitasi sosialdasartuna sosial diluar panti', 0),
(32, 12, 'Provinsi', 'Perlindungan dan Jaminan Sosial pada Saat dan Setelah Tanggap Darurat Bencana bagi Korban Bencana daerah provinsi', 'Jumlah    Warga    Negara korban bencana kab/kota yang memperoleh perlindungan dan jaminan sosial', 0),
(33, 15, '', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan anak usia dini merupakan Peserta Didik yang berusia 5 (lima) tahun sampai dengan 6 (enam) tahun', 'menghitung jumlah anak usia 5 (lima) sampai dengan 6 (enam) tahun pada kabupaten/kota yang bersangkutan', 0),
(34, 15, '', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan anak usia dini merupakan Peserta Didik yang berusia 5 (lima) tahun sampai dengan 6 (enam) tahun', 'menghitung jumlah anak usia 5 (lima) sampai dengan 6 (enam) tahun yang sudah tamat atau sedang belajar di satuan pendidikan anak usia dini', 0),
(35, 15, '', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan dasar merupakan Peserta Didik yang berusia 7 (tujuh) tahun sampai dengan 15 (lima belas) tahun', 'menghitung jumlah anak usia 7 (tujuh) sampai dengan 12 (dua belas) tahun pada kabupaten/kota yang bersangkutan', 0),
(36, 15, '', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan dasar merupakan Peserta Didik yang berusia 7 (tujuh) tahun sampai dengan 15 (lima belas) tahun', 'menghitung jumlah anak usia 7 (tujuh) sampai dengan 12 (dua belas) tahun yang sudah tamat atau sedang belajar di sekolah dasar', 0),
(37, 15, '', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan kesetaraan merupakan Peserta Didik yang berusia 7 (tujuh) tahun sampai dengan 18 (delapan belas) tahun', 'menghitung jumlah anak usia 7 (tujuh) sampai dengan 18 (delapan belas) tahun pada kabupaten/kota yang bersangkutan', 0),
(38, 15, '', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan kesetaraan merupakan Peserta Didik yang berusia 7 (tujuh) tahun sampai dengan 18 (delapan belas) tahun', 'menghitung jumlah anak usia 7 (tujuh) sampai dengan 18 (delapan belas) tahun yang sudah tamat atau sedang belajar di pendidikan kesetaraan', 0),
(39, 28, 'Provinsi', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di dalam Panti Sosial', 'Jumlah   Warga   Negara penyandang disabilitas yang memperoleh rehabilitasi sosial diluar panti', 0),
(40, 28, 'Provinsi', 'Rehabilitasi Sosial Dasar Anak Terlantar di dalam Panti Sosial', 'Jumlah anak telantar yang memperoleh   rehabilitasi sosial diluar panti', 0),
(41, 28, 'Provinsi', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di daam Panti Sosial', 'Jumlah   Warga   Negara lanjut usia terlantar yang memperoleh rehabilitasi sosial diluar panti', 0),
(42, 28, 'Provinsi', 'Rehabilitasi Sosial Dasar Tuna Sosial Khususnya Gelandangan dan Pengemis di dalam Panti Sosial', 'Jumlah Warga Negara/ gelandangan   dan pengemis  yang memperoleh rehabilitasi sosialdasartuna sosial diluar panti', 0),
(43, 28, 'Provinsi', 'Perlindungan dan Jaminan Sosial pada Saat dan Setelah Tanggap Darurat Bencana bagi Korban Bencana daerah provinsi', 'Jumlah    Warga    Negara korban bencana kab/kota yang memperoleh perlindungan dan jaminan sosial', 0),
(44, 17, 'Provinsi', 'Penyediaan dan rehabilitasi rumah yang layak huni bagi korban bencana provinsi', 'Jumlah Warga Negara korban bencana yang memperoleh rumah layak huni', 0),
(45, 17, 'Provinsi', 'Fasilitasi penyediaan rumah yang layak huni bagi masyarakat yang terkena relokasi program Pemerintah Daerah provinsi', 'Jumlah Warga Negara yang terkena relokasi akibat program Pemerintah Daerah provinsi yang memperoleh fasilitasi penyediaan rumah yang layak huni', 0),
(46, 29, 'Provinsi', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan menengah merupakan Peserta Didik yang berusia 16 (enam belas) tahun sampai dengan 18 (delapan belas) tahun', 'menghitung jumlah anak usia 16 (enam belas) sampai dengan 18 (delapan belas) tahun pada provinsi yang bersangkutan', 0),
(47, 29, 'Provinsi', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan khusus merupakan Peserta Didik penyandang disabilitas yang berusia 4 (empat) tahun sampai dengan 18 (delapan  belas) tahun', 'menghitung jumlah anak usia 4 (empat) sampai dengan 18 (delapan belas) tahun pada provinsi yang bersangkutan', 0),
(48, 20, 'Provinsi', 'Pelayanan ketentraman dan ketertiban Umum Provinsi', 'Jumlah Warga Negara yang memperoleh layanan akibat dari penegakan hukum perda dan perkada di Provinsi', 0),
(49, 32, 'Provinsi', 'Pelayanan ketentraman dan ketertiban Umum Provinsi', 'Jumlah Warga Negara yang memperoleh layanan akibat dari penegakan hukum perda dan perkada di Provinsi', 0),
(50, 22, '', 'Pelayanan ketentraman dan ketertiban Umum', 'Jumlah Warga Negara yang memperoleh layanan akibat dari penegakan hukum Perda dan perkada', 0),
(51, 22, '', 'Pelayanan informasi rawan bencana', 'Jumlah Warga Negara yang memperoleh layanan informasi rawan bencana', 0),
(52, 22, '', 'Pelayanan pencegahan dan kesiapsiagaan terhadap bencana', 'Jumlah Warga Negara yang memperoleh layanan pencegahan dan kesiapsiagaan terhadap bencana', 0),
(53, 22, '', 'Pelayanan penyelamatan dan evakuasi korban bencana', 'Jumlah Warga Negara yang memperoleh layanan penyelamatan dan evakuasi korban bencana', 0),
(54, 26, '', 'Pelayanan kesehatan ibu hamil', 'Jumlah Ibu Hamil yang mendapatkan layanan kesehatan', 0),
(55, 26, '', 'Pelayanan kesehatan ibu bersalin', 'Jumlah Ibu Bersalin yang mendapatkan layanan kesehatan', 0),
(56, 26, '', 'Pelayanan kesehatan bayi baru lahir', 'Jumlah Bayi Baru Lahir yang mendapatkan layanan kesehatan', 0),
(57, 26, '', 'Pelayanan kesehatan balita', 'Jumlah Balita yang mendapatkan layanan kesehatan', 0),
(58, 26, '', 'Pelayanan kesehatan pada usia pendidikan dasar', 'Jumlah Warga Negara usia pendidikan dasar yang mendapatkan layanan kesehatan', 0),
(59, 26, '', 'Pelayanan kesehatan pada usia produktif', 'Jumlah Warga Negara usia produktif yang mendapatkan layanan kesehatan', 0),
(60, 26, '', 'Pelayanan kesehatan pada usia lanjut', 'warga negara usia lanjut yang mendapatkan layanan kesehatan', 0),
(61, 26, '', 'Pelayanan kesehatan penderita hipertensi', 'Jumlah Warga Negara penderita hipertensi yang mendapatkan layanan kesehatan', 0),
(62, 26, '', 'Pelayanan kesehatan penderita diabetes melitus', 'Jumlah Warga Negara penderita diabetes mellitus yang mendapatkan layanan kesehatan', 0),
(63, 26, '', 'Pelayanan kesehatan orang dengan gangguan jiwa berat', 'Jumlah Warga Negara dengan gangguan jiwa berat yang terlayani kesehatan', 0),
(64, 26, '', 'Pelayanan kesehatan orang terduga tuberkulosis', 'Jumlah Warga Negara terduga tuberculosis yang mendapatkan layanan kesehatan', 0),
(65, 26, '', 'Pelayanan kesehatan orang dengan risiko terinfeksi virus yang melemahkan daya tahan tubuh manusia (Human Immunodeficiency Virus)', 'Jumlah Warga Negara dengan risiko terinfeksi virus yang melemahkan daya tahan tubuh manusia (Human Immunodeficiency Virus) yang mendapatkan layanan kesehatan', 0),
(66, 11, '', 'Pelayanan kesehatan ibu hamil', 'Jumlah Ibu Hamil yang mendapatkan layanan kesehatan', 0),
(67, 11, '', 'Pelayanan kesehatan ibu bersalin', 'Jumlah Ibu Bersalin yang mendapatkan layanan kesehatan', 0),
(68, 11, '', 'Pelayanan kesehatan bayi baru lahir', 'Jumlah Bayi Baru Lahir yang mendapatkan layanan kesehatan', 0),
(69, 11, '', 'Pelayanan kesehatan balita', 'Jumlah Balita yang mendapatkan layanan kesehatan', 0),
(70, 11, '', 'Pelayanan kesehatan pada usia pendidikan dasar', 'Jumlah Warga Negara usia pendidikan dasar yang mendapatkan layanan kesehatan', 0),
(71, 11, '', 'Pelayanan kesehatan pada usia produktif', 'Jumlah Warga Negara usia produktif yang mendapatkan layanan kesehatan', 0),
(72, 11, '', 'Pelayanan kesehatan pada usia lanjut', 'warga negara usia lanjut yang mendapatkan layanan kesehatan', 0),
(73, 11, '', 'Pelayanan kesehatan penderita hipertensi', 'Jumlah Warga Negara penderita hipertensi yang mendapatkan layanan kesehatan', 0),
(74, 11, '', 'Pelayanan kesehatan penderita diabetes melitus', 'Jumlah Warga Negara penderita diabetes mellitus yang mendapatkan layanan kesehatan', 0),
(75, 11, '', 'Pelayanan kesehatan orang dengan gangguan jiwa berat', 'Jumlah Warga Negara dengan gangguan jiwa berat yang terlayani kesehatan', 0),
(76, 11, '', 'Pelayanan kesehatan orang terduga tuberkulosis', 'Jumlah Warga Negara terduga tuberculosis yang mendapatkan layanan kesehatan', 0),
(77, 11, '', 'Pelayanan kesehatan orang dengan risiko terinfeksi virus yang melemahkan daya tahan tubuh manusia (Human Immunodeficiency Virus)', 'Jumlah Warga Negara dengan risiko terinfeksi virus yang melemahkan daya tahan tubuh manusia (Human Immunodeficiency Virus) yang mendapatkan layanan kesehatan', 0),
(78, 16, '', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di Luar Panti Sosial', 'Jumlah Warga Negara penyandang disabilitas yang memperoleh rehabilitasi sosial diluar panti', 0),
(79, 16, '', 'Rehabilitasi Sosial Dasar Anak Terlantar di Luar Panti Sosial', 'Jumlah anak telantar yang memperoleh rehabilitasi sosial diluar panti', 0),
(80, 16, '', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di Luar Panti Sosial', 'Jumlah Warga Negara lanjut usia terlantar yang memperoleh rehabilitasi sosial diluar panti', 0),
(81, 16, '', 'Rehabilitasi Sosial Dasar Tuna Sosial Khususnya Gelandangan dan Pengemis di Luar Panti Sosial', 'Jumlah Warga Negara/ gelandangan dan pengemis yang memperoleh rehabilitasi sosial dasar tuna sosial diluar panti', 0),
(82, 16, '', 'Perlindungan dan Jaminan Sosial pada Saat dan Setelah Tanggap Darurat Bencana bagi Korban Bencana daerah Kabupaten/Kota', 'Jumlah Warga Negara korban bencana kab/kota yang memperoleh perlindungan dan jaminan sosial', 0),
(83, 30, '', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan anak usia dini merupakan Peserta Didik yang berusia 5 (lima) tahun sampai dengan 6 (enam) tahun', 'menghitung jumlah anak usia 5 (lima) sampai dengan 6 (enam) tahun pada kabupaten/kota yang bersangkutan', 0),
(84, 30, '', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan dasar merupakan Peserta Didik yang berusia 7 (tujuh) tahun sampai dengan 15 (lima belas) tahun', 'menghitung jumlah anak usia 7 (tujuh) sampai dengan 12 (dua belas) tahun pada kabupaten/kota yang bersangkutan', 0),
(85, 30, '', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan kesetaraan merupakan Peserta Didik yang berusia 7 (tujuh) tahun sampai dengan 18 (delapan belas) tahun', 'menghitung jumlah anak usia 7 (tujuh) sampai dengan 18 (delapan belas) tahun pada kabupaten/kota yang bersangkutan', 0),
(86, 27, '', 'Pelayanan kesehatan ibu hamil', 'Jumlah Ibu Hamil yang mendapatkan layanan kesehatan', 0),
(87, 27, '', 'Pelayanan kesehatan ibu bersalin', 'Jumlah Ibu Bersalin yang mendapatkan layanan kesehatan', 0),
(88, 27, '', 'Pelayanan kesehatan bayi baru lahir', 'Jumlah Bayi Baru Lahir yang mendapatkan layanan kesehatan', 0),
(89, 27, '', 'Pelayanan kesehatan balita', 'Jumlah Balita yang mendapatkan layanan kesehatan', 0),
(90, 27, '', 'Pelayanan kesehatan pada usia pendidikan dasar', 'Jumlah Warga Negara usia pendidikan dasar yang mendapatkan layanan kesehatan', 0),
(91, 27, '', 'Pelayanan kesehatan pada usia produktif', 'Jumlah Warga Negara usia produktif yang mendapatkan layanan kesehatan', 0),
(92, 27, '', 'Pelayanan kesehatan pada usia lanjut', 'warga negara usia lanjut yang mendapatkan layanan kesehatan', 0),
(93, 27, '', 'Pelayanan kesehatan penderita hipertensi', 'Jumlah Warga Negara penderita hipertensi yang mendapatkan layanan kesehatan', 0),
(94, 27, '', 'Pelayanan kesehatan penderita diabetes melitus', 'Jumlah Warga Negara penderita diabetes mellitus yang mendapatkan layanan kesehatan', 0),
(95, 27, '', 'Pelayanan kesehatan orang dengan gangguan jiwa berat', 'Jumlah Warga Negara dengan gangguan jiwa berat yang terlayani kesehatan', 0),
(96, 27, '', 'Pelayanan kesehatan orang terduga tuberkulosis', 'Jumlah Warga Negara terduga tuberculosis yang mendapatkan layanan kesehatan', 0),
(97, 27, '', 'Pelayanan kesehatan orang dengan risiko terinfeksi virus yang melemahkan daya tahan tubuh manusia (Human Immunodeficiency Virus)', 'Jumlah Warga Negara dengan risiko terinfeksi virus yang melemahkan daya tahan tubuh manusia (Human Immunodeficiency Virus) yang mendapatkan layanan kesehatan', 0),
(98, 21, '', 'Penyediaan Kebutuhan pokok air minum sehari-hari', 'Jumlah Warga Negara yang memperoleh kebutuhan pokok air minum sehari-hari', 0),
(99, 21, '', 'Penyediaan Pelayanan Pengolahan air limbah domestik', 'Jumlah Warga Negara yang memperoleh layanan pengolahan air limbah domestik', 0),
(100, 31, '', 'Penyediaan dan rehabilitasi rumah yang layak huni bagi korban bencana kabupaten/kota', 'Jumlah Warga Negara korban bencana yang memperoleh rumah layak huni', 0),
(101, 31, '', 'Fasilitasi penyediaan rumah yang layak huni bagi masyarakat yang terkena relokasi program Pemerintah Daerah kabupaten/kota', 'Jumlah Warga Negara yang terkena relokasi akibat program Pemerintah Daerah kabupaten/kota yang memperoleh fasilitasi penyediaan rumah yang layak huni', 0),
(102, 34, 'Provinsi', 'pelayanan kesehatan bagi penduduk terdampak krisis kesehatan akibat bencana dan/atau berpotensi bencana provinsi', 'Jumlah Warga Negara yang terdampak krisis kesehatan akibat bencana dan/atau berpotensi bencana provinsi yang mendapatkan layanan kesehatan', 0),
(103, 34, 'Provinsi', 'pelayanan kesehatan bagi penduduk pada kondisi kejadian luar biasa provinsi', 'Jumlah Warga Negara pada kondisi kejadian luar biasa provinsi yang mendapatkan layanan kesehatan', 0),
(104, 35, 'Provinsi', 'pelayanan kesehatan bagi penduduk terdampak krisis kesehatan akibat bencana dan/atau berpotensi bencana provinsi', 'Jumlah Warga Negara yang terdampak krisis kesehatan akibat bencana dan/atau berpotensi bencana provinsi yang mendapatkan layanan kesehatan', 0),
(105, 35, 'Provinsi', 'pelayanan kesehatan bagi penduduk pada kondisi kejadian luar biasa provinsi', 'Jumlah Warga Negara pada kondisi kejadian luar biasa provinsi yang mendapatkan layanan kesehatan', 0),
(106, 47, 'Provinsi', 'Pelayanan ketentraman dan ketertiban Umum Provinsi', 'Jumlah Warga Negara yang memperoleh layanan akibat dari penegakan hukum perda dan perkada di Provinsi', 0),
(107, 38, 'Provinsi', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan menengah merupakan Peserta Didik yang berusia 16 (enam belas) tahun sampai dengan 18 (delapan belas) tahun', 'menghitung jumlah anak usia 16 (enam belas) sampai dengan 18 (delapan belas) tahun pada provinsi yang bersangkutan', 0),
(108, 38, 'Provinsi', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan khusus merupakan Peserta Didik penyandang disabilitas yang berusia 4 (empat) tahun sampai dengan 18 (delapan  belas) tahun', 'menghitung jumlah anak usia 4 (empat) sampai dengan 18 (delapan belas) tahun pada provinsi yang bersangkutan', 0),
(109, 45, '', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan anak usia dini merupakan Peserta Didik yang berusia 5 (lima) tahun sampai dengan 6 (enam) tahun', 'menghitung jumlah anak usia 5 (lima) sampai dengan 6 (enam) tahun pada kabupaten/kota yang bersangkutan', 0),
(110, 45, '', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan dasar merupakan Peserta Didik yang berusia 7 (tujuh) tahun sampai dengan 15 (lima belas) tahun', 'menghitung jumlah anak usia 7 (tujuh) sampai dengan 12 (dua belas) tahun pada kabupaten/kota yang bersangkutan', 0),
(111, 45, '', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan kesetaraan merupakan Peserta Didik yang berusia 7 (tujuh) tahun sampai dengan 18 (delapan belas) tahun', 'menghitung jumlah anak usia 7 (tujuh) sampai dengan 18 (delapan belas) tahun pada kabupaten/kota yang bersangkutan', 0),
(112, 36, '', 'Pelayanan kesehatan ibu hamil', 'Jumlah Ibu Hamil yang mendapatkan layanan kesehatan', 0),
(113, 36, '', 'Pelayanan kesehatan ibu bersalin', 'Jumlah Ibu Bersalin yang mendapatkan layanan kesehatan', 0),
(114, 36, '', 'Pelayanan kesehatan bayi baru lahir', 'Jumlah Bayi Baru Lahir yang mendapatkan layanan kesehatan', 0),
(115, 36, '', 'Pelayanan kesehatan balita', 'Jumlah Balita yang mendapatkan layanan kesehatan', 0),
(116, 36, '', 'Pelayanan kesehatan pada usia pendidikan dasar', 'Jumlah Warga Negara usia pendidikan dasar yang mendapatkan layanan kesehatan', 0),
(117, 36, '', 'Pelayanan kesehatan pada usia produktif', 'Jumlah Warga Negara usia produktif yang mendapatkan layanan kesehatan', 0),
(118, 36, '', 'Pelayanan kesehatan pada usia lanjut', 'warga negara usia lanjut yang mendapatkan layanan kesehatan', 0),
(119, 36, '', 'Pelayanan kesehatan penderita hipertensi', 'Jumlah Warga Negara penderita hipertensi yang mendapatkan layanan kesehatan', 0),
(120, 36, '', 'Pelayanan kesehatan penderita diabetes melitus(DM)', 'Jumlah Warga Negara penderita diabetes mellitus yang mendapatkan layanan kesehatan', 0),
(121, 36, '', 'Pelayanan Kesehatan Orang Dengan Ganguan Jiwa (ODGJ) Berat ', 'Jumlah Warga Negara dengan gangguan jiwa berat yang terlayani kesehatan', 0),
(122, 36, '', 'Pelayanan Kesehatan Orang Terduga Tuberkulosis', 'Jumlah Warga Negara terduga tuberculosis yang mendapatkan layanan kesehatan', 0),
(123, 36, '', 'Pelayanan kesehatan orang dengan risiko terinfeksi virus yang melemahkan daya tahan tubuh manusia (Human Immunodeficiency Virus)', 'Jumlah Warga Negara dengan risiko terinfeksi virus yang melemahkan daya tahan tubuh manusia (Human Immunodeficiency Virus) yang mendapatkan layanan kesehatan', 0),
(124, 53, 'Provinsi', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di dalam Panti Sosial', 'Jumlah   Warga   Negara penyandang disabilitas yang memperoleh rehabilitasi sosial diluar panti', 0),
(125, 53, 'Provinsi', 'Rehabilitasi Sosial Dasar Anak Terlantar di dalam Panti Sosial', 'Jumlah anak telantar yang memperoleh   rehabilitasi sosial diluar panti', 0),
(126, 53, 'Provinsi', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di daam Panti Sosial', 'Jumlah   Warga   Negara lanjut usia terlantar yang memperoleh rehabilitasi sosial diluar panti', 0),
(127, 53, 'Provinsi', 'Rehabilitasi Sosial Dasar Tuna Sosial Khususnya Gelandangan dan Pengemis di dalam Panti Sosial', 'Jumlah Warga Negara/ gelandangan   dan pengemis  yang memperoleh rehabilitasi sosialdasartuna sosial diluar panti', 0),
(128, 53, 'Provinsi', 'Perlindungan dan Jaminan Sosial pada Saat dan Setelah Tanggap Darurat Bencana bagi Korban Bencana daerah provinsi', 'Jumlah    Warga    Negara korban bencana kab/kota yang memperoleh perlindungan dan jaminan sosial', 0),
(129, 40, 'Provinsi', 'Penyediaan dan rehabilitasi rumah yang layak huni bagi korban bencana provinsi', 'Jumlah Warga Negara korban bencana yang memperoleh rumah layak huni', 0),
(130, 40, 'Provinsi', 'Fasilitasi penyediaan rumah yang layak huni bagi masyarakat yang terkena relokasi program Pemerintah Daerah provinsi', 'Jumlah Warga Negara yang terkena relokasi akibat program Pemerintah Daerah provinsi yang memperoleh fasilitasi penyediaan rumah yang layak huni', 0),
(131, 48, 'Provinsi', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan menengah merupakan Peserta Didik yang berusia 16 (enam belas) tahun sampai dengan 18 (delapan belas) tahun', 'menghitung jumlah anak usia 16 (enam belas) sampai dengan 18 (delapan belas) tahun pada provinsi yang bersangkutan', 0),
(132, 48, 'Provinsi', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan khusus merupakan Peserta Didik penyandang disabilitas yang berusia 4 (empat) tahun sampai dengan 18 (delapan  belas) tahun', 'menghitung jumlah anak usia 4 (empat) sampai dengan 18 (delapan belas) tahun pada provinsi yang bersangkutan', 0),
(133, 41, '', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan anak usia dini merupakan Peserta Didik yang berusia 5 (lima) tahun sampai dengan 6 (enam) tahun', 'menghitung jumlah anak usia 5 (lima) sampai dengan 6 (enam) tahun pada kabupaten/kota yang bersangkutan', 0),
(134, 41, '', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan dasar merupakan Peserta Didik yang berusia 7 (tujuh) tahun sampai dengan 15 (lima belas) tahun', 'menghitung jumlah anak usia 7 (tujuh) sampai dengan 12 (dua belas) tahun pada kabupaten/kota yang bersangkutan', 0),
(135, 41, '', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan kesetaraan merupakan Peserta Didik yang berusia 7 (tujuh) tahun sampai dengan 18 (delapan belas) tahun', 'menghitung jumlah anak usia 7 (tujuh) sampai dengan 18 (delapan belas) tahun pada kabupaten/kota yang bersangkutan', 0),
(136, 58, 'Provinsi', 'Pelayanan ketentraman dan ketertiban Umum Provinsi', 'Jumlah Warga Negara yang memperoleh layanan akibat dari penegakan hukum perda dan perkada di Provinsi', 0),
(137, 58, 'Provinsi', 'Pelayanan ketentraman dan ketertiban Umum Provinsi', 'Jumlah Warga Negara yang memperoleh layanan akibat dari penegakan hukum perda dan perkada di Provinsi', 0),
(138, 57, 'Provinsi', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di dalam Panti Sosial', 'Jumlah   Warga   Negara penyandang disabilitas yang memperoleh rehabilitasi sosial diluar panti', 0),
(139, 57, 'Provinsi', 'Rehabilitasi Sosial Dasar Anak Terlantar di dalam Panti Sosial', 'Jumlah anak telantar yang memperoleh   rehabilitasi sosial diluar panti', 0),
(140, 57, 'Provinsi', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di daam Panti Sosial', 'Jumlah   Warga   Negara lanjut usia terlantar yang memperoleh rehabilitasi sosial diluar panti', 0),
(141, 57, 'Provinsi', 'Rehabilitasi Sosial Dasar Tuna Sosial Khususnya Gelandangan dan Pengemis di dalam Panti Sosial', 'Jumlah Warga Negara/ gelandangan   dan pengemis  yang memperoleh rehabilitasi sosialdasartuna sosial diluar panti', 0),
(142, 57, 'Provinsi', 'Perlindungan dan Jaminan Sosial pada Saat dan Setelah Tanggap Darurat Bencana bagi Korban Bencana daerah provinsi', 'Jumlah    Warga    Negara korban bencana kab/kota yang memperoleh perlindungan dan jaminan sosial', 0),
(143, 57, 'Provinsi', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di dalam Panti Sosial', 'Jumlah   Warga   Negara penyandang disabilitas yang memperoleh rehabilitasi sosial diluar panti', 0),
(144, 57, 'Provinsi', 'Rehabilitasi Sosial Dasar Anak Terlantar di dalam Panti Sosial', 'Jumlah anak telantar yang memperoleh   rehabilitasi sosial diluar panti', 0),
(145, 57, 'Provinsi', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di daam Panti Sosial', 'Jumlah   Warga   Negara lanjut usia terlantar yang memperoleh rehabilitasi sosial diluar panti', 0),
(146, 57, 'Provinsi', 'Rehabilitasi Sosial Dasar Tuna Sosial Khususnya Gelandangan dan Pengemis di dalam Panti Sosial', 'Jumlah Warga Negara/ gelandangan   dan pengemis  yang memperoleh rehabilitasi sosialdasartuna sosial diluar panti', 0),
(147, 57, 'Provinsi', 'Perlindungan dan Jaminan Sosial pada Saat dan Setelah Tanggap Darurat Bencana bagi Korban Bencana daerah provinsi', 'Jumlah    Warga    Negara korban bencana kab/kota yang memperoleh perlindungan dan jaminan sosial', 0),
(148, 64, '', 'Pelayanan ketentraman dan ketertiban Umum', 'Jumlah Warga Negara yang memperoleh layanan akibat dari penegakan hukum Perda dan perkada', 0),
(149, 64, '', 'Pelayanan informasi rawan bencana', 'Jumlah Warga Negara yang memperoleh layanan informasi rawan bencana', 0),
(150, 64, '', 'Pelayanan pencegahan dan kesiapsiagaan terhadap bencana', 'Jumlah Warga Negara yang memperoleh layanan pencegahan dan kesiapsiagaan terhadap bencana', 0),
(151, 64, '', 'Pelayanan penyelamatan dan evakuasi korban bencana', 'Jumlah Warga Negara yang memperoleh layanan penyelamatan dan evakuasi korban bencana', 0),
(152, 59, '', 'Pelayanan kesehatan ibu hamil', 'Jumlah Ibu Hamil yang mendapatkan layanan kesehatan', 0),
(153, 59, '', 'Pelayanan kesehatan ibu bersalin', 'Jumlah Ibu Bersalin yang mendapatkan layanan kesehatan', 0),
(154, 59, '', 'Pelayanan kesehatan bayi baru lahir', 'Jumlah Bayi Baru Lahir yang mendapatkan layanan kesehatan', 0),
(155, 59, '', 'Pelayanan kesehatan balita', 'Jumlah Balita yang mendapatkan layanan kesehatan', 0),
(156, 59, '', 'Pelayanan kesehatan pada usia pendidikan dasar', 'Jumlah Warga Negara usia pendidikan dasar yang mendapatkan layanan kesehatan', 0),
(157, 59, '', 'Pelayanan kesehatan pada usia produktif', 'Jumlah Warga Negara usia produktif yang mendapatkan layanan kesehatan', 0),
(158, 59, '', 'Pelayanan kesehatan pada usia lanjut', 'warga negara usia lanjut yang mendapatkan layanan kesehatan', 0),
(159, 59, '', 'Pelayanan kesehatan penderita hipertensi', 'Jumlah Warga Negara penderita hipertensi yang mendapatkan layanan kesehatan', 0),
(160, 59, '', 'Pelayanan kesehatan penderita diabetes melitus(DM)', 'Jumlah Warga Negara penderita diabetes mellitus yang mendapatkan layanan kesehatan', 0),
(161, 59, '', 'Pelayanan Kesehatan Orang Dengan Ganguan Jiwa (ODGJ) Berat ', 'Jumlah Warga Negara dengan gangguan jiwa berat yang terlayani kesehatan', 0),
(162, 59, '', 'Pelayanan Kesehatan Orang Terduga Tuberkulosis', 'Jumlah Warga Negara terduga tuberculosis yang mendapatkan layanan kesehatan', 0),
(163, 59, '', 'Pelayanan kesehatan orang dengan risiko terinfeksi virus yang melemahkan daya tahan tubuh manusia (Human Immunodeficiency Virus)', 'Jumlah Warga Negara dengan risiko terinfeksi virus yang melemahkan daya tahan tubuh manusia (Human Immunodeficiency Virus) yang mendapatkan layanan kesehatan', 0),
(164, 62, '', 'Penyediaan Kebutuhan pokok air minum sehari-hari', 'Jumlah Warga Negara yang memperoleh kebutuhan pokok air minum sehari-hari', 0),
(165, 62, '', 'Penyediaan Pelayanan Pengolahan air limbah domestik', 'Jumlah Warga Negara yang memperoleh layanan pengolahan air limbah domestik', 0),
(166, 33, 'Provinsi', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan menengah merupakan Peserta Didik yang berusia 16 (enam belas) tahun sampai dengan 18 (delapan belas) tahun', 'menghitung jumlah anak usia 16 (enam belas) sampai dengan 18 (delapan belas) tahun pada provinsi yang bersangkutan', 0),
(167, 33, 'Provinsi', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan khusus merupakan Peserta Didik penyandang disabilitas yang berusia 4 (empat) tahun sampai dengan 18 (delapan  belas) tahun', 'menghitung jumlah anak usia 4 (empat) sampai dengan 18 (delapan belas) tahun pada provinsi yang bersangkutan', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dinas_indikator_detail`
--

CREATE TABLE `tbl_dinas_indikator_detail` (
  `ID` int(11) NOT NULL,
  `indikator_id` int(11) NOT NULL,
  `deskripsi_detail` varchar(300) NOT NULL,
  `standard` int(11) NOT NULL,
  `nonstandard` int(11) NOT NULL,
  `tidak_terlayani` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `target` float NOT NULL,
  `order_number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_dinas_indikator_detail`
--

INSERT INTO `tbl_dinas_indikator_detail` (`ID`, `indikator_id`, `deskripsi_detail`, `standard`, `nonstandard`, `tidak_terlayani`, `total`, `target`, `order_number`) VALUES
(1, 1, ' Permakanan', 1000, 500, 0, 1500, 0, 1),
(2, 1, 'Sandang', 2500, 200, 0, 2700, 0, 2),
(3, 1, 'Asrama yang mudah diakses', 0, 0, 0, 0, 0, 3),
(4, 1, 'Alat bantu,Perbekalan kesehatan', 0, 0, 0, 0, 0, 4),
(5, 1, 'Bimbingan fisik, mental spiritual, dan sosial', 0, 0, 0, 0, 0, 5),
(6, 1, 'Bimbingan keterampilan hidup sehari-hari', 0, 0, 0, 0, 0, 6),
(7, 1, 'Pembuatan nomor induk kependudukan', 0, 0, 0, 0, 0, 7),
(8, 1, 'Akses ke layanan pendidikan dan kesehatan dasar', 0, 0, 0, 0, 0, 8),
(9, 1, 'Pelayanan penelusuran keluarga', 0, 0, 0, 0, 0, 9),
(10, 1, 'Pelayanan reunifikasi keluarga', 0, 0, 0, 0, 0, 10),
(11, 2, ' Pengasuhan', 0, 0, 0, 0, 0, 1),
(12, 2, 'Permakanan', 0, 0, 0, 0, 0, 2),
(13, 2, 'Sandang', 0, 0, 0, 0, 0, 3),
(14, 2, 'Asrama yang mudah diakses', 0, 0, 0, 0, 0, 4),
(15, 2, 'Perbekalan kesehatan ', 0, 0, 0, 0, 0, 5),
(16, 2, 'Bimbingan fisik, mental spiritual, dan sosial', 0, 0, 0, 0, 0, 6),
(17, 2, 'Bimbingan keterampilan hidup sehari-hari', 0, 0, 0, 0, 0, 7),
(18, 2, 'Pembuatan akta kelahiran, nomor induk kependudukan, dan kartu identitas anak', 0, 0, 0, 0, 0, 8),
(19, 2, 'Akses ke layanan pendidikan dan kesehatan dasar', 0, 0, 0, 0, 0, 9),
(20, 2, 'Pelayanan penelusuran keluarga', 0, 0, 0, 0, 0, 10),
(21, 2, 'Pelayanan reunifikasi keluarga', 0, 0, 0, 0, 0, 11),
(22, 2, 'Akses layanan pengasuhan kepada keluarga pengganti  ', 0, 0, 0, 0, 0, 12),
(23, 2, '', 0, 0, 0, 0, 0, 13),
(24, 3, ' Permakanan', 0, 0, 0, 0, 0, 1),
(25, 3, 'Sandang', 0, 0, 0, 0, 0, 2),
(26, 3, 'Asrama yang mudah diakses', 0, 0, 0, 0, 0, 3),
(27, 3, 'Alat bantu', 0, 0, 0, 0, 0, 4),
(28, 3, 'Perbekalan kesehatan', 0, 0, 0, 0, 0, 5),
(29, 3, 'Bimbingan fisik, mental spiritual, dan sosial', 0, 0, 0, 0, 0, 6),
(30, 3, 'Bimbingan keterampilan hidup sehari-hari', 0, 0, 0, 0, 0, 7),
(31, 3, 'Fasilitasi pembuatan nomor induk kependudukan', 0, 0, 0, 0, 0, 8),
(32, 3, 'Akses ke layanan kesehatan dasar', 0, 0, 0, 0, 0, 9),
(33, 3, 'Pelayanan penelusuran keluarga', 0, 0, 0, 0, 0, 10),
(34, 3, 'Pelayanan reunifikasi keluarga', 0, 0, 0, 0, 0, 11),
(35, 3, 'Pemulasaraan', 0, 0, 0, 0, 0, 12),
(36, 4, ' Permakanan', 0, 0, 0, 0, 0, 1),
(37, 4, 'Sandang', 0, 0, 0, 0, 0, 2),
(38, 4, 'Asrama/ cottage yang mudah diakses', 0, 0, 0, 0, 0, 3),
(39, 4, 'Perbekalan kesehatan', 0, 0, 0, 0, 0, 4),
(40, 4, 'Bimbingan fisik, mental spiritual, dan sosial', 0, 0, 0, 0, 0, 5),
(41, 4, 'Bimbingan keterampilan dasar', 0, 0, 0, 0, 0, 6),
(42, 4, 'Fasilitasi pembuatan nomor induk kependudukan, akta kelahiran, surat nikah, dan/atau kartu identitas anak', 2147483647, 0, 0, 555555, 0, 7),
(43, 4, 'Akses ke layanan kesehatan dasar', 0, 0, 0, 0, 0, 8),
(44, 4, 'Pemulangan ke daerah asal', 0, 0, 0, 0, 0, 9),
(45, 5, ' Permakanan ', 0, 0, 0, 0, 0, 1),
(46, 5, 'Sandang', 0, 0, 0, 0, 0, 2),
(47, 5, 'Tempat penampungan pengungsi', 0, 0, 0, 0, 0, 3),
(48, 5, 'Penanganan khusus bagi kelompok rentan', 0, 0, 0, 0, 0, 4),
(49, 5, 'Dukungan psikososial', 0, 0, 0, 0, 0, 5),
(50, 6, '\n Jumlah Warga Negara yang terdampak krisis kesehatan akibat bencana dan/atau berpotensi bencana provinsi\n', 900, 100, 0, 1000, 90, 1),
(51, 7, '\n Jumlah Warga kondisi kejadian luar biasa provinsi yang mendapatkan layanan\n', 0, 0, 0, 0, 0, 1),
(52, 8, '\n Jumlah Ibu Hamil yang mendapatkan layanan\n', 60, 40, 0, 100, 60, 1),
(53, 9, '\n Jumlah Ibu Bersalin yang mendapatkan layanan\n', 30, 470, 0, 500, 6, 1),
(54, 10, '\n Jumlah Bayi Baru Lahir yang mendapatkan layanan\n', 28, 572, 0, 600, 5, 1),
(55, 11, '\n Jumlah Balita yang mendapatkan layanan\n', 20, 180, 0, 200, 10, 1),
(56, 12, '\n Jumlah Warga usia pendidikan dasar yang mendapatkan layanan\n', 20, 180, 0, 200, 10, 1),
(57, 13, '\n Jumlah Warga usia produktif yang mendapatkan layanan\n', 20, 280, 0, 300, 7, 1),
(58, 14, '\n Jumlah warga negara usia lanjut yang mendapatkan layanan kesehatan\n', 17, 383, 0, 400, 4, 1),
(59, 15, '\n Jumlah Warga penderita hipertensi yang mendapatkan layanan\n', 30, 270, 0, 300, 10, 1),
(60, 16, '\n Jumlah Warga penderita diabetes mellitus yang mendapatkan layanan\n', 30, 370, 0, 400, 8, 1),
(61, 17, '\n Jumlah Warga dengan gangguan jiwa berat yang terlayani\n', 40, 460, 0, 500, 8, 1),
(62, 18, '\n Jumlah Warga terduga tuberculosis yang mendapatkan layanan\n', 20, 380, 0, 400, 5, 1),
(63, 19, '\n Jumlah Warga dengan risiko terinfeksi virus HIV yang mendapatkan layanan\n', 200, 200, 0, 400, 50, 1),
(64, 20, '\n Jumlah korban bencana yang memperoleh rumah layak huni\n', 80, 920, 0, 1000, 8, 1),
(65, 21, '\n Jumlah Warga yang terkena relokasi akibat program Pemerintah Daerah kabupaten/kota yang menerima rumah layak huni\n', 100, 7900, 0, 8000, 1, 1),
(66, 22, ' menghitung jumlah anak usia 16 (enam belas) sampai dengan 18 (delapan belas) tahun pada provinsi', 1300, 200, 0, 1500, 87, 1),
(67, 23, ' menghitung jumlah anak usia 16 sampai dengan 18 (delapan belas) tahun yang sudah tamat atau sedang belajar di sekolah menengah atas', 350, 50, 0, 400, 88, 1),
(68, 24, ' menghitung jumlah anak usia 4 (empat) sampai dengan 18 (delapan belas) tahun pada provinsi', 0, 0, 0, 0, 0, 1),
(69, 25, ' menghitung jumlah anak usia 4 (empat) sampai dengan 18 (delapan belas) tahun di pendidikan khusus', 0, 0, 0, 0, 0, 1),
(70, 26, ' Jumlah Warga yang memperoleh kebutuhan air minum', 800, 0, 0, 800, 100, 1),
(71, 27, ' Jumlah Warga yang memperoleh layanan pengolahan air limbah domestik', 90, 810, 0, 900, 10, 1),
(72, 28, ' Jumlah penyandang disabilitas yang mendapatkan rehabilitasi', 850, 50, 0, 900, 94, 1),
(73, 29, ' Jumlah anak telantar yang memperoleh rehabilitasi diluar panti', 194, 106, 0, 300, 65, 1),
(74, 30, ' Jumlah Warga lanjut usia terlantar yang memperoleh rehabilitasi sosial diluar panti', 0, 0, 0, 0, 0, 1),
(75, 31, ' Jumlah gelandangan dan pengemis yang memperoleh rehabilitasi dasartuna diluar panti', 0, 0, 0, 0, 0, 1),
(76, 32, ' Jumlah Warga korban bencana kab/kota yang memperoleh perlindungan dan jaminan sosial', 300, 100, 0, 400, 75, 1),
(77, 33, ' menghitung jumlah anak usia 5 sampai dengan 6 tahun pada kabupaten/kota yang bersangkutan', 98, 202, 0, 300, 33, 1),
(78, 34, ' menghitung jumlah anak usia 5 (lima) sampai dengan 6 (enam) tahun satuan pendidikan anak usia dini', 0, 0, 0, 0, 0, 1),
(79, 35, ' menghitung jumlah anak usia 7 sampai dengan 12 tahun pada kabupaten/kota', 0, 0, 0, 0, 0, 1),
(80, 36, ' menghitung jumlah anak usia 7 (tujuh) sampai dengan 12 (dua belas) tahun yang mendapat pelayanan di sekolah dasar', 0, 0, 0, 0, 0, 1),
(81, 37, ' menghitung jumlah anak usia 7 (tujuh) sampai dengan 18 (delapan belas) tahun yang mendapatkn pelayanan', 0, 0, 0, 0, 0, 1),
(82, 38, ' menghitung jumlah anak usia 7 (tujuh) sampai dengan 18 (delapan belas) tahun yang mendapatkan di pendidikan kesetaraan', 0, 0, 0, 0, 0, 1),
(83, 39, ' Penyediaan Pemakaman', 100, 900, 0, 1000, 10, 1),
(84, 39, 'Penyediaan Sandang', 0, 0, 0, 0, 0, 2),
(85, 39, 'Penyediaan Asrama yang mudah diakses', 0, 0, 0, 0, 0, 3),
(86, 39, 'Penyediaan Alat bantu', 0, 0, 0, 0, 0, 4),
(87, 39, 'Penyediaan Perbekalan', 0, 0, 0, 0, 0, 5),
(88, 39, 'Kesehatan didalam Panti', 0, 0, 0, 0, 0, 6),
(89, 39, 'Pemberian Bimbingan Fisik, Mental Spiritual & Sosial', 0, 0, 0, 0, 0, 7),
(90, 39, 'Pemberian Bimbingan Aktivitas Hidup Sehari-hari', 0, 0, 0, 0, 0, 8),
(91, 39, 'Fasilitas Pembuatan Nomor Induk Penduduk', 0, 0, 0, 0, 0, 9),
(92, 39, 'Akses ke Layanan Pendidikan & Kesehatan Dasar', 0, 0, 0, 0, 0, 10),
(93, 39, 'Pemberian Pelayanan Penelusuran keluarga', 0, 0, 0, 0, 0, 11),
(94, 39, 'Pemberian Pelayanan Reunifikasi Keluarga', 0, 0, 0, 0, 0, 12),
(95, 40, ' Pengasuhan Penyediaan Pemakaman', 90, 7910, 0, 8000, 1, 1),
(96, 40, 'Penyediaan Sandang', 0, 0, 0, 0, 0, 2),
(97, 40, 'Penyediaan Asrama yang mudah diakses', 0, 0, 0, 0, 0, 3),
(98, 40, 'Penyediaan Perbekalan Kesehatan didalam Panti', 0, 0, 0, 0, 0, 4),
(99, 40, 'Pemberian Bimbingan Fisik, Mental Spiritual & Sosial', 0, 0, 0, 0, 0, 5),
(100, 40, 'Pemberian Bimbingan Aktivitas Hidup Sehari-hari', 0, 0, 0, 0, 0, 6),
(101, 40, 'Fasilitas Pembuatan Akte Kelahiran, Nomor Induk Penduduk, dan Kartu Identitas Anak', 0, 0, 0, 0, 0, 7),
(102, 40, 'Akses ke Layanan Pendidikan & Kesehatan Dasar', 0, 0, 0, 0, 0, 8),
(103, 40, 'Pemberian Pelayanan Penelusuran keluarga', 0, 0, 0, 0, 0, 9),
(104, 40, 'Pemberian Pelayanan Reunifikasi Keluarga', 0, 0, 0, 0, 0, 10),
(105, 40, 'Akses Layanan Pengasuhan kepada Keluarga Pengganti', 0, 0, 0, 0, 0, 11),
(106, 41, ' Penyediaan Pemakaman', 0, 0, 0, 0, 0, 1),
(107, 41, 'Penyediaan Sandang', 0, 0, 0, 0, 0, 2),
(108, 41, 'Penyediaan Asrama yang mudah diakses', 0, 0, 0, 0, 0, 3),
(109, 41, 'Penyediaan Alat bantu', 0, 0, 0, 0, 0, 4),
(110, 41, 'Penyediaan Perbekalan Kesehatan didalam Panti Sosial', 0, 0, 0, 0, 0, 5),
(111, 41, 'Pemberian Bimbingan Fisik, Mental Spiritual & Sosial', 0, 0, 0, 0, 0, 6),
(112, 41, 'Pemberian Bimbingan Aktivitas Hidup Sehari-hari', 0, 0, 0, 0, 0, 7),
(113, 41, 'Fasilitas Pembuatan Nomor Induk Penduduk', 0, 0, 0, 0, 0, 8),
(114, 41, 'Akses ke Layanan Kesehatan Dasar', 0, 0, 0, 0, 0, 9),
(115, 41, 'Pemberian Pelayanan Penelusuran keluarga', 0, 0, 0, 0, 0, 10),
(116, 41, 'Pemberian Pelayanan Reunifikasi Keluarga', 0, 0, 0, 0, 0, 11),
(117, 41, 'Pemulasaraan', 0, 0, 0, 0, 0, 12),
(118, 42, ' Penyediaan Pemakaman', 0, 0, 0, 0, 0, 1),
(119, 42, 'Penyediaan Sandang', 0, 0, 0, 0, 0, 2),
(120, 42, 'Penyediaan Asrama/Wisma yang mudah diakses', 0, 0, 0, 0, 0, 3),
(121, 42, 'Penyediaan Perbekalan Kesehatan didalam Panti', 0, 0, 0, 0, 0, 4),
(122, 42, 'Pemberian Bimbingan Fisik, Mental Spiritual & Sosial', 0, 0, 0, 0, 0, 5),
(123, 42, 'Pemberian Bimbingan Keterampilan Hidup Sehari-hari', 0, 0, 0, 0, 0, 6),
(124, 42, 'Pemberian Bimbingan Keterampilan Dasar', 0, 0, 0, 0, 0, 7),
(125, 42, 'Fasilitas Pembuatan Nomor Induk Kependuduk, Kartu Tanda Penduduk, Akta Kelahiran, Surat Nikah, dan/atau Kartu Identitas Anak', 0, 0, 0, 0, 0, 8),
(126, 42, 'Akses ke Layanan Pendidikan & Kesehatan Dasar', 0, 0, 0, 0, 0, 9),
(127, 42, 'Pemulangan ke Daerah Asal', 0, 0, 0, 0, 0, 10),
(128, 43, ' Penyediaan Pemakaman', 0, 0, 0, 0, 0, 1),
(129, 43, 'Penyediaan Sandang', 0, 0, 0, 0, 0, 2),
(130, 43, 'Peyediaan Tempat Penampungan Pengungsi', 0, 0, 0, 0, 0, 3),
(131, 43, 'Penanganan Khusus Bagi Kelompok Rentan', 0, 0, 0, 0, 0, 4),
(132, 43, 'Pelayanan Dukungan Psikososial', 9000, -33, 0, 8967, 100, 5),
(133, 44, '\n Identifikasi Perumahan di lokasi rawan bencana', 0, 0, 0, 0, 0, 1),
(134, 44, 'Identifikasi lahan-lahan potensial sebagai lokasi relokasi perumahan', 0, 0, 0, 0, 0, 2),
(135, 44, 'Data rumah korban bencana alam kejadian sebelumnya yang belum tertangani', 0, 0, 0, 0, 0, 3),
(136, 44, 'Pendataan Tingkat  Kerusakan Rumah', 0, 0, 0, 0, 0, 4),
(137, 44, 'Verifikasi penerima layanan SPM', 0, 0, 0, 0, 0, 5),
(138, 44, 'Penyusunan Rencana Aksi\n\n  \n', 0, 0, 0, 0, 0, 6),
(139, 45, '\n Pendataan perumahan di lokasi pengembangan Kawasan Strategis Provinsi (KSP)', 0, 0, 0, 0, 0, 1),
(140, 45, 'Pendataanperumahan di lokasi kumuh provinsi (10-15 Ha)', 0, 0, 0, 0, 0, 2),
(141, 45, 'Identifikasi rencana pengembangan perumahanperumahan baru\n\n  \n\n  \n\n  \n', 700, 200, 0, 900, 78, 3),
(142, 46, '\n Buku teks pelajaran', 100, 700, 0, 900, 11, 1),
(143, 46, 'perlengkapan belajar\n', 400, 400, 0, 800, 50, 2),
(144, 47, '\n materi ajar sesuai dengan ragam disabilitas', 200, 600, 0, 800, 25, 1),
(145, 47, 'perlengkapan belajar\n', 71, 730, 0, 800, 9, 2),
(146, 48, '\n pengumpulan data', 600, 200, 0, 800, 75, 1),
(147, 48, 'penghitungan kebutuhan pemenuhan Pelayanan Dasar', 500, 400, 0, 900, 56, 2),
(148, 48, 'penyusunan rencana pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 0, 3),
(149, 48, 'pelaksanaan pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 0, 4),
(150, 48, 'pelayanan kerugian materil', 0, 0, 0, 0, 0, 5),
(151, 48, 'pelayanan pengobatan\n', 90, 710, 0, 800, 11, 6),
(152, 49, '\n pengumpulan data', 0, 0, 0, 0, 0, 1),
(153, 49, 'penghitungan kebutuhan pemenuhan Pelayanan Dasar', 80, 720, 0, 800, 10, 2),
(154, 49, 'penyusunan rencana pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 0, 3),
(155, 49, 'pelaksanaan pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 0, 4),
(156, 49, 'pelayanan kerugian materil', 300, -100, 0, 200, 150, 5),
(157, 49, 'pelayanan pengobatan\n', 600, 300, 0, 900, 67, 6),
(158, 50, '\n pengumpulan data', 0, 0, 0, 0, 0, 1),
(159, 50, 'penghitungan kebutuhan pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 0, 2),
(160, 50, 'penyusunan rencana pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 0, 3),
(161, 50, 'pelaksanaan pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 0, 4),
(162, 50, 'pelayanan kerugian materil', 0, 0, 0, 0, 0, 5),
(163, 50, 'pelayanan pengobatan\n', 0, 0, 0, 0, 0, 6),
(164, 51, '\n pengumpulan data', 0, 0, 0, 0, 0, 1),
(165, 51, 'penghitungan kebutuhan pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 0, 2),
(166, 51, 'penyusunan rencana pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 0, 3),
(167, 51, 'pelaksanaan pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 0, 4),
(168, 51, 'pelayanan kerugian materil', 0, 0, 0, 0, 0, 5),
(169, 51, 'pelayanan pengobatan\n', 0, 0, 0, 0, 0, 6),
(170, 52, '\n pengumpulan data', 0, 0, 0, 0, 0, 1),
(171, 52, 'penghitungan kebutuhan pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 0, 2),
(172, 52, 'penyusunan rencana pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 0, 3),
(173, 52, 'pelaksanaan pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 0, 4),
(174, 52, 'pelayanan kerugian materil', 0, 0, 0, 0, 0, 5),
(175, 52, 'pelayanan pengobatan\n', 0, 0, 0, 0, 0, 6),
(176, 53, '\n pengumpulan data', 0, 0, 0, 0, 0, 1),
(177, 53, 'penghitungan kebutuhan pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 0, 2),
(178, 53, 'penyusunan rencana pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 0, 3),
(179, 53, 'pelaksanaan pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 0, 4),
(180, 53, 'pelayanan kerugian materil', 50, 10, 7990, 8000, 1, 5),
(181, 53, 'pelayanan pengobatan\n', 90, 810, 90, 900, 100, 6),
(182, 54, '\n Vaksin Tetasus Difetri (Td)', 0, 0, 0, 0, 0, 1),
(183, 54, 'Tablet Tambah Darah', 0, 0, 0, 0, 0, 2),
(184, 54, 'Alat Deteksi Resiko Ibu Hamil(Test Kehamilan, Pemeriksaan HB, Pemeriksaan Golongan Darah, dan Pemeriksaan Glukoprotein Urin)', 0, 0, 0, 0, 0, 3),
(185, 54, 'Kartu Ibu/ Rekam Medis Ibu', 0, 0, 0, 0, 0, 4),
(186, 54, 'Buku KIA\n', 0, 0, 0, 0, 0, 5),
(187, 55, '\n Formulir Photograf', 0, 0, 0, 0, 0, 1),
(188, 55, 'Kartu Ibu (Rekam Medis Ibu)', 0, 0, 0, 0, 0, 2),
(189, 55, 'Buku KIA\n', 0, 0, 0, 0, 0, 3),
(190, 56, '\n Vitamin Hepatitis BO', 0, 0, 0, 0, 0, 1),
(191, 56, 'Vitamin K1 Injeksi', 0, 0, 0, 0, 0, 2),
(192, 56, 'Salep/Tetes  Mata Antibiotik', 0, 0, 0, 0, 0, 3),
(193, 56, 'Formulir Bayi Baru Lahir', 0, 0, 0, 0, 0, 4),
(194, 56, 'Formulir MTBM', 0, 0, 0, 0, 0, 5),
(195, 56, 'Buku KIA\n', 0, 0, 0, 0, 0, 6),
(196, 57, '\n Kuisioner Pra Skrining Perkembangan (KPSP) atau Instrumen Standart lain yang berlaku', 0, 0, 0, 0, 0, 1),
(197, 57, 'Formulir DDTK', 0, 0, 0, 0, 0, 2),
(198, 57, 'Buku KIA', 0, 0, 0, 0, 0, 3),
(199, 57, 'Vitamin A Biru', 0, 0, 0, 0, 0, 4),
(200, 57, 'Vitamin A Merah', 0, 0, 0, 0, 0, 5),
(201, 57, 'Vaksin Imunisasi Dasar(HBO, BCG, Polio, IPV, DPT -HB -Hib, Campak Rubell)', 0, 0, 0, 0, 0, 6),
(202, 57, 'Vaksin Imunisasi Lanjutan(DPT -HB -Hib, Campak Rubell', 0, 0, 0, 0, 0, 7),
(203, 57, 'Jarum Suntik dan BHP', 0, 0, 0, 0, 0, 8),
(204, 57, 'Peralatan Anafilaktik\n', 0, 0, 0, 0, 0, 9),
(205, 58, '\n Buku Raport Kesehaan Kita', 0, 0, 0, 0, 0, 1),
(206, 58, 'Buku Pemantauan Kesehatan', 0, 0, 0, 0, 0, 2),
(207, 58, 'Kuisioner  Skrining Kesehatan', 0, 0, 0, 0, 0, 3),
(208, 58, 'Formulir Rekapitulasi Hasil Pelayanan Kesehatan Usia sekolah dan Remaja di dalam Sekolah', 0, 0, 0, 0, 0, 4),
(209, 58, 'Formulir Rekapitulasi Hasil Pelayanan Kesehatan Usia sekolah dan Remaja di Luar Sekolah\n', 0, 0, 0, 0, 0, 5),
(210, 59, '\n Pedoman dan Media KIE( Alat Ukur Berat Badan, Alat Ukur Tinggi Badan, Alat Ukur lingkar Perut, Tensimeter, Glukometer, Tes Strip Gula\n\n Darah, Lancet, Kapas Alkohol, KIT IVA Tes)', 0, 0, 0, 0, 0, 1),
(211, 59, 'Formulir Pencatatan dan Pelaporan Apliksi Sistem Informasi Penyakit Tidak Menular (SIPTM)\n', 0, 0, 0, 0, 0, 2),
(212, 60, '\n Strip Uji Pemeriksaan (Gula Darah, Kolestrol)', 0, 0, 0, 0, 0, 1),
(213, 60, 'Intrumen Geriatric Depression Scale (GDS), Intrumen Abbreviated Mental Test (AMT) dan Instrumen Activity Daily Living (ADL) dalam Paket Pengkajian Paripurna Pasien Geriatri (P3G)', 0, 0, 0, 0, 0, 2),
(214, 60, 'Buku Kesehatan Lansia\n', 0, 0, 0, 0, 0, 3),
(215, 61, '\n Pedoman Pengendalian Hipertensi dan Media KIE', 0, 0, 0, 0, 0, 1),
(216, 61, 'Tensimeter', 0, 0, 0, 0, 0, 2),
(217, 61, 'Formulir Pencatatan dan Pelaporan Apliksi Sistem Informasi  (PTM)\n', 0, 0, 0, 0, 0, 3),
(218, 62, '\n Pengukuran berupa (Glukometer, Strip Tes Gula Darah, Kapas Alkohol, Lancet)', 0, 0, 0, 0, 0, 1),
(219, 62, 'Formulir Pencatatan dan Pelaporan Apliksi Sistem Informasi  (PTM)', 0, 0, 0, 0, 0, 2),
(220, 62, 'Pedoman dan Media KIE\n', 0, 0, 0, 0, 0, 3),
(221, 63, '\n Buku Pedoman Diagnosis Penggolongan Gangguan Jiwa (PPDGJ III) atau Buku Pedoman Diagnosis Penggolongan Gangguan Jiwa terbaru (bila sudah tersedia)', 0, 0, 0, 0, 0, 1),
(222, 63, 'Kit Berisi 2 Alat Fiksasi', 0, 0, 0, 0, 0, 2),
(223, 63, 'Penyediaan Formulir Pencatatan dan Pelaporan', 0, 0, 0, 0, 0, 3),
(224, 63, 'Media KIE\n', 0, 0, 0, 0, 0, 4),
(225, 64, '\n Media KIE (Leaflet, Lembar Balik, Poster, Banner)', 0, 0, 0, 0, 0, 1),
(226, 64, 'Reagen  Zn TB', 0, 0, 0, 0, 0, 2),
(227, 64, 'Masker Jenis Rumah Tangga dan Masker N95', 0, 0, 0, 0, 0, 3),
(228, 64, 'Pot dahak, kaca Slide, Bahan Habis Pakai  (oil emersi, Ether Alkohol Lampu Spirtus/Bunsen, Ose/Lidi), Rak Piring', 0, 0, 0, 0, 0, 4),
(229, 64, 'Catridge Tes Cepat Molekuluer', 0, 0, 0, 0, 0, 5),
(230, 64, 'Formulir Pencatatan dan Pelaporan', 0, 0, 0, 0, 0, 6),
(231, 64, 'Pedoman/Standar Operasional Prosedur\n', 0, 0, 0, 0, 0, 7),
(232, 65, '\n Media KIE berupa Lembar Balik, Leaflet, Poster, Banner', 0, 0, 0, 0, 0, 1),
(233, 65, 'Tes Cepat HIV (RDT) Pertama)', 0, 0, 0, 0, 0, 2),
(234, 65, 'Bahan Media Habis Pakai(Handschoen, Alkohol Swab, Plester, Lancet/ Jarum Steril, Jarum+spuit yang sesuai/vacutainer dan jarum sesuai, Alat tulis, Rekam Medis yang berisi Nomor Rekam Medis, Nomor Fasilitas Pelayanan Kesehatan Pelaksana, Nomor KTP/NIK)\n', 90, 710, 0, 800, 11, 3),
(235, 66, '\n Vaksin Tetasus Difetri (Td)', 0, 0, 0, 0, 0, 1),
(236, 66, 'Tablet Tambah Darah', 0, 0, 0, 0, 0, 2),
(237, 66, 'Alat Deteksi Resiko Ibu Hamil(Test Kehamilan, Pemeriksaan HB, Pemeriksaan Golongan Darah, dan Pemeriksaan Glukoprotein Urin)', 0, 0, 0, 0, 0, 3),
(238, 66, 'Kartu Ibu/ Rekam Medis Ibu', 0, 0, 0, 0, 0, 4),
(239, 66, 'Buku KIA\n', 0, 0, 0, 0, 0, 5),
(240, 67, '\n Formulir Photograf', 0, 0, 0, 0, 0, 1),
(241, 67, 'Kartu Ibu (Rekam Medis Ibu)', 0, 0, 0, 0, 0, 2),
(242, 67, 'Buku KIA\n', 0, 0, 0, 0, 0, 3),
(243, 68, '\n Vitamin Hepatitis BO', 0, 0, 0, 0, 0, 1),
(244, 68, 'Vitamin K1 Injeksi', 0, 0, 0, 0, 0, 2),
(245, 68, 'Salep/Tetes  Mata Antibiotik', 0, 0, 0, 0, 0, 3),
(246, 68, 'Formulir Bayi Baru Lahir', 0, 0, 0, 0, 0, 4),
(247, 68, 'Formulir MTBM', 0, 0, 0, 0, 0, 5),
(248, 68, 'Buku KIA\n', 0, 0, 0, 0, 0, 6),
(249, 69, '\n Kuisioner Pra Skrining Perkembangan (KPSP) atau Instrumen Standart lain yang berlaku', 0, 0, 0, 0, 0, 1),
(250, 69, 'Formulir DDTK', 0, 0, 0, 0, 0, 2),
(251, 69, 'Buku KIA', 0, 0, 0, 0, 0, 3),
(252, 69, 'Vitamin A Biru', 0, 0, 0, 0, 0, 4),
(253, 69, 'Vitamin A Merah', 0, 0, 0, 0, 0, 5),
(254, 69, 'Vaksin Imunisasi Dasar(HBO, BCG, Polio, IPV, DPT -HB -Hib, Campak Rubell)', 0, 0, 0, 0, 0, 6),
(255, 69, 'Vaksin Imunisasi Lanjutan(DPT -HB -Hib, Campak Rubell', 0, 0, 0, 0, 0, 7),
(256, 69, 'Jarum Suntik dan BHP', 0, 0, 0, 0, 0, 8),
(257, 69, 'Peralatan Anafilaktik\n', 0, 0, 0, 0, 0, 9),
(258, 70, '\n Buku Raport Kesehaan Kita', 0, 0, 0, 0, 0, 1),
(259, 70, 'Buku Pemantauan Kesehatan', 0, 0, 0, 0, 0, 2),
(260, 70, 'Kuisioner  Skrining Kesehatan', 0, 0, 0, 0, 0, 3),
(261, 70, 'Formulir Rekapitulasi Hasil Pelayanan Kesehatan Usia sekolah dan Remaja di dalam Sekolah', 0, 0, 0, 0, 0, 4),
(262, 70, 'Formulir Rekapitulasi Hasil Pelayanan Kesehatan Usia sekolah dan Remaja di Luar Sekolah\n', 0, 0, 0, 0, 0, 5),
(263, 71, '\n Pedoman dan Media KIE( Alat Ukur Berat Badan, Alat Ukur Tinggi Badan, Alat Ukur lingkar Perut, Tensimeter, Glukometer, Tes Strip Gula\n\n Darah, Lancet, Kapas Alkohol, KIT IVA Tes)', 0, 0, 0, 0, 0, 1),
(264, 71, 'Formulir Pencatatan dan Pelaporan Apliksi Sistem Informasi Penyakit Tidak Menular (SIPTM)\n', 0, 0, 0, 0, 0, 2),
(265, 72, '\n Strip Uji Pemeriksaan (Gula Darah, Kolestrol)', 0, 0, 0, 0, 0, 1),
(266, 72, 'Intrumen Geriatric Depression Scale (GDS), Intrumen Abbreviated Mental Test (AMT) dan Instrumen Activity Daily Living (ADL) dalam Paket Pengkajian Paripurna Pasien Geriatri (P3G)', 0, 0, 0, 0, 0, 2),
(267, 72, 'Buku Kesehatan Lansia\n', 0, 0, 0, 0, 0, 3),
(268, 73, '\n Pedoman Pengendalian Hipertensi dan Media KIE', 0, 0, 0, 0, 0, 1),
(269, 73, 'Tensimeter', 0, 0, 0, 0, 0, 2),
(270, 73, 'Formulir Pencatatan dan Pelaporan Apliksi Sistem Informasi  (PTM)\n', 0, 0, 0, 0, 0, 3),
(271, 74, '\n Pengukuran berupa (Glukometer, Strip Tes Gula Darah, Kapas Alkohol, Lancet)', 0, 0, 0, 0, 0, 1),
(272, 74, 'Formulir Pencatatan dan Pelaporan Apliksi Sistem Informasi  (PTM)', 0, 0, 0, 0, 0, 2),
(273, 74, 'Pedoman dan Media KIE\n', 0, 0, 0, 0, 0, 3),
(274, 75, '\n Buku Pedoman Diagnosis Penggolongan Gangguan Jiwa (PPDGJ III) atau Buku Pedoman Diagnosis Penggolongan Gangguan Jiwa terbaru (bila sudah tersedia)', 0, 0, 0, 0, 0, 1),
(275, 75, 'Kit Berisi 2 Alat Fiksasi', 0, 0, 0, 0, 0, 2),
(276, 75, 'Penyediaan Formulir Pencatatan dan Pelaporan', 0, 0, 0, 0, 0, 3),
(277, 75, 'Media KIE\n', 0, 0, 0, 0, 0, 4),
(278, 76, '\n Media KIE (Leaflet, Lembar Balik, Poster, Banner)', 0, 0, 0, 0, 0, 1),
(279, 76, 'Reagen  Zn TB', 0, 0, 0, 0, 0, 2),
(280, 76, 'Masker Jenis Rumah Tangga dan Masker N95', 0, 0, 0, 0, 0, 3),
(281, 76, 'Pot dahak, kaca Slide, Bahan Habis Pakai  (oil emersi, Ether Alkohol Lampu Spirtus/Bunsen, Ose/Lidi), Rak Piring', 0, 0, 0, 0, 0, 4),
(282, 76, 'Catridge Tes Cepat Molekuluer', 0, 0, 0, 0, 0, 5),
(283, 76, 'Formulir Pencatatan dan Pelaporan', 0, 0, 0, 0, 0, 6),
(284, 76, 'Pedoman/Standar Operasional Prosedur\n', 0, 0, 0, 0, 0, 7),
(285, 77, '\n Media KIE berupa Lembar Balik, Leaflet, Poster, Banner', 0, 0, 0, 0, 0, 1),
(286, 77, 'Tes Cepat HIV (RDT) Pertama)', 0, 0, 0, 0, 0, 2),
(287, 77, 'Bahan Media Habis Pakai(Handschoen, Alkohol Swab, Plester, Lancet/ Jarum Steril, Jarum+spuit yang sesuai/vacutainer dan jarum sesuai, Alat tulis, Rekam Medis yang berisi Nomor Rekam Medis, Nomor Fasilitas Pelayanan Kesehatan Pelaksana, Nomor KTP/NIK)\n', 80, 820, 0, 900, 9, 3),
(288, 78, '\n Layanan Data dan Pengaduan', 0, 0, 0, 0, 0, 1),
(289, 78, 'Layanan Kedaruratan', 0, 0, 0, 0, 0, 2),
(290, 78, 'Penyediaan Pemakaman', 0, 0, 0, 0, 0, 3),
(291, 78, 'Penyediaan Sandang', 0, 0, 0, 0, 0, 4),
(292, 78, 'Penyediaan Alat bantu', 0, 0, 0, 0, 0, 5),
(293, 78, 'Penyediaan Perbekalan Kesehatan', 0, 0, 0, 0, 0, 6),
(294, 78, 'Pemberian Bimbingan Fisik, Mental Spiritual & Sosial', 0, 0, 0, 0, 0, 7),
(295, 78, 'Pemberian Bimbingan Sosial kepada Keluarga Penyandang Disabilitas Terlantar', 0, 0, 0, 0, 0, 8),
(296, 78, 'Fasilitas Pembuatan Nomor Induk Kependuduk, Kartu Tanda Penduduk, Akta Kelahiran, Surat Nikah, dan/atau Kartu Identitas Anak', 0, 0, 0, 0, 0, 9),
(297, 78, 'Akses ke Layanan Pendidikan & Kesehatan Dasar', 0, 0, 0, 0, 0, 10),
(298, 78, 'Pemberian Pelayanan Penelusuran keluarga', 0, 0, 0, 0, 0, 11),
(299, 78, 'Pemberian Pelayanan Reunifikasi Keluarga', 0, 0, 0, 0, 0, 12),
(300, 78, 'Layanan Rujukan\n', 0, 0, 0, 0, 0, 13),
(301, 79, '\n Layanan Data dan Pengaduan', 0, 0, 0, 0, 0, 1),
(302, 79, 'Layanan Kedaruratan', 0, 0, 0, 0, 0, 2),
(303, 79, 'Penyediaan Pemakaman', 0, 0, 0, 0, 0, 3),
(304, 79, 'Penyediaan Sandang', 0, 0, 0, 0, 0, 4),
(305, 79, 'Penyediaan Alat bantu', 0, 0, 0, 0, 0, 5),
(306, 79, 'Penyediaan Perbekalan Kesehatan', 0, 0, 0, 0, 0, 6),
(307, 79, 'Pemberian Bimbingan Fisik, Mental Spiritual & Sosial', 0, 0, 0, 0, 0, 7),
(308, 79, 'Pemberian Bimbingan Sosial kepada Keluarga Penyandang Disabilitas Terlantar', 0, 0, 0, 0, 0, 8),
(309, 79, 'Fasilitas Pembuatan Nomor Induk Kependuduk, Kartu Tanda Penduduk, Akta Kelahiran, Surat Nikah, dan/atau Kartu Identitas Anak', 0, 0, 0, 0, 0, 9),
(310, 79, 'Akses ke Layanan Pendidikan & Kesehatan Dasar', 0, 0, 0, 0, 0, 10),
(311, 79, 'Pemberian Pelayanan Penelusuran keluarga', 0, 0, 0, 0, 0, 11),
(312, 79, 'Pemberian Pelayanan Reunifikasi Keluarga', 0, 0, 0, 0, 0, 12),
(313, 79, 'Layanan Rujukan\n', 0, 0, 0, 0, 0, 13),
(314, 80, '\n Layanan Data dan Pengaduan', 0, 0, 0, 0, 0, 1),
(315, 80, 'Layanan Kedaruratan', 0, 0, 0, 0, 0, 2),
(316, 80, 'Penyediaan Pemakaman', 0, 0, 0, 0, 0, 3),
(317, 80, 'Penyediaan Sandang', 0, 0, 0, 0, 0, 4),
(318, 80, 'Penyediaan Alat bantu', 0, 0, 0, 0, 0, 5),
(319, 80, 'Penyediaan Perbekalan Kesehatan', 0, 0, 0, 0, 0, 6),
(320, 80, 'Pemberian Bimbingan Fisik, Mental Spiritual & Sosial', 0, 0, 0, 0, 0, 7),
(321, 80, 'Pemberian Bimbingan Sosial kepada Keluarga Penyandang Disabilitas Terlantar', 0, 0, 0, 0, 0, 8),
(322, 80, 'Fasilitas Pembuatan Nomor Induk Kependuduk, Kartu Tanda Penduduk, Akta Kelahiran, Surat Nikah, dan/atau Kartu Identitas Anak', 0, 0, 0, 0, 0, 9),
(323, 80, 'Akses ke Layanan Pendidikan & Kesehatan Dasar', 0, 0, 0, 0, 0, 10),
(324, 80, 'Pemberian Pelayanan Penelusuran keluarga', 0, 0, 0, 0, 0, 11),
(325, 80, 'Pemberian Pelayanan Reunifikasi Keluarga', 0, 0, 0, 0, 0, 12),
(326, 80, 'Layanan Rujukan\n', 0, 0, 0, 0, 0, 13),
(327, 81, '\n Layanan Data dan Pengaduan', 0, 0, 0, 0, 0, 1),
(328, 81, 'Layanan Kedaruratan', 0, 0, 0, 0, 0, 2),
(329, 81, 'Penyediaan Pemakaman', 0, 0, 0, 0, 0, 3),
(330, 81, 'Penyediaan Sandang', 0, 0, 0, 0, 0, 4),
(331, 81, 'Penyediaan Alat bantu', 0, 0, 0, 0, 0, 5),
(332, 81, 'Penyediaan Perbekalan Kesehatan', 0, 0, 0, 0, 0, 6),
(333, 81, 'Pemberian Bimbingan Fisik, Mental Spiritual & Sosial', 0, 0, 0, 0, 0, 7),
(334, 81, 'Pemberian Bimbingan Sosial kepada Keluarga Penyandang Disabilitas Terlantar', 20, 80, 20, 100, 20, 8),
(335, 81, 'Fasilitas Pembuatan Nomor Induk Kependuduk, Kartu Tanda Penduduk, Akta Kelahiran, Surat Nikah, dan/atau Kartu Identitas Anak', 30, 70, 30, 100, 30, 9),
(336, 81, 'Akses ke Layanan Pendidikan & Kesehatan Dasar', 0, 0, 0, 0, 30, 10),
(337, 81, 'Pemberian Pelayanan Penelusuran keluarga', 0, 0, 0, 0, 30, 11),
(338, 81, 'Pemberian Pelayanan Reunifikasi Keluarga', 0, 0, 0, 0, 30, 12),
(339, 81, 'Layanan Rujukan\n', 20, 480, 20, 500, 4, 13),
(340, 82, '\n Penyediaan Pemakaman', 0, 0, 0, 0, 4, 1),
(341, 82, 'Penyediaan Sandang', 300, 7700, 300, 8000, 3.75, 2),
(342, 82, 'Peyediaan Tempat Penampungan Pengungsi', 20, 680, 20, 700, 2.85714, 3),
(343, 82, 'Penanganan Khusus Bagi Kelompok Rentan', 80, 20, 80, 100, 80, 4),
(344, 82, 'Pelayanan Dukungan Psikososial\n', 100, 0, 70, 100, 100, 5),
(345, 83, '\n Buku gambar', 0, 0, 0, 0, 0, 1),
(346, 83, 'Alat Warna\n', 0, 0, 0, 0, 0, 2),
(347, 84, '\n buku teks pelajaran', 0, 0, 0, 0, 0, 1),
(348, 84, 'perlengkapan belajar\n', 0, 0, 0, 0, 0, 2),
(349, 85, '\n modul belajar', 80, 620, 0, 700, 11, 1),
(350, 85, 'perlengkapan belajar\n', 0, 0, 0, 0, 0, 2),
(351, 86, '\n Vaksin Tetasus Difetri (Td)', 0, 0, 0, 0, 0, 1),
(352, 86, 'Tablet Tambah Darah', 0, 0, 0, 0, 0, 2),
(353, 86, 'Alat Deteksi Resiko Ibu Hamil(Test Kehamilan, Pemeriksaan HB, Pemeriksaan Golongan Darah, dan Pemeriksaan Glukoprotein Urin)', 0, 0, 0, 0, 0, 3),
(354, 86, 'Kartu Ibu/ Rekam Medis Ibu', 0, 0, 0, 0, 0, 4),
(355, 86, 'Buku KIA\n', 0, 0, 0, 0, 0, 5),
(356, 87, '\n Formulir Photograf', 0, 0, 0, 0, 0, 1),
(357, 87, 'Kartu Ibu (Rekam Medis Ibu)', 0, 0, 0, 0, 0, 2),
(358, 87, 'Buku KIA\n', 0, 0, 0, 0, 0, 3),
(359, 88, '\n Vitamin Hepatitis BO', 0, 0, 0, 0, 0, 1),
(360, 88, 'Vitamin K1 Injeksi', 0, 0, 0, 0, 0, 2),
(361, 88, 'Salep/Tetes  Mata Antibiotik', 0, 0, 0, 0, 0, 3),
(362, 88, 'Formulir Bayi Baru Lahir', 0, 0, 0, 0, 0, 4),
(363, 88, 'Formulir MTBM', 0, 0, 0, 0, 0, 5),
(364, 88, 'Buku KIA\n', 0, 0, 0, 0, 0, 6),
(365, 89, '\n Kuisioner Pra Skrining Perkembangan (KPSP) atau Instrumen Standart lain yang berlaku', 0, 0, 0, 0, 0, 1),
(366, 89, 'Formulir DDTK', 0, 0, 0, 0, 0, 2),
(367, 89, 'Buku KIA', 0, 0, 0, 0, 0, 3),
(368, 89, 'Vitamin A Biru', 0, 0, 0, 0, 0, 4),
(369, 89, 'Vitamin A Merah', 0, 0, 0, 0, 0, 5),
(370, 89, 'Vaksin Imunisasi Dasar(HBO, BCG, Polio, IPV, DPT -HB -Hib, Campak Rubell)', 0, 0, 0, 0, 0, 6),
(371, 89, 'Vaksin Imunisasi Lanjutan(DPT -HB -Hib, Campak Rubell', 0, 0, 0, 0, 0, 7),
(372, 89, 'Jarum Suntik dan BHP', 0, 0, 0, 0, 0, 8),
(373, 89, 'Peralatan Anafilaktik\n', 0, 0, 0, 0, 0, 9),
(374, 90, '\n Buku Raport Kesehaan Kita', 0, 0, 0, 0, 0, 1),
(375, 90, 'Buku Pemantauan Kesehatan', 0, 0, 0, 0, 0, 2),
(376, 90, 'Kuisioner  Skrining Kesehatan', 0, 0, 0, 0, 0, 3),
(377, 90, 'Formulir Rekapitulasi Hasil Pelayanan Kesehatan Usia sekolah dan Remaja di dalam Sekolah', 0, 0, 0, 0, 0, 4),
(378, 90, 'Formulir Rekapitulasi Hasil Pelayanan Kesehatan Usia sekolah dan Remaja di Luar Sekolah\n', 0, 0, 0, 0, 0, 5),
(379, 91, '\n Pedoman dan Media KIE( Alat Ukur Berat Badan, Alat Ukur Tinggi Badan, Alat Ukur lingkar Perut, Tensimeter, Glukometer, Tes Strip Gula\n\n Darah, Lancet, Kapas Alkohol, KIT IVA Tes)', 0, 0, 0, 0, 0, 1),
(380, 91, 'Formulir Pencatatan dan Pelaporan Apliksi Sistem Informasi Penyakit Tidak Menular (SIPTM)\n', 0, 0, 0, 0, 0, 2),
(381, 92, '\n Strip Uji Pemeriksaan (Gula Darah, Kolestrol)', 0, 0, 0, 0, 0, 1),
(382, 92, 'Intrumen Geriatric Depression Scale (GDS), Intrumen Abbreviated Mental Test (AMT) dan Instrumen Activity Daily Living (ADL) dalam Paket Pengkajian Paripurna Pasien Geriatri (P3G)', 0, 0, 0, 0, 0, 2),
(383, 92, 'Buku Kesehatan Lansia\n', 0, 0, 0, 0, 0, 3),
(384, 93, '\n Pedoman Pengendalian Hipertensi dan Media KIE', 0, 0, 0, 0, 0, 1),
(385, 93, 'Tensimeter', 0, 0, 0, 0, 0, 2),
(386, 93, 'Formulir Pencatatan dan Pelaporan Apliksi Sistem Informasi  (PTM)\n', 0, 0, 0, 0, 0, 3),
(387, 94, '\n Pengukuran berupa (Glukometer, Strip Tes Gula Darah, Kapas Alkohol, Lancet)', 0, 0, 0, 0, 0, 1),
(388, 94, 'Formulir Pencatatan dan Pelaporan Apliksi Sistem Informasi  (PTM)', 0, 0, 0, 0, 0, 2),
(389, 94, 'Pedoman dan Media KIE\n', 0, 0, 0, 0, 0, 3),
(390, 95, '\n Buku Pedoman Diagnosis Penggolongan Gangguan Jiwa (PPDGJ III) atau Buku Pedoman Diagnosis Penggolongan Gangguan Jiwa terbaru (bila sudah tersedia)', 0, 0, 0, 0, 0, 1),
(391, 95, 'Kit Berisi 2 Alat Fiksasi', 0, 0, 0, 0, 0, 2),
(392, 95, 'Penyediaan Formulir Pencatatan dan Pelaporan', 0, 0, 0, 0, 0, 3),
(393, 95, 'Media KIE\n', 0, 0, 0, 0, 0, 4),
(394, 96, '\n Media KIE (Leaflet, Lembar Balik, Poster, Banner)', 0, 0, 0, 0, 0, 1),
(395, 96, 'Reagen  Zn TB', 0, 0, 0, 0, 0, 2),
(396, 96, 'Masker Jenis Rumah Tangga dan Masker N95', 0, 0, 0, 0, 0, 3),
(397, 96, 'Pot dahak, kaca Slide, Bahan Habis Pakai  (oil emersi, Ether Alkohol Lampu Spirtus/Bunsen, Ose/Lidi), Rak Piring', 0, 0, 0, 0, 0, 4),
(398, 96, 'Catridge Tes Cepat Molekuluer', 0, 0, 0, 0, 0, 5),
(399, 96, 'Formulir Pencatatan dan Pelaporan', 0, 0, 0, 0, 0, 6),
(400, 96, 'Pedoman/Standar Operasional Prosedur\n', 0, 0, 0, 0, 0, 7),
(401, 97, '\n Media KIE berupa Lembar Balik, Leaflet, Poster, Banner', 0, 0, 0, 0, 0, 1),
(402, 97, 'Tes Cepat HIV (RDT) Pertama)', 80, 6920, 80, 7000, 100, 2),
(403, 97, 'Bahan Media Habis Pakai(Handschoen, Alkohol Swab, Plester, Lancet/ Jarum Steril, Jarum+spuit yang sesuai/vacutainer dan jarum sesuai, Alat tulis, Rekam Medis yang berisi Nomor Rekam Medis, Nomor Fasilitas Pelayanan Kesehatan Pelaksana, Nomor KTP/NIK)\n', 0, 0, 0, 0, 0, 3),
(404, 98, '\n ukuran kuantitas air minum', 0, 0, 0, 0, 0, 1),
(405, 98, 'kualitas air minum\n', 0, 0, 0, 0, 0, 2),
(406, 99, '\n Kuantitas Pelayanan Air Limbah Domestik', 0, 0, 0, 0, 0, 1),
(407, 99, 'Kualitas Pelayanan Air Limbah Domestik\n', 70, 6930, 70, 7000, 100, 2),
(408, 100, '\n Identifikasi Perumahan di lokasi rawan bencana', 0, 0, 0, 0, 0, 1),
(409, 100, 'Identifikasi lahan-lahan potensial sebagai lokasi relokasi perumahan', 0, 0, 0, 0, 0, 2),
(410, 100, 'Data rumah korban bencana alam kejadian sebelumnya yang belum tertangani', 0, 0, 0, 0, 0, 3),
(411, 100, 'Pendataan Tingkat  Kerusakan Rumah', 0, 0, 0, 0, 0, 4),
(412, 100, 'Verifikasi penerima layanan SPM', 0, 0, 0, 0, 0, 5),
(413, 100, 'Penyusunan Rencana Aksi\n\n  \n', 0, 0, 0, 0, 0, 6),
(414, 101, '\n Pendataan perumahan di lokasi yang berpotensi dapat menimbulkan bahaya', 0, 0, 0, 0, 0, 1),
(415, 101, 'Pendataan perumahan di atas lahan bukan fungsi permukiman', 0, 0, 0, 0, 0, 2),
(416, 101, 'Pendataan rumah sewa milik masyarakat, Rumah Susun Umum dan/atau Rumah Khusus eksisting\n\n  \n\n  \n\n  \n', 0, 0, 0, 0, 0, 3),
(417, 102, '\n Obat-obatan dan Bahan Medis Habis Pakai', 0, 0, 0, 0, 0, 1),
(418, 102, 'Makanan Tambahan/Pendamping untuk Kelompok Rentan (MP ASI, MP Ibu Hamil, Pemberian Makanan untuk Bayi dan Anak (PMBA) dll)', 0, 0, 0, 0, 0, 2),
(419, 102, 'Kelengkapan Pendukung Kesehatan Perorangan (Hyegiene Kit dan Family Kit)\n', 0, 0, 0, 0, 0, 3),
(420, 103, '\n Alat Perlindungan Diri (APD) sesuai dngan Jenis Penyakit', 0, 0, 0, 0, 0, 1),
(421, 103, 'Profilaksis/ Vitamin/ obat/ Vaksin', 0, 0, 0, 0, 0, 2),
(422, 103, 'Alat Pemeriksaan Fisik (Stestoskop, Termometer Badan, Tensimeter,Senter, Test Diagnosis Cepat, dll)', 0, 0, 0, 0, 0, 3),
(423, 103, 'Alat dan Bahan Pengambilan Spesimen (Tabung, Pot, Media  Amies, dll) untuk Specimen  yang berasal dari manusia & Lingkungan sesuai jenis Penyakit', 0, 0, 0, 0, 0, 4),
(424, 103, 'Wadah Pengiriman Spesimen (Specimen Carrier)', 0, 0, 0, 0, 0, 5),
(425, 103, 'Tempat Sampah Biologis', 0, 0, 0, 0, 0, 6),
(426, 103, 'Formulir : Form Penyelidikan Epidemiologi Form/ Lembar KIE Alat Tulis yang diperlukan\n', 90, 3910, 0, 4000, 2, 7),
(427, 104, '\r\n Mendapatkan penyuluhan pra-bencana / pra-krisis terkait bencana aspek kesehatan (termasuk PHBS)', 60, 740, 0, 800, 8, 1),
(428, 104, '\r\n\r\n Pelayanan krisis kesehatan saat bencana, meliputi:\r\n\r\n a) layanan medis dasar dan layanan rujukan bila diperlukan;\r\n\r\n b) mendapatkan layanan pencegahan penyakit menular dan penyehatan lingkungan;\r\n\r\n c) mendapatkan layanan gizi darurat / makanan tambahan untuk bayi, ibu hamil dan anak;\r\n\r\n d) ', 70, 740, 0, 800, 9, 2),
(429, 105, '\r\n Mendapatkan pencatatan untuk pelayanan KLB serta penyuluhan (edukasi) terkait KLB', 0, 0, 0, 800, 9, 1),
(430, 105, '\r\n\r\n Pelayanan kesehatan kondisi KLB, meliputi:\r\n\r\n a) Penemuan kasus dan identifikasi faktor risiko melalui penyelidikan epidemiologis\r\n\r\n b) Penatalaksanaan penderita pada kasus konfirmasi, probable dan suspek yang mencakup kegiatan pemeriksaan, pengobatan, perawatan dan isolasi penderita, termasu', 90, 0, 0, 90, 100, 2),
(431, 106, '\r\n pengumpulan data', 300, 2700, 0, 3000, 10, 1),
(432, 106, 'penghitungan kebutuhan pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 0, 2),
(433, 106, 'penyusunan rencana pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 0, 3),
(434, 106, 'pelaksanaan pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 0, 4),
(435, 106, 'pelayanan kerugian materil', 0, 0, 0, 0, 0, 5),
(436, 106, 'pelayanan pengobatan\r\n', 300, 0, 800, 800, 38, 6),
(437, 107, '\r\n Buku teks pelajaran', 100, 200, 0, 300, 33, 1),
(438, 107, 'perlengkapan belajar\r\n', 0, 0, 0, 0, 0, 2),
(439, 108, '\r\n materi ajar sesuai dengan ragam disabilitas', 0, 0, 0, 0, 0, 1),
(440, 108, 'perlengkapan belajar\r\n', 0, 0, 0, 0, 0, 2),
(441, 109, '\r\n Buku gambar', 0, 0, 0, 1999, 0, 1),
(442, 109, 'Alat Warna\r\n', 0, 0, 0, 0, 0, 2),
(443, 110, '\r\n buku teks pelajaran', 0, 30, 0, 30000, 0, 1),
(444, 110, 'perlengkapan belajar\r\n', 0, 0, 0, 0, 0, 2),
(445, 111, '\r\n modul belajar', 0, 0, 0, 4, 0, 1),
(446, 111, 'perlengkapan belajar\r\n', 2002, 1000, 2000, 3000, 100, 2),
(447, 112, '\r\n Mendapatkan vaksin tetanus, tablet tambah darah, test dan pemeriksaan lab standar, kartu KIA, buku KIA)', 0, 0, 0, 0, 0, 1),
(448, 112, 'Satu kali pelayanan pada trimester pertama (K1)', 0, 0, 0, 0, 0, 2),
(449, 112, 'Satu kali pelayanan pada trimester kedua (K2)', 0, 0, 0, 0, 0, 3),
(450, 112, 'Dua kali pelayanan pada trimester ketiga (K3), serta perujukan (bagi ibu hamil dengan komplikasi)\r\n', 0, 0, 0, 0, 0, 4),
(451, 113, '\r\n Mendapatkan pencatatan dengan formulir partograf, kartu KIA, buku KIA', 0, 0, 0, 0, 0, 1),
(452, 113, 'Mendapatkan asuhan layanan persalinan normal yang bersih dan aman selama persalinan dan setelah bayi lahir serta upaya pencegahan komplikasi  infeksi', 0, 0, 0, 0, 0, 2),
(453, 113, 'Mendapatkan asuhan proses pengeluaran janin yang terjadi pada kehamilan cukup bulan (37 – 42 minggu), lahir spontan dengan presentasi belakang kepala yang berlangsung dalam 18 jam, tanpa komplikasi, baik pada Ibu maupun pada janin, serta perujukan (bagi ibu bersalin dengan komplikasi)\r\n', 0, 0, 0, 0, 0, 3),
(454, 114, '\r\n Mendapatkan pelayanan standar 1 (0-6 jam kelahiran) meliputi: vaksin Hepatitis B0, Vitamin K1 injeksi, Salep / tetes mata (antibiotic), pemotongan dan perawatan tali pusar, inisiasi menyusu dini, pemeriksaan dan pencatatan dengan formulir bayi baru lahir, formulir MTBM dan Buku KIA, serta perujuk', 0, 0, 0, 0, 0, 1),
(455, 114, 'Mendapatkan pelayanan standar 2 (6 jam – 28 hari kelahiran) meliputi: konseling ASI eksklusif & perawatan bayi, vaksin Hepatitis B (injeksi) bagi yang belum, Vitamin K1 injeksi (bagi yang belum), , pemeriksaan dan pencatatan dengan formulir bayi baru lahir, formulir MTBM dan Buku KIA', 0, 0, 0, 0, 0, 2),
(456, 114, 'Kunjungan Neonatal 1 (6 – 48 jam)', 0, 0, 0, 0, 0, 3),
(457, 114, 'Kunjungan Neonatal 2 (3-7 hari)', 0, 0, 0, 0, 0, 4),
(458, 114, 'Kunjungan Neonatal 3 (8 - 28 hari)\r\n', 0, 0, 0, 0, 0, 5),
(459, 115, '\r\n Mendapatkan pencatatan pada KPSP dan DDTK serta buku KIA', 0, 0, 0, 0, 0, 1),
(460, 115, ' Pelayanan balita usia 0-11 bulan (standar 1); meliputi: a) Penimbangan minimal 8 kali setahun; b)  Pengukuran panjang/tinggi badan minimal 2 kali ?/tahun; c) Pemantauan perkembangan minimal 2 kali/tahun;d) Pemberian kapsul vitamin A pada usia 6-11 bulan 1 kali setahun; e) Pemberian imunisasi dasar ', 0, 0, 0, 0, 0, 2),
(461, 115, 'Pelayanan balita usia 12-35 bulan (standar 2); meliputi: a) Penimbangan minimal 8 kali setahun; b) Pengukuran panjang/tinggi badan minimal 2 kali/tahun; c)     Pemantauan perkembangan minimal 2 kali/ tahun; d)    Pemberian kapsul vitamin A sebanyak 2 kali setahun; e) Pemberian Imunisasi Lanjutan; f)', 0, 0, 0, 0, 0, 3),
(462, 115, ' Pelayanan balita usia 36-59 bulan (standar 3); meliputi: Penimbangan minimal 8 kali; Pengukuran panjang/tinggi badan minimal 2 ?kali/tahun. ?Pemantauan perkembangan minimal 2 kali/ tahun. Pemberian kapsul vitamin A  sebanyak 2 kali setahun.Pemantauan perkembangan balita. Pemberian kapsul vitamin A.', 0, 0, 0, 0, 0, 4),
(463, 116, '\r\n Mendapatkan pencatatan pada Buku Rapor Kesehatanku, Buku Pemantauan Kesehatan, Kuisioner Skrining Kesehatan, Formulir Rekapitulasi Hasil Pelayanan kesehatan usia sekolah dan remaja di dalam sekolah / di luar Sekolah', 0, 0, 0, 0, 0, 1),
(464, 116, 'Pelayanan Kesehatan pada Usia Pendidikan Dasar umur 7 tahun meliputi: Penilaian status gizi (tinggi badan, berat badan, tanda klinis anemia); dan  Penilaian tanda vital (tekanan darah, frekuensi nadi dan napas); dan Penilaian kesehatan gigi dan mulut; dan Penilaian ketajaman indera penglihatan denga', 0, 0, 0, 0, 0, 2),
(465, 116, 'Pelayanan Kesehatan pada Usia Pendidikan Dasar umur 7 tahun meliputi: Penilaian status gizi (tinggi badan, berat badan, tanda klinis anemia); dan Penilaian tanda vital (tekanan darah, frekuensi nadi dan napas); dan Penilaian kesehatan gigi dan mulut; dan Penilaian ketajaman indera penglihatan dengan', 0, 0, 0, 0, 0, 3),
(466, 117, '\r\n Mendapatkan pencatatan sesuai aplikasi Penyakit Tidak Menular (PTM), serta mendapatkan materi KIE (edukasi)', 0, 0, 0, 0, 0, 1),
(467, 117, '\r\n\r\n Pelayanan Kesehatan pada Usia Pendidikan Dasar umur 7 tahun meliputi:\r\n\r\n \r\n  Deteksi Obesitas dengan cara penimbangan berat badan dan pengukuran tinggi badan (penilaian Indeks Massa Tubuh) dan pengukuran lingkar perut\r\n \r\n  Deteksi Hipertensi, yang dilakukan dengan memeriksa tekanan darah (ten', 0, 0, 0, 0, 0, 2),
(468, 118, '\r\n Mendapatkan pencatatan sesuai Buku Kesehatan Lansia dan mendapatkan edukasi PHBS (Pola Hidup Bersih Sehat)', 0, 0, 0, 0, 0, 1),
(469, 118, 'Pelayanan Kesehatan pada Usia Lanjut, meliputi: Pengukuran tinggi badan, berat badan, dan lingkar perut;Deteksi Hipertensi, yang dilakukan dengan mengukur tekanan darah; Deteksi kemungkinan Diabetes Melitus dengan menggunakan tes cepat gula darah;Deteksi gangguan mental;Deteksi gangguan kognitif; Pe', 0, 0, 0, 0, 0, 2),
(470, 119, '\r\n Mendapatkan pencatatan sesuai SIM PTM dan mendapatkan materi edukasi PTM', 0, 0, 0, 0, 0, 1),
(471, 119, 'Pelayanan Kesehatan pada Penderita Hipertensi, meliputi Pengukuran tekanan darah; dilakukan minimal setiap bulan satu kali, di fasyankes; Edukasi perubahan gaya hidup, dan kepatuhan minum obat.Terapi Farmakologi, ketika ditemukan hasil pemeriksaan Tekanan Darah Sewaktu (TDS) lebih dari 140 mmHg\r\n', 0, 0, 0, 0, 0, 2),
(472, 120, '\r\n Mendapatkan pencatatan sesuai SIM PTM dan mendapatkan materi edukasi PTM', 0, 0, 0, 0, 0, 1),
(473, 120, 'Pelayanan Kesehatan pada Penderita DM, meliputi:Pengukuran gula darah sewaktu (GDS); dilakukan setiap bulan satu kali;Edukasi perubahan gaya hidup dan atau nutrisi Terapi Farmakologi, ketika ditemukan hasil pemeriksaan GDS lebih dari 200 mg/dl.\r\n\r\n  \r\n', 0, 0, 0, 0, 0, 2),
(474, 121, '\r\n Mendapatkan pencatatan sesuai formulir dan mendapatkan materi edukasi gangguan jiwa', 0, 0, 0, 0, 0, 1),
(475, 121, 'Pelayanan Kesehatan pada ODGJ Berat, meliputi:\r\n \r\n  \r\n   pemeriksaan kesehatan jiwa, mencakup :pemeriksaan status mental, dan Wawancara;\r\n  \r\n   edukasi kepatuhan minum obat\r\n  \r\n   rujukan (jika diperlukan)\r\n \r\n\r\n\r\n  \r\n', 0, 0, 0, 0, 0, 2),
(476, 122, '\r\n Mendapatkan pencatatan sesuai formulir dan mendapatkan materi edukasi Tuberkulosis', 0, 0, 0, 0, 0, 1),
(477, 122, 'Pelayanan Kesehatan pada Orang terduga TB, meliputi:\r\n \r\n  \r\n   Pemeriksaan Klinis, mencakup pemeriksaan gejala dan tanda;\r\n  \r\n   Pemeriksaan penunjang, mencakup pemeriksaan dahak dan atau bakteriologis dan atau radiologis;\r\n  \r\n   Edukasi perilaku berisiko dan pencegahan penularan;\r\n \r\n pelayanan ', 0, 0, 0, 0, 0, 2),
(478, 123, '\r\n Mendapatkan pencatatan sesuai formulir dan mendapatkan materi edukasi HIV-AIDS', 0, 0, 0, 0, 0, 1),
(479, 123, 'Pelayanan Kesehatan pada Orang dengan resiko terinfeksi HIV mendapatkan skrining, dengan pemeriksaan tes cepat  HIV, minimal satu kali dalam satu tahun.', 800, 20, 880, 900, 91, 2),
(480, 123, 'Capaian Kinerja (otomatis dihitung oleh SIM)\r\n', 800, 90, 8910, 9000, 10, 3),
(481, 124, '\r\n Penyediaan Pemakaman', 0, 0, 0, 0, 0, 1),
(482, 124, 'Penyediaan Sandang', 0, 0, 0, 0, 0, 2),
(483, 124, 'Penyediaan Asrama yang mudah diakses', 0, 0, 0, 0, 0, 3),
(484, 124, 'Penyediaan Alat bantu', 0, 0, 0, 0, 0, 4),
(485, 124, 'Penyediaan Perbekalan', 0, 0, 0, 0, 0, 5),
(486, 124, 'Kesehatan didalam Panti', 0, 0, 0, 0, 0, 6),
(487, 124, 'Pemberian Bimbingan Fisik, Mental Spiritual & Sosial', 0, 0, 0, 0, 0, 7),
(488, 124, 'Pemberian Bimbingan Aktivitas Hidup Sehari-hari', 0, 0, 0, 0, 0, 8),
(489, 124, 'Fasilitas Pembuatan Nomor Induk Penduduk', 0, 0, 0, 0, 0, 9),
(490, 124, 'Akses ke Layanan Pendidikan & Kesehatan Dasar', 0, 0, 0, 0, 0, 10),
(491, 124, 'Pemberian Pelayanan Penelusuran keluarga', 0, 0, 0, 0, 0, 11),
(492, 124, 'Pemberian Pelayanan Reunifikasi Keluarga\r\n', 0, 0, 0, 0, 0, 12),
(493, 125, '\r\n Pengasuhan Penyediaan Pemakaman', 0, 0, 0, 0, 0, 1),
(494, 125, 'Penyediaan Sandang', 0, 0, 0, 0, 0, 2),
(495, 125, 'Penyediaan Asrama yang mudah diakses', 0, 0, 0, 0, 0, 3),
(496, 125, 'Penyediaan Perbekalan Kesehatan didalam Panti', 0, 0, 0, 0, 0, 4),
(497, 125, 'Pemberian Bimbingan Fisik, Mental Spiritual & Sosial', 0, 0, 0, 0, 0, 5),
(498, 125, 'Pemberian Bimbingan Aktivitas Hidup Sehari-hari', 0, 0, 0, 0, 0, 6),
(499, 125, 'Fasilitas Pembuatan Akte Kelahiran, Nomor Induk Penduduk, dan Kartu Identitas Anak', 0, 0, 0, 0, 0, 7),
(500, 125, 'Akses ke Layanan Pendidikan & Kesehatan Dasar', 0, 0, 0, 0, 0, 8),
(501, 125, 'Pemberian Pelayanan Penelusuran keluarga', 0, 0, 0, 0, 0, 9),
(502, 125, 'Pemberian Pelayanan Reunifikasi Keluarga', 0, 0, 0, 0, 0, 10),
(503, 125, 'Akses Layanan Pengasuhan kepada Keluarga Pengganti\r\n', 0, 0, 0, 0, 0, 11),
(504, 126, '\r\n Penyediaan Pemakaman', 0, 0, 0, 0, 0, 1),
(505, 126, 'Penyediaan Sandang', 0, 0, 0, 0, 0, 2),
(506, 126, 'Penyediaan Asrama yang mudah diakses', 0, 0, 0, 0, 0, 3),
(507, 126, 'Penyediaan Alat bantu', 0, 0, 0, 0, 0, 4),
(508, 126, 'Penyediaan Perbekalan Kesehatan didalam Panti Sosial', 0, 0, 0, 0, 0, 5),
(509, 126, 'Pemberian Bimbingan Fisik, Mental Spiritual & Sosial', 0, 0, 0, 0, 0, 6),
(510, 126, 'Pemberian Bimbingan Aktivitas Hidup Sehari-hari', 0, 0, 0, 0, 0, 7),
(511, 126, 'Fasilitas Pembuatan Nomor Induk Penduduk', 0, 0, 0, 0, 0, 8),
(512, 126, 'Akses ke Layanan Kesehatan Dasar', 0, 0, 0, 0, 0, 9),
(513, 126, 'Pemberian Pelayanan Penelusuran keluarga', 0, 0, 0, 0, 0, 10),
(514, 126, 'Pemberian Pelayanan Reunifikasi Keluarga', 0, 0, 0, 0, 0, 11),
(515, 126, 'Pemulasaraan\r\n', 0, 0, 0, 0, 0, 12),
(516, 127, '\r\n Penyediaan Pemakaman', 0, 0, 0, 0, 0, 1),
(517, 127, 'Penyediaan Sandang', 0, 0, 0, 0, 0, 2),
(518, 127, 'Penyediaan Asrama/Wisma yang mudah diakses', 0, 0, 0, 0, 0, 3),
(519, 127, 'Penyediaan Perbekalan Kesehatan didalam Panti', 0, 0, 0, 0, 0, 4),
(520, 127, 'Pemberian Bimbingan Fisik, Mental Spiritual & Sosial', 0, 0, 0, 0, 0, 5),
(521, 127, 'Pemberian Bimbingan Keterampilan Hidup Sehari-hari', 0, 0, 0, 0, 0, 6),
(522, 127, 'Pemberian Bimbingan Keterampilan Dasar', 0, 0, 0, 0, 0, 7),
(523, 127, 'Fasilitas Pembuatan Nomor Induk Kependuduk, Kartu Tanda Penduduk, Akta Kelahiran, Surat Nikah, dan/atau Kartu Identitas Anak', 0, 0, 0, 0, 0, 8),
(524, 127, 'Akses ke Layanan Pendidikan & Kesehatan Dasar', 0, 0, 0, 0, 0, 9),
(525, 127, 'Pemulangan ke Daerah Asal\r\n', 0, 0, 0, 0, 0, 10),
(526, 128, '\r\n Penyediaan Pemakaman', 0, 0, 0, 0, 0, 1),
(527, 128, 'Penyediaan Sandang', 0, 0, 0, 0, 0, 2),
(528, 128, 'Peyediaan Tempat Penampungan Pengungsi', 0, 0, 0, 0, 0, 3),
(529, 128, 'Penanganan Khusus Bagi Kelompok Rentan', 0, 0, 0, 0, 0, 4),
(530, 128, 'Pelayanan Dukungan Psikososial\r\n', 90, 90, 0, 900, 10, 5),
(531, 129, '\r\n Identifikasi Perumahan di lokasi rawan bencana', 0, 0, 0, 0, 0, 1),
(532, 129, 'Identifikasi lahan-lahan potensial sebagai lokasi relokasi perumahan', 0, 0, 0, 0, 0, 2),
(533, 129, 'Data rumah korban bencana alam kejadian sebelumnya yang belum tertangani', 0, 0, 0, 0, 0, 3),
(534, 129, 'Pendataan Tingkat  Kerusakan Rumah', 0, 0, 0, 0, 0, 4),
(535, 129, 'Verifikasi penerima layanan SPM', 0, 0, 0, 0, 0, 5),
(536, 129, 'Penyusunan Rencana Aksi\r\n\r\n  \r\n', 500, 0, 900, 900, 56, 6),
(537, 130, '\r\n Pendataan perumahan di lokasi pengembangan Kawasan Strategis Provinsi (KSP)', 0, 0, 0, 0, 0, 1),
(538, 130, 'Pendataanperumahan di lokasi kumuh provinsi (10-15 Ha)', 200, 0, 300, 300, 67, 2),
(539, 130, 'Identifikasi rencana pengembangan perumahanperumahan baru\r\n\r\n  \r\n\r\n  \r\n\r\n  \r\n', 90, 70, 830, 900, 18, 3),
(540, 131, '\r\n Buku teks pelajaran', 0, 0, 0, 0, 0, 1),
(541, 131, 'perlengkapan belajar\r\n', 0, 0, 0, 0, 0, 2),
(542, 132, '\r\n materi ajar sesuai dengan ragam disabilitas', 0, 0, 0, 0, 0, 1),
(543, 132, 'perlengkapan belajar\r\n', 90, 90, 0, 900, 10, 2),
(544, 133, '\r\n Buku gambar', 1000, 30, 10, 3000, 32, 1),
(545, 133, 'Alat Warna\r\n', 20, 200, -180, 5000, -4, 2),
(546, 134, '\r\n buku teks pelajaran', 800, 0, 0, 800, 100, 1),
(547, 134, 'perlengkapan belajar\r\n', 50, 0, 0, 400, 13, 2),
(548, 135, '\r\n modul belajar', 90, 0, 0, 8000, 1, 1),
(549, 135, 'perlengkapan belajar\r\n', 200, 0, 700, 700, 29, 2),
(550, 136, '\r\n pengumpulan data', 0, 0, 0, 9000, 0, 1),
(551, 136, 'penghitungan kebutuhan pemenuhan Pelayanan Dasar', 0, 0, 0, 9000, 0, 2),
(552, 136, 'penyusunan rencana pemenuhan Pelayanan Dasar', 0, 0, 0, 7000, 0, 3),
(553, 136, 'pelaksanaan pemenuhan Pelayanan Dasar', 0, 0, 0, 7000, 0, 4),
(554, 136, 'pelayanan kerugian materil', 0, 0, 0, 5700, 0, 5),
(555, 136, 'pelayanan pengobatan\r\n', 0, 0, 0, 6000, 0, 6),
(556, 137, '\r\n pengumpulan data', 200, 0, 800, 800, 25, 1),
(557, 137, 'penghitungan kebutuhan pemenuhan Pelayanan Dasar', 801, 0, 801, 801, 100, 2),
(558, 137, 'penyusunan rencana pemenuhan Pelayanan Dasar', 801, 0, 800, 800, 100, 3),
(559, 137, 'pelaksanaan pemenuhan Pelayanan Dasar', 801, 0, 801, 801, 100, 4),
(560, 137, 'pelayanan kerugian materil', 801, 0, 900, 900, 89, 5),
(561, 137, 'pelayanan pengobatan\r\n', 90, 90, 610, 700, 26, 6),
(562, 138, '\r\n Penyediaan Pemakaman', 0, 0, 0, 0, 0, 1),
(563, 138, 'Penyediaan Sandang', 0, 0, 0, 0, 0, 2),
(564, 138, 'Penyediaan Asrama yang mudah diakses', 0, 0, 0, 0, 0, 3),
(565, 138, 'Penyediaan Alat bantu', 0, 0, 0, 0, 0, 4),
(566, 138, 'Penyediaan Perbekalan', 0, 0, 0, 0, 0, 5),
(567, 138, 'Kesehatan didalam Panti', 0, 0, 0, 0, 0, 6),
(568, 138, 'Pemberian Bimbingan Fisik, Mental Spiritual & Sosial', 0, 0, 0, 0, 0, 7),
(569, 138, 'Pemberian Bimbingan Aktivitas Hidup Sehari-hari', 0, 0, 0, 0, 0, 8),
(570, 138, 'Fasilitas Pembuatan Nomor Induk Penduduk', 0, 0, 0, 0, 0, 9),
(571, 138, 'Akses ke Layanan Pendidikan & Kesehatan Dasar', 0, 0, 0, 0, 0, 10),
(572, 138, 'Pemberian Pelayanan Penelusuran keluarga', 0, 0, 0, 0, 0, 11),
(573, 138, 'Pemberian Pelayanan Reunifikasi Keluarga\r\n', 0, 0, 0, 0, 0, 12),
(574, 139, '\r\n Pengasuhan Penyediaan Pemakaman', 0, 0, 0, 0, 0, 1),
(575, 139, 'Penyediaan Sandang', 0, 0, 0, 0, 0, 2),
(576, 139, 'Penyediaan Asrama yang mudah diakses', 0, 0, 0, 0, 0, 3),
(577, 139, 'Penyediaan Perbekalan Kesehatan didalam Panti', 0, 0, 0, 0, 0, 4),
(578, 139, 'Pemberian Bimbingan Fisik, Mental Spiritual & Sosial', 0, 0, 0, 0, 0, 5),
(579, 139, 'Pemberian Bimbingan Aktivitas Hidup Sehari-hari', 0, 0, 0, 0, 0, 6),
(580, 139, 'Fasilitas Pembuatan Akte Kelahiran, Nomor Induk Penduduk, dan Kartu Identitas Anak', 0, 0, 0, 0, 0, 7),
(581, 139, 'Akses ke Layanan Pendidikan & Kesehatan Dasar', 0, 0, 0, 0, 0, 8),
(582, 139, 'Pemberian Pelayanan Penelusuran keluarga', 0, 0, 0, 0, 0, 9),
(583, 139, 'Pemberian Pelayanan Reunifikasi Keluarga', 0, 0, 0, 0, 0, 10),
(584, 139, 'Akses Layanan Pengasuhan kepada Keluarga Pengganti\r\n', 0, 0, 0, 0, 0, 11),
(585, 140, '\r\n Penyediaan Pemakaman', 0, 0, 0, 0, 0, 1),
(586, 140, 'Penyediaan Sandang', 0, 0, 0, 0, 0, 2),
(587, 140, 'Penyediaan Asrama yang mudah diakses', 0, 0, 0, 0, 0, 3),
(588, 140, 'Penyediaan Alat bantu', 0, 0, 0, 0, 0, 4),
(589, 140, 'Penyediaan Perbekalan Kesehatan didalam Panti Sosial', 0, 0, 0, 0, 0, 5),
(590, 140, 'Pemberian Bimbingan Fisik, Mental Spiritual & Sosial', 0, 0, 0, 0, 0, 6),
(591, 140, 'Pemberian Bimbingan Aktivitas Hidup Sehari-hari', 0, 0, 0, 0, 0, 7),
(592, 140, 'Fasilitas Pembuatan Nomor Induk Penduduk', 0, 0, 0, 0, 0, 8);
INSERT INTO `tbl_dinas_indikator_detail` (`ID`, `indikator_id`, `deskripsi_detail`, `standard`, `nonstandard`, `tidak_terlayani`, `total`, `target`, `order_number`) VALUES
(593, 140, 'Akses ke Layanan Kesehatan Dasar', 0, 0, 0, 0, 0, 9),
(594, 140, 'Pemberian Pelayanan Penelusuran keluarga', 0, 0, 0, 0, 0, 10),
(595, 140, 'Pemberian Pelayanan Reunifikasi Keluarga', 0, 0, 0, 0, 0, 11),
(596, 140, 'Pemulasaraan\r\n', 0, 0, 0, 0, 0, 12),
(597, 141, '\r\n Penyediaan Pemakaman', 0, 0, 0, 800, 0, 1),
(598, 141, 'Penyediaan Sandang', 0, 0, 0, 0, 0, 2),
(599, 141, 'Penyediaan Asrama/Wisma yang mudah diakses', 9, 0, 0, 90, 10, 3),
(600, 141, 'Penyediaan Perbekalan Kesehatan didalam Panti', 0, 0, 0, 0, 10, 4),
(601, 141, 'Pemberian Bimbingan Fisik, Mental Spiritual & Sosial', 0, 0, 0, 0, 10, 5),
(602, 141, 'Pemberian Bimbingan Keterampilan Hidup Sehari-hari', 0, 0, 0, 0, 10, 6),
(603, 141, 'Pemberian Bimbingan Keterampilan Dasar', 0, 0, 0, 0, 10, 7),
(604, 141, 'Fasilitas Pembuatan Nomor Induk Kependuduk, Kartu Tanda Penduduk, Akta Kelahiran, Surat Nikah, dan/atau Kartu Identitas Anak', 0, 0, 0, 0, 10, 8),
(605, 141, 'Akses ke Layanan Pendidikan & Kesehatan Dasar', 0, 0, 0, 0, 10, 9),
(606, 141, 'Pemulangan ke Daerah Asal\r\n', 7, 0, 7, 70, 10, 10),
(607, 142, '\r\n Penyediaan Pemakaman', 90, 0, 90, 900, 10, 1),
(608, 142, 'Penyediaan Sandang', 0, 0, 0, 0, 10, 2),
(609, 142, 'Peyediaan Tempat Penampungan Pengungsi', 0, 0, 0, 0, 10, 3),
(610, 142, 'Penanganan Khusus Bagi Kelompok Rentan', 0, 0, 0, 0, 10, 4),
(611, 142, 'Pelayanan Dukungan Psikososial\r\n', 0, 0, 0, 0, 10, 5),
(612, 143, '\r\n Penyediaan Pemakaman', 0, 0, 0, 0, 10, 1),
(613, 143, 'Penyediaan Sandang', 0, 0, 0, 0, 10, 2),
(614, 143, 'Penyediaan Asrama yang mudah diakses', 0, 0, 0, 0, 10, 3),
(615, 143, 'Penyediaan Alat bantu', 0, 0, 0, 0, 10, 4),
(616, 143, 'Penyediaan Perbekalan', 0, 0, 0, 0, 10, 5),
(617, 143, 'Kesehatan didalam Panti', 0, 0, 0, 0, 10, 6),
(618, 143, 'Pemberian Bimbingan Fisik, Mental Spiritual & Sosial', 0, 0, 0, 0, 10, 7),
(619, 143, 'Pemberian Bimbingan Aktivitas Hidup Sehari-hari', 0, 0, 0, 0, 10, 8),
(620, 143, 'Fasilitas Pembuatan Nomor Induk Penduduk', 0, 0, 0, 0, 10, 9),
(621, 143, 'Akses ke Layanan Pendidikan & Kesehatan Dasar', 0, 0, 0, 0, 10, 10),
(622, 143, 'Pemberian Pelayanan Penelusuran keluarga', 0, 0, 0, 0, 10, 11),
(623, 143, 'Pemberian Pelayanan Reunifikasi Keluarga\r\n', 0, 0, 0, 0, 10, 12),
(624, 144, '\r\n Pengasuhan Penyediaan Pemakaman', 0, 0, 0, 0, 10, 1),
(625, 144, 'Penyediaan Sandang', 0, 0, 0, 0, 10, 2),
(626, 144, 'Penyediaan Asrama yang mudah diakses', 0, 0, 0, 0, 10, 3),
(627, 144, 'Penyediaan Perbekalan Kesehatan didalam Panti', 0, 0, 0, 0, 10, 4),
(628, 144, 'Pemberian Bimbingan Fisik, Mental Spiritual & Sosial', 0, 0, 0, 0, 10, 5),
(629, 144, 'Pemberian Bimbingan Aktivitas Hidup Sehari-hari', 0, 0, 0, 0, 10, 6),
(630, 144, 'Fasilitas Pembuatan Akte Kelahiran, Nomor Induk Penduduk, dan Kartu Identitas Anak', 0, 0, 0, 0, 10, 7),
(631, 144, 'Akses ke Layanan Pendidikan & Kesehatan Dasar', 0, 0, 0, 0, 10, 8),
(632, 144, 'Pemberian Pelayanan Penelusuran keluarga', 0, 0, 0, 0, 10, 9),
(633, 144, 'Pemberian Pelayanan Reunifikasi Keluarga', 0, 0, 0, 0, 10, 10),
(634, 144, 'Akses Layanan Pengasuhan kepada Keluarga Pengganti\r\n', 80, 0, 0, 700, 11, 11),
(635, 145, '\r\n Penyediaan Pemakaman', 0, 0, 0, 0, 11, 1),
(636, 145, 'Penyediaan Sandang', 0, 0, 0, 0, 11, 2),
(637, 145, 'Penyediaan Asrama yang mudah diakses', 0, 0, 0, 0, 11, 3),
(638, 145, 'Penyediaan Alat bantu', 0, 0, 0, 0, 11, 4),
(639, 145, 'Penyediaan Perbekalan Kesehatan didalam Panti Sosial', 0, 0, 0, 0, 11, 5),
(640, 145, 'Pemberian Bimbingan Fisik, Mental Spiritual & Sosial', 0, 0, 0, 0, 11, 6),
(641, 145, 'Pemberian Bimbingan Aktivitas Hidup Sehari-hari', 0, 0, 0, 0, 11, 7),
(642, 145, 'Fasilitas Pembuatan Nomor Induk Penduduk', 0, 0, 0, 0, 11, 8),
(643, 145, 'Akses ke Layanan Kesehatan Dasar', 0, 0, 0, 0, 11, 9),
(644, 145, 'Pemberian Pelayanan Penelusuran keluarga', 0, 0, 0, 0, 11, 10),
(645, 145, 'Pemberian Pelayanan Reunifikasi Keluarga', 0, 0, 0, 0, 11, 11),
(646, 145, 'Pemulasaraan\r\n', 0, 0, 0, 0, 11, 12),
(647, 146, '\r\n Penyediaan Pemakaman', 900, 0, 0, 9000, 10, 1),
(648, 146, 'Penyediaan Sandang', 0, 0, 0, 0, 10, 2),
(649, 146, 'Penyediaan Asrama/Wisma yang mudah diakses', 0, 0, 0, 0, 10, 3),
(650, 146, 'Penyediaan Perbekalan Kesehatan didalam Panti', 0, 0, 0, 0, 10, 4),
(651, 146, 'Pemberian Bimbingan Fisik, Mental Spiritual & Sosial', 0, 0, 0, 0, 10, 5),
(652, 146, 'Pemberian Bimbingan Keterampilan Hidup Sehari-hari', 0, 0, 0, 0, 10, 6),
(653, 146, 'Pemberian Bimbingan Keterampilan Dasar', 0, 0, 0, 0, 10, 7),
(654, 146, 'Fasilitas Pembuatan Nomor Induk Kependuduk, Kartu Tanda Penduduk, Akta Kelahiran, Surat Nikah, dan/atau Kartu Identitas Anak', 0, 0, 0, 0, 10, 8),
(655, 146, 'Akses ke Layanan Pendidikan & Kesehatan Dasar', 0, 0, 0, 0, 10, 9),
(656, 146, 'Pemulangan ke Daerah Asal\r\n', 0, 0, 0, 0, 10, 10),
(657, 147, '\r\n Penyediaan Pemakaman', 80, 0, 0, 800, 10, 1),
(658, 147, 'Penyediaan Sandang', 0, 0, 0, 0, 10, 2),
(659, 147, 'Peyediaan Tempat Penampungan Pengungsi', 70, 10, 0, 700, 10, 3),
(660, 147, 'Penanganan Khusus Bagi Kelompok Rentan', 0, 0, 0, 0, 10, 4),
(661, 147, 'Pelayanan Dukungan Psikososial\r\n', 0, 0, 0, 900, 10, 5),
(662, 148, '\r\n pengumpulan data', 80, 9, 0, 800, 10, 1),
(663, 148, 'penghitungan kebutuhan pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 10, 2),
(664, 148, 'penyusunan rencana pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 10, 3),
(665, 148, 'pelaksanaan pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 10, 4),
(666, 148, 'pelayanan kerugian materil', 0, 0, 0, 0, 10, 5),
(667, 148, 'pelayanan pengobatan\r\n', 0, 0, 0, 0, 10, 6),
(668, 149, '\r\n pengumpulan data', 80, 0, 0, 800, 10, 1),
(669, 149, 'penghitungan kebutuhan pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 10, 2),
(670, 149, 'penyusunan rencana pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 10, 3),
(671, 149, 'pelaksanaan pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 10, 4),
(672, 149, 'pelayanan kerugian materil', 0, 0, 0, 0, 10, 5),
(673, 149, 'pelayanan pengobatan\r\n', 0, 0, 0, 0, 10, 6),
(674, 150, '\r\n pengumpulan data', 0, 0, 0, 0, 10, 1),
(675, 150, 'penghitungan kebutuhan pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 10, 2),
(676, 150, 'penyusunan rencana pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 10, 3),
(677, 150, 'pelaksanaan pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 10, 4),
(678, 150, 'pelayanan kerugian materil', 0, 0, 0, 0, 10, 5),
(679, 150, 'pelayanan pengobatan\r\n', 0, 0, 0, 0, 10, 6),
(680, 151, '\r\n pengumpulan data', 80, 90, 0, 900, 9, 1),
(681, 151, 'penghitungan kebutuhan pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 9, 2),
(682, 151, 'penyusunan rencana pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 9, 3),
(683, 151, 'pelaksanaan pemenuhan Pelayanan Dasar', 0, 0, 0, 0, 9, 4),
(684, 151, 'pelayanan kerugian materil', 0, 0, 0, 0, 9, 5),
(685, 151, 'pelayanan pengobatan\r\n', 80, 0, 0, 700, 11, 6),
(686, 152, '\r\n Mendapatkan vaksin tetanus, tablet tambah darah, test dan pemeriksaan lab standar, kartu KIA, buku KIA)', 0, 0, 0, 0, 0, 1),
(687, 152, 'Satu kali pelayanan pada trimester pertama (K1)', 0, 0, 0, 0, 0, 2),
(688, 152, 'Satu kali pelayanan pada trimester kedua (K2)', 0, 0, 0, 0, 0, 3),
(689, 152, 'Dua kali pelayanan pada trimester ketiga (K3), serta perujukan (bagi ibu hamil dengan komplikasi)\r\n', 0, 0, 0, 0, 0, 4),
(690, 153, '\r\n Mendapatkan pencatatan dengan formulir partograf, kartu KIA, buku KIA', 0, 0, 0, 0, 0, 1),
(691, 153, 'Mendapatkan asuhan layanan persalinan normal yang bersih dan aman selama persalinan dan setelah bayi lahir serta upaya pencegahan komplikasi  infeksi', 0, 0, 0, 0, 0, 2),
(692, 153, 'Mendapatkan asuhan proses pengeluaran janin yang terjadi pada kehamilan cukup bulan (37 – 42 minggu), lahir spontan dengan presentasi belakang kepala yang berlangsung dalam 18 jam, tanpa komplikasi, baik pada Ibu maupun pada janin, serta perujukan (bagi ibu bersalin dengan komplikasi)\r\n', 0, 0, 0, 0, 0, 3),
(693, 154, '\r\n Mendapatkan pelayanan standar 1 (0-6 jam kelahiran) meliputi: vaksin Hepatitis B0, Vitamin K1 injeksi, Salep / tetes mata (antibiotic), pemotongan dan perawatan tali pusar, inisiasi menyusu dini, pemeriksaan dan pencatatan dengan formulir bayi baru lahir, formulir MTBM dan Buku KIA, serta perujuk', 0, 0, 0, 0, 0, 1),
(694, 154, 'Mendapatkan pelayanan standar 2 (6 jam – 28 hari kelahiran) meliputi: konseling ASI eksklusif & perawatan bayi, vaksin Hepatitis B (injeksi) bagi yang belum, Vitamin K1 injeksi (bagi yang belum), , pemeriksaan dan pencatatan dengan formulir bayi baru lahir, formulir MTBM dan Buku KIA', 0, 0, 0, 0, 0, 2),
(695, 154, 'Kunjungan Neonatal 1 (6 – 48 jam)', 0, 0, 0, 0, 0, 3),
(696, 154, 'Kunjungan Neonatal 2 (3-7 hari)', 0, 0, 0, 0, 0, 4),
(697, 154, 'Kunjungan Neonatal 3 (8 - 28 hari)\r\n', 0, 0, 0, 0, 0, 5),
(698, 155, '\r\n Mendapatkan pencatatan pada KPSP dan DDTK serta buku KIA', 0, 0, 0, 0, 0, 1),
(699, 155, ' Pelayanan balita usia 0-11 bulan (standar 1); meliputi: a) Penimbangan minimal 8 kali setahun; b)  Pengukuran panjang/tinggi badan minimal 2 kali ?/tahun; c) Pemantauan perkembangan minimal 2 kali/tahun;d) Pemberian kapsul vitamin A pada usia 6-11 bulan 1 kali setahun; e) Pemberian imunisasi dasar ', 0, 0, 0, 0, 0, 2),
(700, 155, 'Pelayanan balita usia 12-35 bulan (standar 2); meliputi: a) Penimbangan minimal 8 kali setahun; b) Pengukuran panjang/tinggi badan minimal 2 kali/tahun; c)     Pemantauan perkembangan minimal 2 kali/ tahun; d)    Pemberian kapsul vitamin A sebanyak 2 kali setahun; e) Pemberian Imunisasi Lanjutan; f)', 0, 0, 0, 0, 0, 3),
(701, 155, ' Pelayanan balita usia 36-59 bulan (standar 3); meliputi: Penimbangan minimal 8 kali; Pengukuran panjang/tinggi badan minimal 2 ?kali/tahun. ?Pemantauan perkembangan minimal 2 kali/ tahun. Pemberian kapsul vitamin A  sebanyak 2 kali setahun.Pemantauan perkembangan balita. Pemberian kapsul vitamin A.', 0, 0, 0, 0, 0, 4),
(702, 156, '\r\n Mendapatkan pencatatan pada Buku Rapor Kesehatanku, Buku Pemantauan Kesehatan, Kuisioner Skrining Kesehatan, Formulir Rekapitulasi Hasil Pelayanan kesehatan usia sekolah dan remaja di dalam sekolah / di luar Sekolah', 0, 0, 0, 0, 0, 1),
(703, 156, 'Pelayanan Kesehatan pada Usia Pendidikan Dasar umur 7 tahun meliputi: Penilaian status gizi (tinggi badan, berat badan, tanda klinis anemia); dan  Penilaian tanda vital (tekanan darah, frekuensi nadi dan napas); dan Penilaian kesehatan gigi dan mulut; dan Penilaian ketajaman indera penglihatan denga', 0, 0, 0, 0, 0, 2),
(704, 156, 'Pelayanan Kesehatan pada Usia Pendidikan Dasar umur 7 tahun meliputi: Penilaian status gizi (tinggi badan, berat badan, tanda klinis anemia); dan Penilaian tanda vital (tekanan darah, frekuensi nadi dan napas); dan Penilaian kesehatan gigi dan mulut; dan Penilaian ketajaman indera penglihatan dengan', 0, 0, 0, 0, 0, 3),
(705, 157, '\r\n Mendapatkan pencatatan sesuai aplikasi Penyakit Tidak Menular (PTM), serta mendapatkan materi KIE (edukasi)', 0, 0, 0, 0, 0, 1),
(706, 157, '\r\n\r\n Pelayanan Kesehatan pada Usia Pendidikan Dasar umur 7 tahun meliputi:\r\n\r\n \r\n  Deteksi Obesitas dengan cara penimbangan berat badan dan pengukuran tinggi badan (penilaian Indeks Massa Tubuh) dan pengukuran lingkar perut\r\n \r\n  Deteksi Hipertensi, yang dilakukan dengan memeriksa tekanan darah (ten', 0, 0, 0, 0, 0, 2),
(707, 158, '\r\n Mendapatkan pencatatan sesuai Buku Kesehatan Lansia dan mendapatkan edukasi PHBS (Pola Hidup Bersih Sehat)', 0, 0, 0, 0, 0, 1),
(708, 158, 'Pelayanan Kesehatan pada Usia Lanjut, meliputi: Pengukuran tinggi badan, berat badan, dan lingkar perut;Deteksi Hipertensi, yang dilakukan dengan mengukur tekanan darah; Deteksi kemungkinan Diabetes Melitus dengan menggunakan tes cepat gula darah;Deteksi gangguan mental;Deteksi gangguan kognitif; Pe', 0, 0, 0, 0, 0, 2),
(709, 159, '\r\n Mendapatkan pencatatan sesuai SIM PTM dan mendapatkan materi edukasi PTM', 0, 0, 0, 0, 0, 1),
(710, 159, 'Pelayanan Kesehatan pada Penderita Hipertensi, meliputi Pengukuran tekanan darah; dilakukan minimal setiap bulan satu kali, di fasyankes; Edukasi perubahan gaya hidup, dan kepatuhan minum obat.Terapi Farmakologi, ketika ditemukan hasil pemeriksaan Tekanan Darah Sewaktu (TDS) lebih dari 140 mmHg\r\n', 0, 0, 0, 0, 0, 2),
(711, 160, '\r\n Mendapatkan pencatatan sesuai SIM PTM dan mendapatkan materi edukasi PTM', 0, 0, 0, 0, 0, 1),
(712, 160, 'Pelayanan Kesehatan pada Penderita DM, meliputi:Pengukuran gula darah sewaktu (GDS); dilakukan setiap bulan satu kali;Edukasi perubahan gaya hidup dan atau nutrisi Terapi Farmakologi, ketika ditemukan hasil pemeriksaan GDS lebih dari 200 mg/dl.\r\n\r\n  \r\n', 0, 0, 0, 0, 0, 2),
(713, 161, '\r\n Mendapatkan pencatatan sesuai formulir dan mendapatkan materi edukasi gangguan jiwa', 0, 0, 0, 0, 0, 1),
(714, 161, 'Pelayanan Kesehatan pada ODGJ Berat, meliputi:\r\n \r\n  \r\n   pemeriksaan kesehatan jiwa, mencakup :pemeriksaan status mental, dan Wawancara;\r\n  \r\n   edukasi kepatuhan minum obat\r\n  \r\n   rujukan (jika diperlukan)\r\n \r\n\r\n\r\n  \r\n', 0, 0, 0, 0, 0, 2),
(715, 162, '\r\n Mendapatkan pencatatan sesuai formulir dan mendapatkan materi edukasi Tuberkulosis', 0, 0, 0, 0, 0, 1),
(716, 162, 'Pelayanan Kesehatan pada Orang terduga TB, meliputi:\r\n \r\n  \r\n   Pemeriksaan Klinis, mencakup pemeriksaan gejala dan tanda;\r\n  \r\n   Pemeriksaan penunjang, mencakup pemeriksaan dahak dan atau bakteriologis dan atau radiologis;\r\n  \r\n   Edukasi perilaku berisiko dan pencegahan penularan;\r\n \r\n pelayanan ', 0, 0, 0, 0, 0, 2),
(717, 163, '\r\n Mendapatkan pencatatan sesuai formulir dan mendapatkan materi edukasi HIV-AIDS', 90, 0, 0, 900, 10, 1),
(718, 163, 'Pelayanan Kesehatan pada Orang dengan resiko terinfeksi HIV mendapatkan skrining, dengan pemeriksaan tes cepat  HIV, minimal satu kali dalam satu tahun.', 0, 0, 0, 0, 10, 2),
(719, 163, 'Capaian Kinerja (otomatis dihitung oleh SIM)\r\n', 0, 0, 0, 900, 10, 3),
(720, 164, '\r\n ukuran kuantitas air minum', 90, 0, 0, 900, 10, 1),
(721, 164, 'kualitas air minum\r\n', 60, 0, 0, 800, 8, 2),
(722, 165, '\r\n Kuantitas Pelayanan Air Limbah Domestik', 500, 0, 0, 700, 71, 1),
(723, 165, 'Kualitas Pelayanan Air Limbah Domestik\r\n', 0, 0, 0, 0, 0, 2),
(724, 166, '\r\n Buku teks pelajaran', 0, 0, 0, 0, 0, 1),
(725, 166, 'perlengkapan belajar\r\n', 0, 0, 0, 0, 0, 2),
(726, 167, '\r\n materi ajar sesuai dengan ragam disabilitas', 800, 0, 0, 8000, 10, 1),
(727, 167, 'perlengkapan belajar\r\n', 0, 0, 0, 0, 10, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_masalah`
--

CREATE TABLE `tbl_masalah` (
  `id_tr` int(11) NOT NULL,
  `id_masalah` int(11) NOT NULL,
  `id_prov` int(11) NOT NULL,
  `id_kabkot` int(11) NOT NULL,
  `tahun` enum('2018','2019','2020','2020','2021') NOT NULL,
  `keterangan` longtext NOT NULL,
  `wilayah` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_masalah`
--

INSERT INTO `tbl_masalah` (`id_tr`, `id_masalah`, `id_prov`, `id_kabkot`, `tahun`, `keterangan`, `wilayah`) VALUES
(1, 5, 17, 1703, '2019', '<p>\r\n demo masalah</p>\r\n', 'Kabupaten/Kota'),
(2, 5, 51, 5102, '2020', '<p>\r\n masalah kabupaten</p>\r\n', 'Kabupaten/Kota'),
(3, 1, 51, 1112, '2020', '<p>\r\n bayi hilang</p>\r\n', 'Kabupaten/Kota'),
(4, 1, 36, 1101, '2020', '<p>\r\n propinai test</p>\r\n', 'Provinsi');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_master_dinas`
--

CREATE TABLE `tbl_master_dinas` (
  `dinas_id` int(11) NOT NULL,
  `dinas` varchar(100) NOT NULL,
  `dasar_hukum` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_master_dinas`
--

INSERT INTO `tbl_master_dinas` (`dinas_id`, `dinas`, `dasar_hukum`) VALUES
(1, 'SOSIAL', 'Peraturan Menteri Sosial Republik Indonesia Nomor 9 Tahun 2018 '),
(2, 'KESEHATAN', 'Peraturan Menteri Kesehatan Republik Indonesia Nomor 4 Tahun 2019 '),
(3, 'PERUMAHAN RAKYAT', 'Peraturan Menteri Pendidikan Dan Kebudayaan  Republik Indonesia Nomor 32 Tahun 2018 '),
(4, 'TRANTIBUMLINMAS', 'berdasarkan PP 2/2018'),
(5, 'PENDIDIKAN', 'Peraturan Menteri Pendidikan Dan Kebudayaan  Republik Indonesia Nomor 32 Tahun 2018 '),
(6, 'PU', 'Peraturan Menteri Pekerjaan Umum Dan Perumahan Rakyat  Republik Indonesia Nomor  29/Prt/M/2018 ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_master_indikator`
--

CREATE TABLE `tbl_master_indikator` (
  `id` int(11) NOT NULL,
  `dinas_id` int(11) NOT NULL,
  `wilayah` enum('Provinsi','Kabupaten/Kota') NOT NULL,
  `indikator_kinerja` varchar(300) NOT NULL,
  `indikator_pencapaian` varchar(300) NOT NULL,
  `item_indikator` text NOT NULL,
  `satuan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_master_indikator`
--

INSERT INTO `tbl_master_indikator` (`id`, `dinas_id`, `wilayah`, `indikator_kinerja`, `indikator_pencapaian`, `item_indikator`, `satuan`) VALUES
(1, 1, 'Provinsi', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di dalam Panti Sosial', 'Jumlah   Warga   Negara penyandang disabilitas yang memperoleh rehabilitasi sosial diluar panti', '<p>\n Penyediaan Pemakaman#Penyediaan Sandang#Penyediaan Asrama yang mudah diakses#Penyediaan Alat bantu#Penyediaan Perbekalan#Kesehatan didalam Panti#Pemberian Bimbingan Fisik, Mental Spiritual &amp; Sosial#Pemberian Bimbingan Aktivitas Hidup Sehari-hari#Fasilitas Pembuatan Nomor Induk Penduduk#Akses ke Layanan Pendidikan &amp; Kesehatan Dasar#Pemberian Pelayanan Penelusuran keluarga#Pemberian Pelayanan Reunifikasi Keluarga</p>\n', '0'),
(2, 1, 'Provinsi', 'Rehabilitasi Sosial Dasar Anak Terlantar di dalam Panti Sosial', 'Jumlah anak telantar yang memperoleh   rehabilitasi sosial diluar panti', '<p>\n Pengasuhan Penyediaan Pemakaman#Penyediaan Sandang#Penyediaan Asrama yang mudah diakses#Penyediaan Perbekalan Kesehatan didalam Panti#Pemberian Bimbingan Fisik, Mental Spiritual &amp; Sosial#Pemberian Bimbingan Aktivitas Hidup Sehari-hari#Fasilitas Pembuatan Akte Kelahiran, Nomor Induk Penduduk, dan Kartu Identitas Anak#Akses ke Layanan Pendidikan &amp; Kesehatan Dasar#Pemberian Pelayanan Penelusuran keluarga#Pemberian Pelayanan Reunifikasi Keluarga#Akses Layanan Pengasuhan kepada Keluarga Pengganti</p>\n', '0'),
(3, 1, 'Provinsi', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di daam Panti Sosial', 'Jumlah   Warga   Negara lanjut usia terlantar yang memperoleh rehabilitasi sosial diluar panti', '<p>\n Penyediaan Pemakaman#Penyediaan Sandang#Penyediaan Asrama yang mudah diakses#Penyediaan Alat bantu#Penyediaan Perbekalan Kesehatan didalam Panti Sosial#Pemberian Bimbingan Fisik, Mental Spiritual &amp; Sosial#Pemberian Bimbingan Aktivitas Hidup Sehari-hari#Fasilitas Pembuatan Nomor Induk Penduduk#Akses ke Layanan Kesehatan Dasar#Pemberian Pelayanan Penelusuran keluarga#Pemberian Pelayanan Reunifikasi Keluarga#Pemulasaraan</p>\n', '0'),
(4, 1, 'Provinsi', 'Rehabilitasi Sosial Dasar Tuna Sosial Khususnya Gelandangan dan Pengemis di dalam Panti Sosial', 'Jumlah Warga Negara/ gelandangan   dan pengemis  yang memperoleh rehabilitasi sosialdasartuna sosial diluar panti', '<p>\n Penyediaan Pemakaman#Penyediaan Sandang#Penyediaan Asrama/Wisma yang mudah diakses#Penyediaan Perbekalan Kesehatan didalam Panti#Pemberian Bimbingan Fisik, Mental Spiritual &amp; Sosial#Pemberian Bimbingan Keterampilan Hidup Sehari-hari#Pemberian Bimbingan Keterampilan Dasar#Fasilitas Pembuatan Nomor Induk Kependuduk, Kartu Tanda Penduduk, Akta Kelahiran, Surat Nikah, dan/atau Kartu Identitas Anak#Akses ke Layanan Pendidikan &amp; Kesehatan Dasar#Pemulangan ke Daerah Asal</p>\n', '0'),
(5, 1, 'Provinsi', 'Perlindungan dan Jaminan Sosial pada Saat dan Setelah Tanggap Darurat Bencana bagi Korban Bencana daerah provinsi', 'Jumlah    Warga    Negara korban bencana kab/kota yang memperoleh perlindungan dan jaminan sosial', '<p>\n Penyediaan Pemakaman#Penyediaan Sandang#Peyediaan Tempat Penampungan Pengungsi#Penanganan Khusus Bagi Kelompok Rentan#Pelayanan Dukungan Psikososial</p>\n', '0'),
(6, 1, 'Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di Luar Panti Sosial', 'Jumlah Warga Negara penyandang disabilitas yang memperoleh rehabilitasi sosial diluar panti', '<p>\n Layanan Data dan Pengaduan#Layanan Kedaruratan#Penyediaan Pemakaman#Penyediaan Sandang#Penyediaan Alat bantu#Penyediaan Perbekalan Kesehatan#Pemberian Bimbingan Fisik, Mental Spiritual &amp; Sosial#Pemberian Bimbingan Sosial kepada Keluarga Penyandang Disabilitas Terlantar#Fasilitas Pembuatan Nomor Induk Kependuduk, Kartu Tanda Penduduk, Akta Kelahiran, Surat Nikah, dan/atau Kartu Identitas Anak#Akses ke Layanan Pendidikan &amp; Kesehatan Dasar#Pemberian Pelayanan Penelusuran keluarga#Pemberian Pelayanan Reunifikasi Keluarga#Layanan Rujukan</p>\n', '0'),
(7, 1, 'Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Anak Terlantar di Luar Panti Sosial', 'Jumlah anak telantar yang memperoleh rehabilitasi sosial diluar panti', '<p>\n Layanan Data dan Pengaduan#Layanan Kedaruratan#Penyediaan Pemakaman#Penyediaan Sandang#Penyediaan Alat bantu#Penyediaan Perbekalan Kesehatan#Pemberian Bimbingan Fisik, Mental Spiritual &amp; Sosial#Pemberian Bimbingan Sosial kepada Keluarga Penyandang Disabilitas Terlantar#Fasilitas Pembuatan Nomor Induk Kependuduk, Kartu Tanda Penduduk, Akta Kelahiran, Surat Nikah, dan/atau Kartu Identitas Anak#Akses ke Layanan Pendidikan &amp; Kesehatan Dasar#Pemberian Pelayanan Penelusuran keluarga#Pemberian Pelayanan Reunifikasi Keluarga#Layanan Rujukan</p>\n', '0'),
(8, 1, 'Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di Luar Panti Sosial', 'Jumlah Warga Negara lanjut usia terlantar yang memperoleh rehabilitasi sosial diluar panti', '<p>\n Layanan Data dan Pengaduan#Layanan Kedaruratan#Penyediaan Pemakaman#Penyediaan Sandang#Penyediaan Alat bantu#Penyediaan Perbekalan Kesehatan#Pemberian Bimbingan Fisik, Mental Spiritual &amp; Sosial#Pemberian Bimbingan Sosial kepada Keluarga Penyandang Disabilitas Terlantar#Fasilitas Pembuatan Nomor Induk Kependuduk, Kartu Tanda Penduduk, Akta Kelahiran, Surat Nikah, dan/atau Kartu Identitas Anak#Akses ke Layanan Pendidikan &amp; Kesehatan Dasar#Pemberian Pelayanan Penelusuran keluarga#Pemberian Pelayanan Reunifikasi Keluarga#Layanan Rujukan</p>\n', '0'),
(9, 1, 'Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Tuna Sosial Khususnya Gelandangan dan Pengemis di Luar Panti Sosial', 'Jumlah Warga Negara/ gelandangan dan pengemis yang memperoleh rehabilitasi sosial dasar tuna sosial diluar panti', '<p>\n Layanan Data dan Pengaduan#Layanan Kedaruratan#Penyediaan Pemakaman#Penyediaan Sandang#Penyediaan Alat bantu#Penyediaan Perbekalan Kesehatan#Pemberian Bimbingan Fisik, Mental Spiritual &amp; Sosial#Pemberian Bimbingan Sosial kepada Keluarga Penyandang Disabilitas Terlantar#Fasilitas Pembuatan Nomor Induk Kependuduk, Kartu Tanda Penduduk, Akta Kelahiran, Surat Nikah, dan/atau Kartu Identitas Anak#Akses ke Layanan Pendidikan &amp; Kesehatan Dasar#Pemberian Pelayanan Penelusuran keluarga#Pemberian Pelayanan Reunifikasi Keluarga#Layanan Rujukan</p>\n', '0'),
(10, 1, 'Kabupaten/Kota', 'Perlindungan dan Jaminan Sosial pada Saat dan Setelah Tanggap Darurat Bencana bagi Korban Bencana daerah Kabupaten/Kota', 'Jumlah Warga Negara korban bencana kab/kota yang memperoleh perlindungan dan jaminan sosial', '<p>\n Penyediaan Pemakaman#Penyediaan Sandang#Peyediaan Tempat Penampungan Pengungsi#Penanganan Khusus Bagi Kelompok Rentan#Pelayanan Dukungan Psikososial</p>\n', '0'),
(11, 2, 'Provinsi', 'pelayanan kesehatan bagi penduduk terdampak krisis kesehatan akibat bencana dan/atau berpotensi bencana provinsi', 'Jumlah Warga Negara yang terdampak krisis kesehatan akibat bencana dan/atau berpotensi bencana provinsi yang mendapatkan layanan kesehatan', '<p>\r\n Mendapatkan penyuluhan pra-bencana / pra-krisis terkait bencana aspek kesehatan (termasuk PHBS)#</p>\r\n<p>\r\n Pelayanan krisis kesehatan saat bencana, meliputi:</p>\r\n<p>\r\n a) layanan medis dasar dan layanan rujukan bila diperlukan;</p>\r\n<p>\r\n b) mendapatkan layanan pencegahan penyakit menular dan penyehatan lingkungan;</p>\r\n<p>\r\n c) mendapatkan layanan gizi darurat / makanan tambahan untuk bayi, ibu hamil dan anak;</p>\r\n<p>\r\n d) mendapatkan layanan kesehatan reproduksi darurat;</p>\r\n<p>\r\n e) mendapatkan layanan dukungan kesehatan jiwa dan psikososial;</p>\r\n<p>\r\n f) mendapatkan penyuluhan kesehatan</p>\r\n', '0'),
(13, 2, 'Provinsi', 'pelayanan kesehatan bagi penduduk pada kondisi kejadian luar biasa provinsi', 'Jumlah Warga Negara pada kondisi kejadian luar biasa provinsi yang mendapatkan layanan kesehatan', '<p>\r\n Mendapatkan pencatatan untuk pelayanan KLB serta penyuluhan (edukasi) terkait KLB#</p>\r\n<p>\r\n Pelayanan kesehatan kondisi KLB, meliputi:</p>\r\n<p>\r\n a) Penemuan kasus dan identifikasi faktor risiko melalui penyelidikan epidemiologis</p>\r\n<p>\r\n b) Penatalaksanaan penderita pada kasus konfirmasi, probable dan suspek yang mencakup kegiatan pemeriksaan, pengobatan, perawatan dan isolasi penderita, termasuk tindakan karantina sesuai standar yang telah ditetapkan</p>\r\n<p>\r\n c) Penyuluhan kesehatan</p>\r\n<p>\r\n d) Pencegahan dan pengebalan sesuai dengan jenis penyakit</p>\r\n<p>\r\n e) Penanganan jenazah, jika diperlukan</p>\r\n<p>\r\n f) Pemusnahan penyebab penyakit, jika diperlukan</p>\r\n<p>\r\n g) Upaya penanggulangan kesehatan masyarakat lainnya, jika diperlukan antara lain meliburkan sekolah dan/atau menutup fasilitas umum untuk sementara waktu; h) perujukan ke fasyankes rujukan</p>\r\n', '0'),
(14, 2, 'Kabupaten/Kota', 'Pelayanan kesehatan ibu hamil', 'Jumlah Ibu Hamil yang mendapatkan layanan kesehatan', '<p>\r\n Mendapatkan vaksin tetanus, tablet tambah darah, test dan pemeriksaan lab standar, kartu KIA, buku KIA)#Satu kali pelayanan pada trimester pertama (K1)#Satu kali pelayanan pada trimester kedua (K2)#Dua kali pelayanan pada trimester ketiga (K3), serta perujukan (bagi ibu hamil dengan komplikasi)</p>\r\n', '0'),
(15, 2, 'Kabupaten/Kota', 'Pelayanan kesehatan ibu bersalin', 'Jumlah Ibu Bersalin yang mendapatkan layanan kesehatan', '<p>\r\n Mendapatkan pencatatan dengan formulir partograf, kartu KIA, buku KIA#Mendapatkan asuhan layanan persalinan normal yang bersih dan aman selama persalinan dan setelah bayi lahir serta upaya pencegahan komplikasi&nbsp; infeksi#Mendapatkan asuhan proses pengeluaran janin yang terjadi pada kehamilan cukup bulan (37 &ndash; 42 minggu), lahir spontan dengan presentasi belakang kepala yang berlangsung dalam 18 jam, tanpa komplikasi, baik pada Ibu maupun pada janin, serta perujukan (bagi ibu bersalin dengan komplikasi)</p>\r\n', '0'),
(16, 2, 'Kabupaten/Kota', 'Pelayanan kesehatan bayi baru lahir', 'Jumlah Bayi Baru Lahir yang mendapatkan layanan kesehatan', '<p>\r\n Mendapatkan pelayanan standar 1 (0-6 jam kelahiran) meliputi: vaksin Hepatitis B0, Vitamin K1 injeksi, Salep / tetes mata (antibiotic), pemotongan dan perawatan tali pusar, inisiasi menyusu dini, pemeriksaan dan pencatatan dengan formulir bayi baru lahir, formulir MTBM dan Buku KIA, serta perujukan (bagi bayi dengan komplikasi)#Mendapatkan pelayanan standar 2 (6 jam &ndash; 28 hari kelahiran) meliputi: konseling ASI eksklusif &amp; perawatan bayi, vaksin Hepatitis B (injeksi) bagi yang belum, Vitamin K1 injeksi (bagi yang belum), , pemeriksaan dan pencatatan dengan formulir bayi baru lahir, formulir MTBM dan Buku KIA#Kunjungan Neonatal 1 (6 &ndash; 48 jam)#Kunjungan Neonatal 2 (3-7 hari)#Kunjungan Neonatal 3 (8 - 28 hari)</p>\r\n', '0'),
(17, 2, 'Kabupaten/Kota', 'Pelayanan kesehatan balita', 'Jumlah Balita yang mendapatkan layanan kesehatan', '<p>\r\n Mendapatkan pencatatan pada KPSP dan DDTK serta buku KIA# Pelayanan balita usia 0-11 bulan (standar 1); meliputi: a) Penimbangan minimal 8 kali setahun; b)&nbsp; Pengukuran panjang/tinggi badan minimal 2 kali ?/tahun; c) Pemantauan perkembangan minimal 2 kali/tahun;d) Pemberian kapsul vitamin A pada usia 6-11 bulan 1 kali setahun; e) Pemberian imunisasi dasar lengkap; f) Pemberian Edukasi dan informasi #Pelayanan balita usia 12-35 bulan (standar 2); meliputi: a) Penimbangan minimal 8 kali setahun; b) Pengukuran panjang/tinggi badan minimal 2 kali/tahun; c)&nbsp;&nbsp;&nbsp;&nbsp; Pemantauan perkembangan minimal 2 kali/ tahun; d)&nbsp;&nbsp;&nbsp; Pemberian kapsul vitamin A sebanyak 2 kali setahun; e) Pemberian Imunisasi Lanjutan; f) Pemberian Edukasi dan informasi# Pelayanan balita usia 36-59 bulan (standar 3); meliputi: Penimbangan minimal 8 kali; Pengukuran panjang/tinggi badan minimal 2 ?kali/tahun. ?Pemantauan perkembangan minimal 2 kali/ tahun. Pemberian kapsul vitamin A&nbsp; sebanyak 2 kali setahun.Pemantauan perkembangan balita. Pemberian kapsul vitamin A. Pemberian imunisasi dasar lengkap. Pemberian imunisasi lanjutan. Pengukuran berat badan dan panjang/tinggi badan. Pemberian Edukasi dan informasi.</p>\r\n', '0'),
(18, 2, 'Kabupaten/Kota', 'Pelayanan kesehatan pada usia pendidikan dasar', 'Jumlah Warga Negara usia pendidikan dasar yang mendapatkan layanan kesehatan', '<p>\r\n Mendapatkan pencatatan pada Buku Rapor Kesehatanku, Buku Pemantauan Kesehatan, Kuisioner Skrining Kesehatan, Formulir Rekapitulasi Hasil Pelayanan kesehatan usia sekolah dan remaja di dalam sekolah / di luar Sekolah#Pelayanan Kesehatan pada Usia Pendidikan Dasar umur 7 tahun meliputi: Penilaian status gizi (tinggi badan, berat badan, tanda klinis anemia); dan&nbsp; Penilaian tanda vital (tekanan darah, frekuensi nadi dan napas); dan Penilaian kesehatan gigi dan mulut; dan Penilaian ketajaman indera penglihatan dengan poster snellen; serta Mendapatkan umpan balik edukasi dan di rujuk (jika dibutuhkan)#Pelayanan Kesehatan pada Usia Pendidikan Dasar umur 7 tahun meliputi: Penilaian status gizi (tinggi badan, berat badan, tanda klinis anemia); dan Penilaian tanda vital (tekanan darah, frekuensi nadi dan napas); dan Penilaian kesehatan gigi dan mulut; dan Penilaian ketajaman indera penglihatan dengan poster snellen; serta Mendapatkan umpan balik edukasi dan di rujuk (jika dibutuhkan).</p>\r\n<p>\r\n &nbsp;</p>\r\n', '0'),
(19, 2, 'Kabupaten/Kota', 'Pelayanan kesehatan pada usia produktif', 'Jumlah Warga Negara usia produktif yang mendapatkan layanan kesehatan', '<p>\r\n Mendapatkan pencatatan sesuai aplikasi Penyakit Tidak Menular (PTM), serta mendapatkan materi KIE (edukasi)#</p>\r\n<p>\r\n Pelayanan Kesehatan pada Usia Pendidikan Dasar umur 7 tahun meliputi:</p>\r\n<ol>\r\n <li>\r\n  Deteksi Obesitas dengan cara penimbangan berat badan dan pengukuran tinggi badan (penilaian Indeks Massa Tubuh) dan pengukuran lingkar perut</li>\r\n <li>\r\n  Deteksi Hipertensi, yang dilakukan dengan memeriksa tekanan darah (tensimeter), &nbsp;dan</li>\r\n <li>\r\n  Deteksi Diabetes Melitus, yang dilakukan dengan pemeriksaan gula darah puasa atau sewaktu (glucometer);</li>\r\n <li>\r\n  Pelayanan tindaklanjut hasil skrining : (1) merujuk jika diperlukan, dan (2) pemberian penyuluhan</li>\r\n</ol>\r\n', '0'),
(20, 2, 'Kabupaten/Kota', 'Pelayanan kesehatan pada usia lanjut', 'warga negara usia lanjut yang mendapatkan layanan kesehatan', '<div>\r\n Mendapatkan pencatatan sesuai Buku Kesehatan Lansia dan mendapatkan edukasi PHBS (Pola Hidup Bersih Sehat)#Pelayanan Kesehatan pada Usia Lanjut, meliputi: Pengukuran tinggi badan, berat badan, dan lingkar perut;Deteksi Hipertensi, yang dilakukan dengan mengukur tekanan darah; Deteksi kemungkinan Diabetes Melitus dengan menggunakan tes cepat gula darah;Deteksi gangguan mental;Deteksi gangguan kognitif; Pemeriksaan tingkat kemandirian usia lanjut;Anamnesa perilaku nerisiko</div>\r\n<p>\r\n &nbsp;</p>\r\n', 'org'),
(21, 2, 'Kabupaten/Kota', 'Pelayanan kesehatan penderita hipertensi', 'Jumlah Warga Negara penderita hipertensi yang mendapatkan layanan kesehatan', '<p>\r\n Mendapatkan pencatatan sesuai SIM PTM dan mendapatkan materi edukasi PTM#Pelayanan Kesehatan pada Penderita Hipertensi, meliputi Pengukuran tekanan darah; dilakukan minimal setiap bulan satu kali, di fasyankes; Edukasi perubahan gaya hidup, dan kepatuhan minum obat.Terapi Farmakologi, ketika ditemukan hasil pemeriksaan Tekanan Darah Sewaktu (TDS) lebih dari 140 mmHg</p>\r\n', '0'),
(22, 2, 'Kabupaten/Kota', 'Pelayanan kesehatan penderita diabetes melitus(DM)', 'Jumlah Warga Negara penderita diabetes mellitus yang mendapatkan layanan kesehatan', '<div>\r\n Mendapatkan pencatatan sesuai SIM PTM dan mendapatkan materi edukasi PTM#Pelayanan Kesehatan pada Penderita DM, meliputi:Pengukuran gula darah sewaktu (GDS); dilakukan setiap bulan satu kali;Edukasi perubahan gaya hidup dan atau nutrisi Terapi Farmakologi, ketika ditemukan hasil pemeriksaan GDS lebih dari 200 mg/dl.</div>\r\n<p>\r\n &nbsp;</p>\r\n', '0'),
(23, 2, 'Kabupaten/Kota', 'Pelayanan Kesehatan Orang Dengan Ganguan Jiwa (ODGJ) Berat ', 'Jumlah Warga Negara dengan gangguan jiwa berat yang terlayani kesehatan', '<div>\r\n Mendapatkan pencatatan sesuai formulir dan mendapatkan materi edukasi gangguan jiwa#Pelayanan Kesehatan pada ODGJ Berat, meliputi:<br />\r\n <ol>\r\n  <li>\r\n   pemeriksaan kesehatan jiwa, mencakup :pemeriksaan status mental, dan Wawancara;</li>\r\n  <li>\r\n   edukasi kepatuhan minum obat</li>\r\n  <li>\r\n   rujukan (jika diperlukan)</li>\r\n </ol>\r\n</div>\r\n<p>\r\n &nbsp;</p>\r\n', '0'),
(24, 2, 'Kabupaten/Kota', 'Pelayanan Kesehatan Orang Terduga Tuberkulosis', 'Jumlah Warga Negara terduga tuberculosis yang mendapatkan layanan kesehatan', '<div>\r\n Mendapatkan pencatatan sesuai formulir dan mendapatkan materi edukasi Tuberkulosis#Pelayanan Kesehatan pada Orang terduga TB, meliputi:<br />\r\n <ol>\r\n  <li>\r\n   Pemeriksaan Klinis, mencakup pemeriksaan gejala dan tanda;</li>\r\n  <li>\r\n   Pemeriksaan penunjang, mencakup pemeriksaan dahak dan atau bakteriologis dan atau radiologis;</li>\r\n  <li>\r\n   Edukasi perilaku berisiko dan pencegahan penularan;</li>\r\n </ol>\r\n pelayanan merujuk orang yang sudah positif Tuberkulosis untuk memperoleh pengobatan Anti Tuberkulosis dan Pemantauan Pengobatan</div>\r\n<p>\r\n &nbsp;</p>\r\n', '0'),
(25, 2, 'Kabupaten/Kota', 'Pelayanan kesehatan orang dengan risiko terinfeksi virus yang melemahkan daya tahan tubuh manusia (Human Immunodeficiency Virus)', 'Jumlah Warga Negara dengan risiko terinfeksi virus yang melemahkan daya tahan tubuh manusia (Human Immunodeficiency Virus) yang mendapatkan layanan kesehatan', '<div>\r\n Mendapatkan pencatatan sesuai formulir dan mendapatkan materi edukasi HIV-AIDS#Pelayanan Kesehatan pada Orang dengan resiko terinfeksi HIV mendapatkan skrining, dengan pemeriksaan tes cepat&nbsp; HIV, minimal satu kali dalam satu tahun.#Capaian Kinerja (otomatis dihitung oleh SIM)</div>\r\n', '0'),
(26, 5, 'Provinsi', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan menengah merupakan Peserta Didik yang berusia 16 (enam belas) tahun sampai dengan 18 (delapan belas) tahun', 'menghitung jumlah anak usia 16 (enam belas) sampai dengan 18 (delapan belas) tahun pada provinsi yang bersangkutan', '<div>\n Buku teks pelajaran#perlengkapan belajar</div>\n', '0'),
(28, 5, 'Provinsi', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan khusus merupakan Peserta Didik penyandang disabilitas yang berusia 4 (empat) tahun sampai dengan 18 (delapan  belas) tahun', 'menghitung jumlah anak usia 4 (empat) sampai dengan 18 (delapan belas) tahun pada provinsi yang bersangkutan', '<p>\n materi ajar sesuai dengan ragam disabilitas#perlengkapan belajar</p>\n', '0'),
(30, 5, 'Kabupaten/Kota', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan anak usia dini merupakan Peserta Didik yang berusia 5 (lima) tahun sampai dengan 6 (enam) tahun', 'menghitung jumlah anak usia 5 (lima) sampai dengan 6 (enam) tahun pada kabupaten/kota yang bersangkutan', '<p>\n Buku gambar#Alat Warna</p>\n', '0'),
(32, 5, 'Kabupaten/Kota', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan dasar merupakan Peserta Didik yang berusia 7 (tujuh) tahun sampai dengan 15 (lima belas) tahun', 'menghitung jumlah anak usia 7 (tujuh) sampai dengan 12 (dua belas) tahun pada kabupaten/kota yang bersangkutan', '<p>\n buku teks pelajaran#perlengkapan belajar</p>\n', '0'),
(34, 5, 'Kabupaten/Kota', 'Penerima Pelayanan Dasar SPM Pendidikan pada pendidikan kesetaraan merupakan Peserta Didik yang berusia 7 (tujuh) tahun sampai dengan 18 (delapan belas) tahun', 'menghitung jumlah anak usia 7 (tujuh) sampai dengan 18 (delapan belas) tahun pada kabupaten/kota yang bersangkutan', '<p>\n modul belajar#perlengkapan belajar</p>\n', '0'),
(36, 3, 'Provinsi', 'Penyediaan dan rehabilitasi rumah yang layak huni bagi korban bencana provinsi', 'Jumlah Warga Negara korban bencana yang memperoleh rumah layak huni', '<div>\n Identifikasi Perumahan di lokasi rawan bencana#Identifikasi lahan-lahan potensial sebagai lokasi relokasi perumahan#Data rumah korban bencana alam kejadian sebelumnya yang belum tertangani#Pendataan Tingkat&nbsp; Kerusakan Rumah#Verifikasi penerima layanan SPM#Penyusunan Rencana Aksi</div>\n<div>\n &nbsp;</div>\n', '0'),
(38, 3, 'Provinsi', 'Fasilitasi penyediaan rumah yang layak huni bagi masyarakat yang terkena relokasi program Pemerintah Daerah provinsi', 'Jumlah Warga Negara yang terkena relokasi akibat program Pemerintah Daerah provinsi yang memperoleh fasilitasi penyediaan rumah yang layak huni', '<div>\n Pendataan perumahan di lokasi pengembangan Kawasan Strategis Provinsi (KSP)#Pendataanperumahan di lokasi kumuh provinsi (10-15 Ha)#Identifikasi rencana pengembangan perumahanperumahan baru</div>\n<div>\n &nbsp;</div>\n<div>\n &nbsp;</div>\n<div>\n &nbsp;</div>\n', '0'),
(39, 3, 'Kabupaten/Kota', 'Penyediaan dan rehabilitasi rumah yang layak huni bagi korban bencana kabupaten/kota', 'Jumlah Warga Negara korban bencana yang memperoleh rumah layak huni', '<div>\n Identifikasi Perumahan di lokasi rawan bencana#Identifikasi lahan-lahan potensial sebagai lokasi relokasi perumahan#Data rumah korban bencana alam kejadian sebelumnya yang belum tertangani#Pendataan Tingkat&nbsp; Kerusakan Rumah#Verifikasi penerima layanan SPM#Penyusunan Rencana Aksi</div>\n<div>\n &nbsp;</div>\n', '0'),
(40, 3, 'Kabupaten/Kota', 'Fasilitasi penyediaan rumah yang layak huni bagi masyarakat yang terkena relokasi program Pemerintah Daerah kabupaten/kota', 'Jumlah Warga Negara yang terkena relokasi akibat program Pemerintah Daerah kabupaten/kota yang memperoleh fasilitasi penyediaan rumah yang layak huni', '<div>\n Pendataan perumahan di lokasi yang berpotensi dapat menimbulkan bahaya#Pendataan perumahan di atas lahan bukan fungsi permukiman#Pendataan rumah sewa milik masyarakat, Rumah Susun Umum dan/atau Rumah Khusus eksisting</div>\n<div>\n &nbsp;</div>\n<div>\n &nbsp;</div>\n<div>\n &nbsp;</div>\n', '0'),
(41, 6, 'Provinsi', 'Pemenuhan kebutuhan air minum curah lintas kabupaten/kota', 'Jumlah Warga Negara yang memperoleh kebutuhan air minum curah lintas kabupaten/kota', '<p>\n ukuran kuantitas air minum#kualitas air minum</p>\n', '0'),
(42, 6, 'Provinsi', 'Penyediaan pelayanan pengolahan air limbah domestik regional lintas kabupaten/ kota', 'Jumlah Warga Negara yang memperoleh layanan pengolahan air limbah domestik regional lintas kabupaten/kota', '<p>\n Kuantitas Pelayanan Air Limbah Domestik#Kualitas Pelayanan Air Limbah Domestik</p>\n', '0'),
(43, 6, 'Kabupaten/Kota', 'Penyediaan Kebutuhan pokok air minum sehari-hari', 'Jumlah Warga Negara yang memperoleh kebutuhan pokok air minum sehari-hari', '<p>\n ukuran kuantitas air minum#kualitas air minum</p>\n', '0'),
(44, 6, 'Kabupaten/Kota', 'Penyediaan Pelayanan Pengolahan air limbah domestik', 'Jumlah Warga Negara yang memperoleh layanan pengolahan air limbah domestik', '<p>\n Kuantitas Pelayanan Air Limbah Domestik#Kualitas Pelayanan Air Limbah Domestik</p>\n', '0'),
(45, 4, 'Provinsi', 'Pelayanan ketentraman dan ketertiban Umum Provinsi', 'Jumlah Warga Negara yang memperoleh layanan akibat dari penegakan hukum perda dan perkada di Provinsi', '<div>\n pengumpulan data#penghitungan kebutuhan pemenuhan Pelayanan Dasar#penyusunan rencana pemenuhan Pelayanan Dasar#pelaksanaan pemenuhan Pelayanan Dasar#pelayanan kerugian materil#pelayanan pengobatan</div>\n', '0'),
(46, 4, 'Kabupaten/Kota', 'Pelayanan ketentraman dan ketertiban Umum', 'Jumlah Warga Negara yang memperoleh layanan akibat dari penegakan hukum Perda dan perkada', '<div>\n pengumpulan data#penghitungan kebutuhan pemenuhan Pelayanan Dasar#penyusunan rencana pemenuhan Pelayanan Dasar#pelaksanaan pemenuhan Pelayanan Dasar#pelayanan kerugian materil#pelayanan pengobatan</div>\n', '0'),
(47, 4, 'Kabupaten/Kota', 'Pelayanan informasi rawan bencana', 'Jumlah Warga Negara yang memperoleh layanan informasi rawan bencana', '<div>\n pengumpulan data#penghitungan kebutuhan pemenuhan Pelayanan Dasar#penyusunan rencana pemenuhan Pelayanan Dasar#pelaksanaan pemenuhan Pelayanan Dasar#pelayanan kerugian materil#pelayanan pengobatan</div>\n', '0'),
(48, 4, 'Kabupaten/Kota', 'Pelayanan pencegahan dan kesiapsiagaan terhadap bencana', 'Jumlah Warga Negara yang memperoleh layanan pencegahan dan kesiapsiagaan terhadap bencana', '<div>\n pengumpulan data#penghitungan kebutuhan pemenuhan Pelayanan Dasar#penyusunan rencana pemenuhan Pelayanan Dasar#pelaksanaan pemenuhan Pelayanan Dasar#pelayanan kerugian materil#pelayanan pengobatan</div>\n', '0'),
(49, 4, 'Kabupaten/Kota', 'Pelayanan penyelamatan dan evakuasi korban bencana', 'Jumlah Warga Negara yang memperoleh layanan penyelamatan dan evakuasi korban bencana', '<div>\n pengumpulan data#penghitungan kebutuhan pemenuhan Pelayanan Dasar#penyusunan rencana pemenuhan Pelayanan Dasar#pelaksanaan pemenuhan Pelayanan Dasar#pelayanan kerugian materil#pelayanan pengobatan</div>\n', '0'),
(50, 0, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_master_masalah`
--

CREATE TABLE `tbl_master_masalah` (
  `masalah_id` int(11) NOT NULL,
  `Jenis_masalah` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_master_masalah`
--

INSERT INTO `tbl_master_masalah` (`masalah_id`, `Jenis_masalah`) VALUES
(1, 'Pengumpulan Data'),
(2, 'Penghitungan Kebutuhan'),
(3, 'Perencanaan'),
(4, 'Pelaporan'),
(5, 'Lain-Lain');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pungli`
--

CREATE TABLE `tbl_pungli` (
  `id` int(11) NOT NULL,
  `tgl_masuk` datetime NOT NULL,
  `tgl_lapor` datetime NOT NULL,
  `kegiatan` varchar(50) NOT NULL,
  `skpd` varchar(50) NOT NULL,
  `pelaku` varchar(50) NOT NULL,
  `nilai` int(200) NOT NULL,
  `st` int(2) NOT NULL,
  `areaid` int(2) NOT NULL,
  `uname` varchar(8) NOT NULL,
  `modus` varchar(50) NOT NULL,
  `deptid` varchar(20) NOT NULL,
  `propinsi` varchar(200) NOT NULL,
  `kabupaten` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_setting`
--

CREATE TABLE `tbl_setting` (
  `id` bigint(20) NOT NULL,
  `opsi_key` varchar(255) NOT NULL,
  `opsi_val` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_setting`
--

INSERT INTO `tbl_setting` (`id`, `opsi_key`, `opsi_val`) VALUES
(1, 'nama_lembaga', 'DITJEN BINA BANGDA'),
(2, 'nama_ketua', 'asdsdsa'),
(3, 'hp_ketua', '0'),
(4, 'alamat', 'Jl. Taman Makam Pahlawan No. 20 Kalibata, Jakarta Selatan'),
(5, 'telepon', '(021) 34846391'),
(6, 'kota', 'Jakarta Pusat'),
(7, 'email', ' info@bangda.kemendagri.go.id '),
(8, 'web', 'http://bangda.kemendagri.go.id/');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `u_name` varchar(11) NOT NULL,
  `pass_word` varchar(200) NOT NULL,
  `aktif` enum('Y','N') NOT NULL,
  `Keterangan` varchar(250) NOT NULL,
  `id_prov` int(11) NOT NULL,
  `id_kabkot` int(11) NOT NULL,
  `level` enum('admin','ADMINKAB','ADMINPROV','BANGDA','SUBDIT','PENGGUNA') NOT NULL,
  `dinas_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`u_name`, `pass_word`, `aktif`, `Keterangan`, `id_prov`, `id_kabkot`, `level`, `dinas_id`) VALUES
('1100', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'ACEH', 11, 0, 'ADMINPROV', 0),
('1100P', '1e157dd5081c6699192c94068932336f5c00ebf5', 'Y', 'ACEH', 11, 0, 'PENGGUNA', 5),
('1100S', '1e157dd5081c6699192c94068932336f5c00ebf5', 'Y', 'ACEH', 11, 0, 'PENGGUNA', 1),
('1101', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'ACEH SELATAN', 11, 1101, 'ADMINKAB', 0),
('1101P', '', 'Y', 'ACEH SELATAN', 0, 1101, 'PENGGUNA', 5),
('1102', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'ACEH TENGGARA', 11, 1102, 'ADMINKAB', 0),
('1103', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'ACEH TIMUR', 11, 1103, 'ADMINKAB', 0),
('1104', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'ACEH TENGAH', 11, 1104, 'ADMINKAB', 0),
('1105', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'ACEH BARAT 2', 11, 1105, 'ADMINKAB', 0),
('1106', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'ACEH BESAR', 11, 1106, 'ADMINKAB', 0),
('1107', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PIDIE', 11, 1107, 'ADMINKAB', 0),
('1108', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'ACEH UTARA', 11, 1108, 'ADMINKAB', 0),
('1109', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SIMEULUE', 11, 1109, 'ADMINKAB', 0),
('1110', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'ACEH SINGKIL', 11, 1110, 'ADMINKAB', 0),
('1111', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BIREUEN', 11, 1111, 'ADMINKAB', 0),
('1112', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'ACEH BARAT DAYA', 11, 1112, 'ADMINKAB', 0),
('1113', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'GAYO LUES', 11, 1113, 'ADMINKAB', 0),
('1114', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'ACEH JAYA', 11, 1114, 'ADMINKAB', 0),
('1115', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'NAGAN RAYA', 11, 1115, 'ADMINKAB', 0),
('1116', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'ACEH TAMIANG', 11, 1116, 'ADMINKAB', 0),
('1117', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BENER MERIAH', 11, 1117, 'ADMINKAB', 0),
('1118', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PIDIE JAYA', 11, 1118, 'ADMINKAB', 0),
('1171', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA BANDA ACEH', 11, 1171, 'ADMINKAB', 0),
('1172', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA SABANG', 11, 1172, 'ADMINKAB', 0),
('1173', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA LHOKSEUMAWE', 11, 1173, 'ADMINKAB', 0),
('1174', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA LANGSA', 11, 1174, 'ADMINKAB', 0),
('1175', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SUBULUSSALAM', 11, 1175, 'ADMINKAB', 0),
('1200', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SUMATERA UTARA', 12, 0, 'ADMINPROV', 0),
('1201', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TAPANULI TENGAH', 12, 1201, 'ADMINKAB', 0),
('1202', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TAPANULI UTARA', 12, 1202, 'ADMINKAB', 0),
('1203', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TAPANULI SELATAN', 12, 1203, 'ADMINKAB', 0),
('1204', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'NIAS', 12, 1204, 'ADMINKAB', 0),
('1205', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LANGKAT', 12, 1205, 'ADMINKAB', 0),
('1206', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KARO', 12, 1206, 'ADMINKAB', 0),
('1207', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'DELI SERDANG', 12, 1207, 'ADMINKAB', 0),
('1208', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SIMALUNGUN', 12, 1208, 'ADMINKAB', 0),
('1209', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'ASAHAN', 12, 1209, 'ADMINKAB', 0),
('1210', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LABUAN BATU', 12, 1210, 'ADMINKAB', 0),
('1211', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'DAIRI', 12, 1211, 'ADMINKAB', 0),
('1212', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TOBA SAMOSIR', 12, 1212, 'ADMINKAB', 0),
('1213', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MANDAILING NATAL', 12, 1213, 'ADMINKAB', 0),
('1214', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'NIAS SELATAN', 12, 1214, 'ADMINKAB', 0),
('1215', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PAKPAK BHARAT', 12, 1215, 'ADMINKAB', 0),
('1216', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'HUMBANG HASUNDUTAN', 12, 1216, 'ADMINKAB', 0),
('1217', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SAMOSIR', 12, 1217, 'ADMINKAB', 0),
('1218', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SERDANG BEDAGAI', 12, 1218, 'ADMINKAB', 0),
('1219', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BATU BARA', 12, 1219, 'ADMINKAB', 0),
('1220', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PADANG LAWAS UTARA', 12, 1220, 'ADMINKAB', 0),
('1221', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PADANG LAWAS', 12, 1221, 'ADMINKAB', 0),
('1222', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LABUAN BATU SELATAN', 12, 1222, 'ADMINKAB', 0),
('1223', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LABUAN BATU UTARA', 12, 1223, 'ADMINKAB', 0),
('1224', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'NIAS UTARA', 12, 1224, 'ADMINKAB', 0),
('1225', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'NIAS BARAT', 12, 1225, 'ADMINKAB', 0),
('1271', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA MEDAN', 12, 1271, 'ADMINKAB', 0),
('1272', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA PEMATANG SIANTAR', 12, 1272, 'ADMINKAB', 0),
('1273', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA SIBOLGA', 12, 1273, 'ADMINKAB', 0),
('1274', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA TANJUNG BALAI', 12, 1274, 'ADMINKAB', 0),
('1275', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA BINJAI', 12, 1275, 'ADMINKAB', 0),
('1276', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA TEBING TINGGI', 12, 1276, 'ADMINKAB', 0),
('1277', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA PADANG SIDEMPUAN', 12, 1277, 'ADMINKAB', 0),
('1278', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA GUNUNGSITOLI', 12, 1278, 'ADMINKAB', 0),
('1300', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SUMATERA BARAT', 13, 0, 'ADMINPROV', 0),
('1300P', '', 'Y', 'SUMATERA BARAT', 13, 0, 'PENGGUNA', 2),
('1301', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PESISIR SELATAN', 13, 1301, 'ADMINKAB', 0),
('1302', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SOLOK', 13, 1302, 'ADMINKAB', 0),
('1303', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SAWAHLUNTOH-SIJUNJUNG', 13, 1303, 'ADMINKAB', 0),
('1304', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TANAH DATAR', 13, 1304, 'ADMINKAB', 0),
('1305', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PADANG PARIAMAN', 13, 1305, 'ADMINKAB', 0),
('1306', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'AGAM', 13, 1306, 'ADMINKAB', 0),
('1307', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LIMA PULUH KOTO', 13, 1307, 'ADMINKAB', 0),
('1308', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PASAMAN', 13, 1308, 'ADMINKAB', 0),
('1309', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KEPULAUAN MENTAWAI', 13, 1309, 'ADMINKAB', 0),
('1310', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'DHARMASRAYA', 13, 1310, 'ADMINKAB', 0),
('1311', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SOLOK SELATAN', 13, 1311, 'ADMINKAB', 0),
('1312', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PASAMAN BARAT', 13, 1312, 'ADMINKAB', 0),
('1371', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA PADANG', 13, 1371, 'ADMINKAB', 0),
('1372', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA SOLOK', 13, 1372, 'ADMINKAB', 0),
('1373', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA SAWAH LUNTO', 13, 1373, 'ADMINKAB', 0),
('1374', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA PADANG PANJANG', 13, 1374, 'ADMINKAB', 0),
('1375', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA BUKIT TINGGI', 13, 1375, 'ADMINKAB', 0),
('1376', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA PAYAKUMBUH', 13, 1376, 'ADMINKAB', 0),
('1377', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA PARIAMAN', 13, 1377, 'ADMINKAB', 0),
('1400', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'RIAU', 14, 0, 'ADMINPROV', 0),
('1401', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KAMPAR', 14, 1401, 'ADMINKAB', 0),
('1402', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'INDRAGIRI HULU', 14, 1402, 'ADMINKAB', 0),
('1403', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BENGKALIS', 14, 1403, 'ADMINKAB', 0),
('1404', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'INDRAGIRI HILIR', 14, 1404, 'ADMINKAB', 0),
('1405', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PELALAWAN', 14, 1405, 'ADMINKAB', 0),
('1406', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'ROKAN HULU', 14, 1406, 'ADMINKAB', 0),
('1407', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'ROKAN HILIR', 14, 1407, 'ADMINKAB', 0),
('1408', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SIAK', 14, 1408, 'ADMINKAB', 0),
('1409', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KUANTAN SINGINGI', 14, 1409, 'ADMINKAB', 0),
('1410', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PULAU MERANTI', 14, 1410, 'ADMINKAB', 0),
('1471', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA PEKANBARU', 14, 1471, 'ADMINKAB', 0),
('1472', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA DUMAI', 14, 1472, 'ADMINKAB', 0),
('1500', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'JAMBI', 15, 1500, 'ADMINKAB', 0),
('1501', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KERINCI', 15, 1501, 'ADMINKAB', 0),
('1502', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MERANGIN', 15, 1502, 'ADMINKAB', 0),
('1503', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SAROLANGUN', 15, 1503, 'ADMINKAB', 0),
('1504', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BATANG HARI', 15, 1504, 'ADMINKAB', 0),
('1505', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MUARO JAMBI', 15, 1505, 'ADMINKAB', 0),
('1506', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TANJUNG JABUNG BARAT', 15, 1506, 'ADMINKAB', 0),
('1507', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TANJUNG JABUNG TIMUR', 15, 1507, 'ADMINKAB', 0),
('1508', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BUNGO', 15, 1508, 'ADMINKAB', 0),
('1509', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TEBO', 15, 1509, 'ADMINKAB', 0),
('1571', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA JAMBI', 15, 1571, 'ADMINKAB', 0),
('1572', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA SUNGAI PENUH', 15, 1572, 'ADMINKAB', 0),
('1600', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SUMATERA SELATAN', 16, 1600, 'ADMINKAB', 0),
('1601', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'OGAN KOMERING ULU', 16, 1601, 'ADMINKAB', 0),
('1602', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'OGAN KOMERING ILIR', 16, 1602, 'ADMINKAB', 0),
('1603', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MUARA ENIM', 16, 1603, 'ADMINKAB', 0),
('1604', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LAHAT', 16, 1604, 'ADMINKAB', 0),
('1605', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MUSI RAWAS', 16, 1605, 'ADMINKAB', 0),
('1606', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MUSI BANYUASIN', 16, 1606, 'ADMINKAB', 0),
('1607', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BANYUASIN', 16, 1607, 'ADMINKAB', 0),
('1608', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'OGAN KOMERING ULU TIMUR', 16, 1608, 'ADMINKAB', 0),
('1609', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'OGAN KOMERING ULU SELATAN', 16, 1609, 'ADMINKAB', 0),
('1610', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'OGAN ILIR', 16, 1610, 'ADMINKAB', 0),
('1611', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'EMPAT LAWANG', 16, 1611, 'ADMINKAB', 0),
('1612', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PENUKAL ABAB LEMATANG ILIR', 16, 1612, 'ADMINKAB', 0),
('1613', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MUSI RAWAS UTARA', 16, 1613, 'ADMINKAB', 0),
('1671', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA PALEMBANG', 16, 1671, 'ADMINKAB', 0),
('1672', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA PAGAR ALAM', 16, 1672, 'ADMINKAB', 0),
('1673', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA LUBUK LINGGAU', 16, 1673, 'ADMINKAB', 0),
('1674', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA PRABUMULIH', 16, 1674, 'ADMINKAB', 0),
('1700', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BENGKULU', 17, 1700, 'ADMINKAB', 0),
('1701', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BENGKULU SELATAN', 17, 1701, 'ADMINKAB', 0),
('1702', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'REJANG LEBONG', 17, 1702, 'ADMINKAB', 0),
('1703', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BENGKULU UTARA', 17, 1703, 'ADMINKAB', 0),
('1704', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KAUR', 17, 1704, 'ADMINKAB', 0),
('1705', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SELUMA', 17, 1705, 'ADMINKAB', 0),
('1706', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MUKO-MUKO', 17, 1706, 'ADMINKAB', 0),
('1707', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LEBONG', 17, 1707, 'ADMINKAB', 0),
('1708', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KEPAHIANG', 17, 1708, 'ADMINKAB', 0),
('1709', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BENGKULU TENGAH', 17, 1709, 'ADMINKAB', 0),
('1771', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BENGKULU', 17, 1771, 'ADMINKAB', 0),
('1800', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LAMPUNG', 18, 1800, 'ADMINKAB', 0),
('1801', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LAMPUNG SELATAN', 18, 1801, 'ADMINKAB', 0),
('1802', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LAMPUNG TENGAH', 18, 1802, 'ADMINKAB', 0),
('1803', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LAMPUNG UTARA', 18, 1803, 'ADMINKAB', 0),
('1804', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LAMPUNG BARAT', 18, 1804, 'ADMINKAB', 0),
('1805', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TULANG BAWANG', 18, 1805, 'ADMINKAB', 0),
('1806', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TANGGAMUS', 18, 1806, 'ADMINKAB', 0),
('1807', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LAMPUNG TIMUR', 18, 1807, 'ADMINKAB', 0),
('1808', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'WAY KANAN', 18, 1808, 'ADMINKAB', 0),
('1809', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PESAWARAN', 18, 1809, 'ADMINKAB', 0),
('1810', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PRINGSEWU', 18, 1810, 'ADMINKAB', 0),
('1811', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MESUJI', 18, 1811, 'ADMINKAB', 0),
('1812', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TULANG BAWANG BARAT', 18, 1812, 'ADMINKAB', 0),
('1813', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PESISIR BARAT', 18, 1813, 'ADMINKAB', 0),
('1871', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BANDAR LAMPUNG', 18, 1871, 'ADMINKAB', 0),
('1872', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'METRO', 18, 1872, 'ADMINKAB', 0),
('1900', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KEPULAUAN BANGKA BELITUNG', 19, 1900, 'ADMINKAB', 0),
('1901', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BANGKA', 19, 1901, 'ADMINKAB', 0),
('1902', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BELITUNG', 19, 1902, 'ADMINKAB', 0),
('1903', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BANGKA SELATAN', 19, 1903, 'ADMINKAB', 0),
('1904', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BANGKA TENGAH', 19, 1904, 'ADMINKAB', 0),
('1905', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BANGKA BARAT', 19, 1905, 'ADMINKAB', 0),
('1906', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BELITUNG TIMUR', 19, 1906, 'ADMINKAB', 0),
('1971', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA PANGKAL PINANG', 19, 1971, 'ADMINKAB', 0),
('2100', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KEPULAUAN RIAU', 21, 2100, 'ADMINKAB', 0),
('2101', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BINTAN', 21, 2101, 'ADMINKAB', 0),
('2102', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KARIMUN', 21, 2102, 'ADMINKAB', 0),
('2103', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'NATUNA', 21, 2103, 'ADMINKAB', 0),
('2104', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LINGGA', 21, 2104, 'ADMINKAB', 0),
('2105', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KEPULAUAN ANAMBAS', 21, 2105, 'ADMINKAB', 0),
('2171', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA BATAM', 21, 2171, 'ADMINKAB', 0),
('2172', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA TANJUNG PINANG', 21, 2172, 'ADMINKAB', 0),
('3100', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'DKI JAKARTA', 31, 3100, 'ADMINKAB', 0),
('3101', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KEPULAUAN SERIBU', 31, 3101, 'ADMINKAB', 0),
('3171', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'JAKARTA PUSAT', 31, 3171, 'ADMINKAB', 0),
('3172', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'JAKARTA UTARA', 31, 3172, 'ADMINKAB', 0),
('3173', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'JAKARTA BARAT', 31, 3173, 'ADMINKAB', 0),
('3174', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'JAKARTA SELATAN', 31, 3174, 'ADMINKAB', 0),
('3175', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'JAKARTA TIMUR', 31, 3175, 'ADMINKAB', 0),
('3200', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'JAWA BARAT', 32, 3200, 'ADMINKAB', 0),
('3201', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BOGOR', 32, 3201, 'ADMINKAB', 0),
('3202', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SUKABUMI', 32, 3202, 'ADMINKAB', 0),
('3203', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'CIANJUR', 32, 3203, 'ADMINKAB', 0),
('3204', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BANDUNG', 32, 3204, 'ADMINKAB', 0),
('3205', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'GARUT', 32, 3205, 'ADMINKAB', 0),
('3206', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TASIKMALAYA', 32, 3206, 'ADMINKAB', 0),
('3207', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'CIAMIS', 32, 3207, 'ADMINKAB', 0),
('3208', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KUNINGAN', 32, 3208, 'ADMINKAB', 0),
('3209', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'CIREBON', 32, 3209, 'ADMINKAB', 0),
('3210', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MAJALENGKA', 32, 3210, 'ADMINKAB', 0),
('3211', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SUMEDANG', 32, 3211, 'ADMINKAB', 0),
('3212', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'INDRAMAYU', 32, 3212, 'ADMINKAB', 0),
('3213', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SUBANG', 32, 3213, 'ADMINKAB', 0),
('3214', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PURWAKARTA', 32, 3214, 'ADMINKAB', 0),
('3215', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KARAWANG', 32, 3215, 'ADMINKAB', 0),
('3216', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BEKASI', 32, 3216, 'ADMINKAB', 0),
('3217', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BANDUNG BARAT', 32, 3217, 'ADMINKAB', 0),
('3218', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PANGANDARAN', 32, 3218, 'ADMINKAB', 0),
('3271', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA BOGOR', 32, 3271, 'ADMINKAB', 0),
('3272', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA SUKABUMI', 32, 3272, 'ADMINKAB', 0),
('3273', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA BANDUNG', 32, 3273, 'ADMINKAB', 0),
('3274', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA CIREBON', 32, 3274, 'ADMINKAB', 0),
('3275', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA BEKASI', 32, 3275, 'ADMINKAB', 0),
('3276', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA DEPOK', 32, 3276, 'ADMINKAB', 0),
('3277', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA CIMAHI', 32, 3277, 'ADMINKAB', 0),
('3278', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA TASIKMALAYA', 32, 3278, 'ADMINKAB', 0),
('3279', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA BANJAR', 32, 3279, 'ADMINKAB', 0),
('3300', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'JAWA TENGAH', 3300, 0, 'ADMINPROV', 0),
('3301', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'CILACAP', 33, 3301, 'ADMINKAB', 0),
('3302', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BANYUMAS', 33, 3302, 'ADMINKAB', 0),
('3303', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PURBALINGGA', 33, 3303, 'ADMINKAB', 0),
('3304', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BANJARNEGARA', 33, 3304, 'ADMINKAB', 0),
('3305', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KEBUMEN', 33, 3305, 'ADMINKAB', 0),
('3306', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PURWOREJO', 33, 3306, 'ADMINKAB', 0),
('3307', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'WONOSOBO', 33, 3307, 'ADMINKAB', 0),
('3308', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MAGELANG', 33, 3308, 'ADMINKAB', 0),
('3309', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BOYOLALI', 33, 3309, 'ADMINKAB', 0),
('3310', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KLATEN', 33, 3310, 'ADMINKAB', 0),
('3311', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SUKOHARJO', 33, 3311, 'ADMINKAB', 0),
('3312', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'WONOGIRI', 33, 3312, 'ADMINKAB', 0),
('3313', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KARANGANYAR', 33, 3313, 'ADMINKAB', 0),
('3314', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SRAGEN', 33, 3314, 'ADMINKAB', 0),
('3315', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'GROBOGAN', 33, 3315, 'ADMINKAB', 0),
('3316', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BLORA', 33, 3316, 'ADMINKAB', 0),
('3317', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'REMBANG', 33, 3317, 'ADMINKAB', 0),
('3318', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PATI', 33, 3318, 'ADMINKAB', 0),
('3319', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KUDUS', 33, 3319, 'ADMINKAB', 0),
('3320', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'JEPARA', 33, 3320, 'ADMINKAB', 0),
('3321', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'DEMAK', 33, 3321, 'ADMINKAB', 0),
('3322', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SEMARANG', 33, 3322, 'ADMINKAB', 0),
('3323', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TEMANGGUNG', 33, 3323, 'ADMINKAB', 0),
('3324', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KENDAL', 33, 3324, 'ADMINKAB', 0),
('3325', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BATANG', 33, 3325, 'ADMINKAB', 0),
('3326', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PEKALONGAN', 33, 3326, 'ADMINKAB', 0),
('3327', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PEMALANG', 33, 3327, 'ADMINKAB', 0),
('3328', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TEGAL', 33, 3328, 'ADMINKAB', 0),
('3329', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BREBES', 33, 3329, 'ADMINKAB', 0),
('3371', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA MAGELANG', 33, 3371, 'ADMINKAB', 0),
('3372', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA SURAKARTA', 33, 3372, 'ADMINKAB', 0),
('3373', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA SALATIGA', 33, 3373, 'ADMINKAB', 0),
('3374', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA SEMARANG', 33, 3374, 'ADMINKAB', 0),
('3375', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA PEKALONGAN', 33, 3375, 'ADMINKAB', 0),
('3376', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA TEGAL', 33, 3376, 'ADMINKAB', 0),
('3400', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'DAERAH ISTIMEWA YOGYAKARTA', 34, 0, 'ADMINPROV', 0),
('3401', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KULONPROGO', 34, 3401, 'ADMINKAB', 0),
('3402', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BANTUL', 34, 3402, 'ADMINKAB', 0),
('3403', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'GUNUNG KIDUL', 34, 3403, 'ADMINKAB', 0),
('3404', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SLEMAN', 34, 3404, 'ADMINKAB', 0),
('3471', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA YOGYAKARTA', 34, 3471, 'ADMINKAB', 0),
('3500', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'JAWA TIMUR', 35, 0, 'ADMINPROV', 0),
('3501', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PACITAN', 35, 3501, 'ADMINKAB', 0),
('3502', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PONOROGO', 35, 3502, 'ADMINKAB', 0),
('3503', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TRENGGALEK', 35, 3503, 'ADMINKAB', 0),
('3504', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TULUNGAGUNG', 35, 3504, 'ADMINKAB', 0),
('3505', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BLITAR', 35, 3505, 'ADMINKAB', 0),
('3506', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KEDIRI', 35, 3506, 'ADMINKAB', 0),
('3507', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MALANG', 35, 3507, 'ADMINKAB', 0),
('3508', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LUMAJANG', 35, 3508, 'ADMINKAB', 0),
('3509', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'JEMBER', 35, 3509, 'ADMINKAB', 0),
('3510', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BANYUWANGI', 35, 3510, 'ADMINKAB', 0),
('3511', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BONDOWOSO', 35, 3511, 'ADMINKAB', 0),
('3512', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SITUBONDO', 35, 3512, 'ADMINKAB', 0),
('3513', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PROBOLINGGO', 35, 3513, 'ADMINKAB', 0),
('3514', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PASURUAN', 35, 3514, 'ADMINKAB', 0),
('3515', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SIDOARJO', 35, 3515, 'ADMINKAB', 0),
('3516', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MOJOKERTO', 35, 3516, 'ADMINKAB', 0),
('3517', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'JOMBANG', 35, 3517, 'ADMINKAB', 0),
('3518', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'NGANJUK', 35, 3518, 'ADMINKAB', 0),
('3519', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MADIUN', 35, 3519, 'ADMINKAB', 0),
('3520', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MAGETAN', 35, 3520, 'ADMINKAB', 0),
('3521', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'NGAWI', 35, 3521, 'ADMINKAB', 0),
('3522', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BOJONEGORO', 35, 3522, 'ADMINKAB', 0),
('3523', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TUBAN', 35, 3523, 'ADMINKAB', 0),
('3524', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LAMONGAN', 35, 3524, 'ADMINKAB', 0),
('3525', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'GRESIK', 35, 3525, 'ADMINKAB', 0),
('3526', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BANGKALAN', 35, 3526, 'ADMINKAB', 0),
('3527', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SAMPANG', 35, 3527, 'ADMINKAB', 0),
('3528', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PAMEKASAN', 35, 3528, 'ADMINKAB', 0),
('3529', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SUMENEP', 35, 3529, 'ADMINKAB', 0),
('3571', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA KEDIRI', 35, 3571, 'ADMINKAB', 0),
('3572', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA BLITAR', 35, 3572, 'ADMINKAB', 0),
('3573', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA MALANG', 35, 3573, 'ADMINKAB', 0),
('3574', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA PROBOLINGGO', 35, 3574, 'ADMINKAB', 0),
('3575', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA PASURUAN', 35, 3575, 'ADMINKAB', 0),
('3576', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA MOJOKERTO', 35, 3576, 'ADMINKAB', 0),
('3577', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA MADIUN', 35, 3577, 'ADMINKAB', 0),
('3578', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA SURABAYA', 35, 3578, 'ADMINKAB', 0),
('3579', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA BATU', 35, 3579, 'ADMINKAB', 0),
('3600', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BANTEN', 36, 0, 'ADMINPROV', 0),
('3601', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PANDEGLANG', 36, 3601, 'ADMINKAB', 0),
('3602', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LEBAK', 36, 3602, 'ADMINKAB', 0),
('3603', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TANGERANG', 36, 3603, 'ADMINKAB', 0),
('3604', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SERANG', 36, 3604, 'ADMINKAB', 0),
('3671', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA TANGERANG', 36, 3671, 'ADMINKAB', 0),
('3672', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA CILEGON', 36, 3672, 'ADMINKAB', 0),
('3673', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA SERANG', 36, 3673, 'ADMINKAB', 0),
('3674', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA TANGERANG SELATAN', 36, 3674, 'ADMINKAB', 0),
('5100', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BALI', 51, 0, 'ADMINPROV', 0),
('5101', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'JEMBRANA', 51, 5101, 'ADMINKAB', 0),
('5102', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TABANAN', 51, 5102, 'ADMINKAB', 0),
('5103', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BADUNG', 51, 5103, 'ADMINKAB', 0),
('5104', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'GIANYAR', 51, 5104, 'ADMINKAB', 0),
('5105', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KLUNGKUNG', 51, 5105, 'ADMINKAB', 0),
('5106', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BANGLI', 51, 5106, 'ADMINKAB', 0),
('5107', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KARANG ASEM', 51, 5107, 'ADMINKAB', 0),
('5108', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BULELENG', 51, 5108, 'ADMINKAB', 0),
('5171', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA DENPASAR', 51, 5171, 'ADMINKAB', 0),
('5200', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'NUSA TENGGARA BARAT', 52, 0, 'ADMINPROV', 0),
('5201', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LOMBOK BARAT', 52, 5201, 'ADMINKAB', 0),
('5202', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LOMBOK TENGAH ', 52, 5202, 'ADMINKAB', 0),
('5203', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LOMBOK TIMUR', 52, 5203, 'ADMINKAB', 0),
('5204', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SUMBAWA', 52, 5204, 'ADMINKAB', 0),
('5205', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'DOMPU', 52, 5205, 'ADMINKAB', 0),
('5206', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BIMA', 52, 5206, 'ADMINKAB', 0),
('5207', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SUMBAWA BARAT', 52, 5207, 'ADMINKAB', 0),
('5208', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LOMBOK UTARA', 52, 5208, 'ADMINKAB', 0),
('5271', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA MATARAM', 52, 5271, 'ADMINKAB', 0),
('5272', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA BIMA', 52, 5272, 'ADMINKAB', 0),
('5300', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'NUSA TENGGARA TIMUR', 53, 0, 'ADMINPROV', 0),
('5301', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KUPANG', 53, 5301, 'ADMINKAB', 0),
('5302', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TIMOR TENGAH SELATAN', 53, 5302, 'ADMINKAB', 0),
('5303', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TIMOR TENGAH UTARA', 53, 5303, 'ADMINKAB', 0),
('5304', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BELU', 53, 5304, 'ADMINKAB', 0),
('5305', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'ALOR', 53, 5305, 'ADMINKAB', 0),
('5306', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'FLORES TIMUR', 53, 5306, 'ADMINKAB', 0),
('5307', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SIKA', 53, 5307, 'ADMINKAB', 0),
('5308', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'ENDE', 53, 5308, 'ADMINKAB', 0),
('5309', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'NGADA', 53, 5309, 'ADMINKAB', 0),
('5310', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MANGGARAI', 53, 5310, 'ADMINKAB', 0),
('5311', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SUMBA TIMUR', 53, 5311, 'ADMINKAB', 0),
('5312', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SUMBA BARAT', 53, 5312, 'ADMINKAB', 0),
('5313', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LEMBATA', 53, 5313, 'ADMINKAB', 0),
('5314', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'ROTENDAO', 53, 5314, 'ADMINKAB', 0),
('5315', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MANGGARAI BARAT', 53, 5315, 'ADMINKAB', 0),
('5316', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'NAGEKO', 53, 5316, 'ADMINKAB', 0),
('5317', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SUMBA TENGAH', 53, 5317, 'ADMINKAB', 0),
('5318', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SUMBA BARAT DAYA', 53, 5318, 'ADMINKAB', 0),
('5319', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MANGGARAI TIMUR', 53, 5319, 'ADMINKAB', 0),
('5320', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SABU RAIJUA', 53, 5320, 'ADMINKAB', 0),
('5321', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MALAKA', 53, 5321, 'ADMINKAB', 0),
('5371', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA KUPANG', 53, 5371, 'ADMINKAB', 0),
('6100', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KALIMANTAN BARAT', 61, 0, 'ADMINPROV', 0),
('6101', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SAMBAS', 61, 6101, 'ADMINKAB', 0),
('6102', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PONTIANAK', 61, 6102, 'ADMINKAB', 0),
('6103', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SANGGAU', 61, 6103, 'ADMINKAB', 0),
('6104', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KETAPANG', 61, 6104, 'ADMINKAB', 0),
('6105', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SINTANG', 61, 6105, 'ADMINKAB', 0),
('6106', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KAPUAS HULU', 61, 6106, 'ADMINKAB', 0),
('6107', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BENGKAYANG', 61, 6107, 'ADMINKAB', 0),
('6108', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LANDAK', 61, 6108, 'ADMINKAB', 0),
('6109', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SEKADAU', 61, 6109, 'ADMINKAB', 0),
('6110', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MELAWI', 61, 6110, 'ADMINKAB', 0),
('6111', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KAYONG UTARA', 61, 6111, 'ADMINKAB', 0),
('6112', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KUBU RAYA', 61, 6112, 'ADMINKAB', 0),
('6171', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA PONTIANAK', 61, 6171, 'ADMINKAB', 0),
('6172', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA SINGKAWANG', 61, 6172, 'ADMINKAB', 0),
('6200', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KALIMANTAN TENGAH', 62, 6200, 'ADMINKAB', 0),
('6201', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTAWARINGIN BARAT', 62, 6201, 'ADMINKAB', 0),
('6202', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTAWARINGIN TIMUR', 62, 6202, 'ADMINKAB', 0),
('6203', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KAPUAS', 62, 6203, 'ADMINKAB', 0),
('6204', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BARITO SELATAN', 62, 6204, 'ADMINKAB', 0),
('6205', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BARITO UTARA', 62, 6205, 'ADMINKAB', 0),
('6206', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KATINGAN', 62, 6206, 'ADMINKAB', 0),
('6207', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SERUYAN', 62, 6207, 'ADMINKAB', 0),
('6208', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SUKAMARA', 62, 6208, 'ADMINKAB', 0),
('6209', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LAMANDAU', 62, 6209, 'ADMINKAB', 0),
('6210', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'GUNUNG MAS', 62, 6210, 'ADMINKAB', 0),
('6211', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PULANG PISAU', 62, 6211, 'ADMINKAB', 0),
('6212', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MURUNG RAYA', 62, 6212, 'ADMINKAB', 0),
('6213', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BARITO TIMUR', 62, 6213, 'ADMINKAB', 0),
('6271', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA PALANGKA RAYA', 62, 6271, 'ADMINKAB', 0),
('6300', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KALIMANTAN SELATAN', 63, 6300, 'ADMINKAB', 0),
('6301', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TANAH LAUT', 63, 6301, 'ADMINKAB', 0),
('6302', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BARU', 63, 6302, 'ADMINKAB', 0),
('6303', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BANJAR', 63, 6303, 'ADMINKAB', 0),
('6304', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BARITO KUALA', 63, 6304, 'ADMINKAB', 0),
('6305', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TAPIN', 63, 6305, 'ADMINKAB', 0),
('6306', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'HULU SUNGAI SELATAN', 63, 6306, 'ADMINKAB', 0),
('6307', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'HULU SUNGAI TENGAH', 63, 6307, 'ADMINKAB', 0),
('6308', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'HULU SUNGAI UTARA', 63, 6308, 'ADMINKAB', 0),
('6309', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TABALONG', 63, 6309, 'ADMINKAB', 0),
('6310', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TANAH BUMBU', 63, 6310, 'ADMINKAB', 0),
('6311', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BALANGAN', 63, 6311, 'ADMINKAB', 0),
('6371', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA BANJARMASIN', 63, 6371, 'ADMINKAB', 0),
('6372', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA BANJAR BARU', 63, 6372, 'ADMINKAB', 0),
('6400', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KALIMANTAN TIMUR', 64, 6400, 'ADMINKAB', 0),
('6401', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PASER', 64, 6401, 'ADMINKAB', 0),
('6402', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KUTAI KERTANEGARA', 64, 6402, 'ADMINKAB', 0),
('6403', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BERAU', 64, 6403, 'ADMINKAB', 0),
('6404', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BULUNGAN', 64, 6404, 'ADMINKAB', 0),
('6405', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'NUNUKAN', 64, 6405, 'ADMINKAB', 0),
('6406', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MALINAU', 64, 6406, 'ADMINKAB', 0),
('6407', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KUTAI BARAT', 64, 6407, 'ADMINKAB', 0),
('6408', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KUTAI TIMUR', 64, 6408, 'ADMINKAB', 0),
('6409', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PENAJAM PASER UTAMA', 64, 6409, 'ADMINKAB', 0),
('6410', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TANAH TIDUNG', 64, 6410, 'ADMINKAB', 0),
('6471', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA BALIKPAPAN', 64, 6471, 'ADMINKAB', 0),
('6472', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA SAMARINDA', 64, 6472, 'ADMINKAB', 0),
('6473', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA TARAKAN', 64, 6473, 'ADMINKAB', 0),
('6474', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA BONTANG', 64, 6474, 'ADMINKAB', 0),
('6500', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KALIMANTAN UTARA', 65, 6500, 'ADMINKAB', 0),
('6501', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BULUNGAN', 65, 6501, 'ADMINKAB', 0),
('6504', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TANA TIDUNG', 65, 6504, 'ADMINKAB', 0),
('7100', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SULAWESI UTARA', 71, 7100, 'ADMINKAB', 0),
('7101', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BOLAANG MONGONDO', 71, 7101, 'ADMINKAB', 0),
('7102', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MINAHASA', 71, 7102, 'ADMINKAB', 0),
('7103', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KEPULAUAN SANGIHE', 71, 7103, 'ADMINKAB', 0),
('7104', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KEPULAUAN TALAUD', 71, 7104, 'ADMINKAB', 0),
('7105', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MINAHASA SELATAN', 71, 7105, 'ADMINKAB', 0),
('7106', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MINAHSA UTARA', 71, 7106, 'ADMINKAB', 0),
('7107', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MINAHASA TENGGARA', 71, 7107, 'ADMINKAB', 0),
('7108', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BOLMONG UTARA', 71, 7108, 'ADMINKAB', 0),
('7109', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SITARO', 71, 7109, 'ADMINKAB', 0),
('7110', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BOLAANG MONGONDO TIMUR', 71, 7110, 'ADMINKAB', 0),
('7111', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BOLAANG MONGONDO SELATAN', 71, 7111, 'ADMINKAB', 0),
('7171', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA MANADO', 71, 7171, 'ADMINKAB', 0),
('7172', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA BITUNG', 71, 7172, 'ADMINKAB', 0),
('7173', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA TOMOHON', 71, 7173, 'ADMINKAB', 0),
('7174', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA MOBAGO', 71, 7174, 'ADMINKAB', 0),
('7200', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SULAWESI TENGAH', 72, 7200, 'ADMINKAB', 0),
('7201', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BANGGAI', 72, 7201, 'ADMINKAB', 0),
('7202', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'POSO', 72, 7202, 'ADMINKAB', 0),
('7203', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'DONGGALA', 72, 7203, 'ADMINKAB', 0),
('7204', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TOLI-TOLI', 72, 7204, 'ADMINKAB', 0),
('7205', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BUOL', 72, 7205, 'ADMINKAB', 0),
('7206', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MOROWALI', 72, 7206, 'ADMINKAB', 0),
('7207', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BANGGAI KEPULAUAN', 72, 7207, 'ADMINKAB', 0),
('7208', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PARIGI MOUTONG', 72, 7208, 'ADMINKAB', 0),
('7209', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TOJO UNA-UNA', 72, 7209, 'ADMINKAB', 0),
('7210', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SIGI', 72, 7210, 'ADMINKAB', 0),
('7212', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MOROWALI UTARA', 72, 7212, 'ADMINKAB', 0),
('7271', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA PALU', 72, 7271, 'ADMINKAB', 0),
('7300', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SULAWESI SELATAN', 73, 0, 'ADMINPROV', 0),
('7301', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KEPULAUAN SELAYAR', 73, 7301, 'ADMINKAB', 0),
('7302', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BULUKUMBA', 73, 7302, 'ADMINKAB', 0),
('7303', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BANTAENG', 73, 7303, 'ADMINKAB', 0),
('7304', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'JENEPONTO', 73, 7304, 'ADMINKAB', 0),
('7305', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TAKALAR', 73, 7305, 'ADMINKAB', 0),
('7306', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'GOWA', 73, 7306, 'ADMINKAB', 0),
('7307', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SINJAI', 73, 7307, 'ADMINKAB', 0),
('7308', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BONE', 73, 7308, 'ADMINKAB', 0),
('7309', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MAROS', 73, 7309, 'ADMINKAB', 0),
('7310', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PANGKAJENE KEPULAUAN', 73, 7310, 'ADMINKAB', 0),
('7311', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BARRU', 73, 7311, 'ADMINKAB', 0),
('7312', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SOPPENG', 73, 7312, 'ADMINKAB', 0),
('7313', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'WAJO', 73, 7313, 'ADMINKAB', 0),
('7314', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SIDENRENG RAPPANG', 73, 7314, 'ADMINKAB', 0),
('7315', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PINRANG', 73, 7315, 'ADMINKAB', 0),
('7316', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'ENREKANG', 73, 7316, 'ADMINKAB', 0),
('7317', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LUWU', 73, 7317, 'ADMINKAB', 0),
('7318', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TANA TORAJA', 73, 7318, 'ADMINKAB', 0),
('7322', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LUWU UTARA', 73, 7322, 'ADMINKAB', 0),
('7324', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LUWU TIMUR', 73, 7324, 'ADMINKAB', 0),
('7326', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TORAJA UTARA', 73, 7326, 'ADMINKAB', 0),
('7371', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA MAKASAR', 73, 7371, 'ADMINKAB', 0),
('7372', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA PARE-PARE', 73, 7372, 'ADMINKAB', 0),
('7373', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA PALOPO', 73, 7373, 'ADMINKAB', 0),
('7400', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SULAWESI TENGGARA', 74, 0, 'ADMINPROV', 0),
('7401', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOLAKA', 74, 7401, 'ADMINKAB', 0),
('7402', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KENDARI', 74, 7402, 'ADMINKAB', 0),
('7403', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MUNA', 74, 7403, 'ADMINKAB', 0),
('7404', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BUTON', 74, 7404, 'ADMINKAB', 0),
('7405', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KONAWE SELATAN', 74, 7405, 'ADMINKAB', 0),
('7406', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BOMBANA', 74, 7406, 'ADMINKAB', 0),
('7407', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'WAKATOBI', 74, 7407, 'ADMINKAB', 0),
('7408', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOLAKA UTARA', 74, 7408, 'ADMINKAB', 0),
('7409', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KONAWE UTARA', 74, 7409, 'ADMINKAB', 0),
('7410', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BUTON UTARA', 74, 7410, 'ADMINKAB', 0),
('7411', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOLAKA TIMUR', 74, 7411, 'ADMINKAB', 0),
('7412', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KONAWE KEPULAUAN', 74, 7412, 'ADMINKAB', 0),
('7414', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BUTON TENGAH', 74, 7414, 'ADMINKAB', 0),
('7415', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BUTON SELATAN', 74, 7415, 'ADMINKAB', 0),
('7471', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA KENDARI', 74, 7471, 'ADMINKAB', 0),
('7472', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA BAU-BAU', 74, 7472, 'ADMINKAB', 0),
('7500', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'GORONTALO', 75, 0, 'ADMINPROV', 0),
('7501', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'GORONTALO', 75, 7501, 'ADMINKAB', 0),
('7502', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BOALEMO', 75, 7502, 'ADMINKAB', 0),
('7503', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BONE BOLANGO', 75, 7503, 'ADMINKAB', 0),
('7504', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'POHUWATO', 75, 7504, 'ADMINKAB', 0),
('7505', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'GORONTALO UTARA', 75, 7505, 'ADMINKAB', 0),
('7571', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA GORONTALO', 75, 7571, 'ADMINKAB', 0),
('7600', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SULAWESI BARAT', 76, 0, 'ADMINPROV', 0),
('7601', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MAMUJU UTARA', 76, 7601, 'ADMINKAB', 0),
('7602', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MAMUJU', 76, 7602, 'ADMINKAB', 0),
('7603', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MAMASA', 76, 7603, 'ADMINKAB', 0),
('7604', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'POLEWALI MANDAR', 76, 7604, 'ADMINKAB', 0),
('7605', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MAJENE', 76, 7605, 'ADMINKAB', 0),
('7606', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MAMUJU TENGAH', 76, 7606, 'ADMINKAB', 0),
('8100', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MALUKU', 81, 8100, 'ADMINKAB', 0),
('8101', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MALUKU TENGAH', 81, 8101, 'ADMINKAB', 0),
('8102', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MALUKU TENGGARA', 81, 8102, 'ADMINKAB', 0),
('8103', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MALUKU TENGGARA BARAT', 81, 8103, 'ADMINKAB', 0),
('8104', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BURU', 81, 8104, 'ADMINKAB', 0),
('8105', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KEPULAUAN ARU', 81, 8105, 'ADMINKAB', 0),
('8106', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SERAM BAGIAN BARAT', 81, 8106, 'ADMINKAB', 0),
('8107', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SERAM BAGIAN TIMUR', 81, 8107, 'ADMINKAB', 0),
('8108', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MALUKU BARAT DAYA', 81, 8108, 'ADMINKAB', 0),
('8109', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BURU SELATAN', 81, 8109, 'ADMINKAB', 0),
('8171', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA AMBON', 81, 8171, 'ADMINKAB', 0),
('8172', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KAB. TUAL', 81, 8172, 'ADMINKAB', 0),
('8200', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MALUKU UTARA', 82, 8200, 'ADMINKAB', 0),
('8201', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'HALMAHERA BARAT', 82, 8201, 'ADMINKAB', 0),
('8202', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'HALMAHERA TENGAH', 82, 8202, 'ADMINKAB', 0),
('8203', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'HALMAHERA UTARA', 82, 8203, 'ADMINKAB', 0),
('8204', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'HALMAHERA SELATAN', 82, 8204, 'ADMINKAB', 0),
('8205', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KEPULAUAN SULA', 82, 8205, 'ADMINKAB', 0),
('8206', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'HALMAHERA TIMUR', 82, 8206, 'ADMINKAB', 0),
('8207', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PULAU MOROTAI', 82, 8207, 'ADMINKAB', 0),
('8208', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KAB. P. TALIABU', 82, 8208, 'ADMINKAB', 0),
('8271', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA TERNATE', 82, 8271, 'ADMINKAB', 0),
('8272', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA TIDORE KEPULAUAN', 82, 8272, 'ADMINKAB', 0),
('9100', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PAPUA', 91, 0, 'ADMINPROV', 0),
('9101', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MERAUKE', 91, 9101, 'ADMINKAB', 0),
('9102', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'JAYAWIJAYA', 91, 9102, 'ADMINKAB', 0),
('9103', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'JAYAPURA', 91, 9103, 'ADMINKAB', 0),
('9104', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'NABIRE', 91, 9104, 'ADMINKAB', 0),
('9105', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KEPULAUAN YAPEN', 91, 9105, 'ADMINKAB', 0),
('9106', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BIAK NUMFOR', 91, 9106, 'ADMINKAB', 0),
('9107', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PUNCAK JAYA', 91, 9107, 'ADMINKAB', 0);
INSERT INTO `tbl_user` (`u_name`, `pass_word`, `aktif`, `Keterangan`, `id_prov`, `id_kabkot`, `level`, `dinas_id`) VALUES
('9108', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PANIAI', 91, 9108, 'ADMINKAB', 0),
('9109', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MIMIKA', 91, 9109, 'ADMINKAB', 0),
('9110', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SARMI', 91, 9110, 'ADMINKAB', 0),
('9111', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KEEROM', 91, 9111, 'ADMINKAB', 0),
('9112', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PEG. BINTANG', 91, 9112, 'ADMINKAB', 0),
('9113', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'YAHUKIMO', 91, 9113, 'ADMINKAB', 0),
('9114', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TOLIKARA', 91, 9114, 'ADMINKAB', 0),
('9115', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'WAROPEN', 91, 9115, 'ADMINKAB', 0),
('9116', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'BOVEN DIGUL', 91, 9116, 'ADMINKAB', 0),
('9117', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MAPPI', 91, 9117, 'ADMINKAB', 0),
('9118', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'ASMAT', 91, 9118, 'ADMINKAB', 0),
('9119', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SUPIORI', 91, 9119, 'ADMINKAB', 0),
('9120', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MAMBERAMO RAYA', 91, 9120, 'ADMINKAB', 0),
('9121', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MAMBERAMO TENGAH', 91, 9121, 'ADMINKAB', 0),
('9122', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'YALIMO', 91, 9122, 'ADMINKAB', 0),
('9123', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'LANNY JAYA', 91, 9123, 'ADMINKAB', 0),
('9124', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'NDUGA', 91, 9124, 'ADMINKAB', 0),
('9125', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KABUPATEN PUNCAK', 91, 9125, 'ADMINKAB', 0),
('9126', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'DOGIYAI', 91, 9126, 'ADMINKAB', 0),
('9127', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'INTAN JAYA', 91, 9127, 'ADMINKAB', 0),
('9128', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'DEIYAI', 91, 9128, 'ADMINKAB', 0),
('9171', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA JAYAPURA', 91, 9171, 'ADMINKAB', 0),
('9200', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'PAPUA BARAT', 92, 0, 'ADMINPROV', 0),
('9201', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SORONG', 92, 0, 'ADMINPROV', 0),
('9202', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MANOKWARI', 92, 0, 'ADMINPROV', 0),
('9203', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'FAKFAK', 92, 9203, 'ADMINKAB', 0),
('9204', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'SORONG SELATAN', 92, 0, 'ADMINPROV', 0),
('9205', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'RAJA AMPAT', 92, 9205, 'ADMINKAB', 0),
('9206', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TELUK BINTUNI', 92, 9206, 'ADMINKAB', 0),
('9207', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TELUK WONDAMA', 92, 9207, 'ADMINKAB', 0),
('9208', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KAIMANA', 92, 9208, 'ADMINKAB', 0),
('9209', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'TAMBRAUW', 92, 9209, 'ADMINKAB', 0),
('9210', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MAYBRAT', 92, 9210, 'ADMINKAB', 0),
('9211', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'MANOKWARI SELATAN', 92, 9211, 'ADMINKAB', 0),
('9271', '224bec3dd08832bc6a69873f15a50df406045f40', 'Y', 'KOTA SORONG', 92, 9271, 'ADMINKAB', 0),
('admin', '229797cd8d5d128a1043395172e7a4d47732bf0f', 'Y', 'admin', 0, 0, 'admin', 6);

-- --------------------------------------------------------

--
-- Table structure for table `tempdata`
--

CREATE TABLE `tempdata` (
  `Provinsi` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `kabkota` varchar(47) CHARACTER SET utf8 DEFAULT NULL,
  `r1` int(11) DEFAULT NULL,
  `s1` int(11) DEFAULT NULL,
  `p1` int(11) DEFAULT NULL,
  `r2` int(11) DEFAULT NULL,
  `s2` int(11) DEFAULT NULL,
  `p2` int(11) DEFAULT NULL,
  `r3` int(11) DEFAULT NULL,
  `s3` int(11) DEFAULT NULL,
  `p3` int(11) DEFAULT NULL,
  `r4` int(11) DEFAULT NULL,
  `s4` int(11) DEFAULT NULL,
  `p4` int(11) DEFAULT NULL,
  `r5` int(11) DEFAULT NULL,
  `s5` int(11) DEFAULT NULL,
  `p5` int(11) DEFAULT NULL,
  `r6` int(11) DEFAULT NULL,
  `s6` int(11) DEFAULT NULL,
  `p6` int(11) DEFAULT NULL,
  `r7` int(11) DEFAULT NULL,
  `s7` int(11) DEFAULT NULL,
  `p7` int(11) DEFAULT NULL,
  `r8` int(11) DEFAULT NULL,
  `s8` int(11) DEFAULT NULL,
  `p8` int(11) DEFAULT NULL,
  `r9` int(11) DEFAULT NULL,
  `s9` int(11) DEFAULT NULL,
  `p9` int(11) DEFAULT NULL,
  `r10` int(11) DEFAULT NULL,
  `s10` int(11) DEFAULT NULL,
  `p10` int(11) DEFAULT NULL,
  `r11` int(11) DEFAULT NULL,
  `s11` int(11) DEFAULT NULL,
  `p11` int(11) DEFAULT NULL,
  `r12` int(11) DEFAULT NULL,
  `s12` int(11) DEFAULT NULL,
  `p12` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tempdata`
--

INSERT INTO `tempdata` (`Provinsi`, `kabkota`, `r1`, `s1`, `p1`, `r2`, `s2`, `p2`, `r3`, `s3`, `p3`, `r4`, `s4`, `p4`, `r5`, `s5`, `p5`, `r6`, `s6`, `p6`, `r7`, `s7`, `p7`, `r8`, `s8`, `p8`, `r9`, `s9`, `p9`, `r10`, `s10`, `p10`, `r11`, `s11`, `p11`, `r12`, `s12`, `p12`) VALUES
('PROV NAD', ' SIMEULEU', 1632, 2499, 65, 1666, 2368, 70, 1686, 2272, 74, 3404, 8964, 38, 3408, 3602, 95, 0, 54847, 0, 5214, 531, 982, 1646, 15406, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 12, 100),
('PROV NAD', ' ACEH SINGKIL', 2305, 3656, 63, 2304, 349, 660, 2506, 3324, 75, 5553, 16217, 34, 3455, 6085, 57, 0, 69054, 0, 0, 5079, 0, 1512, 3429, 44, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 11949, 0),
('PROV NAD', ' ACEH SELATAN', 4626, 4887, 95, 4427, 4665, 95, 441, 4443, 10, 21565, 25607, 84, 9342, 9342, 100, 99716, 136341, 73, 14266, 16421, 87, 2439, 2497, 98, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3675, 4887, 75),
('PROV NAD', ' ACEH TENGGARA', 5447, 5502, 99, 5252, 5252, 100, 5002, 5002, 100, 24405, 24405, 100, 11793, 29602, 40, 0, 128222, 0, 1663, 11763, 14, 1508, 22615, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 109, 6063, 2),
('PROV NAD', ' ACEH TIMUR', 8424, 1143, 737, 7529, 10911, 69, 8068, 10391, 78, 50701, 50701, 100, 17142, 0, 0, 261128, 0, 0, 13366, 2388, 560, 13227, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PROV NAD', ' ACEH TENGAH', 4208, 4601, 91, 3238, 3959, 82, 3932, 478, 823, 16925, 18935, 89, 5922, 7629, 78, 35823, 131077, 27, 11274, 11658, 97, 1822, 33506, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2179, 6822, 32),
('PROV NAD', ' ACEH BARAT', 3825, 465, 823, 3547, 4438, 80, 3502, 4368, 80, 11122, 16322, 68, 3009, 3718, 81, 98445, 136775, 72, 14819, 14819, 100, 3298, 134762, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1254, 5348, 23),
('PROV NAD', ' ACEH BESAR', 7803, 10678, 73, 7827, 10193, 77, 7545, 9707, 78, 1253, 47365, 3, 7073, 7944, 89, 27001, 271243, 10, 7477, 27896, 27, 16832, 5298, 318, 0, 0, 0, 0, 0, 0, 0, 0, 0, 420, 0, 0),
('PROV NAD', ' PIDIE', 7331, 10231, 72, 7807, 9766, 80, 7492, 9301, 81, 40426, 51053, 79, 14067, 17833, 79, 116984, 268008, 44, 1529, 24778, 6, 10715, 79944, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1685, 11349, 15),
('PROV NAD', ' BIREUEN', 8755, 10082, 87, 8777, 9623, 91, 8794, 9165, 96, 25605, 53504, 48, 17455, 18539, 94, 10533, 280779, 4, 89746, 89746, 100, 15811, 68354, 23, 0, 0, 0, 0, 0, 0, 0, 0, 0, 803, 11145, 7),
('PROV NAD', ' ACEH UTARA', 13634, 14597, 93, 12633, 13934, 91, 12037, 1327, 907, 37021, 64764, 57, 10194, 21341, 48, 50645, 369269, 14, 42774, 46301, 92, 10847, 88625, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 623, 918, 68),
('PROV NAD', ' ACEH BARAT DAYA', 2794, 4322, 65, 2733, 4125, 66, 2648, 3609, 73, 9742, 14289, 68, 2281, 17409, 13, 1382, 79127, 2, 0, 47916, 0, 8447, 37267, 23, 0, 0, 0, 0, 0, 0, 0, 0, 0, 31, 0, 0),
('PROV NAD', ' GAYO LUES', 1979, 2229, 89, 1843, 1982, 93, 1702, 1975, 86, 8413, 9406, 89, 3307, 3482, 95, 35387, 44234, 80, 2594, 3243, 80, 2086, 17258, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 175, 2708, 6),
('PROV NAD', ' ACEH TAMIANG', 5636, 7238, 78, 5526, 6909, 80, 515, 658, 78, 25989, 32104, 81, 6196, 6649, 93, 56121, 18347, 306, 10688, 1657, 645, 13267, 8558, 155, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1043, 7757, 13),
('PROV NAD', ' NAGAN RAYA', 3158, 3695, 85, 2921, 3527, 83, 2825, 3359, 84, 11615, 14262, 81, 0, 3388, 0, 0, 103251, 0, 0, 6289, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 33),
('PROV NAD', ' ACEH JAYA', 2143, 2503, 86, 1859, 2389, 78, 1839, 2275, 81, 7352, 11101, 66, 3264, 3264, 100, 60345, 63175, 96, 4657, 5979, 78, 6482, 11498, 56, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2625, 0),
('PROV NAD', ' BENER MERIAH', 3537, 375, 943, 3251, 3579, 91, 3236, 3409, 95, 9465, 13117, 72, 4026, 5762, 70, 22299, 44796, 50, 4624, 86, 5377, 8062, 1658, 486, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 100),
('PROV NAD', ' PIDIE JAYA', 3215, 3659, 88, 2968, 3492, 85, 2958, 3321, 89, 12608, 16228, 78, 63, 6304, 1, 7858, 93787, 8, 844, 13085, 6, 5071, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20, 182, 11),
('PROV NAD', ' KOTA BANDA ACEH', 5868, 12284, 48, 5771, 11751, 49, 4756, 12977, 37, 12385, 48005, 26, 8597, 1763, 488, 362, 85838, 0, 9297, 27381, 34, 6507, 9418, 69, 0, 0, 0, 0, 0, 0, 0, 0, 0, 21, 7584, 0),
('PROV NAD', ' KOTA SABANG', 797, 815, 98, 742, 908, 82, 744, 865, 86, 1521, 3426, 44, 1312, 1406, 93, 0, 22294, 0, 1903, 1903, 100, 1936, 1936, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 30, 40, 75),
('PROV NAD', ' KOTA LANGSA', 3809, 4046, 94, 3629, 3862, 94, 358, 3678, 10, 12873, 17947, 72, 6312, 6477, 97, 16048, 109734, 15, 12266, 13844, 89, 4808, 2356, 204, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2796, 5431, 51),
('PROV NAD', ' KOTA LHOKSEUMAWE', 4537, 4738, 96, 4265, 4265, 100, 4133, 4414, 94, 19087, 21536, 89, 4719, 54, 8739, 47792, 127212, 38, 8148, 9545, 85, 23845, 29402, 81, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2688, 2688, 100),
('PROV NAD', ' KOTA SUBULUSSALAM', 2255, 2255, 100, 1968, 1968, 100, 1952, 1952, 100, 7765, 7765, 100, 2702, 3474, 78, 606, 45291, 1, 1112, 2923, 38, 1369, 1369, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 973, 2621, 37),
('SUMATERA UTARA', ' NIAS', 2426, 4299, 56, 2079, 4103, 51, 6144, 3908, 157, 1343, 19379, 7, 3748, 3842, 98, 4428, 78104, 6, 1818, 8661, 21, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 10, 100),
('SUMATERA UTARA', ' MANDAILING NATAL', 8318, 11629, 72, 7368, 111, 6638, 7823, 1064, 735, 26935, 52419, 51, 6895, 72003, 10, 200, 240157, 0, 2717, 31343, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 350, 3),
('SUMATERA UTARA', ' TAPANULI SELATAN', 6883, 7168, 96, 6302, 6842, 92, 5669, 6516, 87, 27688, 3231, 857, 8161, 11505, 71, 0, 0, 0, 14604, 20606, 71, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('SUMATERA UTARA', ' TAPANULI TENGAH', 9204, 9947, 93, 89, 9495, 1, 9043, 9043, 100, 2402, 28632, 8, 7072, 7214, 98, 43766, 20577, 213, 2067, 19771, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 36, 36, 100),
('SUMATERA UTARA', ' TAPANULI UTARA', 603, 6431, 9, 5473, 6113, 90, 5762, 6449, 89, 27941, 29495, 95, 15502, 15502, 100, 325, 159965, 0, 175, 32564, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('SUMATERA UTARA', ' TOBA SAMOSIR', 4213, 4632, 91, 3933, 4213, 93, 3933, 4203, 94, 16699, 20843, 80, 437, 437, 100, 0, 39106, 0, 15259, 20792, 73, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4677, 5584, 84),
('SUMATERA UTARA', ' LABUHAN BATU', 10301, 12199, 84, 9544, 11644, 82, 9695, 1109, 874, 48708, 54991, 89, 14805, 11049, 134, 9288, 582726, 2, 17384, 27915, 62, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20774, 21474, 97),
('SUMATERA UTARA', ' ASAHAN', 15298, 16951, 90, 15772, 1618, 975, 14419, 1541, 936, 6014, 76412, 8, 13408, 88935, 15, 491616, 438133, 112, 53353, 53483, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 143, 71, 201),
('SUMATERA UTARA', ' SIMALUNGUN', 17329, 19093, 91, 16306, 19093, 85, 14151, 17357, 82, 78314, 86065, 91, 4014, 17877, 22, 102972, 514862, 20, 48557, 83846, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 184, 0, 0),
('SUMATERA UTARA', ' DAIRI', 6106, 7361, 83, 6011, 7026, 86, 5907, 6692, 88, 22155, 338, 6555, 6698, 41664, 16, 1422, 44011, 3, 1513, 24084, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 718, 6, 11967),
('SUMATERA UTARA', ' KARO', 7874, 9661, 82, 7149, 9222, 78, 803, 8298, 10, 5153, 35221, 15, 100, 8815, 1, 741, 255752, 0, 465, 22412, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1495, 97, 1541),
('SUMATERA UTARA', ' DELI SERDANG', 47409, 49122, 97, 44602, 46889, 95, 40535, 43936, 92, 206778, 221412, 93, 68049, 68049, 100, 607003, 1345483, 45, 126853, 131072, 97, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3463, 52957, 7),
('SUMATERA UTARA', ' LANGKAT', 21418, 23296, 92, 20566, 22237, 92, 20604, 20604, 100, 101441, 104291, 97, 18699, 21493, 87, 499773, 640281, 78, 47968, 77653, 62, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 201, 245, 82),
('SUMATERA UTARA', ' NIAS SELATAN', 5735, 7228, 79, 4606, 6899, 67, 4638, 6443, 72, 1807, 36254, 5, 35091, 35091, 100, 0, 176843, 0, 0, 1598, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('SUMATERA UTARA', ' HUMBANG HASUNDUTAN', 3798, 4026, 94, 2232, 3842, 58, 3207, 3207, 100, 12599, 19138, 66, 10633, 10687, 99, 11225, 11438, 98, 89321, 94233, 95, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 260, 520, 50),
('SUMATERA UTARA', ' PAKPAK BHARAT', 122, 122, 100, 1144, 1144, 100, 1049, 1049, 100, 4062, 4062, 100, 2179, 2179, 100, 1385, 1385, 100, 1922, 1922, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 100),
('SUMATERA UTARA', ' SAMOSIR', 2101, 3142, 67, 2141, 2999, 71, 215, 2156, 10, 9022, 11405, 79, 3086, 3086, 100, 9306, 31674, 29, 4667, 14567, 32, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 18, 18, 100),
('SUMATERA UTARA', ' SERDANG BEDAGAI', 1212, 14091, 9, 11808, 13451, 88, 10752, 11705, 92, 58409, 6427, 909, 12907, 12907, 100, 6464, 375994, 2, 0, 495, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 387, 4952, 8),
('SUMATERA UTARA', ' BATU BARA', 9196, 10209, 90, 9201, 9762, 94, 9183, 9357, 98, 32232, 49197, 66, 8907, 9576, 93, 107928, 243247, 44, 51227, 27913, 184, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 538, 2),
('SUMATERA UTARA', ' PADANG LAWAS UTARA', 7607, 829, 918, 7133, 7913, 90, 7011, 7536, 93, 41223, 41223, 100, 6717, 6717, 100, 143512, 143512, 100, 13789, 13789, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('SUMATERA UTARA', ' PADANG LAWAS', 6202, 8368, 74, 566, 7987, 7, 7151, 767, 932, 22234, 37718, 59, 5897, 6899, 85, 46587, 152373, 31, 2028, 15128, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 100),
('SUMATERA UTARA', ' LABUHAN BATU SELATAN', 8244, 7366, 112, 8292, 7031, 118, 7491, 598, 1253, 16821, 33039, 51, 3271, 14851, 22, 0, 194191, 0, 13429, 12614, 106, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 2, 150),
('SUMATERA UTARA', ' LABUHAN BATU UTARA', 7997, 8072, 99, 7703, 7705, 100, 7674, 7338, 105, 27496, 369, 7451, 7777, 142823, 5, 500, 0, 0, 18322, 14277, 128, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 40647, 0),
('SUMATERA UTARA', ' NIAS UTARA', 2888, 3928, 74, 1434, 375, 382, 2596, 3571, 73, 6523, 8548, 76, 10568, 25726, 41, 46831, 78051, 60, 1237, 446, 277, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('SUMATERA UTARA', ' NIAS BARAT', 1576, 2501, 63, 1352, 2388, 57, 1329, 2388, 56, 1847, 10241, 18, 812, 6958, 12, 605, 49537, 1, 3351, 5621, 60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('SUMATERA UTARA', ' KOTA SIBOLGA', 1832, 2081, 88, 179, 1987, 9, 1747, 1934, 90, 7535, 8101, 93, 5377, 5686, 95, 43418, 53818, 81, 3329, 5106, 65, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 112, 2081, 5),
('SUMATERA UTARA', ' KOTA TANJUNG BALAI', 2889, 4255, 68, 2313, 4061, 57, 13, 3868, 0, 14356, 19179, 75, 86, 101, 85, 11091, 0, 0, 0, 11091, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20, 20, 100),
('SUMATERA UTARA', ' KOTA PEMATANG SIANTAR', 4086, 4963, 82, 4157, 4738, 88, 3863, 4512, 86, 1834, 1834, 100, 11537, 11646, 99, 707, 158109, 0, 5982, 21933, 27, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6102, 251514, 2),
('SUMATERA UTARA', ' KOTA TEBING TINGGI', 3229, 323, 1000, 3023, 3029, 100, 3009, 3024, 100, 13285, 15629, 85, 766, 766, 100, 74742, 74742, 100, 6857, 7619, 90, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4146, 4245, 98),
('SUMATERA UTARA', ' KOTA MEDAN', 39379, 43375, 91, 37943, 42252, 90, 37948, 39552, 96, 149613, 196423, 76, 82094, 87885, 93, 5854, 1547359, 0, 119623, 159039, 75, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 32455, 19247, 169),
('SUMATERA UTARA', ' KOTA BINJAI', 5337, 5665, 94, 4589, 5407, 85, 4573, 515, 888, 137, 25536, 1, 4724, 29201, 16, 98232, 152004, 65, 10394, 18493, 56, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 102, 267901, 0),
('SUMATERA UTARA', ' KOTA PADANGSIDIMPUAN', 3773, 4927, 77, 3208, 4703, 68, 356, 3737, 10, 17807, 22211, 80, 7336, 9386, 78, 58423, 133658, 44, 1432, 1302, 110, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3626, 5337, 68),
('SUMATERA UTARA', ' KOTA GUNUNGSITOLI', 2216, 3166, 70, 1986, 3022, 66, 1986, 2636, 75, 1468, 2928, 50, 6385, 6385, 100, 88712, 82792, 107, 7005, 9341, 75, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6, 100),
('SUMATERA BARAT', ' KEPULAUAN MENTAWAI', 1368, 2655, 52, 618, 2535, 24, 1492, 2414, 62, 2973, 11817, 25, 477, 335, 142, 3538, 54227, 7, 2962, 4834, 61, 2319, 148, 1567, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2314, 0),
('SUMATERA BARAT', ' PESISIR SELATAN', 9345, 9500, 98, 0, 0, 0, 92, 0, 0, 24128, 0, 0, 1277, 0, 0, 0, 0, 0, 4169, 0, 0, 27081, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('SUMATERA BARAT', ' SOLOK', 6927, 8643, 80, 6946, 825, 842, 6533, 7857, 83, 22537, 38468, 59, 7908, 45964, 17, 6179, 232206, 3, 2224, 34909, 6, 1216, 13448, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1718, 3, 57267),
('SUMATERA BARAT', ' SIJUNJUNG', 4287, 5878, 73, 4187, 5611, 75, 4609, 5208, 88, 13711, 26166, 52, 11712, 28237, 41, 5625, 146189, 4, 8238, 18266, 45, 11351, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 380, 5868, 6),
('SUMATERA BARAT', ' TANAH DATAR', 4531, 7073, 64, 4844, 6966, 70, 4278, 643, 665, 19805, 15966, 124, 12885, 6634, 194, 30815, 204639, 15, 26098, 41877, 62, 3169, 4052, 78, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0),
('SUMATERA BARAT', ' PADANG PARIAMAN', 0, 9069, 0, 0, 8656, 0, 0, 8244, 0, 0, 40366, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('SUMATERA BARAT', ' AGAM', 7294, 10565, 69, 7293, 10084, 72, 6955, 9604, 72, 24184, 47023, 51, 0, 19136, 0, 0, 280459, 0, 0, 60844, 0, 0, 124946, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23, 0),
('SUMATERA BARAT', ' LIMA PULUH KOTA', 6814, 8529, 80, 6334, 8142, 78, 639, 7754, 8, 20569, 37965, 54, 10119, 43646, 23, 0, 237718, 0, 18225, 45354, 40, 10107, 9364, 108, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('SUMATERA BARAT', ' PASAMAN', 5263, 7134, 74, 6809, 6809, 100, 4995, 6483, 77, 2783, 3226, 86, 12273, 12346, 99, 118159, 22582, 523, 22582, 22582, 100, 32957, 43899, 75, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('SUMATERA BARAT', ' SOLOK SELATAN', 3395, 4107, 83, 2968, 3921, 76, 3248, 3583, 91, 10676, 18284, 58, 2967, 20832, 14, 28818, 105346, 27, 10005, 12749, 78, 2875, 25336, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4336, 4336, 100),
('SUMATERA BARAT', ' DHARMASRAYA', 3697, 444, 833, 3776, 3827, 99, 3731, 3827, 97, 16277, 21138, 77, 4753, 4843, 98, 150042, 152561, 98, 12821, 17324, 74, 7181, 7181, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 787, 4795, 16),
('SUMATERA BARAT', ' PASAMAN BARAT', 7796, 11561, 67, 8251, 11035, 75, 8289, 1051, 789, 22398, 5146, 435, 9604, 9859, 97, 0, 0, 0, 2523, 27724, 9, 17989, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('SUMATERA BARAT', ' KOTA PADANG', 17559, 18365, 96, 16954, 1753, 967, 16112, 16969, 95, 75294, 81736, 92, 2984, 323, 924, 69257, 688049, 10, 62667, 62667, 100, 12831, 633496, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1756, 225, 780),
('SUMATERA BARAT', ' KOTA SOLOK', 1509, 1612, 94, 1426, 1538, 93, 1363, 1465, 93, 5545, 7171, 77, 925, 784, 118, 3946, 19182, 21, 514, 4543, 11, 3061, 153, 2001, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 4075, 0),
('SUMATERA BARAT', ' KOTA SAWAH LUNTO', 862, 1379, 63, 928, 1317, 70, 1013, 1254, 81, 862, 6138, 14, 1164, 1164, 100, 103182, 37754, 273, 3636, 6207, 59, 1378, 1378, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('SUMATERA BARAT', ' KOTA PADANG PANJANG', 1103, 1192, 93, 1075, 1088, 99, 1072, 1089, 98, 3981, 5305, 75, 3021, 3191, 95, 7238, 2304, 314, 996, 4198, 24, 2304, 2304, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 100),
('SUMATERA BARAT', ' KOTA BUKITTINGGI', 2572, 2843, 90, 2438, 2713, 90, 2427, 2427, 100, 8732, 12654, 69, 4876, 4876, 100, 82423, 82423, 100, 6769, 9787, 69, 14448, 32081, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 68, 68, 100),
('SUMATERA BARAT', ' KOTA PAYAKUMBUH', 3122, 3122, 100, 298, 2982, 10, 2838, 2838, 100, 13086, 13086, 100, 2616, 6775, 39, 85634, 85634, 100, 12012, 12681, 95, 15667, 15667, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3538, 3538, 100),
('SUMATERA BARAT', ' KOTA PARIAMAN', 1816, 1816, 100, 1734, 1734, 100, 0, 1618, 0, 0, 8086, 0, 1749, 3498, 50, 0, 28142, 0, 0, 8719, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('RIAU', ' KUANTAN SINGINGI', 5747, 6963, 83, 5624, 6647, 85, 5461, 5583, 98, 2404, 24929, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2725, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0),
('RIAU', ' INDRAGIRI HULU', 8099, 10591, 76, 6952, 805, 864, 8069, 9629, 84, 8078, 45713, 18, 15312, 18772, 82, 76591, 276143, 28, 18325, 22676, 81, 9043, 107775, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 550, 1139, 48),
('RIAU', ' INDRAGIRI HILIR', 12255, 15841, 77, 4627, 14661, 32, 11158, 11158, 100, 36232, 65055, 56, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6578, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('RIAU', ' PELALAWAN', 8981, 8981, 100, 8061, 8061, 100, 8089, 8517, 95, 37189, 55962, 66, 9736, 51349, 19, 5522, 3395, 163, 13861, 31312, 44, 8112, 77, 10535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2186, 10139, 22),
('RIAU', ' SIAK', 9092, 10104, 90, 7637, 8684, 88, 8572, 9189, 93, 30752, 36339, 85, 18469, 18686, 99, 30101, 273913, 11, 7817, 18541, 42, 7274, 105521, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9747, 12026, 81),
('RIAU', ' KAMPAR', 16951, 18757, 90, 13842, 17908, 77, 16452, 17057, 96, 44618, 87082, 51, 19454, 23515, 83, 22244, 512749, 4, 33121, 101524, 33, 33121, 512749, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 276, 276, 100),
('RIAU', ' ROKAN HULU', 15659, 18698, 84, 14628, 17856, 82, 14617, 17056, 86, 77247, 80667, 96, 17877, 1388, 1288, 741, 409266, 0, 1445, 29175, 5, 1946, 91273, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 173, 180, 96),
('RIAU', ' BENGKALIS', 11453, 12068, 95, 9074, 10911, 83, 10773, 11531, 93, 47332, 44994, 105, 13264, 11446, 116, 30048, 349352, 9, 10446, 19182, 54, 13384, 89469, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3719, 1648, 226),
('RIAU', ' ROKAN HILIR', 12875, 17984, 72, 12382, 1418, 873, 12038, 12707, 95, 46295, 65791, 70, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6627, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('RIAU', ' KEPULAUAN MERANTI', 3711, 3818, 97, 3291, 369, 892, 3398, 353, 963, 9606, 157, 6118, 3938, 3938, 100, 70716, 107716, 66, 25, 83, 30, 4736, 7806, 61, 0, 0, 0, 0, 0, 0, 0, 0, 0, 705, 4122, 17),
('RIAU', ' KOTA PEKANBARU', 22898, 25377, 90, 21571, 24224, 89, 21098, 21562, 98, 57871, 86214, 67, 34085, 37943, 90, 62465, 743665, 8, 7849, 52461, 15, 12135, 133265, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12135, 75108, 16),
('RIAU', ' KOTA DUMAI', 7713, 8513, 91, 7575, 8126, 93, 7383, 7738, 95, 3812, 39883, 10, 16008, 16383, 98, 103219, 190739, 54, 7798, 13632, 57, 19058, 76791, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6813, 32719, 21),
('JAMBI', ' KERINCI', 2963, 4137, 72, 2065, 3948, 52, 376, 376, 100, 27099, 27099, 100, 58782, 89781, 65, 128125, 15841, 809, 4031, 4301, 94, 6047, 6047, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 100),
('JAMBI', ' MERANGIN', 7905, 8068, 98, 4293, 7702, 56, 7348, 7279, 101, 29606, 37053, 80, 13017, 13017, 100, 0, 4807, 0, 18187, 18187, 100, 927, 20158, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 10, 100),
('JAMBI', ' SAROLANGUN', 6454, 6585, 98, 556, 6285, 9, 6054, 5986, 101, 28952, 29058, 100, 5588, 5642, 99, 28251, 176959, 16, 16, 16286, 0, 40942, 48847, 84, 0, 0, 0, 0, 0, 0, 0, 0, 0, 227, 227, 100),
('JAMBI', ' BATANG HARI', 5144, 58, 8869, 4908, 5537, 89, 4664, 5254, 89, 15222, 20431, 75, 6364, 6364, 100, 35635, 92364, 39, 22533, 22533, 100, 3039, 72318, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 204, 204, 100),
('JAMBI', ' MUARO JAMBI', 9803, 10316, 95, 5915, 6619, 89, 8884, 8899, 100, 37239, 40452, 92, 12876, 13375, 96, 2422, 2422, 100, 22206, 30868, 72, 12742, 14774, 86, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23, 23, 100),
('JAMBI', ' TANJUNG JABUNG TIMUR', 91, 94, 97, 37, 55, 67, 100, 99, 101, 78, 89, 88, 95, 96, 99, 8, 55, 15, 26, 85, 31, 51, 55, 93, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50, 25, 200),
('JAMBI', ' TANJUNG JABUNG BARAT', 6272, 703, 892, 3076, 6711, 46, 5671, 6039, 94, 255, 31024, 1, 11952, 12287, 97, 182813, 217912, 84, 12707, 28388, 45, 24852, 53491, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 964, 1163, 83),
('JAMBI', ' TEBO', 6577, 7753, 85, 6207, 7099, 87, 646, 6994, 9, 22309, 3516, 634, 13425, 22762, 59, 249, 14714, 2, 1124, 2554, 44, 13767, 159316, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 5, 80),
('JAMBI', ' BUNGO', 748, 8407, 9, 6086, 7697, 79, 6761, 7091, 95, 26801, 39772, 67, 11811, 12522, 94, 30781, 233733, 13, 10721, 13351, 80, 3848, 100685, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 995, 9661, 10),
('JAMBI', ' KOTA JAMBI', 13626, 14593, 93, 13039, 13039, 100, 13039, 13039, 100, 54003, 54003, 100, 28786, 28897, 100, 28194, 1295, 2177, 77761, 72384, 107, 13026, 22863, 57, 0, 0, 0, 0, 0, 0, 0, 0, 0, 11852, 11852, 100),
('JAMBI', ' KOTA SUNGAI PENUH', 100, 1778, 6, 100, 1613, 6, 96, 1614, 6, 89, 8535, 1, 75, 3354, 2, 95, 3041, 3, 75, 2009, 4, 83, 8356, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, 4, 2500),
('SUMATERA SELATAN', ' OGAN KOMERING ULU', 7968, 8969, 89, 7752, 8561, 91, 7358, 8163, 90, 26329, 34385, 77, 9745, 29571, 33, 6891, 229818, 3, 14655, 218042, 7, 1017, 8475, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 192, 144, 133),
('SUMATERA SELATAN', ' OGAN KOMERING ILIR', 18261, 18303, 100, 16413, 17471, 94, 17964, 16346, 110, 67282, 6544, 1028, 18072, 22367, 81, 110694, 160594, 69, 57919, 49817, 116, 4359, 137182, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5133, 10044, 51),
('SUMATERA SELATAN', ' MUARA ENIM', 12595, 13755, 92, 12494, 13196, 95, 12203, 13196, 92, 5289, 69385, 8, 10103, 10232, 99, 328995, 373858, 88, 31163, 36576, 85, 5099, 5099, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 9, 100),
('SUMATERA SELATAN', ' LAHAT', 8032, 8519, 94, 7376, 8131, 91, 671, 7761, 9, 23173, 29048, 80, 10059, 10059, 100, 219654, 248465, 88, 4919, 21074, 23, 45659, 72851, 63, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8524, 0),
('SUMATERA SELATAN', ' MUSI RAWAS', 81, 913, 9, 82, 8714, 1, 84, 7841, 1, 66, 41382, 0, 95, 0, 0, 95, 0, 0, 61, 0, 0, 98, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 25, 0, 0),
('SUMATERA SELATAN', ' MUSI BANYUASIN', 2637, 10778, 24, 2663, 10347, 26, 2286, 9774, 23, 10848, 49498, 22, 5927, 30542, 19, 10018, 89775, 11, 5933, 22533, 26, 3846, 1202, 320, 0, 0, 0, 0, 0, 0, 0, 0, 0, 56, 70, 80),
('SUMATERA SELATAN', ' BANYU ASIN', 15961, 18215, 88, 14993, 17671, 85, 14929, 16123, 93, 665, 84526, 1, 16559, 19375, 85, 67804, 173436, 39, 59824, 59824, 100, 30599, 42144, 73, 0, 0, 0, 0, 0, 0, 0, 0, 0, 14, 12, 117),
('SUMATERA SELATAN', ' OGAN KOMERING ULU SELATAN', 8035, 8478, 95, 7675, 8093, 95, 7753, 7707, 101, 8274, 37504, 22, 5717, 19823, 29, 0, 75352, 0, 0, 0, 0, 341, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('SUMATERA SELATAN', ' OGAN KOMERING ULU TIMUR', 14156, 14156, 100, 13512, 13512, 100, 13084, 13084, 100, 60342, 60342, 100, 17788, 23988, 74, 83583, 83583, 100, 50938, 50938, 100, 328, 213, 154, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6, 100),
('SUMATERA SELATAN', ' OGAN ILIR', 10003, 10277, 97, 9546, 9808, 97, 9273, 9335, 99, 4386, 4474, 98, 8341, 8501, 98, 153671, 281924, 55, 20711, 27477, 75, 42145, 82385, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1186, 11712, 10),
('SUMATERA SELATAN', ' EMPAT LAWANG', 5119, 5794, 88, 4746, 553, 858, 4692, 5267, 89, 163076, 30812, 529, 4231, 29202, 14, 432, 11347, 4, 4691, 14109, 33, 5503, 9464, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 39, 6009, 1),
('SUMATERA SELATAN', ' PENUKAL ABAB LEMATANG ILIR', 0, 1000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('SUMATERA SELATAN', ' MUSI RAWAS UTARA', 4344, 447, 972, 375, 4267, 9, 3699, 4064, 91, 16232, 24845, 65, 0, 0, 0, 0, 0, 0, 397, 6537, 6, 463, 463, 100, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 100),
('SUMATERA SELATAN', ' KOTA PALEMBANG', 29305, 2961, 990, 27881, 28103, 99, 26487, 26941, 98, 100026, 108791, 92, 30787, 30787, 100, 134725, 0, 0, 104657, 13421, 780, 29255, 228713, 13, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 22966, 1693, 1357),
('SUMATERA SELATAN', ' KOTA PRABUMULIH', 3816, 3952, 97, 3561, 3772, 94, 3534, 3592, 98, 18153, 18416, 99, 7313, 7352, 99, 53317, 72362, 74, 10596, 15052, 70, 17287, 3863, 448, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 2405, 259, 929),
('SUMATERA SELATAN', ' KOTA PAGAR ALAM', 3199, 336, 952, 306, 3212, 10, 2972, 3011, 99, 23757, 22489, 106, 4931, 22294, 22, 61002, 97376, 63, 7898, 12637, 62, 12608, 20954, 60, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3761, 0),
('SUMATERA SELATAN', ' KOTA LUBUKLINGGAU', 4999, 5121, 98, 4408, 4598, 96, 4354, 444, 981, 17511, 19533, 90, 10381, 10805, 96, 71181, 76818, 93, 11367, 14937, 76, 25988, 42592, 61, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 1934, 2081, 93),
('BENGKULU', ' BENGKULU SELATAN', 2738, 1000, 274, 28, 0, 0, 971, 0, 0, 5033, 0, 0, 212, 0, 0, 0, 0, 0, 1056, 0, 0, 2761, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('BENGKULU', ' REJANG LEBONG', 4916, 5237, 94, 4617, 4999, 92, 457, 4762, 10, 22381, 23224, 96, 523, 5336, 10, 8597, 8597, 100, 3386, 3386, 100, 628, 628, 100, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 2139, 2139, 100),
('BENGKULU', ' BENGKULU UTARA', 6078, 6581, 92, 533, 6282, 8, 5666, 5983, 95, 18697, 2918, 641, 0, 0, 0, 0, 98374, 0, 17844, 22384, 80, 1875, 1875, 100, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 813, 7116, 11),
('BENGKULU', ' KAUR', 2475, 2617, 95, 2364, 2364, 100, 2232, 2393, 93, 3471, 13461, 26, 1284, 14012, 9, 10268, 74833, 14, 889, 8677, 10, 817, 1647, 50, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('BENGKULU', ' SELUMA', 3212, 4049, 79, 2625, 3865, 68, 2795, 3464, 81, 10551, 17954, 59, 5714, 5714, 100, 141649, 141649, 100, 20828, 14865, 140, 78, 78, 100, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 69, 69, 100),
('BENGKULU', ' MUKOMUKO', 60, 65, 92, 73, 74, 99, 58, 73, 79, 87, 84, 104, 84, 84, 100, 60, 23, 261, 100, 25, 400, 100, 35, 286, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 80, 57, 140),
('BENGKULU', ' LEBONG', 1846, 2223, 83, 1819, 2122, 86, 921, 2122, 43, 2381, 9859, 24, 194, 194, 100, 0, 72956, 0, 1172, 912, 129, 814, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('BENGKULU', ' KEPAHIANG', 2375, 2726, 87, 2377, 2474, 96, 2487, 2494, 100, 8357, 10912, 77, 5279, 5298, 100, 59597, 67469, 88, 4484, 13095, 34, 2769, 2769, 100, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 100),
('BENGKULU', ' BENGKULU TENGAH', 2115, 2485, 85, 1815, 2371, 77, 2087, 2259, 92, 5387, 885, 609, 2453, 2453, 100, 22328, 76976, 29, 1878, 7513, 25, 3083, 19244, 16, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 475, 650, 73),
('BENGKULU', ' KOTA BENGKULU', 6267, 1000, 627, 6717, 0, 0, 6698, 0, 0, 14851, 0, 0, 5998, 0, 0, 0, 0, 0, 12589, 0, 0, 3149, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10435, 0, 0),
('LAMPUNG', ' LAMPUNG BARAT', 1264, 6497, 19, 819, 6201, 13, 1065, 5906, 18, 3632, 35078, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 175, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1478, 0),
('LAMPUNG', ' TANGGAMUS', 11565, 12119, 95, 10459, 11644, 90, 10988, 1109, 991, 39054, 55504, 70, 20266, 20638, 98, 45494, 37117, 123, 2469, 49544, 5, 37399, 131683, 28, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 2174, 12723, 17),
('LAMPUNG', ' LAMPUNG SELATAN', 21731, 21296, 102, 20708, 21296, 97, 20339, 21245, 96, 75076, 78135, 96, 6883, 76477, 9, 31454, 631622, 5, 39615, 68302, 58, 25081, 28398, 88, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 14337, 16546, 87),
('LAMPUNG', ' LAMPUNG TIMUR', 1915, 20095, 10, 17896, 19181, 93, 17719, 18268, 97, 61738, 73893, 84, 39499, 41609, 95, 50993, 648454, 8, 108487, 142162, 76, 31666, 31666, 100, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 10945, 10945, 100),
('LAMPUNG', ' LAMPUNG TENGAH', 2216, 24963, 9, 10175, 23829, 43, 9778, 22292, 44, 34143, 113579, 30, 22287, 135258, 16, 315, 803917, 0, 23165, 118441, 20, 99, 214974, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 44, 87, 51),
('LAMPUNG', ' LAMPUNG UTARA', 11985, 13235, 91, 11423, 12634, 90, 11461, 12032, 95, 40118, 61664, 65, 39996, 70104, 57, 416928, 403693, 103, 2417, 48005, 5, 9581, 3033, 316, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 595, 7, 8500),
('LAMPUNG', ' WAY KANAN', 4141, 1000, 414, 4141, 0, 0, 4267, 0, 0, 17843, 0, 0, 5919, 0, 0, 3107, 293982, 1, 0, 0, 0, 2369, 75848, 3, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('LAMPUNG', ' TULANGBAWANG', 9412, 10097, 93, 855, 9642, 9, 8626, 9179, 94, 37157, 54023, 69, 2821, 2821, 100, 277735, 292353, 95, 5733, 26499, 22, 8407, 13905, 60, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 78, 36, 217),
('LAMPUNG', ' PESAWARAN', 8364, 8821, 95, 8028, 8419, 95, 7353, 8017, 92, 37595, 51253, 73, 8041, 8041, 100, 34236, 298588, 11, 3907, 29629, 13, 13004, 55594, 23, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 13, 13, 100),
('LAMPUNG', ' PRINGSEWU', 7678, 7678, 100, 7329, 7329, 100, 6729, 6729, 100, 34936, 34936, 100, 21684, 21684, 100, 6856, 66963, 10, 38304, 38303, 100, 10108, 56988, 18, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 120, 0),
('LAMPUNG', ' MESUJI', 3704, 1000, 370, 3512, 0, 0, 4887, 0, 0, 4887, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('LAMPUNG', ' TULANG BAWANG BARAT', 5464, 5685, 96, 505, 5426, 9, 5055, 5168, 98, 17203, 31653, 54, 56562, 56562, 100, 0, 156, 0, 0, 23668, 0, 3079, 42575, 7, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 23, 23, 100),
('LAMPUNG', ' PESISIR BARAT', 2533, 3562, 71, 2289, 3344, 68, 2389, 3185, 75, 8787, 16206, 54, 11442, 21934, 52, 2576, 49859, 5, 2284, 7131, 32, 1791, 55725, 3, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 3562, 0),
('LAMPUNG', ' KOTA BANDAR LAMPUNG', 18869, 20216, 93, 17962, 19297, 93, 1634, 18227, 9, 62614, 91981, 68, 32789, 35029, 94, 420499, 728291, 58, 32882, 42955, 77, 33521, 159366, 21, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 11129, 18302, 61),
('LAMPUNG', ' KOTA METRO', 2958, 2958, 100, 2823, 2823, 100, 2689, 2689, 100, 13458, 13458, 100, 2937, 2937, 100, 113847, 11347, 1003, 11413, 12681, 90, 21744, 21744, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 260, 260, 100),
('KEPULAUAN BANGKA BELITUNG', ' BANGKA', 5989, 6502, 92, 5937, 6207, 96, 5772, 5948, 97, 27436, 29805, 92, 12111, 11946, 101, 50789, 205494, 25, 14565, 21944, 66, 12518, 22256, 56, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 5284, 5323, 99),
('KEPULAUAN BANGKA BELITUNG', ' BELITUNG', 2845, 3327, 86, 221, 3176, 7, 2834, 3025, 94, 8434, 1313, 642, 6413, 6413, 100, 33796, 121541, 28, 11974, 15801, 76, 17374, 57059, 30, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 2051, 4075, 50),
('KEPULAUAN BANGKA BELITUNG', ' BANGKA BARAT', 3975, 4097, 97, 3839, 3903, 98, 3806, 3797, 100, 16959, 17366, 98, 7958, 7958, 100, 75592, 127515, 59, 8354, 12076, 69, 21723, 49834, 44, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 4019, 4725, 85),
('KEPULAUAN BANGKA BELITUNG', ' BANGKA TENGAH', 343, 3749, 9, 3245, 3578, 91, 3246, 3327, 98, 10092, 1337, 755, 6966, 729, 956, 80763, 124404, 65, 7864, 1018, 772, 166, 27618, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 1537, 3796, 40),
('KEPULAUAN BANGKA BELITUNG', ' BANGKA SELATAN', 3445, 3787, 91, 2764, 3648, 76, 3267, 3589, 91, 12541, 17912, 70, 7852, 7466, 105, 4942, 110273, 4, 6938, 1299, 534, 6917, 11468, 60, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 1031, 3867, 27),
('KEPULAUAN BANGKA BELITUNG', ' BELITUNG TIMUR', 2083, 2398, 87, 1859, 229, 812, 1997, 2087, 96, 10728, 12404, 86, 4309, 4288, 100, 31358, 78201, 40, 6068, 1076, 564, 1559, 26598, 6, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 2431, 2431, 100),
('KEPULAUAN BANGKA BELITUNG', ' KOTA PANGKAL PINANG', 4272, 4525, 94, 4172, 4312, 97, 4161, 411, 1012, 14546, 16277, 89, 7886, 7922, 100, 27313, 135618, 20, 7452, 17201, 43, 14471, 19797, 73, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 2105, 5673, 37),
('KEPULAUAN RIAU', ' KARIMUN', 3872, 4934, 78, 4007, 4707, 85, 3911, 4473, 87, 11116, 18462, 60, 4599, 42273, 11, 95212, 164972, 58, 8408, 19688, 43, 13279, 13286, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 10086, 10086, 100),
('KEPULAUAN RIAU', ' BINTAN', 3066, 3393, 90, 3017, 3193, 94, 2922, 3008, 97, 11695, 13964, 84, 6009, 6009, 100, 11303, 94104, 12, 6812, 9194, 74, 11831, 21228, 56, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 24, 24, 100),
('KEPULAUAN RIAU', ' NATUNA', 1411, 1587, 89, 1266, 1487, 85, 1243, 1487, 84, 4428, 9865, 45, 1509, 1601, 94, 709, 51856, 1, 1368, 552, 248, 7523, 26721, 28, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 148, 148, 100),
('KEPULAUAN RIAU', ' LINGGA', 1553, 1683, 92, 1436, 1607, 89, 1318, 1528, 86, 7231, 9971, 73, 1817, 14399, 13, 0, 33924, 0, 0, 7704, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('KEPULAUAN RIAU', ' KEPULAUAN ANAMBAS', 807, 1118, 72, 822, 1066, 77, 751, 842, 89, 4419, 5813, 76, 1784, 1784, 100, 0, 0, 0, 2259, 3313, 68, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('KEPULAUAN RIAU', ' KOTA BATAM', 29359, 31757, 92, 28582, 30314, 94, 24093, 28507, 85, 80545, 156935, 51, 38424, 4073, 943, 54524, 1108429, 5, 19834, 65187, 30, 13457, 183741, 7, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 6102, 37112, 16),
('KEPULAUAN RIAU', ' KOTA TANJUNG PINANG', 4308, 4576, 94, 4047, 437, 926, 3905, 4264, 92, 16097, 21926, 73, 6976, 8354, 84, 109675, 176162, 62, 13795, 16608, 83, 10295, 44796, 23, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 6369, 7, 90986),
('DKI JAKARTA', ' KEPULAUAN SERIBU', 443, 502, 88, 505, 480, 105, 504, 457, 110, 1826, 2071, 88, 894, 980, 91, 564, 150171, 0, 679, 1372, 49, 339, 339, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 12, 12, 100),
('DKI JAKARTA', ' KOTA JAKARTA SELATAN', 46506, 47186, 99, 44204, 4505, 981, 44237, 42896, 103, 154404, 161505, 96, 6302, 64434, 10, 40401, 379545, 11, 171965, 203218, 85, 11096, 446523, 2, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 38906, 49503, 79),
('DKI JAKARTA', ' KOTA JAKARTA TIMUR', 0, 65348, 0, 0, 62378, 0, 0, 59406, 0, 0, 224687, 0, 0, 0, 0, 0, 1976719, 0, 0, 172856, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('DKI JAKARTA', ' KOTA JAKARTA PUSAT', 14863, 16392, 91, 13727, 15648, 88, 13292, 14903, 89, 46163, 59751, 77, 15288, 15153, 101, 18675, 61558, 30, 107918, 103188, 105, 8567, 137977, 6, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 2335, 26822, 9),
('DKI JAKARTA', ' KOTA JAKARTA BARAT', 53397, 46702, 114, 51441, 4458, 1154, 47356, 47526, 100, 177304, 280591, 63, 32061, 228193, 14, 0, 872977, 0, 0, 9509, 0, 8764, 8764, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 61366, 67703, 91),
('DKI JAKARTA', ' KOTA JAKARTA UTARA', 38605, 38965, 99, 3693, 37194, 10, 35266, 35422, 100, 134132, 134455, 100, 42938, 44247, 97, 7442, 1240649, 1, 64413, 10195, 632, 3857, 357195, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 33418, 40362, 83),
('JAWA BARAT', ' BOGOR', 124621, 127203, 98, 114645, 121421, 94, 118225, 131446, 90, 415573, 461969, 90, 221769, 268866, 82, 1586042, 3687447, 43, 102781, 160528, 64, 162865, 857552, 19, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 45, 20442, 0),
('JAWA BARAT', ' SUKABUMI', 40679, 50252, 81, 37502, 47968, 78, 40996, 45684, 90, 138837, 225465, 62, 98572, 4645, 2122, 533753, 1610797, 33, 10112, 23611, 43, 50288, 517895, 10, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 8066, 65909, 12),
('JAWA BARAT', ' CIANJUR', 584, 1498, 39, 574, 1455, 39, 545, 1675, 33, 537, 1784, 30, 487, 2145, 23, 447, 2247, 20, 256, 7412, 3, 314, 758, 41, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 18, 28, 64),
('JAWA BARAT', ' BANDUNG', 70841, 75956, 93, 6451, 70119, 9, 64298, 69205, 93, 277038, 322941, 86, 101963, 114793, 89, 103286, 2531805, 4, 5832, 212166, 3, 96207, 194444, 49, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 16385, 82127, 20),
('JAWA BARAT', ' GARUT', 30267, 55056, 55, 21355, 54014, 40, 26615, 51367, 52, 19616, 254528, 8, 24829, 37464, 66, 6219, 26589, 23, 271609, 284559, 95, 57843, 181219, 32, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 5436, 8, 67950),
('JAWA BARAT', ' TASIKMALAYA', 33085, 34818, 95, 30073, 33236, 90, 30947, 31653, 98, 138811, 184642, 75, 32318, 32318, 100, 59082, 1117030, 5, 60529, 194889, 31, 27338, 27338, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 5546, 39121, 14),
('JAWA BARAT', ' CIAMIS', 18862, 20275, 93, 18357, 1877, 978, 18571, 16974, 109, 69187, 70821, 98, 36168, 45869, 79, 1132554, 69983, 1618, 12674, 213594, 6, 38057, 180556, 21, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 983, 1622, 61),
('JAWA BARAT', ' KUNINGAN', 1899, 23367, 8, 19421, 22305, 87, 19249, 21243, 91, 55183, 58625, 94, 39155, 39899, 98, 327385, 724874, 45, 82113, 131806, 62, 67142, 221023, 30, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 14071, 14072, 100),
('JAWA BARAT', ' CIREBON', 50176, 5216, 962, 47238, 47234, 100, 49442, 47115, 105, 143752, 190872, 75, 41083, 47905, 86, 2077, 1447861, 0, 31508, 675906, 5, 50114, 412023, 12, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 13986, 12851, 109),
('JAWA BARAT', ' MAJALENGKA', 20628, 21403, 96, 19849, 2043, 972, 2021, 20224, 10, 79842, 77091, 104, 19525, 22068, 88, 59176, 741654, 8, 67672, 127833, 53, 10893, 307981, 4, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 9332, 11121, 84),
('JAWA BARAT', ' SUMEDANG', 19664, 20023, 98, 19635, 19907, 99, 19618, 19897, 99, 89836, 89836, 100, 33489, 3662, 915, 37984, 775569, 5, 142205, 154559, 92, 88452, 88452, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 55, 55, 100),
('JAWA BARAT', ' INDRAMAYU', 33453, 43102, 78, 32207, 32207, 100, 3202, 41143, 8, 112231, 187866, 60, 43702, 51054, 86, 15367, 1155512, 1, 23841, 152215, 16, 38913, 38913, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 685971, 685971, 100),
('JAWA BARAT', ' SUBANG', 25774, 27403, 94, 22304, 26639, 84, 21593, 24776, 87, 124889, 150374, 83, 49946, 49946, 100, 78614, 997021, 8, 191228, 192481, 99, 18366, 18366, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 16538, 60412, 27),
('JAWA BARAT', ' PURWAKARTA', 1984, 20106, 10, 18964, 19192, 99, 18935, 1816, 1043, 4486, 88972, 5, 28756, 33018, 87, 5486, 632096, 1, 53932, 81235, 66, 3334, 7269, 46, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 4218, 7114, 59),
('JAWA BARAT', ' KARAWANG', 45068, 4606, 978, 44691, 43967, 102, 44769, 41873, 107, 123243, 16452, 749, 73495, 7477, 983, 32566, 1493742, 2, 162984, 119719, 136, 56072, 395842, 14, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 7341, 19458, 38),
('JAWA BARAT', ' BEKASI', 73617, 76617, 96, 70271, 73824, 95, 72452, 75978, 95, 183589, 669242, 27, 57002, 63021, 90, 1115212, 2623813, 43, 166805, 260591, 64, 161758, 695003, 23, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 285, 285, 100),
('JAWA BARAT', ' BANDUNG BARAT', 2996, 34947, 9, 27972, 33358, 84, 27876, 4738, 588, 66699, 125748, 53, 0, 0, 0, 0, 0, 0, 172, 131188, 0, 4697, 336669, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('JAWA BARAT', ' PANGANDARAN', 672, 672, 100, 6414, 6414, 100, 6044, 6044, 100, 24173, 24173, 100, 462, 462, 100, 281422, 281422, 100, 6207, 6207, 100, 132, 132, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 1357, 1357, 100),
('JAWA BARAT', ' KOTA BOGOR', 20604, 21324, 97, 20354, 20354, 100, 19241, 19638, 98, 75789, 75789, 100, 19823, 20867, 95, 70765, 70765, 100, 94329, 94329, 100, 12658, 202542, 6, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 22053, 22469, 98),
('JAWA BARAT', ' KOTA SUKABUMI', 6247, 6461, 97, 601, 6168, 10, 5727, 5874, 97, 23107, 29282, 79, 12636, 12954, 98, 181024, 21754, 832, 9783, 21754, 45, 20373, 32422, 63, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 7829, 7829, 100),
('JAWA BARAT', ' KOTA BANDUNG', 44711, 1000, 4471, 4126, 0, 0, 0, 0, 0, 192661, 0, 0, 0, 0, 0, 0, 1374656, 0, 0, 0, 0, 0, 417977, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('JAWA BARAT', ' KOTA CIREBON', 1997, 4469, 45, 1914, 4376, 44, 199, 4461, 4, 6132, 7831, 78, 9237, 9611, 96, 19995, 219245, 9, 10655, 27561, 39, 899, 3345, 27, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 3434, 316, 1087),
('JAWA BARAT', ' KOTA BEKASI', 51732, 56143, 92, 47152, 53591, 88, 45747, 47065, 97, 139339, 251895, 55, 80274, 8354, 961, 886708, 1988562, 45, 72914, 110636, 66, 28407, 538848, 5, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 10881, 21693, 50),
('JAWA BARAT', ' KOTA DEPOK', 44727, 4684, 955, 42709, 44711, 96, 40356, 42665, 95, 153482, 165446, 93, 55889, 64319, 87, 380384, 1492623, 25, 65463, 118404, 55, 56298, 428762, 13, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 12946, 13002, 100),
('JAWA BARAT', ' KOTA CIMAHI', 10583, 11875, 89, 10407, 11334, 92, 9996, 10974, 91, 22884, 42225, 54, 10583, 10583, 100, 195585, 409029, 48, 21178, 4816, 440, 30139, 115212, 26, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 4741, 16724, 28),
('JAWA BARAT', ' KOTA TASIKMALAYA', 12397, 13125, 94, 11834, 12653, 94, 11919, 1165, 1023, 52193, 46256, 113, 0, 11567, 0, 0, 213966, 0, 0, 25217, 0, 0, 69611, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 7564, 21464, 35),
('JAWA BARAT', ' KOTA BANJAR', 3304, 3491, 95, 3154, 3259, 97, 3095, 3247, 95, 13683, 1248, 1096, 110, 74851, 0, 5349, 133387, 4, 2685, 7158, 38, 12133, 38439, 32, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 102, 2, 5100),
('JAWA TENGAH', ' CILACAP', 2958, 31186, 9, 28579, 28594, 100, 28465, 28481, 100, 92234, 108817, 85, 58537, 59532, 98, 423587, 1104882, 38, 104943, 175906, 60, 13164, 113523, 12, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 40875, 59729, 68),
('JAWA TENGAH', ' BANYUMAS', 27896, 2932, 951, 24542, 27834, 88, 25291, 27477, 92, 94156, 101881, 92, 39082, 5716, 684, 1574001, 1574087, 100, 129546, 175289, 74, 34528, 149538, 23, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 31069, 32, 97091),
('JAWA TENGAH', ' PURBALINGGA', 15375, 16064, 96, 14206, 14352, 99, 13979, 14331, 98, 52211, 5759, 907, 3376, 3376, 100, 554049, 554049, 100, 56677, 108317, 52, 2689, 59487, 5, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 439, 1631, 27),
('JAWA TENGAH', ' BANJARNEGARA', 15044, 17349, 87, 15164, 15389, 99, 15025, 15255, 98, 58801, 68701, 86, 12423, 1351, 920, 9665, 632168, 2, 14172, 107425, 13, 29223, 72965, 40, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 27, 19413, 0),
('JAWA TENGAH', ' KEBUMEN', 20596, 21485, 96, 19538, 20447, 96, 19649, 19668, 100, 76076, 80805, 94, 21037, 21037, 100, 110081, 888131, 12, 73334, 152834, 48, 2309, 888131, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 25046, 25046, 100),
('JAWA TENGAH', ' PURWOREJO', 8732, 9599, 91, 8587, 8673, 99, 8492, 8682, 98, 34845, 38475, 91, 2353, 23646, 10, 57715, 4633, 1246, 72215, 112006, 64, 17937, 75751, 24, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 768, 9863, 8),
('JAWA TENGAH', ' WONOSOBO', 12225, 13679, 89, 12501, 12537, 100, 124, 12572, 1, 60526, 78067, 78, 27257, 28088, 97, 43039, 487028, 9, 86173, 98636, 87, 22316, 171582, 13, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 7136, 1814, 393),
('JAWA TENGAH', ' MAGELANG', 17491, 19448, 90, 17114, 17122, 100, 16632, 17192, 97, 50213, 77138, 65, 21649, 21649, 100, 376517, 705993, 53, 49355, 157001, 31, 41099, 133541, 31, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 13198, 11902, 111),
('JAWA TENGAH', ' BOYOLALI', 29466, 15601, 189, 28567, 14312, 200, 28488, 14306, 199, 131911, 72505, 182, 75262, 42399, 178, 1368303, 639119, 214, 20677, 142823, 14, 417896, 219, 190820, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 1227, 9768, 13),
('JAWA TENGAH', ' KLATEN', 16075, 17312, 93, 16086, 16086, 100, 15575, 15963, 98, 64451, 67665, 95, 113852, 113852, 100, 229246, 7274, 3152, 144677, 168428, 86, 53362, 113561, 47, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 11595, 17892, 65),
('JAWA TENGAH', ' SUKOHARJO', 12803, 13717, 93, 12538, 12538, 100, 12522, 12522, 100, 42644, 50056, 85, 17131, 17131, 100, 42644, 531318, 8, 58098, 95722, 61, 20906, 26565, 79, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 8481, 8481, 100),
('JAWA TENGAH', ' WONOGIRI', 11175, 12275, 91, 10799, 10799, 100, 10492, 10801, 97, 4762, 56821, 8, 35163, 35163, 100, 312978, 549121, 57, 88306, 117777, 75, 45006, 83703, 54, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 7178, 1107, 648),
('JAWA TENGAH', ' KARANGANYAR', 12773, 13645, 94, 12387, 12387, 100, 1225, 12404, 10, 42038, 49616, 85, 22012, 22012, 100, 126095, 583356, 22, 202665, 232451, 87, 32905, 245938, 13, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 9798, 9798, 100),
('JAWA TENGAH', ' SRAGEN', 1388, 15172, 9, 13739, 1375, 999, 1336, 13761, 10, 48469, 54812, 88, 48155, 48155, 100, 26301, 548799, 5, 53182, 10787, 493, 116875, 138297, 85, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 15534, 18838, 82),
('JAWA TENGAH', ' GROBOGAN', 2056, 20881, 10, 21501, 20603, 104, 2124, 21774, 10, 78386, 94108, 83, 23908, 23908, 100, 3256, 922825, 0, 19266, 21599, 89, 5078, 92282, 6, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 19479, 24912, 78),
('JAWA TENGAH', ' BLORA', 12855, 13355, 96, 11976, 12857, 93, 11779, 11937, 99, 43849, 4662, 941, 13636, 13636, 100, 23447, 54652, 43, 26054, 115091, 23, 17222, 21861, 79, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 6426, 16038, 40),
('JAWA TENGAH', ' REMBANG', 8738, 9668, 90, 9003, 9005, 100, 8852, 8981, 99, 40951, 44233, 93, 16249, 16784, 97, 295067, 308913, 96, 47599, 49218, 97, 16427, 16427, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 9962, 10226, 97),
('JAWA TENGAH', ' PATI', 17758, 18666, 95, 17609, 1761, 1000, 17338, 17467, 99, 77407, 88937, 87, 19023, 19023, 100, 208355, 797738, 26, 78709, 170848, 46, 34963, 78433, 45, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 172, 172, 100),
('JAWA TENGAH', ' KUDUS', 15836, 16503, 96, 15227, 15227, 100, 15152, 15495, 98, 54789, 64396, 85, 38683, 39348, 98, 1976, 561618, 0, 13037, 83157, 16, 7079, 161839, 4, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 1778, 4207, 42),
('JAWA TENGAH', ' JEPARA', 22576, 22576, 100, 20674, 20674, 100, 20721, 20721, 100, 77597, 81541, 95, 20846, 20846, 100, 113577, 132576, 86, 19839, 109502, 18, 38097, 64395, 59, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 9531, 9531, 100),
('JAWA TENGAH', ' DEMAK', 2172, 2211, 98, 20817, 20817, 100, 20792, 20853, 100, 79402, 79402, 100, 20211, 20211, 100, 281107, 816953, 34, 276752, 276752, 100, 59579, 78319, 76, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 20788, 22043, 94),
('JAWA TENGAH', ' SEMARANG', 13015, 14764, 88, 12526, 14086, 89, 1293, 1352, 96, 52597, 5408, 973, 30883, 30612, 101, 147066, 670872, 22, 108612, 108998, 100, 102705, 206116, 50, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 10764, 10764, 100),
('JAWA TENGAH', ' TEMANGGUNG', 10576, 10576, 100, 10316, 10316, 100, 10377, 10377, 100, 43365, 43365, 100, 24382, 24382, 100, 372315, 511942, 73, 73379, 73379, 100, 22989, 62535, 37, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 2466, 1533, 161),
('JAWA TENGAH', ' KENDAL', 1603, 17143, 9, 1532, 1535, 100, 15011, 15232, 99, 75056, 75056, 100, 20472, 20472, 100, 10114, 243943, 4, 27553, 27686, 100, 25717, 95664, 27, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 13949, 13949, 100),
('JAWA TENGAH', ' BATANG', 12566, 1338, 939, 12141, 12567, 97, 12466, 12572, 99, 5023, 60717, 8, 24723, 24917, 99, 256344, 489899, 52, 5887, 109281, 5, 18819, 100084, 19, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 8626, 16899, 51),
('JAWA TENGAH', ' PEKALONGAN', 16024, 16966, 94, 15893, 16235, 98, 15964, 15584, 102, 50483, 65525, 77, 30583, 32271, 95, 328167, 457612, 72, 55119, 80654, 68, 5668, 56562, 10, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 16318, 18264, 89),
('JAWA TENGAH', ' PEMALANG', 25901, 2831, 915, 24585, 24932, 99, 24751, 24934, 99, 114501, 12219, 937, 52753, 58281, 91, 7215, 805579, 1, 77061, 140015, 55, 59188, 242167, 24, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 15898, 16992, 94),
('JAWA TENGAH', ' TEGAL', 26879, 29219, 92, 26647, 27674, 96, 26579, 2658, 1000, 97882, 102743, 95, 25813, 26268, 98, 6038, 1039969, 1, 13542, 60405, 22, 23565, 98927, 24, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 33267, 35324, 94),
('JAWA TENGAH', ' BREBES', 32082, 36352, 88, 3257, 32611, 10, 31838, 32594, 98, 103442, 123827, 84, 50071, 73382, 68, 9221, 1033164, 1, 77605, 203728, 38, 106967, 333104, 32, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 21638, 38, 56942),
('JAWA TENGAH', ' KOTA MAGELANG', 1597, 1706, 94, 1564, 1564, 100, 1546, 1559, 99, 5762, 6109, 94, 2478, 2478, 100, 28099, 88299, 32, 11284, 16096, 70, 3158, 13125, 24, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 2939, 2939, 100),
('JAWA TENGAH', ' KOTA SURAKARTA', 10586, 10867, 97, 9883, 9883, 100, 9896, 9896, 100, 39512, 39512, 100, 21052, 21052, 100, 38532, 38532, 100, 50247, 50247, 100, 18471, 28243, 65, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 6091, 11975, 51),
('JAWA TENGAH', ' KOTA SALATIGA', 2701, 2928, 92, 2533, 2533, 100, 2359, 2533, 93, 116, 11648, 1, 7935, 7935, 100, 52961, 124408, 43, 16981, 21667, 78, 2028, 46433, 4, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 3417, 3998, 85),
('JAWA TENGAH', ' KOTA SEMARANG', 2806, 28758, 10, 26149, 26154, 100, 24756, 26052, 95, 124017, 130535, 95, 49979, 50208, 100, 404694, 1110017, 36, 189699, 184249, 103, 83668, 33808, 247, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 31698, 35397, 90),
('JAWA TENGAH', ' KOTA PEKALONGAN', 6134, 6343, 97, 5864, 5864, 100, 5801, 5839, 99, 23637, 25681, 92, 5682, 5682, 100, 183001, 206091, 89, 1787, 27824, 6, 14085, 13881, 101, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 7096, 8083, 88),
('JAWA TENGAH', ' KOTA TEGAL', 4656, 4938, 94, 4364, 4365, 100, 4345, 4352, 100, 25076, 25076, 100, 3445, 4219, 82, 30888, 178121, 17, 1501, 1577, 95, 8272, 21505, 38, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 6257, 6703, 93),
('D I YOGYAKARTA', ' KULON PROGO', 0, 5889, 0, 0, 5621, 0, 0, 5354, 0, 0, 27576, 0, 0, 0, 0, 0, 271255, 0, 0, 79824, 0, 0, 111985, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('D I YOGYAKARTA', ' BANTUL', 13132, 1472, 892, 12373, 12375, 100, 10575, 12355, 86, 44029, 48774, 90, 26762, 26754, 100, 34509, 34509, 100, 1596, 16618, 10, 34509, 34509, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 880, 880, 100),
('D I YOGYAKARTA', ' GUNUNG KIDUL', 7712, 8965, 86, 7332, 734, 999, 693, 7339, 9, 36125, 39773, 91, 1059, 1059, 100, 51025, 65274, 78, 22077, 62137, 36, 694, 102667, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 301, 301, 100),
('D I YOGYAKARTA', ' SLEMAN', 14932, 14932, 100, 14014, 14015, 100, 14023, 14025, 100, 72598, 74746, 97, 31095, 32029, 97, 494602, 683728, 72, 80809, 106127, 76, 81944, 199387, 41, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 17136, 11812, 145),
('D I YOGYAKARTA', ' KOTA YOGYAKARTA', 3809, 4228, 90, 3617, 3617, 100, 3235, 3621, 89, 1184, 17663, 7, 14157, 14947, 95, 8209, 8209, 100, 38352, 48766, 79, 11967, 11967, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 8906, 8906, 100),
('JAWA TIMUR', ' PACITAN', 6213, 7554, 82, 594, 721, 82, 5849, 6867, 85, 31761, 3425, 927, 1429, 1429, 100, 57809, 340053, 17, 31565, 10275, 307, 30807, 176677, 17, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 4695, 7889, 60),
('JAWA TIMUR', ' PONOROGO', 10081, 12114, 83, 10305, 11564, 89, 10297, 11013, 93, 44064, 54931, 80, 22301, 26485, 84, 37349, 544277, 7, 69572, 153129, 45, 28741, 158929, 18, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 5857, 13544, 43),
('JAWA TIMUR', ' TRENGGALEK', 8485, 9937, 85, 8584, 9486, 90, 8633, 9036, 96, 39073, 45061, 87, 19292, 19318, 100, 139198, 477371, 29, 6508, 10895, 60, 15678, 52884, 30, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 708, 6954, 10),
('JAWA TIMUR', ' TULUNGAGUNG', 15208, 16833, 90, 14625, 16068, 91, 142, 15303, 1, 65346, 76327, 86, 3146, 31469, 10, 614856, 649229, 95, 80403, 149504, 54, 306569, 322637, 95, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 10792, 15244, 71),
('JAWA TIMUR', ' BLITAR', 1569, 18674, 8, 1537, 17825, 9, 15157, 16976, 89, 68335, 84676, 81, 35595, 36353, 98, 503178, 774012, 65, 132553, 180123, 74, 69426, 274472, 25, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 12379, 19342, 64),
('JAWA TIMUR', ' KEDIRI', 25013, 26927, 93, 24259, 25703, 94, 23626, 24479, 97, 103656, 122097, 85, 47674, 49756, 96, 281062, 992177, 28, 14325, 205488, 7, 53409, 276818, 19, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 25388, 28928, 88),
('JAWA TIMUR', ' MALANG', 40754, 42602, 96, 39617, 40665, 97, 38265, 38729, 99, 167143, 193174, 87, 80101, 80101, 100, 1551170, 1551170, 100, 200198, 331772, 60, 297295, 721014, 41, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 21248, 46567, 46),
('JAWA TIMUR', ' LUMAJANG', 14565, 15915, 92, 15067, 15191, 99, 14417, 14468, 100, 63921, 72165, 89, 29692, 31402, 95, 255095, 669941, 38, 76115, 137403, 55, 47848, 183267, 26, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 3119, 2198, 142),
('JAWA TIMUR', ' JEMBER', 35264, 39839, 89, 33405, 38028, 88, 32974, 36217, 91, 153917, 180645, 85, 72912, 73942, 99, 760788, 1548130, 49, 198074, 314763, 63, 86478, 510432, 17, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 3254, 40455, 8),
('JAWA TIMUR', ' BANYUWANGI', 24428, 25159, 97, 23082, 24015, 96, 22285, 2308, 966, 96307, 11408, 844, 51366, 52025, 99, 506152, 1012303, 50, 115955, 230701, 50, 49389, 49389, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 28629, 28629, 100);
INSERT INTO `tempdata` (`Provinsi`, `kabkota`, `r1`, `s1`, `p1`, `r2`, `s2`, `p2`, `r3`, `s3`, `p3`, `r4`, `s4`, `p4`, `r5`, `s5`, `p5`, `r6`, `s6`, `p6`, `r7`, `s7`, `p7`, `r8`, `s8`, `p8`, `r9`, `s9`, `p9`, `r10`, `s10`, `p10`, `r11`, `s11`, `p11`, `r12`, `s12`, `p12`) VALUES
('JAWA TIMUR', ' BONDOWOSO', 9258, 11253, 82, 9669, 10741, 90, 9842, 1023, 962, 47278, 51028, 93, 21246, 21246, 100, 584097, 49116, 1189, 91006, 11415, 797, 65792, 110627, 59, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 5116, 11793, 43),
('JAWA TIMUR', ' SITUBONDO', 8713, 9899, 88, 897, 9449, 9, 8844, 8999, 98, 42863, 44885, 95, 18172, 18262, 100, 33949, 44801, 76, 8772, 89967, 10, 48827, 105443, 46, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 884, 11865, 7),
('JAWA TIMUR', ' PROBOLINGGO', 16499, 19352, 85, 16952, 18473, 92, 17369, 17593, 99, 78696, 87753, 90, 3315, 332, 998, 258488, 749762, 34, 66816, 136235, 49, 33181, 202067, 16, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 10174, 20677, 49),
('JAWA TIMUR', ' PASURUAN', 24366, 26464, 92, 25259, 25259, 100, 24161, 24074, 100, 24074, 121747, 20, 953, 1023, 93, 101315, 1215024, 8, 112803, 112803, 100, 83946, 255733, 33, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 15761, 29036, 54),
('JAWA TIMUR', ' SIDOARJO', 38436, 3858, 996, 36293, 36827, 99, 34989, 35073, 100, 141588, 174938, 81, 64139, 65921, 97, 731706, 1661213, 44, 96688, 160329, 60, 64339, 37045, 174, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 14686, 47776, 31),
('JAWA TIMUR', ' MOJOKERTO', 16468, 18559, 89, 16694, 17715, 94, 16498, 16872, 98, 73936, 84155, 88, 39102, 40695, 96, 125367, 728675, 17, 101126, 117309, 86, 43064, 215726, 20, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 576, 576, 100),
('JAWA TIMUR', ' JOMBANG', 19086, 21479, 89, 18798, 20502, 92, 18624, 19526, 95, 84457, 97394, 87, 41782, 4521, 924, 41925, 805764, 5, 98749, 150398, 66, 9998, 23447, 43, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 13266, 23754, 56),
('JAWA TIMUR', ' NGANJUK', 13578, 17004, 80, 14413, 16231, 89, 13891, 15458, 90, 56158, 77104, 73, 3146, 3146, 100, 80027, 715821, 11, 71246, 147662, 48, 54295, 214746, 25, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 11926, 18515, 64),
('JAWA TIMUR', ' MADIUN', 9191, 10167, 90, 8855, 9705, 91, 8874, 9243, 96, 39578, 46102, 86, 15654, 15871, 99, 131753, 427325, 31, 18203, 111172, 16, 40596, 195128, 21, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 31, 11213, 0),
('JAWA TIMUR', ' MAGETAN', 827, 8922, 9, 8178, 8517, 96, 7921, 8111, 98, 37287, 40454, 92, 19031, 19459, 98, 26075, 346226, 8, 78023, 117139, 67, 84615, 12221, 692, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 10367, 10109, 103),
('JAWA TIMUR', ' NGAWI', 10325, 11968, 86, 10394, 11424, 91, 1017, 1088, 93, 44847, 5427, 826, 1178, 11856, 10, 295608, 573184, 52, 64745, 132349, 49, 167992, 196016, 86, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 7751, 9612, 81),
('JAWA TIMUR', ' BOJONEGORO', 16325, 18527, 88, 17614, 17685, 100, 16834, 16843, 100, 78652, 84009, 94, 35839, 35839, 100, 546178, 811713, 67, 145234, 166736, 87, 10192, 226977, 4, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 2115, 19626, 11),
('JAWA TIMUR', ' TUBAN', 16657, 17741, 94, 16381, 16934, 97, 16062, 16128, 100, 74457, 80443, 93, 40107, 44801, 90, 230076, 771202, 30, 93365, 139815, 67, 43571, 134831, 32, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 10306, 19265, 53),
('JAWA TIMUR', ' LAMONGAN', 16868, 17615, 96, 16814, 16815, 100, 16014, 16014, 100, 73076, 79876, 91, 4098, 4098, 100, 192875, 771021, 25, 126774, 158204, 80, 6027, 203501, 3, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 1115, 20408, 5),
('JAWA TIMUR', ' GRESIK', 1976, 22786, 9, 19672, 21751, 90, 19388, 20715, 94, 84694, 103322, 82, 41668, 42166, 99, 168403, 858663, 20, 86543, 113123, 77, 75257, 434076, 17, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 1276, 25147, 5),
('JAWA TIMUR', ' BANGKALAN', 13293, 17122, 78, 14825, 16343, 91, 14277, 15565, 92, 49998, 77634, 64, 22842, 37251, 61, 414346, 595046, 70, 16644, 112636, 15, 17002, 16195, 105, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 5055, 37775, 13),
('JAWA TIMUR', ' SAMPANG', 14292, 16842, 85, 15344, 16077, 95, 15121, 15311, 99, 6323, 76368, 8, 27088, 48246, 56, 531834, 605617, 88, 96036, 90637, 106, 101042, 137991, 73, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 870, 870, 100),
('JAWA TIMUR', ' PAMEKASAN', 12058, 1401, 861, 13092, 13373, 98, 12738, 12736, 100, 62082, 63524, 98, 17079, 22371, 76, 190071, 572075, 33, 55923, 8387, 667, 41239, 121836, 34, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 408, 15212, 3),
('JAWA TIMUR', ' SUMENEP', 14071, 15472, 91, 13618, 14768, 92, 14065, 14065, 100, 69727, 70152, 99, 30078, 30837, 98, 11313, 718027, 2, 8171, 141414, 6, 26825, 72166, 37, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 6355, 17233, 37),
('JAWA TIMUR', ' KOTA KEDIRI', 4348, 4728, 92, 4274, 4513, 95, 4293, 4298, 100, 19216, 21439, 90, 12057, 12188, 99, 65331, 191975, 34, 14762, 29354, 50, 10453, 59759, 17, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 5161, 6461, 80),
('JAWA TIMUR', ' KOTA BLITAR', 2003, 2391, 84, 2017, 2283, 88, 193, 2174, 9, 7558, 10844, 70, 6836, 6995, 98, 71949, 90796, 79, 14557, 17056, 85, 5223, 32679, 16, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 2671, 3419, 78),
('JAWA TIMUR', ' KOTA MALANG', 11975, 13308, 90, 11636, 12703, 92, 11698, 12098, 97, 47084, 60343, 78, 30178, 30189, 100, 301845, 598351, 50, 40971, 76392, 54, 59283, 178808, 33, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 7452, 11095, 67),
('JAWA TIMUR', ' KOTA PROBOLINGGO', 3806, 4098, 93, 3756, 3911, 96, 3637, 3725, 98, 145, 1858, 8, 8844, 8891, 99, 106069, 153459, 69, 19865, 22391, 89, 21309, 49892, 43, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 2262, 4534, 50),
('JAWA TIMUR', ' KOTA PASURUAN', 3307, 3664, 90, 3237, 3498, 93, 323, 3331, 10, 1396, 16612, 8, 7309, 7375, 99, 39077, 131125, 30, 6836, 16822, 41, 22025, 38762, 57, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 2417, 4122, 59),
('JAWA TIMUR', ' KOTA MOJOKERTO', 2216, 2257, 98, 2111, 2155, 98, 2052, 2052, 100, 9002, 10233, 88, 5757, 5769, 100, 3321, 8986, 37, 932, 12587, 7, 6238, 25428, 25, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 2748, 3279, 84),
('JAWA TIMUR', ' KOTA MADIUN', 2694, 2706, 100, 2584, 2583, 100, 2459, 246, 1000, 1213, 12271, 10, 7189, 7189, 100, 98067, 115637, 85, 13733, 2359, 582, 27748, 49843, 56, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 4505, 538, 837),
('JAWA TIMUR', ' KOTA SURABAYA', 46419, 47104, 99, 43896, 44963, 98, 42902, 42822, 100, 189854, 21359, 889, 90312, 92686, 97, 913636, 2015454, 45, 164546, 236541, 70, 102599, 495439, 21, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 36066, 54404, 66),
('JAWA TIMUR', ' KOTA BATU', 3112, 3462, 90, 3045, 3304, 92, 3147, 3147, 100, 12907, 15695, 82, 5641, 5702, 99, 69481, 13435, 517, 15758, 22949, 69, 7118, 58332, 12, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 1398, 7753, 18),
('BANTEN', ' PANDEGLANG', 0, 1000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('BANTEN', ' LEBAK', 20951, 1000, 2095, 19306, 0, 0, 23608, 0, 0, 91261, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('BANTEN', ' TANGERANG', 77438, 81625, 95, 73497, 77915, 94, 67214, 74205, 91, 257811, 359755, 72, 118438, 69446, 171, 0, 2392286, 0, 0, 164572, 0, 0, 65423, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('BANTEN', ' SERANG', 26622, 31768, 84, 27482, 30324, 91, 28676, 3032, 946, 128496, 187145, 69, 5811, 58971, 10, 105078, 296471, 35, 37085, 95438, 39, 26031, 67745, 38, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 14077, 34379, 41),
('BANTEN', ' KOTA TANGERANG', 39661, 40739, 97, 3644, 38886, 9, 36301, 37035, 98, 7614, 9798, 78, 33274, 33881, 98, 124047, 1187120, 10, 47835, 58071, 82, 88961, 168401, 53, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 1369, 1369, 100),
('BANTEN', ' KOTA CILEGON', 7876, 9469, 83, 8573, 9038, 95, 8329, 8608, 97, 41112, 42867, 96, 7895, 8532, 93, 207744, 274196, 76, 1426, 25132, 6, 13708, 85353, 16, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 10486, 11299, 93),
('BANTEN', ' KOTA SERANG', 11577, 15173, 76, 1314, 14484, 9, 1342, 13794, 10, 66661, 80456, 83, 4959, 0, 0, 167625, 0, 0, 27705, 29844, 93, 14206, 107644, 13, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 17, 0, 0),
('BANTEN', ' KOTA TANGERANG SELATAN', 3239, 32446, 10, 31237, 30907, 101, 31149, 29445, 106, 144026, 147743, 97, 45209, 45632, 99, 1082378, 1077864, 100, 84798, 87003, 97, 269466, 269466, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 7544, 29426, 26),
('BALI', ' JEMBRANA', 4629, 5248, 88, 4604, 5009, 92, 4608, 4773, 97, 18536, 20153, 92, 9192, 9339, 98, 96716, 1765, 5480, 741, 3399, 22, 4021, 32912, 12, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 4972, 9786, 51),
('BALI', ' TABANAN', 53, 5693, 1, 5142, 5585, 92, 5166, 5319, 97, 18144, 19148, 95, 1184, 11318, 10, 209379, 276223, 76, 45633, 73778, 62, 10321, 10231, 101, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 451, 6298, 7),
('BALI', ' BADUNG', 8569, 11329, 76, 10814, 10814, 100, 8279, 10299, 80, 46781, 46781, 100, 18522, 18522, 100, 4247, 4247, 100, 48801, 48801, 100, 119684, 119684, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 5501, 5501, 100),
('BALI', ' GIANYAR', 6405, 6708, 95, 6956, 6956, 100, 6618, 6618, 100, 29975, 29975, 100, 13604, 13604, 100, 122538, 122538, 100, 28961, 28961, 100, 29166, 29166, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 7371, 7371, 100),
('BALI', ' KLUNGKUNG', 2928, 2802, 104, 2801, 2674, 105, 2715, 2547, 107, 14282, 12913, 111, 6977, 6977, 100, 72201, 108518, 67, 28106, 31392, 90, 703, 11782, 6, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 3325, 3325, 100),
('BALI', ' BANGLI', 3524, 3564, 99, 3269, 33, 9906, 3274, 33, 9921, 18755, 21835, 86, 7581, 7835, 97, 64499, 64499, 100, 2573, 2573, 100, 6043, 6043, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 1335, 1335, 100),
('BALI', ' KARANG ASEM', 5884, 714, 824, 6882, 6816, 101, 6757, 6227, 109, 28024, 35089, 80, 14634, 15005, 98, 12176, 2491, 489, 22002, 32996, 67, 4077, 26674, 15, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 5243, 744, 705),
('BALI', ' BULELENG', 10832, 12124, 89, 10816, 11574, 93, 10819, 11022, 98, 52784, 57829, 91, 2048, 22949, 9, 32752, 4094, 800, 45445, 54207, 84, 67359, 67359, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 7731, 12741, 61),
('BALI', ' KOTA DENPASAR', 17987, 18273, 98, 17443, 17443, 100, 16612, 16612, 100, 49124, 57951, 85, 25766, 25892, 100, 225, 640091, 0, 12696, 15365, 83, 6601, 140815, 5, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 14782, 21208, 70),
('NUSA TENGGARA BARAT', ' LOMBOK BARAT', 14444, 15246, 95, 13748, 14553, 94, 1378, 1386, 99, 65724, 66156, 99, 16434, 16646, 99, 0, 479314, 0, 42047, 42047, 100, 2675, 2675, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 14, 675222, 0),
('NUSA TENGGARA BARAT', ' LOMBOK TENGAH', 20501, 21297, 96, 19443, 20329, 96, 19396, 19361, 100, 81224, 96803, 84, 15887, 0, 0, 90532, 581657, 16, 59573, 79935, 75, 13185, 170588, 8, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 21309, 0, 0),
('NUSA TENGGARA BARAT', ' LOMBOK TIMUR', 2761, 28139, 10, 2611, 2686, 97, 25257, 25581, 99, 95382, 127905, 75, 389, 9262, 4, 43714, 72627, 60, 70221, 99259, 71, 31044, 72627, 43, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 6211, 673, 923),
('NUSA TENGGARA BARAT', ' SUMBAWA', 8908, 10706, 83, 879, 10219, 9, 8795, 9733, 90, 8418, 48663, 17, 0, 38931, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 120, 0, 0),
('NUSA TENGGARA BARAT', ' DOMPU', 6674, 6139, 109, 5641, 586, 963, 5713, 5581, 102, 40402, 40949, 99, 9853, 20624, 48, 16772, 138018, 12, 14673, 14106, 104, 6617, 129648, 5, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 8, 8, 100),
('NUSA TENGGARA BARAT', ' BIMA', 11365, 11158, 102, 10847, 10651, 102, 10247, 10144, 101, 46747, 50178, 93, 7741, 0, 0, 83018, 83018, 100, 2358, 35058, 7, 2675, 68097, 4, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 2781, 3191, 87),
('NUSA TENGGARA BARAT', ' SUMBAWA BARAT', 3119, 3757, 83, 2823, 3586, 79, 2841, 3269, 87, 11116, 16301, 68, 4726, 5458, 87, 15248, 86088, 18, 8207, 9979, 82, 3886, 5588, 70, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 3916, 4978, 79),
('NUSA TENGGARA BARAT', ' LOMBOK UTARA', 4724, 504, 937, 4566, 473, 965, 4549, 4549, 100, 16634, 22517, 74, 0, 0, 0, 37814, 52357, 72, 23406, 15006, 156, 9269, 9269, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 2, 2, 100),
('NUSA TENGGARA BARAT', ' KOTA MATARAM', 9902, 9741, 102, 8282, 9299, 89, 7961, 8855, 90, 24476, 3542, 691, 15954, 17218, 93, 178378, 300137, 59, 18902, 31546, 60, 5967, 77435, 8, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 8408, 8408, 100),
('NUSA TENGGARA BARAT', ' KOTA BIMA', 3479, 1000, 348, 3195, 0, 0, 3165, 0, 0, 11091, 0, 0, 0, 0, 0, 0, 0, 0, 12262, 0, 0, 6412, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0),
('NUSA TENGGARA TIMUR', ' SUMBA BARAT', 1634, 1000, 163, 1756, 0, 0, 142, 0, 0, 3549, 0, 0, 3127, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('NUSA TENGGARA TIMUR', ' SUMBA TIMUR', 3192, 1000, 319, 7369, 0, 0, 0, 0, 0, 4546, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('NUSA TENGGARA TIMUR', ' KUPANG', 499, 8613, 6, 5338, 8063, 66, 5324, 8063, 66, 31041, 43819, 71, 8999, 10486, 86, 149923, 209126, 72, 2312, 39326, 6, 1582, 61639, 3, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 98, 132, 74),
('NUSA TENGGARA TIMUR', ' TIMOR TENGAH SELATAN', 10025, 1384, 724, 8097, 13182, 61, 8313, 922, 902, 25591, 57247, 45, 13524, 17158, 79, 214188, 252535, 85, 21213, 39551, 54, 4832, 51483, 9, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 684, 648, 106),
('NUSA TENGGARA TIMUR', ' TIMOR TENGAH UTARA', 4678, 6921, 68, 4999, 6607, 76, 5063, 5801, 87, 20434, 23398, 87, 9813, 11913, 82, 10293, 150604, 7, 3197, 21451, 15, 2845, 38646, 7, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 219, 449, 49),
('NUSA TENGGARA TIMUR', ' BELU', 4098, 5519, 74, 4387, 5266, 83, 4468, 5015, 89, 17072, 20454, 83, 9434, 977, 966, 26192, 114258, 23, 449, 12024, 4, 3909, 32717, 12, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('NUSA TENGGARA TIMUR', ' ALOR', 5468, 1000, 547, 4799, 0, 0, 4799, 0, 0, 17521, 0, 0, 6616, 0, 0, 0, 0, 0, 1951, 0, 0, 2528, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 26, 0, 0),
('NUSA TENGGARA TIMUR', ' LEMBATA', 1428, 2399, 60, 2134, 2157, 99, 2078, 2113, 98, 4427, 874, 507, 5754, 6029, 95, 6217, 72672, 9, 3388, 13169, 26, 2093, 22342, 9, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 387, 2862, 14),
('NUSA TENGGARA TIMUR', ' FLORES TIMUR', 73, 1000, 7, 99, 0, 0, 90, 0, 0, 90, 0, 0, 100, 0, 0, 0, 0, 0, 0, 0, 0, 50, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('NUSA TENGGARA TIMUR', ' SIKKA', 4171, 1000, 417, 5077, 0, 0, 4936, 0, 0, 11512, 0, 0, 6636, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('NUSA TENGGARA TIMUR', ' ENDE', 3597, 6821, 53, 4428, 4534, 98, 394, 6201, 6, 18867, 32414, 58, 4891, 6542, 75, 17884, 73645, 24, 23384, 10898, 215, 10994, 10994, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 28, 28, 100),
('NUSA TENGGARA TIMUR', ' NGADA', 2031, 1000, 203, 259, 0, 0, 1775, 0, 0, 8374, 0, 0, 3681, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('NUSA TENGGARA TIMUR', ' MANGGARAI', 5099, 6463, 79, 5862, 6138, 96, 5824, 6103, 95, 25351, 29533, 86, 15344, 15801, 97, 12526, 220418, 6, 4186, 23041, 18, 6113, 6153, 99, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 1323, 3262, 41),
('NUSA TENGGARA TIMUR', ' ROTE NDAO', 1723, 1000, 172, 0, 0, 0, 2358, 0, 0, 11191, 0, 0, 332, 0, 0, 0, 0, 0, 1609, 0, 0, 347, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0),
('NUSA TENGGARA TIMUR', ' MANGGARAI BARAT', 4547, 7982, 57, 444, 762, 58, 4786, 4936, 97, 25369, 25369, 100, 4367, 4367, 100, 81429, 81429, 100, 16507, 16507, 100, 12858, 12858, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 2, 2, 100),
('NUSA TENGGARA TIMUR', ' SUMBA TENGAH', 2126, 1000, 213, 1762, 0, 0, 0, 0, 0, 4679, 0, 0, 1747, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('NUSA TENGGARA TIMUR', ' SUMBA BARAT DAYA', 4507, 12276, 37, 4298, 11718, 37, 1116, 1116, 100, 97248, 97248, 100, 0, 0, 0, 0, 0, 0, 7571, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29, 29, 100),
('NUSA TENGGARA TIMUR', ' NAGEKEO', 61, 1, 6100, 99, 1, 9900, 92, 1, 9200, 83, 1, 8300, 88, 1, 8800, 7, 0, 0, 44, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 36, 0, 0),
('NUSA TENGGARA TIMUR', ' MANGGARAI TIMUR', 0, 8028, 0, 0, 7662, 0, 0, 7297, 0, 0, 33862, 0, 0, 7068, 0, 0, 164137, 0, 0, 18149, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('NUSA TENGGARA TIMUR', ' SABU RAIJUA', 0, 2743, 0, 0, 2617, 0, 0, 2493, 0, 0, 0, 0, 0, 3769, 0, 0, 0, 0, 0, 11734, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('NUSA TENGGARA TIMUR', ' MALAKA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('NUSA TENGGARA TIMUR', ' KOTA KUPANG', 0, 9349, 0, 0, 8924, 0, 0, 8731, 0, 0, 39437, 0, 0, 7309, 0, 0, 293129, 0, 0, 16663, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('KALIMANTAN BARAT', ' SAMBAS', 10903, 13269, 82, 9889, 12666, 78, 10827, 12063, 90, 44867, 58783, 76, 24089, 27117, 89, 1357, 7581, 18, 8202, 22167, 37, 31109, 117617, 26, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 56, 56, 100),
('KALIMANTAN BARAT', ' BENGKAYANG', 5571, 6433, 87, 4591, 6141, 75, 4625, 5164, 90, 11659, 27179, 43, 8931, 13486, 66, 0, 15989, 0, 435, 15925, 3, 298, 8634, 3, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 321, 0),
('KALIMANTAN BARAT', ' LANDAK', 7789, 8145, 96, 722, 7779, 9, 6984, 7408, 94, 24196, 36043, 67, 1784, 20051, 9, 6037, 256429, 2, 35128, 0, 0, 12445, 60259, 21, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 1395, 1395, 100),
('KALIMANTAN BARAT', ' MEMPAWAH', 4741, 5788, 82, 2953, 5525, 53, 4533, 5262, 86, 15111, 25642, 59, 5316, 30523, 17, 0, 168236, 0, 15454, 2187, 707, 2098, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 35, 3836, 1),
('KALIMANTAN BARAT', ' SANGGAU', 95, 1054, 9, 8334, 10084, 83, 7261, 9212, 79, 11899, 44892, 27, 5523, 8544, 65, 311403, 311403, 100, 20308, 56935, 36, 4198, 89877, 5, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 11355, 11355, 100),
('KALIMANTAN BARAT', ' KETAPANG', 8361, 11279, 74, 3271, 10767, 30, 7169, 7995, 90, 25642, 4998, 513, 8119, 19471, 42, 10564, 206331, 5, 10371, 31153, 33, 14511, 53233, 27, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 179, 543, 33),
('KALIMANTAN BARAT', ' SINTANG', 801, 9769, 8, 5432, 9325, 58, 7031, 8881, 79, 16106, 43023, 37, 11705, 25988, 45, 0, 81601, 0, 10997, 24999, 44, 215, 19354, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 63, 105, 60),
('KALIMANTAN BARAT', ' KAPUAS HULU', 32, 5647, 1, 2444, 5388, 45, 4311, 309, 1395, 3671, 25043, 15, 945, 5237, 18, 2077, 77789, 3, 628, 6479, 10, 18115, 18156, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 4818, 6382, 75),
('KALIMANTAN BARAT', ' SEKADAU', 3543, 4428, 80, 1502, 4226, 36, 3145, 4025, 78, 10961, 19557, 56, 0, 638, 0, 0, 126552, 0, 0, 13933, 0, 215, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('KALIMANTAN BARAT', ' MELAWI', 0, 5, 0, 0, 4, 0, 0, 4, 0, 0, 8, 0, 0, 8, 0, 0, 131, 0, 0, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('KALIMANTAN BARAT', ' KAYONG UTARA', 2018, 28, 7207, 1582, 2674, 59, 1838, 2546, 72, 8458, 11497, 74, 4092, 5056, 81, 1492, 2163, 69, 324, 324, 100, 25882, 25882, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 5, 2964, 0),
('KALIMANTAN BARAT', ' KUBU RAYA', 11641, 11806, 99, 10216, 10518, 97, 9071, 10452, 87, 25228, 39879, 63, 13864, 16328, 85, 8113, 364268, 2, 25531, 41698, 61, 4156, 114389, 4, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 147, 147, 100),
('KALIMANTAN BARAT', ' KOTA PONTIANAK', 12516, 12904, 97, 11995, 12318, 97, 11351, 11731, 97, 49893, 56845, 88, 23009, 20833, 110, 129175, 41747, 309, 34654, 48808, 71, 10763, 154338, 7, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('KALIMANTAN BARAT', ' KOTA SINGKAWANG', 4555, 4978, 92, 4198, 4751, 88, 4101, 4525, 91, 7347, 21968, 33, 7757, 9198, 84, 10485, 133056, 8, 2589, 19528, 13, 14133, 39214, 36, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 2491, 388, 642),
('KALIMANTAN TENGAH', ' KOTAWARINGIN BARAT', 5774, 6624, 87, 5506, 6323, 87, 5541, 6022, 92, 21112, 22558, 94, 8849, 9253, 96, 4005, 44949, 9, 11765, 15049, 78, 7523, 12849, 59, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 2788, 6624, 42),
('KALIMANTAN TENGAH', ' KOTAWARINGIN TIMUR', 8589, 1021, 841, 4618, 9743, 47, 7422, 9282, 80, 19087, 34565, 55, 10694, 10927, 98, 35574, 285287, 12, 9702, 19169, 51, 6239, 81347, 8, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 2276, 11271, 20),
('KALIMANTAN TENGAH', ' KAPUAS', 6666, 7502, 89, 4738, 7101, 67, 4728, 682, 693, 18298, 32151, 57, 6458, 39885, 16, 110602, 242117, 46, 17767, 22771, 78, 22105, 83551, 26, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 101, 22722, 0),
('KALIMANTAN TENGAH', ' BARITO SELATAN', 2252, 2988, 75, 2029, 2852, 71, 1915, 2716, 71, 8254, 13051, 63, 3789, 4787, 79, 3423, 47515, 7, 1893, 7919, 24, 3913, 15078, 26, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 120, 120, 100),
('KALIMANTAN TENGAH', ' BARITO UTARA', 2728, 2848, 96, 2345, 2719, 86, 2288, 2337, 98, 8861, 12206, 73, 3114, 3181, 98, 33613, 34203, 98, 1398, 5185, 27, 3097, 826, 375, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 200, 200, 100),
('KALIMANTAN TENGAH', ' SUKAMARA', 1321, 1443, 92, 831, 1378, 60, 1186, 1285, 92, 3163, 6183, 51, 1749, 1814, 96, 515, 33193, 2, 934, 1646, 57, 1945, 14584, 13, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 80, 5998, 1),
('KALIMANTAN TENGAH', ' LAMANDAU', 1465, 171, 857, 938, 1632, 57, 2412, 1554, 155, 4802, 7326, 66, 256, 8158, 3, 118, 54476, 0, 5067, 5067, 100, 1739, 16913, 10, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 152, 152, 100),
('KALIMANTAN TENGAH', ' SERUYAN', 3341, 4539, 74, 2829, 4334, 65, 2958, 4128, 72, 818, 19459, 4, 866, 3403, 25, 0, 133338, 0, 314, 7055, 4, 3511, 49014, 7, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('KALIMANTAN TENGAH', ' KATINGAN', 3607, 4011, 90, 3228, 4011, 80, 2801, 3551, 79, 8313, 13639, 61, 3268, 3617, 90, 8655, 96183, 9, 5504, 16031, 34, 5651, 31982, 18, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 1549, 4112, 38),
('KALIMANTAN TENGAH', ' PULANG PISAU', 2409, 265, 909, 2278, 2529, 90, 2253, 2409, 94, 8841, 11355, 78, 2337, 2509, 93, 455, 8147, 6, 4078, 11267, 36, 3068, 22634, 14, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 129, 3039, 4),
('KALIMANTAN TENGAH', ' GUNUNG MAS', 2629, 2921, 90, 2418, 2788, 87, 249, 2655, 9, 10355, 12517, 83, 2679, 4524, 59, 35651, 65968, 54, 2224, 6976, 32, 1554, 2309, 67, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 193, 191, 101),
('KALIMANTAN TENGAH', ' BARITO TIMUR', 2213, 2678, 83, 1974, 2558, 77, 1973, 2333, 85, 8481, 9571, 89, 3906, 3208, 122, 5829, 605, 963, 1773, 21, 8443, 3962, 21568, 18, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 114, 114, 100),
('KALIMANTAN TENGAH', ' MURUNG RAYA', 231, 304, 76, 2056, 2888, 71, 686, 2764, 25, 5485, 13029, 42, 2475, 2457, 101, 5676, 75712, 7, 899, 577, 156, 842, 9903, 9, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 128, 3897, 3),
('KALIMANTAN TENGAH', ' KOTA PALANGKA RAYA', 5253, 5916, 89, 4733, 5647, 84, 496, 5378, 9, 17759, 25351, 70, 4668, 5144, 91, 2843, 18746, 15, 3341, 13384, 25, 12606, 4771, 264, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 309, 819, 38),
('KALIMANTAN SELATAN', ' TANAH LAUT', 5966, 7728, 77, 6212, 7728, 80, 6142, 7017, 88, 25553, 36769, 69, 205, 3374, 6, 74806, 20345, 368, 19052, 30829, 62, 24227, 59996, 40, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 250, 8701, 3),
('KALIMANTAN SELATAN', ' KOTA BARU', 5287, 6356, 83, 5044, 5285, 95, 5269, 5291, 100, 22153, 36472, 61, 10179, 12009, 85, 1346, 217027, 1, 15144, 186, 8142, 12525, 57151, 22, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 9, 9, 100),
('KALIMANTAN SELATAN', ' BANJAR', 10578, 12001, 88, 9983, 11474, 87, 11456, 11349, 101, 37783, 48216, 78, 19363, 19633, 99, 158765, 320205, 50, 23729, 3661, 648, 3472, 81079, 4, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 70208, 15223, 461),
('KALIMANTAN SELATAN', ' BARITO KUALA', 4, 6589, 0, 4, 6, 67, 5, 6, 83, 12, 24, 50, 6, 7, 86, 7, 198, 4, 4, 33, 12, 18, 34, 53, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 772, 4, 19300),
('KALIMANTAN SELATAN', ' TAPIN', 1288, 100, 1288, 1921, 100, 1921, 3108, 100, 3108, 1036, 100, 1036, 0, 100, 0, 0, 100, 0, 1473, 100, 1473, 13545, 100, 13545, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 436, 100, 436),
('KALIMANTAN SELATAN', ' HULU SUNGAI SELATAN', 293, 4593, 6, 3481, 4175, 83, 3498, 4175, 84, 11793, 20491, 58, 4167, 1264, 330, 2212, 155402, 1, 3906, 22579, 17, 15395, 50145, 31, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 11, 140, 8),
('KALIMANTAN SELATAN', ' HULU SUNGAI TENGAH', 388, 5099, 8, 4237, 4867, 87, 3973, 4625, 86, 13123, 18549, 71, 8539, 8539, 100, 25065, 167642, 15, 19656, 25303, 78, 17311, 37803, 46, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 200, 250, 80),
('KALIMANTAN SELATAN', ' HULU SUNGAI UTARA', 3616, 4816, 75, 3831, 4597, 83, 3769, 4378, 86, 7559, 17391, 43, 8574, 10866, 79, 85656, 153402, 56, 15861, 18467, 86, 3313, 39882, 8, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 2637, 4525, 58),
('KALIMANTAN SELATAN', ' TABALONG', 4112, 5516, 75, 4073, 5266, 77, 4169, 5015, 83, 19006, 24612, 77, 6954, 7108, 98, 36767, 47423, 78, 9176, 13214, 69, 9332, 53891, 17, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 1747, 5571, 31),
('KALIMANTAN SELATAN', ' TANAH BUMBU', 6306, 8724, 72, 5888, 8344, 71, 5901, 7947, 74, 24242, 47077, 51, 646, 7001, 9, 43721, 219377, 20, 4871, 19853, 25, 14265, 73683, 19, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 7667, 6191, 124),
('KALIMANTAN SELATAN', ' BALANGAN', 2028, 2963, 68, 2254, 2749, 82, 2146, 2694, 80, 7762, 13293, 58, 2236, 27, 8281, 13076, 86507, 15, 2138, 8356, 26, 1229, 45883, 3, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 250, 357, 70),
('KALIMANTAN SELATAN', ' KOTA BANJARMASIN', 14673, 0, 0, 13156, 0, 0, 13158, 0, 0, 62322, 0, 0, 23778, 0, 0, 26216, 0, 0, 29721, 0, 0, 73013, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 182, 0, 0),
('KALIMANTAN SELATAN', ' KOTA BANJAR BARU', 5042, 5384, 94, 5139, 5107, 101, 485, 5078, 10, 15814, 18943, 83, 2877, 3158, 91, 17427, 166259, 10, 17566, 18084, 97, 11471, 7254, 158, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 4157, 6309, 66),
('KALIMANTAN TIMUR', ' PASER', 5182, 2608, 199, 5159, 2608, 198, 5199, 1995, 261, 13313, 2402, 554, 5503, 0, 0, 5025, 0, 0, 5436, 2657, 205, 26421, 1226, 2155, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 25, 14, 179),
('KALIMANTAN TIMUR', ' KUTAI BARAT', 2526, 3148, 80, 2444, 3003, 81, 2649, 2861, 93, 7752, 1911, 406, 2437, 4251, 57, 13596, 94727, 14, 8893, 12397, 72, 8682, 3048, 285, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 488, 1318, 37),
('KALIMANTAN TIMUR', ' KUTAI KARTANEGARA', 0, 17292, 0, 0, 16509, 0, 0, 15723, 0, 0, 74727, 0, 0, 16079, 0, 0, 510539, 0, 0, 93848, 0, 0, 16291, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 1892, 0),
('KALIMANTAN TIMUR', ' KUTAI TIMUR', 432, 7187, 6, 566, 686, 83, 6533, 8631, 76, 9297, 25954, 36, 8225, 19131, 43, 3778, 234601, 2, 8331, 10648, 78, 1226, 1226, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 4339, 935, 464),
('KALIMANTAN TIMUR', ' BERAU', 3871, 5152, 75, 4366, 4917, 89, 2922, 4684, 62, 12406, 21483, 58, 5157, 5719, 90, 11428, 139194, 8, 3356, 15897, 21, 999, 25585, 4, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 771, 6256, 12),
('KALIMANTAN TIMUR', ' PENAJAM PASER UTARA', 3725, 3588, 104, 354, 3425, 10, 3558, 3262, 109, 10821, 20502, 53, 6245, 6767, 92, 29935, 99779, 30, 13017, 1514, 860, 5065, 9205, 55, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 188, 500, 38),
('KALIMANTAN TIMUR', ' MAHAKAM HULU', 438, 588, 74, 540, 562, 96, 498, 535, 93, 1048, 342, 306, 1008, 1036, 97, 4966, 7346, 68, 532, 1253, 42, 2199, 5337, 41, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 642, 641, 100),
('KALIMANTAN TIMUR', ' KOTA BALIKPAPAN', 13333, 13812, 97, 12753, 13497, 94, 12719, 12386, 103, 69506, 72055, 96, 16063, 15874, 101, 68042, 401829, 17, 4754, 47064, 10, 22644, 36565, 62, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 2418, 10017, 24),
('KALIMANTAN TIMUR', ' KOTA SAMARINDA', 16594, 19798, 84, 16275, 18898, 86, 14713, 16258, 90, 72042, 109147, 66, 17569, 18728, 94, 529219, 673132, 79, 69544, 83598, 83, 216843, 291117, 74, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 19612, 17081, 115),
('KALIMANTAN TIMUR', ' KOTA BONTANG', 237, 406, 58, 2416, 3874, 62, 1565, 2399, 65, 5987, 17537, 34, 0, 6442, 0, 10127, 117791, 9, 715, 5223, 14, 5263, 10281, 51, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 1178, 4792, 25),
('KALIMANTAN UTARA', ' MALINAU', 1508, 1727, 87, 1506, 1649, 91, 13, 1564, 1, 5224, 9844, 53, 2537, 3172, 80, 13245, 16777, 79, 3932, 6512, 60, 3911, 1532, 255, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 1807, 16543, 11),
('KALIMANTAN UTARA', ' BULUNGAN', 26, 2803, 1, 2323, 2675, 87, 2608, 2548, 102, 1286, 13954, 9, 5533, 18578, 30, 7737, 25308, 31, 7392, 5478, 135, 2402, 30955, 8, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 3138, 333, 942),
('KALIMANTAN UTARA', ' TANA TIDUNG', 456, 471, 97, 249, 435, 57, 380, 430, 88, 2215, 3053, 73, 0, 0, 0, 0, 0, 0, 585, 791, 74, 417, 4941, 8, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 103, 1509, 7),
('KALIMANTAN UTARA', ' NUNUKAN', 4606, 468, 984, 3368, 4467, 75, 3476, 4255, 82, 22785, 2108, 1081, 725, 745, 97, 7997, 120426, 7, 5112, 937, 546, 4668, 5646, 83, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 17, 17, 100),
('KALIMANTAN UTARA', ' KOTA TARAKAN', 4372, 5423, 81, 4229, 5178, 82, 4106, 4184, 98, 6294, 24769, 25, 4018, 4364, 92, 0, 0, 0, 4036, 13791, 29, 2565, 57039, 4, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 8712, 5, 174240),
('SULAWESI UTARA', ' BOLAANG MONGONDOW', 3149, 5007, 63, 367, 478, 77, 3599, 4592, 78, 16666, 22732, 73, 38928, 44989, 87, 29365, 149356, 20, 16921, 19765, 86, 1267, 3, 42233, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 16, 101, 16),
('SULAWESI UTARA', ' MINAHASA', 5345, 5405, 99, 5286, 516, 1024, 0, 477, 0, 22371, 19773, 113, 12129, 12129, 100, 211429, 211429, 100, 24015, 93541, 26, 68347, 211429, 32, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 21, 1517, 1),
('SULAWESI UTARA', ' KEPULAUAN SANGIHE', 1763, 2046, 86, 162, 1953, 8, 1701, 186, 915, 1531, 1893, 81, 2926, 3925, 75, 11381, 97335, 12, 6899, 18378, 38, 7955, 5697, 140, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 10, 10, 100),
('SULAWESI UTARA', ' KEPULAUAN TALAUD', 1442, 1742, 83, 133, 1663, 8, 1214, 157, 773, 4625, 5955, 78, 1542, 1542, 100, 756, 6856, 11, 2679, 1015, 264, 2497, 2645, 94, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 8, 73, 11),
('SULAWESI UTARA', ' MINAHASA SELATAN', 2682, 3675, 73, 2686, 351, 765, 2564, 3341, 77, 9495, 16689, 57, 246, 2845, 9, 13067, 18516, 71, 246, 3554, 7, 4832, 130602, 4, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 782, 879, 89),
('SULAWESI UTARA', ' MINAHASA UTARA', 3648, 3823, 95, 3353, 3649, 92, 3847, 3475, 111, 13837, 17355, 80, 328, 3338, 10, 12321, 12727, 97, 1987, 20127, 10, 1431, 34, 4209, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 7, 25, 28),
('SULAWESI UTARA', ' BOLAANG MONGONDOW UTARA', 1087, 1694, 64, 1425, 1617, 88, 13, 1534, 1, 3367, 7713, 44, 3748, 3748, 100, 3917, 49066, 8, 3838, 6235, 62, 3285, 49066, 7, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 24, 1829, 1),
('SULAWESI UTARA', ' SIAU TAGULANDANG BIARO', 1055, 2064, 51, 884, 197, 449, 877, 177, 495, 4323, 7776, 56, 163, 3556, 5, 43023, 2888, 1490, 8095, 18678, 43, 4431, 47106, 9, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 2, 6, 33),
('SULAWESI UTARA', ' MINAHASA TENGGARA', 1876, 0, 0, 1615, 0, 0, 1478, 0, 0, 7854, 0, 0, 2871, 0, 0, 16831, 0, 0, 15129, 0, 0, 7701, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('SULAWESI UTARA', ' BOLAANG MONGONDOW SELATAN', 0, 1604, 0, 0, 1502, 0, 0, 1421, 0, 0, 3932, 0, 0, 1421, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('SULAWESI UTARA', ' BOLAANG MONGONDOW TIMUR', 970, 1387, 70, 1071, 1324, 81, 104, 1261, 8, 1632, 6296, 26, 1168, 1258, 93, 28258, 44947, 63, 27346, 27346, 100, 6365, 7605, 84, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 266, 1347, 20),
('SULAWESI UTARA', ' KOTA MANADO', 7317, 7532, 97, 6902, 6945, 99, 6914, 6927, 100, 2364, 29632, 8, 8612, 8612, 100, 50702, 0, 0, 14518, 48256, 30, 14961, 16158, 93, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 33147, 0, 0),
('SULAWESI UTARA', ' KOTA BITUNG', 3935, 4501, 87, 3409, 4297, 79, 3449, 3441, 100, 16664, 16065, 104, 3797, 3497, 109, 89438, 124812, 72, 23581, 23581, 100, 17329, 15798, 110, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 4586, 3735, 123),
('SULAWESI UTARA', ' KOTA TOMOHON', 1676, 1666, 101, 1563, 1589, 98, 1561, 1513, 103, 1454, 1513, 96, 1443, 1516, 95, 11773, 17292, 68, 28459, 28413, 100, 2054, 77492, 3, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 676, 740, 91),
('SULAWESI UTARA', ' KOTA KOTAMOBAGU', 1487, 2353, 63, 155, 2246, 7, 1949, 2222, 88, 5416, 7398, 73, 1752, 4555, 38, 54016, 8178, 661, 20273, 8376, 242, 1873, 6962, 27, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('SULAWESI TENGAH', ' BANGGAI KEPULAUAN', 1912, 2764, 69, 1357, 2639, 51, 1838, 1984, 93, 7344, 9669, 76, 3988, 4726, 84, 2244, 9883, 23, 230, 7985, 3, 2244, 71649, 3, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 101, 2625, 4),
('SULAWESI TENGAH', ' BANGGAI', 7166, 7941, 90, 6689, 7581, 88, 429, 6734, 6, 27215, 35392, 77, 5441, 63884, 9, 15459, 234957, 7, 2306, 31383, 7, 194, 194, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 37, 37, 100),
('SULAWESI TENGAH', ' MOROWALI', 2652, 2828, 94, 2517, 2699, 93, 2507, 257, 975, 5255, 2819, 186, 32476, 123, 26403, 25329, 67974, 37, 895, 14906, 6, 4679, 21383, 22, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 12, 12, 100),
('SULAWESI TENGAH', ' POSO', 3346, 3346, 100, 3406, 3544, 96, 3386, 3493, 97, 12018, 15317, 78, 7307, 8335, 88, 98012, 138677, 71, 12196, 15538, 78, 35257, 41326, 85, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 635, 4158, 15),
('SULAWESI TENGAH', ' DONGGALA', 5956, 7558, 79, 5893, 7214, 82, 5856, 5951, 98, 25138, 33601, 75, 11394, 12346, 92, 20311, 171311, 12, 10021, 17307, 58, 15972, 44952, 36, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 524, 530, 99),
('SULAWESI TENGAH', ' TOLI', 4382, 4427, 99, 4219, 4219, 100, 4247, 4247, 100, 8842, 18015, 49, 3939, 4364, 90, 37021, 58821, 63, 4798, 10878, 44, 5057, 9042, 56, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 610, 610, 100),
('SULAWESI TENGAH', ' BUOL', 2854, 4107, 69, 3367, 3921, 86, 3118, 3734, 84, 0, 17725, 0, 0, 18099, 0, 0, 97583, 0, 0, 10033, 0, 10543, 10543, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('SULAWESI TENGAH', ' PARIGI MOUTONG', 8828, 0, 0, 10674, 0, 0, 7913, 0, 0, 20621, 0, 0, 15197, 0, 0, 0, 0, 0, 20336, 0, 0, 22748, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 8125, 0, 0),
('SULAWESI TENGAH', ' TOJO UNA', 2371, 726, 327, 219, 3461, 6, 1372, 3296, 42, 11575, 14754, 78, 0, 0, 0, 0, 0, 0, 0, 0, 0, 479, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('SULAWESI TENGAH', ' SIGI', 5032, 52, 9677, 4229, 4941, 86, 4346, 4724, 92, 22425, 21279, 105, 6752, 12727, 53, 7868, 149556, 5, 8485, 1845, 460, 18565, 36711, 51, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 1989, 5848, 34),
('SULAWESI TENGAH', ' BANGGAI LAUT', 988, 1461, 68, 524, 1369, 38, 1158, 1267, 91, 3354, 5263, 64, 1371, 1716, 80, 223, 398, 56, 4598, 8483, 54, 2443, 9249, 26, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 2, 1543, 0),
('SULAWESI TENGAH', ' MOROWALI UTARA', 2713, 303, 895, 15, 2717, 1, 1882, 259, 727, 5723, 12407, 46, 1639, 22, 7450, 11355, 11355, 100, 14727, 8384, 176, 4123, 4123, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 5, 44, 11),
('SULAWESI TENGAH', ' KOTA PALU', 7404, 7588, 98, 7062, 7244, 97, 5754, 69, 8339, 40478, 44802, 90, 13986, 13986, 100, 47032, 261385, 18, 23419, 25268, 93, 10897, 29266, 37, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 6591, 9251, 71),
('SULAWESI SELATAN', ' KEPULAUAN SELAYAR', 1951, 2292, 85, 1884, 2187, 86, 1941, 1941, 100, 6881, 10286, 67, 2687, 652, 412, 4265, 71899, 6, 2045, 9943, 21, 1075, 1075, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 13, 13, 100),
('SULAWESI SELATAN', ' BULUKUMBA', 6309, 7625, 83, 6471, 7279, 89, 482, 5983, 8, 19222, 41154, 47, 6548, 12429, 53, 13707, 36358, 38, 5834, 1717, 340, 13795, 40889, 34, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 4138, 4138, 100),
('SULAWESI SELATAN', ' BANTAENG', 7146, 3564, 201, 669, 3404, 20, 674, 337, 200, 20966, 10538, 199, 24176, 12088, 200, 235902, 117951, 200, 271, 1541, 18, 56366, 35229, 160, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 773, 3865, 20),
('SULAWESI SELATAN', ' JENEPONTO', 6264, 7086, 88, 6397, 6764, 95, 6676, 7447, 90, 15199, 3329, 457, 577, 40717, 1, 0, 221766, 0, 9301, 31832, 29, 14424, 261112, 6, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 69, 69, 100),
('SULAWESI SELATAN', ' TAKALAR', 6015, 6107, 98, 5739, 5829, 98, 5715, 5547, 103, 20453, 24581, 83, 6506, 10794, 60, 47998, 47998, 100, 10687, 20937, 51, 913, 19321, 5, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('SULAWESI SELATAN', ' GOWA', 13516, 14025, 96, 11907, 13318, 89, 10584, 12694, 83, 36756, 74529, 49, 21436, 64414, 33, 85328, 146372, 58, 23209, 44871, 52, 199546, 199546, 100, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 28, 32, 88),
('SULAWESI SELATAN', ' SINJAI', 4084, 45, 9076, 3726, 4056, 92, 5438, 4121, 132, 18621, 22726, 82, 4176, 6283, 66, 10221, 165757, 6, 8145, 19099, 43, 868, 56259, 2, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 7065, 7065, 100),
('SULAWESI SELATAN', ' MAROS', 5775, 6345, 91, 5495, 6057, 91, 5455, 5638, 97, 34512, 50216, 69, 6178, 7092, 87, 119871, 227844, 53, 7705, 2897, 266, 12756, 59773, 21, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 14, 20, 70),
('SULAWESI SELATAN', ' PANGKAJENE DAN KEPULAUAN', 5919, 5921, 100, 565, 5162, 11, 5161, 5149, 100, 35209, 25628, 137, 9211, 9211, 100, 60534, 9211, 657, 27357, 1443, 1896, 26123, 34954, 75, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 6756, 6756, 100),
('SULAWESI SELATAN', ' BARRU', 3104, 3396, 91, 3036, 3173, 96, 3044, 3047, 100, 11316, 13986, 81, 5337, 7394, 72, 27787, 104217, 27, 1465, 19709, 7, 8442, 12764, 66, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 125, 125, 100),
('SULAWESI SELATAN', ' BONE', 14298, 152, 9407, 13389, 14509, 92, 13818, 13818, 100, 97321, 97321, 100, 19241, 2332, 825, 7, 0, 0, 34, 35, 97, 23377, 30404, 77, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 27, 0, 0),
('SULAWESI SELATAN', ' SOPPENG', 2757, 3134, 88, 2867, 2992, 96, 2859, 2859, 100, 11132, 11132, 100, 688, 688, 100, 34721, 137254, 25, 14229, 18987, 75, 13401, 14196, 94, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 18, 18, 100),
('SULAWESI SELATAN', ' WAJO', 6792, 7294, 93, 6668, 6963, 96, 6587, 6652, 99, 23042, 25747, 89, 7648, 7792, 98, 171375, 256219, 67, 16144, 49667, 33, 30124, 84624, 36, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 6332, 386, 1640),
('SULAWESI SELATAN', ' SIDENRENG RAPPANG', 0, 6085, 0, 0, 5044, 0, 0, 5012, 0, 0, 5052, 0, 0, 14579, 0, 0, 176707, 0, 0, 28061, 0, 0, 4083, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0),
('SULAWESI SELATAN', ' PINRANG', 7335, 7585, 97, 6889, 7241, 95, 6889, 6847, 101, 34462, 36679, 94, 13712, 13712, 100, 21916, 21916, 100, 426, 8857, 5, 10137, 13625, 74, 2950, 2679, 110, 3200, 3200, 100, 592, 562, 105, 96, 96, 100),
('SULAWESI SELATAN', ' ENREKANG', 2899, 3601, 81, 3246, 3455, 94, 2878, 3266, 88, 8806, 25785, 34, 7527, 6612, 114, 70964, 113178, 63, 4695, 17192, 27, 9916, 4503, 220, 0, 0, 0, 0, 0, 0, 0, 0, 0, 702, 200, 351),
('SULAWESI SELATAN', ' LUWU', 5914, 5914, 100, 7254, 6943, 104, 6943, 6355, 109, 35404, 35404, 100, 1431, 11592, 12, 1431, 16466, 9, 16248, 716, 2269, 33077, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7846, 7846, 100),
('SULAWESI SELATAN', ' TANA TORAJA', 374, 4119, 9, 3867, 3867, 100, 3875, 3875, 100, 13692, 13692, 100, 4841, 4841, 100, 16777, 16777, 100, 6623, 15883, 42, 5758, 5758, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 28, 28, 100),
('SULAWESI SELATAN', ' LUWU UTARA', 463, 5586, 8, 519, 5164, 10, 516, 2675, 19, 14329, 0, 0, 9955, 2226, 447, 130998, 0, 0, 21203, 21203, 100, 6391, 6391, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 385, 4, 9625),
('SULAWESI SELATAN', ' LUWU TIMUR', 6073, 6293, 97, 5498, 5732, 96, 5556, 5725, 97, 2561, 30595, 8, 9685, 11263, 86, 1607, 19569, 8, 632, 15785, 4, 7846, 52129, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('SULAWESI SELATAN', ' TORAJA UTARA', 402, 4242, 9, 3928, 4049, 97, 3937, 3963, 99, 8854, 17771, 50, 0, 0, 0, 44421, 122346, 36, 6175, 15787, 39, 11068, 42626, 26, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1387, 1387, 100),
('SULAWESI SELATAN', ' KOTA MAKASSAR', 27675, 29095, 95, 25926, 25926, 100, 24649, 2598, 949, 137194, 154307, 89, 25224, 25224, 100, 25246, 25246, 100, 83161, 110229, 75, 56092, 56092, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 47618, 47618, 100),
('SULAWESI SELATAN', ' KOTA PAREPARE', 3022, 3032, 100, 2736, 2894, 95, 2738, 2756, 99, 11782, 1387, 849, 5555, 6002, 93, 117174, 88728, 132, 9724, 9724, 100, 7678, 4016, 191, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 2292, 2),
('SULAWESI SELATAN', ' KOTA PALOPO', 3001, 3234, 93, 2956, 3087, 96, 2975, 2975, 100, 135414, 16963, 798, 2861, 37, 7732, 28034, 23144, 121, 10895, 14542, 75, 367, 4619, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3726, 4123, 90),
('SULAWESI TENGGARA', ' BUTON', 166, 2082, 8, 1743, 1988, 88, 1893, 1893, 100, 5573, 12344, 45, 348, 4539, 8, 12124, 55352, 22, 4774, 7423, 64, 1911, 49812, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2282, 0),
('SULAWESI TENGGARA', ' MUNA', 3693, 4977, 74, 2608, 4751, 55, 3761, 4555, 83, 11236, 15512, 72, 3299, 4309, 77, 1121, 119768, 1, 0, 17205, 0, 835, 136973, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 176, 5497, 3),
('SULAWESI TENGGARA', ' KONAWE', 481, 6482, 7, 6036, 8559, 71, 426, 4639, 9, 15414, 2194, 703, 7415, 8218, 90, 22988, 148379, 15, 847, 15495, 5, 9706, 3313, 293, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 17973, 0),
('SULAWESI TENGGARA', ' KOLAKA', 4228, 5031, 84, 4159, 4803, 87, 4319, 457, 945, 21151, 30789, 69, 3598, 3804, 95, 8488, 23645, 36, 5739, 1505, 381, 2157, 2157, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 196, 5281, 4),
('SULAWESI TENGGARA', ' KONAWE SELATAN', 4977, 8216, 61, 5172, 7842, 66, 7409, 7409, 100, 18642, 29692, 63, 6319, 7824, 81, 49621, 180973, 27, 3148, 16948, 19, 14252, 23394, 61, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2031, 2031, 100),
('SULAWESI TENGGARA', ' BOMBANA', 3593, 471, 763, 2631, 4496, 59, 3563, 3648, 98, 12803, 1691, 757, 3262, 3501, 93, 41112, 116599, 35, 5581, 6188, 90, 5143, 8861, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 100),
('SULAWESI TENGGARA', ' WAKATOBI', 1782, 2265, 79, 1724, 2162, 80, 1829, 2059, 89, 8712, 9698, 90, 109, 2154, 5, 53583, 53583, 100, 1893, 10204, 19, 15605, 58437, 27, 0, 0, 0, 0, 0, 0, 0, 0, 0, 518, 518, 100),
('SULAWESI TENGGARA', ' KOLAKA UTARA', 2773, 3655, 76, 2863, 3489, 82, 2852, 2868, 99, 11643, 13061, 89, 4698, 4788, 98, 37801, 93588, 40, 609, 8136, 7, 393, 21057, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 384, 1),
('SULAWESI TENGGARA', ' BUTON UTARA', 1602, 1755, 91, 1166, 1675, 70, 1184, 1595, 74, 5098, 6211, 82, 69, 77, 90, 1593, 2197, 73, 3148, 4382, 72, 2197, 2976, 74, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 100),
('SULAWESI TENGGARA', ' KONAWE UTARA', 1165, 1712, 68, 760, 1632, 47, 1241, 1217, 102, 4525, 4525, 100, 1733, 1878, 92, 38438, 38438, 100, 0, 2242, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 83, 0, 0),
('SULAWESI TENGGARA', ' KOLAKA TIMUR', 319, 419, 76, 382, 3999, 10, 399, 346, 115, 5932, 18419, 32, 2211, 22668, 10, 2462, 46403, 5, 684, 11924, 6, 559, 651, 86, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 405, 0),
('SULAWESI TENGGARA', ' KONAWE KEPULAUAN', 598, 912, 66, 582, 870, 67, 468, 730, 64, 3016, 3901, 77, 592, 4938, 12, 113, 23392, 0, 1175, 1989, 59, 113, 25213, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 100),
('SULAWESI TENGGARA', ' MUNA BARAT', 1157, 22, 5259, 21, 21, 100, 2, 2, 100, 4746, 9544, 50, 835, 6019, 14, 688, 14973, 5, 462, 5944, 8, 1149, 500, 230, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 43, 0),
('SULAWESI TENGGARA', ' BUTON TENGAH', 1632, 1781, 92, 442, 17, 2600, 1647, 1619, 102, 6128, 1206, 508, 6128, 15272, 40, 54851, 52695, 104, 366, 7416, 5, 625, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0),
('SULAWESI TENGGARA', ' BUTON SELATAN', 1523, 1992, 76, 1634, 1982, 82, 1717, 1725, 100, 6575, 8112, 81, 2653, 2653, 100, 64892, 65398, 99, 5379, 699, 770, 1608, 658, 244, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6, 100),
('SULAWESI TENGGARA', ' KOTA KENDARI', 8514, 8697, 98, 8172, 8301, 98, 7942, 82, 9685, 34353, 41075, 84, 10755, 12093, 89, 19187, 24096, 80, 16652, 16652, 100, 5962, 17129, 35, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3928, 5204, 75),
('SULAWESI TENGGARA', ' KOTA BAUBAU', 2821, 3662, 77, 2036, 3496, 58, 2653, 2799, 95, 10371, 18354, 57, 2778, 3936, 71, 2014, 67915, 3, 10978, 31865, 34, 4897, 97022, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1499, 680, 220),
('GORONTALO', ' BOALEMO', 2752, 3634, 76, 251, 3469, 7, 2323, 3051, 76, 10439, 15613, 67, 4656, 18519, 25, 418, 23303, 2, 869, 14677, 6, 6534, 38476, 17, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('GORONTALO', ' GORONTALO', 7604, 8247, 92, 6456, 7873, 82, 7165, 7234, 99, 29225, 35193, 83, 6389, 6857, 93, 122157, 235338, 52, 31368, 39182, 80, 29664, 114092, 26, 0, 0, 0, 0, 0, 0, 0, 0, 0, 391, 9104, 4),
('GORONTALO', ' POHUWATO', 2511, 3028, 83, 2552, 3442, 74, 3159, 3442, 92, 9151, 1011, 905, 2736, 2961, 92, 11162, 16571, 67, 7258, 9913, 73, 1503, 1988, 76, 0, 0, 0, 0, 0, 0, 0, 0, 0, 336, 336, 100),
('GORONTALO', ' BONE BOLANGO', 3155, 3489, 90, 2652, 3331, 80, 238, 3039, 8, 5319, 11897, 45, 2938, 3173, 93, 17027, 100464, 17, 3415, 14461, 24, 19062, 19061, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 16, 16, 100),
('GORONTALO', ' GORONTALO UTARA', 1853, 2728, 68, 1996, 2606, 77, 2043, 2181, 94, 3673, 933, 394, 2229, 13163, 17, 1261, 105709, 1, 6731, 8652, 78, 3163, 74002, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 21, 29),
('GORONTALO', ' KOTA GORONTALO', 3982, 3982, 100, 3924, 3924, 100, 3103, 3103, 100, 12125, 12125, 100, 5185, 5185, 100, 20787, 20787, 100, 7476, 7476, 100, 3676, 3676, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8815, 8815, 100),
('SULAWESI BARAT', ' MAJENE', 3305, 4543, 73, 3572, 4337, 82, 3573, 3673, 97, 9926, 1226, 810, 3936, 4023, 98, 12036, 98898, 12, 8137, 11511, 71, 7572, 32779, 23, 0, 0, 0, 0, 0, 0, 0, 0, 0, 150, 250, 60),
('SULAWESI BARAT', ' POLEWALI MANDAR', 7916, 10233, 77, 8137, 9755, 83, 8186, 8322, 98, 37508, 5158, 727, 5841, 13776, 42, 0, 261745, 0, 150, 32221, 0, 2522, 63203, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('SULAWESI BARAT', ' MAMASA', 2851, 0, 0, 2691, 0, 0, 2712, 0, 0, 10361, 0, 0, 4245, 0, 0, 0, 0, 0, 3762, 0, 0, 942, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 36, 0, 0),
('SULAWESI BARAT', ' MAMUJU', 5158, 5956, 87, 5158, 5404, 95, 522, 5256, 10, 23408, 26217, 89, 12517, 13127, 95, 10707, 123679, 9, 1768, 19443, 9, 26419, 32315, 82, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2853, 5995, 48),
('SULAWESI BARAT', ' MAMUJU UTARA', 3468, 4074, 85, 3126, 3889, 80, 3109, 3704, 84, 9888, 16103, 61, 5777, 5777, 100, 0, 0, 0, 1244, 5273, 24, 2419, 108311, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1438, 4349, 33),
('SULAWESI BARAT', ' MAMUJU TENGAH', 1487, 3011, 49, 1627, 2874, 57, 2937, 2735, 107, 2449, 11775, 21, 2617, 4057, 65, 6079, 16138, 38, 7065, 9702, 73, 693, 15488, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 444, 0),
('MALUKU', ' MALUKU TENGGARA BARAT', 2418, 2792, 0, 2404, 2665, 0, 2424, 2538, 0, 10824, 11742, 0, 6109, 9379, 0, 5886, 5886, 100, 8674, 9379, 92, 5506, 5506, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 17, 17, 100),
('MALUKU', ' MALUKU TENGGARA', 2444, 2885, 0, 1763, 2754, 0, 2103, 2654, 0, 8053, 2645, 0, 419, 10362, 0, 0, 55148, 0, 1624, 9868, 16, 5081, 18153, 28, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2781, 3377, 82),
('MALUKU', ' MALUKU TENGAH', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('MALUKU', ' BURU', 2296, 333, 0, 2026, 3179, 0, 199, 3027, 0, 7218, 1619, 0, 263, 3231, 0, 0, 77758, 0, 983, 763, 129, 1679, 6063, 28, 0, 0, 0, 0, 0, 0, 0, 0, 0, 88, 122, 72),
('MALUKU', ' KEPULAUAN ARU', 1215, 2945, 0, 1529, 2811, 0, 1542, 2762, 0, 6096, 11944, 0, 69, 138, 0, 0, 0, 0, 1689, 1689, 100, 162, 6562, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1326, 1356, 98),
('MALUKU', ' SERAM BAGIAN BARAT', 2206, 2483, 0, 1928, 2358, 0, 1536, 1843, 0, 7193, 7558, 0, 5319, 8136, 0, 7145, 14875, 48, 2453, 2898, 85, 929, 3811, 24, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('MALUKU', ' SERAM BAGIAN TIMUR', 1716, 3375, 0, 313, 3222, 0, 1848, 3067, 0, 6464, 13146, 0, 4901, 0, 0, 0, 0, 0, 837, 966, 87, 2726, 3888, 70, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('MALUKU', ' MALUKU BARAT DAYA', 1192, 2235, 0, 460, 2134, 0, 1077, 2032, 0, 3579, 9364, 0, 1508, 736, 0, 0, 39807, 0, 593, 6963, 9, 530, 351, 151, 0, 0, 0, 0, 0, 0, 0, 0, 0, 453, 500, 91),
('MALUKU', ' BURU SELATAN', 2071, 2575, 0, 192, 2458, 0, 1067, 1067, 0, 7164, 8196, 0, 2284, 1398, 0, 4279, 4279, 100, 3022, 3022, 100, 876, 1112, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 46, 100),
('MALUKU', ' KOTA AMBON', 7375, 9272, 0, 5036, 8848, 0, 6783, 8429, 0, 32323, 50297, 0, 13226, 13226, 0, 9694, 33796, 29, 721, 721, 100, 14026, 164, 8552, 0, 0, 0, 0, 0, 0, 0, 0, 0, 58, 7996, 1),
('MALUKU', ' KOTA TUAL', 1463, 1837, 0, 853, 1735, 0, 127, 127, 0, 598, 8164, 0, 1163, 1163, 0, 743, 4497, 17, 7188, 32528, 22, 1187, 1317, 90, 0, 0, 0, 0, 0, 0, 0, 0, 0, 17, 1866, 1),
('MALUKU UTARA', ' HALMAHERA BARAT', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('MALUKU UTARA', ' HALMAHERA TENGAH', 1421, 1688, 84, 1099, 1593, 69, 1107, 1517, 73, 2425, 2947, 82, 949, 1104, 86, 2076, 38906, 5, 3915, 3553, 110, 2109, 2109, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 100),
('MALUKU UTARA', ' KEPULAUAN SULA', 1331, 3137, 42, 1011, 2996, 34, 452, 1141, 40, 494, 7404, 7, 1234, 2218, 56, 0, 0, 0, 281, 11773, 2, 1645, 88301, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 150, 425, 35),
('MALUKU UTARA', ' HALMAHERA SELATAN', 4637, 6461, 72, 2867, 6168, 46, 4155, 5874, 71, 3155, 23522, 13, 0, 32975, 0, 0, 94174, 0, 0, 43055, 0, 0, 46605, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0),
('MALUKU UTARA', ' HALMAHERA UTARA', 3456, 4111, 84, 3347, 3347, 100, 2802, 332, 844, 1298, 10643, 12, 8389, 8389, 100, 50118, 50118, 100, 1838, 1937, 95, 1599, 1599, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1838, 140, 1313),
('MALUKU UTARA', ' HALMAHERA TIMUR', 1359, 2444, 56, 1129, 2333, 48, 1093, 2222, 49, 2108, 26438, 8, 1146, 13172, 9, 472, 22831, 2, 394, 7464, 5, 3478, 3478, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 100),
('MALUKU UTARA', ' PULAU MOROTAI', 2682, 1858, 144, 2368, 1774, 133, 3038, 1689, 180, 9088, 6917, 131, 677, 3385, 20, 446, 6588, 7, 8818, 6588, 134, 6822, 8439, 81, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 16, 38),
('MALUKU UTARA', ' PULAU TALIABU', 686, 1576, 44, 207, 1504, 14, 731, 1432, 51, 2241, 537, 417, 0, 0, 0, 0, 0, 0, 0, 0, 0, 354, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('MALUKU UTARA', ' KOTA TERNATE', 4532, 4819, 94, 4079, 4271, 96, 4153, 429, 968, 1312, 15825, 8, 7474, 7474, 100, 52914, 146923, 36, 18641, 213, 8752, 17529, 37906, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3336, 6493, 51),
('MALUKU UTARA', ' KOTA TIDORE KEPULAUAN', 1825, 2444, 75, 1828, 2333, 78, 1756, 2222, 79, 8735, 12938, 68, 4044, 4343, 93, 4251, 49504, 9, 4538, 9997, 45, 2483, 40945, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 270, 2627, 10),
('PAPUA BARAT', ' FAKFAK', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA BARAT', ' KAIMANA', 772, 1608, 48, 881, 1535, 57, 879, 879, 100, 5534, 6159, 90, 0, 0, 0, 0, 0, 0, 0, 0, 0, 82, 145, 57, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA BARAT', ' TELUK WONDAMA', 305, 1051, 29, 916, 1003, 91, 916, 963, 95, 0, 0, 0, 0, 0, 0, 0, 0, 0, 550, 1, 55000, 246, 360, 68, 0, 0, 0, 0, 0, 0, 0, 0, 0, 280, 280, 100),
('PAPUA BARAT', ' TELUK BINTUNI', 1094, 1765, 62, 1462, 1684, 87, 1511, 1566, 96, 4342, 541, 803, 3602, 0, 0, 0, 40791, 0, 370, 1394, 27, 2983, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46, 46, 100),
('PAPUA BARAT', ' MANOKWARI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA BARAT', ' SORONG SELATAN', 157, 137, 115, 134, 1307, 10, 476, 1167, 41, 3268, 5705, 57, 1798, 1045, 172, 4667, 13227, 35, 2165, 1104, 196, 2765, 6832, 40, 0, 0, 0, 0, 0, 0, 0, 0, 0, 40, 40, 100);
INSERT INTO `tempdata` (`Provinsi`, `kabkota`, `r1`, `s1`, `p1`, `r2`, `s2`, `p2`, `r3`, `s3`, `p3`, `r4`, `s4`, `p4`, `r5`, `s5`, `p5`, `r6`, `s6`, `p6`, `r7`, `s7`, `p7`, `r8`, `s8`, `p8`, `r9`, `s9`, `p9`, `r10`, `s10`, `p10`, `r11`, `s11`, `p11`, `r12`, `s12`, `p12`) VALUES
('PAPUA BARAT', ' SORONG', 0, 2146, 0, 0, 2049, 0, 0, 1815, 0, 0, 8939, 0, 0, 1679, 0, 0, 55803, 0, 0, 3022, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA BARAT', ' RAJA AMPAT', 586, 145, 404, 495, 1348, 37, 428, 1276, 34, 9611, 11349, 85, 0, 1046, 0, 0, 30243, 0, 0, 2058, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 98, 1647, 6),
('PAPUA BARAT', ' TAMBRAUW', 0, 444, 0, 0, 424, 0, 0, 371, 0, 0, 150, 0, 0, 333, 0, 0, 0, 0, 0, 486, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA BARAT', ' MAYBRAT', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA BARAT', ' MANOKWARI SELATAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA BARAT', ' PEGUNUNGAN ARFAK', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA BARAT', ' KOTA SORONG', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA', ' MERAUKE', 0, 5623, 0, 0, 5367, 0, 0, 5121, 0, 0, 10386, 0, 0, 2253, 0, 0, 152776, 0, 0, 15081, 0, 0, 4782, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 61656, 0),
('PAPUA', ' JAYAWIJAYA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA', ' JAYAPURA', 1511, 3191, 47, 2567, 3046, 84, 2138, 2713, 79, 10289, 1056, 974, 2239, 2239, 100, 2678, 84399, 3, 1801, 3899, 46, 2678, 2693, 99, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9946, 34758, 29),
('PAPUA', ' NABIRE', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA', ' KEPULAUAN YAPEN', 1398, 2616, 53, 1918, 2484, 77, 1905, 2375, 80, 8051, 11093, 73, 1998, 11224, 18, 1432, 59175, 2, 2887, 5974, 48, 226, 1915, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1027, 26222, 4),
('PAPUA', ' BIAK NUMFOR', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA', ' PANIAI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA', ' PUNCAK JAYA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA', ' MIMIKA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA', ' BOVEN DIGOEL', 931, 2126, 44, 1179, 2126, 55, 780, 977, 80, 1014, 7128, 14, 194, 2352, 8, 898, 44362, 2, 1039, 1943, 53, 813, 1351, 60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 901, 18271, 5),
('PAPUA', ' MAPPI', 656, 3189, 21, 2051, 3044, 67, 1309, 2899, 45, 10781, 13559, 80, 2075, 0, 0, 0, 0, 0, 6928, 0, 0, 1031, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 279, 0, 0),
('PAPUA', ' ASMAT', 123, 321, 38, 123, 321, 38, 123, 321, 38, 123, 321, 38, 0, 321, 0, 123, 321, 38, 123, 321, 38, 123, 321, 38, 0, 0, 0, 0, 0, 0, 0, 0, 0, 123, 321, 38),
('PAPUA', ' YAHUKIMO', 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA', ' PEGUNUNGAN BINTANG', 1335, 2258, 59, 981, 2159, 45, 0, 2064, 0, 0, 9578, 0, 0, 12945, 0, 0, 4731, 0, 0, 602, 0, 0, 1497, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 19789, 0),
('PAPUA', ' TOLIKARA', 309, 2514, 12, 350, 2394, 15, 385, 2288, 17, 4004, 10638, 38, 1669, 19456, 9, 6537, 9369, 70, 777, 1791, 43, 123, 2749, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3873, 3769, 103),
('PAPUA', ' SARMI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA', ' KEEROM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA', ' WAROPEN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA', ' SUPIORI', 337, 0, 0, 309, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA', ' MAMBERAMO RAYA', 463, 212, 218, 107, 82, 130, 0, 72, 0, 0, 72, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA', ' NDUGA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA', ' LANNY JAYA', 15, 0, 0, 11, 0, 0, 59, 0, 0, 38, 0, 0, 41, 0, 0, 28, 0, 0, 22, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, 0, 0),
('PAPUA', ' MAMBERAMO TENGAH', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA', ' YALIMO', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA', ' PUNCAK', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA', ' DOGIYAI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA', ' INTAN JAYA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA', ' DEIYAI', 0, 1000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('PAPUA', ' KOTA JAYAPURA', 4948, 659, 751, 4211, 6291, 67, 5226, 5991, 87, 29129, 29369, 99, 102, 9693, 1, 2679, 250583, 1, 16741, 24964, 67, 13274, 29394, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4755, 1386, 343);

-- --------------------------------------------------------

--
-- Table structure for table `tempdatadetail`
--

CREATE TABLE `tempdatadetail` (
  `Wilayah` varchar(14) CHARACTER SET utf8 DEFAULT NULL,
  `Indikator_Kinerja` varchar(119) CHARACTER SET utf8 DEFAULT NULL,
  `Indikator_Pencapaian` varchar(113) CHARACTER SET utf8 DEFAULT NULL,
  `Item` varchar(109) CHARACTER SET utf8 DEFAULT NULL,
  `pencapaian` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tempdatadetail`
--

INSERT INTO `tempdatadetail` (`Wilayah`, `Indikator_Kinerja`, `Indikator_Pencapaian`, `Item`, `pencapaian`) VALUES
('Provinsi', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di dalam Panti Sosial', 'Jumlah   Warga   Negara penyandang disabilitas yang memperoleh rehabilitasi sosial diluar panti', 'a). Permakanan ', 1201),
('Provinsi', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di dalam Panti Sosial', 'Jumlah   Warga   Negara penyandang disabilitas yang memperoleh rehabilitasi sosial diluar panti', 'b). Sandang', 1213),
('Provinsi', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di dalam Panti Sosial', 'Jumlah   Warga   Negara penyandang disabilitas yang memperoleh rehabilitasi sosial diluar panti', 'c). Asrama yang mudah diakses', 4121),
('Provinsi', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di dalam Panti Sosial', 'Jumlah   Warga   Negara penyandang disabilitas yang memperoleh rehabilitasi sosial diluar panti', 'd). Alat bantu', 4212),
('Provinsi', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di dalam Panti Sosial', 'Jumlah   Warga   Negara penyandang disabilitas yang memperoleh rehabilitasi sosial diluar panti', 'e). Perbekalan kesehatan ', 1423),
('Provinsi', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di dalam Panti Sosial', 'Jumlah   Warga   Negara penyandang disabilitas yang memperoleh rehabilitasi sosial diluar panti', 'f). Bimbingan fisik, mental spiritual, dan sosial', 1345),
('Provinsi', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di dalam Panti Sosial', 'Jumlah   Warga   Negara penyandang disabilitas yang memperoleh rehabilitasi sosial diluar panti', 'g). Bimbingan keterampilan hidup sehari-hari', 1113),
('Provinsi', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di dalam Panti Sosial', 'Jumlah   Warga   Negara penyandang disabilitas yang memperoleh rehabilitasi sosial diluar panti', 'h). Pembuatan nomor induk kependudukan', 4121),
('Provinsi', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di dalam Panti Sosial', 'Jumlah   Warga   Negara penyandang disabilitas yang memperoleh rehabilitasi sosial diluar panti', 'i). Akses ke layanan pendidikan dan kesehatan dasar', 2121),
('Provinsi', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di dalam Panti Sosial', 'Jumlah   Warga   Negara penyandang disabilitas yang memperoleh rehabilitasi sosial diluar panti', 'j). Pelayanan penelusuran keluarga', 4121),
('Provinsi', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di dalam Panti Sosial', 'Jumlah   Warga   Negara penyandang disabilitas yang memperoleh rehabilitasi sosial diluar panti', 'k). Pelayanan reunifikasi keluarga', 1434),
('Provinsi', 'Rehabilitasi Sosial Dasar Anak Terlantar di dalam Panti Sosial', 'Jumlah anak telantar yang memperoleh   rehabilitasi sosial diluar panti', 'a). Pengasuhan', 121),
('Provinsi', 'Rehabilitasi Sosial Dasar Anak Terlantar di dalam Panti Sosial', 'Jumlah anak telantar yang memperoleh   rehabilitasi sosial diluar panti', 'b). Permakanan', 141),
('Provinsi', 'Rehabilitasi Sosial Dasar Anak Terlantar di dalam Panti Sosial', 'Jumlah anak telantar yang memperoleh   rehabilitasi sosial diluar panti', 'c). Sandang', 121),
('Provinsi', 'Rehabilitasi Sosial Dasar Anak Terlantar di dalam Panti Sosial', 'Jumlah anak telantar yang memperoleh   rehabilitasi sosial diluar panti', 'd). Asrama yang mudah diakses', 423),
('Provinsi', 'Rehabilitasi Sosial Dasar Anak Terlantar di dalam Panti Sosial', 'Jumlah anak telantar yang memperoleh   rehabilitasi sosial diluar panti', 'e). Perbekalan kesehatan ', 434),
('Provinsi', 'Rehabilitasi Sosial Dasar Anak Terlantar di dalam Panti Sosial', 'Jumlah anak telantar yang memperoleh   rehabilitasi sosial diluar panti', 'f). Bimbingan fisik, mental spiritual, dan sosial', 141),
('Provinsi', 'Rehabilitasi Sosial Dasar Anak Terlantar di dalam Panti Sosial', 'Jumlah anak telantar yang memperoleh   rehabilitasi sosial diluar panti', 'g). Bimbingan keterampilan hidup sehari-hari', 454),
('Provinsi', 'Rehabilitasi Sosial Dasar Anak Terlantar di dalam Panti Sosial', 'Jumlah anak telantar yang memperoleh   rehabilitasi sosial diluar panti', 'h). Pembuatan akta kelahiran, nomor induk kependudukan, dan kartu identitas anak', 767),
('Provinsi', 'Rehabilitasi Sosial Dasar Anak Terlantar di dalam Panti Sosial', 'Jumlah anak telantar yang memperoleh   rehabilitasi sosial diluar panti', 'i). Akses ke layanan pendidikan dan kesehatan dasar', 343),
('Provinsi', 'Rehabilitasi Sosial Dasar Anak Terlantar di dalam Panti Sosial', 'Jumlah anak telantar yang memperoleh   rehabilitasi sosial diluar panti', 'j). Pelayanan penelusuran keluarga', 342),
('Provinsi', 'Rehabilitasi Sosial Dasar Anak Terlantar di dalam Panti Sosial', 'Jumlah anak telantar yang memperoleh   rehabilitasi sosial diluar panti', 'k). Pelayanan reunifikasi keluarga', 242),
('Provinsi', 'Rehabilitasi Sosial Dasar Anak Terlantar di dalam Panti Sosial', 'Jumlah anak telantar yang memperoleh   rehabilitasi sosial diluar panti', 'l). Akses layanan pengasuhan kepada keluarga pengganti', 656),
('Provinsi', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di daam Panti Sosial', 'Jumlah   Warga   Negara lanjut usia terlantar yang memperoleh rehabilitasi sosial diluar panti', 'a). Permakanan', 767),
('Provinsi', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di daam Panti Sosial', 'Jumlah   Warga   Negara lanjut usia terlantar yang memperoleh rehabilitasi sosial diluar panti', 'b). Sandang', 232),
('Provinsi', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di daam Panti Sosial', 'Jumlah   Warga   Negara lanjut usia terlantar yang memperoleh rehabilitasi sosial diluar panti', 'c). Asrama yang mudah diakses', 656),
('Provinsi', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di daam Panti Sosial', 'Jumlah   Warga   Negara lanjut usia terlantar yang memperoleh rehabilitasi sosial diluar panti', 'd). Alat bantu', 232),
('Provinsi', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di daam Panti Sosial', 'Jumlah   Warga   Negara lanjut usia terlantar yang memperoleh rehabilitasi sosial diluar panti', 'e). Perbekalan kesehatan', 657),
('Provinsi', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di daam Panti Sosial', 'Jumlah   Warga   Negara lanjut usia terlantar yang memperoleh rehabilitasi sosial diluar panti', 'f). Bimbingan fisik, mental spiritual, dan sosial', 434),
('Provinsi', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di daam Panti Sosial', 'Jumlah   Warga   Negara lanjut usia terlantar yang memperoleh rehabilitasi sosial diluar panti', 'g). Bimbingan keterampilan hidup sehari-hari', 656),
('Provinsi', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di daam Panti Sosial', 'Jumlah   Warga   Negara lanjut usia terlantar yang memperoleh rehabilitasi sosial diluar panti', 'h). Fasilitasi pembuatan nomor induk kependudukan', 343),
('Provinsi', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di daam Panti Sosial', 'Jumlah   Warga   Negara lanjut usia terlantar yang memperoleh rehabilitasi sosial diluar panti', 'i). Akses ke layanan kesehatan dasar', 776),
('Provinsi', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di daam Panti Sosial', 'Jumlah   Warga   Negara lanjut usia terlantar yang memperoleh rehabilitasi sosial diluar panti', 'j). Pelayanan penelusuran keluarga', 656),
('Provinsi', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di daam Panti Sosial', 'Jumlah   Warga   Negara lanjut usia terlantar yang memperoleh rehabilitasi sosial diluar panti', 'k). Pelayanan reunifikasi keluarga', 343),
('Provinsi', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di daam Panti Sosial', 'Jumlah   Warga   Negara lanjut usia terlantar yang memperoleh rehabilitasi sosial diluar panti', 'l). Pemulasaraan', 232),
('Provinsi', 'Rehabilitasi Sosial Dasar Tuna Sosial Khususnya Gelandangan dan Pengemis di dalam Panti Sosial', 'Jumlah Warga Negara/ gelandangan   dan pengemis  yang memperoleh rehabilitasi sosialdasartuna sosial diluar panti', 'a). Permakanan', 546),
('Provinsi', 'Rehabilitasi Sosial Dasar Tuna Sosial Khususnya Gelandangan dan Pengemis di dalam Panti Sosial', 'Jumlah Warga Negara/ gelandangan   dan pengemis  yang memperoleh rehabilitasi sosialdasartuna sosial diluar panti', 'b). Sandang', 656),
('Provinsi', 'Rehabilitasi Sosial Dasar Tuna Sosial Khususnya Gelandangan dan Pengemis di dalam Panti Sosial', 'Jumlah Warga Negara/ gelandangan   dan pengemis  yang memperoleh rehabilitasi sosialdasartuna sosial diluar panti', 'c). Asrama/ cottage yang mudah diakses', 766),
('Provinsi', 'Rehabilitasi Sosial Dasar Tuna Sosial Khususnya Gelandangan dan Pengemis di dalam Panti Sosial', 'Jumlah Warga Negara/ gelandangan   dan pengemis  yang memperoleh rehabilitasi sosialdasartuna sosial diluar panti', 'd). Perbekalan kesehatan', 454),
('Provinsi', 'Rehabilitasi Sosial Dasar Tuna Sosial Khususnya Gelandangan dan Pengemis di dalam Panti Sosial', 'Jumlah Warga Negara/ gelandangan   dan pengemis  yang memperoleh rehabilitasi sosialdasartuna sosial diluar panti', 'e). Bimbingan fisik, mental spiritual, dan sosial', 766),
('Provinsi', 'Rehabilitasi Sosial Dasar Tuna Sosial Khususnya Gelandangan dan Pengemis di dalam Panti Sosial', 'Jumlah Warga Negara/ gelandangan   dan pengemis  yang memperoleh rehabilitasi sosialdasartuna sosial diluar panti', 'f). Bimbingan keterampilan dasar', 878),
('Provinsi', 'Rehabilitasi Sosial Dasar Tuna Sosial Khususnya Gelandangan dan Pengemis di dalam Panti Sosial', 'Jumlah Warga Negara/ gelandangan   dan pengemis  yang memperoleh rehabilitasi sosialdasartuna sosial diluar panti', 'g). Fasilitasi pembuatan nomor induk kependudukan, akta kelahiran, surat nikah, dan/atau kartu identitas anak', 878),
('Provinsi', 'Rehabilitasi Sosial Dasar Tuna Sosial Khususnya Gelandangan dan Pengemis di dalam Panti Sosial', 'Jumlah Warga Negara/ gelandangan   dan pengemis  yang memperoleh rehabilitasi sosialdasartuna sosial diluar panti', 'h). Akses ke layanan kesehatan dasar', 876),
('Provinsi', 'Rehabilitasi Sosial Dasar Tuna Sosial Khususnya Gelandangan dan Pengemis di dalam Panti Sosial', 'Jumlah Warga Negara/ gelandangan   dan pengemis  yang memperoleh rehabilitasi sosialdasartuna sosial diluar panti', 'i). Pemulangan ke daerah asal', 565),
('Provinsi', 'Perlindungan dan Jaminan Sosial pada Saat dan Setelah Tanggap Darurat Bencana bagi Korban Bencana daerah provinsi', 'Jumlah    Warga    Negara korban bencana kab/kota yang memperoleh perlindungan dan jaminan sosial', 'a). Permakanan ', 454),
('Provinsi', 'Perlindungan dan Jaminan Sosial pada Saat dan Setelah Tanggap Darurat Bencana bagi Korban Bencana daerah provinsi', 'Jumlah    Warga    Negara korban bencana kab/kota yang memperoleh perlindungan dan jaminan sosial', 'b). Sandang', 767),
('Provinsi', 'Perlindungan dan Jaminan Sosial pada Saat dan Setelah Tanggap Darurat Bencana bagi Korban Bencana daerah provinsi', 'Jumlah    Warga    Negara korban bencana kab/kota yang memperoleh perlindungan dan jaminan sosial', 'c). Tempat penampungan pengungsi', 343),
('Provinsi', 'Perlindungan dan Jaminan Sosial pada Saat dan Setelah Tanggap Darurat Bencana bagi Korban Bencana daerah provinsi', 'Jumlah    Warga    Negara korban bencana kab/kota yang memperoleh perlindungan dan jaminan sosial', 'd). Penanganan khusus bagi kelompok rentan', 657),
('Provinsi', 'Perlindungan dan Jaminan Sosial pada Saat dan Setelah Tanggap Darurat Bencana bagi Korban Bencana daerah provinsi', 'Jumlah    Warga    Negara korban bencana kab/kota yang memperoleh perlindungan dan jaminan sosial', 'e). Dukungan psikososial', 343),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di Luar Panti Sosial', NULL, 'a). Permakanan paling banyak diberikan paling lama 7 hari', 657),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di Luar Panti Sosial', NULL, 'b). Sandang ', 232),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di Luar Panti Sosial', NULL, 'c). Alat bantu', 656),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di Luar Panti Sosial', NULL, 'd). Perbekalan kesehatan', 121),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di Luar Panti Sosial', NULL, 'e). Bimbingan fisik, mental spiritual, dan sosial kepada Penyandang Disabilitas terlantar', 323),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di Luar Panti Sosial', NULL, 'f). Bimbingan sosial kepada keluarga Penyandang Disabilitas terlantar dan masyarakat', 131),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di Luar Panti Sosial', NULL, 'g). Fasilitasi pembuatan nomor induk kependudukan, akta kelahiran, surat nikah dan kartu identitas anak', 434),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di Luar Panti Sosial', NULL, 'h). Akses ke layanan pendidikan dan kesehatan dasar', 545),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di Luar Panti Sosial', NULL, 'i). Pelayanan penelusuran keluarga', 343),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di Luar Panti Sosial', NULL, 'j). Reunifikasi dan atau/ reintegrasi sosial', 232),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Penyandang Disabilitas Terlantar di Luar Panti Sosial', NULL, 'k). Rujukan', 545),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Anak Terlantar di Luar Panti Sosial', NULL, 'a). Permakanan paling banyak diberikan paling lama 7 hari', 434),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Anak Terlantar di Luar Panti Sosial', NULL, 'b). Sandang ', 242),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Anak Terlantar di Luar Panti Sosial', NULL, 'c). Alat bantu', 546),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Anak Terlantar di Luar Panti Sosial', NULL, 'd). Perbekalan kesehatan', 232),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Anak Terlantar di Luar Panti Sosial', NULL, 'e). Bimbingan fisik, mental spiritual, dan sosial kepada Anak terlantar', 232),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Anak Terlantar di Luar Panti Sosial', NULL, 'f). Bimbingan sosial kepada keluarga Anak terlantar dan masyarakat', 455),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Anak Terlantar di Luar Panti Sosial', NULL, 'g). Fasilitasi pembuatan nomor induk kependudukan, akta kelahiran, surat nikah dan kartu identitas anak', 232),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Anak Terlantar di Luar Panti Sosial', NULL, 'h). Akses ke layanan pendidikan dan kesehatan dasar', 545),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Anak Terlantar di Luar Panti Sosial', NULL, 'i). Pelayanan penelusuran keluarga', 656),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Anak Terlantar di Luar Panti Sosial', NULL, 'j). Reunifikasi dan atau/ reintegrasi sosial', 121),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Anak Terlantar di Luar Panti Sosial', NULL, 'k). Rujukan', 121),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di Luar Panti Sosial', NULL, 'a). Permakanan paling banyak diberikan paling lama 7 hari', 332),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di Luar Panti Sosial', NULL, 'b). Sandang ', 121),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di Luar Panti Sosial', NULL, 'c). Alat bantu', 434),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di Luar Panti Sosial', NULL, 'e). Bimbingan fisik, mental spiritual, dan sosial kepada Lanjut Usia terlantar', 332),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di Luar Panti Sosial', NULL, 'd). Perbekalan kesehatan', 434),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di Luar Panti Sosial', NULL, 'f). Bimbingan sosial kepada keluarga Lanjut Usia terlantar dan masyarakat', 232),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di Luar Panti Sosial', NULL, 'g). Fasilitasi pembuatan nomor induk kependudukan, akta kelahiran, surat nikah dan kartu identitas anak', 435),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di Luar Panti Sosial', NULL, 'h). Akses ke layanan pendidikan dan kesehatan dasar', 656),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di Luar Panti Sosial', NULL, 'i). Pelayanan penelusuran keluarga', 431),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di Luar Panti Sosial', NULL, 'j). Reunifikasi dan atau/ reintegrasi sosial', 111),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Lanjut Usia Terlantar di Luar Panti Sosial', NULL, 'k). Rujukan', 122),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Tuna Sosial Khususnya Gelandangan dan Pengemis di Luar Panti Sosial', NULL, 'a). Permakanan paling banyak diberikan paling lama 7 hari', 131),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Tuna Sosial Khususnya Gelandangan dan Pengemis di Luar Panti Sosial', NULL, 'b). Sandang ', 121),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Tuna Sosial Khususnya Gelandangan dan Pengemis di Luar Panti Sosial', NULL, 'c). Alat bantu', 111),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Tuna Sosial Khususnya Gelandangan dan Pengemis di Luar Panti Sosial', NULL, 'd). Perbekalan kesehatan', 211),
('Kabupaten/Kota', 'Rehabilitasi Sosial Dasar Tuna Sosial Khususnya Gelandangan dan Pengemis di Luar Panti Sosial', NULL, 'e). Bimbingan fisik, mental spiritual, dan sosial kepada Gelandangan dan Pengemis', 211),
('Kabupaten/Kota', 'Perlindungan dan Jaminan Sosial pada Saat dan Setelah Tanggap Darurat Bencana bagi Korban Bencana daerah Kabupaten/Kota', NULL, 'a). Permakanan', 311),
('Kabupaten/Kota', 'Perlindungan dan Jaminan Sosial pada Saat dan Setelah Tanggap Darurat Bencana bagi Korban Bencana daerah Kabupaten/Kota', NULL, 'b). Sandang', 121),
('Kabupaten/Kota', 'Perlindungan dan Jaminan Sosial pada Saat dan Setelah Tanggap Darurat Bencana bagi Korban Bencana daerah Kabupaten/Kota', NULL, 'c). Tempat penampungan pengungsi', 323),
('Kabupaten/Kota', 'Perlindungan dan Jaminan Sosial pada Saat dan Setelah Tanggap Darurat Bencana bagi Korban Bencana daerah Kabupaten/Kota', NULL, 'd). Penanganan khusus bagi kelompok rentan', 121),
('Kabupaten/Kota', 'Perlindungan dan Jaminan Sosial pada Saat dan Setelah Tanggap Darurat Bencana bagi Korban Bencana daerah Kabupaten/Kota', NULL, 'e). Dukungan psikososial', 434);

-- --------------------------------------------------------

--
-- Stand-in structure for view `viewtempdetail`
-- (See below for the actual view)
--
CREATE TABLE `viewtempdetail` (
`wilayah` varchar(14)
,`Indikator_Kinerja` varchar(119)
,`Indikator_pencapaian` varchar(113)
,`items` text
,`pencapaian` text
,`TotalPencapaian` decimal(32,0)
,`SasaranPencapaian` decimal(33,0)
);

-- --------------------------------------------------------

--
-- Table structure for table `wilayah`
--

CREATE TABLE `wilayah` (
  `deptid` varchar(4) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wilayah`
--

INSERT INTO `wilayah` (`deptid`, `nama`) VALUES
('11', 'NANGGROE ACEH DARUSSALAM'),
('12', 'SUMATERA UTARA'),
('13', 'SUMATERA BARAT'),
('14', 'RIAU'),
('15', 'JAMBI'),
('16', 'SUMATERA SELATAN'),
('17', 'BENGKULU'),
('18', 'LAMPUNG'),
('19', 'KEPULAUAN BANGKA BELITUNG'),
('21', 'KEPULAUAN RIAU'),
('31', 'DKI JAKARTA'),
('32', 'JAWA BARAT'),
('33', 'JAWA TENGAH'),
('34', 'DI YOGYAKARTA'),
('35', 'JAWA TIMUR'),
('36', 'BANTEN'),
('51', 'BALI'),
('52', 'NUSA TENGGARA BARAT'),
('53', 'NUSA TENGGARA TIMUR'),
('61', 'KALIMANTAN BARAT'),
('62', 'KALIMANTAN TENGAH'),
('63', 'KALIMANTAN SELATAN'),
('64', 'KALIMANTAN TIMUR'),
('65', 'KALIMANTAN UTARA'),
('71', 'SULAWESI UTARA'),
('72', 'SULAWESI TENGAH'),
('73', 'SULAWESI SELATAN'),
('74', 'SULAWESI TENGGARA'),
('75', 'GORONTALO'),
('76', 'SULAWESI BARAT'),
('81', 'MALUKU'),
('82', 'MALUKU UTARA'),
('91', 'PAPUA'),
('92', 'PAPUA BARAT');

-- --------------------------------------------------------

--
-- Structure for view `viewtempdetail`
--
DROP TABLE IF EXISTS `viewtempdetail`;

CREATE ALGORITHM=UNDEFINED DEFINER=`them9155`@`localhost` SQL SECURITY DEFINER VIEW `viewtempdetail`  AS  select `tempdatadetail`.`Wilayah` AS `wilayah`,`tempdatadetail`.`Indikator_Kinerja` AS `Indikator_Kinerja`,`tempdatadetail`.`Indikator_Pencapaian` AS `Indikator_pencapaian`,group_concat(`tempdatadetail`.`Item` separator '<br/>') AS `items`,group_concat(`tempdatadetail`.`pencapaian` separator '<br/>') AS `pencapaian`,sum(`tempdatadetail`.`pencapaian`) AS `TotalPencapaian`,(sum(`tempdatadetail`.`pencapaian`) + 10) AS `SasaranPencapaian` from `tempdatadetail` group by `tempdatadetail`.`Wilayah`,`tempdatadetail`.`Indikator_Kinerja`,`tempdatadetail`.`Indikator_Pencapaian` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `departement`
--
ALTER TABLE `departement`
  ADD PRIMARY KEY (`deptid`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jns_akun`
--
ALTER TABLE `jns_akun`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kd_aktiva` (`kd_aktiva`);

--
-- Indexes for table `kab`
--
ALTER TABLE `kab`
  ADD PRIMARY KEY (`id_kabkot`);

--
-- Indexes for table `project_requests`
--
ALTER TABLE `project_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prop`
--
ALTER TABLE `prop`
  ADD PRIMARY KEY (`id_prov`);

--
-- Indexes for table `subdit`
--
ALTER TABLE `subdit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_anggaran`
--
ALTER TABLE `tbl_anggaran`
  ADD PRIMARY KEY (`id_tr_dinas`);

--
-- Indexes for table `tbl_dinas`
--
ALTER TABLE `tbl_dinas`
  ADD PRIMARY KEY (`id_tr_dinas`);

--
-- Indexes for table `tbl_dinas_dokumen`
--
ALTER TABLE `tbl_dinas_dokumen`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_dinas_indikator`
--
ALTER TABLE `tbl_dinas_indikator`
  ADD PRIMARY KEY (`indikator_id`);

--
-- Indexes for table `tbl_dinas_indikator_detail`
--
ALTER TABLE `tbl_dinas_indikator_detail`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_masalah`
--
ALTER TABLE `tbl_masalah`
  ADD PRIMARY KEY (`id_tr`);

--
-- Indexes for table `tbl_master_dinas`
--
ALTER TABLE `tbl_master_dinas`
  ADD PRIMARY KEY (`dinas_id`);

--
-- Indexes for table `tbl_master_indikator`
--
ALTER TABLE `tbl_master_indikator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_master_masalah`
--
ALTER TABLE `tbl_master_masalah`
  ADD PRIMARY KEY (`masalah_id`);

--
-- Indexes for table `tbl_setting`
--
ALTER TABLE `tbl_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`u_name`);

--
-- Indexes for table `wilayah`
--
ALTER TABLE `wilayah`
  ADD PRIMARY KEY (`deptid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jns_akun`
--
ALTER TABLE `jns_akun`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;
--
-- AUTO_INCREMENT for table `project_requests`
--
ALTER TABLE `project_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `subdit`
--
ALTER TABLE `subdit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_anggaran`
--
ALTER TABLE `tbl_anggaran`
  MODIFY `id_tr_dinas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_dinas`
--
ALTER TABLE `tbl_dinas`
  MODIFY `id_tr_dinas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `tbl_dinas_dokumen`
--
ALTER TABLE `tbl_dinas_dokumen`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_dinas_indikator`
--
ALTER TABLE `tbl_dinas_indikator`
  MODIFY `indikator_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=168;
--
-- AUTO_INCREMENT for table `tbl_dinas_indikator_detail`
--
ALTER TABLE `tbl_dinas_indikator_detail`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=728;
--
-- AUTO_INCREMENT for table `tbl_masalah`
--
ALTER TABLE `tbl_masalah`
  MODIFY `id_tr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_master_dinas`
--
ALTER TABLE `tbl_master_dinas`
  MODIFY `dinas_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_master_indikator`
--
ALTER TABLE `tbl_master_indikator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `tbl_master_masalah`
--
ALTER TABLE `tbl_master_masalah`
  MODIFY `masalah_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_setting`
--
ALTER TABLE `tbl_setting`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
