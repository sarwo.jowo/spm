<?php
//1. Nomor Card Reader = sam_csn		2.Nomor Kartu Produsen = terminal_sn  	Nomor Kartu SAM = sam_cn		Nomor PCID = pcid_number
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Free extends REST_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();       
		$this->load->model('Access','access',true);			
    }
    
	function Prop_post(){
		$param = $this->input->post();
		$user_id = isset($param['user_id']) ? $param['user_id'] : '';
		$password = isset($param['password']) ? $param['password'] : '';
		//error_log(MD5($password));
		if($user_id != '' AND $password != '')
		{	
        $this->db->where('user_id', $user_id);
        $this->db->where('user_passwd', MD5($password));
        $query = $this->db->from('sys_users')->get();
		$num_results = $query->num_rows();		
		if ($num_results  > 0)
		{	
		$select = 'id_prov, nama_prov';
		$sort = '';				
		$where = '';
		$dt = $this->access->readtable('all_int_provinsi',$select,'','','','','')->result_array();			
		$this->response($dt, 200);
		}
		else {
		$this->response(array('status' => 'failed', 502));
		}
		}
	}	
	function Kab_post(){
		$param = $this->input->post();
		$user_id = isset($param['user_id']) ? $param['user_id'] : '';
		$password = isset($param['password']) ? $param['password'] : '';
		//error_log(MD5($password));
		if($user_id != '' AND $password != '')
		{	
        $this->db->where('user_id', $user_id);
        $this->db->where('user_passwd', MD5($password));
        $query = $this->db->from('sys_users')->get();
		$num_results = $query->num_rows();
				
		if ($num_results  > 0)
		{	
		$select = 'id_prov, id_kabkot, nama_kabkot';
		$sort = '';				
		$where = '';
		$dt = $this->access->readtable('all_int_kabkot',$select,'','','','','')->result_array();			
		$this->response($dt, 200);
		}
		else {
		$this->response(array('status' => 'failed', 502));
		}
		}
	}
	function Kec_post(){
		$param = $this->input->post();
		$user_id = isset($param['user_id']) ? $param['user_id'] : '';
		$password = isset($param['password']) ? $param['password'] : '';
		
		//error_log(MD5($password));
		if($user_id != '' AND $password != '')
		{	
        $this->db->where('user_id', $user_id);
        $this->db->where('user_passwd', MD5($password));
        $query = $this->db->from('sys_users')->get();
		$num_results = $query->num_rows();
				
		if ($num_results  > 0)
		{	
		$select = '*';
		$sort = '';				
		$where = '';
		$dt = $this->access->readtable('all_int_kecamatan',$select,'','','','','')->result_array();			
		$this->response($dt, 200);
		}
		else {
		$this->response(array('status' => 'failed', 502));
		}
		}
	}
	function Kel_post(){
		$param = $this->input->post();
		$user_id = isset($param['user_id']) ? $param['user_id'] : '';
		$password = isset($param['password']) ? $param['password'] : '';
		//error_log(MD5($password));
		if($user_id != '' AND $password != '')
		{	
        $this->db->where('user_id', $user_id);
        $this->db->where('user_passwd', MD5($password));
        $query = $this->db->from('sys_users')->get();
		$num_results = $query->num_rows();
				
		if ($num_results  > 0)
		{	
		$select = '*';
		$sort = '';				
		$where = '';
		$dt = $this->access->readtable('all_int_kelurahan',$select,'','','','','')->result_array();			
		$this->response($dt, 200);
		}
		else {
		$this->response(array('status' => 'failed', 502));
		}
		}
	}
    
	public function data2_post(){
		//$save = 0;
		$param = $this->input->post();
		$name = isset($param['name']) ? $param['name'] : '';		
		//$this->response(array('status' => 'oke', 200));
		$this->response($name, 200);        
	}	
	

}
