<?php
//1. Nomor Card Reader = sam_csn		2.Nomor Kartu Produsen = terminal_sn  	Nomor Kartu SAM = sam_cn		Nomor PCID = pcid_number
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Parse extends REST_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();       
		$this->load->model('Access','access',true);			
    }
    function Req_post(){
		$param = $this->input->post();
		$user_id = isset($param['user_id']) ? $param['user_id'] : '';
		$password = isset($param['password']) ? $param['password'] : '';		
		if($user_id != '' AND $password != '')
		{	
	    $this->db->where('user_id', $user_id);
        $this->db->where('user_passwd', MD5($password));
        $query = $this->db->from('sys_users')->get(); 	
		$num = $query->num_rows();
		//error_log($num);			
		if ($num  > 0)
		{	
		$select = 'order_request_detail.id as id_tr,order_request_detail.orid as orid,order_request_detail.distributor_id as id_produsen,order_request.order_code, order_request.order_date, order_request.consumer_id as id_pengguna,
		order_request.pks_number,order_request.pks_date,order_request.qty,(SELECT company.name FROM company where order_request.consumer_id = company.id ORDER BY id DESC LIMIT 1) as name_pengguna,order_request_detail.terminal_sn as no_cardreader,order_request_detail.sam_csn as no_kartu_produsen,order_request_detail.location_name,(SELECT company.name FROM company where order_request_detail.distributor_id = company.id ORDER BY id DESC LIMIT 1) as name_produsen,trim(order_request_detail.sam_cn) as nomor_kartu_sam,order_request_detail.pcid_number,order_request_detail.location_address as alamat_penempatan,order_request_detail.perso_status as perso_status,order_request_detail.perso_date tgl_perso,order_request_detail.activation_date as activation_date,order_request_detail.validation_status as activation_status,order_request_detail.activation_request_date
';
		$sort = '';				
		$where = array('created_by'=>$user_id);
		$dt = $this->access->readtable('order_request',$select,$where,array('order_request_detail' =>'order_request_detail.orid = order_request.id'),'','','left')->result_array();		
		$sql = $this->db->last_query();
		//error_log($sql);
		$this->response($dt, 200);
		}
		else {
		$this->response(array('status' => 'failed', 502));
		}
		}
	}
	function Prop_post(){
		$param = $this->input->post();
		$user_id = isset($param['user_id']) ? $param['user_id'] : '';
		$password = isset($param['password']) ? $param['password'] : '';
		//error_log(MD5($password));
		if($user_id != '' AND $password != '')
		{	
        $this->db->where('user_id', $user_id);
        $this->db->where('user_passwd', MD5($password));
        $query = $this->db->from('sys_users')->get();
		$num_results = $query->num_rows();		
		if ($num_results  > 0)
		{	
		$select = 'id_prov, nama_prov';
		$sort = '';				
		$where = '';
		$dt = $this->access->readtable('all_int_provinsi',$select,'','','','','')->result_array();			
		$this->response($dt, 200);
		}
		else {
		$this->response(array('status' => 'failed', 502));
		}
		}
	}	
	function Kab_post(){
		$param = $this->input->post();
		$user_id = isset($param['user_id']) ? $param['user_id'] : '';
		$password = isset($param['password']) ? $param['password'] : '';
		//error_log(MD5($password));
		if($user_id != '' AND $password != '')
		{	
        $this->db->where('user_id', $user_id);
        $this->db->where('user_passwd', MD5($password));
        $query = $this->db->from('sys_users')->get();
		$num_results = $query->num_rows();
				
		if ($num_results  > 0)
		{	
		$select = 'id_prov, id_kabkot, nama_kabkot';
		$sort = '';				
		$where = '';
		$dt = $this->access->readtable('all_int_kabkot',$select,'','','','','')->result_array();			
		$this->response($dt, 200);
		}
		else {
		$this->response(array('status' => 'failed', 502));
		}
		}
	}
	function Kec_post(){
		$param = $this->input->post();
		$user_id = isset($param['user_id']) ? $param['user_id'] : '';
		$password = isset($param['password']) ? $param['password'] : '';
		//error_log(MD5($password));
		if($user_id != '' AND $password != '')
		{	
        $this->db->where('user_id', $user_id);
        $this->db->where('user_passwd', MD5($password));
        $query = $this->db->from('sys_users')->get();
		$num_results = $query->num_rows();
				
		if ($num_results  > 0)
		{	
		$select = '*';
		$sort = '';				
		$where = '';
		$dt = $this->access->readtable('all_int_kecamatan',$select,'','','','','')->result_array();			
		$this->response($dt, 200);
		}
		else {
		$this->response(array('status' => 'failed', 502));
		}
		}
	}
	function Kel_post(){
		$param = $this->input->post();
		$user_id = isset($param['user_id']) ? $param['user_id'] : '';
		$password = isset($param['password']) ? $param['password'] : '';
		//error_log(MD5($password));
		if($user_id != '' AND $password != '')
		{	
        $this->db->where('user_id', $user_id);
        $this->db->where('user_passwd', MD5($password));
        $query = $this->db->from('sys_users')->get();
		$num_results = $query->num_rows();
				
		if ($num_results  > 0)
		{	
		$select = '*';
		$sort = '';				
		$where = '';
		$dt = $this->access->readtable('all_int_kelurahan',$select,'','','','','')->result_array();			
		$this->response($dt, 200);
		}
		else {
		$this->response(array('status' => 'failed', 502));
		}
		}
	}
    public function Act_post()
	{	
		$param = $this->input->post();
		$sam_cn = isset($param['sam_cn']) ? $param['sam_cn'] : '';
		$activation_date = isset($param['activation_date']) ? $param['activation_date'] : '';
		$activation_request_date = isset($param['activation_request_date']) ? $param['activation_request_date'] : '';	
		$user_id = isset($param['user_id']) ? $param['user_id'] : '';
		$password = isset($param['password']) ? $param['password'] : '';
		$activation_request_date = isset($param['activation_request_date']) ? $param['activation_request_date'] : '';
		$id_produsen = isset($param['id_pengguna']) ? $param['id_pengguna'] : '';
		$activation_number = isset($param['activation_number']) ? $param['activation_number'] : '';
		$reply_activation_number = isset($param['reply_activation_number']) ? $param['reply_activation_number'] : '';
		$data = $this->access->readtable('order_request_detail','',array('sam_cn'=>$sam_cn),'','','')->row();
		$id_tr = $data->id;
		$orid = $data->orid;				
		$terminal_sn = $data->terminal_sn;
		$sam_csn = $data->sam_csn;						
		$pcid_number = $data->pcid_number;					
		$location_address = $data->location_address;
		$host = $_SERVER['REMOTE_ADDR'];	
        // error_log();
		if ($user_id == ''  AND $password == '' AND  $sam_cn == '' AND  $activation_date == '' AND  $activation_request_date AND $reply_activation_number == '' AND $activation_number ='')
		{
			 $this->response(array('status' => 'fail kosong', 502));
		} else {         		
				//$this->db->select('*');
				//$this->db->from('sys_users');
				$this->db->where('user_id', $user_id);
				$this->db->where('user_passwd', MD5($password));
				$query = $this->db->from('sys_users')->get();
				$num_results = $query->num_rows();				
				if ($num_results  > 0)
				{	
				 $data = array('orid'  => $orid,
						'ordid' => $id_tr,
						'activation_number'  => $activation_number,
						'reply_activation_number'  => $reply_activation_number,
						'sam_csn'    => $sam_csn,
						'terminal_sn'    => $terminal_sn,
						'sam_cn'    => $sam_cn,
						'pcid_number'  => $pcid_number,
						'location_address'    => $location_address,
						'activation_date'    => $activation_date,					
						'created_by'    => $user_id,	
                        'host'    => $host,						
						'activation_request_date' => $activation_request_date
						);	
                    $data2 = array('orid'  => $orid,
						//'orid' => $ordid,						
						'sam_csn'    => $sam_csn,
						'terminal_sn'    => $terminal_sn,
						'sam_cn'    => $sam_cn,
						'pcid_number'  => $pcid_number,
						'created_by'    => $user_id,	
                        'host'    => $host					
						);
					$data3 = array('validation_status'  => 't',
						'activation_date' => $activation_date,						
						'activation_request_date'  => $activation_request_date,
						'activation_number'    => $activation_number,
						'reply_activation_number'    => $reply_activation_number										
						);			 
				
		         $insert = $this->access->updatetable('order_request_detail', $data3, array('sam_cn' => $sam_cn));				
				   if ($insert) {
					    $insert = $this->db->insert('terminal_sam', $data2);
					    $insert = $this->db->insert('sam_activation', $data);
						$this->response(array('status' => 'ok', 200));
					} else {
						$this->response(array('status' => 'fail', 502));
					}
				 
                // $this->response($data2, 200);				 
				}
				else
				{ 
				 $this->response(array('status' => 'failed', 502));
				}
			}	
	}	
	public function data_post()
	{	
		$param = $this->input->post();
		$id_tr = isset($param['id_tr']) ? $param['id_tr'] : '';
		$activation_date = isset($param['activation_date']) ? $param['activation_date'] : '';
		$activation_request_date = isset($param['activation_request_date']) ? $param['activation_request_date'] : '';	
		$user_id = isset($param['user_id']) ? $param['user_id'] : '';
		$password = isset($param['password']) ? $param['password'] : '';
		$activation_request_date = isset($param['activation_request_date']) ? $param['activation_request_date'] : '';
		$id_produsen = isset($param['id_pengguna']) ? $param['id_pengguna'] : '';
		$activation_number = isset($param['activation_number']) ? $param['activation_number'] : '';
		$reply_activation_number = isset($param['reply_activation_number']) ? $param['reply_activation_number'] : '';
		$data = $this->access->readtable('order_request_detail','',array('id'=>$id_tr),'','','')->row();
		$sam_cn = $data->sam_cn;
		$orid = $data->orid;				
		$terminal_sn = $data->terminal_sn;
		$sam_csn = $data->sam_csn;						
		$pcid_number = $data->pcid_number;					
		$location_address = $data->location_address;
		$host = $_SERVER['REMOTE_ADDR'];	
        // error_log($orid);
		if ($user_id == ''  AND $password == '' AND  $id_tr == '' AND  $activation_date == '' AND  $activation_request_date AND $reply_activation_number == '' AND $activation_number ='')
		{
			 $this->response(array('status' => 'fail kosong', 502));
		} else {         		
				;
				$this->db->where('user_id', $user_id);
				$this->db->where('user_passwd', MD5($password));
				$query = $this->db->from('sys_users')->get();
				$num_results = $query->num_rows();				
				if ($num_results  > 0)
				{	
				 $data = array('orid'  => $orid,
						'ordid' => $id_tr,
						'activation_number'  => $activation_number,
						'reply_activation_number'  => $reply_activation_number,
						'sam_csn'    => $sam_csn,
						'terminal_sn'    => $terminal_sn,
						'sam_cn'    => $sam_cn,
						'pcid_number'  => $pcid_number,
						'location_address'    => $location_address,
						'activation_date'    => $activation_date,					
						'created_by'    => $user_id,	
                        'host'    => $host,						
						'activation_request_date'    => $activation_request_date
						);	
                    $data2 = array('orid'  => $orid,
						//'orid' => $orid,						
						'sam_csn'    => $sam_csn,
						'terminal_sn'    => $terminal_sn,
						'sam_cn'    => $sam_cn,
						'pcid_number'  => $pcid_number,
						'created_by'    => $user_id,	
                        'host'    => $host					
						);
					$data3 = array('validation_status'  => 't',
						'activation_date' => $activation_date,						
						'activation_request_date'  => $activation_request_date,
						'activation_number'    => $activation_number,
						'reply_activation_number'    => $reply_activation_number										
						);
				 $insert = $this->db->insert('sam_activation', $data); 
				// $sql = $this->db->last_query();
		         $insert = $this->access->updatetable('order_request_detail', $data3, array('id' => $id_tr));
				 $insert = $this->db->insert('terminal_sam', $data2);
				// $this->response(array('status' => 'ok', 200));
				   if ($insert) {
						$this->response(array('status' => 'ok', 200));
					} else {
						$this->response(array('status' => 'fail', 502));
					}
				 
                // $this->response($data2, 200);				 
				}
				else
				{ 
				 $this->response(array('status' => 'failed', 502));
				}
			}	
	}
	
	public function sam_post()
	{	
		$param = $this->input->post();
		$sam_csn = isset($param['sam_csn']) ? $param['sam_csn'] : '';
		$activation_date = isset($param['activation_date']) ? $param['activation_date'] : '';
		$activation_request_date = isset($param['activation_request_date']) ? $param['activation_request_date'] : '';	
		$user_id = isset($param['user_id']) ? $param['user_id'] : '';
		$password = isset($param['password']) ? $param['password'] : '';
		$activation_request_date = isset($param['activation_request_date']) ? $param['activation_request_date'] : '';
		$id_produsen = isset($param['id_pengguna']) ? $param['id_pengguna'] : '';
		$activation_number = isset($param['activation_number']) ? $param['activation_number'] : '';
		$reply_activation_number = isset($param['reply_activation_number']) ? $param['reply_activation_number'] : '';
		$data = $this->access->readtable('order_request_detail','',array('sam_csn'=>$sam_csn),'','','')->row();
		$id_tr = $data->id;
		$orid = $data->orid;				
		$terminal_sn = $data->terminal_sn;
		$sam_cn = $data->sam_cn;						
		$pcid_number = $data->pcid_number;					
		$location_address = $data->location_address;
		$host = $_SERVER['REMOTE_ADDR'];	
        // error_log();
		if ($user_id == ''  AND $password == '' AND  $sam_csn == '' AND  $activation_date == '' AND  $activation_request_date AND $reply_activation_number == '' AND $activation_number ='')
		{
			 $this->response(array('status' => 'fail kosong', 502));
		} else {         		
				$this->db->where('user_id', $user_id);
				$this->db->where('user_passwd', MD5($password));
				$query = $this->db->from('sys_users')->get();
				$num_results = $query->num_rows();	
				
				if ($num_results  > 0)
				{	
				 $data = array('orid'  => $orid,
						'ordid' => $id_tr,
						'activation_number'  => $activation_number,
						'reply_activation_number'  => $reply_activation_number,
						'sam_csn'    => $sam_csn,
						'terminal_sn'    => $terminal_sn,
						'sam_cn'    => $sam_cn,
						'pcid_number'  => $pcid_number,
						'location_address'    => $location_address,
						'activation_date'    => $activation_date,					
						'created_by'    => $user_id,	
                        'host'    => $host,						
						'activation_request_date'    => $activation_request_date
						);	
                    $data2 = array('orid'  => $orid,
						//'orid' => $ordid,						
						'sam_csn'    => $sam_csn,
						'terminal_sn'    => $terminal_sn,
						'sam_cn'    => $sam_cn,
						'pcid_number'  => $pcid_number,
						'created_by'    => $user_id,	
                        'host'    => $host					
						);
					$data3 = array('validation_status'  => 't',
						'activation_date' => $activation_date,						
						'activation_request_date'  => $activation_request_date,
						'activation_number'    => $activation_number,
						'reply_activation_number'    => $reply_activation_number										
						);
				 
				// $sql = $this->db->last_query();
		         $insert = $this->access->updatetable('order_request_detail', $data3, array('sam_cn' => $sam_cn));				
				// $this->response(array('status' => 'ok', 200));
				   if ($insert) {
					    $insert = $this->db->insert('sam_activation', $data); 
				        $insert = $this->db->insert('terminal_sam', $data2);
						$this->response(array('status' => 'ok', 200));
					} else {
						$this->response(array('status' => 'fail', 502));
					}
				 
                // $this->response($data2, 200);				 
				}
				else
				{ 
				 $this->response(array('status' => 'failed', 502));
				}
			}	
	}	
	
	public function inti_post()
	{	
		$param = $this->input->post();
		$sam_csn = isset($param['sam_csn']) ? $param['sam_csn'] : '';
		$activation_date = isset($param['activation_date']) ? $param['activation_date'] : '';
		$prop = isset($param['prop']) ? $param['prop'] : '';
		$kab = isset($param['kab']) ? $param['kab'] : '';
		$kec = isset($param['kec']) ? $param['kec'] : '';
		$kel = isset($param['kel']) ? $param['kel'] : '';
		$activation_request_date = isset($param['activation_request_date']) ? $param['activation_request_date'] : '';	
		$user_id = isset($param['user_id']) ? $param['user_id'] : '';
		$password = isset($param['password']) ? $param['password'] : '';
		$activation_request_date = isset($param['activation_request_date']) ? $param['activation_request_date'] : '';
		$id_produsen = isset($param['id_pengguna']) ? $param['id_pengguna'] : '';
		$activation_number = isset($param['activation_number']) ? $param['activation_number'] : '';
		$reply_activation_number = isset($param['reply_activation_number']) ? $param['reply_activation_number'] : '';
		$data = $this->access->readtable('order_request_detail','',array('sam_csn'=>$sam_csn),'','','')->row();
		$id_tr = $data->id;
		$orid = $data->orid;				
		$terminal_sn = $data->terminal_sn;
		$sam_cn = $data->sam_cn;						
		$pcid_number = $data->pcid_number;					
		$location_address = $data->location_address;
		$host = $_SERVER['REMOTE_ADDR'];	
        // error_log();
		if ($user_id == ''  AND $password == '' AND  $sam_csn == '' AND  $activation_date == '' AND  $activation_request_date AND $reply_activation_number == '' AND $activation_number ='' AND $prop ='' AND $kab ='' AND $kec ='' AND $kel ='')
		{
			 $this->response(array('status' => 'fail kosong', 502));
		} else {         		
				$this->db->where('user_id', $user_id);
				$this->db->where('user_passwd', MD5($password));
				$query = $this->db->from('sys_users')->get();
				$num_results = $query->num_rows();					
				if ($num_results  > 0)
				{	
					 $data = array('orid'  => $orid,
						'ordid' => $id_tr,
						'activation_number'  => $activation_number,
						'reply_activation_number'  => $reply_activation_number,
						'sam_csn'    => $sam_csn,						
						'terminal_sn'    => $terminal_sn,
						'sam_cn'    => $sam_cn,
						'pcid_number'  => $pcid_number,
						'location_address'    => $location_address,
						'activation_date'    => $activation_date,					
						'created_by'    => $user_id,	
                        'host'    => $host,						
						'activation_request_date'    => $activation_request_date
						);	
                    $data2 = array('orid'  => $orid,
						//'orid' => $ordid,						
						'sam_csn'    => $sam_csn,
						'terminal_sn'    => $terminal_sn,
						'sam_cn'    => $sam_cn,
						'pcid_number'  => $pcid_number,
						'created_by'    => $user_id,	
                        'host'    => $host					
						);
					$data3 = array('validation_status'  => 't',
						'activation_date' => $activation_date,
                        'province_id'    => $prop,
                        'city_id'    => $kab,
						'district_id'    => $kec,
						'village_id'    => $kel,						
						'activation_request_date'  => $activation_request_date,
						'activation_number'    => $activation_number,
						'reply_activation_number'    => $reply_activation_number										
						);
				 
				// $sql = $this->db->last_query();
		         $insert = $this->access->updatetable('order_request_detail', $data3, array('sam_cn' => $sam_cn));				
				// $this->response(array('status' => 'ok', 200));
				   if ($insert) {
					    $insert = $this->db->insert('sam_activation', $data); 
				        $insert = $this->db->insert('terminal_sam', $data2);
						$this->response(array('status' => 'ok', 200));
					} else {
						$this->response(array('status' => 'fail', 502));
					}
				 
                // $this->response($data2, 200);				 
				}
				else
				{ 
				 $this->response(array('status' => 'failed', 502));
				}
			}	
	}
	
	public function sam2_post()
	{	
		$param = $this->input->post();
		$id_tr = isset($param['id_tr']) ? $param['id_tr'] : '';
		$activation_date = isset($param['activation_date']) ? $param['activation_date'] : '';
		$prop = isset($param['prop']) ? $param['prop'] : '';
		$kab = isset($param['kab']) ? $param['kab'] : '';
		$kec = isset($param['kec']) ? $param['kec'] : '';
		$kel = isset($param['kel']) ? $param['kel'] : '';
		$ncard = isset($param['ncard']) ? $param['ncard'] : '';		
		$cprodusen = isset($param['cprodusen']) ? $param['cprodusen'] : '';
		$activation_request_date = isset($param['activation_request_date']) ? $param['activation_request_date'] : '';	
		$user_id = isset($param['user_id']) ? $param['user_id'] : '';
		$password = isset($param['password']) ? $param['password'] : '';
		$activation_request_date = isset($param['activation_request_date']) ? $param['activation_request_date'] : '';
		$id_produsen = isset($param['id_pengguna']) ? $param['id_pengguna'] : '';
		$activation_number = isset($param['activation_number']) ? $param['activation_number'] : '';
		$reply_activation_number = isset($param['reply_activation_number']) ? $param['reply_activation_number'] : '';		
		$data = $this->access->readtable('order_request_detail','',array('id'=>$id_tr),'','','')->row();		
		$orid = $data->orid;				
		$terminal_sn = $data->terminal_sn;
		$sam_cn = $data->sam_cn;						
		$pcid_number = $data->pcid_number;					
		$location_address = $data->location_address;
		$host = $_SERVER['REMOTE_ADDR'];	
        // error_log($orid);
		if ($user_id == ''  AND $password == '' AND  $id_tr == '' AND  $activation_date == '' AND  $activation_request_date AND $reply_activation_number == '' AND $activation_number ='')
		{
			 $this->response(array('status' => 'fail kosong', 502));
		} else {         		
				$this->db->where('user_id', $user_id);
				$this->db->where('user_passwd', MD5($password));
				$query = $this->db->from('sys_users')->get();
				$num_results = $query->num_rows();	
				
				if ($num_results  > 0)
				{	
				 $data = array('orid'  => $orid,
						'ordid' => $id_tr,
						'activation_number'  => $activation_number,
						'reply_activation_number'  => $reply_activation_number,
						'sam_csn'    => $sam_csn,
						'terminal_sn'    => $terminal_sn,						
						'sam_cn'    => $ncard,
						'terminal_cn'    => $cprodusen,
						'pcid_number'  => $pcid_number,
						'location_address'    => $location_address,
						'activation_date'    => $activation_date,					
						'created_by'    => $user_id,	
                        'host'    => $host,						
						'activation_request_date'    => $activation_request_date
						);	
                    $data2 = array('orid'  => $orid,
						//'orid' => $orid,						
						'sam_csn'    => $sam_csn,
						'terminal_sn'    => $terminal_sn,
						'sam_cn'    => $sam_cn,
						'pcid_number'  => $pcid_number,
						'created_by'    => $user_id,	
                        'host'    => $host					
						);
					$data3 = array('validation_status'  => 't',
						'activation_date' => $activation_date,
                        'province_id'    => $prop,
						'city_id'    => $kab,
						'district_id'    => $kec,
						'village_id'    => $kel,						
						'activation_request_date'  => $activation_request_date,
						'activation_number'    => $activation_number,
						'reply_activation_number'    => $reply_activation_number										
						);
				 $insert = $this->db->insert('sam_activation', $data); 
				// $sql = $this->db->last_query();
		         $insert = $this->access->updatetable('order_request_detail', $data3, array('id' => $id_tr));
				 $insert = $this->db->insert('terminal_sam', $data2);
				// $this->response(array('status' => 'ok', 200));
				   if ($insert) {
						$this->response(array('status' => 'ok', 200));
					} else {
						$this->response(array('status' => 'fail', 502));
					}
				 
                // $this->response($data2, 200);				 
				}
				else
				{ 
				 $this->response(array('status' => 'failed', 502));
				}
			}	
	}
	public function data2_post(){
		//$save = 0;
		$param = $this->input->post();
		$name = isset($param['name']) ? $param['name'] : '';		
		$this->response(array('status' => 'oke', 200));
		$this->response($name, 200);        
	}	
	

}
