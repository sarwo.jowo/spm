<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class User extends REST_Controller {
    function __construct($config = 'rest') {
        parent::__construct($config);
    }
     function index_get() {
        $user = $this->get('u_name');
        if ($user == '') {
            $user = $this->db->get('sys_users')->result();
        } else {
            //$this->db->where('u_name', $user);
            $user = $this->db->get('sys_users')->result();
        }
        $this->response($user, 200);
    }
    function index_post() {
        $data = array(
                    'u_name'     => $this->post('u_name'),
                    'pass_word'  => md5($this->post('pass_word')),
                    'hp'    => $this->post('hp'),
                    'alamat'  => $this->post('alamat'),
                    'kota'    => $this->post('kota'),
                    );
        $insert = $this->db->insert('tbl_user', $data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
     function index_put() {
        $user = $this->put('user');
        $data = array(
                    'email'       => $this->put('email'),
                    'hp'      => $this->put('hp'),                 
                    'alamat'    => $this->put('alamat'));
        $this->db->where('u_name', $user);
        $update = $this->db->update('tbl_user', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }



}