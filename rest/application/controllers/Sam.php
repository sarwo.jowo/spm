<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Parse extends REST_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();       
		$this->load->model('Access','access',true);			
    }
    function Req_post(){
		$param = $this->input->post();
		$user_id = isset($param['user_id']) ? $param['user_id'] : '';
		$password = isset($param['password']) ? $param['password'] : '';
		//error_log(MD5($password));
		if($user_id != '' AND $password != '')
		{	
  		$array = array('user_id' => $user_id, 'user_passwd' => MD5($password));
        $this->db->get_where('sys_users',$array);
		//$sql = $this->db->last_query();
		//error_log($sql);
		$num_results = $this->db->count_all_results();
				
		if ($num_results  > 0)
		{	
		$select = 'order_request_detail.id as id_tr,order_request_detail.orid as orid,order_request_detail.distributor_id as id_produsen,order_request.order_code, order_request.order_date, order_request.consumer_id as id_pengguna,
		order_request.pks_number,order_request.pks_date,order_request.qty,(SELECT company.name FROM company where order_request.consumer_id = company.id ORDER BY id DESC LIMIT 1) as name_pengguna,order_request_detail.terminal_sn as no_cardreader,order_request_detail.sam_csn as no_kartu_produsen,order_request_detail.location_name,(SELECT company.name FROM company where order_request_detail.distributor_id = company.id ORDER BY id DESC LIMIT 1) as name_produsen,order_request_detail.sam_cn as nomor_kartu_sam,order_request_detail.pcid_number,order_request_detail.location_address as alamat_penempatan,order_request_detail.perso_status as perso_status,order_request_detail.perso_date tgl_perso,order_request_detail.activation_date as activation_date,order_request_detail.validation_status as activation_status,order_request_detail.activation_request_date
';
		$sort = '';				
		$where = array('created_by'=>$user_id);
		$dt = $this->access->readtable('order_request',$select,$where,array('order_request_detail' =>'order_request_detail.orid = order_request.id'),'','','left')->result_array();		
		$sql = $this->db->last_query();
		//error_log($sql);
		$this->response($dt, 200);
		}
		else {
		$this->response(array('status' => 'failed', 502));
		}
		}
	}
    public function Act_post()
	{	
		$param = $this->input->post();
		$sam_cn = isset($param['sam_cn']) ? $param['sam_cn'] : '';
		$activation_date = isset($param['activation_date']) ? $param['activation_date'] : '';
		$activation_request_date = isset($param['activation_request_date']) ? $param['activation_request_date'] : '';	
		$user_id = isset($param['user_id']) ? $param['user_id'] : '';
		$password = isset($param['password']) ? $param['password'] : '';
		$activation_request_date = isset($param['activation_request_date']) ? $param['activation_request_date'] : '';
		$id_produsen = isset($param['id_pengguna']) ? $param['id_pengguna'] : '';
		$activation_number = isset($param['activation_number']) ? $param['activation_number'] : '';
		$reply_activation_number = isset($param['reply_activation_number']) ? $param['reply_activation_number'] : '';
		$data = $this->access->readtable('order_request_detail','',array('sam_cn'=>$sam_cn),'','','')->row();
		$id_tr = $data->id;
		$orid = $data->distributor_id;				
		$terminal_sn = $data->terminal_sn;
		$sam_csn = $data->sam_csn;						
		$pcid_number = $data->pcid_number;					
		$location_address = $data->location_address;
		$host = $_SERVER['REMOTE_ADDR'];	
        // error_log();
		if ($user_id == ''  AND $password == '' AND  $sam_cn == '' AND  $activation_date == '' AND  $activation_request_date AND $reply_activation_number == '' AND $activation_number ='')
		{
			 $this->response(array('status' => 'fail kosong', 502));
		} else {         		
				//$this->db->select('*');
				//$this->db->from('sys_users');
				$array = array('user_id' => $user_id, 'user_passwd' => MD5($password));
                $this->db->get_where('sys_users',$array);
				$num_results = $this->db->count_all_results();
				
				if ($num_results  > 0)
				{	
				 $data = array('orid'  => $orid,
						'ordid' => $id_tr,
						'activation_number'  => $activation_number,
						'reply_activation_number'  => $reply_activation_number,
						'sam_csn'    => $sam_csn,
						'terminal_sn'    => $terminal_sn,
						'sam_cn'    => $sam_cn,
						'pcid_number'  => $pcid_number,
						'location_address'    => $location_address,
						'activation_date'    => $activation_date,					
						'created_by'    => $user_id,	
                        'host'    => $host,						
						'activation_request_date'    => $activation_request_date
						);	
                    $data2 = array('orid'  => $id_tr,
						'orid' => $ordid,						
						'sam_csn'    => $sam_csn,
						'terminal_sn'    => $terminal_sn,
						'sam_cn'    => $sam_cn,
						'pcid_number'  => $pcid_number,
						'created_by'    => $user_id,	
                        'host'    => $host					
						);
					$data3 = array('validation_status'  => 't',
						'activation_date' => $activation_date,						
						'activation_request_date'  => $activation_request_date,
						'activation_number'    => $activation_number,
						'reply_activation_number'    => $reply_activation_number										
						);
				 $insert = $this->db->insert('sam_activation', $data); 
				// $sql = $this->db->last_query();
		         $insert = $this->access->updatetable('order_request_detail', $data3, array('sam_cn' => $sam_cn));
				 $insert = $this->db->insert('terminal_sam', $data2);
				// $this->response(array('status' => 'ok', 200));
				   if ($insert) {
						$this->response(array('status' => 'ok', 200));
					} else {
						$this->response(array('status' => 'fail', 502));
					}
				 
                // $this->response($data2, 200);				 
				}
				else
				{ 
				 $this->response(array('status' => 'failed', 502));
				}
			}	
	}	
	
		
	public function data_post(){
		//$save = 0;
		$param = $this->input->post();
		$name = isset($param['name']) ? $param['name'] : '';		
		//$this->response(array('status' => 'oke', 200));
		$this->response($name, 200);        
	}	
	

}
