<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Access extends CI_Model {
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();	
    }
    function readtable($tablename = '', $select='', $where = '', $join = '', $limit = '',$sort = '',$join_model = '',$group_by ='')
    {         
        if(!empty($where)){
    		$this->db->where($where);
        }
        if(!empty($group_by)){
            $this->db->group_by($group_by);
        }
        if(is_array($join) AND !empty($join)){
            foreach($join as $key => $data){  
                $this->db->join($key, $data,$join_model);
            }
        }
        if(is_array($limit) AND !empty($limit)){ 
            $this->db->limit($limit[0], $limit[1]);
        }
        if(!empty($select)){
            $this->db->select($select);
        }  
		if(is_array($sort) AND !empty($sort)){
            foreach($sort as $keys => $datas){  
                $this->db->order_by($keys, $datas);
            }
        }
        $query = $this->db->get($tablename);
		
        return $query;
    }
    function inserttable($tablename = '', $data = '')
    {
	  // error_log('insert'.$this->db->last_query());
       $this->db->insert($tablename, $data); 
	   
	   return $this->db->insert_id();
	   
    }
    function updatetable($tablename = '', $data = '', $where='')
    {    
        $this->db->where($where);
        return $this->db->update($tablename, $data);
    }
    function deletetable($tablename = '',$where='')
    {
        return $this->db->delete($tablename, $where); 
    }
	
	function readtable_or($tablename = '', $select='', $where = '', $where_or = '', $join = '', $limit = '',$sort = '',$join_model = '',$group_by ='')
    {         
        if(!empty($where)){
    		$this->db->where($where);
        }
		if(!empty($where_or)){
    		$this->db->or_where($where_or);
        }
        if(!empty($group_by)){
            $this->db->group_by($group_by);
        }
        if(is_array($join) AND !empty($join)){
            foreach($join as $key => $data){  
                $this->db->join($key, $data,$join_model);
            }
        }
        if(is_array($limit) AND !empty($limit)){ 
            $this->db->limit($limit[0], $limit[1]);
        }
        if(!empty($select)){
            $this->db->select($select);
        }  
		if(is_array($sort) AND !empty($sort)){
            foreach($sort as $keys => $datas){  
                $this->db->order_by($keys, $datas);
            }
        }
        $query = $this->db->get($tablename);
        return $query;
    }
	
}