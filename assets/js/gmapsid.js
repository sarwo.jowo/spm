
var Coord = [];

Coord[1] = {
    lt: 4.061535597066106,
    lg: 96.9873046875,
    lev: 7
};
Coord[2] = {
    lt: 2.1418349697685968,
    lg: 99.140625,
    lev: 7
};
Coord[3] = {
    lt: -1.2743089918452106,
    lg: 100.5908203125,
    lev: 7
};
Coord[4] = {
    lt: 0.5932511181408705,
    lg: 101.854248046875,
    lev: 7
};
Coord[5] = {
    lt: -1.7355743631421197,
    lg: 102.7001953125,
    lev: 7
};
Coord[6] = {
    lt: -3.359889094873377,
    lg: 104.1064453125,
    lev: 7
};
Coord[7] = {
    lt: -3.403757879577576,
    lg: 102.2607421875,
    lev: 7
};

Coord[8] = {
    lt: -4.937724274302479,
    lg: 104.8974609375,
    lev: 8
};

Coord[9] = {
    lt: -2.3449261578261145,
    lg: 106.776123046875,
    lev: 8
};
Coord[10] = {
    lt: 0.4724067568443019,
    lg: 104.5294189453125,
    lev: 8
};
Coord[11] = {
    lt: -6.21359928052261,
    lg: 106.85508728027344,
    lev: 10
};
Coord[12] = {
    lt: -6.893707270014225,
    lg: 107.545166015625,
    lev: 8
};
Coord[13] = {
    lt: -7.318881730366743,
    lg: 109.75341796875,
    lev: 8
};
Coord[14] = {
    lt: -7.92323411294785,
    lg: 110.41259765625,
    lev: 9
};




Coord[15] = {
    lt: -7.836173352103856,
    lg: 112.642822265625,
    lev: 8
};
Coord[16] = {
    lt: -6.620957270326322,
    lg: 106.072998046875,
    lev: 8
};
Coord[17] = {
    lt: -8.407168163601074,
    lg: 115.07080078125,
    lev: 9
};


Coord[18] = {
    lt: -8.68420891954859,
    lg: 117.5372314453125,
    lev: 7
};

Coord[19] = {
    lt: -9.25393615681445,
    lg: 122.156982421875,
    lev: 7
};
Coord[20] = {
    lt: -0.4394488164139641,
    lg: 111.07177734375,
    lev: 7
};
Coord[21] = {
    lt: -1.6806671337507222,
    lg: 113.44482421875,
    lev: 7
};
Coord[22] = {
    lt: -3.019841106168974,
    lg: 115.477294921875,
    lev: 8
};
Coord[23] = {
    lt: 0.1374308,
    lg: 116.5338974,
    lev: 7
};
Coord[24] = {
    lt: 2.5337023,
    lg: 116.5832555,
    lev: 7
};
Coord[25] = {
    lt: 0.8239462091017685,
    lg: 124.398193359375,
    lev: 8
};
Coord[26] = {
    lt: -0.7800052024755708,
    lg: 120.926513671875,
    lev: 7
};
Coord[27] = {
    lt: -3.5901776188373495,
    lg: 120.12451171875,
    lev: 7
};
Coord[28] = {
    lt: -4.0176994643368396,
    lg: 122.0361328125,
    lev: 7
};
Coord[29] = {
    lt: 0.7140928403610856,
    lg: 122.4481201171875,
    lev: 8
};
Coord[30] = {
    lt: -2.2516174965491453,
    lg: 119.366455078125,
    lev: 8
};
Coord[31] = {
    lt: -5.2502085616531686,
    lg: 128.56201171875,
    lev: 6
};


Coord[32] = {
    lt: -0.4833927027896987,
    lg: 126.4306640625,
    lev: 7
};



Coord[33] = {
    lt: -2.141834969768584,
    lg: 132.4072265625,
    lev: 7
};

Coord[34] = {
    lt: -4.36832042087623,
    lg: 138.1640625,
    lev: 6
};



//

createMapInit = function(div) {
    var base=$("#base_url").val();
    var map = new GMaps({
        div: div,
        lat: -2.5452630879999996,
        lng: 118.032525,
        zoom: 5,
        scrollwheel: false,
        panControl: false,
        scaleControl: false,
        zoomControl: false,
        mapTypeControl: false,
        streetViewControl: false
    });

    map.addControl({
        position: 'bottom_right',
        content: '<a id="zoomIn" href="javascript:void()"><img src="'+base+'images/toggle_plus.gif" /></a>',
        style: {
            margin: '5px',
            border: 'solid 1px #717B87',
            background: '#fff'
        },
        events: {
            click: function(){
                map.setZoom(parseInt(map.getZoom())+1);
            }
        }
    });

    map.addControl({
        position: 'bottom_right',
        content: '<a id="zoomOut" href="javascript:void()"><img src="'+base+'images/toggle_minus.gif" /></a>',
        style: {
            margin: '5px',
            border: 'solid 1px #717B87',
            background: '#fff'
        },
        events: {
            click: function(){
                map.setZoom(map.getZoom()-1);
            }
        }
    });

};

createMapJenisData = function(div, dataPolygon, color, hover_color, provFunc) {
    var map = new GMaps({
        div: div,
        lat:  1.5452630879999996,
        lng: 123.032525,
        zoom: 5,
        scrollwheel: true,
        panControl: false,
        scaleControl: false,
        zoomControl: false,
        mapTypeControl: false,
        streetViewControl: false
    });
    var base=$("#base_url").val();
    map.addControl({
        position: 'bottom_right',
        content: '<a id="zoomIn" href="javascript:void()"><img src="'+base+'images/toggle_plus.gif" /></a>',
        style: {
            margin: '5px',
            border: 'solid 1px #717B87',
            background: '#fff'
        },
        events: {
            click: function(){
                map.setZoom(parseInt(map.getZoom(), 10)+1);


            }
        }
    });

    map.addControl({
        position: 'bottom_right',
        content: '<a id="zoomOut" href="javascript:void()"><img src="'+base+'images/toggle_minus.gif" /></a>',
        style: {
            margin: '5px',
            border: 'solid 1px #717B87',
            background: '#fff'
        },
        events: {
            click: function(){
                map.setZoom(map.getZoom()-1);

            }
        }
    });

    console.log('sas');
         var base=$("#base_url").val();
         var urusan=$("#urusan").val();
         var controllers=$("#controllers").val();
         $.ajax({
                type:'POST',
                url: base+controllers+'/get_data/'+urusan,
                data:$("#form_filter").serialize(),
                success: function(datax) {
                    var datanya=  datax.split("|");
                    var urutan=  datax.split(",");
                    urutan.shift();
                        var i=0;
                        var j=0;
                        var colornya="#FF0000";
                        var ids = [], prov = [], poly = [];
                        for (var i = 0; i < dataPolygon.length; i++) {

                          console.log(dataPolygon[i]);
                          console.log('iiiii-----');

                            ids[i] = dataPolygon[i].properties["id_prov"];
                            prov[i] = dataPolygon[i].properties["name"];
                            j=(datanya.length);
                            for(j=0;j<datanya.length;j++){
                                if(ids[i]==urutan[j]){
                                console.log(datanya[j]);
                                    if(((datanya[j])=="0") ||((datanya[j])=="") ){
                                        colornya='#000';
                                    }  else if (((datanya[j])<=30) && ((datanya[j])>=1)){
                                        colornya='#FF0000';
                                    }  else if (((datanya[j])<=40) && ((datanya[j])>=31)){
                                        colornya='#cf6317';
                                    }  else if (((datanya[j])<=60) && ((datanya[j])>=41)){
                                        colornya='#2C4F9B';
                                    }  else if (((datanya[j])<=80) && ((datanya[j])>=61)){
                                        colornya='#FFFF00';
                                    }  else if((datanya[j])>80){
                                        colornya='#00FF00';
                                    }
                                    console.log(prov[i] +"-"+colornya);
                                }

                            }

                            poly[i] = map.drawPolygon({
                                index: i,
                                row: {
                                    id: ids[i],
                                    name: prov[i]
                                },
                                paths: dataPolygon[i].geometry.coordinates,
                                useGeoJSON: true,
                                strokeColor: '#dedede',
                                strokeOpacity: 1,
                                fillOpacity :0.9,
                                strokeWeight: 1,
                                fillColor:  colornya,
                                click: function (e) {
                                     confirmdlg(e.latLng, this.row.id,prov[i] );
                                  },
                                mouseover: function (e) {
                                  },
                                mouseout: function (e) {
                                   // this.setOptions({ fillColor: color });
                                }
                            });
                        }
                }
         });

};

createMapKabupaten = function(div, id, location) {
    var mapOptions = {
        zoom: Coord[id].lev,
        center: new google.maps.LatLng(Coord[id].lt, Coord[id].lg),
        mapTypeId: google.maps.MapTypeId.HYBRID,
        scrollwheel: false,
        panControl: false,
        scaleControl: false,
        zoomControl: false,
        mapTypeControl: false,
        streetViewControl: false,
    };

    var map = new google.maps.Map(document.getElementById(div), mapOptions);
    // console.log(location);

    $.ajax({
        url: location+'/map_'+id+'_data.json',
        success: function(json){

            for (var i = 0; i < json.length; i++) {

                var coord = json[i].coordinates;
                for (var j = 0; j < coord.length; j++) {
                    createPolygon(map, createFromCoordinates(coord[j]), json[i].properties);
                };
            };
        }
    });
};

createFromCoordinates = function(coord) {
    var pts = [];
    for (var i = 0; i < coord.length; i++) {
        pts.push(new google.maps.LatLng(coord[i].jb, coord[i].kb));
    }

    return pts;
}

createPolygon = function(map, points, prop) {
    var polygon = new google.maps.Polygon({
            map: map,
            paths: points,
            strokeColor: '#E7E7E7',
            strokeOpacity: 0.9,
            strokeWeight: 1,
            fillColor: '#99B3CC',
            fillOpacity: 0.35,
            property: prop
        }),
        theToolTip = new InfoBubble({
          map: map,
          disableAutoPan: true,
          hideCloseButton: true,
          arrowPosition: 30,
          arrowStyle: 2,
          arrowSize: 10,
          shadowStyle: 1,
          padding: 5,
           borderRadius: 4,
          borderWidth: 1,
          borderColor: '#2c2c2c'
        }),
         eHover = google.maps.event.addListener(polygon, 'mouseover', function(e){
            //this.setOptions({ fillColor: '#4E639C' });

            //create an options object
            var kabupaten = polygon.property.name;
            var theContent = '<div style="color:#fff;font-weight:bold">'+kabupaten+'</div>';

            // create the tooltip
            theToolTip.setContent( theContent );
            theToolTip.setPosition( e.latLng );
            theToolTip.open();
        }),
        eHout = google.maps.event.addListener(polygon, 'mouseout', function(e){
            //this.setOptions({ fillColor: '#99B3CC' });

            if ( theToolTip ) theToolTip.close();
        });

        // google.maps.event.removeListener( eHover );
        // google.maps.event.removeListener( eHout );
}
