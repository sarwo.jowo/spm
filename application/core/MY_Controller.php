<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{

	public $data = array();

	public function __construct() {

		parent::__construct();

	//	$this->output->enable_profiler(TRUE);

		// cek status login user
		if ($this->session->userdata('login') == FALSE) {
			redirect('login');
		} else {
			
			$this->data['u_name'] = $this->session->userdata('u_name');
			$this->data['id'] = $this->session->userdata('id');
			
			$this->data['isi'] = '';
			$this->data['judul_browser'] = '';
			$this->data['judul_utama'] = '';
			$this->data['judul_sub'] = '';
			$this->data['link_aktif'] = '';
			$this->data['css_files'] = array();
			$this->data['js_files'] = array();
			$this->data['js_files2'] = array();

			// notifikasi
			
			
			
			
			

		}
	}
}


class PinjamanController extends MY_Controller
{

	public function __construct() {
		parent::__construct();
		// cek status level admin
		if ($this->session->userdata('level') == 'admin' || $this->session->userdata('level') == 'pinjaman') {
			//oke
			$this->data['akses'] = TRUE;
		} else {
			// no
			$this->data['akses'] = FALSE;
			redirect('home/no_akses');
		}
	}   

}

class OPPController extends MY_Controller
{

	public function __construct() {
		parent::__construct();
		// cek status level admin
		if ($this->session->userdata('level') == 'admin' || $this->session->userdata('level') == 'pinjaman' || $this->session->userdata('level') == 'operator') {
			//oke
			$this->data['akses'] = TRUE;
		} else {
			// no
			$this->data['akses'] = FALSE;
			redirect('home/no_akses');
		}
	}   

}