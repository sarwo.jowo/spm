<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper('fungsi');
		$this->load->model('home_m');
		$this->load->model('Dasboard_m');		
		//$this->load->model('Calendar_model');
	}	
	
	public function index() {
	    $this->data['judul_browser'] = 'Beranda';
		$this->data['judul_utama'] = 'Beranda';
		$this->data['judul_sub'] = 'Menu Utama';
		
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';
		//$this->data['js_files'][] = base_url() . 'assets/easyui/datagrid-detailview.js';

		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';
		#include seach
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';
		$this->data['js_files2'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.js';		
		$this->data['css_files'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.css';
		$level = $this->session->userdata('level');
				
		$this->data['dt'] = $this->home_m->charts();
		$tgl = date('Y-m-d');				
		if ($level == "admin"){
		$this->data['isi'] = $this->load->view('dashboard/maindashboard', $this->data, TRUE);
		}
		else if($level=="ADMINPROV"){
		$this->data['isi'] = $this->load->view('dashboard/provdashboard', $this->data, TRUE);
		}
		else if ($level =="ADMINKAB"){
		$this->data['isi'] = $this->load->view('dashboard/kabdashboard', $this->data, TRUE);	
		}	
		else if ($level=="SUBDIT"){
		$this->data['isi'] = $this->load->view('dashboard/dinasdashboard', $this->data, TRUE);
		}
		else
		{
        $this->data['isi'] = $this->load->view('dashboard/penggunadashboard', $this->data, TRUE);
        }		
		
		
		$this->load->view('themes/layout_utama_v', $this->data);
			
		
	}

	public function dataperdinas(){
		$this->data['judul_browser'] = 'Beranda';
		$this->data['judul_utama'] = 'Beranda';
		$this->data['judul_sub'] = 'Menu Utama';
		
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';
		//$this->data['js_files'][] = base_url() . 'assets/easyui/datagrid-detailview.js';

		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';
		#include seach
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';
		$this->data['js_files2'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.js';		
		$this->data['css_files'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.css';
		$level = $this->session->userdata('level');
				
		$tgl = date('Y-m-d');				
		
		//load data here...
		//end
		$this->data['isi'] = $this->load->view('dashboard/dataperdinas', $this->data, TRUE);
		$this->load->view('themes/layout_utama_v', $this->data);
	}

	public function dataperdinasdetail(){
		$this->data['judul_browser'] = 'Beranda';
		$this->data['judul_utama'] = 'Beranda';
		$this->data['judul_sub'] = 'Menu Utama';
		
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';
		//$this->data['js_files'][] = base_url() . 'assets/easyui/datagrid-detailview.js';

		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';
		#include seach
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';
		$this->data['js_files2'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.js';		
		$this->data['css_files'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.css';
		$level = $this->session->userdata('level');
				
		$tgl = date('Y-m-d');				
		
		//load data here...
		//end
		$this->data['isi'] = $this->load->view('dashboard/dataperdinasdetail', $this->data, TRUE);
		$this->load->view('themes/layout_utama_v', $this->data);
	}
	
 
	function getDatachart1($tahun){
		# code...        
        
		
		$level = $this->session->userdata('level');
		if ($level == "admin"){
		echo $this->Dasboard_m->getDataforDashboarPie($tahun);
		}
		else if($level=="ADMINPROV"){
		echo $this->Dasboard_m->getDataforDashboarPie2($tahun);
		}
		else if ($level =="ADMINKAB"){
		echo $this->Dasboard_m->getDataforDashboarPie3($tahun);	
		}	
		else if ($level=="SUBDIT"){
		echo $this->Dasboard_m->getDataforDashboarPie($tahun);
		}
		else
		{
        echo $this->Dasboard_m->getDataforDashboarPie($tahun);
        }	
		
	}

	function getDatachart1Detail(){
		# code...    
        		
		$tahun= $this->input->get('tahun');
		$dinas=$this->input->get('dinas');
	      // error_log($dinas);		
		echo $this->Dasboard_m->getDataForDashboardPieDetail($tahun,$dinas);
	}
	
	
	function getDatachart2Detail(){
		# code...    
        		
		$tahun= $this->input->get('tahun');
		$dinas=$this->input->get('dinas');
		$prov=$this->input->get('prov');
	   // error_log($tahun);

		 //$id_prov = str_replace('%20',' ', $pr);
		 $id_prov = explode("-", $prov);
         $id_prov = $id_prov[1];
		// error_log($id_prov);
		
		echo $this->Dasboard_m->getDataForDashboardPieDetail2($tahun,$dinas,$id_prov);
		//print_r($demo);
	}
	
	function getDatachart2($tahun){
		# code...  
        //$tahun = "2019";		
        
		$level = $this->session->userdata('level');
		if ($level == "admin"){
		echo $this->Dasboard_m->getDataforDashboardColumn($tahun);
		}
		else if($level=="ADMINPROV"){
		echo $this->Dasboard_m->getDataforDashboardColumnprov($tahun);
		}
		else if ($level =="ADMINKAB"){
		echo $this->Dasboard_m->getDataforDashboardColumn($tahun);	
		}	
		else if ($level=="SUBDIT"){
		echo $this->Dasboard_m->getDataforDashboardColumn($tahun);
		}
		else
		{
        echo $this->Dasboard_m->getDataforDashboardColumn($tahun);
        }	
	}
	
	function getDatachart3($tahun){
		# code...        
        //echo $this->Dasboard_m->getDataforDashboardColumndemo($tahun);
		
		echo $this->Dasboard_m->getDataforDashboarPie2($tahun);
	}
	
	//Box 1
	function getJumlahProvinsiKab(){
		
		$level = $this->session->userdata('level');		
		if ($level == "admin"){
		echo $this->Dasboard_m->getJumlahProvinsiKab();
		}
		else if($level=="ADMINPROV"){
		echo $this->Dasboard_m->getJumlahProvinsiKab2();
		}
		else if ($level =="ADMINKAB"){
		echo $this->Dasboard_m->getJumlahProvinsiKab();
		}	
		else if ($level=="SUBDIT"){
		echo $this->Dasboard_m->getJumlahProvinsiKab();
		}
		else
		{
        echo $this->Dasboard_m->getJumlahProvinsiKab();
        }
			
	}
	
	function getJumlahAnggaran($tahun){
		
					
		$level = $this->session->userdata('level');		
		if ($level == "admin"){
		echo $this->Dasboard_m->getJmlAngg($tahun);
		}
		else
		{
        echo $this->Dasboard_m->getJmlAngg($tahun);
        }	
			
	}
	
	function getJumlahMasalah($tahun){
		
					
		$level = $this->session->userdata('level');		
		if ($level == "admin"){
		echo $this->Dasboard_m->getJmlMasalah($tahun);
		}
		else
		{
        echo $this->Dasboard_m->getJmlMasalah($tahun);
        }	
			
	}

	//Box 2
	function getTotalprovkabspm($tahun){
		
	//	error_log('111111111111');
		
		$level = $this->session->userdata('level');		
		if ($level == "admin"){
		echo $this->Dasboard_m->getTotalprovkabspm($tahun);
		}
		else if($level=="ADMINPROV"){
		echo $this->Dasboard_m->getTotalprovkabspm2($tahun);
		}
		else if ($level =="ADMINKAB"){
		echo $this->Dasboard_m->getTotalprovkabspm3($tahun);
		}	
		else if ($level=="SUBDIT"){
		echo $this->Dasboard_m->getTotalprovkabspm($tahun);
		}
		else
		{
        echo $this->Dasboard_m->getTotalprovkabspm($tahun);
        }	
	}


	//Box 3
	function getPersentasiSPM($tahun){
		
		$level = $this->session->userdata('level');		
		if ($level == "admin"){
		echo $this->Dasboard_m->getPersentasiSPM($tahun);
		}
		else if($level=="ADMINPROV"){
		echo $this->Dasboard_m->getPersentasiSPMprov($tahun);
		}
		else if ($level =="ADMINKAB"){
		echo $this->Dasboard_m->getPersentasiSPMkab($tahun);
		}	
		else if ($level=="SUBDIT"){
		echo $this->Dasboard_m->getPersentasiSPM($tahun);
		}
		else
		{
        echo $this->Dasboard_m->getPersentasiSPM($tahun);
        }	
		
		
		
	}

}
