<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Indikator extends AdminController {

	public function __construct() {
		parent::__construct();			
    }
    
    public function index() {
		$this->data['judul_browser'] = 'Data';
		$this->data['judul_utama'] = 'Indikator';
       
        $this->output->set_template('gc');
        
		$this->load->library('grocery_CRUD');
		$crud = new grocery_CRUD();
		$crud->set_table('tbl_master_indikator');
		$crud->set_subject('Master Data Indikator');
        	   
		$crud->columns('dinas_id','wilayah','indikator_kinerja');
        $crud->fields('dinas_id','wilayah','indikator_kinerja','indikator_pencapaian','item_indikator','satuan');
        $crud->set_relation('dinas_id', 'tbl_master_dinas', 'dinas');
        $crud->display_as('dinas_id','Dinas');
		
		$crud->display_as('item_indikator','item indikator<br/><span style="color:red">*Gunakan tanda titik koma (;) untuk memisahkan item</span>');

		// $crud->callback_edit_field('item_indikator',array($this,'_set_note_items'));
		// $crud->callback_add_field('item_indikator',array($this,'_set_note_items'));
		
		//$crud->unset_read();
        $output = $crud->render();
                
		$out['output'] = $this->data['judul_browser'];
		$this->load->section('judul_browser', 'default_v', $out);
		$out['output'] = $this->data['judul_utama'];
		$this->load->section('judul_utama', 'default_v', $out);
		$out['output'] = $this->data['judul_sub'];
		$this->load->section('judul_sub', 'default_v', $out);
		$out['output'] = $this->data['u_name'];
		$this->load->section('u_name', 'default_v', $out);
		$this->load->view('default_v', $output);	

	}
	
	function _set_note_items() {
		return "<input type='password' name='pass_word' value='' /><br />Kosongkan password jika tidak ingin ubah/isi.";
	}
}