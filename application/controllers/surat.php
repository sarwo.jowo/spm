<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Surat extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper('fungsi');
		$this->load->library('email');
		$this->load->model('depart_m');
		$this->load->model('surat_m');
	}	
	
	public function index() {
	    $this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';
		

		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';
		#include seach
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';		
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';
		
			
		$tgl = date('Y-m-d');
        $this->data['depart'] = $this->depart_m->get_list_depart();		
		
		$this->data['isi'] = $this->load->view('home_list_t', $this->data, TRUE);

		$this->load->view('themes/layout_utama_v', $this->data);
		
	}
    function listsurat(){
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';
		

		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';
		#include seach
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';		
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';
		
		
		
		$this->load->view('themes/layout_utama_v', $this->data);
	}
	function terima(){	
	    $this->data['judul_browser'] = 'Surat';
		$this->data['judul_utama'] = 'Surat';
		$this->data['judul_sub'] = 'Masuk';
						
	    //$this->data['cash'] = "200";				
		//$this->data['active'] = "100";		
		// $this->data['active'] = $this->home_m->get_sum('actv'); //active point		
		// $this->data['register'] = $this->home_m->get_sum('rgtr'); //register point		
		
		$tgl = date('Y-m-d');
        $this->data['depart'] = $this->depart_m->get_list_depart();		
		
		$this->data['isi'] = $this->load->view('home_list_t', $this->data, TRUE);

		$this->load->view('themes/layout_utama_v', $this->data);
	
	}
	function uploadfsurat(){	
	    $this->data['judul_browser'] = 'Surat';
		$this->data['judul_utama'] = 'Surat';
		$this->data['judul_sub'] = 'Masuk';
		
		$tgl = date('Y-m-d');
        $this->data['depart'] = $this->depart_m->get_list_depart();		
		
		$this->data['isi'] = $this->load->view('home_list_t', $this->data, TRUE);

		$this->load->view('themes/layout_utama_v', $this->data);
		
	}
	function masuk(){	
	  	if(!isset($_POST)) {
			show_404();
		}
		if($this->surat_m->create()){
			 echo json_encode(array('ok' => true, 'msg' => '<div class="text-green"><i class="fa fa-check"></i> Data berhasil disimpan </div>'));
		}else
		{
			echo json_encode(array('ok' => false, 'msg' => '<div class="text-red"><i class="fa fa-ban"></i> Gagal menyimpan data <strong></strong>. </div>'));
		}
	}
	function daftar(){	
		
		$username = isset($_POST['username']) ? $_POST['username'] : 0;		
		$password = isset($_POST['passwords']) ? $_POST['passwords'] : 0;	
		$email = isset($_POST['email']) ? $_POST['email'] : 0;
		
		$pesan = "Selamat anda telah bergabung di ".$company[0].", Username ".$username[0]." dan Password ".$password[0]."";
		
		$email = $email;
		
		//error_log(serialize($dataa));			

		//save upline and member
	
		
		
		if(!$this->sendEmail($from, $_pass, $email, $pesan, $company)){
		echo json_encode(array('ok' => false, 'msg' => '<div class="text-red"><i class="fa fa-ban"></i> Gagal menyimpan data, pastikan nilai lebih dari <strong>0 (NOL)</strong>. </div>'));
		}else{
		echo json_encode(array('ok' => true, 'msg' => '<div class="text-green"><i class="fa fa-check"></i> Data berhasil disimpan </div>'));
		}
	}
	
	function error($data)
	{		
	    $this->data['judul_browser'] = 'Register';
		$this->data['judul_utama'] = 'error';
		$this->data['judul_sub'] = '';
		
		$this->data['isi'] = '<div class="alert alert-danger">Anda tidak memiliki Akses.</div>';

		$this->load->view('themes/layout_utama_v', $this->data);
		
	}
	
	function upgrade(){
		if(!isset($_POST)) {
			show_404();
		}
		//$this->register_m->upgrade();
		if($this->register_m->upgrade()){
			echo json_encode(array('ok' => true, 'msg' => '<div class="text-green"><i class="fa fa-check"></i> Data berhasil disimpan </div>'));
		} else {
			echo json_encode(array('ok' => false, 'msg' => '<div class="text-red"><i class="fa fa-ban"></i> Gagal menyimpan data, pastikan nilai lebih dari <strong>0 (NOL)</strong>. </div>'));
		}
	}	
	
	function check_exist($field=null){
		$dt = $_POST[$field];
		//$dt = $_POST['username'];
		$chk = $this->register_m->select_field('member', $field, $field, $dt);
		$cnt_chk = count($chk);		
		echo $cnt_chk;
		//echo json_encode(array('ok' => false, 'msg' => '<div class="text-red"><i class="fa fa-ban"></i> '$cnt_chk.'</strong>. </div>'));

	}
	function check_kiri($field=null){
	    $dt = $_POST['username'];
		$cnt_chk=0;
		$chk = $this->register_m->select_field('upline', $field, 'username', $dt);		
		if($chk[0] !="") 
			$cnt_chk =1;		
		echo $cnt_chk;
		
	}
	
	function check_kanan($field=null){
		$dt = $_POST[$field];
		$posisi = "kanan";
		$chk = $this->register_m->select_field('upline', $posisi, $posisi, $dt);
		$cnt_chk = count($chk);		
		
		echo $cnt_chk;
	}
	
	function sendEmail($from=null, $pass=null, $to=null, $msg=null, $company=null){
		
		/*
		$msg = "Selamat ";		
		$from = "hanssn88@gmail.com";
		$pass = "";
		$to = $from;
		*/
		
		$sub = "Informasi Pendaftaran";
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_timeout' => 5,
            'smtp_user' => $from,
            'smtp_pass' => $pass,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'crlf' => "\r\n",
            'newline' => "\r\n",
			'priority' => '1'           
        );	
        
        $this->email->initialize($config);
        $this->email->from($from, $company);
        $this->email->to($to);
		$this->email->reply_to('noreply@cca.com', 'samurai.co.id');
        $this->email->subject($sub);
        $this->email->message($msg);
		
		/**
		if (!$this->email->send()){
        $data['message'] ="Email not sent \n".$this->email->print_debugger();      
		} else{
		  $data['message'] ="Email was successfully sent to $to";      
		}
		print_r($data);
		**/
		
        return $this->email->send();      
    }	
	
	function test($u=null){
		print_r($this->register_m->select_field('admin'));
	}
		
	public function no_akses() {
		$this->data['judul_browser'] = 'Tidak Ada Akses';
		$this->data['judul_utama'] = 'Tidak Ada Akses';
		$this->data['judul_sub'] = '';
		$this->data['isi'] = '<div class="alert alert-danger">Anda tidak memiliki Akses.</div>';
		$this->load->view('themes/layout_utama_v', $this->data);
	}
	
	

}
