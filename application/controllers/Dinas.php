<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dinas extends AdminController {

	public function __construct() {
		parent::__construct();	
		$this->load->model('generate_model');
    }
     public function index() {
         redirect(site_url('home'));
     } 
    
    public function indexKab() {
		$this->data['judul_browser'] = 'Data';
		$this->data['judul_utama'] = 'SPM Kabupaten/Kota';
	  
		$this->output->set_template('gc');

		$this->load->library('grocery_CRUD');
		$crud = new grocery_CRUD();
        $crud->set_table('tbl_dinas');
		$act = $this->uri->segment(3);
        if($this->session->userdata('level') == 'SUBDIT')
        {
		$crud->where('tbl_dinas.dinas',$this->session->userdata('dinas_id'));
		$crud->where('wilayah', 'Kabupaten/Kota');	
		}
		else
		{
		 $crud->where('wilayah', 'Kabupaten/Kota');	
		}
		
		
		if($this->session->userdata('level') == 'ADMINPROV')
        {	
	    $crud->where('tbl_dinas.id_prov',$this->session->userdata('id_prov'));
		$crud->where('wilayah', 'Kabupaten/Kota');	
		$crud->columns('id_tr_dinas','dinas','id_prov','id_kabkot','tahun','flag_generated','status_dokumen');
	    }
		
		
		if($this->session->userdata('level') == 'ADMINKAB')
        {	
	    if ($act != 'add')
		{	
        $crud->set_relation('id_kabkot', 'kab', 'nama_kabkot');	
		}
	    $crud->where('tbl_dinas.id_kabkot',$this->session->userdata('u_name'));
		$crud->where('wilayah', 'Kabupaten/Kota');	
		$crud->columns('id_tr_dinas','id_kabkot','dinas','tahun','flag_generated','status_dokumen');
		$crud->unset_delete();
	    }
		
		if($this->session->userdata('level') == 'ADMINPROV')
        {
        $crud->unset_add();
        $crud->unset_edit();
		$crud->unset_delete();
		}	

		if($this->session->userdata('level') == 'admin')
        {
        $crud->unset_add();
        $crud->unset_edit();
		//$crud->unset_delete();
		}		
		
				
		$crud->set_subject('Data Dinas Kabupaten/Kota');
        $crud->callback_before_insert(array($this,'check2'));   
		
		$crud->field_type('tahun','dropdown', array('2018' => '2018', '2019' => '2019','2020' => '2020','2021' => '2021','2022' => '2022'));
        $crud->fields('dinas','id_prov','id_kabkot','tahun','wilayah');     
        $crud->callback_add_field('wilayah',array($this,'wilayah_kab_callback'));
        $crud->set_relation('dinas', 'tbl_master_dinas', 'dinas');
		
        $crud->display_as('dinas','Dinas');
        $crud->display_as('id_prov','Provinsi');
        $crud->display_as('id_kabkot','Kabupaten');
        $crud->display_as('flag_generated','status Laporan');
        $crud->display_as('id_tr_dinas','ID');
      
      		
		if($this->session->userdata('level') == 'ADMINKAB')
        {
		$crud->field_type('id_kabkot', 'hidden',$this->session->userdata('u_name'));
        $crud->field_type('id_prov', 'hidden',$this->session->userdata('id_prov'));					
		}	
		else
		{
		$crud->set_relation('id_kabkot', 'kab', 'nama_kabkot');
		$crud->set_relation('id_prov', 'prop', 'nama_prov');		
        $crud->set_relation('id_kabkot', 'kab', 'nama_kabkot');	
		}	

        $crud->add_action('lihat detail laporan', '', '','glyphicon glyphicon-list',array($this,'generate_reportkab'));
        $crud->add_action('lihat dokumen', '', '','glyphicon glyphicon-file',array($this,'view_dokument'));

        //$this->load->library('gc_dependent_select');
		if($this->session->userdata('level') == 'SUBDIT')
        {		
		$crud->unset_add();
        $crud->unset_edit();
		$crud->unset_delete();
		}
        // settings      
         

        $output = $crud->render();


        
		$out['output'] = $this->data['judul_browser'];
		$this->load->section('judul_browser', 'default_v', $out);
		$out['output'] = $this->data['judul_utama'];
		$this->load->section('judul_utama', 'default_v', $out);
		$out['output'] = $this->data['judul_sub'];
		$this->load->section('judul_sub', 'default_v', $out);
		$out['output'] = $this->data['u_name'];
		$this->load->section('u_name', 'default_v', $out);
		$this->load->view('default_v', $output);	

    }
    
	function rolekey_prov()
	{
		$id_prov = 11;
		$dinas = 6;
		$tahun = 2020;
		$ada = $this->generate_model->check_exists1($id_prov,$dinas,$tahun);
		if ($ada > 0)
		  {
		   $this->form_validation->set_message('username_check', 'The username already exists');
		   return FALSE;
		  }
		  else
		  {
		   return TRUE;
		  }		
	}

	

    function view_dokument($primary_key , $row){

        return site_url('generate_indikator/UploadDokumen').'?dinasid='.$row->id_tr_dinas;
        
    }

    function generate_reportprov($primary_key , $row){

        if($row->flag_generated=="aktif"){
            return site_url('generate_indikator/editindikator').'?dinasid='.$row->id_tr_dinas.'&wilayah=provinsi&dinas='.$row->dinas;
        }
        else{
            return site_url('generate_indikator/index').'?dinasid='.$row->id_tr_dinas.'&wilayah=provinsi&dinas='.$row->dinas;
        }
        
    }


    public function indexProv() {
		$this->data['judul_browser'] = 'Data.';
		$this->data['judul_utama'] = 'SPM Provinsi.';
	  
		$this->output->set_template('gc');

		$this->load->library('grocery_CRUD');
		$crud = new grocery_CRUD();
        $crud->set_table('tbl_dinas');		
		
		$act = $this->uri->segment(3);
		
       
		$crud->set_subject('Data Dinas Provinsi');
        	   
		
        $crud->fields('dinas','id_prov','tahun','wilayah');
        //$crud->field_type('wilayah','hidden','Provinsi');
		$crud->field_type('tahun','dropdown', array('2018' => '2018', '2019' => '2019','2020' => '2020','2021' => '2021','2022' => '2022'));
        $crud->callback_add_field('wilayah',array($this,'wilayah_prov_callback'));        
        $crud->set_relation('dinas', 'tbl_master_dinas', 'dinas');
		
		
        
        $crud->display_as('dinas','Dinas');
        $crud->display_as('id_prov','Provinsi');
        $crud->display_as('flag_generated','status Laporan');
        $crud->display_as('id_tr_dinas','ID');
        
		$user = substr($this->session->userdata('u_name'), 0, 2);
       // $crud->set_relation('id_prov', 'prop', 'nama_prov');		

        //$crud->set_relation('id_kabkot', 'kab', 'nama_kabkot');

        $crud->add_action('detail laporan', '', '','glyphicon glyphicon-list',array($this,'generate_reportprov'));
        $crud->add_action('lihat dokumen', '', '','glyphicon glyphicon-file',array($this,'view_dokument'));
		
		
		if($this->session->userdata('level') == 'SUBDIT')
        {
		$crud->field_type('id_prov', 'hidden',$user);
        $crud->set_relation('id_prov', 'prop', 'nama_prov');	
        $crud->columns('id_tr_dinas','id_prov','dinas','tahun','flag_generated','status_dokumen');
		$crud->where('tbl_dinas.dinas',$this->session->userdata('dinas_id'));
		$crud->where('wilayah', 'Provinsi');	
		}
		else
		{
		 $crud->where('wilayah', 'Provinsi');	
		}
		
		if($this->session->userdata('level') == 'ADMINPROV')
        {
		$crud->unset_delete();	
		$crud->field_type('id_prov', 'hidden',$user);
		error_log($act);
		if ($act != 'add')
		{	
        $crud->set_relation('id_prov', 'prop', 'nama_prov');	
		}
		$crud->columns('id_tr_dinas','id_prov','dinas','tahun','flag_generated','status_dokumen');
		$user = substr($this->session->userdata('u_name'), 0, 2);	
		$crud->where('tbl_dinas.id_prov',$user);
		$crud->where('wilayah', 'Provinsi');	
		}
		else
		{
		 $crud->columns('id_tr_dinas','id_prov','dinas','tahun','flag_generated','status_dokumen');	
		 $crud->where('wilayah', 'Provinsi');	
		}
        
		if($this->session->userdata('level') == 'SUBDIT' OR $this->session->userdata('level') == 'admin')
        {
		$crud->set_relation('id_prov', 'prop', 'nama_prov');
		
        $crud->unset_add();
        $crud->unset_edit();
		//$crud->unset_delete();
		}
		
	    if($this->session->userdata('level') == 'SUBDIT' OR $this->session->userdata('level') == 'admin')
        {
		$crud->set_relation('id_prov', 'prop', 'nama_prov');
		
        $crud->unset_add();
        $crud->unset_edit();
		// $crud->unset_delete();
		}
		
		
		//$crud->set_rules('id_prov','tahun','numeric');
		//$this->grocery_crud->set_rules('id_prov', 'id_prov','trim|required|xss_clean|rolekey_prov');
		$crud->callback_before_insert(array($this,'check'));
		

        //custom action Button
        $crud->unset_read();        
        //endregion
        $output = $crud->render();
        
        
		$out['output'] = $this->data['judul_browser'];
		$this->load->section('judul_browser', 'default_v', $out);
		$out['output'] = $this->data['judul_utama'];
		$this->load->section('judul_utama', 'default_v', $out);
		$out['output'] = $this->data['judul_sub'];
		$this->load->section('judul_sub', 'default_v', $out);
		$out['output'] = $this->data['u_name'];
		$this->load->section('u_name', 'default_v', $out);
		$this->load->view('default_v', $output);	

    }
	

	function check($post_array)
	{
		$id_prov = $post_array['id_prov'];
		$dinas = $post_array['dinas'];
		$tahun = $post_array['tahun'];
		$query = $this->db->query("SELECT * FROM `tbl_dinas` WHERE `tahun` = '".$tahun."' AND `dinas` = '".$dinas."' AND `id_prov` = '".$id_prov."'" );
		
		$num = $query->num_rows();
		//echo $num;
		
		if ($num > 0)
		  {
		   //$this->form_validation->set_message('username_check', 'The username already exists');
		   return FALSE;
		  }
		  else
		  {
		   return TRUE;
		  }		
	}
	function check2($post_array)
	{
		$id_kabkot = $post_array['id_kabkot'];
		$dinas = $post_array['dinas'];
		$tahun = $post_array['tahun'];
		$query = $this->db->query("SELECT * FROM `tbl_dinas` WHERE `tahun` = '".$tahun."' AND `dinas` = '".$dinas."' AND `id_kabkot` = '".$id_kabkot."'" );
		
		$num = $query->num_rows();
		//echo $num;
		
		if ($num > 0)
		  {
		   //$this->form_validation->set_message('username_check', 'The username already exists');
		   return FALSE;
		  }
		  else
		  {
		   return TRUE;
		  }		
	}	
		function generate_reportkab($primary_key , $row){

			if($row->flag_generated=="aktif"){
				return site_url('generate_indikator/editIndikator').'?dinasid='.$row->id_tr_dinas.'&wilayah=kabkota&dinas='.$row->dinas;
			}
			else{
				return site_url('generate_indikator/index').'?dinasid='.$row->id_tr_dinas.'&wilayah=kabkota&dinas='.$row->dinas;
			}
			
		}


    function wilayah_prov_callback()
    {
        return '<input type="text" maxlength="50" readonly="readonly" value="Provinsi" name="wilayah" style="width:462px">';
    }

    function status_dokumen_callback()
    {
        return '<input type="text" maxlength="50" readonly="readonly" value="belum tersedia" name="status_dokumen" style="width:462px">';
    }

    function wilayah_kab_callback()
    {
        return '<input type="text" maxlength="50" readonly="readonly" value="Kabupaten/Kota" name="wilayah" style="width:462px">';
    }
    
    
}