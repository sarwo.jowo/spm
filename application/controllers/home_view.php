<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_view extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('home_m');	
		
	}	
	
	function index()
	{
	    $this->data['judul_browser'] = 'Beranda';
		$this->data['judul_utama'] = 'Beranda';
		$this->data['judul_sub'] = 'Menu Utama';
		
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';
		//$this->data['js_files'][] = base_url() . 'assets/easyui/datagrid-detailview.js';

		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';
		#include seach
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';
		$this->data['js_files2'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.js';
		
		$this->data['css_files'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.css';
				
		$this->data['brt'] = $this->home_m->get_berita();
        $this->data['brtutm'] = $this->home_m->get_berita_utm();
			
		$this->data['dt'] = $this->home_m->charts();
			
		$tgl = date('Y-m-d');				
		
		$this->data['isi'] = $this->load->view('home_list_new', $this->data, TRUE);
		$this->load->view('themes/layout_utama_d', $this->data);		
		
	}

    function tentang()
	{	
		$this->data['judul_browser'] = 'Beranda';
		$this->data['judul_utama'] = 'Beranda';
		$this->data['judul_sub'] = 'Menu Utama';
		
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';
		//$this->data['js_files'][] = base_url() . 'assets/easyui/datagrid-detailview.js';

		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';
		#include seach
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';
		$this->data['js_files2'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.js';
		
		$this->data['css_files'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.css';
		
		$tgl = date('Y-m-d');				
		
		$this->data['isi'] = $this->load->view('home_about', $this->data, TRUE);
		$this->load->view('themes/layout_utama_d', $this->data);
	
	}
	
	
	
	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */