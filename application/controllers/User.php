<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends AdminController {

	public function __construct() {
		parent::__construct();	
	}	
	
	public function index() {
		
		//echo "Mobil itu berwarna " . $this->uri->segment('3');
		
		$this->data['judul_browser'] = 'Data';
		$this->data['judul_utama'] = 'Data';
		$this->data['judul_sub'] = 'User';

		$this->output->set_template('gc');

		$this->load->library('grocery_CRUD');
		$crud = new grocery_CRUD();
		
		
	    //error_log($this->session->userdata('id_kabkot'));	
		
		
		$crud->set_subject('Data User');
        
		$crud->columns('u_name', 'level','Keterangan','dinas_id', 'aktif');


		$crud->field_type('aktif','dropdown',
			array('Y' => 'Aktif','N' => 'Non Aktif'));
		
        if($this->session->userdata('level') == 'ADMINPROV')
        {			
		//$crud->field_type('level','input',array('user' => 'User Dinas Propinsi'));
		$crud->fields('u_name', 'level','dinas_id','Keterangan','pass_word', 'aktif','id_prov');
        $crud->field_type('level', 'hidden', 'PENGGUNA');
		$crud->field_type('id_prov', 'hidden', $this->session->userdata('id_prov'));
		$crud->field_type('Keterangan', 'hidden', $this->session->userdata('Keterangan'));
	    $crud->set_relation('dinas_id', 'tbl_master_dinas', 'dinas');

        }else
        {
		$crud->fields('u_name', 'level','dinas_id','Keterangan','pass_word', 'aktif','id_kabkot');
		$crud->field_type('level', 'hidden', 'PENGGUNA');
		$crud->field_type('id_kabkot', 'hidden', $this->session->userdata('u_name'));
		$crud->field_type('Keterangan', 'hidden', $this->session->userdata('Keterangan'));
        $crud->set_relation('dinas_id', 'tbl_master_dinas', 'dinas');
			
		}	


        if($this->session->userdata('level') == 'ADMINPROV')
        {
		//
		$crud->set_table('tbl_user');
		$crud->where('tbl_user.id_prov',$this->session->userdata('id_prov'));
		//$crud->where('tbl_user.id_prov',13);
		error_log($this->session->userdata('id_prov'));
		}
		
		if($this->session->userdata('level') == 'ADMINKAB')
        {
		$crud->set_table('tbl_user');
        $crud->where('tbl_user.id_kabkot',$this->session->userdata('u_name'));
		}

		
		$crud->display_as('u_name','Username');
		$crud->display_as('dinas_id','Dinas');
		
		//$crud->display_as('id_kabkot','Kabupaten');

		$crud->required_fields('u_name','level','aktif');

		$crud->callback_edit_field('pass_word',array($this,'_set_password_input_to_empty'));
		$crud->callback_add_field('pass_word',array($this,'_set_password_input_to_empty'));
		
		$crud->callback_edit_field('u_name',array($this,'_set_uname_input_to_empty'));
		$crud->callback_add_field('u_name',array($this,'_set_uname_input_to_empty'));
		

		$crud->callback_before_insert(array($this,'_encrypt_password_callback'));
		$crud->callback_before_update(array($this,'_encrypt_password_callback'));
		$crud->unset_read();
		$output = $crud->render();

		$out['output'] = $this->data['judul_browser'];
		$this->load->section('judul_browser', 'default_v', $out);
		$out['output'] = $this->data['judul_utama'];
		$this->load->section('judul_utama', 'default_v', $out);
		$out['output'] = $this->data['judul_sub'];
		$this->load->section('judul_sub', 'default_v', $out);
		$out['output'] = $this->data['u_name'];
		$this->load->section('u_name', 'default_v', $out);

		$this->load->view('default_v', $output);
		

	}

	function _set_password_input_to_empty() {
		return "<input type='password' name='pass_word' value='' /><br />Kosongkan password jika tidak ingin ubah/isi.";
	}
	
	function _set_uname_input_to_empty() {
		return "<input type='text' name='u_name' value='' /><br />Gunakan kode Propinsi + nomor urut Misal : 1100 + (1. kesehatan, 2. Pendidikan ....)";
	}

	function _encrypt_password_callback($post_array) {
		if(!empty($post_array['pass_word'])) {
			$post_array['pass_word'] = sha1('nsi' . $post_array['pass_word']);
		} else {
			unset($post_array['pass_word']);
		}
		return $post_array;
	}

}
