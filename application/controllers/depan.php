<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Depan extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('home_m');	
		
	}
		
	
	function index()
	{
	 $this->data['judul_browser'] = 'Beranda';
		$this->data['judul_utama'] = 'Beranda';
		$this->data['judul_sub'] = 'Menu Utama';
		
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';
		//$this->data['js_files'][] = base_url() . 'assets/easyui/datagrid-detailview.js';

		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';
		#include seach
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';
		$this->data['js_files2'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.js';
		
		$this->data['css_files'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.css';
				
		$this->data['brt'] = $this->home_m->get_berita();
        $this->data['brtutm'] = $this->home_m->get_berita_utm();		

			
		$tgl = date('Y-m-d');					
		
		//$this->data['isi'] = $this->load->view('depan', $this->data, TRUE);
		$this->load->view('depan/index', $this->data);		
		
	}	
	function berita()
	{
		$this->data['judul_browser'] = 'Beranda';
		$this->data['judul_utama'] = 'Beranda';
		$this->data['judul_sub'] = 'Menu Utama';
		
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';
		//$this->data['js_files'][] = base_url() . 'assets/easyui/datagrid-detailview.js';

		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';
		#include seach
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';
		$this->data['js_files2'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.js';
		
		$this->data['css_files'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.css';
		$this->data['brt'] = $this->home_m->get_berita2();
        $this->data['brtutm'] = $this->home_m->get_berita_utm2();
		$tgl = date('Y-m-d');				
		
		$this->data['isi'] = $this->load->view('home_news', $this->data, TRUE);
		$this->load->view('themes/layout_utama_d2', $this->data);
		
	}
	function newsdetail($newsdetail)
	{
		$this->data['judul_browser'] = 'Beranda';
		$this->data['judul_utama'] = 'Beranda';
		$this->data['judul_sub'] = 'Menu Utama';
		
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';
		//$this->data['js_files'][] = base_url() . 'assets/easyui/datagrid-detailview.js';

		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';
		#include seach
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';
		$this->data['js_files2'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.js';
		
		$this->data['css_files'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.css';
		$this->data['id'] = $newsdetail;
		$newsdetail = $newsdetail;
      	$this->data['temp'] = $this->home_m->get_det_berita($newsdetail);		
		$this->data['isi'] = $this->load->view('home_berita', $this->data, TRUE);
		//$this->data['isi'] = $this->load->view('home_list_m',array('data'=>$data),true);
		$this->load->view('themes/layout_utama_d2', $this->data);
		
	}
	function tentang()
	{	
		$this->data['judul_browser'] = 'Beranda';
		$this->data['judul_utama'] = 'Beranda';
		$this->data['judul_sub'] = 'Menu Utama';
		
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';
		//$this->data['js_files'][] = base_url() . 'assets/easyui/datagrid-detailview.js';

		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';
		#include seach
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';
		$this->data['js_files2'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.js';
		
		$this->data['css_files'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.css';
		
		$tgl = date('Y-m-d');				
		$this->data['ttg'] = $this->home_m->get_about();
		$this->data['isi'] = $this->load->view('home_about', $this->data, TRUE);
		$this->load->view('themes/layout_utama_d2', $this->data);
	
	}
	
	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */