<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Angg extends AdminController {

	public function __construct() {
		parent::__construct();	
		//$this->load->model('dept_m');
    }
     public function index() {
         redirect(site_url('home'));
     } 
    
    public function anggaranKab() {
		$this->data['judul_browser'] = 'Data';
		$this->data['judul_utama'] = 'Anggaran Kabupaten/Kota';
	  
		$this->output->set_template('gc');

		$this->load->library('grocery_CRUD');
		$crud = new grocery_CRUD();
        $crud->set_table('tbl_newanggaran');
        if($this->session->userdata('level') == 'SUBDIT')
        {
		$crud->where('tbl_newanggaran.dinas',$this->session->userdata('dinas_id'));
		$crud->where('wilayah', 'Kabupaten/Kota');	
		}
		else
		{
		 $crud->where('wilayah', 'Kabupaten/Kota');	
		}
		
		
		if($this->session->userdata('level') == 'ADMINPROV')
        {	
	    $crud->where('tbl_newanggaran.id_prov',$this->session->userdata('id_prov'));
		$crud->where('wilayah', 'Kabupaten/Kota');	
		$crud->columns('id_tr_dinas','dinas','id_prov','id_kabkot','tahun','flag_generated');
	    }
		
		
		if($this->session->userdata('level') == 'ADMINKAB')
        {	
	    $crud->where('tbl_newanggaran.id_kabkot',$this->session->userdata('u_name'));
		$crud->where('wilayah', 'Kabupaten/Kota');	
		$crud->columns('id_tr_dinas','dinas','tahun','flag_generated');
	    }
		
		
		if($this->session->userdata('level') == 'admin'  OR $this->session->userdata('level') == 'SUBDIT')
        {	
	   // $crud->where('tbl_newanggaran.id_prov',$this->session->userdata('id_prov'));
		$crud->where('wilayah', 'Kabupaten/Kota');	
		$crud->columns('id_tr_dinas','dinas','id_prov','id_kabkot','tahun','flag_generated');
	    }
		
		
		if($this->session->userdata('level') == 'ADMINPROV' OR $this->session->userdata('level') == 'admin')
        {
        $crud->unset_add();
        $crud->unset_edit();
		$crud->unset_delete();
		}		
		
				
		$crud->set_subject('Data Dinas Kabupaten/Kota');
        	   
		$act = $this->uri->segment(3);
        if($this->session->userdata('level') == 'ADMINKAB' && $act != 'add')
        {
	   
        $crud->set_relation('id_kabkot', 'kab', 'nama_kabkot');	
		
	    } 
		
        $crud->fields('dinas','id_prov','id_kabkot','tahun','wilayah'); 
		
        $crud->callback_add_field('wilayah',array($this,'wilayah_kab_callback'));
        $crud->set_relation('dinas', 'tbl_master_dinas', 'dinas');
        $crud->display_as('dinas','Dinas');
        $crud->display_as('id_prov','Provinsi');
        $crud->display_as('id_kabkot','Kabupaten');
        $crud->display_as('flag_generated','status Anggaran');
        $crud->display_as('id_tr_dinas','ID');
        $crud->field_type('tahun','dropdown', array('2018' => '2018', '2019' => '2019','2020' => '2020','2021' => '2021','2022' => '2022')); 
      		
		if($this->session->userdata('level') == 'ADMINKAB')
        {
		$crud->field_type('id_kabkot', 'hidden',$this->session->userdata('u_name'));
        $crud->field_type('id_prov', 'hidden',$this->session->userdata('id_prov'));					
		}	
		else
		{
		$crud->set_relation('id_prov', 'prop', 'nama_prov');		
        $crud->set_relation('id_kabkot', 'kab', 'nama_kabkot');	
		}	

        $crud->add_action('lihat detail laporan', '', '','glyphicon glyphicon-list',array($this,'generate_reportkab'));
      //  $crud->add_action('lihat dokumen', '', '','glyphicon glyphicon-file',array($this,'view_dokument'));
	
        //$this->load->library('gc_dependent_select');
		if($this->session->userdata('level') == 'SUBDIT')
        {		
		$crud->unset_add();
        $crud->unset_edit();
		$crud->unset_delete();
		}
        // settings

      
            //custom action Button
        $crud->unset_read();        
        //endregion
        $output = $crud->render();

        
		$out['output'] = $this->data['judul_browser'];
		$this->load->section('judul_browser', 'default_v', $out);
		$out['output'] = $this->data['judul_utama'];
		$this->load->section('judul_utama', 'default_v', $out);
		$out['output'] = $this->data['judul_sub'];
		$this->load->section('judul_sub', 'default_v', $out);
		$out['output'] = $this->data['u_name'];
		$this->load->section('u_name', 'default_v', $out);
		$this->load->view('default_v', $output);	

    }
    
    function generate_reportkab($primary_key , $row){

        if($row->flag_generated=="aktif"){
            return site_url('generate_anggaran/editIndikator').'?dinasid='.$row->id_tr_dinas.'&wilayah=kabkota&dinas='.$row->dinas;
        }
        else{
            return site_url('generate_anggaran/index').'?dinasid='.$row->id_tr_dinas.'&wilayah=kabkota&dinas='.$row->dinas;
        }
        
    }

    function view_dokument($primary_key , $row){

        return site_url('generate_indikator/UploadDokumen').'?dinasid='.$row->id_tr_dinas;
        
    }

    function generate_reportprov($primary_key , $row){

        if($row->flag_generated=="aktif"){
            return site_url('generate_anggaran/editindikator').'?dinasid='.$row->id_tr_dinas.'&wilayah=provinsi&dinas='.$row->dinas;
        }
        else{
            return site_url('generate_anggaran/index').'?dinasid='.$row->id_tr_dinas.'&wilayah=provinsi&dinas='.$row->dinas;
        }
        
    }


    public function anggaranProv() {
		$this->data['judul_browser'] = 'Data';
		$this->data['judul_utama'] = 'Anggaran Provinsi';
	  
		$this->output->set_template('gc');

		$this->load->library('grocery_CRUD');
		$crud = new grocery_CRUD();
        $crud->set_table('tbl_newanggaran');		
		
		
       
		$crud->set_subject('Data Anggaran Provinsi');
        	   
		$crud->columns('id_tr_dinas','id_prov','dinas','tahun','flag_generated');
        $crud->fields('dinas','id_prov','tahun','wilayah');
        //$crud->field_type('wilayah','hidden','Provinsi');
        $crud->callback_add_field('wilayah',array($this,'wilayah_prov_callback'));        
        $crud->set_relation('dinas', 'tbl_master_dinas', 'dinas');
        
        $crud->display_as('dinas','Dinas');
        $crud->display_as('id_prov','Provinsi');
        $crud->display_as('flag_generated','status Laporan');
        $crud->display_as('id_tr_dinas','ID');
        $crud->field_type('tahun','dropdown', array('2018' => '2018', '2019' => '2019','2020' => '2020','2021' => '2021','2022' => '2022'));
		$user = substr($this->session->userdata('u_name'), 0, 2);
       // $crud->set_relation('id_prov', 'prop', 'nama_prov');		
		$crud->field_type('id_prov', 'hidden',$user);
       // $crud->set_relation('id_prov', 'prop', 'nama_prov');
        //$crud->set_relation('id_kabkot', 'kab', 'nama_kabkot');

        $crud->add_action('detail laporan', '', '','glyphicon glyphicon-list',array($this,'generate_reportprov'));
       // $crud->add_action('lihat dokumen', '', '','glyphicon glyphicon-file',array($this,'view_dokument'));
		
		
		if($this->session->userdata('level') == 'SUBDIT')
        {

		$crud->where('tbl_newanggaran.dinas',$this->session->userdata('dinas_id'));
		$crud->where('wilayah', 'Provinsi');	
		}
		else
		{
		 $crud->where('wilayah', 'Provinsi');	
		}
		
		if($this->session->userdata('level') == 'ADMINPROV')
        {
		$user = substr($this->session->userdata('u_name'), 0, 2);	
		$crud->where('tbl_newanggaran.id_prov',$user);
		$crud->where('wilayah', 'Provinsi');	
		}
		else
		{
		 $crud->where('wilayah', 'Provinsi');	
		}
        
		if($this->session->userdata('level') == 'SUBDIT' OR $this->session->userdata('level') == 'admin')
        {
        $crud->unset_add();
        $crud->unset_edit();
		$crud->unset_delete();
		}
		
	    $act = $this->uri->segment(3);
        if($this->session->userdata('level') == 'ADMINPROV' && $act != 'add')
        {
	   
        $crud->set_relation('id_prov', 'prop', 'nama_prov');	
		
	    } 
		if($this->session->userdata('level') == 'admin' OR $this->session->userdata('level') == 'SUBDIT'){
		$crud->set_relation('id_prov', 'prop', 'nama_prov');	
		}	
		
        //custom action Button
        $crud->unset_read();        
        //endregion
        $output = $crud->render();
        
        
		$out['output'] = $this->data['judul_browser'];
		$this->load->section('judul_browser', 'default_v', $out);
		$out['output'] = $this->data['judul_utama'];
		$this->load->section('judul_utama', 'default_v', $out);
		$out['output'] = $this->data['judul_sub'];
		$this->load->section('judul_sub', 'default_v', $out);
		$out['output'] = $this->data['u_name'];
		$this->load->section('u_name', 'default_v', $out);
		$this->load->view('default_v', $output);	

    }

    function wilayah_prov_callback()
    {
        return '<input type="text" maxlength="50" readonly="readonly" value="Provinsi" name="wilayah" style="width:462px">';
    }

    function status_dokumen_callback()
    {
        return '<input type="text" maxlength="50" readonly="readonly" value="belum tersedia" name="status_dokumen" style="width:462px">';
    }

    function wilayah_kab_callback()
    {
        return '<input type="text" maxlength="50" readonly="readonly" value="Kabupaten/Kota" name="wilayah" style="width:462px">';
    }
    
    
}