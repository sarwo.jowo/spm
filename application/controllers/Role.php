<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Role extends MY_Controller {

	public function __construct() {
		parent::__construct();		
		$this->load->model('Admin_m');				
	}	
	
	public function index() {
		if($this->session->userdata('level') !=1){
			$this->no_akses();		
			return false;
		}
		$this->data['judul_browser'] = 'User';
		$this->data['judul_utama'] = 'List';
		$this->data['judul_sub'] = 'Users';
		$this->data['title_box'] = 'List of users';

		$this->data['users'] = $this->Admin_m->get_admin();
		
		$this->data['isi'] = $this->load->view('user_v', $this->data, TRUE);

		$this->load->view('themes/layout_utama_v', $this->data);
	}
	
	public function save(){		
		if($this->session->userdata('level') !=1){
			$this->no_akses();		
			return false;
		}
		echo $this->Admin_m->save();
	}
	
	public function del(){	
		if($this->session->userdata('level') !=1){
			$this->no_akses();		
			return false;
		}
		echo $this->Admin_m->del();
	}
	
	public function no_akses() {
		if ($this->session->userdata('login') == FALSE) {
			redirect('/');
			return false;
		}
		$this->data['judul_browser'] = 'Tidak Ada Akses';
		$this->data['judul_utama'] = 'Tidak Ada Akses';
		$this->data['judul_sub'] = '';
		$this->data['isi'] = '<div class="alert alert-danger">Anda tidak memiliki Akses.</div>';
		$this->load->view('themes/layout_utama_v', $this->data);
	}


}
