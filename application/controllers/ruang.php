<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ruang extends MY_Controller {

	public function __construct() {
		parent::__construct();	
		//$this->output->enable_profiler(TRUE);
		$this->load->helper('fungsi');
		$this->load->model('ruang_m');		
	}	

	public function index() {
		$this->data['judul_browser'] = 'Transaksi';
		$this->data['judul_utama'] = 'Transaksi';
		$this->data['judul_sub'] = 'Data Ruang';

		$this->data['judul_browser'] = 'Transaksi';
		$this->data['judul_utama'] = 'Transaksi';
		$this->data['judul_sub'] = 'Pengeluaran Kas Tunai';

		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';

		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';

		#include daterange
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';

		//number_format
		$this->data['js_files'][] = base_url() . 'assets/extra/fungsi/number_format.js';

		$this->data['ruang_id'] = $this->ruang_m->get_list_ruang();
		//$this->data['akun_id'] = $this->pengeluaran_m->get_data_akun();
		//error_log("testing ruang");
		
		$this->data['isi'] = $this->load->view('ruang_v', $this->data, TRUE);
		$this->load->view('themes/layout_utama_v', $this->data);
	}

	function ajax_list() {
		/*Default request pager params dari jeasyUI*/
		$offset = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$limit  = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		$sort  = isset($_POST['sort']) ? $_POST['sort'] : 'tgl_transaksi';
		$order  = isset($_POST['order']) ? $_POST['order'] : 'desc';
		$kode_transaksi = isset($_POST['kode_transaksi']) ? $_POST['kode_transaksi'] : '';
		$tgl_dari = isset($_POST['tgl_dari']) ? $_POST['tgl_dari'] : '';
		$tgl_sampai = isset($_POST['tgl_sampai']) ? $_POST['tgl_sampai'] : '';
		$search = array('kode_transaksi' => $kode_transaksi, 
			'tgl_dari' => $tgl_dari, 
			'tgl_sampai' => $tgl_sampai);
		$offset = ($offset-1)*$limit;
		$data   = $this->ruang_m->get_data_transaksi_ajax($offset,$limit,$search,$sort,$order);
		$i	= 0;
		$rows   = array(); 

		foreach ($data['data'] as $r) {
			$startdate = explode(' ', $r->startdate);
			$txt_tanggal = jin_date_ina($startdate[0]);
			$txt_tanggal .= ' - ' . substr($startdate[1], 0, 5);			
			$status = $r->st;
			if ($status == 0)
			{
			$st = "Menunggu Approval";	
			}				
			else if ($status == 1)
			{
			$st = "Approved";	
			}
			else
			{
			$st = "No status";	
			}
			$nmruang = $this->ruang_m->get_jenis_ruang($r->rgid);	
			$ruang = $nmruang[0]->nama;
		    			
			$rows[$i]['id'] = $r->id;
			$rows[$i]['startdate'] = $r->startdate;
			$rows[$i]['enddate'] = $r->enddate;
			$rows[$i]['title'] = $r->title;			
			$rows[$i]['description'] = $r->description;	            			
			$rows[$i]['u_name'] = $r->u_name;
            $rows[$i]['st'] = $st;			
			$rows[$i]['rgid'] = $ruang;
			$i++;
		}
		//keys total & rows wajib bagi jEasyUI
		$result = array('total'=>$data['count'],'rows'=>$rows);
		echo json_encode($result); //return nya json
	}

	public function create() {
		
		//error_log(serialize($_POST));
		//print_r($_POST);
		if(!isset($_POST)) {
			show_404();
		}
		if($this->ruang_m->create()){
			echo json_encode(array('ok' => true, 'msg' => '<div class="text-green"><i class="fa fa-check"></i> Data berhasil disimpan </div>'));
		}else
		{
			echo json_encode(array('ok' => false, 'msg' => '<div class="text-red"><i class="fa fa-ban"></i> Gagal menyimpan data <strong></strong>. </div>'));
		}
	}

	public function update($id=null) {
		if(!isset($_POST)) {
			show_404();
		}
		if($this->ruang->update($id)) {
			echo json_encode(array('ok' => true, 'msg' => '<div class="text-green"><i class="fa fa-check"></i> Data berhasil diubah </div>'));
		} else {
			echo json_encode(array('ok' => false, 'msg' => '<div class="text-red"><i class="fa fa-ban"></i>  Maaf, Data gagal diubah, pastikan nilai lebih dari <strong>0 (NOL)</strong>. </div>'));
		}

	}
	public function delete() {
		if(!isset($_POST))	 {
			show_404();
		}
		$id = intval(addslashes($_POST['id']));
		if($this->ruang->delete($id))
		{
			echo json_encode(array('ok' => true, 'msg' => '<div class="text-green"><i class="fa fa-check"></i> Data berhasil dihapus </div>'));
		} else {
			echo json_encode(array('ok' => false, 'msg' => '<div class="text-red"><i class="fa fa-ban"></i> Maaf, Data gagal dihapus </div>'));
		}
	}


function cetak_laporan() {
	// $pengeluaran = $this->ruang_m->lap_data_booking();
	// if($pengeluaran == FALSE) {
		// redirect('pengeluaran_kas');
		// exit();
	// }

	// $tgl_dari = $_REQUEST['tgl_dari']; 
	// $tgl_sampai = $_REQUEST['tgl_sampai']; 

	// $this->load->library('Pdf');
	// $pdf = new Pdf('L', 'mm', 'A4', true, 'UTF-8', false);
	// $pdf->set_nsi_header(TRUE);
	// $pdf->AddPage('L');
	// $html = '';
	// $html .= '
	// <style>
		// .h_tengah {text-align: center;}
		// .h_kiri {text-align: left;}
		// .h_kanan {text-align: right;}
		// .txt_judul {font-size: 12pt; font-weight: bold; padding-bottom: 12px;}
		// .header_kolom {background-color: #cccccc; text-align: center; font-weight: bold;}
		// .txt_content {font-size: 10pt; font-style: arial;}
	// </style>
	// '.$pdf->nsi_box($text = '<span class="txt_judul">Laporan Data Ruang<br></span>
		// <span> Periode '.jin_date_ina($tgl_dari).' - '.jin_date_ina($tgl_sampai).'</span>', $width = '100%', $spacing = '0', $padding = '1', $border = '0', $align = 'center').'

	// <table width="100%" cellspacing="0" cellpadding="3" border="1" border-collapse= "collapse">
	// <tr class="header_kolom">
		// <th class="h_tengah" style="width:5%;" > No. </th>
		
		// <th class="h_tengah" style="width:15%;"> Tanggal </th>
		// <th class="h_tengah" style="width:40%;"> Uraian  </th>
		// <th class="h_tengah" style="width:20%;"> Jumlah  </th>
		// <th class="h_tengah" style="width:10%;"> User </th>
	// </tr>';

	// $no =1;
	// $jml_tot = 0;
	// foreach ($pengeluaran as $row) {
		// $tgl_bayar = explode(' ', $row->tgl_catat);
		// $txt_tanggal = jin_date_ina($tgl_bayar[0],'p');

		// $jml_tot += 0;

		// $html .= '
		// <tr>
			// <td class="h_tengah" >'.$no++.'</td>
			
			// <td class="h_tengah"> '.$txt_tanggal.'</td>
			// <td class="h_kiri"> '.$row->keterangan.'</td>
			// <td class="h_kanan"> -</td>
			// <td> '.$row->user_name.'</td>
		// </tr>';
	// }
	// $html .= '
	// <tr>
		// <td colspan="4" class="h_tengah"><strong> Jumlah Total </strong></td>
		// <td class="h_kanan"> <strong>'.number_format($jml_tot).'</strong></td>
	// </tr>
	// </table>';
	// $pdf->nsi_html($html);
	// $pdf->Output('trans_k'.date('Ymd_His') . '.pdf', 'I');
} 
}