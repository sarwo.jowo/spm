<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Saber extends MY_Controller {

	public function __construct() {
		parent::__construct();
	//	$this->load->helper('fungsi');
		$this->load->helper('fungsi');	
		$this->load->library('email');
		$this->load->model('area_m');
		$this->load->model('prop_m');
		$this->load->model('saber_m');
		$this->load->model('files_model');
		$this->load->model('dataku');
		$this->load->helper('form','url');
	}	
	
	public function index() {
	    $this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';
		

		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';
		#include seach
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';		
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';
				
		$tgl = date('Y-m-d');
		
        $this->data['area'] = $this->area_m->get_list_area();	
        $this->data['isi'] = $this->load->view('home_saber', $this->data, TRUE);

		$this->load->view('themes/layout_utama_v', $this->data);
		
	}
	
	function detail($saber_id = NULL){
		
		//error_log($saber_id);
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';
		

		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';
		#include seach
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';		
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';
				
		$tgl = date('Y-m-d');
        $this->data['gbr'] = "";
        //$this->data['temp'] = $this->saber_m->get_id_pungli();	
		$row= $this->saber_m->get_id_pungli2($saber_id);
		$this->data['temp'] = $row;
		$this->data['area'] = $this->area_m->get_area($row->areaid);
		$this->data['files'] = $this->saber_m->get_files($row->id);		
		$this->data['isi'] = $this->load->view('home_saber2', $this->data, TRUE);

		$this->load->view('themes/layout_utama_v', $this->data);
		
	}
	function uploadfile(){
		
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';
		

		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';
		#include seach
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';		
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';
				
		$tgl = date('Y-m-d');
        $this->data['gbr'] = "";
        $row= $this->saber_m->get_id_pungli();
		$this->data['temp'] = $row;
		$this->data['area'] = $this->area_m->get_area($row->areaid);
		$this->data['files'] = $this->saber_m->get_files($row->id);		
		$this->data['isi'] = $this->load->view('home_saber2', $this->data, TRUE);

		$this->load->view('themes/layout_utama_v', $this->data);
		
	}
    function listlapor(){
		//error_log($usename);
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';
		
		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';
		#include seach
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';		
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';
	    $this->data['area'] = $this->area_m->get_list_area();
		$this->data['wl'] = $this->area_m->get_list_wl();
		$prop = "-";
		$this->data['prop'] = $this->prop_m->get_list_prop();
        $this->data['kab'] = $this->prop_m->get_list_kab($prop);        		
		$this->data['isi'] = $this->load->view('listsaber_v', $this->data, TRUE);
		$this->load->view('themes/layout_utama_v', $this->data);
	}
	function getprop(){
		$prov = $this->input->post('pr');
	    $arrStates = $this->prop_m->get_list_kab($prov);  //mengambil data dari database melalui model mcombox
        foreach ($arrStates as $states) {
                $arrstates[$states->kabupaten] = $states->kabupaten;
            } //array dibuat untuk ditampilkan pada combox box
        print form_dropdown('kab',$arrstates); //setelah berhasil diambil lalu ditampilkan
   

	}
	function listlapormember(){
		//error_log($usename);
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';
		
		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';
		#include seach
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';		
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';
		//$this->files_model->get_files();	
		$this->data['kab'] = $this->prop_m->get_list_kab2();  
		$this->data['area'] = $this->area_m->get_list_area();
		$this->data['isi'] = $this->load->view('listsaber_m', $this->data, TRUE);
		$this->load->view('themes/layout_utama_v', $this->data);
	}
	function ajax_list_member() {
		/*Default request pager params dari jeasyUI*/		
		//error_log($usename);
		$offset = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$limit  = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		$sort  = isset($_POST['sort']) ? $_POST['sort'] : 'tgl_transaksi';
		$order  = isset($_POST['order']) ? $_POST['order'] : 'desc';
		$area = isset($_POST['area']) ? $_POST['area'] : '';
		$kab = isset($_POST['kab']) ? $_POST['kab'] : '';
		$search = array('area' => $area, 
			'kab' => $kab);
		
		$offset = ($offset-1)*$limit;
		
		$data   = $this->saber_m->get_data_transaksi_ajax_member($offset,$limit,$search,$sort,$order);
		$i	= 0;
		$rows   = array(); 

		foreach ($data['data'] as $r) {		
			$tgl_lapor = date('d-M-Y', strtotime($r->tgl_masuk));
            $tgl_kejadian = date('d-M-Y', strtotime($r->tgl_lapor));			
			$rows[$i]['id'] = $r->id;
			$rows[$i]['tgl_lapor'] = $tgl_lapor;
			$rows[$i]['tgl_kejadian'] = $tgl_kejadian;
			$rows[$i]['area'] = "-";
			$rows[$i]['kegiatan'] = $r->kegiatan;
			$rows[$i]['skpd'] = $r->skpd;			
			$rows[$i]['pelaku'] = $r->pelaku;
            $rows[$i]['nilai'] = $r->nilai;           	
            $rows[$i]['detail'] ='<a href="'.site_url('Saber').'/detail/' . $r->id . '" title="Detail"> <i class="fa fa-search"></i> Detail </a>';
			$i++;
		}
		$result = array('total'=>$data['count'],'rows'=>$rows);
		echo json_encode($result); //return nya json
	}
	function excel()
	{
		$tgl_dari = $_REQUEST['tgl_dari']; 
		$tgl_sampai = $_REQUEST['tgl_sampai']; 
		$query['data'] = $this->saber_m->get_data_cetak(); 
        $this->load->view('cetak',$query);
	}
	function ajax_list($usename=null) {
		/*Default request pager params dari jeasyUI*/		
		
	    $offset = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$limit  = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		$sort  = isset($_POST['sort']) ? $_POST['sort'] : 'tgl_transaksi';
		$order  = isset($_POST['order']) ? $_POST['order'] : 'desc';
		$area = isset($_POST['area']) ? $_POST['area'] : '';
		$pr = isset($_POST['pr']) ? $_POST['pr'] : '';
		$kab = isset($_POST['kab']) ? $_POST['kab'] : '';
		
		$search = array('area' => $area, 'pr' => $pr,
			'kab' => $kab);
		$offset = ($offset-1)*$limit;		
		$data   = $this->saber_m->get_data_transaksi_ajax($offset,$limit,$search,$sort,$order);
		$i	= 0;
		$rows   = array();  
		foreach ($data['data'] as $r) {
			$nmarea = $this->area_m->get_area($r->areaid);	
			$area = $nmarea[0]->nama;		
			$tgl_lapor = date('d-M-Y', strtotime($r->tgl_masuk));
            $tgl_kejadian = date('d-M-Y', strtotime($r->tgl_lapor));			
			$rows[$i]['id'] = $r->id;
			$rows[$i]['tgl_lapor'] = $tgl_lapor;
			$rows[$i]['tgl_kejadian'] = $tgl_kejadian;
			$rows[$i]['area'] = $area;
			$rows[$i]['kegiatan'] = $r->kegiatan;
			$rows[$i]['skpd'] = $r->skpd;			
			$rows[$i]['pelaku'] = $r->pelaku;
            $rows[$i]['nilai'] = $r->nilai;			
			//$rows[$i]['u_name'] = $r->u_name;
           
            $rows[$i]['detail'] ='<a href="'.site_url('Saber').'/detail/' . $r->id . '" title="Detail"> <i class="fa fa-search"></i> Detail </a>';
					
			$i++;
		}
		$result = array('total'=>$data['count'],'rows'=>$rows);
		echo json_encode($result); //return nya json
	}
	function terima(){	
	    $this->data['judul_browser'] = 'Surat';
		$this->data['judul_utama'] = 'Surat';
		$this->data['judul_sub'] = 'Masuk';						
	  
		$tgl = date('Y-m-d');
        $this->data['depart'] = $this->depart_m->get_list_depart();		
		
		$this->data['isi'] = $this->load->view('home_list_t', $this->data, TRUE);
		$this->load->view('themes/layout_utama_v', $this->data);
	
	}
	public function upload()
	{
		if(isset($_FILES['image']))
		{
			$config['upload_path'] = './assets/uploads';
			$config['allowed_types'] = 'gif|jpg|png|bmp|doc|docx|pdf';
            $config['max_size']  = 1024 * 8;
            $config['encrypt_name'] = TRUE; 
			$this->load->library('upload', $config);
			if($this->upload->do_upload('image'))
			{
			$image_data = $this->upload->data();
            $this->files_model->insert_file($image_data['file_name'], $_POST['idsb']);			
				
			}
		}	
	} 
    	
	function deletefile()
	{
		$id = $_POST["id"];	
		$row= $this->saber_m->get_file_name($id);
	    if(!isset($_POST)) {
			show_404();
		}
		if($this->saber_m->deletefile()){
			 $path_to_file = './assets/uploads/'.$row->filename;
	         unlink($path_to_file);
			 echo json_encode(array('ok' => true, 'msg' => '<div class="text-green"><i class="fa fa-check"></i> File berhasil di delete</div>'));
		    			 
		}else
		{
			echo json_encode(array('ok' => false, 'msg' => '<div class="text-red"><i class="fa fa-ban"></i> File gagal delete <strong></strong>. </div>'));
		}
	}
	function masuk(){	
	  	if(!isset($_POST)) {
			show_404();
		}
		if($this->saber_m->create()){
			 echo json_encode(array('ok' => true, 'msg' => '<div class="text-green"><i class="fa fa-check"></i> Data berhasil disimpan </div>'));
		}else
		{
			echo json_encode(array('ok' => false, 'msg' => '<div class="text-red"><i class="fa fa-ban"></i> Gagal menyimpan data <strong></strong>. </div>'));
		}
	}
	function daftar(){	
		
		$username = isset($_POST['username']) ? $_POST['username'] : 0;		
		$password = isset($_POST['passwords']) ? $_POST['passwords'] : 0;	
		$email = isset($_POST['email']) ? $_POST['email'] : 0;
		
		$pesan = "Selamat anda telah bergabung di ".$company[0].", Username ".$username[0]." dan Password ".$password[0]."";
		
		$email = $email;
		
		
		if(!$this->sendEmail($from, $_pass, $email, $pesan, $company)){
		echo json_encode(array('ok' => false, 'msg' => '<div class="text-red"><i class="fa fa-ban"></i> Gagal menyimpan data, pastikan nilai lebih dari <strong>0 (NOL)</strong>. </div>'));
		}else{
		echo json_encode(array('ok' => true, 'msg' => '<div class="text-green"><i class="fa fa-check"></i> Data berhasil disimpan </div>'));
		}
	}
	
	function error($data)
	{		
	    $this->data['judul_browser'] = 'Register';
		$this->data['judul_utama'] = 'error';
		$this->data['judul_sub'] = '';		
		$this->data['isi'] = '<div class="alert alert-danger">Anda tidak memiliki Akses.</div>';
		$this->load->view('themes/layout_utama_v', $this->data);
		
	}
 	function cetak_laporan2() {
		$data_pungli  = $this->saber_m->get_data_cetak();
		//$this->data['gtc'] = $this->home_m->get_sum('gtc');
	    $total = $this->saber_m->get_jml();
		print_r($total->nilai);
		//print_r($total);
	}
	function cetak_laporan_m() {
		$data_pungli  = $this->saber_m->get_data_cetak_m();
		$total = $this->saber_m->get_jml_m();
		$total = $total->nilai;
		$area = $_REQUEST['area'];
		$kab = $_REQUEST['kab'];
		if ($kab != '')
		{
		$kab = '<span class="txt_judul"> '.($kab).'<br></span>';
		}
		else
		{
		$kab = "";	
		}
		$pr = '<span class="txt_judul"> '.$this->session->userdata('propinsi').'<br></span>';
		if($area != '')
		{
		$nmarea = $this->area_m->get_area($area);	
	    $area = $nmarea[0]->nama;	
		$area = '<span class="txt_judul"> AREA '.($area).'<br></span>';
		}
		else
		{
		$area = "";			
		}
			
		if($data_pungli == FALSE) {
			echo 'DATA KOSONG<br>Pastikan Filter Tanggal dengan benar.';
			exit();
		}

		 		       
		$this->load->library('Pdf');
		$pdf = new Pdf('L', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->set_nsi_header(TRUE);
		$pdf->AddPage('L');
		$html = '';
		$html .= '
		<style>
			.h_tengah {text-align: center;}
			.h_kiri {text-align: left;}
			.h_kanan {text-align: right;}
			.txt_judul {font-size: 15pt; font-weight: bold; padding-bottom: 12px;}
			.header_kolom {background-color: #cccccc; text-align: center; font-weight: bold;}
		</style>
		'.$pdf->nsi_box($text = '<span class="txt_judul">LAPORAN DATA PENGADUAN<br></span>'.($pr).''.($kab).''.($area).'<br>', $width = '100%', $spacing = '0', $padding = '1', $border = '0', $align = 'center').'
		<table width="100%" cellspacing="0" cellpadding="3" border="1" nobr="true">
			<tr class="header_kolom">
				<th style="width:3%;" > No </th>
				<th style="width:10%;"> Tgl lapor </th>
				<th style="width:10%;"> Tgl Kejadian </th>
				<th style="width:14%;"> Kegiatan </th>
				<th style="width:13%;"> SKPD  </th>				
				<th style="width:14%;"> Pelaku</th>
				<th style="width:14%;"> Nilai </th>
				<th style="width:13%;"> Modus </th>
				<th style="width:8%;"> Pelapor </th>
			</tr>';
		$no =1;
		//$rows   = array(); 
		foreach ($data_pungli as $r) {
			$id = $r['id'];	
            $tgl_lapor = date('d-M-Y', strtotime($r['tgl_masuk']));
            $tgl_kejadian = date('d-M-Y', strtotime($r['tgl_lapor']));		
			$html .= '
			<tr>
			<td align="center">'.$no.'</td>
			<td>'.$tgl_lapor.'</td>
			<td>'.$tgl_kejadian.'</td>
			<td align="right"> '.$r['kegiatan'].'</td>
			<td align="right">'.$r['skpd'].'</td>
			<td align="right">'.$r['pelaku'].'</td>
			<td align="right">'.number_format((float)$r['nilai'], 2, '.', '').'</td>
			<td>'.$r['modus'].'</td>
			<td>'.$r['uname'].'</td>
			</tr>';
		$no++;
			}
			$html .= '
			<tr>
			<td align="center" colspan="4">Total Nilai dalam rupiah</td>
			<td></td>
		    <td align="right" colspan="3">Rp '.number_format((float)$total, 2, '.', '').'</td>
			
			</tr>';
			
			
		$html .='</table>';
		
		$pdf->nsi_html($html);
		$pdf->Output('pinjam'.date('Ymd_His') . '.pdf', 'I');
		error_log($total);
		
	}
	function cetak_laporan() {
		$data_pungli  = $this->saber_m->get_data_cetak();
		$total = $this->saber_m->get_jml();
		$total = $total->nilai;
		$area = $_REQUEST['area'];
		if($area != '')
		{
		$nmarea = $this->area_m->get_area($area);	
	    $area = $nmarea[0]->nama;		
		$area = '<span class="txt_judul"> Area : '.($area).'<br></span>';
		}
		else
		{
		$area = "";			
		}		

		if($data_pungli == FALSE) {
			echo 'DATA KOSONG<br>Pastikan Filter Tanggal dengan benar.';
			exit();
		}

		//$area = $_REQUEST['area']; 
		$pr = $_REQUEST['pr']; 
		if ($pr != '')
		{
		$pr = '<span class="txt_judul">'.($pr).'<br></span>';	
		}
		else
		{
		$pr = ""; 	
		}
		$kab = $_REQUEST['kab']; 
		
		   if($kab != '')			
			{
			$kab = '<span class="txt_judul">'.($kab).'<br></span>';	 	
			}
			else{
			$kab = ''; 
			}
		       
		$this->load->library('Pdf');
		$pdf = new Pdf('L', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->set_nsi_header(TRUE);
		$pdf->AddPage('L');
		$html = '';
		$html .= '
		<style>
			.h_tengah {text-align: center;}
			.h_kiri {text-align: left;}
			.h_kanan {text-align: right;}
			.txt_judul {font-size: 15pt; font-weight: bold; padding-bottom: 12px;}
			.header_kolom {background-color: #cccccc; text-align: center; font-weight: bold;}
		</style>
		'.$pdf->nsi_box($text = '<span class="txt_judul">LAPORAN DATA PENGADUAN<br></span> '.($pr).''.($kab).''.$area.'', $width = '100%', $spacing = '0', $padding = '1', $border = '0', $align = 'center').'
		<table width="100%" cellspacing="0" cellpadding="3" border="1" nobr="true">
			<tr class="header_kolom">
				<th style="width:3%;" > No </th>
				<th style="width:10%;"> Tgl lapor </th>
				<th style="width:10%;"> Tgl Kejadian </th>
				<th style="width:14%;"> Kegiatan </th>
				<th style="width:13%;"> SKPD  </th>				
				<th style="width:14%;"> Pelaku</th>
				<th style="width:14%;"> Nilai </th>
				<th style="width:13%;"> Modus </th>
				<th style="width:8%;"> Pelapor </th>
			</tr>';
		$no =1;
		//$rows   = array(); 
		foreach ($data_pungli as $r) {
			$id = $r['id'];	
            $tgl_lapor = date('d-M-Y', strtotime($r['tgl_masuk']));
            $tgl_kejadian = date('d-M-Y', strtotime($r['tgl_lapor']));			
			$html .= '
			<tr>
			<td align="center">'.$no.'</td>
			<td>'.$tgl_lapor.'</td>
			<td>'.$tgl_kejadian.'</td>
			<td align="right"> '.$r['kegiatan'].'</td>
			<td align="right">'.$r['skpd'].'</td>
			<td align="right">'.$r['pelaku'].'</td>
			<td align="right">'.number_format((float)$r['nilai'], 2, '.', '').'</td>
			<td>'.$r['modus'].'</td>
			<td>'.$r['uname'].'</td>
			</tr>';
		$no++;
			}
			$html .= '
			<tr>
			<td align="center" colspan="4">Total Nilai dalam rupiah</td>
			<td></td>
		    <td align="right" colspan="3">Rp '.number_format((float)$total, 2, '.', '').'</td>
			
			</tr>';
			
			
		$html .='</table>';
		
		$pdf->nsi_html($html);
		$pdf->Output('pinjam'.date('Ymd_His') . '.pdf', 'I');
		//error_log($total);
	} 	
	
	function check_exist($field=null){
		$dt = $_POST[$field];
		//$dt = $_POST['username'];
		$chk = $this->register_m->select_field('member', $field, $field, $dt);
		$cnt_chk = count($chk);		
		echo $cnt_chk;
		//echo json_encode(array('ok' => false, 'msg' => '<div class="text-red"><i class="fa fa-ban"></i> '$cnt_chk.'</strong>. </div>'));

	}
	function test($field=null){
		    $total = $this->saber_m->lap_total();
		error_log($total);
	    // $dt = $_POST['username'];
		// $cnt_chk=0;
		// $chk = $this->register_m->select_field('upline', $field, 'username', $dt);		
		// if($chk[0] !="") 
			// $cnt_chk =1;		
		// echo $cnt_chk;
		//error_log($this->session->userdata('kabupaten'));
		//error_log($this->session->userdata('propinsi'));
		
	}
	
	function check_kanan($field=null){
		$dt = $_POST[$field];
		$posisi = "kanan";
		$chk = $this->register_m->select_field('upline', $posisi, $posisi, $dt);
		$cnt_chk = count($chk);		
		
		echo $cnt_chk;
	}
	
	function sendEmail($from=null, $pass=null, $to=null, $msg=null, $company=null){
				
		$sub = "Informasi Pendaftaran";
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_timeout' => 5,
            'smtp_user' => $from,
            'smtp_pass' => $pass,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'crlf' => "\r\n",
            'newline' => "\r\n",
			'priority' => '1'           
        );	
        
        $this->email->initialize($config);
        $this->email->from($from, $company);
        $this->email->to($to);
		$this->email->reply_to('noreply@cca.com', 'samurai.co.id');
        $this->email->subject($sub);
        $this->email->message($msg);
		
		/**
		if (!$this->email->send()){
        $data['message'] ="Email not sent \n".$this->email->print_debugger();      
		} else{
		  $data['message'] ="Email was successfully sent to $to";      
		}
		print_r($data);
		**/
		
        return $this->email->send();      
    }		
	public function no_akses() {
		$this->data['judul_browser'] = 'Tidak Ada Akses';
		$this->data['judul_utama'] = 'Tidak Ada Akses';
		$this->data['judul_sub'] = '';
		$this->data['isi'] = '<div class="alert alert-danger">Anda tidak memiliki Akses.</div>';
		$this->load->view('themes/layout_utama_v', $this->data);
	}
	
	

}
