<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About extends AdminController {

	public function __construct() {
		parent::__construct();	
	}	
	
	public function index() {
		$this->data['judul_browser'] = 'Aplikasi';
		$this->data['judul_utama'] = 'About';
		$this->data['judul_sub'] = 'About';

		$this->output->set_template('gc');
		//$crud->set_theme('twitter-bootstrap');

		$this->load->library('grocery_CRUD');
		$crud = new grocery_CRUD();
		$crud->set_table('about');
       //$crud->unset_operations();
		$crud->set_subject('Tentang');		
		$crud->columns('tentang');
		$crud->fields('tentang');
		
		//$crud->unset_fields(array('id'))
		//$crud->unset_columns(array('id'));
		$crud->required_fields('tentang');
	   	$crud->unset_read();
		$output = $crud->render();

		$out['output'] = $this->data['judul_browser'];
		$this->load->section('judul_browser', 'default_v', $out);
		$out['output'] = $this->data['judul_utama'];
		$this->load->section('judul_utama', 'default_v', $out);
		$out['output'] = $this->data['judul_sub'];
		$this->load->section('judul_sub', 'default_v', $out);
		$out['output'] = $this->data['u_name'];
		$this->load->section('u_name', 'default_v', $out);
		$this->load->view('default_v', $output);		

	}

	function _set_password_input_to_empty() {
		return "<input type='password' name='pass_word' value='' /><br />Kosongkan password jika tidak ingin ubah/isi.";
	}

	function _encrypt_password_callback($post_array) {
		if(!empty($post_array['pass_word'])) {
			$post_array['pass_word'] = sha1('nsi' . $post_array['pass_word']);
		} else {
			unset($post_array['pass_word']);
		}
		return $post_array;
	}

}
