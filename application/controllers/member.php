<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Controller {

	public $data = array ('pesan' => '');
	
	public function __construct () {
		parent::__construct();
		//$this->output->enable_profiler(TRUE);
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('Member_m','member', TRUE);
		$this->load->model('home_m');	
	}

	public function _cek_login() {
		if ($this->session->userdata('login') == FALSE && $this->session->userdata('level') == 'member') {
			redirect('member');
		}
	}
	
	public function index() {
		// status user login = BENAR, pindah ke halaman home
		if ($this->session->userdata('login') == TRUE && $this->session->userdata('level') == 'member') {
			redirect('member/view');
		} else {
			// status login salah, tampilkan form login
			// validasi sukses
			if($this->member->validasi()) {
				// cek di database sukses
				if($this->member->cek_user() == 'ok') {
					redirect('member/view');
				} else {
					// error_log($this->member->cek_user());
					$this->data['pesan'] = $this->member->cek_user();				
				}
			} else {
				// validasi gagal
         }
		 
         $this->data['jenis'] = 'member';
         $this->load->view('themes/login_form_v', $this->data);
		}
	}

	public function view() {
		$this->data['judul_browser'] = 'Beranda';
		$this->data['judul_utama'] = 'Beranda';
		$this->data['judul_sub'] = 'Menu Utama';
		
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';
		//$this->data['js_files'][] = base_url() . 'assets/easyui/datagrid-detailview.js';

		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';
		#include seach
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';
		$this->data['js_files2'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.js';
		
		$this->data['css_files'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.css';
		
		
		$this->data['cash'] = $this->home_m->get_sum('cash'); //cash point		
		$this->data['shop'] = $this->home_m->get_sum('shop'); //shoping point		
		$this->data['post'] = $this->home_m->get_sum('post'); //posting point		
		$this->data['active'] = $this->home_m->get_sum('actv'); //active point		
		$this->data['register'] = $this->home_m->get_sum('rgtr'); //register point		
		$this->data['reseller'] = $this->home_m->get_sum('rsel'); //reseller point		
		$this->data['redemption'] = $this->home_m->get_sum('rdmp'); //redemption point		
		$this->data['gtc'] = $this->home_m->get_sum('gtc'); //gtc1 point
        $this->data['gtc_unit'] = "0"; //gtc1 point		
		$tgl = date('Y-m-d');
		$rate = $this->home_m->get_rate($tgl);
		$this->data['rate'] = isset($rate->rate) ? $rate->rate/100 : '00';
		
		$this->data['member'] = $this->home_m->get_anggota_all(); //member		
		
		$this->data['isi'] = $this->load->view('home_list_m', $this->data, TRUE);
		$this->load->view('themes/layout_utama_m', $this->data);
	}

	
	public function logout() {
		$this->member->logout();
		redirect('member');
	}
}