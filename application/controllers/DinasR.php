<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DinasR extends AdminController {

	public function __construct() {
		parent::__construct();	
		//$this->load->model('dept_m');
    }
     public function index() {
         redirect(site_url('home'));
     } 
    
    public function indexKab() {
		$this->data['judul_browser'] = 'Data';
		$this->data['judul_utama'] = 'SPM Kabupaten';
	  
		$this->output->set_template('gc');

		$this->load->library('grocery_CRUD');
		$crud = new grocery_CRUD();
        $crud->set_table('tbl_dinas');		
		
		$kab = $this->uri->segment(3);
		
		$kab = explode("-", $kab);
        $kab = $kab[1];
		//error_log($kab); 
		 
		$tahun = $this->uri->segment(4);
		$din = $this->uri->segment(5);
		
		//$prov = str_replace('%20',' ', $prop);
				
		
		if($din == "SOSIAL")
		{
			$dinas = "1";
		}
		else if ($din == "KESEHATAN")
		{
		   $dinas = "2";	
		}	
		else if ($din == "TRANTIBUMLINMAS")
		{
		   $dinas = "4";	
		}
		else if ($din == "PENDIDIKAN")
		{
		  $dinas = "5";	
		}	
		
		else if ($din == "PU")
		{
		$dinas = "6";	
			
		}
       else
	   {
		  $dinas = "3"; 
	    }			
       
		$crud->set_subject('Data Dinas Kabupaten/Kota');        	   
		$crud->columns('dinas','id_kabkot','tahun','flag_generated');		
        $crud->fields('dinas','id_prov','id_kabkot','tahun','wilayah');  
        //$crud->field_type('id_tr_dinas','hidden','id_tr_dinas');
        $crud->callback_add_field('wilayah',array($this,'wilayah_prov_callback'));        
        $crud->set_relation('dinas', 'tbl_master_dinas', 'dinas');
        
        $crud->display_as('dinas','Dinas');
        $crud->display_as('id_prov','Provinsi');
        $crud->display_as('id_kabkot','Kabupaten');
        $crud->display_as('flag_generated','status Laporan');
        $crud->display_as('id_tr_dinas','ID');
        
		$user = substr($this->session->userdata('u_name'), 0, 2);       	
		$crud->set_relation('id_kabkot', 'kab', 'nama_kabkot');
		
      

        $crud->add_action('detail laporan', '', '','glyphicon glyphicon-list',array($this,'generate_reportprov'));
        $crud->add_action('lihat dokumen', '', '','glyphicon glyphicon-file',array($this,'view_dokument'));
		
		$crud->where('wilayah', 'Kabupaten/Kota');
       	$crud->where('tbl_dinas.dinas',$dinas);
        $crud->where('tbl_dinas.tahun',$tahun);		
        $crud->where('tbl_dinas.id_kabkot',$kab);			
		
     
        $crud->unset_add();
        $crud->unset_edit();
		$crud->unset_delete();     
        $output = $crud->render();
        
        
		$out['output'] = $this->data['judul_browser'];
		$this->load->section('judul_browser', 'default_v', $out);
		$out['output'] = $this->data['judul_utama'];
		$this->load->section('judul_utama', 'default_v', $out);
		$out['output'] = $this->data['judul_sub'];
		$this->load->section('judul_sub', 'default_v', $out);
		$out['output'] = $this->data['u_name'];
		$this->load->section('u_name', 'default_v', $out);
		$this->load->view('default_v', $output);

    }
    
    function generate_reportkab($primary_key , $row){

        if($row->flag_generated=="aktif"){
            return site_url('view_indikator/editIndikator').'?dinasid='.$row->id_tr_dinas.'&wilayah=kabkota&dinas='.$row->dinas;
        }
        else{
            return site_url('view_indikator/index').'?dinasid='.$row->id_tr_dinas.'&wilayah=kabkota&dinas='.$row->dinas;
        }
        
    }

    function view_dokument($primary_key , $row){

        return site_url('view_indikator/UploadDokumen').'?dinasid='.$row->id_tr_dinas;
        
    }

    function generate_reportprov($primary_key , $row){

        if($row->flag_generated=="aktif"){
            return site_url('view_indikator/editindikator').'?dinasid='.$row->id_tr_dinas.'&wilayah=provinsi&dinas='.$row->dinas;
        }
        else{
            return site_url('view_indikator/index').'?dinasid='.$row->id_tr_dinas.'&wilayah=provinsi&dinas='.$row->dinas;
        }
        
    }

    public function indexProv2() {
		$this->data['judul_browser'] = 'Data';
		$this->data['judul_utama'] = 'SPM Provinsi';
	  
		$this->output->set_template('gc');

		$this->load->library('grocery_CRUD');
		$crud = new grocery_CRUD();
        $crud->set_table('tbl_dinas');	
        //$this->db->join('user_log','user_log.user_id = users.id');		
		
        $prov = $this->uri->segment(3);
		
		// $prov = explode("-", $prov);
        // $prov = $prov[1];
		 
		 
		$tahun = $this->uri->segment(4);
		$din = $this->uri->segment(5);
		
		
		if($din == "SOSIAL")
		{
			$dinas = "1";
		}
		else if ($din == "KESEHATAN")
		{
		   $dinas = "2";	
		}	
		else if ($din == "TRANTIBUMLINMAS")
		{
		   $dinas = "4";	
		}
		else if ($din == "PENDIDIKAN")
		{
		  $dinas = "5";	
		}	
		
		else if ($din == "PU")
		{
		$dinas = "6";	
			
		}
       else
	   {
		  $dinas = "3"; 
	    }
		
		$crud->set_subject('Data Dinas Provinsi');
        	   
		$crud->columns('dinas','tahun','flag_generated','status_dokumen','id_prov');
        $crud->fields('dinas','id_prov','tahun','wilayah');
        //$crud->field_type('wilayah','hidden','Provinsi');
        $crud->callback_add_field('wilayah',array($this,'wilayah_prov_callback'));
        $crud->set_relation('dinas', 'tbl_master_dinas', 'dinas'); 		
        $crud->display_as('dinas','Dinas');
        $crud->display_as('id_prov','Provinsi');
        $crud->display_as('flag_generated','status Laporan');
        $crud->display_as('id_tr_dinas','ID');
        
		$user = substr($this->session->userdata('u_name'), 0, 2);
       	$crud->field_type('id_prov', 'hidden',$user);
        $crud->set_relation('id_prov', 'prop', 'nama_prov');
       

        $crud->add_action('detail laporan', '', '','glyphicon glyphicon-list',array($this,'generate_reportprov'));
        $crud->add_action('lihat dokumen', '', '','glyphicon glyphicon-file',array($this,'view_dokument'));
		
		if($this->session->userdata('level') == 'ADMINPROV')
        {
		$user = substr($this->session->userdata('u_name'), 0, 2);	
		$crud->where('tbl_dinas.id_prov',$user);
		$crud->where('wilayah', 'Provinsi');	
		//$crud->like('tbl_master_dinas.dinas',$dinas);
        $crud->where('tbl_dinas.tahun',$tahun);
		}else if ($this->session->userdata('level') == 'SUBDIT')
		{
		$crud->where('tbl_dinas.dinas',$this->session->userdata('dinas_id'));
		$crud->where('wilayah', 'Provinsi');	
        $crud->like('prop.nama_prov',$dinas);
        $crud->where('tbl_dinas.tahun',$tahun);		
		}	
		
		else
		{		 
		 $crud->where('wilayah', 'Provinsi');
		 $crud->like('tbl_dinas.dinas',$dinas);
         $crud->where('tbl_dinas.id_prov',$prov);
         $crud->where('tbl_dinas.tahun',$tahun);
		 
          		 
		}
		
		
        
		if($this->session->userdata('level') == 'SUBDIT' OR $this->session->userdata('level') == 'admin')
        {
        $crud->unset_add();
        $crud->unset_edit();
		$crud->unset_delete();
		}
        //custom action Button
        $crud->unset_read();        
        //endregion
        $output = $crud->render();
        
        
		$out['output'] = $this->data['judul_browser'];
		$this->load->section('judul_browser', 'default_v', $out);
		$out['output'] = $this->data['judul_utama'];
		$this->load->section('judul_utama', 'default_v', $out);
		$out['output'] = $this->data['judul_sub'];
		$this->load->section('judul_sub', 'default_v', $out);
		$out['output'] = $this->data['u_name'];
		$this->load->section('u_name', 'default_v', $out);
		$this->load->view('default_v', $output);	

    }
    public function indexProv() {
		$this->data['judul_browser'] = 'Data';
		$this->data['judul_utama'] = 'SPM Provinsi';
	  
		$this->output->set_template('gc');

		$this->load->library('grocery_CRUD');
		$crud = new grocery_CRUD();
        $crud->set_table('tbl_dinas');	
        //$this->db->join('user_log','user_log.user_id = users.id');		
		
        $prov = $this->uri->segment(3);
		
		$prov = explode("-", $prov);
        $prov = $prov[1];
		 
		 
		$tahun = $this->uri->segment(4);
		$din = $this->uri->segment(5);
		
		
				if($din == "SOSIAL")
		{
			$dinas = "1";
		}
		else if ($din == "KESEHATAN")
		{
		   $dinas = "2";	
		}	
		else if ($din == "TRANTIBUMLINMAS")
		{
		   $dinas = "4";	
		}
		else if ($din == "PENDIDIKAN")
		{
		  $dinas = "5";	
		}	
		
		else if ($din == "PU")
		{
		$dinas = "6";	
			
		}
       else
	   {
		  $dinas = "3"; 
	    }
		
		$crud->set_subject('Data Dinas Provinsi');
        	   
		$crud->columns('dinas','id_prov','tahun','flag_generated','status_dokumen','id_prov');
        $crud->fields('dinas','id_prov','tahun','wilayah');
        //$crud->field_type('wilayah','hidden','Provinsi');
        $crud->callback_add_field('wilayah',array($this,'wilayah_prov_callback'));
        $crud->set_relation('dinas', 'tbl_master_dinas', 'dinas'); 		
        $crud->display_as('dinas','Dinas');
        $crud->display_as('id_prov','Provinsi');
        $crud->display_as('flag_generated','status Laporan');
        $crud->display_as('id_tr_dinas','ID');
        
		$user = substr($this->session->userdata('u_name'), 0, 2);
       	$crud->field_type('id_prov', 'hidden',$user);
        $crud->set_relation('id_prov', 'prop', 'nama_prov');
       

        $crud->add_action('detail laporan', '', '','glyphicon glyphicon-list',array($this,'generate_reportprov'));
        $crud->add_action('lihat dokumen', '', '','glyphicon glyphicon-file',array($this,'view_dokument'));
		
		if($this->session->userdata('level') == 'ADMINPROV')
        {
		$user = substr($this->session->userdata('u_name'), 0, 2);	
		$crud->where('tbl_dinas.id_prov',$user);
		$crud->where('wilayah', 'Provinsi');	
		//$crud->like('tbl_master_dinas.dinas',$dinas);
        $crud->where('tbl_dinas.tahun',$tahun);
		}else if ($this->session->userdata('level') == 'SUBDIT')
		{
		$crud->where('tbl_dinas.dinas',$this->session->userdata('dinas_id'));
		$crud->where('wilayah', 'Provinsi');	
        $crud->like('prop.nama_prov',$dinas);
        $crud->where('tbl_dinas.tahun',$tahun);		
		}	
		
		else
		{		 
		 $crud->where('wilayah', 'Provinsi');
		 $crud->like('tbl_dinas.dinas',$dinas);
         $crud->where('tbl_dinas.id_prov',$prov);
         $crud->where('tbl_dinas.tahun',$tahun);
		 
          		 
		}
		
		
        
		if($this->session->userdata('level') == 'SUBDIT' OR $this->session->userdata('level') == 'admin')
        {
        $crud->unset_add();
        $crud->unset_edit();
		$crud->unset_delete();
		}
        //custom action Button
        $crud->unset_read();        
        //endregion
        $output = $crud->render();
        
        
		$out['output'] = $this->data['judul_browser'];
		$this->load->section('judul_browser', 'default_v', $out);
		$out['output'] = $this->data['judul_utama'];
		$this->load->section('judul_utama', 'default_v', $out);
		$out['output'] = $this->data['judul_sub'];
		$this->load->section('judul_sub', 'default_v', $out);
		$out['output'] = $this->data['u_name'];
		$this->load->section('u_name', 'default_v', $out);
		$this->load->view('default_v', $output);	

    }

    function wilayah_prov_callback()
    {
        return '<input type="text" maxlength="50" readonly="readonly" value="Provinsi" name="wilayah" style="width:462px">';
    }

    function status_dokumen_callback()
    {
        return '<input type="text" maxlength="50" readonly="readonly" value="belum tersedia" name="status_dokumen" style="width:462px">';
    }

    function wilayah_kab_callback()
    {
        return '<input type="text" maxlength="50" readonly="readonly" value="Kabupaten/Kota" name="wilayah" style="width:462px">';
    }
    
    
}