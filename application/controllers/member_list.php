<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member_list extends MY_Controller {

	public function __construct() {
		parent::__construct();	
	}	
	
	public function index() {
		
		$this->data['judul_browser'] = 'Profile Setting';
		$this->data['judul_utama'] = 'Profile';
		$this->data['judul_sub'] = 'Anggota';	
		$this->output->set_template('gc');

		$this->load->library('grocery_CRUD');
		$crud = new grocery_CRUD();
		$this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');
		$crud->set_table('member');
		$crud->set_subject('Data Anggota');

		$crud->columns('username','nama','email','tglahir','kelamin','alamat','kota','propinsi','phone','hp','bank','norek');
		$crud->fields('id','username','password','nama','email','tglahir','kelamin','alamat','kota','propinsi','phone','hp','bank','norek','kawin','tgl','foto');		
		
		$crud->display_as('id','ID Anggota');
		$crud->display_as('username','Username');
		$crud->display_as('nama','Nama Lengkap');		
		$crud->display_as('email','Email');		
		$crud->display_as('tglahir','Tanggal Lahir');
		$crud->display_as('phone','Nomor Telepon');
		$crud->display_as('hp','HP');
		$crud->display_as('tglahir','Tanggal Lahir');		
		
		$crud->display_as('foto','Photo');
		$crud->display_as('tgl','Tanggal Aktif');
		

		$crud->set_field_upload('foto','uploads/anggota');
		$crud->callback_after_upload(array($this,'callback_after_upload'));
		$crud->callback_column('foto',array($this,'callback_column_pic'));

		$crud->required_fields('nama','username','email');
		$crud->unset_texteditor('alamat');
		$crud->field_type('id','invisible'); 
		$crud->field_type('tgl','readonly'); 
		// Dropdown 
		$crud->field_type('kelamin','dropdown',
			array('L' => 'Laki-laki','P' => 'Perempuan'));
		$crud->display_as('kelamin','Jenis Kelamin');
		
		$crud->field_type('kawin','dropdown',
			array('Belum Kawin' => 'Belum Kawin',
				'Kawin' => 'Kawin',
				'Cerai Hidup' => 'Cerai Hidup',
				'Cerai Mati' => 'Cerai Mati',
				'Lainnya' => 'Lainnya'));
		$crud->display_as('kawin','Status Perkawinan');
		//Pemangggilan field
		$crud->callback_column('id',array($this, '_kolom_id_cb'));
		$crud->callback_column('alamat',array($this, '_kolom_alamat'));
		
		
		$crud->callback_edit_field('password',array($this,'_set_password_input_to_empty'));
		$crud->callback_add_field('password',array($this,'_set_password_input_to_empty'));

		$crud->callback_before_insert(array($this,'_encrypt_password_callback'));
		$crud->callback_before_update(array($this,'_encrypt_password_callback'));

		$crud->unset_read();
		
		$output = $crud->render();
		
		$out['output'] = $this->data['judul_browser'];
		$this->load->section('judul_browser', 'default_v', $out);
		$out['output'] = $this->data['judul_utama'];
		$this->load->section('judul_utama', 'default_v', $out);
		$out['output'] = $this->data['judul_sub'];
		$this->load->section('judul_sub', 'default_v', $out);
		$out['output'] = $this->data['u_name'];
		$this->load->section('u_name', 'default_v', $out);
		$out['output'] = $this->data['id'];
		$this->load->section('id', 'default_v', $out);
		
		$this->load->view('default_v', $output);

	}

	function _set_password_input_to_empty() {
		return "<input type='password' name='pass_word' value='' /><br />Kosongkan password jika tidak ingin ubah/isi.";
	}

	function _encrypt_password_callback($post_array) {
		if(!empty($post_array['password'])) {
			$post_array['password'] = md5($post_array['password']);
		} else {
			unset($post_array['password']);
		}
		return $post_array;
	}

	function _kolom_id_cb ($value, $row) {
		$value = '<div style="text-align:center;">'.$row->id.'</div>';
		return $value;
	}	
	
	function _kolom_alamat($value, $row) {
		$value = wordwrap($value, 35, "<br />");
		return nl2br($value);
	}

	function callback_column_pic($value, $row) {
		if($value) {
			return '<div style="text-align: center;"><a class="image-thumbnail" href="'.base_url().'uploads/anggota/' . $value .'"><img src="'.base_url().'uploads/anggota/' . $value . '" alt="' . $value . '" width="30" height="40" /></a></div>';
		} else {
			return '<div style="text-align: center;"><img src="'.base_url().'assets/theme_admin/img/photo.jpg" alt="default" width="30" height="40" /></div>';
		}
	}

	function callback_after_upload($uploader_response,$field_info, $files_to_upload) {
		$this->load->library('image_moo');
        //Is only one file uploaded so it ok to use it with $uploader_response[0].
		$file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;
		$this->image_moo->load($file_uploaded)->resize(250,250)->save($file_uploaded,true);
		return true;
	}



}
