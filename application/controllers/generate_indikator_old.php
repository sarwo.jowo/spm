<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Generate_indikator extends AdminController {

	public function __construct() {
		parent::__construct();	
		$this->load->model('Generate_model');
		$this->load->helper('url');
    }

    public function index(){
        $this->data['judul_browser'] = 'Beranda';
		$this->data['judul_utama'] = 'Beranda';
		$this->data['judul_sub'] = 'Menu Utama';
		
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';
		//$this->data['js_files'][] = base_url() . 'assets/easyui/datagrid-detailview.js';

		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';
		#include seach
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';
		$this->data['js_files2'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.js';		
		$this->data['css_files'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.css';
		$level = $this->session->userdata('level');				
		$tgl = date('Y-m-d');				
        
        //logic here..
		$idtransaksi=$_REQUEST['dinasid'];
		$dinasid=$_REQUEST['dinas'];
		$wilayah=$_REQUEST['wilayah'];
		//$this->data['id']=$id;
		$dinas=$this->Generate_model->get_Dasar_Hukum($dinasid);
		$this->data['id_tr_dinas']=$idtransaksi;
		$this->data['dasarhukum']=$dinas->dasar_hukum;
		if($wilayah=='kabkota')
		{
			$this->data['wilayah']='Kabupaten/Kota';
			$wilayah='Kabupaten/Kota';
		}
		else{
			$this->data['wilayah']=$wilayah;
		}
		
        $this->data['indikator']=$this->Generate_model->get_template_indikator($dinasid,$wilayah);
        //end
        
        
		$this->data['isi'] = $this->load->view('indikator/generate_indikator_v', $this->data, TRUE);
		$this->load->view('themes/layout_utama_v', $this->data);
		
	}

	public function save_action() 
    {

    		$jumlahindikator=count($this->input->post("indikator_kinerja"));
		$wilayah=$this->input->post('wilayah',TRUE);
		//Table Indikator
		$i=0;$k=1;
		foreach($this->input->post("indikator_kinerja") as $c){

			$indikator_pencapaian=$this->input->post('indikator_pencapaian')[$i];
             
			
			$data = array(
				'id_tr_dinas' => $this->input->post('id_tr_dinas',TRUE),
				'wilayah' => $this->input->post('wilayah',TRUE),
				'indikator_kinerja' => $c,
				'indikator_pencapaian' => $indikator_pencapaian,
				);
			//
			$idnya=$this->Generate_model->insert_Indikator($data);

			//update main Table for flag report generated
			$dt = array('flag_generated'=>1);
			$idtr=$this->input->post('id_tr_dinas',TRUE);
			$this->Generate_model->updateFlagReport($idtr,$dt);
			

			//insert detail
			$j=0;
			foreach($this->input->post("itemdesc$k") as $a){
				
				$standard=$this->input->post("standard$k")[$j];				
				$total=$this->input->post("total$k")[$j];
				//$nonstandard=$this->input->post("nonstandard$k")[$j];				
				$nonstandard= $total - $standard;				
				//$nonstandard=$total-$standard;
				$target=0;
				if($total!=0 && $standard!=0){
					$target = ($standard/$total)*100;
				}
										
				//pull to Array
				$datadetail=array(
					'indikator_id'=>$idnya,
					'deskripsi_detail'=>$a,
					'standard'=>$standard,
					'nonstandard'=>$nonstandard,
					//'tidak_terlayani'=>$tidak_terlayani,
					'total'=>$total,
					'target'=> $target,
					//'persen'=> $target,
					'order_number'=>$j+1
				);

				$this->Generate_model->insert_Indikator_Detail($datadetail);
				$j++;
			}

			$i++;
			$k++;
		}		

		
		$this->session->set_flashdata('message', 'Create Record Success');
		if($wilayah=="provinsi"){
			redirect(site_url('Dinas/indexProv'));
			//redirect($_SERVER['HTTP_REFERER']);
		}
		else{
			redirect(site_url('Dinas/indexKab'));
			//redirect($_SERVER['HTTP_REFERER']);
		}
        
        
    }

	public function editIndikator()
	{
		$this->data['judul_browser'] = 'Beranda';
		$this->data['judul_utama'] = 'Beranda';
		$this->data['judul_sub'] = 'Menu Utama';
		
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';
		//$this->data['js_files'][] = base_url() . 'assets/easyui/datagrid-detailview.js';

		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';
		#include seach
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';
		$this->data['js_files2'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.js';		
		$this->data['css_files'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.css';
		$level = $this->session->userdata('level');				
		$tgl = date('Y-m-d');				
        
        //logic here..
		$idtransaksi=$_REQUEST['dinasid'];
		$dinasid=$_REQUEST['dinas'];
		$wilayah=$_REQUEST['wilayah'];
		//$this->data['id']=$id;
		$dinas=$this->Generate_model->get_Dasar_Hukum($dinasid);
		$this->data['id_tr_dinas']=$idtransaksi;
		$this->data['dasarhukum']=$dinas->dasar_hukum;
		if($wilayah=='kabkota')
		{
			$this->data['wilayah']='Kabupaten/Kota';
			$wilayah='Kabupaten/Kota';
		}
		else{
			$this->data['wilayah']=$wilayah;
		}
		
		$this->data['indikator']=$this->Generate_model->get_indikator($idtransaksi);
		
		//$this->data['indikator']=$this->Generate_model->get_indikator($idtransaksi);
		
		$this->data['indikatordetail']=$this->Generate_model->get_indikator_detail($idtransaksi);

		//error_log(serialize($total));
		$this->data['isi'] = $this->load->view('indikator/edit_indikator_v', $this->data, TRUE);
		$this->load->view('themes/layout_utama_v', $this->data);
	}
   public function saveedit_action()
	{
		$jumlahitemindikator=$this->input->post("jumlahrow",TRUE);
		$wilayah=$this->input->post('wilayah',TRUE);
		$standard=$this->input->post("standard");
		$nonstandard=$this->input->post("nonstandard");
		$total = $this->input->post("total");
		$id = $this->input->post('id');
		$target = 0;
		$result = array();
		foreach($id AS $key => $val){
		 if($_POST['standard'][$key] != 0){$target = $_POST['standard'][$key]/$_POST['total'][$key]*100;}

		$result[] = array(	
		
		  "id" => $id[$key],
		  "standard"  => $_POST['standard'][$key],
		  "nonstandard"  => $_POST['nonstandard'][$key],
		  "total"  => $_POST['total'][$key],
		  "target"  => $target   	  
		 );
		
		}
		
		$this->db->update_batch('tbl_dinas_indikator_detail', $result, 'id'); 
		error_log($sql = $this->db->last_query());
         
	   //rror_log();
		
        if($wilayah=="provinsi"){
			//redirect(site_url('Dinas/indexProv'));
			redirect($_SERVER['HTTP_REFERER']);
		}
		else{
			//redirect(site_url('Dinas/indexKab'));
			redirect($_SERVER['HTTP_REFERER']);
		}

	}
	public function saveedit_action2()
	{
		$jumlahitemindikator=$this->input->post("jumlahrow",TRUE);
		$wilayah=$this->input->post('wilayah',TRUE);
		error_log($jumlahitemindikator);

		$i=1;
		for($j=0;$j<$jumlahitemindikator;$j++)
		{
			$id = $this->input->post("id$i");
			$total = $this->input->post("total$i");
			$standard=$this->input->post("standard$i");
			$nonstandard=$this->input->post("nonstandard$i");
			$tidak_terlayani=$total - $nonstandard;
			$terlayani = $standard + $nonstandard;
			$target=0;
			if($total!=0 && $standard!=0){
				$target = ($terlayani/$total)*100;
			}
			         
		   
			$data = array(
				'total' => $total,
				'standard' => $standard,
				'nonstandard' => $nonstandard,
				'tidak_terlayani' => $tidak_terlayani,
				'target'=> $target				
				);
			error_log(serialize($j));	
		    error_log(serialize($total));
			
			$this->Generate_model->updateIndikatorDetail($id,$data);

			$i++;
		}
		
		$this->session->set_flashdata('message', 'Create Record Success');
        if($wilayah=="provinsi"){
			//redirect(site_url('Dinas/indexProv'));
			redirect($_SERVER['HTTP_REFERER']);
		}
		else{
			//redirect(site_url('Dinas/indexKab'));
			redirect($_SERVER['HTTP_REFERER']);
		}

	}

	public function UploadDokumen(){
		$this->data['judul_browser'] = 'Beranda';
		$this->data['judul_utama'] = 'Beranda';
		$this->data['judul_sub'] = 'Menu Utama';
		
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';
		//$this->data['js_files'][] = base_url() . 'assets/easyui/datagrid-detailview.js';

		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';
		#include seach
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';
		$this->data['js_files2'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.js';		
		$this->data['css_files'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.css';
		$level = $this->session->userdata('level');				
		$tgl = date('Y-m-d');				
        
        //logic here..
		$idtransaksi=$_REQUEST['dinasid'];
		$this->data['id_tr_dinas']=$idtransaksi;
		$this->data['dokument'] = $this->Generate_model->getListDokumen($idtransaksi);
		
		
		$this->data['isi'] = $this->load->view('indikator/upload_dokumen_v', $this->data, TRUE);
		$this->load->view('themes/layout_utama_v', $this->data);
	}

	public function upload(){
		
		// if(isset($_FILES['image']))
		// {
			$id_tr= $_POST['id_tr_dinas'];
			$config['upload_path'] = './assets/uploads';
			$config['allowed_types'] = 'gif|jpg|png|bmp|doc|docx|pdf';
            $config['max_size']  = 1024 * 8;
            $config['encrypt_name'] = TRUE; 
			$this->load->library('upload', $config);
			if($this->upload->do_upload('image'))
			{
				$image_data = $this->upload->data();
				//error_log($image_data['file_name']);			
				$this->Generate_model->insert_file($image_data['file_name'], $_POST['id_tr_dinas'],$_POST['Deskripsi']);			
				//update main Table for flag report generated
				$dt = array('status_dokumen'=>'tersedia');
				$this->Generate_model->updateFlagReport($id_tr,$dt);
			}
			else
			{
				$error = array('error' => $this->upload->display_errors());
			
				//print_r($error);
			}

			redirect(site_url('generate_indikator/UploadDokumen?dinasid='.$id_tr));

	//	}	
	}

	function deletefile(){
		$id = $_POST["id"];	

		$row= $this->Generate_model->get_file_name($id);
		$id_tr_dinas=$row->id_tr_dinas;
		//error_log($row->filename);
	    if(!isset($_POST)) {
			show_404();
		}
		if($this->Generate_model->delete_file()){
			 $path_to_file = './assets/uploads/'.$row->filename;
	         unlink($path_to_file);
			 error_log('sukses');
			 echo json_encode(array('ok' => true, 'msg' => '<div class="text-green"><i class="fa fa-check"></i> File berhasil di delete</div>'));

			 //update main Table for flag report generated
			 $dokumen = $this->Generate_model->getListDokumen($id_tr_dinas);
			 if(count($dokumen)==0){
				$dt = array('status_dokumen'=>'tidak tersedia');
			 	$this->Generate_model->updateFlagReport($id_tr_dinas,$dt);
			 }
			 
			 		    			 
		}else
		{
			//error_log('gagaal');
			echo json_encode(array('ok' => false, 'msg' => '<div class="text-red"><i class="fa fa-ban"></i> File gagal delete <strong></strong>. </div>'));
		}
	}

}