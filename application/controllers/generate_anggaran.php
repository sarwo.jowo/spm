<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Generate_anggaran extends AdminController
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Generate_model');
		$this->load->helper('url');
	}

	public function index()
	{
		$this->data['judul_browser'] = 'Beranda';
		$this->data['judul_utama'] = 'Beranda';
		$this->data['judul_sub'] = 'Menu Utama';

		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';
		//$this->data['js_files'][] = base_url() . 'assets/easyui/datagrid-detailview.js';

		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';
		#include seach
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';
		$this->data['js_files2'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.js';
		$this->data['css_files'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.css';
		$level = $this->session->userdata('level');
		$tgl = date('Y-m-d');

		//logic here..
		$idtransaksi = $_REQUEST['dinasid'];
		$dinasid = $_REQUEST['dinas'];
		$wilayah = $_REQUEST['wilayah'];
		//$this->data['id']=$id;
		$dinas = $this->Generate_model->get_Dasar_Hukum($dinasid);
		$this->data['id_tr_dinas'] = $idtransaksi;
		$this->data['dasarhukum'] = $dinas->dasar_hukum;
		if ($wilayah == 'kabkota') {
			$this->data['wilayah'] = 'Kabupaten/Kota';
			$wilayah = 'Kabupaten/Kota';
		} else {
			$this->data['wilayah'] = $wilayah;
		}

		$datanya = $this->Generate_model->get_anggaran_indikator($idtransaksi);
		if(count($datanya)>0){
			$this->data['indikator'] = $datanya;
		}
		else{
			$this->data['indikator'] = $this->Generate_model->get_template_indikator($dinasid, $wilayah);
		}

		//end


		$this->data['isi'] = $this->load->view('anggaran/generate_indikator_v', $this->data, TRUE);
		$this->load->view('themes/layout_utama_v', $this->data);
	}
	public function save_action()
	{

		$jumlahindikator = count($this->input->post("indikator_kinerja"));
		$wilayah = $this->input->post('wilayah', TRUE);
		//Table Indikator
		$i = 0;
		$k = 1;
		foreach ($this->input->post("indikator_kinerja") as $c) {

			$indikator_pencapaian = $this->input->post('indikator_pencapaian')[$i];
			$id_indikator = $this->input->post('id_indikator')[$i];

			$data = array(
				'id_tr_dinas' => $this->input->post('id_tr_dinas', TRUE),
				'wilayah' => $this->input->post('wilayah', TRUE),
				'indikator_kinerja' => $c,
				'id_indikator' => $id_indikator,
				'indikator_pencapaian' => $indikator_pencapaian,
			);
			//
			$idnya = $this->Generate_model->insert_Anggaran($data);

			//update main Table for flag report generated
			$dt = array('flag_generated' => 1);
			$idtr = $this->input->post('id_tr_dinas', TRUE);
			$this->Generate_model->updateFlagReport2($idtr, $dt);


			$i++;
			$k++;
		}




		$this->session->set_flashdata('message', 'Create Record Success');
		if ($wilayah == "provinsi") {
			redirect(site_url('Angg/anggaranProv'));
			//redirect($_SERVER['HTTP_REFERER']);
		} else {
			redirect(site_url('Angg/anggaranKab'));
			//redirect($_SERVER['HTTP_REFERER']);
		}
	}

	public function editIndikator()
	{
		$this->data['judul_browser'] = 'Beranda';
		$this->data['judul_utama'] = 'Beranda';
		$this->data['judul_sub'] = 'Menu Utama';

		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';
		//$this->data['js_files'][] = base_url() . 'assets/easyui/datagrid-detailview.js';

		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';
		#include seach
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';
		$this->data['js_files2'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.js';
		$this->data['css_files'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.css';
		$level = $this->session->userdata('level');
		$tgl = date('Y-m-d');

		//logic here..
		$idtransaksi = $_REQUEST['dinasid'];
		$dinasid = $_REQUEST['dinas'];
		$wilayah = $_REQUEST['wilayah'];
		//$this->data['id']=$id;
		$dinas = $this->Generate_model->get_Dasar_Hukum($dinasid);
		$this->data['id_tr_dinas'] = $idtransaksi;
		$this->data['dasarhukum'] = $dinas->dasar_hukum;
		if ($wilayah == 'kabkota') {
			$this->data['wilayah'] = 'Kabupaten/Kota';
			$wilayah = 'Kabupaten/Kota';
		} else {
			$this->data['wilayah'] = $wilayah;
		}

		$this->data['indheader'] = $this->Generate_model->get_anggaranDistinct($idtransaksi);
		$this->data['indikator'] = $this->Generate_model->get_anggaran($idtransaksi);



		//error_log(serialize($total));
		$this->data['isi'] = $this->load->view('anggaran/edit_indikator_v', $this->data, TRUE);
		$this->load->view('themes/layout_utama_v', $this->data);
	}
	public function saveedit_action()
	{
		//die(var_dump($_POST)); //debug post
		
		$jumlahitemindikator = $this->input->post("jumlahrow", TRUE);
		$wilayah = $this->input->post('wilayah', TRUE);
		$kegiatan = $this->input->post("kegiatan");
		$indikator_kinerja = $this->input->post("indikator_kinerja");
		$indikator_pencapaian = $this->input->post("indikator_pencapaian");
		$pagu = $this->input->post("pagu");
		$targetang = $this->input->post("targetang");
		$capaian = $this->input->post("capaian");
		$indikator_id = $this->input->post('indikator_id');
		$id_tr_dinas=$this->input->post('id_tr_dinas');
		//$target = 0;
		$result = array();
		$resultnew = array();
		$a = 0;
		$i =0;
		foreach ($indikator_id as $key => $val) {
			// if($_POST['standard'][$key] != 0){$target = $_POST['standard'][$key]/$_POST['total'][$key]*100;}

			if ($indikator_id[$key] == 0) {

				$resultnew[] = array(

					"indikator_id" => $indikator_id[$key],
					"id_indikator" => $_POST['id_indikator'][$key],
					"indikator_kinerja" => $_POST['ik'][$key],
					"indikator_pencapaian" => $_POST['ip'][$key],
					"kegiatan"  => $_POST['kegiatan'][$key],
					"pagu"  => $_POST['pagu'][$key],
					"wilayah" => $wilayah,
					"id_tr_dinas"=>$id_tr_dinas,
					"targetang"  => $_POST['targetang'][$key],
					"capaian"  => ($_POST['targetang'][$key]/$_POST['pagu'][$key])*100
				);
				$a++;
			} else {

				$result[] = array(
				 // $target=0;
				// if($total!=0 && $standard!=0){
					// $target = ($terlayani/$total)*100;
				// }

					"indikator_id" => $indikator_id[$key],
					"id_indikator" => $_POST['id_indikator'][$key],
					"kegiatan"  => $_POST['kegiatan'][$key],
					"pagu"  => $_POST['pagu'][$key],
					"targetang"  => $_POST['targetang'][$key],
					"capaian"  => ($_POST['targetang'][$key]/$_POST['pagu'][$key])*100
				);
			}

			$i++;
		}

		$this->db->update_batch('tbl_anggaran_indikator', $result, 'indikator_id');
		if ($a > 0) {
			//$sql = $this->generate_model->insertBulk_Anggaran($resultnew);
			$sql = $this->db->insert_batch('tbl_anggaran_indikator', $resultnew);
			if ($sql) { // Jika sukses
				echo "<script>alert('Data berhasil disimpan');window.location = '" . base_url('index.php/siswa') . "';</script>";
			} else { // Jika gagal
				echo "<script>alert('Data gagal disimpan');window.location = '" . base_url('index.php/siswa/form') . "';</script>";
			}
		}


		error_log($sql = $this->db->last_query());

		//rror_log();

		if ($wilayah == "provinsi") {
			//redirect(site_url('Dinas/indexProv'));
			redirect($_SERVER['HTTP_REFERER']);
		} else {
			//redirect(site_url('Dinas/indexKab'));
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	function deletefile()
	{
		$id = $_POST["id"];

		$row = $this->Generate_model->get_file_name($id);
		$id_tr_dinas = $row->id_tr_dinas;
		//error_log($row->filename);
		if (!isset($_POST)) {
			show_404();
		}
		if ($this->Generate_model->delete_file()) {
			$path_to_file = './assets/uploads/' . $row->filename;
			unlink($path_to_file);
			error_log('sukses');
			echo json_encode(array('ok' => true, 'msg' => '<div class="text-green"><i class="fa fa-check"></i> File berhasil di delete</div>'));

			//update main Table for flag report generated
			$dokumen = $this->Generate_model->getListDokumen($id_tr_dinas);
			if (count($dokumen) == 0) {
				$dt = array('status_dokumen' => 'tidak tersedia');
				$this->Generate_model->updateFlagReport($id_tr_dinas, $dt);
			}
		} else {
			//error_log('gagaal');
			echo json_encode(array('ok' => false, 'msg' => '<div class="text-red"><i class="fa fa-ban"></i> File gagal delete <strong></strong>. </div>'));
		}
	}
}
