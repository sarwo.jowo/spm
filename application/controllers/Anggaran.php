<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Anggaran extends AdminController {

	public function __construct() {
		parent::__construct();	
		//$this->load->model('dept_m');
    }
     public function index() {
         redirect(site_url('home'));
     } 
    
	function format_num($value, $row){
      return number_format($value, 0);
    }
    public function anggaranKab() {
		$this->data['judul_browser'] = 'Data';
		$this->data['judul_utama'] = 'Anggaran Untuk Kabupaten/Kota';
	  
		$this->output->set_template('gc');

		$this->load->library('grocery_CRUD');
		$crud = new grocery_CRUD();
        $crud->set_table('tbl_anggaran');
        $crud->where('wilayah', 'Kabupaten/Kota');
		$crud->set_subject('Data Anggaran Kabupaten/Kota');
        $crud->display_as('tahun','Tahun Mulai');
		$crud->display_as('tahun2','Tahun Akhir');	   
		$crud->columns('dinas','tahun','anggaran');
       
        $crud->field_type('wilayah', 'hidden');		
        $crud->callback_add_field('wilayah',array($this,'wilayah_kab_callback'));
        $crud->set_relation('dinas', 'tbl_master_dinas', 'dinas');
        $crud->display_as('dinas','Dinas');
        $crud->display_as('id_prov','Provinsi');
        $crud->display_as('id_kabkot','Kabupaten');
        $crud->display_as('id_tr_dinas','ID');
		
		
		if($this->session->userdata('level') == 'ADMINPROV')
        {	
	    $crud->where('tbl_anggaran.id_prov',$this->session->userdata('id_prov'));
		$crud->where('wilayah', 'Kabupaten/Kota');	
		$crud->fields('dinas','id_kabkot','tahun','anggaran','wilayah'); 
	    }
		
		
		if($this->session->userdata('level') == 'ADMINKAB')
        {	
	    $crud->where('tbl_anggaran.id_kabkot',$this->session->userdata('u_name'));
		$crud->where('wilayah', 'Kabupaten/Kota');	
		$crud->fields('dinas','id_kabkot','tahun','anggaran','wilayah'); 
	    }
		
		
		
        if($this->session->userdata('level') == 'SUBDIT' OR $this->session->userdata('level') == 'admin' OR $this->session->userdata('level') == 'ADMINPROV' )
        {
        $crud->unset_add();
        $crud->unset_edit();
		$crud->unset_delete();
		}
		
        $crud->set_relation('id_prov', 'prop', 'nama_prov');
        $crud->set_relation('id_kabkot', 'kab', 'nama_kabkot');

         $output = $crud->render();
      //  $output->output.= $js;

        if($this->session->userdata('level') == 'SUBDIT' OR $this->session->userdata('level') == 'admin')
        {
        $crud->unset_add();
        $crud->unset_edit();
		$crud->unset_delete();
		}
		$crud->callback_column('anggaran',array($this,'format_num'));
		
		$out['output'] = $this->data['judul_browser'];
		$this->load->section('judul_browser', 'default_v', $out);
		$out['output'] = $this->data['judul_utama'];
		$this->load->section('judul_utama', 'default_v', $out);
		$out['output'] = $this->data['judul_sub'];
		$this->load->section('judul_sub', 'default_v', $out);
		$out['output'] = $this->data['u_name'];
		$this->load->section('u_name', 'default_v', $out);
		$this->load->view('default_v', $output);	

    }
	
	
    
    function generate_reportkab($primary_key , $row){

        if($row->flag_generated=="aktif"){
            return site_url('generate_indikator/editIndikator').'?dinasid='.$row->id_tr_dinas.'&wilayah=kabkota&dinas='.$row->dinas;
        }
        else{
            return site_url('generate_indikator/index').'?dinasid='.$row->id_tr_dinas.'&wilayah=kabkota&dinas='.$row->dinas;
        }
        
    }

    function view_dokument($primary_key , $row){

        return site_url('generate_indikator/UploadDokumen').'?dinasid='.$row->id_tr_dinas;
        
    }

    function generate_reportprov($primary_key , $row){

        if($row->flag_generated=="aktif"){
            return site_url('generate_indikator/editindikator').'?dinasid='.$row->id_tr_dinas.'&wilayah=provinsi&dinas='.$row->dinas;
        }
        else{
            return site_url('generate_indikator/index').'?dinasid='.$row->id_tr_dinas.'&wilayah=provinsi&dinas='.$row->dinas;
        }
        
    }

    public function anggaranProv() {
		$this->data['judul_browser'] = 'Data';
		$this->data['judul_utama'] = 'Anggaran Provinsi';
	  
		$this->output->set_template('gc');

		$this->load->library('grocery_CRUD');
		$crud = new grocery_CRUD();
        $crud->set_table('tbl_anggaran');
        $crud->where('wilayah', 'Provinsi');
		$crud->set_subject('Anggaran Provinsi');
        	   
		$crud->columns('dinas','tahun','anggaran');
        $crud->fields('dinas','id_prov','tahun','anggaran','program','kegiatan','sub_kegiatan','wilayah');
        $crud->field_type('wilayah','hidden','Provinsi');
        $crud->callback_add_field('wilayah',array($this,'wilayah_prov_callback'));        
        $crud->set_relation('dinas', 'tbl_master_dinas', 'dinas');
        
        $crud->display_as('dinas','Dinas');
		$crud->display_as('anggaran','anggaran (Rp)');
	
		$crud->display_as('tahun','Tahun Anggaran');
		
        $crud->display_as('id_prov','Provinsi');
      //  $crud->display_as('flag_generated','status Laporan');
        $crud->display_as('id_tr_dinas','ID');
		$crud->callback_column('anggaran',array($this,'format_num'));
		
		
		if($this->session->userdata('level') == 'SUBDIT' OR $this->session->userdata('level') == 'admin')
        {
        $crud->unset_add();
        $crud->unset_edit();
		$crud->unset_delete();
		}

        $crud->set_relation('id_prov', 'prop', 'nama_prov');
        $crud->set_relation('id_kabkot', 'kab', 'nama_kabkot');
       
        //$crud->unset_read();        
        //endregion
        $output = $crud->render();
        
       
		
		$out['output'] = $this->data['judul_browser'];
		$this->load->section('judul_browser', 'default_v', $out);
		$out['output'] = $this->data['judul_utama'];
		$this->load->section('judul_utama', 'default_v', $out);
		$out['output'] = $this->data['judul_sub'];
		$this->load->section('judul_sub', 'default_v', $out);
		$out['output'] = $this->data['u_name'];
		$this->load->section('u_name', 'default_v', $out);
		$this->load->view('default_v', $output);	

    }

    function wilayah_prov_callback()
    {
        return '<input type="hidden" maxlength="50" readonly="readonly" value="Provinsi" name="wilayah" style="width:462px">';
    }

    function status_dokumen_callback()
    {
        return '<input type="text" maxlength="50" readonly="readonly" value="belum tersedia" name="status_dokumen" style="width:462px">';
    }

    function wilayah_kab_callback()
    {
        return '<input type="hidden" maxlength="50" readonly="readonly" value="Kabupaten/Kota" name="wilayah" style="width:462px">';
    }
    
    
}