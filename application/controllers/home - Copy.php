<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper('fungsi');
		$this->load->model('home_m');
		$this->load->model('Dasboard_m');		
		//$this->load->model('Calendar_model');
	}	
	
	public function index() {
	    $this->data['judul_browser'] = 'Beranda';
		$this->data['judul_utama'] = 'Beranda';
		$this->data['judul_sub'] = 'Menu Utama';
		
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';
		//$this->data['js_files'][] = base_url() . 'assets/easyui/datagrid-detailview.js';

		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';
		#include seach
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';
		$this->data['js_files2'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.js';		
		$this->data['css_files'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.css';
		$level = $this->session->userdata('level');
				
		$this->data['dt'] = $this->home_m->charts();
		$tgl = date('Y-m-d');				
		
		$this->data['isi'] = $this->load->view('dashboard/maindashboard', $this->data, TRUE);
		$this->load->view('themes/layout_utama_v', $this->data);
		
	}

	public function dataperdinas(){
		$this->data['judul_browser'] = 'Beranda';
		$this->data['judul_utama'] = 'Beranda';
		$this->data['judul_sub'] = 'Menu Utama';
		
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';
		//$this->data['js_files'][] = base_url() . 'assets/easyui/datagrid-detailview.js';

		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';
		#include seach
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';
		$this->data['js_files2'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.js';		
		$this->data['css_files'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.css';
		$level = $this->session->userdata('level');
				
		$tgl = date('Y-m-d');				
		
		//load data here...
		//end
		$this->data['isi'] = $this->load->view('dashboard/dataperdinas', $this->data, TRUE);
		$this->load->view('themes/layout_utama_v', $this->data);
	}

	public function dataperdinasdetail(){
		$this->data['judul_browser'] = 'Beranda';
		$this->data['judul_utama'] = 'Beranda';
		$this->data['judul_sub'] = 'Menu Utama';
		
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';
		//$this->data['js_files'][] = base_url() . 'assets/easyui/datagrid-detailview.js';

		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';
		#include seach
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';
		$this->data['js_files2'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.js';		
		$this->data['css_files'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.css';
		$level = $this->session->userdata('level');
				
		$tgl = date('Y-m-d');				
		
		//load data here...
		//end
		$this->data['isi'] = $this->load->view('dashboard/dataperdinasdetail', $this->data, TRUE);
		$this->load->view('themes/layout_utama_v', $this->data);
	}
	
	//data per dinas
	public function get_dummydata(){


          // Datatables Variables
          $draw = intval($this->input->get("draw"));
          $start = intval($this->input->get("start"));
          $length = intval($this->input->get("length"));


          $books = $this->Dasboard_m->get_dummydata();

          $data = array();
		  $i=1;
          foreach($books->result() as $r) {

               $data[] = array(
				    $i,
                    $r->Provinsi,
					'<a href="dataperdinasdetail">'.$r->kabkota.'</a>',										
                    $r->r1,
                    $r->s1,
					$r->p1,
					$r->r2,
                    $r->s2,
					$r->p2,
					$r->r3,
                    $r->s3,
					$r->p3,
					$r->r4,
                    $r->s4,
					$r->p4,
					$r->r5,
                    $r->s5,
					$r->p5,
					$r->r6,
                    $r->s6,
					$r->p6,
					$r->r7,
                    $r->s7,
					$r->p7,
					$r->r8,
                    $r->s8,
					$r->p8,
					$r->r9,
                    $r->s9,
					$r->p9,
					$r->r10,
                    $r->s10,
					$r->p10,
					$r->r11,
                    $r->s11,
					$r->p11,
					$r->r12,
                    $r->s12,
					$r->p12					
			   );
			   $i++;
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $books->num_rows(),
                 "recordsFiltered" => $books->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
	}
	 
	public function get_dummydatadetail(){

          // Datatables Variables
          $draw = intval($this->input->get("draw"));
          $start = intval($this->input->get("start"));
          $length = intval($this->input->get("length"));


          $books = $this->Dasboard_m->get_dummydatadetail();

        //   $data = array();
		//   $i=1;
        //   foreach($books->result() as $r) {

        //        $data[] = array(
		// 		   "Wilayah" -> $r->Wilayah,
		// 		   "Indikator_Kinerja" -> $r->Indikator_Kinerja,
        //            "Indikator_Pencapaian" -> $r->Indikator_Pencapaian,
		// 		   "Item" -> $r->Item,
		// 		   "Pencapaian" ->	$r->pencapaian                	
		// 	   );
		// 	   $i++;
        //   }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $books->num_rows(),
                 "recordsFiltered" => $books->num_rows(),
                 "data" => $books->result()
            );
          echo json_encode($output);
          exit();
    }



	function getDatachart1($tahun){
		# code...        
        echo $this->Dasboard_m->getDataforDashboarPie($tahun);
	}

	function getDatachart1Detail(){
		# code...        
		$tahun= $this->input->get('tahun');
		$dinas=$this->input->get('dinas');
        echo $this->Dasboard_m->getDataForDashboardPieDetail($tahun,$dinas);
	}
	
	function getDatachart2($tahun){
		# code...        
        echo $this->Dasboard_m->getDataforDashboardColumn($tahun);
	}
	
	//Box 1
	function getJumlahProvinsiKab(){
		echo $this->Dasboard_m->getJumlahProvinsiKab();
	}

	//Box 2
	function getTotalprovkabspm($tahun){
		echo $this->Dasboard_m->getTotalprovkabspm($tahun);
	}


	//Box 3
	function getPersentasiSPM($tahun){
		echo $this->Dasboard_m->getPersentasiSPM($tahun);
	}

}
