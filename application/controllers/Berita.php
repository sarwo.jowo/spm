<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Berita extends AdminController {

	public function __construct() {
		parent::__construct();	
	}	
	
	public function index() {
		$this->data['judul_browser'] = 'Berita';
		$this->data['judul_utama'] = 'Berita';
		$this->data['judul_sub'] = 'User';

		$this->output->set_template('gc');
		//$crud->set_theme('twitter-bootstrap');

		$this->load->library('grocery_CRUD');
		$crud = new grocery_CRUD();
		$crud->set_table('berita');
       //$crud->unset_operations();
		$crud->set_subject('Data berita');		
		$crud->columns('judul','berita','file','lamp');
		$crud->fields('judul','berita','file','lamp','utama');
		
		//$crud->unset_fields(array('id'))
		//$crud->unset_columns(array('id'));
		$crud->unset_columns(array('berita'));
		$crud->display_as('judul','Judul Berita');
		$crud->display_as('berita','Berita');
		$crud->display_as('file','Gambar');
		$crud->display_as('lamp','Nama File');
		$crud->required_fields('judul','berita','utama');
	    $crud->set_field_upload('file','assets/uploads/files');
		$crud->set_field_upload('lamp','assets/uploads/files');
		$crud->field_type('utama','true_false');
		$crud->unset_read();
		$output = $crud->render();

		$out['output'] = $this->data['judul_browser'];
		$this->load->section('judul_browser', 'default_v', $out);
		$out['output'] = $this->data['judul_utama'];
		$this->load->section('judul_utama', 'default_v', $out);
		$out['output'] = $this->data['judul_sub'];
		$this->load->section('judul_sub', 'default_v', $out);
		$out['output'] = $this->data['u_name'];
		$this->load->section('u_name', 'default_v', $out);
		$this->load->view('default_v', $output);		

	}

	function _set_password_input_to_empty() {
		return "<input type='password' name='pass_word' value='' /><br />Kosongkan password jika tidak ingin ubah/isi.";
	}

	function _encrypt_password_callback($post_array) {
		if(!empty($post_array['pass_word'])) {
			$post_array['pass_word'] = sha1('nsi' . $post_array['pass_word']);
		} else {
			unset($post_array['pass_word']);
		}
		return $post_array;
	}

}
