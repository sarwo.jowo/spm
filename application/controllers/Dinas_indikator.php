<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dinas_indikator extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('dinas_indikator_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $dinas_indikator = $this->dinas_indikator_model->get_all();

        $data = array(
            'dinas_indikator_data' => $dinas_indikator
        );

        $this->load->view('tbl_dinas_indikator_list', $data);
    }

    public function read($id) 
    {
        $row = $this->dinas_indikator_model->get_by_id($id);
        if ($row) {
            $data = array(
		'indikator_id' => $row->indikator_id,
		'id_tr_dinas' => $row->id_tr_dinas,
		'wilayah' => $row->wilayah,
		'indikator_kinerja' => $row->indikator_kinerja,
		'indikator_pencapaian' => $row->indikator_pencapaian,
	    );
            $this->load->view('tbl_dinas_indikator_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('dinas_indikator'));
        }
    }
    
    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('dinas_indikator/create_action'),
	    'indikator_id' => set_value('indikator_id'),
	    'id_tr_dinas' => set_value('id_tr_dinas'),
	    'wilayah' => set_value('wilayah'),
	    'indikator_kinerja' => set_value('indikator_kinerja'),
	    'indikator_pencapaian' => set_value('indikator_pencapaian'),
	);
        $this->load->view('tbl_dinas_indikator_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_tr_dinas' => $this->input->post('id_tr_dinas',TRUE),
		'wilayah' => $this->input->post('wilayah',TRUE),
		'indikator_kinerja' => $this->input->post('indikator_kinerja',TRUE),
		'indikator_pencapaian' => $this->input->post('indikator_pencapaian',TRUE),
	    );

            $this->dinas_indikator_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('dinas_indikator'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->dinas_indikator_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('dinas_indikator/update_action'),
		'indikator_id' => set_value('indikator_id', $row->indikator_id),
		'id_tr_dinas' => set_value('id_tr_dinas', $row->id_tr_dinas),
		'wilayah' => set_value('wilayah', $row->wilayah),
		'indikator_kinerja' => set_value('indikator_kinerja', $row->indikator_kinerja),
		'indikator_pencapaian' => set_value('indikator_pencapaian', $row->indikator_pencapaian),
	    );
            $this->load->view('tbl_dinas_indikator_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('dinas_indikator'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('indikator_id', TRUE));
        } else {
            $data = array(
		'id_tr_dinas' => $this->input->post('id_tr_dinas',TRUE),
		'wilayah' => $this->input->post('wilayah',TRUE),
		'indikator_kinerja' => $this->input->post('indikator_kinerja',TRUE),
		'indikator_pencapaian' => $this->input->post('indikator_pencapaian',TRUE),
	    );

            $this->dinas_indikator_model->update($this->input->post('indikator_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('dinas_indikator'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->dinas_indikator_model->get_by_id($id);

        if ($row) {
            $this->dinas_indikator_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('dinas_indikator'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('dinas_indikator'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_tr_dinas', ' ', 'trim|required|numeric');
	$this->form_validation->set_rules('wilayah', ' ', 'trim|required');
	$this->form_validation->set_rules('indikator_kinerja', ' ', 'trim|required');
	$this->form_validation->set_rules('indikator_pencapaian', ' ', 'trim|required');

	$this->form_validation->set_rules('indikator_id', 'indikator_id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

};

/* End of file dinas_indikator.php */
/* Location: ./application/controllers/dinas_indikator.php */