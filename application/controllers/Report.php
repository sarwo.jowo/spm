<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH."/third_party/PHPExcel.php";
class Report extends MY_Controller {


	public function __construct() {
		parent::__construct();		
		$this->load->model('Report_m');		
	}	
	
	public function index() {
		$myLevel = array(1,2,3);
		if(!in_array($this->session->userdata('level'), $myLevel)){
			$this->no_akses();
			return false;
		}
		
		$this->data['js_files'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.js';		
		$this->data['css_files'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.css';
		$this->data['froms'] = isset($_REQUEST['froms']) ? $_REQUEST['froms'] : '';
		$this->data['to'] = isset($_REQUEST['to']) ? $_REQUEST['to'] : '';
		$this->data['judul_browser'] = 'Land lord';
		$this->data['judul_utama'] = 'Land lord';
		$this->data['judul_sub'] = 'Report';
		$this->data['title_box'] = 'List of Land Lord';
		$this->data['landlord'] = $this->Report_m->get_landlord();		
		
		$this->data['isi'] = $this->load->view('reports/landlord_req', $this->data, TRUE);

		$this->load->view('themes/layout_utama_v', $this->data);
	}
	
	public function member() {
		$myLevel = array(1,2,3);
		if(!in_array($this->session->userdata('level'), $myLevel)){
			$this->no_akses();
			return false;
		}
		
		$this->data['js_files'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.js';		
		$this->data['css_files'][] = base_url() . 'assets/datetimepicker/jquery.datetimepicker.css';
		$this->data['froms'] = isset($_REQUEST['froms']) ? $_REQUEST['froms'] : '';
		$this->data['to'] = isset($_REQUEST['to']) ? $_REQUEST['to'] : '';
		$this->data['judul_browser'] = 'Land lord';
		$this->data['judul_utama'] = 'Land lord';
		$this->data['judul_sub'] = 'Report';
		$this->data['title_box'] = 'List of Land Lord';
		$this->data['member'] = $this->Report_m->get_member();		
		
		$this->data['isi'] = $this->load->view('reports/member_req', $this->data, TRUE);

		$this->load->view('themes/layout_utama_v', $this->data);
	}
	
	function export_m(){
		$froms = isset($_REQUEST['froms']) ? $_REQUEST['froms'] : '';
		$to = isset($_REQUEST['to']) ? $_REQUEST['to'] : '';
		$periode = '';
		if(!empty($froms) && !empty($to)){
			$periode = " Periode : ".$froms." - ".$to;
		}
		$member = $this->Report_m->get_member();	
		$dates = date("d-m-y");
		$this->load->library('excel');
		
		$this->excel->setActiveSheetIndex(0);
		
		$this->excel->getActiveSheet()->setTitle('data_members_'.$dates);		
		$this->excel->getActiveSheet()->setCellValue('A1', 'Data Members' .$periode);		
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);		
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);		
		$this->excel->getActiveSheet()->mergeCells('A1:F1');		
		$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
		// $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		
		
		$this->excel->getActiveSheet()->getStyle('A3:F3')->getFont()->setSize(12);				
		$this->excel->getActiveSheet()->getStyle('A3:GF3')->getFont()->setBold(true);
		
		$styleArray = array(
		  'borders' => array(
			'allborders' => array(
			  'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		  )
		);
		
		$this->excel->getActiveSheet()->getStyle('A3:F3')->applyFromArray($styleArray);		
		
		$this->excel->getActiveSheet()->setCellValue('A3', 'No.');
        $this->excel->getActiveSheet()->setCellValue('B3', 'Reg. Date');
        $this->excel->getActiveSheet()->setCellValue('C3', 'Username');
        $this->excel->getActiveSheet()->setCellValue('D3', 'Fullname');
        $this->excel->getActiveSheet()->setCellValue('E3', 'Email');
        $this->excel->getActiveSheet()->setCellValue('F3', 'Address');
        // $this->excel->getActiveSheet()->setCellValue('G3', 'Photo');
       
		
		$this->excel->getActiveSheet()->getStyle('A3:F3')->getFill()
					->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
					->getStartColor()->setARGB('FFE8E5E5');
		
		$this->excel->getActiveSheet()->getStyle('A3:F3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$i=4;
		$no = 1;
		$address = '';
		$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
		$objDrawing->setName('Sample image');
		$objDrawing->setDescription('Sample image');
		foreach($member as $m){
			$photo = '';				
			if(!empty($m['photo'])){
				$photo = base_url('uploads/member/'.$m['photo']);
			}else{
				$photo = base_url('uploads/member/no_photo.jpg');
			}
			$gdImage = imagecreatefromjpeg($photo);
			$phone1 = !empty($m['phone1']) ? $m['phone1'] : 'Not Availabe';
			$phone2 = !empty($m['phone2']) ? $m['phone2'] : 'Not Availabe';
			$address = $m['street_name'].", ".$m['name'].", ".$m['province_name']."\nPhone 1 : ".$phone1."\nPhone 2 : ".$phone2;
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $no++);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, date("d-M-Y", strtotime($m['create_date'])));
			$this->excel->getActiveSheet()->setCellValue('C'.$i, ucwords($this->converter->decode($m['username'])));
			$this->excel->getActiveSheet()->setCellValue('D'.$i, ucwords($m['first_name']).' '.ucwords($m['last_name']));
			$this->excel->getActiveSheet()->setCellValue('E'.$i, $m['email']);
			$this->excel->getActiveSheet()->setCellValue('F'.$i, $address);
			// $this->excel->getActiveSheet()->setCellValue('G'.$i, $photo);
			
			
			// $objDrawing->setImageResource($gdImage);
			// $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
			// $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
			// $objDrawing->setHeight(150);
			// $objDrawing->setCoordinates('G'.$i);
			// $objDrawing->setWorksheet($this->excel->getActiveSheet());
			
			$this->excel->getActiveSheet()->getStyle('A'.$i.':F'.$i)->applyFromArray($styleArray);
			$this->excel->getActiveSheet()->getStyle('A'.$i.':F'.$i)->getFont()->setSize(12);
			$this->excel->getActiveSheet()->getStyle('A'.$i.':F'.$i)->getAlignment()->setWrapText(true);
			$this->excel->getActiveSheet()->getStyle('B'.$i.':F'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);			
			$this->excel->getActiveSheet()->getStyle('A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);			
			$this->excel->getActiveSheet()->getStyle('A'.$i.':F'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$i++;
		}
        unset($styleArray);	
		
		$this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToPage(true);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
		
		$filename='data_members_'.$dates.'.xls';
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'"'); 
		header('Cache-Control: max-age=0'); 					 
		
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  		
		$objWriter->save('php://output');
    }	
	
	function export_ll(){
		$froms = isset($_REQUEST['froms']) ? $_REQUEST['froms'] : '';
		$to = isset($_REQUEST['to']) ? $_REQUEST['to'] : '';
		$periode = '';
		if(!empty($froms) && !empty($to)){
			$periode = " Periode : ".$froms." - ".$to;
		}
		$landlord = $this->Report_m->get_landlord();
		$dates = date("d-m-y");
		$this->load->library('excel');
		
		$this->excel->setActiveSheetIndex(0);
		
		$this->excel->getActiveSheet()->setTitle('data_landlords_'.$dates);		
		$this->excel->getActiveSheet()->setCellValue('A1', 'Data Land Lord' .$periode);		
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);		
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);		
		$this->excel->getActiveSheet()->mergeCells('A1:H1');		
		$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		
		$this->excel->getActiveSheet()->getStyle('A3:H3')->getFont()->setSize(12);				
		$this->excel->getActiveSheet()->getStyle('A3:H3')->getFont()->setBold(true);
		
		$styleArray = array(
		  'borders' => array(
			'allborders' => array(
			  'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		  )
		);
		
		$this->excel->getActiveSheet()->getStyle('A3:H3')->applyFromArray($styleArray);		
		$this->excel->getActiveSheet()->setCellValue('A3', 'No.');
        $this->excel->getActiveSheet()->setCellValue('B3', 'Reg. Date');
        $this->excel->getActiveSheet()->setCellValue('C3', 'Land Lord Name');
        $this->excel->getActiveSheet()->setCellValue('D3', 'Owner');
        $this->excel->getActiveSheet()->setCellValue('E3', 'Address');
        $this->excel->getActiveSheet()->setCellValue('F3', 'Email');
        $this->excel->getActiveSheet()->setCellValue('G3', 'RNP Status');
        $this->excel->getActiveSheet()->setCellValue('H3', 'Legal Status');
		
		$this->excel->getActiveSheet()->getStyle('A3:H3')->getFill()
					->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
					->getStartColor()->setARGB('FFE8E5E5');
		
		$this->excel->getActiveSheet()->getStyle('A3:H3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$i=4;
		$no = 1;
		$address = '';
		$status = array ('0'=>'Not Available',1=>'Approved', 2=>'Revise',3=>'Rejected');
		foreach($landlord as $l){
			$phone1 = !empty($l['ll_phone1']) ? $l['ll_phone1'] : 'Not Availabe';
			$phone2 = !empty($l['ll_phone2']) ? $l['ll_phone2'] : 'Not Availabe';
			$address = $l['street'].", ".$l['name'].", ".$l['province_name']."\nPhone 1 : ".$phone1."\nPhone 2 : ".$phone2;
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $no++);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, date("d-M-Y", strtotime($l['create_date'])));
			$this->excel->getActiveSheet()->setCellValue('C'.$i, ucwords($l['ll_firstname'].' '.$l['ll_lastname']));
			$this->excel->getActiveSheet()->setCellValue('D'.$i, ucwords($this->converter->decode($l['username'])));
			$this->excel->getActiveSheet()->setCellValue('E'.$i, $address);
			$this->excel->getActiveSheet()->setCellValue('F'.$i, $l['email']);
			$this->excel->getActiveSheet()->setCellValue('G'.$i, $status[$l['rnp_status']]);
			$this->excel->getActiveSheet()->setCellValue('H'.$i, $status[$l['legal_status']]);
			$this->excel->getActiveSheet()->getStyle('A'.$i.':H'.$i)->applyFromArray($styleArray);
			$this->excel->getActiveSheet()->getStyle('A'.$i.':H'.$i)->getFont()->setSize(12);
			$this->excel->getActiveSheet()->getStyle('A'.$i.':H'.$i)->getAlignment()->setWrapText(true);
			$this->excel->getActiveSheet()->getStyle('B'.$i.':H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);	
			$this->excel->getActiveSheet()->getStyle('A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);				
			$this->excel->getActiveSheet()->getStyle('A'.$i.':H'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$i++;
		}
        unset($styleArray);	
		$this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToPage(true);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
		$filename='data_landlords_'.$dates.'.xls';
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); 					 
		
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  		
		$objWriter->save('php://output');
    }

		
	public function no_akses() {
		if ($this->session->userdata('login') == FALSE) {
			redirect('/');
			return false;
		}
		$this->data['judul_browser'] = 'Tidak Ada Akses';
		$this->data['judul_utama'] = 'Tidak Ada Akses';
		$this->data['judul_sub'] = '';
		$this->data['isi'] = '<div class="alert alert-danger">Anda tidak memiliki Akses.</div>';
		$this->load->view('themes/layout_utama_v', $this->data);
	}


}
