<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Verify extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('Api_m');
	}


	public function index(){
		$id	= $this->input->get('id');		
		$result	= array();
		if(!empty($id) && (int) $id > 0){
			$id	= $this->converter->decode($id);			
		}
		 $this->load->view('themes/verify');
	}


}

?>