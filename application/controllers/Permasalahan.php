<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permasalahan extends AdminController {

	public function __construct() {
		parent::__construct();	
		//$this->load->model('dept_m');
    }
     public function index() {
         redirect(site_url('home'));
     } 
    
    public function Kab() {
		$this->data['judul_browser'] = 'Data';
		$this->data['judul_utama'] = 'Permasalahan Untuk Kabupaten/Kota';
	  
		$this->output->set_template('gc');

		$this->load->library('grocery_CRUD');
		$crud = new grocery_CRUD();
        $crud->set_table('tbl_masalah');
        $crud->where('wilayah', 'Kabupaten/Kota');
		$crud->set_subject('Permasalahan Kabupaten/Kota');
        $crud->display_as('tahun','Tahun');		   
		$crud->columns('id_masalah','id_kabkot','tahun','keterangan');
        $crud->fields('id_masalah','id_kabkot','tahun','keterangan','wilayah'); 
        $crud->field_type('wilayah', 'hidden');		
        $crud->callback_add_field('wilayah',array($this,'wilayah_kab_callback'));
        $crud->set_relation('id_masalah', 'tbl_master_masalah', 'Jenis_masalah');
        $crud->display_as('id_masalah','Jenis Masalah');
        $crud->display_as('id_prov','Provinsi');
        $crud->display_as('id_kabkot','Kabupaten');
        $crud->display_as('id_tr_dinas','ID');		
        $crud->field_type('id_kabkot', 'hidden',$this->session->userdata('id_kabkot'));
		
         $crud->field_type('id_kabkot', 'hidden',$this->session->userdata('u_name'));
         if($this->session->userdata('level') == 'SUBDIT' OR $this->session->userdata('level') == 'admin' OR $this->session->userdata('level') == 'ADMINPROV')
        {
        $crud->unset_add();
        $crud->unset_edit();
		$crud->unset_delete();
		}else
		{
		$crud->unset_delete();	
		}
		$act = $this->uri->segment(3);
		if ($act != 'add')
		{	
        $crud->set_relation('id_kabkot', 'kab', 'nama_kabkot');	
		}
		else
		{
			
		}	
		
		if($this->session->userdata('level') == 'ADMINPROV')
        {
		$crud->where('tbl_masalah.id_prov',$this->session->userdata('id_prov'));
		//$crud->where('wilayah', 'Kabupaten/Kota');	
		}
		
		
		if($this->session->userdata('level') == 'ADMINKAB')
        {
		$crud->where('tbl_masalah.id_kabkot',$this->session->userdata('id_kabkot'));
		//$crud->where('wilayah', 'Kabupaten/Kota');	
		}
		// else
		// {
		 // $crud->where('wilayah', 'Kabupaten/Kota');	
		// }
		if($this->session->userdata('level') == 'ADMINPROV')
        {
        $crud->unset_add();
        $crud->unset_edit();
		$crud->unset_delete();
		}	

		if($this->session->userdata('level') == 'admin')
        {
        $crud->unset_add();
        $crud->unset_edit();
		//$crud->unset_delete();
		}		
		
		
        $output = $crud->render();       
		$out['output'] = $this->data['judul_browser'];
		$this->load->section('judul_browser', 'default_v', $out);
		$out['output'] = $this->data['judul_utama'];
		$this->load->section('judul_utama', 'default_v', $out);
		$out['output'] = $this->data['judul_sub'];
		$this->load->section('judul_sub', 'default_v', $out);
		$out['output'] = $this->data['u_name'];
		$this->load->section('u_name', 'default_v', $out);
		$this->load->view('default_v', $output);	

    }
    
    function generate_reportkab($primary_key , $row){

        if($row->flag_generated=="aktif"){
            return site_url('generate_indikator/editIndikator').'?dinasid='.$row->id_tr_dinas.'&wilayah=kabkota&dinas='.$row->dinas;
        }
        else{
            return site_url('generate_indikator/index').'?dinasid='.$row->id_tr_dinas.'&wilayah=kabkota&dinas='.$row->dinas;
        }
        
    }

    function view_dokument($primary_key , $row){

        return site_url('generate_indikator/UploadDokumen').'?dinasid='.$row->id_tr_dinas;
        
    }

    function generate_reportprov($primary_key , $row){

        if($row->flag_generated=="aktif"){
            return site_url('generate_indikator/editindikator').'?dinasid='.$row->id_tr_dinas.'&wilayah=provinsi&dinas='.$row->dinas;
        }
        else{
            return site_url('generate_indikator/index').'?dinasid='.$row->id_tr_dinas.'&wilayah=provinsi&dinas='.$row->dinas;
        }
        
    }


    public function Prov() {
		$this->data['judul_browser'] = 'Data';
		$this->data['judul_utama'] = 'Permasalahan Provinsi';
	  
		$this->output->set_template('gc');

		$this->load->library('grocery_CRUD');
		$crud = new grocery_CRUD();
        $crud->set_table('tbl_masalah');
        $crud->where('wilayah', 'Provinsi');
		
		$crud->set_subject('Permasalahan Provinsi');
        	   
	     $crud->display_as('tahun','Tahun');		   
		
        
        $crud->field_type('wilayah', 'hidden');		
        $crud->callback_add_field('wilayah',array($this,'wilayah_prov_callback'));
        $crud->set_relation('id_masalah', 'tbl_master_masalah', 'Jenis_masalah');
        $crud->display_as('id_masalah','Jenis Masalah');
        $crud->display_as('id_prov','Provinsi');
        //$crud->display_as('id_kabkot','Kabupaten');
        $crud->display_as('id_tr_dinas','ID');		
        
		
        $crud->columns('id_masalah','id_prov','tahun','keterangan');
		$crud->fields('id_masalah','id_prov','tahun','keterangan','wilayah'); 
		$crud->field_type('id_prov', 'hidden',$this->session->userdata('id_prov'));
		//$crud->set_relation('id_prov', 'prop', 'nama_prov');
        //$crud->set_relation('id_kabkot', 'kab', 'nama_kabkot');
        //$crud->field_type('wilayah', 'hidden');
        //$crud->add_action('detail laporan', '', '','glyphicon glyphicon-list',array($this,'generate_reportprov'));
        //$crud->add_action('lihat dokumen', '', '','glyphicon glyphicon-file',array($this,'view_dokument'));
		
        if($this->session->userdata('level') == 'SUBDIT' OR $this->session->userdata('level') == 'admin' OR $this->session->userdata('level') == 'ADMINKAB')
        {
        $crud->unset_add();
        $crud->unset_edit();
		$crud->unset_delete();
		}else
		{
		$crud->unset_delete();	
		}	
        $act = $this->uri->segment(3);
		if ($act != 'add')
		{	
        $crud->set_relation('id_prov', 'prop', 'nama_prov');	
		}
		if($this->session->userdata('level') == 'ADMINPROV')
        {
		$crud->where('tbl_masalah.id_prov',$this->session->userdata('id_prov'));
		//$crud->where('wilayah', 'Kabupaten/Kota');	
		}
        //custom action Button
		//$crud->unset_delete(); 
       // $crud->unset_read();        
        //endregion
		if($this->session->userdata('level') == 'SUBDIT')
        {
        $crud->unset_add();
        $crud->unset_edit();
		$crud->unset_delete();
		}	

		if($this->session->userdata('level') == 'admin')
        {
        $crud->unset_add();
        $crud->unset_edit();
		//$crud->unset_delete();
		}		
		
        $output = $crud->render();
        
        
		$out['output'] = $this->data['judul_browser'];
		$this->load->section('judul_browser', 'default_v', $out);
		$out['output'] = $this->data['judul_utama'];
		$this->load->section('judul_utama', 'default_v', $out);
		$out['output'] = $this->data['judul_sub'];
		$this->load->section('judul_sub', 'default_v', $out);
		$out['output'] = $this->data['u_name'];
		$this->load->section('u_name', 'default_v', $out);
		$this->load->view('default_v', $output);	

    }

    function wilayah_prov_callback()
    {
        return '<input type="hidden" maxlength="50" readonly="readonly" value="Provinsi" name="wilayah" style="width:462px">';
    }

    function status_dokumen_callback()
    {
        return '<input type="text" maxlength="50" readonly="readonly" value="belum tersedia" name="status_dokumen" style="width:462px">';
    }

    function wilayah_kab_callback()
    {
        return '<input type="hidden" maxlength="50" readonly="readonly" value="Kabupaten/Kota" name="wilayah" style="width:462px">';
    }
    
    
}