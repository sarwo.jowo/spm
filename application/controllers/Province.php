<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Province extends MY_Controller {

	public function __construct() {
		parent::__construct();		
				
		$this->load->model('Province_m');			
	}	
	
	public function index() {
		if($this->session->userdata('level') !=1){
			$this->no_akses();		
			return false;
		}
		$this->data['judul_browser'] = 'Province';
		$this->data['judul_utama'] = 'List';
		$this->data['judul_sub'] = 'of Province';
		$this->data['title_box'] = 'List of Province';	
		$this->data['provinces'] = $this->Province_m->get_province();
		$this->data['isi'] = $this->load->view('province_v', $this->data, TRUE);

		$this->load->view('themes/layout_utama_v', $this->data);
	}
	
	
	public function save_pro(){	
		if($this->session->userdata('level') !=1){
			$this->no_akses();		
			return false;
		}
		echo $this->Province_m->save_pro();
	}
	
	public function del_pro(){	
		if($this->session->userdata('level') !=1){
			$this->no_akses();		
			return false;
		}
		echo $this->Province_m->del_pro();
	}
	
	public function save_city(){	
		if($this->session->userdata('level') !=1){
			$this->no_akses();		
			return false;
		}
		echo $this->Province_m->save_city();
	}
	
	public function del_city(){
		if($this->session->userdata('level') !=1){
			$this->no_akses();		
			return false;
		}		
		echo $this->Province_m->del_city();
	}
	
	public function city() {
		if($this->session->userdata('level') !=1){
			$this->no_akses();		
			return false;
		}
		$this->data['judul_browser'] = 'City';
		$this->data['judul_utama'] = 'List';
		$this->data['judul_sub'] = 'of City';
		$this->data['title_box'] = 'List of City';
		$this->data['city'] = $this->Province_m->get_city();
		
		$province_list[''] = '-- Pilih Provinsi --';
		$province_list += $this->Province_m->select_pro();
		$this->data['lp'] = form_dropdown('province_name', $province_list, '', 'class="form-control" style="width:99%" id="province_name"');
		$this->data['isi'] = $this->load->view('city_v', $this->data, TRUE);
			
		$this->load->view('themes/layout_utama_v', $this->data);
	}
	
	

	public function no_akses() {
		if ($this->session->userdata('login') == FALSE) {
			redirect('/');
			return false;
		}
		$this->data['judul_browser'] = 'Tidak Ada Akses';
		$this->data['judul_utama'] = 'Tidak Ada Akses';
		$this->data['judul_sub'] = '';

		$this->data['isi'] = '<div class="alert alert-danger">Anda tidak memiliki Akses.</div>';

		$this->load->view('themes/layout_utama_v', $this->data);
	}


}
