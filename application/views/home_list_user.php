<style>
.form-control{
	height:17px;
	width:30%;
}
.panel-body{
	font-size:14px;
}
</style>


<div class="row">
		<div class="col-md-12">
     		<div class="box box-solid box-primary">
			<div class="box-header">
				<h3 class="box-title">Berita Terbaru</h3>
				<div class="box-tools pull-right">
					<button class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<div class="box-body" style="display:block">


		<table class="table table-striped table-condensed">
			<?php 
										$i= 1;
										foreach($brtutm as $row)
										{
									    $id = $row->id;
										?>            
			<div></div>
           <tr>
			<td colspan="2">Tanggal terbit : <?php echo $row->tgl;?></td>									
			</tr>			
			<tr>
			<td><table width="60px" height="60px" border="1"><tr><td><img src="<?php echo base_url("assets/uploads/files");?><?php echo "/";?><?php echo $row->file;?>" width="80px" height="80px"></td></tr></table></td><td><?php echo $row->berita;?></td>
									
			</tr>			
		
			<?php
											$i++;
											}										 										 
										  ?>
		</table>

            </div>
			<div class="box-body" style="display:block">
           


		<table class="table table-striped table-condensed">
		<tr><td colspan="2">Berita lainya</td></tr>
			<?php 
										$i= 1;
										foreach($brt as $row)
										{
									    $id = $row->id;
										?>
            
			<tr>
				<td class="col-md-8"><b><a href="<?php echo base_url('home/newsdetail')."/".$row->id;?>"><?php echo $row->judul;?> </a>
									</b></br>
				
						</td>
				<td class="col-md-4"><b><?php echo $row->tgl;?> </b></td>
			</tr>			
		
			<?php
											$i++;
											}										 										 
										  ?>
		</table>

            </div>
		</div>  
        </div>
	<div class="col-md-12">
    <div class="box box-solid box-primary">
   <div class="box-header">
				<h3 class="box-title">Dashboard Kabupaten Monthly</h3>
				<div class="box-tools pull-right">
					<button class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
<div class="box-body" style="display:block">
 <label for="jenis">Ganti Tampilan</label>
									  <select onchange="jenis()" id="jenis" name="jenis">
									  <option value="0">Pilih Jenis Charts</option>
									  <option value="1">Pie</option>
									  <option value="2">Line</option>
									 </select>
<div id="chartA"></div>
</div>           
</div></div>
	<div class="col-md-12">
	<div class="box-body">
          
	 <div class="row">		 

		
		<div class="col-md-12">          	  
	            
        <div class="box box-solid box-primary">
			<div class="box-header">
				<h3 class="box-title">Dashboard Kabupaten</h3>
				<div class="box-tools pull-right">
					<button class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<div class="box-body" style="display:block">
			 <label for="jenis2">Ganti Tampilan</label>
									  <select onchange="jenis2()" id="jenis2" name="jenis2">
									  <option value="0">Pilih Jenis Charts</option>
									  <option value="1">Pie</option>
									  <option value="2">Line</option>
									 </select>
			<div id="container"></div>
            </div>
		
		
		</div>
        </div>		
	</div>	  
  </div>	  
</div>	  
</div>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript">
 
$(function () {	
	ch();
	chb();
	
	$('#jenis').on('change', function() {
	        //alert('testing');
			var pr = $(this).val();
			console.log(pr); 
           // alert(pr);
			if (pr == 2)
			{
	      	ch();
			}
			if (pr == 1)
			{
	      	ch2();
			}
		 });
		 
		  $('#jenis2').on('change', function() {
	        var pr2 = $(this).val();
			console.log(pr2); 
           // alert(pr2);
			if (pr2 == 2)
			{
	      	 chb2();
			//alert('pie');
			}
			if (pr2 == 1)
			{
			//alert('line');
	      	 chb();
			}
		   });		 
	
});
function ch ()
{
 $('#chartA').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Laporan Per Bulan (All)'
        },
        xAxis: {
             categories: [<?php 
					// data yang diambil dari database
					if(count($bln)>0)
					{
					   foreach ($bln as $data) {
					   echo "[" . $data->bln ."],\n";
					   }
					}
					?>
			 
			 ]
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Dalam Rupiah (Jutaan)'
            }
        },
         legend: {
                    enabled: false
                },
        credits: {
            enabled: false
        },
        tooltip: {
            shared: true
        },
        series: [
		
		{
            name: 'Monthly'			,
            data: [
					<?php 
					// data yang diambil dari database
					if(count($dt2)>0)
					{
					   foreach ($dt2 as $dt2) {
					   echo "[" . $dt2->nilai ."],\n";
					   }
					}
					?>
			]           
        }
		
		
		]
    });	
}
function ch2()
{
	
$('#chartA').highcharts({
		
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false
		},
		title: {
			text: 'laporan Per Area'
		},
		
		tooltip: {
			pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			
		},
		plotOptions: {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.percentage:.1f} %',
					style: {
						color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
					}
				}
			}
		},
		series: [{
			type: 'pie',
			name: 'Persentase Pengaduan',
			data: [
					<?php 
					// data yang diambil dari database
					if(count($dt2b)>0)
					{
					foreach ($dt2b as $dt2b) {
					echo "['" . $dt2b->bln ."'," . $dt2b->nilai ."],\n";
					}
					}				
					?>
			]
		}]
	});			

}
function chb()
{
	$('#container').highcharts({
		
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false
		},
		title: {
			text: 'laporan Per Area'
		},
		
		tooltip: {
			pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			
		},
		plotOptions: {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.percentage:.1f} %',
					style: {
						color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
					}
				}
			}
		},
		series: [{
			type: 'pie',
			name: 'Persentase Pengaduan',
			data: [
					<?php 
					// data yang diambil dari database
					if(count($dt)>0)
					{
					   foreach ($dt as $data) {
					   echo "['" . $data->nama ."'," . $data->nilai ."],\n";
					   }
					}
					?>
			]
		}]
	});			
}
function chb2(){
	$('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Laporan Per Area'
        },
        xAxis: {
             categories: [
			        <?php 
					if(count($dtb2)>0)
					{
					   foreach ($dtb2 as $dtb2) {
					   echo "['" . $dtb2->nama ."'],\n";
					   }
					}
					?>
			 
			 
			 ]
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Dalam Rupiah (Jutaan)'
            }
        },
         legend: {
                    // layout: 'vertical',
                    // align: 'right',
                    // verticalAlign: 'top',
                    // x: -10,
                    // y: 100,
                    // borderWidth: 0
					enabled: false
                },
        credits: {
            enabled: false
        },
        tooltip: {
            shared: true
        },
        series: [
		
		{
            name: 'Kabupaten'			,
            data: [
					<?php 
					// data yang diambil dari database
					if(count($dtb)>0)
					{
					   foreach ($dtb as $dtb) {
					   //echo "[" . $dt3->nilai ."],\n";
					    echo "[" . $dtb->nilai ."],\n";
					   }
					}
					?>
			]           
        }
		
		
		
		]
    });	
		
}
 
</script>


