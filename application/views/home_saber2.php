<style type="text/css">
td, div {
	font-family: "Arial","?Helvetica","?sans-serif";
}
.datagrid-header-row * {
	font-weight: bold;
}
.messager-window * a:focus, .messager-window * span:focus {
	color: blue;
	font-weight: bold;
}
.daterangepicker * {
	font-family: "Source Sans Pro","Arial","?Helvetica","?sans-serif";
	box-sizing: border-box;
}
.glyphicon	{font-family: "Glyphicons Halflings"}
</style>
<?php 
// buat tanggal sekarang
$tanggal = date('Y-m-d H:i');
$tanggal_arr = explode(' ', $tanggal);
$tanggal2 = date('Y-m-d H:i');
?>                     <div class="row">
                        <!-- left column -->
                        <div class="col-md-6">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Laporan Pungli</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                    <div class="box-body">  
																 
								        <div class="form-group">
										 <label>Tanggal lapor</label>
                                           <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control"  name="tglmasuk"  id="tglmasuk"  value ="<?php echo $temp->tgl_masuk; ?>" disabled/>
                                        </div><!-- /.input group -->        </div>
	                                    <div class="form-group">
                                        <label>Tanggal Kejadian:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control"  name="tglsurat"  id="tglsurat"  value ="<?php echo $temp->tgl_lapor; ?>" disabled/>
                                        </div><!-- /.input group -->
                                         </div><!-- /.form group -->

										 
									   <div class="form-group">									   
                                         <label for="asalsurat">Pilih Area</label>                                            
										 <select id="depart_id" name="depart_id" class="form-control" required="true">
										<option value="0"><?php echo $area[0]->nama; ?></option>										
									</select>									  
									  </div>
									  <div class="form-group">
                                            <label>Kegiatan</label>
                                            <textarea class="form-control" id="kegiatan"  name="kegiatan" rows="3" value ="<?php echo $temp->kegiatan;?>" disabled><?php echo $temp->kegiatan;?></textarea>
                                        </div>
										 <div class="form-group">
                                            <label for="SKPD">SKPD</label>
                                            <input type="text" id="skpd" name="skpd" class="form-control"  value ="<?php echo $temp->skpd; ?>" disabled/>
                                        </div> 
										 <div class="form-group">
                                            <label for="pelaku">Pelaku</label>
                                            <input type="text" id="pelaku" name="pelaku" class="form-control" value ="<?php echo $temp->pelaku; ?>" disabled/>
                                        </div> 
										<div class="form-group">
                                            <label for="modus">Modus</label>
                                            <input type="text" id="modul" name="modul" class="form-control"  value ="<?php echo $temp->modus; ?>" disabled/>
                                        </div> 
									  <div class="form-group">
                                            <label for="modus">Nilai</label>
                                            <input type="text" id="nilai" name="nilai" class="form-control"  value ="<?php echo $temp->nilai; ?>" disabled/>
                                        </div> 							
                               
                                    </div><!-- /.box-body -->                                
                              
                            </div><!-- /.box -->         
                                
                            </div><!-- /.box -->
							
							 <div class="col-md-6">
                              <div class="box box-warning">
                                <div class="box-header">
                                    <h3 class="box-title">Datang Pendukung</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">                                   
                         				<form action="<?php echo site_url('Saber/upload'); ?>" method="post" enctype="multipart/form-data" id="form-upload">
				                        <input type="hidden" id="idsb" name="idsb"  value ="<?php echo $temp->id; ?>">
                                  		<input name="image" type="file" />
				                        <input type="submit" id="submit-button" value="Upload" class="btn btn-primary" />
			                            </form>
			                    <div id="output"></div>								   
				            </div>				
                          </div>	  
			                <div class="box-body no-padding">
                                   
                                    <div class="table-responsive">
                                        <!-- .table - Uses sparkline charts-->
                                        <table class="table table-striped">
												  
                                            <tr>
                                                <th>No</th>
                                                <th>File Name</th>
                                                 <th>Tools</th>
                                            </tr>
										
										<?php 
										$i= 1;
										foreach($files as $row)
										{
									    $id = $row->id;
										?>
                                        										
                                            <tr>
                                                <td><a href="#"><?php echo $i;?></a></td>
                                                <td><div id="sparkline-1">
												<a href="<?php echo base_url('assets/uploads')."/".$row->filename;?>" target="_new" id="<?php echo $row->filename;?>"><?php echo $row->filename;?> </a>
												</div></td>
                                                 <td>												 
												<button id="<?php echo $id;?>" class="btn btn-primary" onClick="reply_click(this.id)">Hapus</button>
												</td>
                                            </tr>
                                                                                     
											
                                        <?php
											$i++;
											}										 										 
										  ?>
										  </table>
                                    </div>
                                </div><!-- /.box-body--> 
			      
    
        <!-- InputMask -->
        <script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>
        <!-- date-range-picker -->
        <script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <!-- bootstrap color picker -->
        <script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript"></script>
        <!-- bootstrap time picker -->
        <script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
		<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/vendor/jquery.ui.widget.js"></script>
		<!-- The Templates plugin is included to render the upload/download listings -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/tmpl.min.js"></script>
		<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/load-image.all.min.js"></script>
		<!-- The Canvas to Blob plugin is included for image resizing functionality -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/canvas-to-blob.min.js"></script>
		<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
		<!-- blueimp Gallery script -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/jquery.blueimp-gallery.min.js"></script>
		<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/jquery.iframe-transport.js"></script>
		<!-- The basic File Upload plugin -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/jquery.fileupload.js"></script>
		<!-- The File Upload processing plugin -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/jquery.fileupload-process.js"></script>
		<!-- The File Upload image preview & resize plugin -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/jquery.fileupload-image.js"></script>
		<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.form.js'); ?>"></script>
		<!-- The File Upload audio preview plugin -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/jquery.fileupload-audio.js"></script>
		<!-- The File Upload validation plugin -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/jquery.fileupload-validate.js"></script>
		<!-- The File Upload user interface plugin -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/jquery.fileupload-ui.js"></script>
		<!-- The main application script -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/main.js"></script>
		<!-- blueimp Gallery styles -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/theme_admin/upload/css/blueimp-gallery.min.css">
		<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/theme_admin/upload/css/jquery.fileupload.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/theme_admin/upload/css/jquery.fileupload-ui.css">
		<!-- CSS adjustments for browsers with JavaScript disabled -->
		<noscript><link rel="stylesheet" href="<?php echo base_url(); ?>assets/theme_admin/upload/css/jquery.fileupload-noscript.css"></noscript>
		<noscript><link rel="stylesheet" href="<?php echo base_url(); ?>assets/theme_admin/upload/css/jquery.fileupload-ui-noscript.css"></noscript>
      	   
       <script type="text/javascript">
	      jQuery(document).ready(function() { 
		  	jQuery('#form-upload').on('submit', function(e) {
				e.preventDefault();
				jQuery('#submit-button').attr('disabled', ''); 
				jQuery("#output").html('<div style="padding:10px"><img src="<?php echo base_url('assets/images/loading.gif'); ?>" alt="Please Wait"/> <span>Mengunggah...</span></div>');
				jQuery(this).ajaxSubmit({
					target: '#output',
					success:  sukses 
				});
			});
          }); 
		 function sukses()  { 
		    alert('upload file Pendukung Berhasil');
			location.reload();
			jQuery('#form-upload').resetForm();
			jQuery('#submit-button').removeAttr('disabled');			

		  }
		  function reply_click(clicked_id)
			{			
			alert('Delete File Pendukung');
			var data = "id="+clicked_id;            			
			var url = '<?php echo site_url('Saber/deletefile');?>';					
			$.ajax({
				//alert('testing');
				type	: "POST",
				url		: url,
				data	: data,
				success	: function(result) {
					var result = eval('('+result+')');								
					if(result.ok) {						
						$.messager.show({
							title:'<div>Informasi</div>',
							msg: result.msg,
							timeout:2000,
							showType:'slide'                        							
							
						});	
                        location.reload();						
						
					}else{
						$.messager.show({
							title:'<div>Informasi</div>',
							msg: result.msg,
							timeout:2000,
							showType:'slide'							
							
						});		
						location.reload();						
					}
				}
			});
			}		  
		  function disable(){
				$("#rbutton").attr("disabled","disabled");
			}
	      		
        </script>	
       