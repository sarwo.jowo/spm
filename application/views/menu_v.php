<!-- search form -->
<a href="<?php echo site_url('home');?>" class="logo">
<!-- Add the class icon to your logo image or logo icon to add the margining -->

<div style="text-align:center;"><img  src="<?php echo base_url().'assets/logo/'; ?><?php echo $this->session->userdata('u_name'); ?>.png" width="80px" height="80px"></div>
</a>

<!-- /.search form -->
<?php if ($level == "admin"){?>
<ul class="sidebar-menu">
<!-- Menu Transaksi -->
<li class="treeview <?php $menu_home_arr= array('homeadmin', '');
	 if(in_array($this->uri->segment(1), $menu_home_arr)) {echo "active";}?>">
		<a href="#">
			<img height="20" src="<?php echo base_url().'assets/theme_admin/img/home.png'; ?>"> <span>Beranda</span>
		</a>
		
 <ul class="treeview-menu"> 
	<li class="<?php if ($this->uri->segment(3) == 'homeProv') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>homeprov"> <i class="fa fa-folder-open-o"></i>Provinsi</a></li>
   <li class="<?php if ($this->uri->segment(2) == 'homeadmin') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>homeadmin"> <i class="fa fa-folder-open-o"></i>Kabupaten/Kota</a></li>
	
</li>
</ul>

</li>
<?php } else {?>
<ul class="sidebar-menu">
<li class="<?php 
	 $menu_home_arr= array('home', '');
	 if(in_array($this->uri->segment(1), $menu_home_arr)) {echo "active";}?>">
		<a href="<?php echo base_url(); ?>home">
			<img height="20" src="<?php echo base_url().'assets/theme_admin/img/home.png'; ?>"> <span>Beranda</span>
		</a>
</li>

		
		
<?php } ?>




<?php if ($level == "SUBDIT"){?>
<li  class="treeview <?php 
$menu_sett_arr= array('indexProv','indexKab','generate_indikator');
if(in_array($this->uri->segment(1), $menu_sett_arr)) {echo "active";}?>">
<a href="#">
	<i class="fa fa-user"></i>
	<span>Capaian</span>
	<i class="fa fa-angle-left pull-right"></i>
</a>
<ul class="treeview-menu"> 
   <li class="<?php if ($this->uri->segment(2) == 'indexProv') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Dinas/indexProv"> <i class="fa fa-folder-open-o"></i>Capaian SPM Provinsi</a></li>
	<li class="<?php if ($this->uri->segment(2) == 'indexKab') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Dinas/indexKab"> <i class="fa fa-folder-open-o"></i>Capaian SPM Kabupaten/Kota</a></li>
</li>
</ul>
<li  class="treeview <?php 
$menu_sett_arr= array('anggaranProv','anggaranKab');
if(in_array($this->uri->segment(1), $menu_sett_arr)) {echo "active";}?>">
<a href="#">
	<i class="fa fa-user"></i>
	<span>Anggaran</span>
	<i class="fa fa-angle-left pull-right"></i>
</a>
<ul class="treeview-menu"> 
   <li class="<?php if ($this->uri->segment(2) == 'anggaranProv') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Angg/anggaranProv"> <i class="fa fa-folder-open-o"></i>Anggaran Provinsi</a></li>
	<li class="<?php if ($this->uri->segment(2) == 'anggaranKab') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Angg/anggaranKab"> <i class="fa fa-folder-open-o"></i>Anggaran Kabupaten</a></li>
</li>
</ul>



<li  class="treeview <?php 
$menu_sett_arr= array('Prov','Kab');
if(in_array($this->uri->segment(1), $menu_sett_arr)) {echo "active";}?>">
<a href="#">
	<i class="fa fa-user"></i>
	<span>Permasalahan</span>
	<i class="fa fa-angle-left pull-right"></i>
</a>
<ul class="treeview-menu"> 
   <li class="<?php if ($this->uri->segment(2) == 'Prov') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Permasalahan/Prov"> <i class="fa fa-folder-open-o"></i>Permasalahan Provinsi</a></li>
	<li class="<?php if ($this->uri->segment(2) == 'Kab') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Permasalahan/Kab"> <i class="fa fa-folder-open-o"></i>Permasalahan Kabupaten/Kota</a></li>
</li>
</ul>
<?php } ?>


<?php if ($level == "PENGGUNA"){?>
<li  class="treeview <?php 
$menu_sett_arr= array('indexProv','indexKab','generate_indikator');
if(in_array($this->uri->segment(1), $menu_sett_arr)) {echo "active";}?>">
<a href="#">
	<i class="fa fa-user"></i>
	<span>Capaian</span>
	<i class="fa fa-angle-left pull-right"></i>
</a>
<ul class="treeview-menu"> 
   <li class="<?php if ($this->uri->segment(2) == 'indexProv') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Dinas/indexProv"> <i class="fa fa-folder-open-o"></i>Capaian SPM Provinsi</a></li>
	<li class="<?php if ($this->uri->segment(2) == 'indexKab') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Dinas/indexKab"> <i class="fa fa-folder-open-o"></i>Capaian SPM Kabupaten/Kota</a></li>
</li>
</ul>
<li  class="treeview <?php 
$menu_sett_arr= array('anggaranProv','anggaranKab');
if(in_array($this->uri->segment(1), $menu_sett_arr)) {echo "active";}?>">
<a href="#">
	<i class="fa fa-user"></i>
	<span>Anggaran</span>
	<i class="fa fa-angle-left pull-right"></i>
</a>
<ul class="treeview-menu"> 
   <li class="<?php if ($this->uri->segment(2) == 'anggaranProv') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Angg/anggaranProv"> <i class="fa fa-folder-open-o"></i>Anggaran Provinsi</a></li>
	<li class="<?php if ($this->uri->segment(2) == 'anggaranKab') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Angg/anggaranKab"> <i class="fa fa-folder-open-o"></i>Anggaran Kabupaten/Kota</a></li>
</li>
</ul>
<?php } ?>


<?php if ($level == "admin"){?>
<li  class="treeview <?php 
$menu_sett_arr= array('indexProv','indexKab','generate_indikator');
if(in_array($this->uri->segment(1), $menu_sett_arr)) {echo "active";}?>">
<a href="#">
	<i class="fa fa-user"></i>
	<span>Capaian</span>
	<i class="fa fa-angle-left pull-right"></i>
</a>
<ul class="treeview-menu"> 
   <li class="<?php if ($this->uri->segment(2) == 'indexProv') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Dinas/indexProv"> <i class="fa fa-folder-open-o"></i>Capaian SPM Provinsi</a></li>
	<li class="<?php if ($this->uri->segment(2) == 'indexKab') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Dinas/indexKab"> <i class="fa fa-folder-open-o"></i>Capaian SPM Kabupaten/Kota</a></li>
</ul>
</li>

<li  class="treeview <?php 
$menu_sett_arr= array('anggaranProv','anggaranKab');
if(in_array($this->uri->segment(1), $menu_sett_arr)) {echo "active";}?>">
<a href="#">
	<i class="fa fa-user"></i>
	<span>Anggaran</span>
	<i class="fa fa-angle-left pull-right"></i>
</a>
<ul class="treeview-menu"> 
   <li class="<?php if ($this->uri->segment(2) == 'anggaranProv') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Angg/anggaranProv"> <i class="fa fa-folder-open-o"></i>Anggaran Provinsi</a></li>
	<li class="<?php if ($this->uri->segment(2) == 'anggaranKab') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Angg/anggaranKab"> <i class="fa fa-folder-open-o"></i>Anggaran Kabupaten/Kota</a></li>
</li>
</ul>


<li  class="treeview <?php 
$menu_sett_arr= array('Prov','Kab');
if(in_array($this->uri->segment(2), $menu_sett_arr)) {echo "active";}?>">

<a href="#">
	<i class="fa fa-user"></i>
	<span>Permasalahan</span>
	<i class="fa fa-angle-left pull-right"></i>
</a>
<ul class="treeview-menu"> 
   <li class="<?php if ($this->uri->segment(2) == 'Prov') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Permasalahan/Prov"> <i class="fa fa-folder-open-o"></i> Provinsi</a></li>
	<li class="<?php if ($this->uri->segment(2) == 'Kab') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Permasalahan/Kab"> <i class="fa fa-folder-open-o"></i> Kabupaten/Kota</a></li>
</li>
</ul>

<?php } ?>


<?php if ($level == "ADMINPROV"){?>
<li  class="treeview <?php 
$menu_sett_arr= array('indexProv','indexKab','generate_indikator');
if(in_array($this->uri->segment(1), $menu_sett_arr)) {echo "active";}?>">
<a href="#">
	<i class="fa fa-user"></i>
	<span>Capaian</span>
	<i class="fa fa-angle-left pull-right"></i>
</a>
<ul class="treeview-menu"> 
   <li class="<?php if ($this->uri->segment(2) == 'indexProv') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Dinas/indexProv"> <i class="fa fa-folder-open-o"></i>Capaian SPM Provinsi</a></li>
	<li class="<?php if ($this->uri->segment(2) == 'indexKab') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Dinas/indexKab"> <i class="fa fa-folder-open-o"></i>Capaian SPM Kabupaten/Kota</a></li>
</ul>
</li>

<li  class="treeview <?php 
$menu_sett_arr= array('anggaranProv','anggaranKab');
if(in_array($this->uri->segment(1), $menu_sett_arr)) {echo "active";}?>">
<a href="#">
	<i class="fa fa-user"></i>
	<span>Anggaran</span>
	<i class="fa fa-angle-left pull-right"></i>
</a>
<ul class="treeview-menu"> 
   <li class="<?php if ($this->uri->segment(2) == 'anggaranProv') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Angg/anggaranProv"> <i class="fa fa-folder-open-o"></i>Anggaran Provinsi</a></li>
	<li class="<?php if ($this->uri->segment(2) == 'anggaranKab') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Angg/anggaranKab"> <i class="fa fa-folder-open-o"></i>Anggaran Kabupaten/Kota</a></li>
</li>
</ul>


<li  class="treeview <?php 
$menu_sett_arr= array('Prov','Kab');
if(in_array($this->uri->segment(2), $menu_sett_arr)) {echo "active";}?>">

<a href="#">
	<i class="fa fa-user"></i>
	<span>Permasalahan</span>
	<i class="fa fa-angle-left pull-right"></i>
</a>
<ul class="treeview-menu"> 
   <li class="<?php if ($this->uri->segment(2) == 'Prov') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Permasalahan/Prov"> <i class="fa fa-folder-open-o"></i> Provinsi</a></li>
	<li class="<?php if ($this->uri->segment(2) == 'Kab') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Permasalahan/Kab"> <i class="fa fa-folder-open-o"></i> Kabupaten/Kota</a></li>
</li>
</ul>

<?php } ?>

<?php if ($level == "ADMINPROV"){?>
<li class="<?php 
	 $menu_home_arr= array('User', '');
	 if(in_array($this->uri->segment(1), $menu_home_arr)) {echo "active";}?>">
		<a href="<?php echo base_url(); ?>User">
			<i class="fa fa-folder-open-o"></i> Data User </a></li>   
		</a>
</li>
<?php } ?>





<?php if ($level == "ADMINKAB"){?>
<li  class="treeview <?php 
$menu_sett_arr= array('indexProv','indexKab','generate_indikator');
if(in_array($this->uri->segment(1), $menu_sett_arr)) {echo "active";}?>">
<a href="#">
	<i class="fa fa-user"></i>
	<span>Capaian</span>
	<i class="fa fa-angle-left pull-right"></i>
</a>
<ul class="treeview-menu"> 
 	<li class="<?php if ($this->uri->segment(2) == 'indexKab') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Dinas/indexKab"> <i class="fa fa-folder-open-o"></i>Capaian SPM Kabupaten</a></li>
</ul>
</li>



<li  class="treeview <?php 
$menu_sett_arr= array('anggaranProv','anggaranKab');
if(in_array($this->uri->segment(1), $menu_sett_arr)) {echo "active";}?>">
<a href="#">
	<i class="fa fa-user"></i>
	<span>Anggaran</span>
	<i class="fa fa-angle-left pull-right"></i>
</a>
<ul class="treeview-menu"> 
  	<li class="<?php if ($this->uri->segment(2) == 'anggaranKab') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Angg/anggaranKab"> <i class="fa fa-folder-open-o"></i>Anggaran Kabupaten/Kota</a></li>
</li>
</ul>


<li  class="treeview <?php 
$menu_sett_arr= array('Prov','Kab');
if(in_array($this->uri->segment(2), $menu_sett_arr)) {echo "active";}?>">

<a href="#">
	<i class="fa fa-user"></i>
	<span>Permasalahan</span>
	<i class="fa fa-angle-left pull-right"></i>
</a>
<ul class="treeview-menu"> 
	<li class="<?php if ($this->uri->segment(2) == 'Kab') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Permasalahan/Kab"> <i class="fa fa-folder-open-o"></i> Kabupaten/Kota</a></li>
</li>
</ul>
<?php } ?>

<?php if ($level == "ADMINKAB"){?>
<li class="<?php 
	 $menu_home_arr= array('User', '');
	 if(in_array($this->uri->segment(1), $menu_home_arr)) {echo "active";}?>">
		<a href="<?php echo base_url(); ?>User">
			<i class="fa fa-folder-open-o"></i> Data User </a></li>   
		</a>
</li>
<?php } ?>

<?php if ($level == "admin"){?>
<li  class="treeview <?php 
$menu_sett_arr= array('profil');
if(in_array($this->uri->segment(1), $menu_sett_arr)) {echo "active";}?>">

<a href="#">
	<i class="fa fa-gear"></i>
	<span> General Setting</span>
	<i class="fa fa-angle-left pull-right"></i>
</a>

<ul class="treeview-menu">          
	<li class="<?php if ($this->uri->segment(1) == 'profil') { echo 'active'; } ?>"><a href="<?php echo base_url();?>profil"> <i class="fa fa-folder-open-o"></i> Aplikasi</a></li>
</ul>
</li>

<?php if ($level == "admin"){?>
<li  class="treeview 
<?php 
$menu_data_arr= array('masterdinas','Indikator','Pengguna','user','Berita','About');
if(in_array($this->uri->segment(1), $menu_data_arr)) {echo "active";}?>">

<a href="#">
	<img height="20" src="<?php echo base_url().'assets/theme_admin/img/data.png'; ?>">
	<span>Master Data</span>
	<i class="fa fa-angle-left pull-right"></i>
</a>
<ul class="treeview-menu">
    <li class="<?php if ($this->uri->segment(1) == 'Berita') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Berita"> <i class="fa fa-folder-open-o"></i>Berita</a></li>
	<li class="<?php if ($this->uri->segment(1) == 'Dinas') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>masterdinas"> <i class="fa fa-folder-open-o"></i>Dinas</a></li>
	<li class="<?php if ($this->uri->segment(1) == 'Indikator') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Indikator"> <i class="fa fa-folder-open-o"></i>Indikator</a></li>
	<li class="<?php if ($this->uri->segment(1) == 'Pengguna') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>Pengguna"> <i class="fa fa-folder-open-o"></i> Data Pengguna </a></li> 
    <li class="<?php if ($this->uri->segment(1) == 'About') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>About"> <i class="fa fa-folder-open-o"></i>Tentang Aplikasi</a></li>

</ul>
</li>
<?php } ?>

<?php } ?>

</ul>