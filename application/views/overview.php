<div class="row">
	<div class="col-md-12">
<div class="box box-primary">
	
	<div class="box-body">
<form id="register_frm" class="form-horizontal" autocomplete="off">		
<div id="myWizard">
    <div class="tab-content">
      <div class="tab-pane active" id="step1">
	 
    <div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
     
   </div>
</div>
		
	</form>	
	</div>
</div>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url('/assets/highcharts/highcharts.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/highcharts/modules/exporting.js'); ?>"></script>
<script type="text/javascript">
$(document).ready(function() {
			var options = {
	            chart: {
	                renderTo: 'container',
	                type: 'column',
	                marginRight: 130,
	                marginBottom: 25
	            },
	            title: {
	                text: 'laporan Per Bulan',
	                x: -20 //center
	            },
	            subtitle: {
	                text: '',
	                x: -20
	            },
			 xAxis: {
            categories: [
                'Jan',
                'Feb',
                'Mar',
                'Apr',
                'May',
                'Jun',
                'Jul',
                'Aug',
                'Sep',
                'Oct',
                'Nov',
                'Dec'
            ],
            crosshair: true
        },
	            // xAxis: {
	                // categories: []
	            // },
				
			 // xAxis: {
            // categories: [
                // 'Jan',
                // 'Feb',
                // 'Mar',
                // 'Apr',
                // 'May',
                // 'Jun',
                // 'Jul',
                // 'Aug',
                // 'Sep',
                // 'Oct',
                // 'Nov',
                // 'Dec'
			 // ]},
            // crosshair: true
	            yAxis: {
	                title: {
	                    text: 'Requests'
	                },
	                plotLines: [{
	                    value: 0,
	                    width: 1,
	                    color: '#808080'
	                }]
	            },
	            tooltip: {
	                formatter: function() {
	                        return '<b>'+ this.series.name +'</b><br/>'+
	                        this.x +': '+ this.y;
	                }
	            },
	            legend: {
	                layout: 'vertical',
	                align: 'right',
	                verticalAlign: 'top',
	                x: -10,
	                y: 100,
	                borderWidth: 0
	            },
	            
	            series: []
	        }
	        
	        $.getJSON("data", function(json) {
				//options.xAxis.categories = json[0]['data'];
	        	options.series[0] = json[1];
	        	options.series[1] = json[2];
	        	options.series[2] = json[3];
		        chart = new Highcharts.Chart(options);
	        });
	    });
</script>
