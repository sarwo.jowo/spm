<style type="text/css">
td, div {
	font-family: "Arial","?Helvetica","?sans-serif";
}
.datagrid-header-row * {
	font-weight: bold;
}
.messager-window * a:focus, .messager-window * span:focus {
	color: blue;
	font-weight: bold;
}
.daterangepicker * {
	font-family: "Source Sans Pro","Arial","?Helvetica","?sans-serif";
	box-sizing: border-box;
}
.glyphicon	{font-family: "Glyphicons Halflings"}
</style>
<?php 
// buat tanggal sekarang
$tanggal = date('Y-m-d H:i');
$tanggal_arr = explode(' ', $tanggal);
$txt_tanggal = $tanggal;
//$txt_tanggal .= ' - ' . $tanggal_arr[1];
$tanggal2 = date('Y-m-d H:i');
?>

<!-- Data Grid -->
<table   id="dg" 
class="easyui-datagrid"
title="Daftar yang telah di laporan" 
style="width:auto; height: auto;" 
url="<?php echo site_url('Saber/ajax_list');?>" 
pagination="true" rownumbers="true" 
fitColumns="true" singleSelect="true" collapsible="true"
sortName="tgl_lapor" sortOrder="desc"
toolbar="#tb"
striped="true">
<thead>
	<tr>
		<th data-options="field:'id', sortable:'true',halign:'center', align:'center'" hidden="true">ID</th>
		<th data-options="field:'tgl_lapor',width:'20',halign:'center', align:'center'">Tanggal Lapor</th>
		<th data-options="field:'tgl_kejadian',width:'20',halign:'center', align:'center'">Tanggal Kejadian</th>
		<th data-options="field:'kegiatan',width:'20',halign:'center', align:'center'">Kegiatan</th>
		<th data-options="field:'skpd', width:'30', halign:'center', align:'left'">SKPD</th>
		<th data-options="field:'pelaku',width:'20', halign:'center', align:'left'" >Pelaku</th>		
		<th data-options="field:'nilai', width:'15', halign:'center', align:'center'">Nilai </th>
         <th data-options="field:'detail', width:'15', halign:'center', align:'center'">Detail</th>	
		
	</tr>
</thead>
</table>

<!-- Toolbar -->
<div id="tb" style="height: 35px;">
		<div class="pull-right" style="vertical-align: middle;">
		
		<select id="pr" name="pr" style="height:27px" >
			<option value=""> -- Pilih Propinsi --</option>	
			<?php	
										foreach ($prop as $r) {
										   $rg = $r->propinsi;
                                          // $kd= $r->deptid;										   
											echo '<option value="'.$r->propinsi.'">
											 '.$rg.'
											</option>';
										}
										?>
			
		</select>
		 <select name="kab" id="kab" style="height:27px">
           <option></option>
          </select>
-
		<select id="area" name="area" style="width:170px; height:27px" >
			<option value=""> -- Area --</option>	
			<?php	
										foreach ($area as $row) {
										   $ruang = $row->nama;											
											echo '<option value="'.$row->id.'">
											'.$kode.' '.$ruang.'
											</option>';
										}
										?>
			
		</select>
		<a href="javascript:void(0);" id="btn_filter" class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="doSearch()">Cari</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-print" plain="false" onclick="excel()">Export Excel</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-print" plain="false" onclick="cetak()">Cetak Laporan</a>
		<a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-clear" plain="false" onclick="clearSearch()">Hapus Filter</a>
	</div>
</div>


<script type="text/javascript">
$(document).ready(function() {

 // $('#pr').on('change', function() {
  // var value = $(this).val();
  // alert(value);
  // });  
	 $('#pr').on('change', function() {
			var pr = $(this).val();
			console.log(pr);  //menampilan pada log browser apa yang dibawa pada saat dipilih pada combo box
			$.ajax({
				url: "<?=base_url()?>Saber/getprop/",       //memanggil function controller dari url
				async: false,
				type: "POST",     //jenis method yang dibawa ke function
				data: "pr="+pr,   //data yang akan dibawa di url
				dataType: "html",
				success: function(data) {
					 $('#kab').html(data);
			        //location.reload();   //hasil ditampilkan pada combobox id=kota
				}
			})
		});



//$.datepicker.setDefaults( $.datepicker.regional[ "id" ] );
$(".dtpicker").datetimepicker({
	language:  'id',
	weekStart: 1,
	autoclose: true,
	todayBtn: true,
	todayHighlight: true,
	pickerPosition: 'bottom-right',
	//format: "dd MM yyyy - hh:ii",
	//linkField: "tgl_transaksi",
	linkFormat: "yyyy-mm-dd hh:ii"
});	


$("#kode_transaksi").keyup(function(event){
	if(event.keyCode == 13){
		$("#btn_filter").click();
	}
});

$("#kode_transaksi").keyup(function(e){
	var isi = $(e.target).val();
	$(e.target).val(isi.toUpperCase());
});

fm_filter_tgl();
}); // ready

function fm_filter_tgl() {
	$('#daterange-btn').daterangepicker({
		ranges: {
			'Hari ini': [moment(), moment()],
			'Kemarin': [moment().subtract('days', 1), moment().subtract('days', 1)],
			'7 Hari yang lalu': [moment().subtract('days', 6), moment()],
			'30 Hari yang lalu': [moment().subtract('days', 29), moment()],
			'Bulan ini': [moment().startOf('month'), moment().endOf('month')],
			'Bulan kemarin': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
		},
		showDropdowns: true,
		format: 'YYYY-MM-DD',
		tgl_lapor: moment().startOf('year').startOf('month'),
		//endDate: moment().endOf('year').endOf('month')
	},

	function(start, end) {
//$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		$('#reportrange span').html(start.format('D MMM YYYY') + ' - ' + end.format('D MMM YYYY'));
		doSearch();
		});
		}
</script>

<script type="text/javascript">
var url;
function form_select_clear() {
	$('select option')
	.filter(function() {
		return !this.value || $.trim(this.value).length == 0;
	})
	.remove();
	$('select option')
	.first()
	.prop('selected', true);	
}

function doSearch(){
  var pr = $('#pr').val();
  var kab = $('#kab').val();
  var area = $('#area').val();
  
   $('#dg').datagrid('load',{
   area: $('#area').val(),
   pr: $('#pr').val(),
   kab: $('#kab').val()
   });
  }
function clearSearch(){
	location.reload();
}


function hapus(){  
	var row = $('#dg').datagrid('getSelected');  
	if (row){ 
		$.messager.confirm('Konfirmasi','Apakah Anda akan menghapus Jadwal Meeting : <code>' + row.id_txt + '</code> ?',function(r){  
			if (r){  
				$.ajax({
					type	: "POST",
					url		: "<?php echo site_url('ruang/delete'); ?>",
					data	: 'id='+row.id,
					success	: function(result){
						var result = eval('('+result+')');
						$.messager.show({
							title:'<div><i class="fa fa-info"></i> Informasi</div>',
							msg: result.msg,
							timeout:2000,
							showType:'slide'
						});
						if(result.ok) {
							$('#dg').datagrid('reload');
						}
					},
					error : function (){
						$.messager.show({
							title:'<div><i class="fa fa-warning"></i> Peringatan !</div>',
							msg: '<div class="text-red"><i class="fa fa-ban"></i> Maaf, Terjadi kesalahan koneksi, silahkan muat ulang !</div>',
							timeout:2000,
							showType:'slide'
						});
					}
				});  
			}  
		}); 
	}  else {
		$.messager.show({
			title:'<div><i class="fa fa-warning"></i> Peringatan !</div>',
			msg: '<div class="text-red"><i class="fa fa-ban"></i> Maaf, Data harus dipilih terlebih dahulu </div>',
			timeout:2000,
			showType:'slide'
		});	
	}
	$('.messager-button a:last').focus();
}
function excel () {
	var area	 	= $('#area').val();
	var pr			= $('#pr').val();
	var kab			= $('#kab').val();

	var win = window.open('<?php echo site_url("Saber/excel/?area=' + area + '&pr=' + pr + '&kab=' + kab +'"); ?>');
	if (win) {
		win.focus();
	} else {
		alert('Popup jangan di block');
	}
}
function cetak () {
	var area	 	= $('#area').val();
	var pr			= $('#pr').val();
	var kab			= $('#kab').val();
	
	var win = window.open('<?php echo site_url("Saber/cetak_laporan/?area=' + area + '&pr=' + pr + '&kab=' + kab +'"); ?>');
	if (win) {
		win.focus();
	} else {
		alert('Popup jangan di block');
	}
}
</script>