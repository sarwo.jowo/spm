<style type="text/css">
	.row * {
		box-sizing: border-box;
	}
	.kotak_judul {
		 border-bottom: 1px solid #fff; 
		 padding-bottom: 2px;
		 margin: 0;
	}
	.box-header {
		color: #444;
		display: block;
		padding: 10px;
		position: relative;
	}
</style>
<?php
$tanggal = date('Y-m');
$txt_periode_arr = explode('-', $tanggal);
	if(is_array($txt_periode_arr)) {
		$txt_periode = jin_nama_bulan($txt_periode_arr[1]) . ' ' . $txt_periode_arr[0];
	}

?>

<div class="modal fade" role="dialog" id="confirm_del">
          <div class="modal-dialog" style="width:400px">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title"><strong>Confirmation</strong></h4>
              </div>
			 
              <div class="modal-body">
				<h4 class="text-center">Apakah anda yakin untuk menghapusnya ? </h4>
				<input type="hidden" id="del_id" value="">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>               
                <button type="button" class="btn btn-success yes_del">Delete</button>               
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
</div>

<div class="modal fade" role="dialog" id="frm_province">
          <div class="modal-dialog" style="width:400px">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add Province</h4>
              </div>
			 
              <div class="modal-body" style="padding-bottom:2px;">
				
				 <form role="form" id="form_province" autocomplete="off">
                <!-- text input -->
				
                <div class="form-group">
                  <label>Province Name</label><span class="label label-danger pull-right province_name_error"></span>
                  <input style="height:20px; width:92%" type="text" class="form-control" name="province_name" id="province_name" value="" placeholder="Province Name" >
				 <input type="hidden" name="id_province" id="id_province" value="">
                </div>
				
				 <div class="form-group">
                  <label>Status</label><span class="label label-danger pull-right status_error"></span>
                  <select class="form-control" id="status" name="status" style="width:99%">
					  <option value="">-- Pilih Status --</option>
					  <option value=1>Active</option>
					  <option value=0>Inactive</option>
				  </select>
                </div>				
			
              </form>

              </div>
              <div class="modal-footer" style="margin-top:1px;">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>               
                <button type="button" class="btn btn-success yes_save">Save</button>               
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
</div>


 <div class="box box-success">
 <div class="box-header">              
    <a href="#"><button class="btn btn-success add_province"><i class="fa fa-plus"></i> Add Province</button></a>
                
</div>
<div class="box-body">
<div class='alert alert-info alert-dismissable' id="success-alert">
   
    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
    <div id="id_text"><b>Welcome</b> </div>
</div>
	<table id="example88" class="table table-bordered table-striped">
		<thead><tr>
			<th style="text-align:center; width:6%">No.</th>
			<th style="text-align:center; width:60%">Province Name</th>
			<th style="text-align:center; width:10%">Status</th>
				
			<th style="text-align:center; width:14%">View</th>
		</tr>
		</thead>
		<tbody>
		<?php 
			if(!empty($provinces)){
				$i=1;
				foreach($provinces as $p){
					$status = null;
					if($p['status'] == 1){
						$status = '<small class="label label-success bg-green">Active</small>';
					}else if($p['status'] == 0){
						$status = '<small class="label label-danger">Inactive</small>';
					}else{
						$status = '';
					}
					$info = $p['province_id'].'_'.ucwords($p['province_name']).'_'.$p['status'];
					echo '<tr>';
					echo '<td align="center">'.$i++.'.</td>';
					echo '<td>'.ucwords($p['province_name']).'</td>';
					echo '<td align="center">'.$status.'</td>';
					echo '<td align="center" >
			<a href="#" title="Edit" id="'.$info.'" class="edit_pro"><button class="btn btn-xs btn-success"><i class="fa fa-edit"></i> Edit</button></a>
			<a href="#" title="Delete" id="'.$p['province_id'].'" class="del_pro"><button class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i> Delete</button></a></td>';
					echo '</tr>';
				}
			}else{
				echo '<tr>';
				echo '<td colspan=4 align="center"><strong>No Data</strong></td>';
				echo '</tr>';
			}
		?>
		
		
	
	</tbody>
	
	</table>
</div>

</div>

<script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
	
<script type="text/javascript">
$("#success-alert").hide();

$('.del_pro').click(function(){
	var val = $(this).get(0).id;
	$('#del_id').val(val);
	$('#confirm_del').modal({
		backdrop: 'static',
		keyboard: false
	});
	$("#confirm_del").modal('show');
});

$('.yes_del').click(function(){
	var id = $('#del_id').val();
	var url = '<?php echo site_url('province/del_pro');?>';
	$.ajax({
		data : {id : id},
		url : url,
		type : "POST",
		success:function(response){
			$('#confirm_del').modal('hide');
			$("#id_text").html('<b>Success,</b> Data provinsi telah dihapus');
			$("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
				$("#success-alert").alert('close');
				location.reload();
			});						
		}
	});
	
});
$('.yes_save').click(function(){
	var name = $('#province_name').val();
	var status = $('#status').val();
	$('.province_name_error').text('');
	$('.status_error').text('');
	if(name == ''){
		$('.province_name_error').text('Nama Provinsi harus diisi');
		return false;
	}
	if(status == ''){
		$('.status_error').text('Status harus dipilih');
		return false;
	}
	var url = '<?php echo site_url('province/save_pro');?>';
	var data = $('#form_province').serialize();
	$.ajax({
		url : url,
		data : data,
		type : "POST",
		success:function(res){			
			if(res > 0){
				$('#frm_province').modal('hide');
				$("#id_text").html('<b>Success,</b> Data provinsi telah disimpan');
				$("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
					$("#success-alert").alert('close');
					location.reload();
				});								
			}
		}
	});
});

$(function() {               
    $('#example88').dataTable({});
 });
	
$('.add_province').click(function(){
	
	$('#form_province').find("input[type=text], select, input[type=hidden]").val("");
	$('#frm_province').modal({
		backdrop: 'static',
		keyboard: false
	});
	$('#frm_province').modal('show');
});
$('.edit_pro').click(function(){
	$('#form_province').find("input[type=text], select, input[type=hidden]").val("");
	var val = $(this).get(0).id;
	var dt = val.split('_');
	$('#id_province').val(dt[0]);
	$('#province_name').val(dt[1]);
	$('#status').val(dt[2]);	
	$('#frm_province').modal({
		backdrop: 'static',
		keyboard: false
	});
	$('#frm_province').modal('show');
});
</script>
