<h3>Dasar Hukum : <?php echo $dasarhukum;?></h3>
<div class="row">
<form action="save_action" method="post">
<input type="hidden" value="<?php echo $id_tr_dinas;?>" name="id_tr_dinas"/>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th class="text-center">No</th>
                <th class="text-center">Jenis Layanan SPM</th>
                <th class="text-center" colspan="3">Indikator Pencapaian</th>                 
                <th class="text-center">Total Pencapaian</th>
            </tr>               
        </thead>
        <tbody>
            <tr>
                <td class="text-center">A.</td>
                <td colspan="5">
                    <label><?php echo $wilayah;?></label>
                    <input type="hidden" value="<?php echo $wilayah;?>" name="wilayah"/>
                </td>
            </tr>
            <?php 
            $i=1; 
            foreach ($indikator as $item):?>
            <tr>
                <td><?php echo $i?></td>
                <td>
                    <label><?php echo $item['indikator_kinerja'];?></label>
					<input type="hidden" value="<?php echo $item['id'];?>" name="id_indikator[]"/>
                    <input type="hidden" value="<?php echo $item['indikator_kinerja'];?>" name="indikator_kinerja[]"/>
                </td>
                <td colspan="3">
                    <?php echo $item['indikator_pencapaian'];?>
                    <input type="hidden" value="<?php echo $item['indikator_pencapaian'];?>" name="indikator_pencapaian[]"/>
                </td>
                <td class="text-center"></td>
            </tr>
            <tr>
                <td colspan="2"></td>
               <td class="text-center"><b>Jumlah yang harus dilayani</b></td>
                <td class="text-center"><b>Jumlah yang terlayani sesuai standar</b></td>
                <td class="text-center"><b>Jumlah yang terlayani belum sesuai standar</b></td>
				<td class="text-center"><b>Jumlah yang belum terlayani</b></td>
                <td></td>
            </tr>            
            <?php 
               //row detail
               $itemindikator=explode ("#", $item['item_indikator']); 
               foreach($itemindikator as $detail):
            ?>     
            <tr>
                <td></td>
                <td>
				<?php echo strip_tags($detail);?>
                <input type="hidden" name="itemdesc<?php echo $i?>[]" class="form-control name_list" value="<?php echo strip_tags($detail);?>" readonly /></td>
                <td>
                <input type="number" name="total<?php echo $i?>[]"  class="form-control name_list" readonly />    
                </td>
                <td>
                <input type="number" name="standard<?php echo $i?>[]"  class="form-control name_list" readonly  />
                </td>
                <td>
                <input type="number" name="nonstandard<?php echo $i?>[]"  class="form-control name_list" readonly />
                </td>
				
				<td>
                <input type="number" name="tidak_terlayani<?php echo $i?>[]"  class="form-control name_list" readonly />
                </td>
                <td>
                    <!-- <input type="number" name="itemvalue<?php echo $i?>[]"  class="form-control name_list" value="0" readonly /> -->
                </td>            
            </tr>            
            <?php             
                endforeach;
            $i++;  
            endforeach;?>            
        </tbody>
    </table>    
  
</form>
</div>