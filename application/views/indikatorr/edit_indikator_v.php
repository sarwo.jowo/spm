<h3>Dasar Hukum : <?php echo $dasarhukum;?></h3>
<div class="row">
<form action="saveedit_action" method="post">
<input type="hidden" value="<?php echo $id_tr_dinas;?>" name="id_tr_dinas"/>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th class="text-center">No</th>
                <th class="text-center">Jenis Layanan SPM</th>
                <th class="text-center" colspan="3">Indikator Pencapaian</th>                 
                <th class="text-center">Pencapaian</th>
            </tr>                       
        </thead>
        <tbody>
            <tr>
                <td class="text-center">A.</td>
                <td colspan="5">
                    <label><?php echo $wilayah;?></label>     
                    <input type="hidden" value="<?php echo $wilayah;?>" name="wilayah"/>               
                </td>
            </tr>
            <?php 
            $i=1; 
			
            foreach ($indikator as $item):?>
            <tr>
                <td><?php echo $i?></td>
                <td>
                    <label><?php echo $item['indikator_kinerja'];?></label>                    
                </td>
                <td colspan="4">
                    <?php echo $item['indikator_pencapaian'];?>                    
                </td>
                <td class="text-center">  </td>
            </tr>
            <tr>
                <td colspan="2"></td>
                <td class="text-center"><b>Jumlah yang harus dilayani</b></td>
                <td class="text-center"><b>Jumlah yang terlayani</b></td>
                <td class="text-center"><b>Jumlah yang belum terlayani</b></td>
				<?php 
				$query = $this->db->query('SELECT sum(target) as TTL, count(indikator_id) as JML FROM tbl_dinas_indikator_detail where indikator_id = '.$item['indikator_id']);
				$row = $query->row();		
				
				?>  
                <td>Persentase capaian  <?php echo round($row->TTL/$row->JML,2); echo ' %'; ?> </td>
				
				
            </tr>     
            <?php 
               //row detail
               $x=0; 
               $ttl=0;		   
               foreach($indikatordetail as $detail):
               if($detail["indikator_id"] == $item["indikator_id"])
               {
                 $nonstd =   $detail["total"] -  $detail["standard"];
            ?>       
            <tr>
                <td>
                <label></label>
                 <input type="hidden" name="id[]" value="<?php echo $detail["id"] ?>" />
                </td>                                    
                <td><?php echo $detail['deskripsi_detail'] ?></td>
                <td>
                    <?php echo $detail['total'] ?>
                </td>
                <td>
				<?php echo $detail["standard"]?>
                  </td>
                <td>
				<?php echo  $nonstd ?>               
                </td>								
                <td>
				<?php if($detail["total"]>0) {echo round($detail["standard"]/$detail["total"]*100); echo "%"; }  else echo 0 ?>
                                           
                </td>            
            </tr> 
        		
            <?php  
			$x++;
				if($detail["total"]>0)
				{
             	$ttl=($ttl + round($detail["standard"]/$detail["total"]*100)/$x);
				}				
                }                 
               endforeach;
			   ?>
			   <tr>
			   <td></td>
			   <td></td>
				<td></td>
				<td></td>
				
			   <td></td>
			   <td>
			   <input type="hidden" name="subtotal" value="<?php echo $ttl; ?>" />
			   </td>
			   </tr>
			   <?php
			   
            $i++;  
            endforeach;?>  
                 </tbody>
    </table>    


</form>
</div>