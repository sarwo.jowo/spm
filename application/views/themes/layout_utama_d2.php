<?php
$a_home 		    = $this->uri->segment(2) == 'view' ? 'active' : '';
$laporan 	= $this->uri->segment(2) == 'laporan' ? 'active' : '';


?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title><?php echo $judul_browser;?></title>
	<link rel="shortcut icon" href="<?php echo base_url(); ?>icon.ico" type="image/x-icon" />
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<!-- bootstrap 3.0.2 -->
	<link href="<?php echo base_url(); ?>assets/theme_admin/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<!-- font Awesome -->
	<link href="<?php echo base_url(); ?>assets/theme_admin/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<!-- Ionicons -->
	<link href="<?php echo base_url(); ?>assets/theme_admin/css/ionicons.min.css" rel="stylesheet" type="text/css" />
	<!-- Theme style -->
	<link href="<?php echo base_url(); ?>assets/theme_admin/css/AdminLTE.css" rel="stylesheet" type="text/css" />

	<?php 
	foreach($css_files as $file) { ?>
		<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
	<?php } ?>
	
	  <!-- calendar -->
    <link href="<?php echo base_url(); ?>assets/theme_admin/css/fullcalendar.print.css" rel="stylesheet"  media="print">
    <link href="<?php echo base_url(); ?>assets/theme_admin/css/fullcalendar.css" rel="stylesheet" >
	
	
	<link href="<?php echo base_url(); ?>assets/theme_admin/css/jquery-ui-1.8.21.custom.css" rel="stylesheet" type="text/css" />	

	<link href="<?php echo base_url(); ?>assets/theme_admin/css/custome.css" rel="stylesheet" type="text/css" />	

	<!-- jQuery 2.0.2 -->
	<script src="<?php echo base_url(); ?>assets/theme_admin/js/jquery.min.js"></script>	
	<!-- Bootstrap -->
	<script src="<?php echo base_url(); ?>assets/theme_admin/js/bootstrap.min.js" type="text/javascript"></script>
	
	<script src="<?php echo base_url(); ?>assets/theme_admin/js/jqClock.min.js" type="text/javascript"></script>

	<?php foreach($js_files as $file) { ?>
		<script src="<?php echo $file; ?>"></script>
	<?php } ?>

	<!-- AdminLTE App -->
	<script src="<?php echo base_url(); ?>assets/theme_admin/js/AdminLTE/app.js" type="text/javascript"></script>
	
	 <script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>

	<?php foreach($js_files2 as $file) { ?>
		<script src="<?php echo $file; ?>"></script>
	<?php } ?>
	<!-- Waktu -->
	<script type="text/javascript">
    $(document).ready(function(){    
      $(".jam").clock({"format":"24","calendar":"false"});
    });    
  </script>
	
</head>
<body class="skin-blue">
	<!-- header logo: style can be found in header.less -->
	<header class="header">
		<a href="<?php echo site_url();?>login" class="logo">
			<!-- Add the class icon to your logo image or logo icon to add the margining -->
			<div style="text-align:center;"><img height="50" src="<?php echo site_url().'assets/theme_admin/img/logo4.png'; ?>"></div>
		</a>
		<!-- Header Navbar: style can be found in header.less -->
		<nav class="navbar navbar-static-top" role="navigation">
					
			<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
			     	<li class="<?php echo $a_home; ?>"><a href="<?php echo site_url('depan'); ?>">Halaman Utama</a></li>					
					<li class="<?php echo $laporan; ?>"><a href="<?php echo site_url('depan/berita'); ?>">Berita</a></li>						
					<li class="<?php echo $laporan; ?>"><a href="<?php echo site_url('depan/tentang'); ?>">Tentang Program</a></li>
					<li class="<?php echo $laporan; ?>"><a href="<?php echo site_url('login'); ?>">Login</a></li>
					
			</ul>
		
		</div><!--/.nav-collapse -->	
			
	</header>
	
	 <br>
	<div class="container">

	

	<div class="row">
		<div class="box box-primary">
			<div class="box-body">			
				
			<section class="content">
				<?php
				if (! empty ($isi)){
					echo $isi;
				}
				?>

			</section><!-- /.content -->

			</div><!--box-p -->
		</div><!--box-body -->
	</div><!--row -->
	<div class="row">
	<footer class="">
	
   <div align="center"><strong>Copyright &copy; 2019 <a href="http://www.bangda.kemendagri.go.id">Dirjen Bina Pembangunan Daerah (Dirjen BANGDA) </a>.</strong><br> Jl. Taman Makam Pahlawan No. 20, Kalibata, RT.6/RW.7, RT.12/RW.5, Rawajati, Kec. Pancoran, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12750</div>  
   </footer>
</div><!--row -->   


</div>
	
		
</body>
</html>