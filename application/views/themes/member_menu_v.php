<?php
$a_home 			= $this->uri->segment(2) == 'home_view' ? 'active' : '';
$a_lap_simpanan 	= $this->uri->segment(2) == 'lap_simpanan' ? 'active' : '';
$a_lap_pinjaman 	= $this->uri->segment(2) == 'lap_pinjaman' ? 'active' : '';
$a_lap_bayar	 	= $this->uri->segment(2) == 'lap_bayar' ? 'active' : '';
$a_ubah_pic		 	= $this->uri->segment(2) == 'ubah_pic' ? 'active' : '';
$a_ubah_pass	 	= $this->uri->segment(2) == 'ubah_pass' ? 'active' : '';
$a_ajuan_list	 	= $this->uri->segment(2) == 'pengajuan' ? 'active' : '';
$a_ajuan_baru	 	= $this->uri->segment(2) == 'pengajuan_baru' ? 'active' : '';

?>

<!-- Static navbar -->
<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
			     	<li class="<?php echo $a_home; ?>"><a href="<?php echo site_url('member/view'); ?>">Beranda</a></li>					
					<li class="<?php echo $a_lap_simpanan; ?>"><a href="<?php echo site_url('member/lap_simpanan'); ?>">Berita</a></li>						
					<li class="<?php echo $a_lap_pinjaman; ?>"><a href="<?php echo site_url('member/lap_pinjaman'); ?>">Tentang Program</a></li>
					<li class="<?php echo $a_home; ?>"><a href="<?php echo site_url('login'); ?>">Login Sistem</a></li>
			</ul>
		
		</div><!--/.nav-collapse -->
	</div><!--/.container-fluid -->
</nav>