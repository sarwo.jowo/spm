<style type="text/css">
td, div {
	font-family: "Arial","?Helvetica","?sans-serif";
}
.datagrid-header-row * {
	font-weight: bold;
}
.messager-window * a:focus, .messager-window * span:focus {
	color: blue;
	font-weight: bold;
}
.daterangepicker * {
	font-family: "Source Sans Pro","Arial","?Helvetica","?sans-serif";
	box-sizing: border-box;
}
.glyphicon	{font-family: "Glyphicons Halflings"}
</style>
<?php 
// buat tanggal sekarang
$tanggal = date('Y-m-d H:i');
$tanggal_arr = explode(' ', $tanggal);
$txt_tanggal = $tanggal;
//$txt_tanggal .= ' - ' . $tanggal_arr[1];
$tanggal2 = date('Y-m-d H:i');
?>
				   <div class="row">
                        <!-- left column -->
                        <div class="col-md-6">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Laporan Pungli</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <form role="form" id="register_frm"  method="post" >
                                    <div class="box-body">
								        <div class="form-group">
                                         	<input type="hidden" name="tgl_masuk_txt" class="form-control" id="tgl_masuk_txt"  required="true" readonly="readonly" />						                     
                                             <input type="hidden" name="tgl_masuk" class="form-control"  id="tgl_masuk" />
				                         </div>
	                                    <div class="form-group">
                                        <label>Tanggal Kejadian:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control"  name="tglsurat"  id="tglsurat" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask/>
                                        </div><!-- /.input group -->
                                         </div><!-- /.form group -->

										 
									   <div class="form-group">									   
                                         <label for="asalsurat">Pilih Area</label>                                            
										 <select id="area" name="area" class="form-control" required="true">
										<option value="0"> - Pilih Area -</option>			
										<?php	
										foreach ($area as $row) {
											if(strlen($row->st) != 1){
												$kode ='';
												$ruang = $row->nama;
											}else{
												$kode ='';
												$ruang = $row->nama;
											}
											echo '<option value="'.$row->id.'">
											'.$kode.' '.$ruang.'
											</option>';
										}
										?>
									</select>									  
									  </div>
									  <div class="form-group">
                                            <label>Kegiatan</label>
                                            <textarea class="form-control" id="kegiatan"  name="kegiatan" rows="3" placeholder="Enter ..."></textarea>
                                        </div>
										 <div class="form-group">
                                            <label for="SKPD">SKPD</label>
                                            <input type="text" id="skpd" name="skpd" class="form-control"  placeholder="SKPD">
                                        </div> 
										 <div class="form-group">
                                            <label for="pelaku">Pelaku</label>
                                            <input type="text" id="pelaku" name="pelaku" class="form-control"  placeholder="Pelaku">
                                        </div> 
										<div class="form-group">
                                            <label for="modus">Modus</label>
                                            <input type="text" id="modus" name="modus" class="form-control"  placeholder="Modus">
                                        </div> 
									  <div class="form-group">
                                            <label for="modus">Nilai</label>
                                            <input type="number" id="nilai" name="nilai" class="form-control"  placeholder="Nilai">
                                        </div> 
		
												 
									<div class="box-footer">
									 <a href="javascript:void(0)" class="btn btn-primary" onclick="save()">Simpan</a>
                                       </form>	
                                    </div>	
                               
                                    </div><!-- /.box-body -->                                
                              
                            </div><!-- /.box -->         
                                
                            </div><!-- /.box -->
							
							 <div class="col-md-6">
                              <div class="box box-warning">
                                <div class="box-header">
                                    <h3 class="box-title">Datang Pendukung</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">                                   
                              	 <form id="fileupd" action="upload/do_uploads" method="POST" enctype="multipart/form-data">
								<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
								<div class="row fileupload-buttonbar">
									<div class="col-lg-7">							
									
									   <div class="form-group">
                                            <label for="file">File input</label>
                                            <input type="file" id="file" class="file-loading" placeholder="Enter ..." disabled/>
                                            <p class="help-block">Pilih File Surat</p>
                                          </div> 
							            <!-- 
										<button type="submit" class="btn btn-primary start" onclick="disable()">
											<i class="glyphicon glyphicon-upload"></i>
											<span>Start upload</span>
										</button> 
										-->
										<a href="javascript:void(0)" class="btn btn-primary" onclick="disable()">Upload File</a>	
										
                                     </div>
									<p>&nbsp;</p>									 
						      </div>
							   <div class="box-body no-padding">
                                   
                                    <div class="table-responsive">
                                        <!-- .table - Uses sparkline charts-->
                                        <table class="table table-striped">
										<?php
										// $i = 1;
										// foreach($file->result() as $row)
										//{
									  ?>   
			  
                                            <tr>
                                                <th>No</th>
                                                <th>File Name</th>
                                                <th>Tools</th>
                                            </tr>                                                                                     
											
                                        <?php
											//$i++;
											//}
										  echo '</table>';										 
										  ?>
                                    </div>
                                </div><!-- /.box-body--> 
							  
							</form>													   
				            </div>				
                          </div>	  
			  
			      
    
        <!-- InputMask -->
        <script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>
        <!-- date-range-picker -->
        <script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <!-- bootstrap color picker -->
        <script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript"></script>
        <!-- bootstrap time picker -->
        <script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
		<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/vendor/jquery.ui.widget.js"></script>
		<!-- The Templates plugin is included to render the upload/download listings -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/tmpl.min.js"></script>
		<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/load-image.all.min.js"></script>
		<!-- The Canvas to Blob plugin is included for image resizing functionality -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/canvas-to-blob.min.js"></script>
		<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
		<!-- blueimp Gallery script -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/jquery.blueimp-gallery.min.js"></script>
		<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/jquery.iframe-transport.js"></script>
		<!-- The basic File Upload plugin -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/jquery.fileupload.js"></script>
		<!-- The File Upload processing plugin -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/jquery.fileupload-process.js"></script>
		<!-- The File Upload image preview & resize plugin -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/jquery.fileupload-image.js"></script>
		<!-- The File Upload audio preview plugin -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/jquery.fileupload-audio.js"></script>
		<!-- The File Upload validation plugin -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/jquery.fileupload-validate.js"></script>
		<!-- The File Upload user interface plugin -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/jquery.fileupload-ui.js"></script>
		<!-- The main application script -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/main.js"></script>
		<!-- blueimp Gallery styles -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/theme_admin/upload/css/blueimp-gallery.min.css">
		<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/theme_admin/upload/css/jquery.fileupload.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/theme_admin/upload/css/jquery.fileupload-ui.css">
		<!-- CSS adjustments for browsers with JavaScript disabled -->
		<noscript><link rel="stylesheet" href="<?php echo base_url(); ?>assets/theme_admin/upload/css/jquery.fileupload-noscript.css"></noscript>
		<noscript><link rel="stylesheet" href="<?php echo base_url(); ?>assets/theme_admin/upload/css/jquery.fileupload-ui-noscript.css"></noscript>

      	   
       <script type="text/javascript">
	   
            $(function() {
                //Datemask dd/mm/yyyy
                $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
                //Datemask2 mm/dd/yyyy
                $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
                //Money Euro
                $("[data-mask]").inputmask();
                //Date range picker             
                // var url2 = '<?php echo site_url('http://localhost/esapa/Saber/uploadfile');?>';
				// location = location[url2]
				//window.location = location.href;
                //Timepicker
                $(".timepicker").timepicker({
                    showInputs: false
                });
				$('#tgl_masuk_txt').val('<?php echo $txt_tanggal;?>');
	            $('#tgl_masuk').val('<?php echo $tanggal;?>');	
				
				//$.datepicker.setDefaults( $.datepicker.regional[ "id" ] );
				$(".dtpicker").datetimepicker({
				language:  'id',
				weekStart: 1,
				autoclose: true,
				todayBtn: true,
				todayHighlight: true,
				pickerPosition: 'bottom-right',
			linkFormat: "yyyy-mm-dd hh:ii"
             });			
            });			
		   function hapus() {
			$.messager.alert('Warning','Delete File Surat');	
			}
			function disable(){
				$("#rbutton").attr("disabled","disabled");
			}
	       function save() {			   
			var data = $("#register_frm").serialize();			
			var kegiatan = $('#kegiatan').val();   
			if(kegiatan == ''){	
			$.messager.alert('Warning','Maaf, kegiatan harus di isi');          			
				return false;
			}	
			
			var skpd = $('#skpd').val();   
			if(skpd == ''){	
                  $.messager.alert('Warning','lengkapi isian skpd');
			return false;
			}
            var pelaku = $('#pelaku').val();   
			if(pelaku == ''){	
                  $.messager.alert('Warning','lengkapi isian pelaku');
			return false;
			}	
            var nilai = $('#nilai').val();   
			if(nilai == ''){	
                  $.messager.alert('Warning','lengkapi isian nilai hanya bisa dengan angka');
			return false;
			}	
            function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
            }			
			
			var url = '<?php echo site_url('Saber/masuk');?>';
			
			$.ajax({
				//alert('testing');
				type	: "POST",
				url		: url,
				data	: data,
				success	: function(result) {
					var result = eval('('+result+')');								
					if(result.ok) {						
						$.messager.show({
							title:'<div>Informasi</div>',
							msg: result.msg,
							timeout:2000,
							showType:'slide'                        							
							
						});						
						window.location.replace(window.location.href + "/uploadfile");					
						
					}else{
						$.messager.show({
							title:'<div>Informasi</div>',
							msg: result.msg,
							timeout:2000,
							showType:'slide'
							
							
						});						
					}
				}
			});			
		   }		
        </script>	
       