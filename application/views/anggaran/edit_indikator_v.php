<h3>Dasar Hukum : <?php echo $dasarhukum; ?></h3>
<div class="row">
    <form action="saveedit_action" method="post">
        <input type="hidden" value="<?php echo $id_tr_dinas; ?>" name="id_tr_dinas" />
        <table id="myTable" class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">Jenis Layanan SPM</th>
                    <th class="text-center" colspan="3">Anggaran</th>

                </tr>
                <tr>
                    <th class="text-center">(1)</th>
                    <th class="text-center">(2)</th>
                    <th class="text-center" colspan="3"></th>

                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="text-center">A.</td>
                    <td colspan="4">
                        <label><?php echo $wilayah; ?></label>
                        <input type="hidden" value="<?php echo $wilayah; ?>" name="wilayah" />
                    </td>
                </tr>
                <?php
                $i = 1;
                $a = 0;


                foreach($indheader as $header) : ?>
                <tr>
                        <td><?php echo $i ?></td>
                        <td>
                            <label id="lbindikatork">
                                <?php echo $header['indikator_kinerja']; ?>                                
                            </label>
                        </td>
                        <td colspan="3">
                            <label id="lbindikatorp">
                            <?php echo $header['indikator_pencapaian']; ?>
                            </label>
                            
                            <!--  -->
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Kegiatan</td>
                        <td class="text-center"><b>Pagu</b></td>
                        <td class="text-center"><b>Target</b></td>
                        <td class="text-center"><b>Capaian</b></td>
                    </tr>
                    
                <?php
                foreach ($indikator as $item) : if ($header['indikator_kinerja']==$item['indikator_kinerja']) { ?>
                    
                    <tr>
                        <td>
                            <label></label>
                            <input type="hidden" name="ik[]" value="<?php echo $item['indikator_kinerja']; ?>"/>
                            <input type="hidden" name="ip[]" value="<?php echo $item['indikator_pencapaian']; ?>"/>
                            <input type="hidden" name="id_indikator[]" value="<?php echo $header["id_indikator"] ?>" />
                            <input type="hidden" name="indikator_id[]" value="<?php echo $item["indikator_id"] ?>" />
                        </td>
                        <td>
                            <input type="text" name="kegiatan[]" value="<?php echo $item["kegiatan"] ?>" class="form-control name_list" />
                        </td>
                        <td>
                            <input type="number" name="pagu[]" value="<?php echo $item["pagu"] ?>" class="form-control name_list" />
                        </td>
                        <td>
                            <input type="number" name="targetang[]" value="<?php echo $item["targetang"] ?>" class="form-control name_list" />
                        </td>
                        <td>
                            <input type="number" name="capaian[]" value="<?php echo $item["capaian"] ?>" class="form-control name_list" Disabled>
                        </td>
                    </tr>                            
                <?php

                    
                    $a++;
                }
                   // echo $i;
                endforeach;?>
                <tr>
                        <td colspan="5">
                        <button type="button" id="add" data-ik="<?php echo $header['indikator_kinerja']; ?>" data-ikid="<?php echo $header['id_indikator']; ?>" data-ip="<?php echo $header['indikator_pencapaian']; ?>" class="btn btn-warning btn_add">Tambah anggaran</button>
                        </td>
                    </tr>
            <?php   
            $i++;
            endforeach;
                ?>

                
                
            </tbody>
        </table>
        

        <?php if ($this->session->userdata('level') != "SUBDIT") { ?>
            <button type="submit" class="btn btn-success">Simpan &MediumSpace; </button>
            <a href="<?php echo site_url('Angg') ?>" class="btn btn-default">Kembali &MediumSpace; </a>

        <?php
        }
        ?>

    </form>
</div>


<script type="text/javascript">

    $(document).ready(function(){
        
        $(document).on('click', '.btn_add', function(){            

//jika mau nambahnya disetiap baris
                        

            var ik = $(this).data("ik");
            var ip = $(this).data("ip");
            var ip = $(this).data("ip");
            var idk = $(this).data("ikid");            
            console.log(ik);
            console.log(ip);
            console.log("indikator id:"+idk);
           // var header='<tr><td></td><td><input type="text" name="ind_kinerja[]" placeholder="Indikator Kinerja" class="form-control name_list" /></td><td colspan="3"><input type="text" placeholder="indikator Pencapaian" name="ind_pencapaian[]" class="form-control name_list" /></td></tr>';
           // var subheader = '<tr><td></td><td>Kegiatan</td><td class="text-center"><b>Pagu</b></td><td class="text-center"><b>Target</b></td><td class="text-center"><b>Capaian</b></td></tr>';
            var html='<tr><td><input type="hidden" name="ik[]" value="'+ik+'"/><input type="hidden" name="ip[]" value="'+ip+'"/> <input type="hidden" name="indikator_id[]" value="0" /><input type="hidden" name="id_indikator[]" value="'+idk+'" /><button type="button" name="remove" class="btn btn-danger btn_remove">X</button></td><td><input type="text" name="kegiatan[]" Placeholder="Kegiatan" class="form-control name_list" /></td><td><input type="number" name="pagu[]" value="0" class="form-control name_list" /></td><td><input type="number" value="0" name="targetang[]" class="form-control name_list" /></td><td><input type="number" value="0" name="capaian[]"  class="form-control name_list" /></td></tr>';

            //appent to new row
            var rowid=$(this).closest('tr').index();
            var currentrow = parseInt(rowid)-1;
            console.log(rowid);
             $('#myTable > tbody > tr').eq(currentrow).after(html);            
        });

        $(document).on('click', '.btn_remove', function(){            
           // $(this).closest('tr').next().remove(); //remove row header kegiatan
           // $(this).closest('tr').next().remove(); //remove row input kegiatan
            $(this).parents("tr").remove(); //remove indikator
        });
    })
    
</script>