<style>
.form-control{
	height:17px;
	width:30%;
}
.panel-body{
	font-size:14px;
}
</style>


<div class="row">
	<div class="col-md-12">
		<div class="box box-solid box-primary">
		
		<div class="box-body">
          <div class="row">
	 <div class="row">		 
		<div class="col-md-6 col-xs-6" style="border-right: 1px solid #fff;  width:47%">
     		<div class="box box-solid box-primary">
			<div class="box-header">
				<h3 class="box-title">Annoucements</h3>
				<div class="box-tools pull-right">
					<button class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<div class="box-body" style="display:block">
           


		<table class="table table-striped table-condensed">
			
            
			<tr>
				<td class="col-md-8"><b>Beginner package</b></br>
					Due to high feedback on promotion package, we are proud to announce package for Beginner. Beginner package comes with 5% of Direct sponsor bonus, 5% of Pairing bonus. Beginner/ Starter 1 package been given 90 days period to upgrade to higher package, else the account will remained as Beginner/ Starter 1 package after the end of 90 days. Terms and conditions applied.
				</td>
				<td class="col-md-4">May 18, 2016</td>
			</tr>
			
			<tr>
				<td class="col-md-8"><b>New features added</b></br>
					1. User search is now available under genealogy tree, downline account can be search by account username. Quick placement button also being included, by clicking to latest placement, it will redirect to empty placement position for new account registration.<br/> 2. New registered account will directly pushing RAP to Posting point without have to reconvert it from RAP to Posting point.<br/> 3. Automation process for RAP to Posting point with minimum 10.00 under RAP will be automatically convert from RAP to Posting point daily. 
				</td>
				<td class="col-md-4">May 18, 2016</td>
			</tr>
			
			<tr>
				<td class="col-md-8"><b>GTC 2 Buy</b></br>
					Currently experiencing unable to purchase GTC 2 which posted by members, technical team is resolving the issue right now. Sorry for inconveniences caused.
				</td>
				<td class="col-md-4">May 16, 2016</td>
			</tr>
			
			<tr>
				<td class="col-md-8"><b>GTC 2</b></br>
					GTC 2 will be open for trading on 15th May 2016
				</td>
				<td class="col-md-4">May 11, 2016</td>
			</tr>
			
			<tr>
				<td class="col-md-8"><b>End of promotion for Starter 1 Package</b></br>
					Starter 1 package will ended it promotion by 15th May 2016.
				</td>
				<td class="col-md-4">May 11, 2016</td>
			</tr>
			
		</table>

            </div>
		</div>  
        </div>
		
		<div class="col-md-6 col-xs-6" style="border-left: 1px solid #fff; width:47%">          	  
	            
        <div class="box box-solid box-primary">
			<div class="box-header">
				<h3 class="box-title">Summary</h3>
				<div class="box-tools pull-right">
					<button class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<div class="box-body" style="display:block">
             <table style="border-left: 1px solid #fff; width:100%">
			 <tr>
			 <td class="box-header">
			 	<div class="small-box bg-yellow">
			<div class="inner">
				<h2 class="kotak_judul"><?php echo $cash;?></h2>
				<table>
					<tr>
						
						<td>Cash Points</td>
					</tr>
				</table>
			</div>
			<div class="icon">
				<i class="fa fa-money"></i>
			</div>
			<a class="small-box-footer" href="<?php echo site_url('record');?>">
				More info
				<i class="fa fa-arrow-circle-right"></i>
			</a>
		</div>
			 
			 
			 </td>
			 <td class="box-header">
			
		<div class="small-box bg-green">
			<div class="inner">
				<h2 class="kotak_judul"> <?php echo $active;?></h2>
				<table>
					<tr>
						
						<td> &nbsp; Activation Points</td>
					</tr>					
				</table>
			</div>
			<div class="icon">
				<i class="ion ion-ios7-briefcase-outline"></i>
			</div>
			<a class="small-box-footer" href="<?php echo site_url('record/activation');?>">
				More info
				<i class="fa fa-arrow-circle-right"></i>
			</a>
		</div>
		       </td>
			 </tr>
			 <tr>
			 <td class="box-header">
			 
			 
			 <div class="small-box bg-purple">
			<div class="inner">
				<h2 class="kotak_judul"> <?php echo $register;?> </h2>
				<table>
					<tr>
					<td> &nbsp; Register Points</td>
					</tr>					
				</table>
			</div>
			<div class="icon">
				<i class="fa fa-book"></i>
			</div>
			<a class="small-box-footer" href="<?php echo site_url('record/regpoints');?>">
				More info
				<i class="fa fa-arrow-circle-right"></i>
			</a>
		</div>
			 </td>
			 <td class="box-header">
					<div class="small-box bg-red">
			<div class="inner">
				<h2 class="kotak_judul"> <?php echo $shop;?></h2>
				<table>
					<tr>
						<td> &nbsp; Shooping Points</td>
					</tr>
					</table>
			</div>
			<div class="icon">
				<i class="ion ion-person-add"></i>
			</div>
			<a class="small-box-footer" href="<?php echo site_url('record/shoppoints');?>">
				More info
				<i class="fa fa-arrow-circle-right"></i>
			</a>
		</div>
		
		       </td>
			 </tr>	 
			 
			 <tr>
			 <td class="box-header">
			 		<div class="small-box bg-red">
			<div class="inner">
				<h2 class="kotak_judul"><?php echo $reseller;?> </h2>
				<table>
					<tr>
					<td>Reseller Adversiting</td>
					</tr>					
				</table>
			</div>
			<div class="icon">
				<i class="fa fa-calendar"></i>
			</div>
			<a class="small-box-footer" href="<?php echo site_url('record/reseller');?>">
				More info
				<i class="fa fa-arrow-circle-right"></i>
			</a>
		</div>
			 
			 
			 </td>
			 <td class="box-header">
			
		<div class="small-box bg-red">
			<div class="inner">
				<h2 class="kotak_judul"> <?php echo $redemption;?> </h2>
				<table>
					<tr>
					<td> Redemtion points</td>
					</tr>					
				</table>
			</div>
			<div class="icon">
				<i class="fa fa-calendar"></i>
			</div>
			<a class="small-box-footer" href="<?php echo site_url('record/redempoints');?>">
				More info
				<i class="fa fa-arrow-circle-right"></i>
			</a>
		</div>
		       </td>
			 </tr>
			 <tr>
			 <td class="box-header">
			 		<div class="small-box bg-red">
			<div class="inner">
				<h2 class="kotak_judul"> <?php echo $post;?></h2>
				<table>
					<tr>
					<td>Posting Points</td>
					</tr>
				</table>
			</div>
			<div class="icon">
				<i class="fa fa-calendar"></i>
			</div>
			<a class="small-box-footer" href="<?php echo site_url('record/posting');?>">
				More info
				<i class="fa fa-arrow-circle-right"></i>
			</a>
		</div>
			 
			 
			 
			 </td>
			 <td class="box-header">
			 
			 
			 		<div class="small-box bg-red">
			<div class="inner">
				<h2 class="kotak_judul"> <?php echo $gtc;?></h2>
				<table>
					<tr>
				    <td>e-share</td>
					</tr>					
				</table>
			</div>
			<div class="icon">
				<i class="fa fa-calendar"></i>
			</div>
			<a class="small-box-footer" href="<?php echo site_url('record/gtcc');?>">
				More info
				<i class="fa fa-arrow-circle-right"></i>
			</a>
		</div>
		
		       </td>
			 </tr>
			 
	
			 		

            </div>
	 
        </div>
		
		
		</div>
        </div>		
	</div>	  
  </div>	  
</div>	  
</div>


