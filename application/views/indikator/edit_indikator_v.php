<?php if ($dinasid == 4) { ?>
    <h3>Dasar Hukum.: <?php echo $dasarhukum; ?></h3>
    <div class="row">
        <form action="saveedit_action" method="post">
            <input type="hidden" value="<?php echo $id_tr_dinas; ?>" name="id_tr_dinas" />
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">No</th>
                        <th class="text-center">Jenis Layanan SPM</th>
                        <th class="text-center" colspan="3">Indikator Pencapaian</th>
                        <th class="text-center">Pencapaian</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center">A.</td>
                        <td colspan="5">
                            <label><?php echo $wilayah; ?></label>
                            <input type="hidden" value="<?php echo $wilayah; ?>" name="wilayah" />
                        </td>
                    </tr>
                    <?php
                    $i = 1;

                    foreach ($indikator as $item) : ?>
                        <tr>
                            <td>
                                <?php
                                if ($i == 2) {
                                    $no = '';
                                } else {
                                    $no = $i;
                                }
                                echo $no;
                                ?></td>
                            <td>
                                <label><?php echo $item['indikator_kinerja']; ?></label>
                            </td>
                            <td>

                                <?php
                                if ($i == 1) {

                                    echo "Jumlah warga negara yang berhak mendapat layanan";
                                } else if ($i == 2) {
                                    echo "Jumlah pelaksanaan penegakan Perda/Perkada";
                                } else if ($i == 3) {
                                    echo "Jumlah yang harus dilayani";
                                } else if ($i == 4) {
                                    echo "Jumlah yang harus dilayani";
                                } else if ($i == 5) {
                                    echo "Jumlah yang harus dilayani";
                                } else {
                                    echo "Jumlah kebakaran";
                                }

                                ?>

                            </td>
                            <td class="text-center"> <?php
                                                        if ($i == 1) {

                                                            echo "Jumlah warga negara yang terlayani";
                                                        } else if ($i == 2) {
                                                            echo "Jumlah penegakan perda/perkada  yang sesuai mutu layanan dasar";
                                                        } else if ($i == 3) {
                                                            echo "jumlah yang telah dilayani sesuai standar";
                                                        } else if ($i == 4) {
                                                            echo "jumlah yang telah dilayani sesuai standar";
                                                        } else if ($i == 5) {
                                                            echo "jumlah yang telah dilayani sesuai standar";
                                                        } else {
                                                            echo "jumlah yang telah dilayani sesuai standar";
                                                        }

                                                        ?></td>
                            <td class="text-center"> <?php
                                                        if ($i == 1) {

                                                            echo "Jumlah belum terlayani";
                                                        } else if ($i == 2) {
                                                            echo "Jumlah penegakan perda/perkada  yang tidak sesuai mutu layanan dasar";
                                                        } else if ($i == 3) {
                                                            echo "belum terlayani";
                                                        } else if ($i == 4) {
                                                            echo "belum terlayani";
                                                        } else if ($i == 5) {
                                                            echo "belum terlayani";
                                                        } else {
                                                            echo "belum terlayani";
                                                        }
                                                        ?></td>
                            <td class="text-center">Capaian
                                <?php
                                $query = $this->db->query('SELECT sum(target) as TTL, count(indikator_id) as JML FROM tbl_dinas_indikator_detail where indikator_id = ' . $item['indikator_id']);
                                $row = $query->row();

                                ?>
                                <?php echo round($row->TTL / $row->JML, 2);
                                echo ' %'; ?>


                            </td>


                        </tr>




                        <?php
                        //row detail
                        $x = 0;
                        $ttl = 0;
                        foreach ($indikatordetail as $detail) :
                            if ($detail["indikator_id"] == $item["indikator_id"]) {
                                $nonstd =   $detail["total"] -  $detail["standard"];

                        ?>
                                <tr>
                                    <td>
                                        <label></label>
                                        <input type="hidden" name="id[]" value="<?php echo $detail["id"] ?>" />
                                    </td>
                                    <td><?php echo $detail['deskripsi_detail'] ?></td>
                                    <td>
                                        <input type="number" name="total[]" value="<?php echo $detail["total"] ?>" class="form-control name_list" />
                                    </td>
                                    <td>
                                        <input type="number" name="standard[]" value="<?php echo $detail["standard"] ?>" class="form-control name_list" />
                                    </td>
                                    <td>
                                        <input type="number" name="nonstandard[]" value="<?php echo $nonstd; ?>" class="form-control name_list" readonly />
                                    </td>


                                    <td>
                                        <input type="text" name="target[]" value="<?php if ($detail["total"] > 0) {
                                                                                        echo round($detail["standard"] / $detail["total"] * 100);
                                                                                        echo "%";
                                                                                    } else echo 0 ?>" class="form-control name_list" readonly />
                                    </td>
                                </tr>

                        <?php
                                $x++;
                                if ($detail["total"] > 0) {
                                    $ttl = ($ttl + round($detail["standard"] / $detail["total"] * 100) / $x);
                                }
                            }
                        endforeach;
                        ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td></td>
                            <td>
                                <input type="hidden" name="subtotal" value="<?php echo $ttl; ?>" />
                            </td>
                        </tr>
                    <?php

                        $i++;
                    endforeach; ?>
                </tbody>
            </table>
            <?php if ($this->session->userdata('level') != "SUBDIT") { ?>
                <input type="hidden" name="jumlahrow" value="<?php echo count($indikatordetail) ?>" />
                <button type="submit" class="btn btn-success">Simpan &MediumSpace; </button>
                <a href="<?php echo site_url('Dinas') ?>" class="btn btn-default">Kembali &MediumSpace; </a>

            <?php
            }
            ?>

        </form>
    </div>
<?php } else if ($dinasid == 6) { ?>

    <h3>Dasar Hukum.: <?php echo $dasarhukum; ?></h3>
    <div class="row">
        <form action="saveedit_action" method="post">
            <input type="hidden" value="<?php echo $id_tr_dinas; ?>" name="id_tr_dinas" />
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">No</th>
                        <th class="text-center">Jenis Layanan SPM</th>
                        <th class="text-center" colspan="3">Indikator Pencapaian</th>
                        <th class="text-center">Pencapaian</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center">A.</td>
                        <td colspan="5">
                            <label><?php echo $wilayah; ?></label>
                            <input type="hidden" value="<?php echo $wilayah; ?>" name="wilayah" />
                        </td>
                    </tr>
                    <?php
                    $i = 1;

                    foreach ($indikator as $item) : ?>
                        <tr>
                            <td>
                                <?php
                                if ($i == 2) {
                                    $no = '';
                                } else {
                                    $no = $i;
                                }
                                echo $no;
                                ?></td>
                            <td>
                                <label><?php echo $item['indikator_kinerja']; ?></label>
                            </td>
                            <td>

                                <?php
                                if ($i == 1) {

                                    echo "Jumlah warga negara yang berhak mendapat layanan";
                                } else if ($i == 2) {
                                    echo "Jumlah pelaksanaan penegakan Perda/Perkada";
                                } else if ($i == 3) {
                                    echo "Jumlah yang harus dilayani";
                                } else if ($i == 4) {
                                    echo "Jumlah yang harus dilayani";
                                } else if ($i == 5) {
                                    echo "Jumlah yang harus dilayani";
                                } else {
                                    echo "Jumlah kebakaran";
                                }

                                ?>

                            </td>
                            <td class="text-center"> <?php
                                                        if ($i == 1) {

                                                            echo "Jumlah warga negara yang terlayani";
                                                        } else if ($i == 2) {
                                                            echo "Jumlah penegakan perda/perkada  yang sesuai mutu layanan dasar";
                                                        } else if ($i == 3) {
                                                            echo "jumlah yang telah dilayani sesuai standar";
                                                        } else if ($i == 4) {
                                                            echo "jumlah yang telah dilayani sesuai standar";
                                                        } else if ($i == 5) {
                                                            echo "jumlah yang telah dilayani sesuai standar";
                                                        } else {
                                                            echo "jumlah yang telah dilayani sesuai standar";
                                                        }

                                                        ?></td>
                            <td class="text-center"> <?php
                                                        if ($i == 1) {

                                                            echo "Jumlah belum terlayani";
                                                        } else if ($i == 2) {
                                                            echo "Jumlah penegakan perda/perkada  yang tidak sesuai mutu layanan dasar";
                                                        } else if ($i == 3) {
                                                            echo "belum terlayani";
                                                        } else if ($i == 4) {
                                                            echo "belum terlayani";
                                                        } else if ($i == 5) {
                                                            echo "belum terlayani";
                                                        } else {
                                                            echo "belum terlayani";
                                                        }
                                                        ?></td>
                            <td class="text-center">Capaian
                                <?php
                                $query = $this->db->query('SELECT sum(target) as TTL, count(indikator_id) as JML FROM tbl_dinas_indikator_detail where indikator_id = ' . $item['indikator_id']);
                                $row = $query->row();

                                ?>
                                <?php echo round($row->TTL / $row->JML, 2);
                                echo ' %'; ?>


                            </td>


                        </tr>




                        <?php
                        //row detail
                        $x = 0;
                        $ttl = 0;
                        $tTotal=0;
                        $tStandard=0;
                        $tTarget=0;
                        $tnonstd=0;
                        $nonstd=0;
                        $isTotal=true;
                        foreach ($indikatordetail as $detail) :
                            if ($detail["indikator_id"] == $item["indikator_id"]) {
                                $nonstd =   $detail["total"] -  $detail["standard"];

                        ?>

                                <!-- untuk Header Total -->
                                <?php
                                // hitung total
                                //looping untuk dapat totalnya    
                                foreach($indikatordetail as $detail2){
                                    if ($detail2["indikator_id"] == $item["indikator_id"]) {
                                        $tnonstd = $tnonstd +  ($detail2["total"] -  $detail2["standard"]);
                                        $tTotal = $tTotal + $detail2["total"];
                                        $tStandard = $tStandard + $detail2["standard"];
                                        if($detail2["total"]>0){
                                            $tTarget = $tTarget + round($detail2["standard"] / $detail2["total"] * 100);    
                                        }
                                        else{
                                            $tTarget =$tTarget + 0;
                                        }
                                    }
                                }
                                //end looping for get total

                                if ($isTotal == true) {
                                ?>
                                <tr>
                                    <td>                                       
                                    </td>
                                    <td></td>
                                    <td>
                                        <input type="number" readonly value="<?php echo $tTotal ?>" class="form-control name_list" />
                                    </td>
                                    <td>
                                        <input type="number" readonly  value="<?php echo $tStandard ?>" class="form-control name_list" />
                                    </td>
                                    <td>
                                        <input type="number" value="<?php echo $tnonstd; ?>" class="form-control name_list" readonly />
                                    </td>


                                    <td>
                                        <input type="text" value="<?php if ($tTarget > 0) {
                                                                                        echo $tTarget;
                                                                                        echo "%";
                                                                                    } else echo 0 ?>" class="form-control name_list" readonly />
                                    </td>
                                </tr>

                                <?php $isTotal = false;
                                } ?>
                                <!-- end header total -->

                                <tr>
                                    <td>
                                        <label></label>
                                        <input type="hidden" name="id[]" value="<?php echo $detail["id"] ?>" />
                                    </td>
                                    <td><?php echo $detail['deskripsi_detail'] ?></td>
                                    <td>
                                        <input type="number" name="total[]" value="<?php echo $detail["total"] ?>" class="form-control name_list" />
                                    </td>
                                    <td>
                                        <input type="number" name="standard[]" value="<?php echo $detail["standard"] ?>" class="form-control name_list" />
                                    </td>
                                    <td>
                                        <input type="number" name="nonstandard[]" value="<?php echo $nonstd; ?>" class="form-control name_list" readonly />
                                    </td>


                                    <td>
                                        <input type="text" name="target[]" value="<?php if ($detail["total"] > 0) {
                                                                                        echo round($detail["standard"] / $detail["total"] * 100);
                                                                                        echo "%";
                                                                                    } else echo 0 ?>" class="form-control name_list" readonly />
                                    </td>
                                </tr>

                        <?php
                                $x++;
                                if ($detail["total"] > 0) {
                                    $ttl = ($ttl + round($detail["standard"] / $detail["total"] * 100) / $x);
                                }
                            }
                        endforeach;
                        ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td></td>
                            <td>
                                <input type="hidden" name="subtotal" value="<?php echo $ttl; ?>" />
                            </td>
                        </tr>
                    <?php

                        $i++;
                    endforeach; ?>
                </tbody>
            </table>
            <?php if ($this->session->userdata('level') != "SUBDIT") { ?>
                <input type="hidden" name="jumlahrow" value="<?php echo count($indikatordetail) ?>" />
                <button type="submit" class="btn btn-success">Simpan &MediumSpace; </button>
                <a href="<?php echo site_url('Dinas') ?>" class="btn btn-default">Kembali &MediumSpace; </a>

            <?php
            }
            ?>

        </form>
    </div>


<?php
} else {
?>

    <h3>Dasar Hukum. : <?php echo $dasarhukum; ?></h3>
    <div class="row">
        <form action="saveedit_action" method="post">
            <input type="hidden" value="<?php echo $id_tr_dinas; ?>" name="id_tr_dinas" />
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">No</th>
                        <th class="text-center">Jenis Layanan SPM</th>
                        <th class="text-center" colspan="3">Indikator Pencapaian</th>
                        <th class="text-center">Pencapaian</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center">A.</td>
                        <td colspan="5">
                            <label><?php echo $wilayah; ?></label>
                            <input type="hidden" value="<?php echo $wilayah; ?>" name="wilayah" />
                        </td>
                    </tr>
                    <?php
                    $i = 1;

                    foreach ($indikator as $item) : ?>
                        <tr>
                            <td><?php echo $i ?></td>
                            <td>
                                <label><?php echo $item['indikator_kinerja']; ?></label>
                            </td>
                            <td colspan="4">
                                <?php echo $item['indikator_pencapaian']; ?>
                            </td>
                            <td class="text-center"> </td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td class="text-center"><b>Jumlah yang harus dilayani</b></td>
                            <td class="text-center"><b>Jumlah yang terlayani</b></td>
                            <td class="text-center"><b>Jumlah yang belum terlayani</b></td>
                            <?php
                            $query = $this->db->query('SELECT sum(target) as TTL, count(indikator_id) as JML FROM tbl_dinas_indikator_detail where indikator_id = ' . $item['indikator_id']);
                            $row = $query->row();

                            ?>
                            <td>Persentase capaian <?php echo round($row->TTL / $row->JML, 2);
                                                    echo ' %'; ?> </td>


                        </tr>
                        <?php
                        //row detail
                        $x = 0;
                        $ttl = 0;
                        foreach ($indikatordetail as $detail) :
                            if ($detail["indikator_id"] == $item["indikator_id"]) {
                                $nonstd =   $detail["total"] -  $detail["standard"];

                        ?>
                                <tr>
                                    <td>
                                        <label></label>
                                        <input type="hidden" name="id[]" value="<?php echo $detail["id"] ?>" />
                                    </td>
                                    <td><?php echo $detail['deskripsi_detail'] ?></td>
                                    <td>
                                        <input type="number" name="total[]" value="<?php echo $detail["total"] ?>" class="form-control name_list" />
                                    </td>
                                    <td>
                                        <input type="number" name="standard[]" value="<?php echo $detail["standard"] ?>" class="form-control name_list" />
                                    </td>
                                    <td>
                                        <input type="number" name="nonstandard[]" value="<?php echo $nonstd; ?>" class="form-control name_list" readonly />
                                    </td>


                                    <td>
                                        <input type="text" name="target[]" value="<?php if ($detail["total"] > 0) {
                                                                                        echo round($detail["standard"] / $detail["total"] * 100);
                                                                                        echo "%";
                                                                                    } else echo 0 ?>" class="form-control name_list" readonly />
                                    </td>
                                </tr>

                        <?php
                                $x++;
                                if ($detail["total"] > 0) {
                                    $ttl = ($ttl + round($detail["standard"] / $detail["total"] * 100) / $x);
                                }
                            }
                        endforeach;
                        ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td></td>
                            <td>
                                <input type="hidden" name="subtotal" value="<?php echo $ttl; ?>" />
                            </td>
                        </tr>
                    <?php

                        $i++;
                    endforeach; ?>
                </tbody>
            </table>
            <?php if ($this->session->userdata('level') != "SUBDIT") { ?>
                <input type="hidden" name="jumlahrow" value="<?php echo count($indikatordetail) ?>" />
                <button type="submit" class="btn btn-success">Simpan &MediumSpace; </button>
                <a href="<?php echo site_url('Dinas') ?>" class="btn btn-default">Kembali &MediumSpace; </a>

            <?php
            }
            ?>

        </form>
    </div>
<?php } ?>