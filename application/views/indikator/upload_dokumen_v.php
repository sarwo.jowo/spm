
<div class="col-md-12">
    <div class="box box-warning">
        <div class="box-header">
            <h3 class="box-title">File Utama</h3>
        </div><!-- /.box-header -->
        <div class="box-body">  
       <?php if ($this->session->userdata('level') != "SUBDIT"){?>
            <form action="<?php echo site_url('generate_indikator/upload'); ?>" method="post" enctype="multipart/form-data" id="form-upload">                
                <label for="Deskripsi">Deskripsi</label>
                <input type="text"  name="Deskripsi" class="form-control"/>
                <!-- <label for="wilayah">Wilayah</label>
                <input type="text" value="<?php echo $wilayah; ?>" name="wilayah" class="form-control"  readonly/> -->
                <label for="image">Pilih File</label> 
                <input type="file" name="image" size="300" />
                <input type="hidden" name="id_tr_dinas" value="<?php echo $id_tr_dinas ?>" />
				
                <br /><br />
                <input type="submit" class="btn btn-warning" value="upload" />
				
            </form>

            <?php } ?>
            <br/>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama Dokumen</td>
                        <td style="width:20%">File</td>                        
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i=1;
                    if($dokument!=null){

                        foreach($dokument as $doc):
                            $id = $doc['ID'];
                        ?>
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $doc['keterangan'] ?></td>
                            <td>
                                <a href="<?php echo base_url('assets/uploads')."/".$doc['filename'];?>" target="_new" id="<?php echo $doc['filename'];?>" class="btn btn-success">download </a>								
                                <button id="<?php echo $id; ?>" class="btn btn-danger" onClick="reply_click(this.id)">Hapus</button>
                            </td>
                        </tr>
                        <?php
                    endforeach;
                    }else{ ?>
                        <tr>
                            <td colspan="3">Tidak ada data</td>
                        </tr>
                    <?php    
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">

function reply_click(clicked_id)
			{			
			alert('Delete File');
			var data = "id="+clicked_id;            			
			var url = '<?php echo site_url('generate_indikator/deletefile');?>';					
			$.ajax({
				//alert('testing');
				type	: "POST",
				url		: url,
				data	: data,
				success	: function(result) {
					var result = eval('('+result+')');								
					if(result.ok) {		
                      alert('Delete Suskes');					
					  location.reload();						
						
					}else{		
					alert('Delete gagal');					
					location.reload();						
					}
				}
			});
			}

</script>