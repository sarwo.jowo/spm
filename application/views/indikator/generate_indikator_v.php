<?php if ($dinasid==4){?>
<h3>Dasar Hukum: <?php echo $dasarhukum;?></h3>
<div class="row">
<form action="save_action" method="post">
<input type="hidden" value="<?php echo $id_tr_dinas;?>" name="id_tr_dinas"/>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th class="text-center">No</th>
                <th class="text-center">Jenis Layanan SPM</th>
                <th class="text-center" colspan="3">Indikator Pencapaian</th>                 
                <th class="text-center">Total Pencapaian</th>
            </tr>
            <tr>
                <th class="text-center">(1)</th>
                <th class="text-center">(2)</th>
                <th class="text-center" colspan="3"></th> 
                <th class="text-center">(4)</th>
            </tr>            
        </thead>
        <tbody>
            <tr>
                <td class="text-center">A.</td>
                <td colspan="5">
                    <label><?php echo $wilayah;?></label>
                    <input type="hidden" value="<?php echo $wilayah;?>" name="wilayah"/>
                </td>
            </tr>
            <?php 
            $i=1; 
            $x=0;
            $did=4;
            foreach ($indikator as $item):?>
            <tr>
                <td>
                    <?php                    
                         if($i==2) {                         
                           $no='';
                         }else{
							$no=$i; 
						}	
                          echo $no;
            //  }
                    ?>
                </td>
                <td>
                    <label><?php echo $item['indikator_kinerja'];?></label>
					<input type="hidden" value="<?php echo $item['id'];?>" name="id_indikator[]"/>
                    <input type="hidden" value="<?php echo $item['indikator_kinerja'];?>" name="indikator_kinerja[]"/>
					<input type="hidden" value="<?php echo $item['indikator_pencapaian'];?>" name="indikator_pencapaian[]"/>
                </td>
                <td>
				  
                    <?php
                    if($i==1){

					echo "Jumlah warga negara yang berhak mendapat layanan";
					}else if ($i==2)
					{ 
				    echo "Jumlah pelaksanaan penegakan Perda/Perkada";
				    }else if ($i==3)	
					{
					echo "Jumlah yang harus dilayani";	
					}else if ($i==4)	
					{
					echo "Jumlah yang harus dilayani";	
					}else if($i==5)
					{						
					echo "Jumlah yang harus dilayani";
					}else
                    {
					echo "Jumlah kebakaran";	
					}	
					
					?>
                    
                </td>
				 <td class="text-center"> <?php
                    if($i==1){

					echo "Jumlah warga negara yang terlayani";
					}else if ($i==2)
					{ 
				    echo "Jumlah penegakan perda/perkada  yang sesuai mutu layanan dasar";
				    }else if ($i==3)	
					{
					echo "jumlah yang telah dilayani sesuai standar";	
					}else if ($i==4)	
					{
					echo "jumlah yang telah dilayani sesuai standar";	
					}else if($i==5)
					{						
					echo "jumlah yang telah dilayani sesuai standar";
					}					
					else
                    {
					echo "jumlah yang telah dilayani sesuai standar";	
					}	
					
					?></td>
				  <td class="text-center"> <?php
                    if($i==1){

					echo "Jumlah belum terlayani";
					}else if ($i==2)
					{ 
				    echo "Jumlah penegakan perda/perkada  yang tidak sesuai mutu layanan dasar";
				    }else if ($i==3)	
					{
					echo "belum terlayani";	
					}else if ($i==4)	
					{
					echo "belum terlayani";	
					}
					else if($i==5)
					{						
				    echo "belum terlayani";
					}else
                    {
					echo "belum terlayani";	
					}					
					?></td>
                <td class="text-center">Capaian</td>
            </tr>
              
            <?php 
               //row detail
               $itemindikator=explode ("#", $item['item_indikator']); 
               foreach($itemindikator as $detail):
            ?>     
            <tr>
                <td></td>
                <td>
				<?php echo strip_tags($detail);?>
                <input type="hidden" name="itemdesc<?php echo $i?>[]" class="form-control name_list" value="<?php echo strip_tags($detail);?>" readonly /></td>
                <td>
                <input type="number" name="total<?php echo $i?>[]"  class="form-control name_list"  />    
                </td>
                <td>
                <input type="number" name="standard<?php echo $i?>[]"  class="form-control name_list"   />
                </td>
                <td>
                <input type="number" name="nonstandard<?php echo $i?>[]"  class="form-control name_list" readonly />
                </td>
				
				<td>
                <input type="number" name="tidak_terlayani<?php echo $i?>[]"  class="form-control name_list" readonly />
                </td>
                <!--<td>-->
                    <!-- <input type="number" name="itemvalue<?php echo $i?>[]"  class="form-control name_list" value="0" readonly /> -->
                <!--</td>            -->
            </tr>            
            <?php             
                endforeach;
            $i++;  
            endforeach;?>            
        </tbody>
    </table>    
    <button type="submit" class="btn btn-success">Proses &MediumSpace; </button> 
	<a href="<?php echo site_url('Dinas') ?>" class="btn btn-default">Kembali &MediumSpace; </a>
</form>
</div>

<?php }else{?>
<h3>Dasar Hukum..: <?php echo $dasarhukum;?></h3>
<div class="row">
<form action="save_action" method="post">
<input type="hidden" value="<?php echo $id_tr_dinas;?>" name="id_tr_dinas"/>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th class="text-center">No</th>
                <th class="text-center">Jenis Layanan SPM</th>
                <th class="text-center" colspan="3">Indikator Pencapaian</th>                 
                <th class="text-center">Total Pencapaian</th>
            </tr>
            <tr>
                <th class="text-center">(1)</th>
                <th class="text-center">(2)</th>
                <th class="text-center" colspan="3"></th> 
                <th class="text-center">(4)</th>
            </tr>            
        </thead>
        <tbody>
            <tr>
                <td class="text-center">A.</td>
                <td colspan="5">
                    <label><?php echo $wilayah;?></label>
                    <input type="hidden" value="<?php echo $wilayah;?>" name="wilayah"/>
                </td>
            </tr>
            <?php 
            $i=1; 
            $x=0;
            $did=4;
            foreach ($indikator as $item):?>
            <tr>
                <td>
                    <?php 
          
                    // } 
                    // else{
                        echo $i;
                    
                    ?>
                </td>
                <td>
                    <label><?php echo $item['indikator_kinerja'];?></label>
					<input type="hidden" value="<?php echo $item['id'];?>" name="id_indikator[]"/>
                    <input type="hidden" value="<?php echo $item['indikator_kinerja'];?>" name="indikator_kinerja[]"/>
                </td>
                <td colspan="3">
                    <?php echo $item['indikator_pencapaian'];?>
                    <input type="hidden" value="<?php echo $item['indikator_pencapaian'];?>" name="indikator_pencapaian[]"/>
                </td>
                <td class="text-center"></td>
            </tr>
            <tr>
                <td colspan="2"></td>
               <td class="text-center"><b>Jumlah yang harus dilayani</b></td>
                <td class="text-center"><b>Jumlah yang terlayani sesuai standar</b></td>
                <td class="text-center"><b>Jumlah yang terlayani belum sesuai standar</b></td>
				<td class="text-center"><b>Capaian</b></td>
                <!--<td></td>-->
            </tr>            
            <?php 
               //row detail
               $itemindikator=explode ("#", $item['item_indikator']); 
               foreach($itemindikator as $detail):
            ?>     
            <tr>
                <td></td>
                <td>
				<?php echo strip_tags($detail);?>
                <input type="hidden" name="itemdesc<?php echo $i?>[]" class="form-control name_list" value="<?php echo strip_tags($detail);?>" readonly /></td>
                <td>
                <input type="number" name="total<?php echo $i?>[]"  class="form-control name_list"  />    
                </td>
                <td>
                <input type="number" name="standard<?php echo $i?>[]"  class="form-control name_list"   />
                </td>
                <td>
                <input type="number" name="nonstandard<?php echo $i?>[]"  class="form-control name_list" readonly />
                </td>
				
				<td>
                <input type="number" name="tidak_terlayani<?php echo $i?>[]"  class="form-control name_list" readonly />
                </td>
                <!--<td>-->
                    <!-- <input type="number" name="itemvalue<?php echo $i?>[]"  class="form-control name_list" value="0" readonly /> -->
                <!--</td>            -->
            </tr>            
            <?php             
                endforeach;
            $i++;  
            endforeach;?>            
        </tbody>
    </table>    
    <button type="submit" class="btn btn-success">Proses Laporan &MediumSpace; </button> 
	<a href="<?php echo site_url('Dinas') ?>" class="btn btn-default">Kembali &MediumSpace; </a>
</form>
</div>

<?php };?>