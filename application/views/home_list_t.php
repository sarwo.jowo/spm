<style type="text/css">
td, div {
	font-family: "Arial","?Helvetica","?sans-serif";
}
.datagrid-header-row * {
	font-weight: bold;
}
.messager-window * a:focus, .messager-window * span:focus {
	color: blue;
	font-weight: bold;
}
.daterangepicker * {
	font-family: "Source Sans Pro","Arial","?Helvetica","?sans-serif";
	box-sizing: border-box;
}
.glyphicon	{font-family: "Glyphicons Halflings"}
</style>
<?php 
// buat tanggal sekarang
$tanggal = date('Y-m-d H:i');
$tanggal_arr = explode(' ', $tanggal);
$txt_tanggal = $tanggal;
//$txt_tanggal .= ' - ' . $tanggal_arr[1];
$tanggal2 = date('Y-m-d H:i');
?>
				   <div class="row">
                        <!-- left column -->
                        <div class="col-md-6">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Surat Masuk</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <form role="form" id="register_frm"  method="post" >
                                    <div class="box-body">
								        <div class="form-group">
                                            <label for="tglsurat">Tanggal Diterima</label>
											<input type="text" name="tgl_masuk_txt" class="form-control" id="tgl_masuk_txt"  required="true" readonly="readonly" />						                     
                                             <input type="hidden" name="tgl_masuk" class="form-control"  id="tgl_masuk" />
				                         </div>              
									   <div class="form-group">
									   
                                            <label for="asalsurat">Asal Surat</label>                                            
                         <select id="depart_id" name="depart_id" class="form-control" required="true">
						<option value="0"> - Pilih kementerian Asal -</option>			
						<?php	
						foreach ($depart as $row) {
							if(strlen($row->st) != 1){
								$kode ='';
								$ruang = $row->nama;
							}else{
								$kode ='';
								$ruang = $row->nama;
							}
							echo '<option value="'.$row->id.'">
							'.$kode.' '.$ruang.'
							</option>';
						}
						?>
					</select>									  
									  </div>
										 <div class="form-group">
                                            <label for="nomorsurat">Nomor Surat</label>
                                            <input type="text" id="nomorsurat" name="nomorsurat" class="form-control"  placeholder="Nomor Surat">
                                        </div>                                        
												
										<div class="form-group">
                                        <label>tanggal Surat:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control"  name="tglsurat"  id="tglsurat" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask/>
                                        </div><!-- /.input group -->
                                         </div><!-- /.form group -->
                                         <div class="form-group">
                                            <label>Perihal</label>
                                            <textarea class="form-control" id="perihal"  name="perihal" rows="3" placeholder="Enter ..."></textarea>
                                        </div>		 
									<div class="box-footer">
									 <a href="javascript:void(0)" class="btn btn-primary" onclick="save()">Simpan</a>
                                       </form>	
                                    </div>	
                               
                                    </div><!-- /.box-body -->                                
                              
                            </div><!-- /.box -->         
                                
                            </div><!-- /.box -->
							
							 <div class="col-md-6">
                              <div class="box box-warning">
                                <div class="box-header">
                                    <h3 class="box-title">File Surat</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">                                   
                              	 <form id="fileupd" action="upload/do_uploads" method="POST" enctype="multipart/form-data">
								<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
								<div class="row fileupload-buttonbar">
									<div class="col-lg-7">							
									
									   <div class="form-group">
                                            <label for="file">File input</label>
                                            <input type="file" id="file" placeholder="Enter ..." disabled/>
                                            <p class="help-block">Pilih File Surat</p>
                                          </div> 
							            <!-- 
										<button type="submit" class="btn btn-primary start" onclick="disable()">
											<i class="glyphicon glyphicon-upload"></i>
											<span>Start upload</span>
										</button> 
										-->
										<a href="javascript:void(0)" class="btn btn-primary" onclick="disable()">Upload File</a>	
										
                                     </div>
									<p>&nbsp;</p>									 
						      </div>
							   <div class="box-body no-padding">
                                   
                                    <div class="table-responsive">
                                        <!-- .table - Uses sparkline charts-->
                                        <table class="table table-striped">
										<?php
										// $i = 1;
										// foreach($file->result() as $row)
										//{
									  ?>   
			  
                                            <tr>
                                                <th>No</th>
                                                <th>File</th>
                                                <th>Ukuran</th>
                                                <th>Tools</th>
                                            </tr>
                                            <tr>
                                                <td><a href="#">1</a></td>
                                                <td><div id="sparkline-1">surat  XXX</div></td>
                                                <td>209</td>
                                                <td>
												<a href="javascript:void(0)" class="btn btn-primary" onclick="hapus()">Delete</a>
												</td>
                                            </tr>
                                            <tr>
                                                <td><a href="#">2</a></td>
                                                <td><div id="sparkline-2">surat  XXX</div></td>
                                                <td>131</td>
                                                <td>
											 <a href="javascript:void(0)" class="btn btn-primary" onclick="hapus()">Delete</a>													
												</td>
                                            </tr>                                           
											
                                        <?php
											//$i++;
											//}
										  echo '</table>';										 
										  ?>
                                    </div>
                                </div><!-- /.box-body--> 
							  
							</form>													   
				            </div>				
                          </div>		  
			  
			   <div class="box box-warning">
                                <div class="box-header">
                                    <h3 class="box-title">File Lampiran</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">                                   
                                 
								 <form id="fileupload" action="upload/do_uploads" method="POST" enctype="multipart/form-data">
								<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
								<div class="row fileupload-buttonbar">
									<div class="col-lg-7">
										<!-- The fileinput-button span is used to style the file input field as button -->
										<span class="btn btn-success fileinput-button">
											<i class="glyphicon glyphicon-plus"></i>
											<span>Add</span>
											<input type="file" name="userfile" multiple>
										</span>
										<button type="submit" class="btn btn-primary start">
											<i class="glyphicon glyphicon-upload"></i>
											<span>Start upload</span>
										</button>					
										
										<!-- The global file processing state -->
										<span class="fileupload-process"></span>
									</div>
									<!-- The global progress state -->
									<div class="col-lg-5 fileupload-progress fade">
										<!-- The global progress bar -->
										<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
											<div class="progress-bar progress-bar-success" style="width:0%;"></div>
										</div>
										<!-- The extended global progress state -->
										<div class="progress-extended">&nbsp;</div>
									</div>
								</div>
								<!-- The table listing the files available for upload/download -->
								<table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
							</form>
							 
							  <!-- The blueimp Gallery widget -->
							<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
								<div class="slides"></div>
								<h3 class="title"></h3>
								<a class="prev">‹</a>
								<a class="next">›</a>
								<a class="close">×</a>
								<a class="play-pause"></a>
								<ol class="indicator"></ol>
							</div>
                            
							<script id="template-upload" type="text/x-tmpl">
							{% for (var i=0, file; file=o.files[i]; i++) { %}
								<tr class="template-upload fade">
									<td>
										<span class="preview"></span>
									</td>
									<td>
										<p class="name">{%=file.name%}</p>
										<strong class="error text-danger"></strong>
									</td>
									<td>
										<p class="size">Processing...</p>
										<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
									</td>
									<td>
										{% if (!i && !o.options.autoUpload) { %}
											<button class="btn btn-primary start" disabled>
												<i class="glyphicon glyphicon-upload"></i>
												<span>Start</span>
											</button>
										{% } %}
										{% if (!i) { %}
											<button class="btn btn-warning cancel">
												<i class="glyphicon glyphicon-ban-circle"></i>
												<span>Cancel</span>
											</button>
										{% } %}
									</td>
								</tr>
							{% } %}
							</script>
                           
                       <!-- The template to display files available for download -->
						<script id="template-download" type="text/x-tmpl">
						{% for (var i=0, file; file=o.files[i]; i++) { %}
							<tr class="template-download fade">
								<td>
									<span class="preview">
										{% if (file.thumbnailUrl) { %}
											<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
										{% } %}
									</span>
								</td>
								<td>
									<p class="name">
										{% if (file.url) { %}
											<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
										{% } else { %}
											<span>{%=file.name%}</span>
										{% } %}
									</p>
									{% if (file.error) { %}
										<div><span class="label label-danger">Error</span> {%=file.error%}</div>
									{% } %}
								</td>
								<td>
									<span class="size">{%=o.formatFileSize(file.size)%}</span>
								</td>
								<td>
									{% if (file.deleteUrl) { %}
										<button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
											<i class="glyphicon glyphicon-trash"></i>
											<span>Delete</span>
										</button>										
										
										
									{% } else { %}
										<button class="btn btn-warning cancel">
											<i class="glyphicon glyphicon-ban-circle"></i>
											<span>Cancel</span>
										</button>
									{% } %}
								</td>
							</tr>
						{% } %}
						</script>								 
                                
                                	
												
                            </div><!--/.col (right) -->
                </div>   <!-- /.row -->    
            </div>   <!-- /.row -->         
    
        <!-- InputMask -->
        <script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>
        <!-- date-range-picker -->
        <script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <!-- bootstrap color picker -->
        <script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript"></script>
        <!-- bootstrap time picker -->
        <script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
		<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/vendor/jquery.ui.widget.js"></script>
		<!-- The Templates plugin is included to render the upload/download listings -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/tmpl.min.js"></script>
		<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/load-image.all.min.js"></script>
		<!-- The Canvas to Blob plugin is included for image resizing functionality -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/canvas-to-blob.min.js"></script>
		<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
		<!-- blueimp Gallery script -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/jquery.blueimp-gallery.min.js"></script>
		<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/jquery.iframe-transport.js"></script>
		<!-- The basic File Upload plugin -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/jquery.fileupload.js"></script>
		<!-- The File Upload processing plugin -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/jquery.fileupload-process.js"></script>
		<!-- The File Upload image preview & resize plugin -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/jquery.fileupload-image.js"></script>
		<!-- The File Upload audio preview plugin -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/jquery.fileupload-audio.js"></script>
		<!-- The File Upload validation plugin -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/jquery.fileupload-validate.js"></script>
		<!-- The File Upload user interface plugin -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/jquery.fileupload-ui.js"></script>
		<!-- The main application script -->
		<script src="<?php echo base_url(); ?>assets/theme_admin/upload/js/main.js"></script>
		<!-- blueimp Gallery styles -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/theme_admin/upload/css/blueimp-gallery.min.css">
		<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/theme_admin/upload/css/jquery.fileupload.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/theme_admin/upload/css/jquery.fileupload-ui.css">
		<!-- CSS adjustments for browsers with JavaScript disabled -->
		<noscript><link rel="stylesheet" href="<?php echo base_url(); ?>assets/theme_admin/upload/css/jquery.fileupload-noscript.css"></noscript>
		<noscript><link rel="stylesheet" href="<?php echo base_url(); ?>assets/theme_admin/upload/css/jquery.fileupload-ui-noscript.css"></noscript>

      	   
       <script type="text/javascript">
	   
            $(function() {
                //Datemask dd/mm/yyyy
                $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
                //Datemask2 mm/dd/yyyy
                $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
                //Money Euro
                $("[data-mask]").inputmask();
                //Date range picker             

                //Timepicker
                $(".timepicker").timepicker({
                    showInputs: false
                });
				$('#tgl_masuk_txt').val('<?php echo $txt_tanggal;?>');
	            $('#tgl_masuk').val('<?php echo $tanggal;?>');	
				
				//$.datepicker.setDefaults( $.datepicker.regional[ "id" ] );
				$(".dtpicker").datetimepicker({
				language:  'id',
				weekStart: 1,
				autoclose: true,
				todayBtn: true,
				todayHighlight: true,
				pickerPosition: 'bottom-right',
			linkFormat: "yyyy-mm-dd hh:ii"
             });			
            });			
		   function hapus() {
			$.messager.alert('Warning','Delete File Surat');	
			}
			function disable(){
				$("#rbutton").attr("disabled","disabled");
			}
	       function save() {			   
			var data = $("#register_frm").serialize();			
			var nomorsurat = $('#nomorsurat').val();   
			if(nomorsurat == ''){	
			$.messager.alert('Warning','Maaf, Nomor Surat harus di isi');          			
				return false;
			}	
			var perihal = $('#perihal').val();   
			if(perihal == ''){	
                  $.messager.alert('Warning','lengkapi perihal surat');
			return false;
			}		
			
			var url = '<?php echo site_url('surat/masuk');?>';
			$.ajax({
				type	: "POST",
				url		: url,
				data	: data,
				success	: function(result) {
					var result = eval('('+result+')');								
					if(result.ok) {						
						$.messager.show({
							title:'<div>Informasi</div>',
							msg: result.msg,
							timeout:2000,
							showType:'slide'
							
							
						});
						window.location.reload();
						
					}else{
						$.messager.show({
							title:'<div>Informasi</div>',
							msg: result.msg,
							timeout:2000,
							showType:'slide'
							
							
						});						
					}
				}
			});			
		   }		
        </script>	
       