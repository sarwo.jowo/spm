<style type="text/css">
td, div {
	font-family: "Arial","?Helvetica","?sans-serif";
}
.datagrid-header-row * {
	font-weight: bold;
}
.messager-window * a:focus, .messager-window * span:focus {
	color: blue;
	font-weight: bold;
}
.daterangepicker * {
	font-family: "Source Sans Pro","Arial","?Helvetica","?sans-serif";
	box-sizing: border-box;
}
.glyphicon	{font-family: "Glyphicons Halflings"}
</style>
<?php 
// buat tanggal sekarang
$tanggal = date('Y-m-d H:i');
$tanggal_arr = explode(' ', $tanggal);
$txt_tanggal = $tanggal;
//$txt_tanggal .= ' - ' . $tanggal_arr[1];
$tanggal2 = date('Y-m-d H:i');
?>
				   <div class="row">
                        <!-- left column -->
                        <div class="col-md-6">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Surat Masuk</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <form role="form" id="register_frm"  method="post" >
                                    <div class="box-body">
								        <div class="form-group">
                                            <label for="tglsurat">Tanggal Diterima</label>
											<input type="text" name="tgl_masuk_txt" class="form-control" id="tgl_masuk_txt"  required="true" readonly="readonly" />						                     
                                             <input type="hidden" name="tgl_masuk" class="form-control"  id="tgl_masuk" />
				                         </div>              
									   <div class="form-group">
									   
                                            <label for="asalsurat">Asal Surat</label>                                            
                         <select id="depart_id" name="depart_id" class="form-control" required="true">
						<option value="0"> - Pilih kementerian Asal -</option>			
						<?php	
						foreach ($depart as $row) {
							if(strlen($row->st) != 1){
								$kode ='';
								$ruang = $row->nama;
							}else{
								$kode ='';
								$ruang = $row->nama;
							}
							echo '<option value="'.$row->id.'">
							'.$kode.' '.$ruang.'
							</option>';
						}
						?>
					</select>									  
									  </div>
										 <div class="form-group">
                                            <label for="nomorsurat">Nomor Surat</label>
                                            <input type="text" id="nomorsurat" name="nomorsurat" class="form-control"  placeholder="Nomor Surat">
                                        </div>                                        
												
										<div class="form-group">
                                        <label>tanggal Surat:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control"  name="tglsurat"  id="tglsurat" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask/>
                                        </div><!-- /.input group -->
                                         </div><!-- /.form group -->
                                         <div class="form-group">
                                            <label>Perihal</label>
                                            <textarea class="form-control" id="perihal"  name="perihal" rows="3" placeholder="Enter ..."></textarea>
                                        </div>		 
									<div class="box-footer">
									 <a href="javascript:void(0)" class="btn btn-primary" onclick="save()">Simpan</a>
                                       </form>	
                                    </div>	
                               
                                    </div><!-- /.box-body -->                                
                              
                            </div><!-- /.box -->         
                                
                            </div><!-- /.box -->
							
							 <div class="col-md-6">
                            <!-- general form elements disabled -->
                            <div class="box box-warning">
                                <div class="box-header">
                                    <h3 class="box-title">File Surat</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">                                   
                                        <!-- text input -->
                                      
                              <div class="form-group">
                                            <label for="file">File input</label>
                                            <input type="file" id="file" placeholder="Enter ..." disabled/>
                                            <p class="help-block">Pilih File Surat</p>
                               </div>                     
								<div class="form-group">
                                            <label for="file2">File input</label>
                                            <input type="file" id="file" placeholder="Enter ..." disabled/>
                                            <p class="help-block">Pilih File Surat</p>
                               </div>
							 
                                 <div class="box-body no-padding">
                                    <div id="world-map" style="height: 300px;"></div>
                                    <div class="table-responsive">
                                        <!-- .table - Uses sparkline charts-->
                                        <table class="table table-striped">
                                            <tr>
                                                <th>Country</th>
                                                <th>Visitors</th>
                                                <th>Online</th>
                                                <th>Page Views</th>
                                            </tr>
                                            <tr>
                                                <td><a href="#">USA</a></td>
                                                <td><div id="sparkline-1"></div></td>
                                                <td>209</td>
                                                <td>239</td>
                                            </tr>
                                            <tr>
                                                <td><a href="#">India</a></td>
                                                <td><div id="sparkline-2"></div></td>
                                                <td>131</td>
                                                <td>958</td>
                                            </tr>
                                            <tr>
                                                <td><a href="#">Britain</a></td>
                                                <td><div id="sparkline-3"></div></td>
                                                <td>19</td>
                                                <td>417</td>
                                            </tr>
                                            <tr>
                                                <td><a href="#">Brazil</a></td>
                                                <td><div id="sparkline-4"></div></td>
                                                <td>109</td>
                                                <td>476</td>
                                            </tr>
                                            <tr>
                                                <td><a href="#">China</a></td>
                                                <td><div id="sparkline-5"></div></td>
                                                <td>192</td>
                                                <td>437</td>
                                            </tr>
                                            <tr>
                                                <td><a href="#">Australia</a></td>
                                                <td><div id="sparkline-6"></div></td>
                                                <td>1709</td>
                                                <td>947</td>
                                            </tr>
                                        </table><!-- /.table -->
                                    </div>
                                </div><!-- /.box-body-->	
												
                            </div><!--/.col (right) -->
                         </div>   <!-- /.row -->         
    
        <!-- InputMask -->
        <script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>
        <!-- date-range-picker -->
        <script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <!-- bootstrap color picker -->
        <script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript"></script>
        <!-- bootstrap time picker -->
        <script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
      	   
       <script type="text/javascript">
	   
            $(function() {
                //Datemask dd/mm/yyyy
                $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
                //Datemask2 mm/dd/yyyy
                $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
                //Money Euro
                $("[data-mask]").inputmask();
                //Date range picker             

                //Timepicker
                $(".timepicker").timepicker({
                    showInputs: false
                });
				$('#tgl_masuk_txt').val('<?php echo $txt_tanggal;?>');
	            $('#tgl_masuk').val('<?php echo $tanggal;?>');	
				
				//$.datepicker.setDefaults( $.datepicker.regional[ "id" ] );
				$(".dtpicker").datetimepicker({
				language:  'id',
				weekStart: 1,
				autoclose: true,
				todayBtn: true,
				todayHighlight: true,
				pickerPosition: 'bottom-right',
			linkFormat: "yyyy-mm-dd hh:ii"
             });			
            });			
			
	       function save() {			   
			var data = $("#register_frm").serialize();			
			var nomorsurat = $('#nomorsurat').val();   
			if(nomorsurat == ''){	
			$.messager.alert('Warning','Maaf, Nomor Surat harus di isi');          			
				return false;
			}	
			var perihal = $('#perihal').val();   
			if(perihal == ''){	
                  $.messager.alert('Warning','lengkapi perihal surat');
			return false;
			}		
			
			var url = '<?php echo site_url('surat/masuk');?>';
			$.ajax({
				type	: "POST",
				url		: url,
				data	: data,
				success	: function(result) {
					var result = eval('('+result+')');								
					if(result.ok) {						
						$.messager.show({
							title:'<div>Informasi</div>',
							msg: result.msg,
							timeout:2000,
							showType:'slide'
							
							
						});
						window.location.reload();
						
					}else{
						$.messager.show({
							title:'<div>Informasi</div>',
							msg: result.msg,
							timeout:2000,
							showType:'slide'
							
							
						});						
					}
				}
			});			
		   }		
        </script>	
       