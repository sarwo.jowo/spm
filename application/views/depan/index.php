<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Sistem Pelaporan SPM</title>
    <!-- core CSS -->
    <link href="<?php echo base_url(); ?>multi/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>multi/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>multi/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>multi/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>multi/css/owl.transitions.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>multi/css/prettyPhoto.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>multi/css/main.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>multi/css/responsive.css" rel="stylesheet">

	
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>multi/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>multi/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>multi/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>multi/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>multi/images/ico/apple-touch-icon-57-precomposed.png">
</head>
<!--/head-->

<body id="home" class="homepage">

    <header id="header">
        <nav id="main-menu" class="navbar navbar-default navbar-fixed-top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="./"><img src="<?php echo base_url(); ?>multi/images/logo4.png" alt="logo"></a>
                </div>

                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="scroll active"><a href="./">Home</a></li>                 
                        <li class="scroll"><a href="#meet-team">Daftar Urusan</a></li>               
                        <li class="scroll"><a href="#blog">Blog</a></li>
						<li class="scroll"><a href="#blog">FAQ</a></li>
                        <li class="scroll"><a href="login">Login</a></li>     

                    </ul>
                </div>
            </div>
            <!--/.container-->
        </nav>
        <!--/nav-->
    </header>
    <!--/header-->

    <section id="main-slider">
        <div class="owl-carousel">
            <div class="item" style="background-image: url(images/slider/bg1.png);">
                <div class="slider-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="carousel-content">
                                    <h2>Sistem Pelaporan SPM  DITJEN BANGDA</h2>
                                    <p>Kunci Kesuksesan Pembangunan Daerah: Konsistensi Perencanaan dan Penganggaran</p>
                                    <a class="btn btn-primary btn-lg" href="#">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.item-->
            <div class="item" style="background-image: url(images/slider/bg2.png);">
                <div class="slider-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="carousel-content">
                                    <h2>Standar Pelayanan Minimal, Hak Warga Negara Secara Minimal</h2>
              <p>Kunci Kesuksesan Pembangunan Daerah: Konsistensi Perencanaan dan Penganggaran</p>
									
                                    <a class="btn btn-primary btn-lg" href="#">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.item-->
        </div>
        <!--/.owl-carousel-->
    </section>
 
    <section id="meet-team">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown">KEMENTERIAN DAN LEMBAGA</h2>
                <p class="text-center wow fadeInDown">Sistem Ini melibatkan beberapa kementerian <br> Yang terlibat dalam sistem pelaporan SPM</p>
            </div>

            <div class="row">
                <div class="col-sm-4 col-xs-6 team-column col-md-3">
                    <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="100ms">
                        <div class="team-img">
                            <img class="img-responsive" src="images/team/sosial.png" alt="">
                        </div>
                        <div class="team-info">
                            <h3>SOSIAL</h3>
                            <span>Kementerian Sosial Republik Indonesia</span>
                        </div>                      
                    </div>
                </div>
                <div class="col-sm-4 col-xs-6 team-column col-md-3">
                    <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="200ms">
                        <div class="team-img">
                            <img class="img-responsive" src="images/team/pendidikan.png" alt="">
                        </div>
                        <div class="team-info">
                            <h3>PENDIDIKAN</h3>
                            <span>Kementerian Pendidikan dan Kebudayaan, Republik Indonesia</span>
                        </div>
                        
                       
                    </div>
                </div>
                <div class="col-sm-4 col-xs-6 team-column col-md-3">
                    <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="300ms">
                        <div class="team-img">
                            <img class="img-responsive" src="images/team/kemkes.png" alt="">
                        </div>
                        <div class="team-info">
                            <h3>KESEHATAN</h3>
                            <span>Kementerian Kesehatan Rebuplik Indonesia</span>
                        </div>
                      
                       
                    </div>
                </div>
                <div class="col-sm-4 col-xs-6 team-column col-md-3">
                    <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="0ms">
                        <div class="team-img">
                            <img class="img-responsive" src="images/team/mempera.png" alt="">
                        </div>
                        <div class="team-info">
                            <h3>PUPR</h3>
                            <span>Kementrian Perumahan Rakyat Republik Indonesia</span>
                        </div>
                    
                        
                    </div>
                </div>
				
				 <div class="col-sm-4 col-xs-6 team-column col-md-3">
                    <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="0ms">
                        <div class="team-img">
                            <img class="img-responsive" src="images/team/pupr.png" alt="">
                        </div>
                        <div class="team-info">
                            <h3>PEKERJAAN UMUM</h3>
                            <span>Kementerian Pekerjaan Umum dan Perumahan Rakyat Republik Indonesia</span>
                        </div>
                     
                        
                    </div>
                </div>
				
				 <div class="col-sm-4 col-xs-6 team-column col-md-3">
                    <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="0ms">
                        <div class="team-img">
                            <img class="img-responsive" src="images/team/bnbp.png" alt="">
                        </div>
                        <div class="team-info">
                            <h3>BNPB</h3>
                            <span>BNPB (Badan Nasional Penanggulangan Bencana) </span>
                        </div>
                     
                        
                    </div>
                </div>
				
				
				 <div class="col-sm-4 col-xs-6 team-column col-md-3">
                    <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="0ms">
                        <div class="team-img">
                            <img class="img-responsive" src="images/team/bappenas.png" alt="">
                        </div>
                        <div class="team-info">
                            <h3>BAPPENAS</h3>
                            <span>Kementerian Perencanaan Pembangunan Nasional/Badan Perencanaan Pembangunan Nasional (BAPPENAS)</span>
                        </div>
                      
                        
                    </div>
                </div>
				
            </div>

          
               
            </div>       
    </section>
	
	<section id="blog">
    <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 mb-30">
                    <h3 class="column-title">Daftar Permen SPM</h3>
                         <div role="tabpanel" class="tab-pane fade active in" id="tab1" aria-labelledby="tab1">
                                <p>1.Peraturan Menteri Sosial Republik Indonesia Nomor 9 Tahun 2018 </p>
								<p>2.Peraturan Menteri Kesehatan Republik Indonesia Nomor 4 Tahun 2019 </p>
								<p>3.Peraturan Menteri Pendidikan Dan Kebudayaan Republik Indonesia Nomor 32 Tahun 2018 </p>
								<p>4.Peraturan Menteri  PP 2/2018 </p>
								<p>5.Peraturan Menteri Pendidikan Dan Kebudayaan Republik Indonesia Nomor 32 Tahun 2018 </p>
								<p>6.Peraturan Menteri Pekerjaan Umum Dan Perumahan Rakyat Republik Indonesia Nomor 29/Prt/M/2018 </p>
                        </div>
                           
				   
				   
				   
                            
                </div>

                <div class="col-md-4 col-sm-6 mb-30">
                    <h3 class="column-title">History Berita</h3>
                                                      
                            <div role="tabpanel" class="tab-pane fade active in" id="tab1" aria-labelledby="tab1">
                                <p>Pemerintah Daerah Diminta Segera Membentuk Tim Penerapan Standar Pelayanan Minimal (SPM)</p>
                                <p>Poros Maritim Selaras dengan Tujuan Pembangunan Berkelanjutan 14 (TPM-14)</p>
								<p>Pemerintah Dorong Program Satu PAUD Satu Desa</p>
								<p>Hasil Penilaian Kinerja Konvergensi Intervensi Pencegahan Stunting Oleh Kabupaten/Kota</p>
                            </div>
                           
                      
                   
                </div>

                <div class="col-md-4 col-sm-6 mb-30">
                    <h3 class="column-title">Faqs</h3>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Enim eiusmod high life accusamus
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Nihil anim keffiyeh helvetica
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Vegan excepteur butcher vice lomo
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
  </div>
  </div>
	  </section>
	
	
    <!--/#meet-team-->
      <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2019. DITJEN BINA PEMBANGUNAN DAERAH KEMENTERIAN DALAM NEGERI<a target="_blank" href="#" title="DITJEN BINA PEMBANGUNAN DAERAH KEMENTERIAN DALAM NEGERI"></a>
                </div>         
            </div>
        </div>
    </footer>
    <!--/#footer-->

    <script src="<?php echo base_url(); ?>multi/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>multi/js/bootstrap.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script src="<?php echo base_url(); ?>multi/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url(); ?>multi/js/mousescroll.js"></script>
    <script src="<?php echo base_url(); ?>multi/js/smoothscroll.js"></script>
    <script src="<?php echo base_url(); ?>multi/js/jquery.prettyPhoto.js"></script>
    <script src="<?php echo base_url(); ?>multi/js/jquery.isotope.min.js"></script>
    <script src="<?php echo base_url(); ?>multi/js/jquery.inview.min.js"></script>
    <script src="<?php echo base_url(); ?>multi/js/wow.min.js"></script>
    <script src="<?php echo base_url(); ?>multi/js/main.js"></script>


</body>

</html>