<style type="text/css">
	.row * {
		box-sizing: border-box;
	}
	.kotak_judul {
		 border-bottom: 1px solid #fff; 
		 padding-bottom: 2px;
		 margin: 0;
	}
	.box-header {
		color: #444;
		display: block;
		padding: 10px;
		position: relative;
	}
</style>
<?php
$tanggal = date('Y-m');
$txt_periode_arr = explode('-', $tanggal);
	if(is_array($txt_periode_arr)) {
		$txt_periode = jin_nama_bulan($txt_periode_arr[1]) . ' ' . $txt_periode_arr[0];
	}

?>

<div class="modal fade" role="dialog" id="frm_city">
          <div class="modal-dialog" style="width:400px">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add City</h4>
              </div>
			 
              <div class="modal-body" style="padding-bottom:2px;">
				
				 <form role="form" id="form_city" autocomplete="off">
                <!-- text input -->
				
                <div class="form-group">
                  <label>City Name</label><span class="label label-danger pull-right city_name_error"></span>
                  <input style="height:20px; width:92%;" type="text" class="form-control" name="city_name" id="city_name" value="" placeholder="City Name" required>
                 <input type="hidden" name="id_city" id="id_city" value="">
                </div>
				
				<div class="form-group">
                  <label>Province Name</label><span class="label label-danger pull-right province_name_error"></span>
                 <?php echo $lp;?>
                 
                </div>
				
				 <div class="form-group">
                  <label>Status</label><span class="label label-danger pull-right status_error"></span>
                  <select class="form-control" id="status" name="status" style="width:99%">
					  <option value="">-- Pilih Status --</option>
					  <option value=1>Active</option>
					  <option value=0>Inactive</option>
				  </select>
                </div>				

              </form>

              </div>
              <div class="modal-footer" style="margin-top:1px;">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>               
                <button type="button" class="btn btn-success yes_save">Save</button>               
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
</div>


 <div class="box box-success">
 <div class="box-header">
    <a href="#"><button class="btn btn-success add_city"><i class="fa fa-plus"></i> Add City</button></a>	 
			  
</div>
<div class="box-body">
<div class='alert alert-info alert-dismissable' id="success-alert">
   
    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
    <div id="id_text"><b>Welcome</b> </div>
</div>
	<table id="example88" class="table table-bordered table-striped">
		<thead><tr>
			<th style="text-align:center; width:5%">No.</th>
			<th style="text-align:center; width:31%">City Name</th>
			<th style="text-align:center; width:31%">Provinci</th>
			<th style="text-align:center; width:19%">Status</th>
				
			<th style="text-align:center; width:14%">View</th>
		</tr>
		</thead>
		<tbody>
		<?php 
		if(!empty($city)){
			$i=1;
			foreach($city as $c){
				$status = null;
				if($c['status'] == 1){
					$status = '<small class="label label-success bg-green">Active</small>';
				}else if($c['status'] == 0){
					$status = '<small class="label label-danger">Inactive</small>';
				}else{
					$status = '';
				}
				$info = $c['city_id'].'_'.$c['name'].'_'.$c['province_id'].'_'.$c['status'];
				echo '<tr>';
				echo '<td align="center">'.$i++.'.</td>';
				echo '<td>'.$c['name'].'</td>';
				echo '<td>'.$c['province_name'].'</td>';
				echo '<td align="center">'.$status.'</td>';
				echo '<td align="center" >
			<a href="#" title="Edit" id="'.$info.'" class="edit_city"><button class="btn btn-xs btn-success"><i class="fa fa-edit"></i> Edit</button></a>
			<a href="#" title="Delete" id="'.$c['city_id'].'" class="del_city"><button class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i> Delete</button></a></td>';
				echo '</tr>';
			}
		}else{
			echo '<tr>';
			echo '<td colspan=5 align=center><strong>No Data</strong></td>';
			echo '</tr>';
		}
		?>
		
	</tbody>
	
	</table>
</div>

</div>

<script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
	
<script type="text/javascript">
$("#success-alert").hide();

$('.edit_city').click(function(){
	$('#frm_city').find("input[type=text], select, input[type=hidden]").val("");
	var val = $(this).get(0).id;
	var dt = val.split('_');
	$('#id_city').val(dt[0]);
	$('#city_name').val(dt[1]);
	$('#province_name').val(dt[2]);
	$('#status').val(dt[3]);
	$('#frm_city').modal({
		backdrop: 'static',
		keyboard: false
	});
	$('#frm_city').modal('show');
});


$('.add_city').click(function(){
	$('#frm_city').find("input[type=text], select, input[type=hidden]").val("");
	$('#frm_city').modal({
		backdrop: 'static',
		keyboard: false
	});
	$('#frm_city').modal('show');
});
$('.yes_save').click(function(){
	var city_name = $('#city_name').val();
	var province_name = $('#province_name').val();
	var status = $('#status').val();
	$('.city_name_error').text('');
	$('.province_name_error').text('');
	$('.status_error').text('');
	if(city_name == ''){
		$('.city_name_error').text('Nama Kota harus diisi');
		return false;
	}
	if(province_name == ''){
		$('.province_name_error').text('Nama Provinsi harus diisi');
		return false;
	}
	if(status == ''){
		$('.status_error').text('Status harus dipilih');
		return false;
	}
	var url = '<?php echo site_url('province/save_city');?>';
	var data = $('#form_city').serialize();
	$.ajax({
		url : url,
		data : data,
		type : "POST",
		success:function(res){				
			if(res > 0){				
				$("#id_text").html('<b>Success,</b> Data telah disimpan');
				$("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
					$("#success-alert").alert('close');
					location.reload();
				});		
				$('#frm_city').modal('hide');
			}
		}
	});
});
$(function() {               
    $('#example88').dataTable({});
});
</script>
