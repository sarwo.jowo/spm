<style type="text/css">
	.row * {
		box-sizing: border-box;
	}
	.kotak_judul {
		 border-bottom: 1px solid #fff; 
		 padding-bottom: 2px;
		 margin: 0;
	}
	.dataTables_empty{
		font-weight:bold;
		text-align:center;
	}
</style>
<?php
$tanggal = date('Y-m');
$txt_periode_arr = explode('-', $tanggal);
	if(is_array($txt_periode_arr)) {
		$txt_periode = jin_nama_bulan($txt_periode_arr[1]) . ' ' . $txt_periode_arr[0];
	}

?>


 <div class="box box-success">
 <div class="box-header">
              <h3 class="box-title" style="text-align:center; font-size: 15pt; font-weight: 600;"><?php echo $title_box;?></h3>
</div>
<div class="box-body">

	<table id="example88" class="table table-bordered table-striped">
		<thead><tr>
			<th style="text-align:center; width:5%">No.</th>
			<th style="text-align:center; width:15%">Username</th>
			<th style="text-align:center; width:18%">Fullname</th>
			
			<th style="text-align:center; width:15%">Email</th>
			<th style="text-align:center; width:25%">Address</th>
			<th style="text-align:center; width:10%">Photo</th>			
			<th style="text-align:center; width:5%">View</th>
		</tr>
		</thead>
		<tbody>
		<?php 
		if(!empty($member)){
			$i=1;
			foreach($member as $m){
				$photo = '';
				if(!empty($m['photo'])){
					$photo = base_url('uploads/member/'.$m['photo']);
				}else{
					$photo = base_url('uploads/member/no_photo.jpg');
				}
				$info = site_url('member/edit/'.$m['user_id']);
				if(($l_evel == "rnp" && ($m['rnp_status'] != 1 || $m['rnp_status'] !=3)) || ($l_evel == "legal" && ($m['legal_status'] != 1 || $m['legal_status'] !=3))){
					$edit = null;
					// $edit = '<a href="'.$info.'" title="Edit" class="edit_user"><button class="btn btn-xs btn-success"><i class="fa fa-edit"></i> Edit</button></a><br/><p></p>';
				}else{
					$edit = null;
				}
				$phone1 = !empty($m['phone1']) ? $m['phone1'] : 'Not Availabe';
				$phone2 = !empty($m['phone2']) ? $m['phone2'] : 'Not Availabe';
				echo '<tr>';
				echo '<td align="center" style="vertical-align: middle;">'.$i++.'.</td>';
				echo '<td style="vertical-align: middle;">'.ucwords($this->converter->decode($m['username'])).'</td>';
				echo '<td style="vertical-align: middle;">'.ucwords($m['first_name']).' '.ucwords($m['last_name']).'</td>';
				echo '<td style="vertical-align: middle;">'.$m['email'].'</td>';
				echo '<td style="vertical-align: middle;">'.$m['street_name'].'<br/>'.$m['name'].', '.$m['province_name'].', '.$m['postal_code'].'<br/>Phone 1 : '.$phone1.'<br/>Phone 2 : '.$phone2.'</td>';
				echo '<td align="center" style="vertical-align: middle;"><img width="80" height="100" src="'.$photo.'"/></td>';
				echo '<td align="center" style="vertical-align: middle;">'.$edit.'
				<a href="'.site_url('member/view/'.$m['user_id']).'"><button class="btn btn-xs btn-success"><i class="fa fa-eye" title="View"></i> View</button></a><br/><p></p>
				<a href="'.site_url('member/history/'.$m['user_id']).'"><button class="btn btn-xs btn-success"><i class="fa fa-reorder"></i> History Revise</button></a></td>';
				echo '</tr>';
			}
		}
		?>
	</tbody>
	
	</table>
</div>

</div>

<script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
	
 <script type="text/javascript">
            $(function() {               
                $('#example88').dataTable({});
            });
        </script>
