<style type="text/css">
	.row * {
		box-sizing: border-box;
	}
	.kotak_judul {
		 border-bottom: 1px solid #fff; 
		 padding-bottom: 2px;
		 margin: 0;
	}
	.dataTables_empty{
		font-weight:bold;
		text-align:center;
	}
</style>
<?php
$tanggal = date('Y-m');
$txt_periode_arr = explode('-', $tanggal);
	if(is_array($txt_periode_arr)) {
		$txt_periode = jin_nama_bulan($txt_periode_arr[1]) . ' ' . $txt_periode_arr[0];
	}

?>


 <div class="box box-success">
 <div class="box-header">
              <h3 class="box-title" style="text-align:center; font-size: 15pt; font-weight: 600;"><?php echo $title_box;?></h3>
</div>
<div class="box-body">

	<table id="example88" class="table table-bordered table-striped">
		<thead><tr>
			<th style="text-align:center; width:5%">No.</th>
			<th style="text-align:center; width:13%">Send By</th>
			<th style="text-align:center; width:10%">Send Date</th>	
			<th style="text-align:center; width:13%">Username</th>
			<th style="text-align:center; width:13%">Subject</th>			
			<th style="text-align:center; width:40%">Pesan </th>					
			
		</tr>
		</thead>
		<tbody>
		<?php 
		if(!empty($member)){
			$i=1;
			foreach($member as $m){
				if($m['send_level'] == 1){
					$level = " as Admin";
				}else if($m['send_level'] == 2){
					$level = " as RNP";
				}else if($m['send_level'] == 3){
					$level = " as Legal";
				}else{
					$level = "";
				}
				echo '<tr>';
				echo '<td align="center">'.$i++.'.</td>';
				echo '<td>'.ucwords($this->converter->decode($m['admin_username'])).''.$level.'</td>';
				echo '<td align="center">'.date("d-M-Y H:i:s", strtotime($m['send_date'])).'</td>';		
				echo '<td>'.ucwords($this->converter->decode($m['username'])).'</td>';
				echo '<td>'.ucwords($m['subject']).'</td>';
				echo '<td>'.$m['pesan'].'</td>';
						
				echo '</tr>';
			}
		}
		?>
	</tbody>
	
	</table>
</div>

</div>

<script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
	
 <script type="text/javascript">
            $(function() {               
                $('#example88').dataTable({});
            });
        </script>
