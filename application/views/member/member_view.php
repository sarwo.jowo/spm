<style type="text/css">
	.row * {
		box-sizing: border-box;
	}
	.kotak_judul {
		 border-bottom: 1px solid #fff; 
		 padding-bottom: 2px;
		 margin: 0;
	}
	.table > tbody > tr > td{
		vertical-align : middle;
	}
</style>
<?php
$tanggal = date('Y-m');
$txt_periode_arr = explode('-', $tanggal);
	if(is_array($txt_periode_arr)) {
		$txt_periode = jin_nama_bulan($txt_periode_arr[1]) . ' ' . $txt_periode_arr[0];
	}

?>


<div class="modal modal-primary fade" role="dialog" id="rvise_frm">
          <div class="modal-dialog" style="width:600px">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title"><strong>Send Revise</strong></h4>
              </div>
			  
			      <div class="modal-body" style="padding-bottom:2px;">
				
				<form role="form" id="revise_frm" autocomplete="off">
                <!-- text input -->
				<div class="row">
				<div class="col-md-6">
                <div class="form-group">
				  <input type="hidden" value="<?php echo $member->user_id;?>" name="user_send" id="user_send"/>
				  <input type="hidden" name="akses" id="akses"/>
                  <label>Username</label><span class="label label-danger pull-right username_error"></span>
                  <input type="text" readonly class="form-control" name="username" id="username" placeholder="Username" autocomplete="off" value="<?php echo !empty($member->username) ? ucwords($this->converter->decode($member->username)) : '';?>" />
                  
                </div>
				
				 <div class="form-group">
                  <label>Email</label>
                  <input type="email" readonly class="form-control" name="email" id="email" value="<?php echo !empty($member->email) ? $member->email : '';?>" placeholder="Email" autocomplete="off" />
                </div>	

						
				
				</div>				
				
				<div class="col-md-6">
				<div class="form-group">
                  <label>Contact</label>
                  <input type="text" readonly class="form-control" name="contact" id="contact" autocomplete="off" value="<?php echo !empty($member->phone1) ? $member->phone1 : 'Not Available';?>" />
                </div>
				
				<div class="form-group">
                  <label>Subject</label><span class="label label-danger pull-right subject_error"></span>
                  <input type="text" class="form-control" name="subject" id="subject" value="" placeholder="Subject" autocomplete="off" />
                </div>					
				
                </div>
				
                </div>
				<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
					  <label>Pesan</label><span class="label label-danger pull-right content_body_error"></span>
					  <textarea class="textarea" name="content_body" id="content_body" placeholder="Pesan" style="width: 100%; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; height:200px; text-align:left;"></textarea>
					</div>	
				</div>
				</div>
              </form>

              </div>
			  
			  
              
              <div class="modal-footer" style="margin-top:1px;">  
		  
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="glyphicon glyphicon glyphicon-remove"></i> Cancel</button>  
				 <button type="button" class="btn btn-success send_revise"><i class="glyphicon glyphicon glyphicon-envelope"></i> Send to Member</button> 
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
</div>

<div class="modal modal-primary fade" role="dialog" id="alert_notes">
          <div class="modal-dialog" style="width:600px">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title"><strong>Notes</strong></h4>
              </div>
              <div class="modal-body">
			  <form id="notes_frm">
				<input type="hidden" name="_id" id="_id" value="<?php echo $member->user_id;?>" style="color:black" />
				<input type="hidden" name="jns" id="jns" style="color:black" /><span class="label label-danger pull-right notes_error"></span>
                <textarea class="textarea" name="notes" id="notes" placeholder="Notes" style="width: 100%; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; height:200px; text-align:left;"></textarea>
				
              </div>
			  </form>
              <div class="modal-footer" style="margin-top:1px;">  
		  
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>  
				 <button type="button" class="btn btn-success save_notes">Save</button> 
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
</div>


<div class="modal fade" role="dialog" id="confirm_dialogs">
          <div class="modal-dialog" style="width:400px">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Confirmation</h4>
              </div>
			 
              <div class="modal-body">
				<input type="hidden" name="status_users" id="status_users"/>
				<h4 class="text-center text_dialog"> Are you want to Approve it ?</h4>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>               
                <button type="button" class="btn btn-success yes_status">Yes</button>               
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
</div>


 <div class="box box-success">

<div class="box-body">	
	<table  class="table table-bordered table-reponsive">
		<tr class="header_kolom">
			<th style="width:5%; vertical-align: middle; text-align:center">Photo</th>
			<th style="width:25%; vertical-align: middle; text-align:center"> Identitas Information  </th>
		</tr>
		<tr>
			<?php 
				$photo = null;
				if(!empty($member->photo)){
					$photo = base_url('uploads/member/'.$member->photo);
				}else{
					$photo = base_url('uploads/member/no_photo.jpg');
				}
			?>
			<td class="h_tengah" style="vertical-align:middle;"><img src="<?php echo $photo;?>"/> </td>
			<td> 
			<table class="table table-responsive">
			<tr>
			<td width="10%"><b> Username </td>
			<td width="2%">:</td><td><?php echo !empty($member->username) ? ucwords($this->converter->decode($member->username)) : 'Not Available';?></td>
			<td width="14%"><b> Registration Date </td><td width="2%">:</td>
			<td colspan=4><?php echo !empty($member->create_date) ? date('d-M-Y', strtotime($member->create_date)) : 'Not Available';?></td>
			</tr>
			<tr>
			<td><b> Fullname</b> </td><td width="2%">:</td><td><?php echo !empty($member->first_name) || !empty($member->last_name) ? $member->first_name.' '.$member->last_name : 'Not Available';?> </td>
			<td width="10%"><b> First Name</b> </td><td width="2%">:</td><td><?php echo !empty($member->first_name) ? $member->first_name : 'Not Available';?> </td>
			<td width="10%"><b> Last Name</b></td><td width="2%">:</td><td><?php echo !empty($member->last_name) ? $member->last_name : 'Not Available';?></td>
			</tr>
			<tr></tr>
			<tr><td><b>Gender</b></td><td width="2%">:</td><td colspan=7><?php echo !empty($member->gender) ? $member->gender : 'Not Available';?></td></tr>
			<tr><td><b>Email</b></td><td width="2%">:</td><td colspan=7><?php echo !empty($member->email) ? $member->email : 'Not Available';?></td></tr>			
			</table>
			</td>
		</tr>
	</table>
	
	<table  class="table table-bordered table-reponsive">
		<tr class="header_kolom">
			
			<th style="width:25%; vertical-align: middle; text-align:center"> Contact and Address Information  </th>
			
		</tr>
		<tr>
			
			<td> 
			<table class="table table-responsive">
			<tr><td width="10%"><b>Street</b></td><td width="2%">:</td><td><?php echo !empty($member->street_name) ? $member->street_name : 'Not Available';?></td></tr>
			<tr><td><b>City </b> </td><td>:</td><td><?php echo !empty($member->name) ? $member->name : 'Not Available';?></td></tr>
			<tr><td><b>Province</b></td><td>:</td><td><?php echo !empty($member->province_name) ? $member->province_name : 'Not Available';?></td></tr>
			<tr><td><b>Postal Code</b></td><td>:</td><td><?php echo !empty($member->postal_code) ? $member->postal_code : 'Not Available';?></td></tr>
			<tr><td><b>Phone 1</b></td><td>:</td><td><?php echo !empty($member->phone1) ? $member->phone1 : 'Not Available';?></td></tr>
			<tr><td><b>Phone 2</b></td><td>:</td><td><?php echo !empty($member->phone2) ? $member->phone2 : 'Not Available';?></td></tr>			
			</table>
			</td>
			
			
		</tr>
		
	</table>
	<!--
	<table  class="table table-bordered table-reponsive">
		<tr class="header_kolom">
			
			<th style="width:25%; vertical-align: middle; text-align:center"> RNP Approval Information  </th>
			<th style="width:25%; vertical-align: middle; text-align:center"> Legal Approval Information  </th>
		</tr>
		<tr>
			
			<td> 
			<table class="table table-responsive">
			<tr><td width="12%"><b>Status</b></td><td width="2%">:</td>
			<?php 
			$_hide = '';
			if($member->rnp_status == 1){
				$status_rnp = "Approve";
			}else if($member->rnp_status == 2){
				$status_rnp = "<div style='color:#006ddb;'><strong>Revise</strong></div>";
			}else if($member->rnp_status == 3){
				$status_rnp = "<div style='color:red;'><strong>Rejected</strong></div>";
				$_hide = "style='display:none;'";
			}else{
				$status_rnp = "Not Available";
			} 
			$rnp_notes =  !empty($member->rnp_notes) ? $member->rnp_notes : 'Not Available';
			?>
			<td><?php echo $status_rnp; ?></td>
			<td width="8%"><b>By</b></td><td width="2%">:</td><td><?php echo !empty($member->operator_name) ? $this->converter->decode($member->operator_name) : 'Not Available';?></td>
			</tr>
			<tr><td><b>Date </b> </td><td>:</td><td colspan=4><?php echo $member->rnp_date>0 ? date("d-M-Y", strtotime($member->rnp_date)) : 'Not Available';?></td></tr>
			<tr><td style="vertical-align : top;"><b>Notes</b></td><td>:</td><td colspan=4><label id="rnp_notes" style="font-weight:normal; text-align:justify;"><?php echo $rnp_notes; ?></label>
			<?php if($member->rnp_status !=3){
				if($l_evel == "rnp") { ?>
			<a href="javascript:void(0)" onclick="showRemarks('rnp_notes', '<?php echo $rnp_notes;?>');"><br/><span class="label label-primary pull-right">Set Notes</span></a>
				<?php } } ?>
			</td></tr>		
			</table>
			</td>
			
			<td> 
			<table class="table table-responsive">
			
			<?php 
			$hide = '';
			if($member->legal_status == 1){
				$status_legal = "<div style='color:#00a65a;'>Approve</div>";
			}else if($member->legal_status == 2){
				$status_legal = "<div style='color:#006ddb;'>Revise</div>";
			}else if($member->legal_status == 3){
				$status_legal = "<div style='color:#00a65a;'>Rejected</div>";
				$hide = "style='display:none;'";
			}else{
				$status_legal = "Not Available";
			} 
			
			$legal_notes = !empty($member->legal_notes) ? $member->legal_notes : 'Not Available';
			?>
			
			<tr><td width="12%"><b>Status</b></td><td width="2%">:</td><td><?php echo $status_legal; ?></td>
			<td width="8%"><b>By</b></td><td width="2%">:</td><td><?php echo !empty($member->legal_by) ? $member->legal_by : 'Not Available';?></td> </tr>
			<tr><td><b>Date</b></td><td>:</td><td colspan=4><?php echo $member->legal_date>0 ? $member->legal_date : 'Not Available';?></td></tr>
			<tr><td style="vertical-align : top;"><b>Notes</b></td><td>:</td><td colspan=4><label id="legal_notes" style="font-weight:normal; text-align:justify;"><?php echo $legal_notes; ?></label>
			<?php if($l_evel == "legal") { ?>
			<a href="javascript:void(0)" onclick="showRemarks('legal_notes', '<?php echo $legal_notes;?>');"><br/><span class="label label-primary pull-right">Set Notes</span></a>
			<?php } ?>
			</td></tr>	
				
			
			</table>
			</td>
		</tr>
		
	</table>
	-->
</div>
<!--
<?php if($member->rnp_status !=3){
	if($l_evel == "legal" || $l_evel == "rnp") { 
		if(($l_evel == "rnp" && $member->rnp_status != 1) || ($l_evel == "legal" && $member->legal_status != 1)){ ?>
<div class="box-footer" style="height:35px;">
	<div class="clearfix"></div>
	<div class="pull-right">
		<button type="button" class="btn btn-danger rej"><i class="glyphicon glyphicon-remove"></i> Reject</button>		
		<button type="button" class="btn btn-warning rev"><i class="glyphicon glyphicon-pencil"></i> Revise</button>
		<button type="button" class="btn btn-success appr"><i class="glyphicon glyphicon-ok"></i> Approve</button>		
	</div>
</div>
<?php } 
	} } ?>
	-->
</div>

<script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
	
<script type="text/javascript">

var session_level = '<?php echo $this->session->userdata('l_evel');?>';
var user_id = '<?php echo $member->user_id;?>';
$('.send_revise').click(function(){
	var subject = $('#subject').val();
	var pesan = $('#content_body').val();
	$(".subject_error").text('');
	$(".content_body_error").text('');
	if(subject == ''){
		$(".subject_error").text('Subject harus diisi');
		return false;
	}
	if(pesan == ''){
		$(".content_body_error").text('Pesan harus diisi');
		return false;
	}
	var url = '<?php echo site_url('member/send_revise');?>';
	var data = $('#revise_frm').serialize();
	$.ajax({
		data : data,
		url : url,
		type : "POST",
		success:function(res){
			if(res > 0){
				window.location.href = '<?php echo site_url('member');?>';
			}
		}
	})
});

$('.yes_status').click(function(){
	var url = '<?php echo site_url('member/update_status');?>';
	var val = $('#status_users').val();
	$.ajax({
		url : url,
		type : "POST",
		data : {type : session_level, nilai : val, id : user_id},
		success : function(res){
			if(res > 0){
				window.location.href = '<?php echo site_url('member');?>';
			}
		}
	});
});

$(".save_notes").click(function(){	
	var notes = $("#notes").val();	
	$(".notes_error").text('');
	if(notes == ''){
		$(".notes_error").text('Notes harus diisi');
		return false;
	}
	
	var url = '<?php echo site_url('member/update_notes');?>';
	var data = $("#notes_frm").serialize();
	$.ajax({
		data : data,
		url : url,
		type : "POST",
		success:function(res){
			if(res > 0){
				var id = $("#jns").val();
				$("#"+id).text(notes);
				$('#alert_notes').modal('hide');	
			}
		}
	});
});
function showRemarks(jns, notes){
	$("#notes").text('');
	if(notes != 'Not Available'){
		$("#notes").text(notes);
	}
	
	$("#jns").val(jns);
	$('#alert_notes').modal({
		backdrop: 'static',
		keyboard: false
	});
	$('#alert_notes').modal('show');	
	return false;
}
$('.appr').click(function(){
	$('#status_users').val(1);
	$('.text_dialog').text('Are you want to Approve it ?');
	$('#confirm_dialogs').modal({
		backdrop: 'static',
		keyboard: false
	});
	$("#confirm_dialogs").modal('show');
});
$('.rej').click(function(){
	$('#status_users').val(3);
	$('.text_dialog').text('Are you want to Reject it ?');
	$('#confirm_dialogs').modal({
		backdrop: 'static',
		keyboard: false
	});
	$("#confirm_dialogs").modal('show');
});

$('.rev').click(function(){
	$('#akses').val(session_level);
	$('#rvise_frm').modal({
		backdrop: 'static',
		keyboard: false
	});
	$('#rvise_frm').modal('show');
});
</script>
