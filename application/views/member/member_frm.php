<style type="text/css">
	.row * {
		box-sizing: border-box;
	}
	.kotak_judul {
		 border-bottom: 1px solid #fff; 
		 padding-bottom: 2px;
		 margin: 0;
	}
	.table > tbody > tr > td{
		vertical-align : middle;
	}
	.custom-file-input::-webkit-file-upload-button {
		visibility: hidden;
	}
	.custom-file-input::before {
	  content: 'Select Photo';
	  display: inline-block;
	  background: -webkit-linear-gradient(top, #f9f9f9, #e3e3e3);
	  border: 1px solid #999;
	  border-radius: 3px;
	  padding: 1px 4px;
	  outline: none;
	  white-space: nowrap;
	  -webkit-user-select: none;
	  cursor: pointer;
	  text-shadow: 1px 1px #fff;
	  font-weight: 700;  
	}
	.custom-file-input:hover::before {	 
	  color: #d3394c;
	}

	.custom-file-input:active::before {
	  background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9);
	  color: #d3394c;
	}

</style>
<?php
$tanggal = date('Y-m');
$txt_periode_arr = explode('-', $tanggal);
	if(is_array($txt_periode_arr)) {
		$txt_periode = jin_nama_bulan($txt_periode_arr[1]) . ' ' . $txt_periode_arr[0];
	}
$gender = array(
	''		 => '--Pilih Gender--',
	'Pria' 	 => 'Pria',
	'Wanita' => 'Wanita'
);
?>

<div class="box box-success">

<div class="box-body">	
	<table  class="table table-bordered table-reponsive">
	<form enctype="multipart/form-data" accept-charset="utf-8" name="frm_editusers" id="frm_editusers"  method="post" action="<?=site_url()?>member/save_edit">
		<tr class="header_kolom">
			<th style="width:5%; vertical-align: middle; text-align:center">Photo</th>
			<th style="width:25%; vertical-align: middle; text-align:center"> Identitas Information  </th>
		</tr>
		<tr>
			<?php 
				$photo = null;
				if(!empty($member->photo)){
					$photo = base_url('uploads/member/'.$member->photo);
				}else{
					$photo = base_url('uploads/member/no_photo.jpg');
				}
			?>
			<td class="h_tengah" style="vertical-align:middle;">
			<img src="<?php echo $photo;?>"/> 		
			</td>
			<td> 
			<table class="table table-responsive">
			<tr style="vertical-align:middle;">
			<td width="10%"><b> Username </td>
			<td width="2%">:</td>
			<td>
			<input class="form-control" name="username" id="username" placeholder="Username" style="width:85%; height:18px;" type="text" value="<?php echo !empty($member->username) ? ucwords($this->converter->decode($member->username)) : '';?>">
			</td>
			<td width="14%"><b> Registration Date </td><td width="2%">:</td>
			<td colspan=4><?php echo !empty($member->create_date) ? date('d-M-Y', strtotime($member->create_date)) : '';?></td>
			</tr>
			<tr>
			<td><b> Fullname</b> </td><td width="2%">:</td>
			<td>
			<input class="form-control" style="width:85%; height:18px;" type="text" value="<?php echo !empty($member->first_name) || !empty($member->last_name) ? $member->first_name.' '.$member->last_name : '';?>" readonly>
			 </td>
			<td width="10%"><b> First Name</b> </td><td width="2%">:</td><td>
			<input class="form-control" name="first_name" id="first_name" placeholder="First Name" style="width:85%; height:18px;" type="text" value="<?php echo !empty($member->first_name) ? $member->first_name : '';?>">
			 </td>
			<td width="10%"><b> Last Name</b></td><td width="2%">:</td><td>
			<input class="form-control"  name="last_name" id="last_name" placeholder="Last Name" style="width:85%; height:18px;" type="text" value="<?php echo !empty($member->last_name) ? $member->last_name : '';?>">
			</td>
			</tr>
			<tr></tr>
			<tr>
			<td><b>Gender</b></td><td width="2%">:</td>
			<td>
			<?php echo form_dropdown('gender', $gender, !empty($member->gender) ? $member->gender : '', 'class="form-control" id="gender" style="height:30px;"');?>
			</td>
			<td><b>Email</b><span class="label label-danger pull-right email_error"></span></td><td width="2%">:</td><td colspan=4>
			<input class="form-control" name="email" id="email" placeholder="Email" style="width:95%; height:18px;" type="text" value="<?php echo !empty($member->email) ? $member->email : '';?>">
			</td>
			</tr>
			<tr><td><b>Photo</b></td><td width="2%">:</td><td colspan=7>
			<input id="file" name="filefoto" id="filefoto" style="width:100%;" type="file" class="custom-file-input" accept="image/*" />.
			</td></tr>			
			</table>
			</td>
		</tr>
	</table>
	
	<table  class="table table-bordered table-reponsive">
		<tr class="header_kolom">
			
			<th style="width:25%; vertical-align: middle; text-align:center"> Contact and Address Information  </th>
			
		</tr>
		<tr>
			
			<td> 
			<table class="table table-responsive">
			<tr><td width="10%"><b>Street</b></td><td width="2%">:</td><td>
			<input class="form-control" name="street_name" id="street_name" placeholder="Street" style="width:97%; height:18px;" type="text" value="<?php echo !empty($member->street_name) ? $member->street_name : '';?>">
			</td></tr>
			<tr><td><b>Province</b></td><td>:</td><td><?php echo $lp;?></td></tr>
			
			<tr><td><b>City </b> </td><td>:</td><td>			
			<?php echo $lc;?></td></tr>
			
			<tr><td><b>Postal Code</b></td><td>:</td><td>
			<input class="form-control" name="postal_code" id="postal_code" placeholder="Postal Code" style="width:97%; height:18px;" type="text" value="<?php echo !empty($member->postal_code) ? $member->postal_code : '';?>" onkeypress="return isNumber(event)" maxlength="6">
			</td></tr>
			<tr><td><b>Phone 1</b></td><td>:</td><td>
			<input class="form-control" name="phone1" id="phone1" placeholder="Phone 1" style="width:97%; height:18px;" type="text" value="<?php echo !empty($member->phone1) ? $member->phone1 : '';?>" onkeypress="return isNumber(event)" maxlength="15">
			</td></tr>
			<tr><td><b>Phone 2</b></td><td>:</td><td>
			<input class="form-control" name="phone2" id="phone2" placeholder="Phone 2" style="width:97%; height:18px;" type="text" value="<?php echo !empty($member->phone2) ? $member->phone2 : '';?>" onkeypress="return isNumber(event)" maxlength="15">
			</td></tr>			
			</table>
			</td>
			
			
		</tr>
	</form>
	</table>
	

</div>
<div class="box-footer" style="height:35px;">
	<div class="clearfix"></div>
	<div class="pull-right">
		<button type="button" class="btn btn-danger canc"><i class="glyphicon glyphicon-remove"></i> Cancel</button>	
		<button type="button" class="btn btn-success save"><i class="glyphicon glyphicon-ok"></i> Save</button>		
	</div>
</div>
</div>

<script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/theme_admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
	
<script type="text/javascript">

var session_level = '<?php echo $this->session->userdata('l_evel');?>';
var user_id = '<?php echo $member->user_id;?>';
$('#province_name').change(function() {
	var id = $('#province_name').val();
	$("#city_name > option").remove();
    $.ajax({
        url: '<?php echo site_url('member/get_cities/');?>/'+id,        
        type: 'POST',           
        data: {province_id : $('#province_name').val()},
        success: function(data) {
			
			$.each(data,function(id,city){
				var opt = $('<option />');
				opt.val(id);
				opt.text(city);
				$('#city_name').append(opt);
			});
        }
   });
});
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
} 
$('.save').click(function(){
	var email = $("#email").val();
	$('.email_error').text('');
	if(!(validateEmail(email))){
		$('.email_error').text('Email Invalid');
		return false;
	}
	$('#frm_editusers').submit();
});
 
</script>
