<style>
.form-control{
	height:17px;
	width:30%;
}
.panel-body{
	font-size:14px;
}
</style>

<script src="https://code.highcharts.com/highcharts.js"></script>

<div class="box-body" style="display:block">
    <label for="jenis2">Year : </label>
		<select id="tahun" name="tahun">				
		</select>
</div>

<!-- Small boxes (Stat box) -->
<div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
		  
		  <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><label id="lbltarget"></label><sup style="font-size: 20px">%</sup></h3>

              <p>Rata-Rata Pencapaian</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">
              <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
		  
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><label id="totalProvkab"></label></h3>

              <p>Jumlah Capaian Anggaran/Juta</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">
              <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><label id="totalmengisiSPM"></label></h3>

              <p>Jumlah Permasalahan</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">
              <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
          
          
        </div>
        
       <div class="col-lg-9">
        <div id="dinasnew" class="col-md-12">
        </div>
       </div>
        
</div>
<!-- /.row -->



<div class="row">
    <div id="columnProvinsi" class="col-md-12">
    </div>
</div>


<script type="text/javascript">

$(document).ready(function (){

    var d = new Date();
    var currentyear = d.getFullYear();
    var a = parseInt(currentyear)-5;

    for(var i=parseInt(currentyear);i>a;i--){
        if(i==currentyear){
            $("#tahun").append("<option value='"+i+"' selected>"+i+"</option>")    
        }else{
            $("#tahun").append("<option value='"+i+"'>"+i+"</option>")
        }        
    }

    loadChartnew(currentyear);
    loadChartProv(currentyear);
    loadValidSPM(currentyear);
    loadTotalUploadSPM(currentyear);
   // loadTotalProvKab(currentyear);
    loadTotalAngg(currentyear);

    $('#tahun').on('change', function() {
	    var tahun = $(this).val();	
        loadChartnew(tahun);
        loadChartProv(tahun);
        loadValidSPM(tahun);
        loadTotalUploadSPM(tahun);	
		//loadTotalProvKab(tahun);
		loadTotalAngg(tahun);
	});


});

function loadChartnew(tahun){
	    var url = '<?php echo site_url('home/getDatachart1');?>'+"/"+tahun;           
       $.ajax({

        url: url,
        dataType: 'json',
        error: function () {
            alert("error occured!!!");
        },
        success: function (data) {

            //set data source
            var d = new Date();
            var currentyear = d.getFullYear();
            var category = [];
            var datanya = [];
            for (i = 0; i < data.length; i++) {
                category.push(data[i].dinas);
                datanya.push(parseFloat(data[i].NilaiPersentasi));                
            }
			//y: parseFloat(data[i].Persentasi)

            //chart here..
            Highcharts.chart('dinasnew', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'PENCAPAIAN SPM  KABUPATEN SEMUA BIDANG.' //'PENCAPAIAN SPM BERDASARKAN PROVINSI'
                },
                xAxis: {
                    categories: category 
                },
                yAxis: [{
                    min: 0,
                    title: {
                        text: ''
                    }
                }],
                legend: {
                    shadow: false
                },
                tooltip: {
                    shared: true
                },
                plotOptions: {
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
									loadChart1Detail(tahun, this.category);
                                }
                            }
                        }
                    }
                },
                series: [{
                    name: 'Pencapaian Pelaporan SPM (%)',
                    color: 'rgba(18,170,10,1)',
                    data: datanya                  
                }]
            });
            //end
        }
    });
	
}	

function loadChartProv(tahun,dinas){

  
var url = '<?php echo site_url('homeprov/getDatachartProvKabupaten');?>'+"/"+tahun;    
$.ajax({

    url: url,
    dataType: 'json',
    error: function () {
        alert("error occured!!!");
    },
    success: function (data) {

        //set data source
        var d = new Date();
        var currentyear = d.getFullYear();
        var lastyear = d.getFullYear() - 1;
        var _data = [];
        for (i = 0; i < data.length; i++) {
         // var id_prov: data[i].id_prov;
            _data.push({
                    name: data[i].nama_prov+'-'+data[i].id_prov ,
                    //id_prov: data[i].id_prov,
                    y: parseFloat(data[i].nilaiPersentasi)
            });
            
        }

        //chart here..   xAxis: {
       // categories: ['A', 'B', 'C', 'D', 'E']   },
        Highcharts.chart('columnProvinsi', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Pencapaian SPM Urusan Berdasarkan Provinsi'
            },
            subtitle: {
                text: null
            },
             // xAxis: {
                // categories:  this.name
            // },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: null
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {
                            //loadChart2Detail(tahun,dinas, this.name);
                            //location.href = '<?php echo site_url('DinasR/indexprov'); ?>/' + this.name + '/' + tahun;
                            //alert(id_prov);
                            var valuenya = this.name;
                                var id = valuenya.split("-");
                                var idnya = id[1];
                                var namanya = id[0];
                                //alert(id[1]);
                                //loadChartKabupatenProvinsi(tahun, idnya, "prov", namanya);
                                loadChart2(tahun, idnya);
                            }
                        }
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },

            series: [
                {
                    name: "Provinsi",
                    colorByPoint: true,
                    data: _data
                }
            ]
        }
        
        //render button
        ,function (chart) { // on complete
                    var i = 0;

                    chart.renderer.button('<<',0, 5)
                        .attr({
                            zIndex: 3
                        })
                        .on('click', function () {
                            //do something here...
                            //alert('test');
                          //  loadChart1(tahun);
                        })
                        .add();

        }
        //end render

        );
        //end

        
    }
    });

}
function loadChart2(tahun,idprov){

  
    var url = '<?php echo site_url('homeprov/getDataKabupatenByID');?>'+"?tahun="+tahun+"&id_prov="+idprov;    
    $.ajax({

        url: url,
        dataType: 'json',
        error: function () {
            alert("error occured!!!");
        },
        success: function (data) {

            //set data source
            var d = new Date();
            var currentyear = d.getFullYear();
            var lastyear = d.getFullYear() - 1;
            var _data = [];
            for (i = 0; i < data.length; i++) {
             // var id_prov: data[i].id_prov;
                _data.push({
                        name: data[i].nama_kabkot+'-'+data[i].id_kabkot,						
                        y: parseFloat(data[i].nilaiPersentasi)
                });
                
            }

            //chart here..   xAxis: {
           // categories: ['A', 'B', 'C', 'D', 'E']   },
            Highcharts.chart('columnProvinsi', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Pencapaian SPM Urusan Berdasarkan Kabupaten'
                },
                subtitle: {
                    text: null
                },
				 // xAxis: {
					// categories:  this.name
				// },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: null
                    }

                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
								//loadChart2Detail(tahun,dinas, this.name);
								var valuenya = this.name;
                                var id = valuenya.split("-");
                                var idnya = id[1];
                                var namanya = id[0];
                                //alert(id[1]);
                                loadChartKabupatenProvinsi(tahun, idnya, "kab", namanya);
                                }
                            }
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },

                series: [
                    {
                        name: "Kabupaten",
                        colorByPoint: true,
                        data: _data
                    }
                ]
            }
            
            //render button
            ,function (chart) { // on complete
                        var i = 0;

                        chart.renderer.button('<<',0, 5)
                            .attr({
                                zIndex: 3
                            })
                            .on('click', function () {
                                //do something here...
								//alert('test');
                              //  loadChart1(tahun);
                            })
                            .add();

            }
            //end render

            );
            //end

            
        }
        });

}

function loadChartKabupatenProvinsi(tahun, id, type, nama){
        console.log("tahun:"+tahun+"|id:"+id+"|type:"+type);
	    var url = '<?php echo site_url('home/getDatachartForAllDinas');?>'+"?tahun="+tahun+"&id="+id+"?&type="+type;           
       $.ajax({

        url: url,
        dataType: 'json',
        error: function () {
            alert("error occured!!!");
        },
        success: function (data) {

            //set data source
            var d = new Date();
            var currentyear = d.getFullYear();
            var category = [];
            var datanya = [];
            for (i = 0; i < data.length; i++) {
                category.push(data[i].dinas);
                datanya.push(parseFloat(data[i].NilaiPersentasi));                
            }
			//y: parseFloat(data[i].Persentasi)

            //chart here..
            Highcharts.chart('columnProvinsi', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'PENCAPAIAN SPM KABUPATEN '+nama+' SEMUA BIDANG' //'PENCAPAIAN SPM BERDASARKAN PROVINSI'
                },
                xAxis: {
                    categories: category 
                },
                yAxis: [{
                    min: 0,
                    title: {
                        text: ''
                    }
                }],
                legend: {
                    shadow: false
                },
                tooltip: {
                    shared: true
                },
                plotOptions: {
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    var name1 = nama+"-"+id;
									var dinas = this.category;
									location.href = '<?php echo site_url('DinasR/indexkab'); ?>/' + name1 + '/' + tahun + '/' + dinas;
                                }
                            }
                        }
                    }
                },
                series: [{
                    name: 'Pencapaian Pelaporan SPM (%)',
                    color: 'rgba(18,170,10,1)',
                    data: datanya                  
                }]
            });
            //end
        }
    });
	
}



function loadChart1Detail(tahun,dinas){

 // alert(dinas);
   
    var url = '<?php echo site_url('home/getDatachart1Detail');?>'+"?tahun="+tahun+"&dinas="+dinas;           

    $.ajax({

        url: url,
        dataType: 'json',
        error: function () {
            alert("error occured!!!");
        },
        success: function (data) {

            //set data source
            var d = new Date();
            var currentyear = d.getFullYear();
            var lastyear = d.getFullYear() - 1;
            var _data = [];
            for (i = 0; i < data.length; i++) {
             // var id_prov: data[i].id_prov;
                _data.push({
                        name: data[i].nama_prov+'-'+data[i].id_prov ,
						//id_prov: data[i].id_prov,
                        y: parseFloat(data[i].Persentasi)
                });
                
            }

            //chart here..   xAxis: {
           // categories: ['A', 'B', 'C', 'D', 'E']   },
            Highcharts.chart('dinasnew', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Pencapaian SPM Urusan '+dinas+' Berdasarkan Provinsi'
                },
                subtitle: {
                    text: null
                },
				 // xAxis: {
					// categories:  this.name
				// },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: null
                    }

                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
								loadChart2Detail(tahun,dinas, this.name);
							    //alert(id_prov);
							
                                }
                            }
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },

                series: [
                    {
                        name: "Provinsi",
                        colorByPoint: true,
                        data: _data
                    }
                ]
            }
            
            //render button
            ,function (chart) { // on complete
                        var i = 0;

                        chart.renderer.button('<<',0, 5)
                            .attr({
                                zIndex: 3
                            })
                            .on('click', function () {
                                //do something here...
								//alert('test');
                              //  loadChart1(tahun);
                            })
                            .add();

            }
            //end render

            );
            //end

            
        }
        });

}


function loadChart2Detail(tahun,dinas,prov){

 // alert(dinas);
   
    var url = '<?php echo site_url('home/getDatachart2Detail');?>'+"?tahun="+tahun+"&dinas="+dinas+"&prov="+prov;           

    $.ajax({

        url: url,
        dataType: 'json',
        error: function () {
            alert("error occured!!!");
        },
        success: function (data) {

            //set data source
            var d = new Date();
            var currentyear = d.getFullYear();
            var lastyear = d.getFullYear() - 1;
            var _data = [];
            for (i = 0; i < data.length; i++) {
                
                _data.push({
                        name: data[i].nama_kabkot+'-'+data[i].id_kabkot ,
						y: parseFloat(data[i].Persentasi)
                });
                
            }

            //chart here..
            Highcharts.chart('dinasnew', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Pencapaian SPM Urusan '+dinas+' Berdasarkan Kabupaten.'
                },
                subtitle: {
                    text: null
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: null
                    }

                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
									//loadChart1Detail(tahun, this.category);
                                   // location.href = '<?php echo site_url('DinasR/indexProv'); ?>' //+
									location.href = '<?php echo site_url('DinasR/indexKab'); ?>/' + this.name + '/' + tahun + '/' + dinas;
                                    //location.href = 'https://en.wikipedia.org/wiki/' +
                                    //   this.options.key;
								// alert(this.id_prov);
                                }
                            }
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },

                series: [
                    {
                        name: "Provinsi",
                        colorByPoint: true,
                        data: _data
                    }
                ]
            }
            
            //render button
            ,function (chart) { // on complete
                        var i = 0;

                        chart.renderer.button('<<',0, 5)
                            .attr({
                                zIndex: 3
                            })
                            .on('click', function () {
                                //do something here...
								//alert('test');
                              //  loadChart1(tahun);
                            })
                            .add();

            }
            //end render

            );
            //end

            
        }
        });

}


//region valid SPM
function loadValidSPM($tahun){
    var url = '<?php echo site_url('home/getPersentasiSPM');?>'+"/"+$tahun;       

    $.ajax({
        url:url,
        dataType:'json',
        error:function(){
            alert("error occured!!!");
        },
        success:function(data){
            $("#lbltarget").text(parseFloat(data[0].Persentasi).toFixed(2));
        }
    });
}

function loadTotalAngg($tahun){
   // var url = '<?php echo site_url('home/getJumlahProvinsiKab');?>'; 
    var url = '<?php echo site_url('homeadmin/getJumlahAnggaran');?>'+"/"+$tahun;  	

    $.ajax({
        url:url,
        dataType:'json',
        error:function(){
            alert("error occured!!!");
        },
        success:function(data){
			var ttl = data[0].Total;
			var ttl2 = parseFloat(ttl)/1000000;
            $("#totalProvkab").text(ttl2);
        }
    });
}
//endregion

//region total Prov Kab Upload SPM
function loadTotalUploadSPM($tahun){
    var url = '<?php echo site_url('home/getJumlahMasalah');?>'+"/"+$tahun;       

    $.ajax({
        url:url,
        dataType:'json',
        error:function(){
            alert("error occured!!!");
        },
        success:function(data){
            $("#totalmengisiSPM").text(data[0].Total);
        }
    });
}
//endregion



</script>