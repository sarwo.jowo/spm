<style>
.form-control{
	height:17px;
	width:30%;
}
.panel-body{
	font-size:14px;
}
</style>

<script src="https://code.highcharts.com/highcharts.js"></script>

<div class="box-body" style="display:block">
    <label for="jenis2">Year : </label>
		<select id="tahun" name="tahun">				
		</select>
</div>

<!-- Small boxes (Stat box) -->
<div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><label id="totalProvkab"></label></h3>

              <p>Pelaporan</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">
              <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><label id="totalmengisiSPM"></label></h3>

              <p>Penggaran</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">
              <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><label id="lbltarget"></label><sup style="font-size: 20px">%</sup></h3>

              <p>Permasalahan</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">
              <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
          
        </div>
        
       <div class="col-lg-9">
        <div id="pieDinas" class="col-md-12">
        </div>
       </div>
        
</div>
<!-- /.row -->



<div class="row">
    <div id="columnProvinsi" class="col-md-12">
    </div>
</div>


<script type="text/javascript">

$(document).ready(function (){

    var d = new Date();
    var currentyear = d.getFullYear();
    var a = parseInt(currentyear)-5;

    for(var i=parseInt(currentyear);i>a;i--){
        if(i==currentyear){
            $("#tahun").append("<option value='"+i+"' selected>"+i+"</option>")    
        }else{
            $("#tahun").append("<option value='"+i+"'>"+i+"</option>")
        }        
    }

    loadChart1(currentyear);
    loadChart2(currentyear);
    loadValidSPM(currentyear);
    loadTotalUploadSPM(currentyear);
    loadTotalProvKab();


    $('#tahun').on('change', function() {
	    var tahun = $(this).val();	
        loadChart1(tahun);
        loadChart2(tahun);
        loadValidSPM(tahun);
        loadTotalUploadSPM(tahun);	
	});


});

//Pie Chart
function loadChart1(tahun){

    var url = '<?php echo site_url('home/getDatachart1');?>'+"/"+tahun;           
    var data = "tahun="+tahun;
    $.ajax({

        url: url,
        dataType: 'json',        
        error: function () {
            alert("error occured!!!");
        },
        success: function (data) {

            //set data source
            var d = new Date();
            var currentyear = d.getFullYear();
            var _data = [];
            for (i = 0; i < data.length; i++) {
                
                //var nilaiy=parseFloat(data[i].NilaiPersentasi)/data[i].total*100;
                var nilaiy=parseFloat(data[i].NilaiPersentasi);

                if(i==0){
                    _data.push({
                        name: data[i].dinas,                        
                        y: nilaiy,
                        sliced: true,
                        selected: true
                    });
                }
                else{
                    _data.push({
                        name: data[i].dinas,
                        y: nilaiy
                    });
                }
                
            }
            
			/**
			 Highcharts.chart('pieDinas', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'PENCAPAIAN SPM BERDASARKAN URUSAN' 
                },
                xAxis: {
                    categories: category 
                },
                yAxis: [{
                    min: 0,
                    title: {
                        text: ''
                    }
                }],
                legend: {
                    shadow: false
                },
                tooltip: {
                    shared: true
                },
                plotOptions: {
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                   // location.href = '<?php echo site_url('home/dataperdinas'); ?>' //+
                                    //location.href = 'https://en.wikipedia.org/wiki/' +
                                    //   this.options.key;
                                }
                            }
                        }
                    }
                },
                series: [{
                    name: 'dinas',
                    color: 'rgba(18,170,10,1)',
                    data: _data                     
                }]
            });
			**/
			
            
            Highcharts.chart('pieDinas', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'PENCAPAIAN SPM BERDASARKAN URUSAN'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    loadChart1Detail(tahun, this.name);
                                    // location.href = '<php echo site_url('home/dataperdinas'); ?>' //+
                                    // //+'?value=' + this.x; //untuk value
                                    // +'?jenis=' + this.name; //untuk name
                                }
                            }
                        }
                    }
                },
                series: [{
                    name: 'dinas',
                    colorByPoint: true,
                    data: _data                    
                }]
            });
			
			
            //end
        }
    });
}

//column chart
function loadChart2(tahun){

    var url = '<?php echo site_url('home/getDatachart2');?>'+"/"+tahun;           
    $.ajax({

        url: url,
        dataType: 'json',
        error: function () {
            alert("error occured!!!");
        },
        success: function (data) {

            //set data source
            var d = new Date();
            var currentyear = d.getFullYear();
            var category = [];
            var datanya = [];
            for (i = 0; i < data.length; i++) {
                category.push(data[i].nama_prov);
                datanya.push(parseFloat(data[i].nilaiPersentasi));                
            }

            //chart here..
            Highcharts.chart('columnProvinsi', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'PENCAPAIAN SPM BERDASARKAN PROVINSI SEMUA URUSAN' //'PENCAPAIAN SPM BERDASARKAN PROVINSI'
                },
                xAxis: {
                    categories: category 
                },
                yAxis: [{
                    min: 0,
                    title: {
                        text: ''
                    }
                }],
                legend: {
                    shadow: false
                },
                tooltip: {
                    shared: true
                },
                plotOptions: {
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                   // location.href = '<?php echo site_url('home/dataperdinas'); ?>' //+
                                    //location.href = 'https://en.wikipedia.org/wiki/' +
                                    //   this.options.key;
                                }
                            }
                        }
                    }
                },
                series: [{
                    name: 'Provinsi',
                    color: 'rgba(18,170,10,1)',
                    data: datanya                    
                }]
            });
            //end
        }
    });
}

function loadChart1Detail(tahun,dinas){

    var url = '<?php echo site_url('home/getDatachart1Detail');?>'+"?tahun="+tahun+"&dinas="+dinas;           

    $.ajax({

        url: url,
        dataType: 'json',
        error: function () {
            alert("error occured!!!");
        },
        success: function (data) {

            //set data source
            var d = new Date();
            var currentyear = d.getFullYear();
            var lastyear = d.getFullYear() - 1;
            var _data = [];
            for (i = 0; i < data.length; i++) {
                
                _data.push({
                        name: data[i].nama_prov,
                        y: parseFloat(data[i].Persentasi)
                });
                
            }

            //chart here..
            Highcharts.chart('pieDinas', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Pencapaian SPM Urusan '+dinas+' Berdasarkan Provinsi'
                },
                subtitle: {
                    text: null
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: null
                    }

                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:.1f}%'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },

                series: [
                    {
                        name: "Provinsi",
                        colorByPoint: true,
                        data: _data
                    }
                ]
            }
            
            //render button
            ,function (chart) { // on complete
                        var i = 0;

                        chart.renderer.button('<<',0, 5)
                            .attr({
                                zIndex: 3
                            })
                            .on('click', function () {
                                //do something here...
                                loadChart1(tahun);
                            })
                            .add();

            }
            //end render

            );
            //end

            
        }
        });

}

//region valid SPM
function loadValidSPM($tahun){
    var url = '<?php echo site_url('home/getPersentasiSPM');?>'+"/"+$tahun;       

    $.ajax({
        url:url,
        dataType:'json',
        error:function(){
            alert("error occured!!!");
        },
        success:function(data){
            $("#lbltarget").text(parseFloat(data[0].Persentasi).toFixed(2));
        }
    });
}

//endregion

//region total Prov Kab 
function loadTotalProvKab(){
    var url = '<?php echo site_url('home/getJumlahProvinsiKab');?>';       

    $.ajax({
        url:url,
        dataType:'json',
        error:function(){
            alert("error occured!!!");
        },
        success:function(data){
            $("#totalProvkab").text(data[0].Total);
        }
    });
}
//endregion

//region total Prov Kab Upload SPM
function loadTotalUploadSPM($tahun){
    var url = '<?php echo site_url('home/getTotalprovkabspm');?>'+"/"+$tahun;       

    $.ajax({
        url:url,
        dataType:'json',
        error:function(){
            alert("error occured!!!");
        },
        success:function(data){
            $("#totalmengisiSPM").text(data[0].Total);
        }
    });
}
//endregion


</script>