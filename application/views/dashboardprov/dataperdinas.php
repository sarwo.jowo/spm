<style>
.form-control{
	height:17px;
	width:30%;
}
.panel-body{
	font-size:10px;
}
.style1{
    text-align:'center';
}

</style>

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<h2>LAPORAN PENCAPAIAN SPM BIDANG KESEHATAN</h2>    
<br/>

<div class="row">    
        <label style="font:bold;color: red;">Note: R=Realisasi, S=Sasaran, P=Persentasi</label>
    <div class="table-responsive">
    <table id="book-table" class="table table-bordered text_font small">
        <thead style="text-align:center">
            <tr>
                <th rowspan="3">NO</th>
                <th rowspan="3">PROVINSI</th>
                <th rowspan="3" style="width:30">PROVINSI DAN KAB/KOTA</th>
                <th colspan="3">INDIKATOR 1</th>
                <th colspan="3">INDIKATOR 2</th>
                <th colspan="3">INDIKATOR 3</th>
                <th colspan="3">INDIKATOR 4</th>
                <th colspan="3">INDIKATOR 5</th>
                <th colspan="3">INDIKATOR 6</th>
                <th colspan="3">INDIKATOR 7</th>
                <th colspan="3">INDIKATOR 8</th>
                <th colspan="3">INDIKATOR 9</th>
                <th colspan="3">INDIKATOR 10</th>
                <th colspan="3">INDIKATOR 11</th>
                <th colspan="3">INDIKATOR 12</th>
            </tr>
            <tr>
                <th colspan="3">2017</th>
                <th colspan="3">2017</th>
                <th colspan="3">2017</th>
                <th colspan="3">2017</th>
                <th colspan="3">2017</th>
                <th colspan="3">2017</th>
                <th colspan="3">2017</th>
                <th colspan="3">2017</th>
                <th colspan="3">2017</th>
                <th colspan="3">2017</th>
                <th colspan="3">2017</th>
                <th colspan="3">2017</th>
            </tr>
            <tr>
                <th>R</th>
                <th>S</th>
                <th>P</th>
                <th>R</th>
                <th>S</th>
                <th>P</th>
                <th>R</th>
                <th>S</th>
                <th>P</th>
                <th>R</th>
                <th>S</th>
                <th>P</th>
                <th>R</th>
                <th>S</th>
                <th>P</th>
                <th>R</th>
                <th>S</th>
                <th>P</th>
                <th>R</th>
                <th>S</th>
                <th>P</th>
                <th>R</th>
                <th>S</th>
                <th>P</th>
                <th>R</th>
                <th>S</th>
                <th>P</th>
                <th>R</th>
                <th>S</th>
                <th>P</th>
                <th>R</th>
                <th>S</th>
                <th>P</th>
                <th>R</th>
                <th>S</th>
                <th>P</th>
            </tr>
        </thead>
        <tbody>                 
        </tbody>
    </table>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function() {
    var groupColumn = 1;    
    var orderbyNo = 0;
    
    $('#book-table').DataTable({
        "columnDefs": [
            { "visible": false, "targets": groupColumn }
        ]   ,
        "order": [[ orderbyNo, 'asc' ]],
        "displayLength": 25,
        "scrollX": true,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        },
        "ajax": {
            url : "<?php echo site_url("home/get_dummydata") ?>",
            type : 'GET'
        }                
    });

    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
        var currentOrder = table.order()[0];
        if ( currentOrder[0] === groupColumn && currentOrder[1] === 'asc' ) {
            table.order( [ groupColumn, 'desc' ] ).draw();
        }
        else {
            table.order( [ groupColumn, 'asc' ] ).draw();
        }
    } );
});
</script>