<style>
.form-control{
	height:17px;
	width:30%;
}
.panel-body{
	font-size:10px;
}
.style1{
    text-align:'center';
}

/* Master Detail */
td.details-control {
    
    background: url('../assets/images/tick.png') no-repeat center center;
    cursor: pointer;
}
tr.shown td.details-control {
    background: url('../assets/images/uparrow.png') no-repeat center center;    
}
/* end master Detail */

</style>

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<h2>Dasar Hukum :Peraturan Menteri Sosial Republik Indonesia Nomor 9 Tahun 2018</h2>    
<br/>

<div class="row">            
    <div class="table-responsive">
    <table id="book-table" class="table table-bordered text_font small">
        <thead style="text-align:center">
            <tr>
                <th></th>
                <th>Wilayah</th>
                <th>Indikator Kinerja sesuai Tugas dan Fungsi SKPD</th>                
                <th>Indikator Pencapaian</th>                
                <th>Pencapaian</th>          
                <th>Sasaran</th>      
            </tr>            
            <tr>
                <th></th>
                <th>(1)</th>
                <th>(2)</th>
                <th>(3)</th>
                <th>(4)</th>
                <th>(5)</th>                
            </tr>
        </thead>
        <tbody>                 
        </tbody>
    </table>
    </div>
</div>


<script type="text/javascript">
// $(document).ready(function() {

//     var groupColumn = 0;    
//     var orderbyNo = 0;

//     $('#book-table').DataTable({
//         "ajax": {
//             url : "<?php echo site_url("home/get_dummydatadetail") ?>",
//             type : 'GET'
//         },
//         "columnDefs": [
//             { "visible": false, "targets": groupColumn }
//         ]   ,
//         "order": [[ groupColumn, 'desc' ]],
//         "displayLength": 25,
//         "   scrollX": true,
//         "drawCallback": function ( settings ) {
//             var api = this.api();
//             var rows = api.rows( {page:'current'} ).nodes();
//             var last=null;
 
//             api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
//                 if ( last !== group ) {
//                     $(rows).eq( i ).before(
//                         '<tr class="group"><td colspan="5">'+group+'</td></tr>'
//                     );
 
//                     last = group;
//                 }
//             } );
//         },
//     });
// });

$(document).ready(function() {
    var groupColumn = 1;    
    var table = $('#book-table').DataTable( {
        "ajax": {
            url : "<?php echo site_url("home/get_dummydatadetail") ?>",
            type : 'GET'
        },
        "columns": [
            {
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
            { "data": "wilayah" },
            { "data": "Indikator_Kinerja" },
            { "data": "Indikator_pencapaian" },            
            { "data": "TotalPencapaian" },
            { "data": "SasaranPencapaian" }
        ],
        "order": [[1, 'desc']],
        //Grouping
        "displayLength": 25,
        "columnDefs": [
            { "visible": false, "targets": groupColumn }
        ],
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        },
        //end
    } );
     

     //Master Detail 
    // Add event listener for opening and closing details
    $('#book-table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
    //end Master Detail
} );

//Master Detail
function format ( d ) {
    // `d` is the original data object for the row
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        // '<tr>'+
        //     '<td>Item:</td>'+
        //     '<td>'+d.Wilayah+'</td>'+
        // '</tr>'+
        '<tr>'+
            '<td></td>'+
            '<td>'+d.items+'</td>'+
            '<td>'+d.pencapaian+'</td>'+
        '</tr>'+        
    '</table>';
}
//end master Detail
</script>