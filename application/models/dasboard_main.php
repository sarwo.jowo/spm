 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dasboard_main extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

    public function get_dummydata()
    {
           
        return $this->db->get("tempdata");
    }
    
    function getDataforDashboarPie($tahun)
    {                
        $query = " select a.dinas, COALESCE(x.Persentasi,0) as 'NilaiPersentasi' from 
                    tbl_master_dinas a 
                    left join
                    (
                        SELECT d.dinas_id, Round(sum(a.target)/count(a.ID),2) as 'Persentasi' FROM `tbl_dinas_indikator_detail` a 
                        inner join tbl_dinas_indikator b on a.indikator_id=b.indikator_id 
                        inner join tbl_dinas c on b.id_tr_dinas=c.id_tr_dinas
                        inner join tbl_master_dinas d on c.dinas=d.dinas_id
                        and c.tahun='$tahun'
                        group by dinas_id
                    )x
                    on x.dinas_id=a.dinas_id";

        $result = $this->db->query($query)->result();
		//return $result;    

        return json_encode($result);
    }
	function getDataforDashboarPie2($tahun)
    { 
        $id_prov = $this->session->userdata('id_prov');		
        $query = " select a.dinas, COALESCE(x.Persentasi,0) as 'NilaiPersentasi' from 
                    tbl_master_dinas a 
                    left join
                    (
                        SELECT d.dinas_id, c.id_prov,Round(sum(a.target)/count(a.ID),2) as 'Persentasi' FROM `tbl_dinas_indikator_detail` a 
                        inner join tbl_dinas_indikator b on a.indikator_id=b.indikator_id 
                        inner join tbl_dinas c on b.id_tr_dinas=c.id_tr_dinas
                        inner join tbl_master_dinas d on c.dinas=d.dinas_id
                        and c.tahun='$tahun' and c.id_prov = '$id_prov'
                        group by dinas_id
                    )x
                    on x.dinas_id=a.dinas_id";

        $result = $this->db->query($query)->result();
		//return $result;    

        return json_encode($result);
    }
	function getDataforDashboarPie3($tahun)
    {    
        $id_kabkot = $this->session->userdata('id_kabkot');	
        $query = " select a.dinas, COALESCE(x.Persentasi,0) as 'NilaiPersentasi' from 
                    tbl_master_dinas a 
                    left join
                    (
                        SELECT d.dinas_id, Round(sum(a.target)/count(a.ID),2) as 'Persentasi' FROM `tbl_dinas_indikator_detail` a 
                        inner join tbl_dinas_indikator b on a.indikator_id=b.indikator_id 
                        inner join tbl_dinas c on b.id_tr_dinas=c.id_tr_dinas
                        inner join tbl_master_dinas d on c.dinas=d.dinas_id
                        and c.tahun='$tahun' and c.id_kabkot = '$id_kabkot' 
                        group by dinas_id
                    )x
                    on x.dinas_id=a.dinas_id";

        $result = $this->db->query($query)->result();
		//return $result;    

    return json_encode($result);
    }

    function getDataForDashboardPieDetail($tahun, $dinas){
                $query=" SELECT e.nama_prov,e.id_prov, sum(a.target)/count(a.ID) as 'Persentasi' FROM `tbl_dinas_indikator_detail` a 
                inner join tbl_dinas_indikator b on a.indikator_id=b.indikator_id 
                inner join tbl_dinas c on b.id_tr_dinas=c.id_tr_dinas
                inner join tbl_master_dinas d on c.dinas=d.dinas_id
                inner join prop e on e.id_prov=c.Id_prov
                and c.tahun='$tahun'     
                and d.dinas like '$dinas'
                group by e.id_prov";

                $result = $this->db->query($query)->result();
                
                return json_encode($result);
    }
	
	function getDataForDashboardPieDetail2($tahun, $dinas,$prov){
		       // and c.id_prov='$prov'				
               // and d.dinas like '$dinas'
                $query=" SELECT c.id_kabkot,d.dinas,e.nama_kabkot, sum(a.target)/count(a.ID) as 'Persentasi' FROM `tbl_dinas_indikator_detail` a 
                inner join tbl_dinas_indikator b on a.indikator_id=b.indikator_id 
                inner join tbl_dinas c on b.id_tr_dinas=c.id_tr_dinas
                inner join tbl_master_dinas d on c.dinas=d.dinas_id				
                inner join kab e on e.id_kabkot=c.id_kabkot				
                and c.tahun='$tahun' 
                and d.dinas like '$dinas'				
                group by e.id_kabkot";

                $result = $this->db->query($query)->result();
                
               
				//$result = $this->db->last_query();		
		      //   error_log($result);
				  return json_encode($result);
    }

    function getDataforDashboardColumn($tahun)
    {                
        $query = "SELECT p.nama_prov, 
                    case when x.Persentasi is null then 0
                    else x.Persentasi
                    end as 'nilaiPersentasi'
                    FROM 
                    prop p
                    
                    left join 
                    (
                        select c.dinas,c.id_prov, round(sum(a.target)/count(a.ID),2) as 'Persentasi'
                        from
                        tbl_dinas_indikator_detail a 
                        inner join tbl_dinas_indikator b on a.indikator_id=b.indikator_id 
                        inner join tbl_dinas c on b.id_tr_dinas=c.id_tr_dinas
                        inner join tbl_master_dinas d on c.dinas=d.dinas_id
                        and c.tahun='$tahun'
                        group by c.id_prov
                    ) x 
                    on p.id_prov=x.id_prov";

        $result = $this->db->query($query)->result();
		//return $result;    
       
        return json_encode($result);
    }
	
	function getDataforDashboardColumnprov($tahun)
    {   
    $id_prov = $this->session->userdata('id_prov');	
       $query = "SELECT p.nama_kabkot, 
                    case when x.Persentasi is null then 0
                    else x.Persentasi
                    end as 'nilaiPersentasi'
                    FROM 
                    kab p
                    
                    inner join 
                    (
                        select c.dinas,c.id_prov,c.id_kabkot, round(sum(a.target)/count(a.ID),2) as 'Persentasi'
                        from
                        tbl_dinas_indikator_detail a 
                        inner join tbl_dinas_indikator b on a.indikator_id=b.indikator_id 
                        inner join tbl_dinas c on b.id_tr_dinas=c.id_tr_dinas
                        and c.tahun='$tahun' and c.id_prov =  '$id_prov'
                        group by c.id_kabkot
                    ) x 
                    on p.id_kabkot=x.id_kabkot";

        $result = $this->db->query($query)->result();
        
        return json_encode($result);		
    }
	
	
	function getDataforDashboardColumndemo($tahun)
    {   
      $id_prov = $this->session->userdata('id_prov');	
       $query = "SELECT p.nama_kabkot
                    FROM 
                    kab p
                    
                    left join 
                    (
                        select c.dinas,c.id_prov,c.id_kabkot
                        from  tbl_dinas c
                        where c.tahun='$tahun' and c.id_prov =  $id_prov 
                        group by c.id_kabkot
                    ) x 
                    on p.id_kabkot=x.id_kabkot";

        $result = $this->db->query($query)->result();
        
        return json_encode($result);
		// $result = $this->db->last_query();
		
		// error_log($result);
    }
	function getDataforDashboardColumndinas($tahun)
    {   
    $id_prov = $this->session->userdata('id_prov');	
       $query = "SELECT p.nama_prov, 
                    case when x.Persentasi is null then 0
                    else x.Persentasi
                    end as 'nilaiPersentasi'
                    FROM 
                    prop p
                    
                    left join 
                    (
                        select c.dinas,c.id_prov, round(sum(a.target)/count(a.ID),2) as 'Persentasi'
                        from
                        tbl_dinas_indikator_detail a 
                        inner join tbl_dinas_indikator b on a.indikator_id=b.indikator_id 
                        inner join tbl_dinas c on b.id_tr_dinas=c.id_tr_dinas
                        inner join tbl_master_dinas d on c.dinas=d.dinas_id
                        and c.tahun='$tahun' and c.id_prov =  $id_prov 
                        group by c.id_prov
                    ) x 
                    on p.id_prov=x.id_prov";

        $result = $this->db->query($query)->result();

        return json_encode($result);
    }

    //box 3
    function getPersentasiSPM($tahun)
    {                
        $query = "select coalesce(sum(a.target)/count(a.ID),0) as 'Persentasi'
                from
                tbl_dinas_indikator_detail a 
                inner join tbl_dinas_indikator b on a.indikator_id=b.indikator_id 
                inner join tbl_dinas c on b.id_tr_dinas=c.id_tr_dinas
                inner join tbl_master_dinas d on c.dinas=d.dinas_id
                and c.tahun='$tahun'";

        $result = $this->db->query($query)->result();
		//return $result;    

        return json_encode($result);
    }
	
	
   function getPersentasiSPMprov($tahun)
    {  
        $id_prov = $this->session->userdata('id_prov');	
        $query = "select coalesce(sum(a.target)/count(a.ID),0) as 'Persentasi'
                from
                tbl_dinas_indikator_detail a 
                inner join tbl_dinas_indikator b on a.indikator_id=b.indikator_id 
                inner join tbl_dinas c on b.id_tr_dinas=c.id_tr_dinas
                inner join tbl_master_dinas d on c.dinas=d.dinas_id
                and c.tahun='$tahun' and c.id_prov = $id_prov";

        $result = $this->db->query($query)->result();
		//return $result;    

        return json_encode($result);
    }
	
	
	function getPersentasiSPMkab($tahun)
    {  
        $id_kabkot = $this->session->userdata('id_kabkot');	
        $query = "select coalesce(sum(a.target)/count(a.ID),0) as 'Persentasi'
                from
                tbl_dinas_indikator_detail a 
                inner join tbl_dinas_indikator b on a.indikator_id=b.indikator_id 
                inner join tbl_dinas c on b.id_tr_dinas=c.id_tr_dinas
                inner join tbl_master_dinas d on c.dinas=d.dinas_id
                and c.tahun='$tahun' and c.id_kabkot = $id_kabkot";

        $result = $this->db->query($query)->result();
		//return $result;    

        return json_encode($result);
    }

    //box 1
	function getJumlahProvinsiKab2()
    {    
        $id_prov = $this->session->userdata('id_prov');		
        $query ="select coalesce(sum(x.total),0) as 'Total' 
                from
                ( 
                 select count(id_kabkot) as 'total' from kab where id_prov = $id_prov
                )x ";

        $result = $this->db->query($query)->result();
		//return $result;    

        return json_encode($result);
    }
	
	function getJumlahProvinsiKab3()
    {    
        $id_kabkot = $this->session->userdata('id_kabkot');			
        $query ="select coalesce(sum(x.total),0) as 'Total' 
                from
                ( 
                 select count(id_kabkot) as 'total' from kab where id_kabkot = $id_kabkot
                )x ";

        $result = $this->db->query($query)->result();
		//return $result;    

        return json_encode($result);
    }
    function getJumlahProvinsiKab()
    {                
        $query ="select coalesce(sum(x.total),0) as 'Total' 
                from
                ( 
                    SELECT count(id_prov) as 'total' from prop 
                    union
                    select count(id_kabkot) as 'total' from kab
                )x ";

        $result = $this->db->query($query)->result();
		//return $result;    

        return json_encode($result);
    }
	
	//untuk Box Total
	function getJmlAngg($tahun)
    {                
               $query ="select  FORMAT(sum(a.targetang),2) as 'Total' from tbl_anggaran_indikator a,  tbl_newanggaran b 
                where a.id_tr_dinas=b.id_tr_dinas and b.tahun='$tahun'";

        $result = $this->db->query($query)->result();
        return json_encode($result);
    }
	
		//untuk Box Total
	function getJmlMasalah($tahun)
    {                
         $query ="select  coalesce(count(id_tr),0) as 'Total' from tbl_masalah where tahun='$tahun'";;

        $result = $this->db->query($query)->result();
        return json_encode($result);
    }

    //box 2
    function getTotalprovkabspm($tahun)
    {                
        $query ="select  coalesce(count(distinct b.id_tr_dinas),0) as 'Total' from tbl_dinas_indikator a, tbl_dinas b 
                where a.id_tr_dinas=b.id_tr_dinas and tahun='$tahun'";

        $result = $this->db->query($query)->result();
		//return $result;    

        return json_encode($result);
    }
	
	function getTotalprovkabspm2($tahun)
    {     
        $id_prov = $this->session->userdata('id_prov');	
        $query ="select  coalesce(count(distinct b.id_tr_dinas),0) as 'Total' from tbl_dinas_indikator a, tbl_dinas b 
                where a.id_tr_dinas=b.id_tr_dinas and tahun='$tahun' and id_prov = $id_prov";

        $result = $this->db->query($query)->result();
		//return $result;    

        return json_encode($result);
    }
	
	function getTotalprovkabspm3($tahun)
    {     
        $id_kabkot = $this->session->userdata('id_kabkot');	
        $query ="select  coalesce(count(distinct b.id_tr_dinas),0) as 'Total' from tbl_dinas_indikator a, tbl_dinas b 
                where a.id_tr_dinas=b.id_tr_dinas and tahun='$tahun' and id_kabkot = $id_kabkot";

        $result = $this->db->query($query)->result();
		//return $result;    

        return json_encode($result);
    }
}