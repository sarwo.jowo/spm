<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pendapatan extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function get()
	{
		$this->db->select('*');
		$this->db->like('tanggal','2013-');
		$this->db->order_by('tanggal','asc');
		return $this->db->get('tbl_pendapatan');
	}
	public function get2()
	{
		$sql = "SELECT * FROM tbl_pendapatan ";		
		$result['count'] = $this->db->query($sql)->num_rows();
		$result['data'] = $this->db->query($sql)->result();
		return $result;
	}
}

/* End of file pendapatan.php */
/* Location: ./application/models/pendapatan.php */