<?php
class Files_Model extends CI_Model {
 
   public function insert_file($filename, $idsb)
   {
      $data = array(
         'filename'     => $filename,
         'idsb'        => $idsb
      );
      $this->db->insert('file', $data);
      return $this->db->insert_id();
   }

public function get_files()
{
  $this->db->select("*");
  $this->db->from('file');
  $query = $this->db->get();
  return $query->result();

}

public function delete_file($file_id)
{
   $file = $this->get_file($file_id);
   if (!$this->db->where('id', $file_id)->delete('file'))
   {
      return FALSE;
   }
   unlink('./asset/upload/' . $file->filename);  
   return TRUE;
}
 
public function get_file($file_id)
{
   return $this->db->select()
         ->from('file')
         ->where('id', $file_id)
         ->get()
         ->row();
}
 
}
?>