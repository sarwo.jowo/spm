<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gtc_m extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	function get_gtc_ajax($offset, $limit, $q='', $sort, $order) {
		$sql = "SELECT * FROM history_gtc";
			
		$result['count'] = $this->db->query($sql)->num_rows();
		$sql .=" ORDER BY {$sort} {$order} ";
		$sql .=" LIMIT {$offset},{$limit} ";
		$result['data'] = $this->db->query($sql)->result();
		return $result;
	}
	
	function get_member_ajax($offset, $limit, $q='', $sort, $order) {
		$sql = "SELECT * FROM member ";		
		$result['count'] = $this->db->query($sql)->num_rows();
		$sql .=" ORDER BY {$sort} {$order} ";
		$sql .=" LIMIT {$offset},{$limit} ";
		$result['data'] = $this->db->query($sql)->result();		
		return $result;
	}
	
	function get_lastgtc($jenis=0){
		$this->db->where('jn', $jenis);
		$this->db->order_by('id', 'desc');
		return $this->db->get('gtc',1)->row();
	}
	
	public function create() {
		if (str_replace(',', '', $this->input->post('harga')) < 0) {
			return FALSE;
		}
		if (str_replace(',', '', $this->input->post('unit')) < 0) {
			return FALSE;
		}
		
		// $last_gtc = $this->get_lastgtc(1); 
		
		
		// TRANSACTIONAL DB COMMIT
		$this->db->trans_start();	
		$data = array(			
			'username'		=> 'dxplor',
			'price'		    => str_replace(',', '', $this->input->post('harga')),	
			'unit'		    => str_replace(',', '', $this->input->post('unit')),	
			'jn'		    => 1,	
			'ket'		    => 'Jual'	
				
		);		
		$this->db->insert('history_gtc', $data);
		
		$data_gtc = array(			
			'username'		=> 'dxplor',
			'price_jual'    => str_replace(',', '', $this->input->post('harga')),	
			'unit'		    => str_replace(',', '', $this->input->post('unit')),	
			'jn'		    => 1			
		);		
		$this->db->insert('gtc', $data_gtc);
		
		$data_transaksi = array(			
			'username'		=> 'dxplor',
			'price' 	    => str_replace(',', '', $this->input->post('harga')),	
			'unit'		    => str_replace(',', '', $this->input->post('unit')),	
			'jn'		    => 1,
			'type_tr'	    => 'J'
		);		
		$this->db->insert('gtc_transaksi', $data_transaksi);
		
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			// error insert
			return FALSE;
		} else {
			$this->db->trans_complete();
			return TRUE;
		}		
	}	
	
	public function get_gtc($jns=null)
	{
		// $tgl_dari = isset($_REQUEST['froms']) ? $_REQUEST['froms'] : '';
		// $tgl_sampai = isset($_REQUEST['to']) ? $_REQUEST['to'] : '';
		
		// if(!empty($tgl_dari) && !empty($tgl_sampai)){
			// $tgl_dari = explode(' ',$tgl_dari);
			// $tgl_daris = explode('-',$tgl_dari[0]);
			// $tgl_sampai = explode(' ',$tgl_sampai);
			// $tgl_sampais = explode('-',$tgl_sampai[0]);
			// $froms = $tgl_daris[2].'-'.$tgl_daris[1].'-'.$tgl_daris[0].' '.$tgl_dari[1];
			// $to = $tgl_sampais[2].'-'.$tgl_sampais[1].'-'.$tgl_sampais[0].' '.$tgl_sampai[1];
			// $this->db->where("GRAB_ON >=", $froms);
			// $this->db->where("GRAB_ON <=", $to);
		// }
		$res = array();
		$this->db->where('jenis', $jns);				
		$t = $this->db->get('master_point')->result_array();		
		if(!empty($t)){
			foreach($t as $gtc){
				$res[$gtc['username']] = $gtc['nilai'];
			}
		}
		return $res;
	}
	
	function get_gtcTransaksi($username=null){
		$this->db->where('username', $username);
		$this->db->order_by('id', 'DESC');
		return $this->db->get('gtc_transaksi')->result_array();
	}
	
	function get_recentTransaksi(){		
		$this->db->order_by('last_date', 'DESC');
		return $this->db->get('history_gtc')->result_array();
	}
	
	function getMy_gtc($username=null){
		$result  = 0;
		$this->db->select_sum('unit');
		$this->db->where('username', $username);
		$res = $this->db->get('gtc')->row();
		if(!empty($res) || $res > 0){
			$result = $res->unit;
		}
		return $result;
	}
	
	function get_gtc_jual(){
		$res = null;
		$this->db->where('jn',1);
		$this->db->where('type_tr','J');
		$t = $this->db->get('gtc_transaksi')->result_array();
		if(!empty($t)){
			$res = $t;
		}else{
			$this->db->where('jn !=',1);
			$this->db->where('type_tr','J');
			$res = $this->db->get('gtc_transaksi')->result_array();			
		}
			return $res;
	}	
	
	function get_price(){
		$this->db->where('price >', 0);
		$this->db->where('price !=', '');
		$this->db->where('type_tr', 'J');
		$this->db->group_by('price');
		$this->db->order_by('price', 'ASC');
		return $this->db->get('gtc_transaksi')->result_array();
	}
	
	function get_sum_gtc(){
		$result = array();
		$sql = "select sum(unit) as ammout_unit, price, jn from gtc_transaksi where type_tr='J' group by price, jn";
		$res = $this->db->query($sql)->result_array();
		if(!empty($res)){
			foreach($res as $r){
				$result[$r['price']][$r['jn']][] = $r['ammout_unit'];				
			}
		}		
		return $result;
	}
	
	function get_sumprice(){
		$price = $_POST['price'];
		$result = 0;
		$this->db->select_sum('unit');
		$this->db->where('price', $price);
		$this->db->where('type_tr', 'J');
		$res = $this->db->get('gtc_transaksi')->row();
		if(!empty($res)){
			$result = $res->unit;
		}		
		return $result;
	}
	
	function sell_gtc(){
		$price = isset($_POST['price']) ? $_POST['price'] : 0;		
		$unit = isset($_POST['unit']) ? $_POST['unit'] : 0;
		$sisa = isset($_POST['sisa']) ? $_POST['sisa'] : 0;
		$username = isset($_POST['username']) ? $_POST['username'] : '';
		$payble = isset($_POST['payble']) ? $_POST['payble'] : '';
		//error_log(serialize($_POST));		
				
		$this->db->trans_start();
		$data_gtc = array(
			'unit' 			=> '-'.$unit,
			'price_beli' 	=> 0,
			'username'		=> $username,
			'jn'			=> 2			
		);
		
		$this->db->insert('gtc', $data_gtc);
		
		$data_gtctransaksi = array(
			'unit'	=> $unit,
			'price'	=> $price,
			'type_tr'	=> 'J',
			'jn'		=> 2,
			'username'	=> $username
		);		
		$this->db->insert('gtc_transaksi', $data_gtctransaksi);
		
		$data_history = array(
			'username' 	=> $username,
			'price'	   	=> $price,
			'unit' 		=> $unit,
			'jn'		=> 2,
			'ket'		=> 'Jual'
		);
		$this->db->insert('history_gtc', $data_history);
				
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			// error insert
			return FALSE;
		} else {
			$this->db->trans_complete();
			return TRUE;
		}
	}
	
	function get_sell_id($price=null){
		$this->db->where('price', $price);
		$this->db->where('jn', 2);
		$this->db->where('type_tr', 'J');
		$this->db->where('unit >', 0);
		$this->db->order_by('id', 'ASC');
		return $this->db->get('gtc_transaksi', 1)->row();
	}
	
	function get_sell_comp_id($price=null){
		$this->db->where('price', $price);
		$this->db->where('jn', 1);
		$this->db->where('type_tr', 'J');
		$this->db->where('unit >', 0);
		$this->db->order_by('id', 'ASC');
		return $this->db->get('gtc_transaksi', 1)->row();
	}
	
	function get_lastpoint($username=null, $jns=null){
		$this->db->select('nilai');
		$this->db->where('username', $username);
		$this->db->where('jenis', $jns);
		$nilai = $this->db->get('master_point')->row();		
		if(!empty($nilai)){
			return $nilai->nilai;
		}else{
			return null;
		}
		
	}	
	
	function get_gtc_jualo($price=0){
		$res = array();		
		$this->db->where('jn',1);		
		$this->db->where('type_tr','J');
		$this->db->where('price',$price);
		$this->db->where('unit >',0);
		$this->db->order_by('id','ASC');
		$t = $this->db->get('gtc_transaksi')->result_array();
		foreach($t as $_t){
			$res[] = $_t;
		}
		$this->db->where('jn !=',1);
		$this->db->where('type_tr','J');
		$this->db->where('price',$price);
		$this->db->where('unit >',0);
		$this->db->order_by('id','ASC');
		$_res = $this->db->get('gtc_transaksi')->result_array();
		if(!empty($_res)){
			foreach($_res as $r){
				array_push($res,$r);
			}
		}		
		return $res;
	}
	
	
	function buy_gtc(){
		$price = isset($_POST['price']) ? $_POST['price'] : 0;		
		$unit = isset($_POST['unit']) ? $_POST['unit'] : 0;
		$_sisa = isset($_POST['sisa']) ? $_POST['sisa'] : 0;
		$username = isset($_POST['username']) ? $_POST['username'] : '';
		$payble = isset($_POST['payble']) ? $_POST['payble'] : '';
				
		$t = $this->get_gtc_jualo($price);
		$beli = -$unit;
		$sisa = 0;
		$penjual = array();		
		$cnt = count($t);		
		
		for($i=0;$i<$cnt;$i++){
			$sisa = $t[$i]['unit'] + $beli;
			
			$t[$i] +=array('sisa'=> $sisa);			
			if($sisa >= 0){	
				$penjual[] = $t[$i];
				break;
			}else{
				$penjual[] = $t[$i];
			}
			$beli = $sisa;
		}		
		
		$sisa_unit = 0;
		$beli_unit = 0;
		$uang = 0;
		$cash = 0;
		$shop = 0;
		$gtc = 0;
		$dapat_uang = array();
		$uang_jual = array();	

		// proses transaksi jual-beli GTC unit
		$this->db->trans_start();
		foreach($penjual as $p){
			$sisa_unit = 0;
			$beli_unit = 0;
			if($p['sisa'] <= 0){
				$sisa_unit = 0;
				$beli_unit = $p['unit'];
			}else{
				$sisa_unit = $p['sisa'];
				$beli_unit = $p['unit'] - $p['sisa'];
			}	
			
			$this->db->where('id', $p['id']);
			$this->db->update('gtc_transaksi', array('unit'=> $sisa_unit));
			
			if($p['username'] != 'dxplor'){				
				$uang = $beli_unit * $price;			
				$cash = $uang * (50/100);
				$shop = $uang * (10/100);
				$gtc = $uang * (30/100);				
				$uang_jual = array(
					'gtc'	=> $gtc,
					'shop'	=> $shop,
					'cash'	=> $cash
				);
				foreach($uang_jual as $key => $val){
					$last = $this->get_lastpoint($p['username'], $key);
					$dapat_uang = array(							
						'nilai'	=> $val + $last							
					);
					$this->db->where('username', $p['username']);
					$this->db->where('jenis', $key);
					$this->db->update('master_point', $dapat_uang);
						
					$gtc_point = array(
						'username' 		=> $p['username'],
						'Description'	=> 'Penjualan GTC Units',
						'jenis'			=> $key,
						'nilai'			=> $val,
						'Balance'		=> $val + $last
					);
					$this->db->insert('point', $gtc_point);
				}
			}
			// end proses transaksi jual-beli GTC unit
			
			//debet gtc point pembeli
			$this->db->where('username', $username);
			$this->db->where('jenis', 'gtc');
			$this->db->update('master_point', array('nilai' => $_sisa));
			
			$my_gtc = array(
				'username' 		=> $username,
				'Description'	=> 'Pembelian GTC Units',
				'jenis'			=> 'gtc',
				'nilai'			=> '-'.$payble,
				'Balance'		=> $_sisa
			);
			$this->db->insert('point', $my_gtc);
			//end debet gtc point pembeli
			
			$data_gtc = array(
				'username' 		=> $username,
				'price_beli'	=> $price,
				'unit' 			=> $unit,
				'jn'			=> 2
			);
			$this->db->insert('gtc', $data_gtc);
			
			$data_history = array(
				'username' 	=> $username,
				'price'	   	=> $price,
				'unit' 		=> $unit,
				'jn'		=> 2,
				'ket'		=> 'Beli GTC :'.$unit.' Units'
			);
			$this->db->insert('history_gtc', $data_history);
			
		}	
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			// error insert
			return FALSE;
		} else {
			$this->db->trans_complete();
			return TRUE;
		}
	}		
}

/* End of file pendapatan.php */
/* Location: ./application/models/pendapatan.php */