<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Prop_m extends CI_Model {

	public function __construct() {
		parent::__construct();
	}	
	
	function get_list_kab($state){  //funtion menampilkan kota berdasarkan provinsi
      //  error_log($state);
		//$state = "BENGKULU";
		$this->db->select('*');
		//$this->db->like('propinsi',$state);
        $this->db->where('propinsi',$state);		
		$this->db->from('tbl_user');		
		$this->db->order_by('id', 'ASC');
		$query = $this->db->get();
		if($query->num_rows()>0){
			$out = $query->result();
			return $out;
		} else {
			return FALSE;
		}
    }

	function get_list_prop() {
		$this->db->distinct();
		$this->db->select('propinsi');			
		$this->db->from('tbl_user');		
		$this->db->order_by('id', 'ASC');
		$query = $this->db->get();
		if($query->num_rows()>0){
			$out = $query->result();
			return $out;
		} else {
			return FALSE;
		}
	}
	function get_list_kab2() {
		$level = $this->session->userdata('level');
			
		$this->db->distinct();
		$this->db->select('kabupaten');			
		$this->db->from('tbl_user');	
		if ($level== "ADMINPROV")
		{
		$this->db->where('propinsi', $this->session->userdata('propinsi'));	
		}
		else{
		$this->db->where('kabupaten', $this->session->userdata('kabupaten'));			
		}
        		
		$this->db->order_by('id', 'ASC');
		$query = $this->db->get();
		if($query->num_rows()>0){
			$out = $query->result();
			return $out;
		} else {
			return FALSE;
		}
	}
	function get_area($id) {
		//error_log($id);
		$this->db->select('*');
		$this->db->from('area');
		$this->db->where('id',$id);
		$query = $this->db->get();	
        		
		if($query->num_rows()>0){
			$out = $query->result();
			return $out;
		} else {
			return FALSE;
		}	
       // error_log($this->db->last_query());	
	}
	public function delete($id){
		return $this->db->delete('area', array('id' => $id)); 
	}
}