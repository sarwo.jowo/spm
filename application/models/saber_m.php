<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Saber_m extends CI_Model{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}	
	public function create() {
		//error_log
		$tgl_lapor = date('Y-m-d', strtotime($this->input->post('tglsurat')));
		$data = array(			
			'tgl_masuk'				=>	$this->input->post('tgl_masuk'),
			'tgl_lapor'			    =>	$tgl_lapor,			
			'kegiatan'		        =>  $this->input->post('kegiatan'),
			'skpd'		            =>  $this->input->post('skpd'),
			'pelaku'		        =>  $this->input->post('pelaku'),
			'modus'		            =>  $this->input->post('modus'),
			'nilai'		            =>  $this->input->post('nilai'),
			'areaid'		        =>  $this->input->post('area'),
			'kabupaten'		        =>  $this->session->userdata('kabupaten'),
			'propinsi'		        =>  $this->session->userdata('propinsi'),
			'st'		            =>  '1',
			'uname'		            =>  $this->session->userdata('u_name')
			);
		return $this->db->insert('tbl_pungli', $data);		 	
 	}	
	function get_files($id)
	{
	 // error_log($id);
	  $this->db->where('idsb',$id);
	  $this->db->select("*");
	  $this->db->from('file');
	  $query = $this->db->get();
	  return $query->result();
	}	
	
	public function import_db($data) {
		if(is_array($data)) {

			$pair_arr = array();
			foreach ($data as $rows) {
				//if(trim($rows['A']) == '') { continue; }
				// per baris
				
				$pair = array();
				foreach ($rows as $key => $val) {
					$pass="123456";
					if($key == 'A') { $pair['u_name'] = $val; }
					if($key == 'A') { $pair['deptid'] = $val; }
					if($key == 'B') { $pair['propinsi'] = $val; }
					if($key == 'C') { $pair['kabupaten'] = $val; }
					if($key == 'D') { $pair['level'] = $val; }
					if($key == 'E') { $pair['Aktif'] = $val; }
					if($key == 'F') $pair['pass_word'] = sha1('nsi'. $val);					
				}
				//$pair['jabatan_id'] = 2;
				$pair_arr[] = $pair;
			}
			//var_dump($pair_arr);
			//return 1;
			return $this->db->insert_batch('tbl_user', $pair_arr);
		} else {
			return FALSE;
		}
	}
	function deletefile()
	{  
	$id = $_POST["id"];			
	$this->db->where('id', $id);    
	return $this->db->delete('file');
	}	
	function get_data_transaksi_ajax($offset, $limit, $q='', $sort, $order) {
		$user = $this->session->userdata('u_name');
		// error_log($q['pr']);
		// error_log($q['kab']);
		// error_log($q['area']);
		$sql = "SELECT * FROM tbl_pungli where st = 1";
		if(is_array($q)) {
			if($q['area'] != '' && $q['pr'] == '' && $q['kab'] == '') {						
					$sql .=" AND areaid = '".$q['area']."'";
					//error_log('1--');
			}
			elseif($q['area'] == '' && $q['kab'] == '-' && $q['pr'] != '')
			{
				$sql .=" AND propinsi = '".$q['pr']."'";
				//error_log('2--');
			}
			elseif($q['area'] == '' && $q['kab'] != '' && $q['pr'] == '')
			{
				$sql .=" AND kabupaten = '".$q['kab']."'";
				//error_log('3--');
			}
			elseif($q['area'] == '' && $q['kab'] != '' && $q['pr'] != '')
			{
				$sql .=" AND kabupaten = '".$q['kab']."' AND propinsi = '".$q['pr']."'";
				//error_log('6+');
			}
			elseif($q['area'] != '' && $q['pr'] != '' && $q['kab'] == ''){
				
				$sql .=" AND areaid = '".$q['area']."' AND propinsi = '".$q['pr']."'";
				//error_log('4--');
			}	
			elseif($q['area'] != '' && $q['pr'] != '' && $q['kab'] != ''){
				//error_log('5--');
				$sql .=" AND areaid = '".$q['area']."' AND propinsi = '".$q['pr']."' AND kabupaten = '".$q['pr']."'";
			}	
			
			else
			{
				//error_log('kosong');
			}	 
		}	
		
		$result['count'] = $this->db->query($sql)->num_rows();
		$sql .=" ORDER BY {$sort} {$order} ";
		$sql .=" LIMIT {$offset},{$limit} ";
		$result['data'] = $this->db->query($sql)->result();
		return $result;
	}
	function get_data_transaksi_ajax_member($offset, $limit, $q='', $sort, $order) {
		
		$user = $this->session->userdata('u_name');
		
		$sql = "SELECT * FROM tbl_pungli where uname='".$user."'";
		if(is_array($q)) {
			if($q['area'] != '' && $q['kab']== '') {			
                   $area = $q['area'];	
					//$area = '2';					
					$sql .=" AND areaid = '".$area."'";
					$sql .=" AND propinsi = '".$this->session->userdata('propinsi')."'";
				}
				else if($q['area'] == '' && $q['kab']!= '')
				{
					$sql .=" AND kabupaten = '".$q['kab']."'";	
                    $sql .=" AND propinsi = '".$this->session->userdata('propinsi')."'";					
				}
				else if($q['area'] != '' && $q['kab']!= '')
				{
					$sql .=" AND kabupaten = '".$q['kab']."' AND areaid = '".$q['area']."'";
                    $sql .=" AND propinsi = '".$this->session->userdata('propinsi')."'";					
				}
			else{
				   // $prop = $q['propinsi'];	
					//$area = '2';					
					$sql .=" AND propinsi = '".$this->session->userdata('propinsi')."'";
			}				
							
		}	
		$result['count'] = $this->db->query($sql)->num_rows();
		$sql .=" ORDER BY {$sort} {$order} ";
		$sql .=" LIMIT {$offset},{$limit} ";
		$result['data'] = $this->db->query($sql)->result();
		
		//error_log($this->db->last_query());
		return $result;
	}
	
	function get_data_cetak() {
		
		$area = isset($_REQUEST['area']) ? $_REQUEST['area'] : '';
		$pr =  isset($_REQUEST['pr']) ? $_REQUEST['pr'] : '';
		$kab =  isset($_REQUEST['kab']) ? $_REQUEST['kab'] : '';
		$sql = '';
		$sql = "SELECT * FROM tbl_pungli where st = 1";
		
		
		
	       if($area != '' && $pr == '' && $kab == '') {						
					$sql .=" AND areaid = '".$area."'";
					//error_log('1--');
			}
			elseif($area == '' && $kab == '-' && $pr != '')
			{
				$sql .=" AND propinsi = '".$pr."'";
				//error_log('2--');
			}
			elseif($area == '' && $kab != '' && $pr == '')
			{
				$sql .=" AND kabupaten = '".$kab."'";
				//error_log('3--');
			}
			elseif($area == '' && $kab != '' && $pr != '')
			{
				$sql .=" AND kabupaten = '".$kab."' AND propinsi = '".$pr."'";
				//error_log('6+');
			}
			elseif($area != '' && $pr != '' && $kab == '-'){
				
				$sql .=" AND areaid = '".$area."' AND propinsi = '".$pr."'";
				//error_log('4--');
			}	
			elseif($area != '' && $pr != '' && $kab != ''){
				//error_log('5--');
				$sql .=" AND areaid = '".$area."' AND propinsi = '".$pr."' AND kabupaten = '".$pr."'";
			}	
			
			else
			{
				//error_log('kosong');
			}	 	
		
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$out = $query->result_array();
			return $out;
		} else {
			return FALSE;
		}
	}
	function get_sum() {
		$cnts = '00';

		$area = isset($_REQUEST['area']) ? $_REQUEST['area'] : '';
		$pr =  isset($_REQUEST['pr']) ? $_REQUEST['pr'] : '';
		$kab =  isset($_REQUEST['kab']) ? $_REQUEST['kab'] : '';
		$sql = '';
		$sql = "SELECT sum(nilai) as nilai FROM tbl_pungli where st = 1";
		
	         if($area != '' && $pr == '' && $kab == '') {						
					$sql .=" AND areaid = '".$area."'";
					//error_log('1--');
			}
			elseif($area == '' && $kab == '-' && $pr != '')
			{
				$sql .=" AND propinsi = '".$pr."'";
				//error_log('2--');
			}
			elseif($area == '' && $kab != '' && $pr == '')
			{
				$sql .=" AND kabupaten = '".$kab."'";
				//error_log('3--');
			}
			elseif($area == '' && $kab != '' && $pr != '')
			{
				$sql .=" AND kabupaten = '".$kab."' AND propinsi = '".$pr."'";
				//error_log('6+');
			}
			elseif($area != '' && $pr != '' && $kab == '-'){
				
				$sql .=" AND areaid = '".$area."' AND propinsi = '".$pr."'";
				//error_log('4--');
			}	
			elseif($area != '' && $pr != '' && $kab != ''){
				//error_log('5--');
				$sql .=" AND areaid = '".$area."' AND propinsi = '".$pr."' AND kabupaten = '".$pr."'";
			}	
			
			else
			{
				//error_log('kosong');
			}	 			
		$query = $this->db->get('tbl_pungli')->row();	
		$cnt = $query->nilai;
		if($cnt > 0){
			$cnts = $cnt;
		}
		return $cnts;
	}
	
	function lap_total()
	{
	   $cnts = '00';
	    $area = isset($_REQUEST['area']) ? $_REQUEST['area'] : '';
		$pr =  isset($_REQUEST['pr']) ? $_REQUEST['pr'] : '';
		$kab =  isset($_REQUEST['kab']) ? $_REQUEST['kab'] : '';
		
		$sql = '';
		$sql = "SELECT sum(nilai) as nilai FROM tbl_pungli where st = 1";
		
		 if($area  != '' && $pr == '' && $kab == '') {						
					$sql .=" AND areaid LIKE '%".$area."%'";
			}
			elseif($area  == '' && $kab == '' &&  $pr != '')
			{
				$sql .=" AND propinsi LIKE '%".$pr."%'";
			}
			elseif($area  == '' && $kab != '' && $pr == '')
			{
				$sql .=" AND kabupaten LIKE '%".$kab."%'";
			}
			elseif($area  != '' && $pr != '' && $kab == ''){
				
				$sql .=" AND areaid LIKE '%".$area."%' AND propinsi LIKE '%".$pr."%'";
			}	
			elseif($area  == '' && $pr != '' && $kab == ''){
				
				$sql .=" AND areaid LIKE '%".$area."%' AND propinsi LIKE '%".$pr."%' AND propinsi LIKE '%".$pr."%'";
			}	
			
			else
			{
				
			}
		$query = $this->db->query($sql);
		//$cnt = $row['nilai'];
		
		if($query){
			$rs = $query->result_array();
			foreach($rs as $data){
				$result['nilai'] = $data['nilai'];
			}
		}
		// if($cnt > 0){
			// $cnts = $cnt;
		// }
		return $result;
		
	}
	function get_jml() {
		 // $cnts = '00';
	      $area = isset($_REQUEST['area']) ? $_REQUEST['area'] : '';
		  $pr =  isset($_REQUEST['pr']) ? $_REQUEST['pr'] : '';
		  $kab =  isset($_REQUEST['kab']) ? $_REQUEST['kab'] : '';
		   $this->db->select('SUM(nilai) AS nilai');
		   $this->db->from('tbl_pungli');	
		    if($area != '' && $pr == '' && $kab == '') {						
		 			
					$this->db->where('areaid', $area);
			}
			elseif($area == '' && $kab == '-' && $pr != '')
			{
				
				$this->db->where('propinsi', $pr);
				//error_log('2--');
			}
			elseif($area == '' && $kab != '' && $pr == '')
			{
				//$sql .=" AND kabupaten = '".$kab."'";
				$this->db->where('kabupaten', $kab);
				//error_log('3--');
			}
			elseif($area == '' && $kab != '' && $pr != '')
			{
				//$sql .=" AND kabupaten = '".$kab."' AND propinsi = '".$pr."'";
				$this->db->where('kabupaten', $kab);
				$this->db->where('propinsi', $pr);
				error_log('6+');
			}
			elseif($area != '' && $pr != '' && $kab == '-'){
				
				
				$sql .=" AND areaid = '".$area."' AND propinsi = '".$pr."'";
				$this->db->where('areaid', $area);
				$this->db->where('propinsi', $pr);
				//error_log('4--');
			}	
			elseif($area != '' && $pr != '' && $kab != ''){
				//error_log('5--');
				//$sql .=" AND areaid = '".$area."' AND propinsi = '".$pr."' AND kabupaten = '".$pr."'";
				$this->db->where('areaid', $area);
				$this->db->where('propinsi', $pr);
				$this->db->where('kabupaten', $kab);
			}	
			
			else
			{
				//error_log('kosong');
			}				
        		
		$query = $this->db->get();
		return $query->row();
	}
	function get_data_cetak_m() {
		
		$area = isset($_REQUEST['area']) ? $_REQUEST['area'] : '';
		$kab = isset($_REQUEST['kab']) ? $_REQUEST['kab'] : '';
		$sql = '';
		$sql = "SELECT * FROM tbl_pungli where st = 1";		
	      
			   if($area != '' && $kab == '') {			
                   
					//$area = '2';					
					$sql .=" AND areaid = '".$area."'";
					$sql .=" AND propinsi = '".$this->session->userdata('propinsi')."'";
				}
				else if($area == '' && $kab != '')
				{
					$sql .=" AND kabupaten = '".$kab."'";	
                    $sql .=" AND propinsi = '".$this->session->userdata('propinsi')."'";					
				}
				else if($area != '' && $kab != '')
				{
					$sql .=" AND kabupaten = '".$kab."' AND areaid = '".$area."'";
                    $sql .=" AND propinsi = '".$this->session->userdata('propinsi')."'";					
				}
			else{
				   // $prop = $q['propinsi'];	
					//$area = '2';					
					$sql .=" AND propinsi = '".$this->session->userdata('propinsi')."'";
			}				
							
		 	
		
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$out = $query->result_array();
			return $out;
		} else {
			return FALSE;
		}
	}
	function get_jml_m() {
		 // $cnts = '00';
	       $area = isset($_REQUEST['area']) ? $_REQUEST['area'] : '';
		   $kab = isset($_REQUEST['kab']) ? $_REQUEST['kab'] : '';
		   $this->db->select('SUM(nilai) AS nilai');
		   $this->db->from('tbl_pungli');	
		  
			if($area != '' && $kab == '') {			
                  // $area = $q['area'];	
					//$area = '2';					
					//$sql .=" AND areaid = '".$area."'";
					$this->db->where('areaid', $area);
					$this->db->where('propinsi', $this->session->userdata('propinsi'));
				}
				else if($area == '' && $kab != '')
				{
					$this->db->where('kabupaten', $kab);
					$this->db->where('propinsi', $this->session->userdata('propinsi'));
					//$sql .=" AND kabupaten = '".$q['kab']."'";					
				}
				else if($area != '' && $kab != '')
				{
					$this->db->where('kabupaten', $kab);
					$this->db->where('areaid', $area);
					$this->db->where('propinsi', $this->session->userdata('propinsi'));
									
				}
			else{
				 $this->db->where('propinsi', $this->session->userdata('propinsi'));					
					
			}			
        		
		$query = $this->db->get();
		return $query->row();
	}
	function get_id_pungli()
	{
		$this->db->select('*');
		$this->db->from('tbl_pungli');
		$this->db->where('uname', $this->session->userdata('u_name'));
		$this->db->order_by('id', 'desc');
		$this->db->limit(1);		
		$query = $this->db->get();
		if($query->num_rows()>0){
			$out = $query->row();
			return $out;
		} else {
			return FALSE;
		}
	}
	function get_id_pungli2($id)
	{
		$this->db->select('*');
		$this->db->from('tbl_pungli');
		$this->db->where('uname', $this->session->userdata('u_name'));
		$this->db->where('id', $id);				
		$query = $this->db->get();
		if($query->num_rows()>0){
			$out = $query->row();
			return $out;
		} else {
			return FALSE;
		}
	}
	function get_file_name($id)
	{
		$this->db->select('*');
		$this->db->from('file');
		$this->db->where('id',$id);
		$query = $this->db->get();
		if($query->num_rows()>0){
			$out = $query->row();
			return $out;
		} else {
			return FALSE;
		}
	}
	
	function get_laporan_tahun($kas) {
		$this->db->select('*');
		$this->db->from('v_transaksi');
		
		if(isset($_REQUEST['periode'])) {
			$tgl_arr = explode('-', $_REQUEST['periode']);
			$thn = $tgl_arr[0];
			$bln = $tgl_arr[1];
		} else {
			$thn = date('Y');
			$bln = date('m');
		}
		$where = "(YEAR(tgl) = '".$thn."' AND  MONTH(tgl) = '".$bln."') AND (dari_kas = '".$kas_id."' OR  untuk_kas = '".$kas_id."')";
		$this->db->where($where);
		$this->db->order_by('tgl', 'ASC');
		$query = $this->db->get();

		if($query->num_rows()>0) {
			$out = $query->result();
			return $out;
		} else {
			return array();
		}
	}
	
	function get_jml_temp($jenis, $id) {
		$this->db->select('SUM(jumlah) AS jml_total');
		$this->db->from('tbl_trans_sp');
		$this->db->where('anggota_id',$id);
		$this->db->where('dk','D');
		$this->db->where('jenis_id', $jenis);
		
		$query = $this->db->get();
		return $query->row();
	}

	
}