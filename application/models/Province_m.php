<?php

class Province_m extends CI_Model
{
    function  __construct()
    {
        parent:: __construct();
    }    
		
	function get_province(){		
		$this->db->where('is_delete', '0');
		$this->db->order_by('province_name', "ASC");
        $result = $this->db->get('provinces')->result_array();		
        return $result;
	}	
	
	function select_pro(){
		$res = array();
		$this->db->where('is_delete', '0');
		$this->db->where('status', '1');
		$this->db->order_by('province_name', "ASC");
        $province = $this->db->get('provinces')->result_array();	
		if(!empty($province)){
			foreach($province as $p){
				$res[$p['province_id']] = $p['province_name'];
			}
		}
		return $res;
	}
	
	function select_city($province_id =0){
		$res = array();
		$this->db->where('is_delete', '0');
		$this->db->where('status', '1');
		$this->db->where('province_id', $province_id);
		$this->db->order_by('name', "ASC");
        $city = $this->db->get('cities')->result_array();	
		if(!empty($city)){
			foreach($city as $c){
				$res[$c['city_id']] = $c['name'];
			}
		}
		return $res;
	}
	
	function save_pro(){		
		$save =0;
		$id_pro = isset($_POST['id_province']) ? $_POST['id_province'] : '';		
		$name = $_POST['province_name'];
		$status = $_POST['status'];
				
		$data = array(			
			'province_name'		=> $name,			
			'status'			=> $status			
		);
			
		
		if(!empty($id_pro)){
			$this->db->where('province_id', $id_pro);
			$save = $this->db->update('provinces', $data);
		}else{
			$data += array('create_date' => date('Y-m-d'), 'create_user' => 1);
			$save = $this->db->insert('provinces', $data);
		}
		return $save;
	}
	
	function del_pro(){
		$id = $_POST['id'];
		$this->db->where('province_id', $id);
		return $this->db->update('provinces', array('is_delete' => 1, 'modified_user'	=> 2));
	}
	
	function save_city(){		
		$save =0;
		$id_city = isset($_POST['id_city']) ? $_POST['id_city'] : '';		
		$city_name = $_POST['city_name'];
		$province_id = $_POST['province_name'];
		$status = $_POST['status'];
				
		$data = array(		
			'name'			=> $city_name,
			'province_id'	=> $province_id,			
			'status'		=> $status			
		);
			
		
		if(!empty($id_city)){
			$data += array('modified_user' => 2);
			$this->db->where('city_id', $id_city);
			$save = $this->db->update('cities', $data);
		}else{
			$data += array('create_date' => date('Y-m-d'), 'create_user' => 1);
			$save = $this->db->insert('cities', $data);
		}
		return $save;
	}
	
	function del_city(){
		$id = $_POST['id'];
		$this->db->where('province_id', $id);
		return $this->db->update('provinces', array('is_delete' => 1, 'modified_user'	=> 2));
	}
	
	function get_city(){	
		$this->db->select('cities.*, provinces.province_name');
		$this->db->join('provinces', 'provinces.province_id = cities.province_id', 'left');
		$this->db->where('cities.is_delete', '0');
		$this->db->where('provinces.is_delete', '0');
		$this->db->order_by('name', "ASC");
        $result = $this->db->get('cities')->result_array();		
        return $result;
	}	
	
}