<?php

class Admin_m extends CI_Model
{
    function  __construct()
    {
        parent:: __construct();
    }    
		
	function get_admin(){		
		$this->db->where('is_delete', '0');
		$this->db->or_where('is_delete', '');
		$this->db->order_by('username', "ASC");
        $result = $this->db->get('admin')->result_array();	
;		
        return $result;
	}	
	
	
	function save(){		
		$save =0;
		$id_user = isset($_POST['id_user']) ? $_POST['id_user'] : '';		
		$fullname = $_POST['fullname'];				
		$email = $_POST['email'];
		$status = $_POST['status'];
		$level = $_POST['level'];
		$username = $this->converter->encode($_POST['username']);
		$password = isset($_POST['password']) ? $this->converter->encode($_POST['password']) : '';			
		$data = array(			
			'username'		=> $username,
			'fullname'		=> $fullname,
			'email'			=> $email,
			'status'		=> $status,
			'level'			=> $level			
		);
		if(!empty($password)){
			$data += array('password' => $password);
		}		
		
		if(!empty($id_user)){
			$data += array('modified_by'	=> $this->session->userdata('operator_id'));
			$this->db->where('operator_id', $id_user);
			$save = $this->db->update('admin', $data);
		}else{
			$data += array('create_date' => date('Y-m-d'), 'create_user' => $this->session->userdata('operator_id'));
			$save = $this->db->insert('admin', $data);
		}
		
		return $save;
	}
	
	function del(){
		$id = $_POST['id'];
		$this->db->where('operator_id', $id);
		return $this->db->update('admin', array('is_delete' => 1, 'modified_by'	=> $this->session->userdata('operator_id')));
	}
	
	function del_level(){
		$id = $_POST['id'];
		$this->db->where('id', $id);
		return $this->db->update('level', array('is_delete' => 1, 'modified_by'	=> $this->session->userdata('operator_id')));
	}
	
	function get_level(){
		$level = array();
		$this->db->where('is_delete', '0');		
		$this->db->order_by('level', "ASC");
        $level = $this->db->get('level')->result_array();			
		return $level;
	}
		
	function select_level(){
		$res = array();
		$this->db->where('is_delete', '0');		
		$this->db->order_by('level', "ASC");
        $level = $this->db->get('level')->result_array();	
		if(!empty($level)){
			foreach($level as $l){
				$res[$l['id']] = $l['level'];
			}
		}
		return $res;
	}
}