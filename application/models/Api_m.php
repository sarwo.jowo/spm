<?php

class Api_m extends CI_Model
{
    function  __construct()
    {
        parent:: __construct();
    }    
		
	function get_landlord(){
		$this->db->select('landlord_info.*, users.username, cities.name, provinces.province_name');
		$this->db->join('cities', 'cities.city_id = landlord_info.city', 'left');
		$this->db->join('provinces', 'provinces.province_id = landlord_info.province', 'left');
		$this->db->join('users', 'users.user_id = landlord_info.user_id', 'left');		
		$this->db->where('landlord_info.is_delete', '0');
		$this->db->order_by('landlord_id', "DESC");
        $result = $this->db->get('landlord_info')->result_array();		
        return $result;
	}	
	
	function get_province(){		
		$this->db->where('is_delete', '0');
		$this->db->order_by('province_name', "ASC");
        $result = $this->db->get('provinces')->result_array();		
        return $result;
	}	
	
	function get_city(){	
		$this->db->select('cities.city_id, cities.name, provinces.province_id, provinces.province_name');
		$this->db->join('provinces', 'provinces.province_id = cities.province_id', 'left');
		$this->db->where('cities.is_delete', '0');
		$this->db->where('provinces.is_delete', '0');
		// $this->db->where('cities.city_id', '0');
		$this->db->order_by('cities.name', "ASC");
        $result = $this->db->get('cities')->result_array();		
        return $result;
	}	
	
	function get_admins(){
		$res = array();
		$r = $this->db->get('admin')->result_array();
		if(!empty($r)){
			foreach($r as $user){
				$res[$user['operator_id']] = $user['fullname'];
			}
		}
		return $res;
	}
	
	function get_member(){		
		$this->db->select('users.*, cities.name, provinces.province_name');
		$this->db->join('cities', 'cities.city_id = users.city', 'left');
		$this->db->join('provinces', 'provinces.province_id = users.province', 'left');		
		$this->db->where('users.is_delete', '0');
		$this->db->order_by('username', "ASC");
        $result = $this->db->get('users')->result_array();			
        return $result;
	}	
	
	function save_data($table=null, $data=array(), $where=null, $val=null){
		$id = $val;
		$save = 0;
		if(!empty($id)){
			$this->db->where($where, $id);
			$this->db->update($table, $data);
			$save = $id;
		}else{
			$this->db->insert($table, $data);
			$save = $this->db->insert_id();
		}
		$save = (int) $save;			
		return $save;
	}
	
	
	function get_photo($id){	
		$this->db->where('is_delete', '0');
		$this->db->where('ll_id', $id);				
        $result = $this->db->get('building_pic')->result_array();	
        return $result;
	}	
	
	function get_key_val() {
		$out = array();
		$this->db->select('id,setting_key,setting_val');
		$this->db->from('setting');
		$query = $this->db->get();
		if($query->num_rows()>0){
				$result = $query->result();
				foreach($result as $value){
					$out[$value->setting_key] = $value->setting_val;
				}
				return $out;
		} else {
			return FALSE;
		}
	}
	
	function get_value($tabel=null,$where=null,$val=null){
		$value = '';
		$this->db->select($where);
		$this->db->where($where, $val);
		$result	= $this->db->get($tabel)->result();
		if(!empty($result)){
			foreach($result as $r){
				$value	= $r->$where;
			}
		}			
		return $value;
	}
	
	function get_val($tabel=null, $where=null, $val=null){
		$this->db->where($where, $val);
		$result	= $this->db->get($tabel)->result();
		if(!empty($result)){
			foreach($result as $r){
				$value	= $r->$where;
			}
		}			
		return $value;
	}
	
	function login($email=null, $pass=null){
		$pass = $this->converter->encode($pass);
		$this->db->select('users.*, cities.name, provinces.province_name');
		$this->db->join('cities', 'cities.city_id = users.city', 'left');
		$this->db->join('provinces', 'provinces.province_id = users.province', 'left');		
		$this->db->where('users.is_delete', '0');
		$this->db->where('email', $email);
		$this->db->where('password', $pass);
		$res = $this->db->get('users',1)->row();		
		return $res;
	}
	
	function del_data($tabel=null, $where=null, $val=null){
		$this->db->where($where, $val);
		return $this->db->update($tabel, array('is_delete' => '1'));
	}
	
}