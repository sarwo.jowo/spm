<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Proses_m extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
		
	function get_data(){
		$result = array();
		$dt = $this->db->get('jaringan_day')->result_array();
		$sisa = '';
		$field = '';
		$bayar = '';
		if(!empty($dt)){
			foreach($dt as $t){
				if($t['pvkanan'] > $t['pvkiri']){
					$sisa = $t['pvkanan'] - $t['pvkiri'];
					$field = 'pvkanan';
					$bayar = $t['pvkanan'];
				}elseif($t['pvkiri'] > $t['pvkanan']){
					$sisa = $t['pvkiri'] - $t['pvkanan'];
					$field = 'pvkiri';
					$bayar = $t['pvkiri'];
				}elseif($t['pvkiri'] == $t['pvkanan']){
					$sisa = 0;
					$field = 0;
					$bayar = $t['pvkiri'];
				}else{
					$sisa = '';
					$field = '';
					$bayar = '';
				}
				$result[] = array(
					'username'	=> $t['username'],
					'sisa'		=> $sisa,
					'bayar'		=> $bayar,
					'field'		=> $field
				);
			}
		}
		return $result;
	}
	
	function proses(){
		$proses = $this->get_data();
		$this->db->empty_table('jaringan_day'); 
		$date = date("m-d-Y");
		$date1 = str_replace('-', '/', $date);
		
		$tomorrow = date('m-d-Y',strtotime($date1 . "-1 days"));
		$min_date = date('Y-m-d',strtotime($tomorrow));
		
		foreach($proses as $key=>$p){
			if($p['sisa'] > 0){
				$data = array(
					'username'	=> $p['username'],
					$p['field']	=> $p['sisa'],
					'tgl'		=> $min_date
				);
				$this->db->insert('jaringan_day', $data);
			}	
			$bayar = $p['bayar'];
			$cash = $bayar * (60/100);
			$rgtr = $bayar * (20/100);
			$gtc = $bayar * (20/100);
			$bonus_self = array(
				'cash'	=> $cash,
				'rgtr'	=> $rgtr,
				'gtc' 	=> $gtc
			);
			$last = $this->get_points();
			foreach($bonus_self as $key=>$val){
				$last_point = $last[$p['username']][$key];
				$t = array(
					'username'		=> $p['username'],
					'jenis' 		=> $key,
					'nilai' 		=> $val,
					'Description'	=> 'Bonus matching',
					'Balance'		=> $last_point + $val
				);
				$this->db->insert('point', $t);
				
				$master = array(					
					'jenis'		=> $key,
					'nilai'		=> $last_point + $val
				);
				$this->db->where('username', $p['username']);
				$this->db->update('master_point', $master);					
			}
			
			$data_payout = array(
				'username' 	=> $p['username'],
				'total'	   	=> $cash,
				'jenis'	   	=> 'kompairing',
				'date'		=> date('Y-m-d'),
				'dari'		=> $p['username']
			);
			$this->db->insert('komisi', $data_payout);			
			
			$paket = $this->dataupline($p['username']);
			$bonus_upl = $bayar * 0.02;
			$cash = $bonus_upl * (60/100);
			$rgtr = $bonus_upl * (20/100);
			$gtc = $bonus_upl * (20/100);
			$bonus_upline = array(
				'cash'	=> $cash,
				'rgtr'	=> $rgtr,
				'gtc' 	=> $gtc
			);
			
			for($i=0; $i<$paket; $i++){
				$uplines = 'upline'.$i;
				$_upline = $this->dataupline($uplines, $username);
				$_upline = isset($_upline[0]) ? $_upline[0] : 0;
				$data_payout = array(
					'username' 	=> $_upline,
					'total'	   	=> $cash,
					'jenis'	   	=> 'kommatchpairing',
					'date'		=> date('Y-m-d'),
					'dari'		=> $p['username']
				);
				$this->db->insert('komisi', $data_payout);
				$last_point = 0;
				foreach($bonus_upline as $key=>$val){
					$last_point = $last[$_upline][$key];
					$t = array(
						'username'		=> $_upline,
						'jenis' 		=> $key,
						'nilai' 		=> $val,
						'Description'	=> 'Bonus Sponsor',
						'Balance'		=> $last_point + $val
					);
					$this->db->insert('point', $t);
				
					$master = array(					
						'jenis'		=> $key,
						'nilai'		=> $last_point + $val
					);
					$this->db->where('username', $_upline);
					$this->db->update('master_point', $master);	
				
				}				
			}			
		}
	}
	
	function get_paket($username=null){
		$this->db->select('paket');
		$this->db->where('username', $username);
		$paket = $this->db->get('member')->row();
		
		$this->db->select('matchingupline');
		$this->db->where('id_package', $paket->paket);
		$leader = $this->db->get('package')->row();		
		return $leader->matchingupline;
	}
	
	function dataupline($field=null, $username=null) {
		$dup = array();
		$this->db->select($field);
		$this->db->where('username', $username);
		$dt = $this->db->get('upline')->result_array();
		if(!empty($dt)){
			foreach($dt as $t){
				$dup[] = $t[$field];
			}
		}	
		return $dup;
	}
	
	
	function get_points(){
		$res = array();
		$get = $this->db->get('master_point')->result_array();
		if(!empty($get)){
			foreach($get as $g){
				$res[$g['username']][$g['jenis']] = $g['nilai'];
			}
		}
		return $res;
	}
	
}