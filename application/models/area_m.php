<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Area_m extends CI_Model {

	public function __construct() {
		parent::__construct();
	}	
	function get_list_area() {
		$this->db->select('*');
		$this->db->from('area');		
		$this->db->order_by('id', 'ASC');
		$query = $this->db->get();
		if($query->num_rows()>0){
			$out = $query->result();
			return $out;
		} else {
			return FALSE;
		}
	}
	function get_list_wl() {
		$this->db->select('*');
		$this->db->from('wilayah');		
		//$this->db->order_by('id', 'ASC');
		$query = $this->db->get();
		if($query->num_rows()>0){
			$out = $query->result();
			return $out;
		} else {
			return FALSE;
		}
	}
	function get_area($id) {
		error_log($id);
		$this->db->select('*');
		$this->db->from('area');
		$this->db->where('id',$id);
		$query = $this->db->get();	
        		
		if($query->num_rows()>0){
			$out = $query->result();
			return $out;
		} else {
			return FALSE;
		}	
       // error_log($this->db->last_query());	
	}
	public function delete($id){
		return $this->db->delete('area', array('id' => $id)); 
	}
}