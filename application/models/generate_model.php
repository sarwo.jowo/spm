<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Generate_model extends CI_Model {

	public function __construct() {
		parent::__construct();
    }
        
    public function get_template_indikator($id, $wilayah)
	{		
        $this->db->where('dinas_id', $id);
        $this->db->where('wilayah', $wilayah);
		$this->db->order_by('id', "ASC");
        $result = $this->db->get('tbl_master_indikator')->result_array();		
        return $result;
    }

    public function get_anggaran_indikator($id_tr_dinas)
	{		
        $this->db->where('id_tr_dinas', $id_tr_dinas);        
		$this->db->order_by('indikator_id', "ASC");
        $result = $this->db->get('tbl_anggaran_indikator')->result_array();		
        return $result;
    }
	
	function check_exists1($dinas,$id_prov,$tahun)
	{
		$this->db->where('tahun',$tahun);
		//$this->db->where('dinas',$dinas);
		//$this->db->where('id_prov',$id_prov);
		$query = $this->db->get('tbl_dinas');
		$jml = $query->num_rows();
		error_log($jml);
		if ($query->num_rows() > 0){
			return 1;
		}
		else{
			return 0;
		}
	}
	
	function check_exists2($tahun,$dinas,$id_kabkot)
	{
		$this->db->where('tahun',$tahun);
		$this->db->where('dinas',$dinas);
		$this->db->where('id_kabkot',$id_kabkot);
		$query = $this->db->get('tbl_dinas');
		if ($query->num_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
    
    public function get_Dasar_Hukum($id)
	{		
        $this->db->where('dinas_id', $id);        
        return $this->db->get('tbl_master_dinas')->row();        

    }

    function insert_Indikator($data)
    {
        $this->db->insert('tbl_dinas_indikator', $data);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }
	
	function insert_Anggaran($data)
    {
        $this->db->insert('tbl_anggaran_indikator', $data);
        $insert_id = $this->db->insert_id();
		//$sql = $this->db->last_query();
		
		//error_log($sql);
        return  $insert_id;
    }

    function insertBulk_Anggaran($data)
    {
        return $this->db->insert_batch('tbl_anggaran_indikator', $data);
          
    }

    function insert_Indikator_Detail($data)
    {
        $this->db->insert('tbl_dinas_indikator_detail', $data);        
    }

    public function get_indikator($id_tr_dinas)
	{		
        // $this->db->where('id_tr_dinas', $id_tr_dinas);        
		// $this->db->order_by('indikator_id', "ASC");
        // $result = $this->db->get('tbl_dinas_indikator')->result_array();	
        
		//a.indikator_id,a.indikator_pencapaian,a.indikator_kinerja,a.id_tr_dinas, 
		$this->db->select("a.indikator_id,a.indikator_pencapaian,a.indikator_kinerja,a.id_tr_dinas, SUM(target) AS TTL");
		$this->db->from('tbl_dinas_indikator a');
		$this->db->join('tbl_dinas_indikator_detail b','b.indikator_id=a.indikator_id');
		$this->db->where("a.id_tr_dinas",$id_tr_dinas);
		$this->db->group_by('b.indikator_id');
		$query = $this->db->get();
		$sql = $this->db->last_query();
		
		//error_log($sql);
		
		
      
		
         return $query->result_array();
    }

   public function get_anggaran($id_tr_dinas)
	{		
   
        $this->db->select("*");
		$this->db->from('tbl_anggaran_indikator');
	//	$this->db->join('tbl_dinas_indikator_detail b','b.indikator_id=a.indikator_id');
		$this->db->where("id_tr_dinas",$id_tr_dinas);
		//$this->db->group_by('b.indikator_id');
		$query = $this->db->get();
		$sql = $this->db->last_query();
		
		//error_log($sql);
		return $query->result_array();
    }

    public function get_anggaranDistinct($id_tr_dinas)
	{		
   
        //$this->db->distinct();
        $this->db->select('DISTINCT(indikator_kinerja), indikator_pencapaian,id_indikator');
        //$this->db->select("indikator_kinerja");
		$this->db->from('tbl_anggaran_indikator');
	//	$this->db->join('tbl_dinas_indikator_detail b','b.indikator_id=a.indikator_id');
		$this->db->where("id_tr_dinas",$id_tr_dinas);
		//$this->db->group_by('b.indikator_id');
		$query = $this->db->get();
		$sql = $this->db->last_query();
		
		//error_log($sql);
		return $query->result_array();
    }

    public function get_indikator_detail($id_tr_dinas)
	{	        
        $this->db->select("id,tbl_dinas_indikator.indikator_id,deskripsi_detail,standard,nonstandard,tidak_terlayani,total,target");
        $this->db->from('tbl_dinas_indikator_detail');
        $this->db->join('tbl_dinas_indikator','tbl_dinas_indikator_detail.indikator_id=tbl_dinas_indikator.indikator_id');
        $this->db->where("tbl_dinas_indikator.id_tr_dinas",$id_tr_dinas);
        $query = $this->db->get();
        return $query->result_array();

        // $this->db->select("a.ID, a.indikator_id,a.deskripsi_detail,a.standard,a.nonstandard,a.total");
        // $this->db->from("tbl_dinas_indikator_detail a");
        // $this->db->join('tbl_indikator_detail b','b.indikator_id=a.indikator_id');
        // $this->db->where("b.id_tr_dinas",$id_tr_dinas);
        // $this->db->get()->result_array();


    }
    

    function updateFlagReport($id,$data)
    {
        $this->db->where("id_tr_dinas", $id);
        $this->db->update("tbl_dinas", $data);
    }
	
	 function updateFlagReport2($id,$data)
    {
        $this->db->where("id_tr_dinas", $id);
        $this->db->update("tbl_newanggaran", $data);
    }

    function updateIndikatorDetail($data)
    {
		//error_log($data);
		//error_log($id);
        //$this->db->where("ID", $id);
        $this->db->update("tbl_dinas_indikator_detail", $data);
		error_log($sql = $this->db->last_query());
		
		
    }
    
	function updateIndikatortotal($id,$data)
    {
        $this->db->where("indikator_id", $id);
        $this->db->update("tbl_dinas_indikator", $data);
    }
	
    function getListDokumen($id_tr_dinas){
        $this->db->where('id_tr_dinas', $id_tr_dinas);        
		$this->db->order_by('ID', "ASC");
        $result = $this->db->get('tbl_dinas_dokumen')->result_array();		
        return $result;
    }

    public function insert_file($filename, $id_tr_dinas, $desc)
   {
      $data = array(
         'filename'     => $filename,
         'id_tr_dinas'  => $id_tr_dinas,
         'keterangan'   => $desc
      );
      $this->db->insert('tbl_dinas_dokumen', $data);
      return $this->db->insert_id();
   }

   function get_file_name($id)
	{
		$this->db->select('*');
		$this->db->from('tbl_dinas_dokumen');
		$this->db->where('ID',$id);
		$query = $this->db->get();
		if($query->num_rows()>0){
			$out = $query->row();
			return $out;
		} else {
			return FALSE;
		}
	}

    function delete_file()
	{  
        $id = $_POST["id"];			
        $this->db->where('ID', $id);    
        return $this->db->delete('tbl_dinas_dokumen');
	}
	

    
}