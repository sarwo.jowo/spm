<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_m extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	//hitung jumlah anggota total
	function get_anggota_all() {
		$this->db->select('*');
		$this->db->from('member');
		$query = $this->db->get();
		return $query->num_rows();
	}
	
   
	public function charts()
	{
		$thn = date('Y');
		$data = $this->db->query("SELECT a.nama as nama,sum(b.nilai) as nilai from  area a INNER JOIN
                      tbl_pungli b ON a.id = b.areaid  GROUP BY b.areaid");
		return $data->result();
	}
	public function bln()
	{
		
		$thn = date('Y');
		$data = $this->db->query("SELECT MONTH(tgl_lapor) as bln from  tbl_pungli GROUP BY MONTH(tgl_lapor)");
		return $data->result();
	}
	
	public function charts2()
	{
		$thn = date('Y');
		$data = $this->db->query("SELECT MONTH(tgl_lapor) as bln,sum(b.nilai) as nilai from  tbl_pungli b  GROUP BY MONTH(tgl_lapor)");
		return $data->result();
	}
	public function charts3()
	{
		$thn = date('Y');
		$data = $this->db->query("SELECT sum(nilai) as nilai,propinsi as propinsi  from  tbl_pungli GROUP BY propinsi");
		return $data->result();
	}
	public function prop()
	{
		$thn = date('Y');
		$data = $this->db->query("SELECT propinsi  from  tbl_pungli GROUP BY propinsi");
		return $data->result();
	}
	
	
	///untuk propinsi
	public function bln_cp()
	{
		
		$thn = date('Y');
		$data = $this->db->query("SELECT MONTH(tgl_lapor) as bln from  tbl_pungli GROUP BY MONTH(tgl_lapor)");
		return $data->result();
	}
	public function cprop()
	{
		$thn = date('Y');
		$data = $this->db->query("SELECT kabupaten as kabupaten  from  tbl_pungli where propinsi = '".$this->session->userdata('propinsi')."' GROUP BY kabupaten");
		return $data->result();
	}
	public function charts4_cp()
	{
		$thn = date('Y');
		$data = $this->db->query("SELECT sum(nilai) as nilai,kabupaten as kabupaten  from  tbl_pungli where propinsi = '".$this->session->userdata('propinsi')."'  GROUP BY kabupaten");
		return $data->result();
	}
	public function charts3_cp()
	{
		$thn = date('Y');
		$data = $this->db->query("SELECT sum(nilai) as nilai,propinsi as propinsi  from  tbl_pungli where propinsi = '".$this->session->userdata('propinsi')."'  GROUP BY propinsi");
		return $data->result();
	}
	public function charts_cp()
	{
		$thn = date('Y');
		$data = $this->db->query("SELECT a.nama as nama,sum(b.nilai) as nilai from  area a INNER JOIN
                      tbl_pungli b ON a.id = b.areaid  where b.propinsi = '".$this->session->userdata('propinsi')."' GROUP BY b.areaid");
		return $data->result();
	}
	public function charts2_cp()
	{
		$thn = date('Y');
		$data = $this->db->query("SELECT MONTH(tgl_lapor) as bln, sum(b.nilai) as nilai from  tbl_pungli b  where b.propinsi = '".$this->session->userdata('propinsi')."' GROUP BY MONTH(tgl_lapor)");
		return $data->result();
	}
	//////////////////////////////
	///untuk kabupaten
	public function bln_kb()
	{
		
		$thn = date('Y');
		$data = $this->db->query("SELECT MONTH(tgl_lapor) as bln from  tbl_pungli where kabupaten = '".$this->session->userdata('kabupaten')."' GROUP BY MONTH(tgl_lapor)");
		return $data->result();
	}
	
	
	public function charts_kb()
	{
		$thn = date('Y');
		$data = $this->db->query("SELECT a.nama as nama,sum(b.nilai) as nilai from  area a INNER JOIN
                      tbl_pungli b ON a.id = b.areaid  where b.kabupaten = '".$this->session->userdata('kabupaten')."' GROUP BY b.areaid");
		return $data->result();
	}
	public function charts2_kb()
	{
		$thn = date('Y');
		$data = $this->db->query("SELECT sum(b.nilai) as nilai, MONTH(tgl_lapor) as bln from  tbl_pungli b  where b.kabupaten = '".$this->session->userdata('kabupaten')."' GROUP BY MONTH(tgl_lapor)");
		return $data->result();
	}
	
	///////////
	function get_lapor_tahun($tahun) {
		$this->db->select('*');
		$this->db->from('tbl_pungli');
		$thn = date('Y');
		$bln = date('m');
		$where = "(YEAR(tgl) = '".$thn."' AND  MONTH(tgl) = '".$bln."')";
		$this->db->where($where);
		$query = $this->db->get();
		if($query->num_rows()>0) {
			$out = $query->result();
			return $out;
		} else {
			return array();
		}	
	}	
    function get_about()
	{
	  $this->db->select("*");
	  $this->db->from('about');
	  //$this->db->order_by('id', 'desc');
	  $this->db->limit('1');
	  $query = $this->db->get();
	  return $query->result();
	}
	public function get_data()
	{
		$this->db->select('*');
		$this->db->like('tanggal','2013-');
		$this->db->order_by('tanggal','asc');
		//$this->db->limit('10');
		return $this->db->get('tbl_pendapatan');
	}
	function get_berita()
	{
	  $this->db->select("*");
	  $this->db->from('berita');
	  $this->db->order_by('id', 'desc');
	  $this->db->limit('10');
	  $query = $this->db->get();
	  return $query->result();
	}
	function get_berita2()
	{
	  $this->db->select("*");
	  $this->db->from('berita');
	  $this->db->order_by('id', 'desc');
	  $this->db->limit('50');
	  $query = $this->db->get();
	  return $query->result();
	}
	function get_berita_utm()
	{
	  $this->db->select("*");
	  $this->db->from('berita');
	  $this->db->where('utama', '1');
	  $this->db->order_by('id', 'desc');
	  $this->db->limit('1');
	  $query = $this->db->get();
	  return $query->result();
	}
	function get_berita_utm2()
	{
	  $this->db->select("*");
	  $this->db->from('berita');
	  $this->db->where('utama', '1');
	  $this->db->order_by('id', 'desc');
	  $this->db->limit('5');
	  $query = $this->db->get();
	  return $query->result();
	}
	
	function get_det_berita($newsdetail)
	{
	    $this->db->select('*');
		$this->db->from('berita');
		$this->db->where('id', $newsdetail);
		//$this->db->order_by('id', 'desc');
		//$this->db->limit(1);		
		$query = $this->db->get();
		if($query->num_rows()>0){
			$out = $query->row();
			return $out;
		} else {
			return FALSE;
		}
	}
	
	//menghitung jumlah points
	function get_sum_lap() {
		$cnts = '00';
		$this->db->select('*');
		$this->db->where('uname', $this->session->userdata('u_name'));	
		$this->db->from('tbl_pungli');	
		$query = $this->db->get();
		$cnt = $cnt = $query->num_rows();
		return $cnt;
	}
	
	function get_sum_lap_all() {
		$cnts = '00';
		$this->db->select('*');		
		$this->db->from('tbl_pungli');	
		$query = $this->db->get();	
	     $cnts = $query->num_rows();
		return $cnts;
	}
  	
	function get_sum_user() {
		$cnts = '00';
		$this->db->select('*');
       // $this->db->where('uname', $this->session->userdata('u_name'));		
		
		$this->db->from('tbl_user');	
		$query = $this->db->get();
        $cnts = $query->num_rows();
		return $cnts;
	}

	
}