<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dept_m extends CI_Model {

	public function __construct() {
		parent::__construct();
	}	
	function get_dept() {
		$this->db->select('*');
        $query = $this->db->get('departement');
        if ($query->num_rows() > 0)
        {
            $nama_dept = array();
            foreach ($query->result_array() as $row)
            {
                $nama_dept[$row['deptid']] = $row['nama']; 
            }
        }
        $query->free_result();
        return $nama_dept;
	}

	
	public function delete($id){
		return $this->db->delete('departement', array('id' => $id)); 
	}
}