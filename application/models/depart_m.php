<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Depart_m extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	#panggil data kas
	function get_data_ruang() {
		$this->db->select('*');
		$this->db->from('departement');		
		//$this->db->where('st', '1');
		$this->db->order_by('id', 'ASC');
		$query = $this->db->get();
		if($query->num_rows()>0){
			$out = $query->result();
			return $out;
		} else {
			return FALSE;
		}
	}
	
	
	function get_list_depart() {
		$this->db->select('*');
		$this->db->from('departement');		
		//$this->db->where('st', '1');
		$this->db->order_by('id', 'ASC');
		$query = $this->db->get();
		if($query->num_rows()>0){
			$out = $query->result();
			return $out;
		} else {
			return FALSE;
		}
	}
	
	function get_jenis_ruang($rgid) {
		$this->db->select('*');
		$this->db->from('ruang');
		$this->db->where('id',$rgid);
		$query = $this->db->get();
		
		
		
		if($query->num_rows()>0){
			$out = $query->result();
			return $out;
		} else {
			return FALSE;
		}		 
	}

	//hitung jumlah total 
	function get_jml_pengeluaran() {
		$this->db->select('SUM(jumlah) AS jml_total');
		$this->db->from('tbl_trans_kas');
		$this->db->where('akun','Pengeluaran');
		$query = $this->db->get();
		return $query->row();
	}



	public function delete($id){
		return $this->db->delete('tbl_trans_kas', array('id' => $id)); 
	}
}